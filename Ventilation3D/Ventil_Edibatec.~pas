Unit Ventil_Edibatec;
                       
{$Include 'Ventil_Directives.pas'}

Interface
Uses
db,
  SysUtils, Forms, ExtCtrls, COntrols,
  CatalMain, CatalServices;

Const

  c_Edibatec_Caisson         = 'CAISVEN';
  c_Edibatec_Conduits        = 'CNDCIRC';
  c_Edibatec_SortiesToit     = 'SORTOIT';
  c_Edibatec_Fournitures     = 'FOURAER';
  c_Edibatec_EntreesAir      = 'VMCENTR';                                                                                    
  c_Edibatec_Bouches         = 'VMCEXTR';
  c_Edibatec_Clapets         = 'CLAPCPF';
  c_Edibatec_Accessoires     = 'ACCRESO';
  c_Edibatec_Reductions      = 'REDUAER';
  c_Edibatec_Confluences     = 'CONFAER';
  c_Edibatec_Coudes          = 'COUDAER';
  c_Edibatec_Silencieux      = 'SILENCA';
  c_Edibatec_AccessoGen      = 'ACCESSO';
  c_Edibatec_Associations    = 'ASSOCIA';
  c_Edibatec_RegulateurDebit = 'REG_DEB';
  c_Edibatec_Registre        = 'REGSTRA';


  {$IFDEF FRANCEAIR_COLLECTAIR}
  c_Edibatec_GammeCoude = 'COUDAERF_A12';
  {$ELSE}
        {$IFDEF ANJOS_OPTIMA3D}
        c_Edibatec_GammeCoude = 'COUDAERXXX15';
        c_Edibatec_GammePiegesASon = 'SILENCAXXX13';
        {$ELSE}
        c_Edibatec_GammeCoude = '';
        c_Edibatec_GammePiegesASon = '';
        {$ENDIF}
  {$ENDIF}

{$IfDef AERAU_CLIMAWIN}
c_Edibatec_ListeCodesFabricants : Array[1..4] Of String = ('VIM', 'ANJ', 'F_A', 'CAL');
{$Else}
  {$IfDef VIM_OPTAIR}
  c_Edibatec_ListeCodesFabricants : Array[1..1] Of String = ('VIM');
  {$ENDIF}
{$IfDef CALADAIR_VMC}
c_Edibatec_ListeCodesFabricants : Array[1..1] Of String = ('CAL');
{$ELSE}
{$IfDef LINDAB_VMC}
c_Edibatec_ListeCodesFabricants : Array[1..1] Of String = ('LIN');
{$ELSE}
  {$IfDef FRANCEAIR_COLLECTAIR}
  c_Edibatec_ListeCodesFabricants : Array[1..1] Of String = ('F_A');
  {$Else}
        {$IfDef OUESTVENTIL}
        c_Edibatec_ListeCodesFabricants : Array[1..1] Of String = ('001');
        {$ELSE}
                {$IfDef ANJOS_OPTIMA3D}
                c_Edibatec_ListeCodesFabricants : Array[1..1] Of String = ('ANJ');
                {$ENDIF}
        {$ENDIF}
  {$ENDIF}
{$ENDIF}
{$ENDIF}
{$ENDIF}

  {$IFDEF VIM_OPTAIR}
  c_Edibatec_ListeCodesClasses : Array[1..16] Of String = ('ASSOCIA', 'SILENCA', 'COUDAER', 'CONFAER', 'REDUAER', 'ACCRESO',
                                                           'CLAPCPF', 'VMCEXTR', 'VMCENTR', 'FOURAER', 'SORTOIT', 'CAISVEN',
                                                           'ACCESSO', 'CNDCIRC', 'REG_DEB', 'REGSTRA');
  c_Edibatec_CodesClassesPourCatalogue : String = 'CNDCIRC;SILENCA;COUDAER;CONFAER;REDUAER;ACCRESO;CLAPCPF;VMCEXTR;VMCENTR;FOURAER;SORTOIT;CAISVEN;ACCESSO;ASSOCIA;REG_DEB;REGSTRA';
  {$ELSE}
  c_Edibatec_ListeCodesClasses : Array[1..16] Of String = ('ASSOCIA', 'SILENCA', 'COUDAER', 'CONFAER', 'REDUAER', 'ACCRESO',
                                                           'CLAPCPF', 'VMCEXTR', 'VMCENTR', 'FOURAER', 'SORTOIT', 'CAISVEN',
                                                           'ACCESSO', 'CNDCIRC', 'REG_DEB', 'REGSTRA');
  c_Edibatec_CodesClassesPourCatalogue : String = 'CNDCIRC;SILENCA;COUDAER;CONFAER;REDUAER;ACCRESO;CLAPCPF;VMCEXTR;VMCENTR;FOURAER;SORTOIT;CAISVEN;ACCESSO;ASSOCIA;REG_DEB;REGSTRA';
  {$ENDIF}

  { ************************************************************  Bouches d'extraction B023 ****************************************************** }
  { B023BAA } c_Edibatec_Bouche_TypeBouche = 'TYPE_DE_BOUCHE'; // Autor�glable|Hygror�glable|VMC gaz (auto et thermor�glable)|Autre
  { B023BEA } c_Edibatec_Bouche_TypeHygro  = 'TYPE_HYGRO'; // A|B|Clim|Gaz|Autre
  { B023BAC } c_Edibatec_Bouche_Piece      = 'DESTINATION'; // Cuisine|WC|Pi�ce Humide|Salle de bains|Salles d'eau|Autre pi�ce
  { B023BAV } c_Edibatec_Bouche_QMini      = 'DEBIT_MINIMAL'; // Q Mini nominal
  { B023BAX } c_Edibatec_Bouche_QMaxi      = 'DEBIT_MAXIMAL'; // Q Maxi nominal
  { B023BEI } c_Edibatec_Bouche_QPointe    = 'DEBIT_DE_POINTE'; // Q Pointe
  { B023BBD } c_Edibatec_Bouche_PMini      = 'DEPRESSION_MINIMALE'; // Pression minimale de fonctionnement
  { B023BBE } c_Edibatec_Bouche_PMaxi      = 'DEPRESSION_MAXIMALE'; // Pression maximale de fonctionnement
  { B023BBR } c_Edibatec_Bouche_Dnew       = 'DNEW'; // Isolement acoustique de la bouche obtenue sans aucun dispositif acoustique en option (dB)
  { B023BBU } c_Edibatec_Bouche_Dnew_Isol  = 'DNEW_ISOL'; // Isolement acoustique de la bouche obtenue avec dispositif acoustique (dB)
  { B023BFD } c_Edibatec_Bouche_Bandes_Dne = 'BANDES_DNE';
  { B023BDZ } c_Edibatec_Bouche_Bandes_Lw  = 'BANDES_LW2';
              c_Edibatec_Bouche_Reglage_Min= 'REGLAGE_MIN';
              c_Edibatec_Bouche_Reglage_Max= 'REGLAGE_MAX';
              c_Edibatec_Bouche_Coef_A     = 'COEF_A';
              c_Edibatec_Bouche_Coef_B     = 'COEF_B';
              c_Edibatec_Bouche_Coef_C     = 'COEF_C';
              c_Edibatec_Bouche_Coef_D     = 'COEF_D';
              c_Edibatec_Bouche_Dzeta      = 'DZETA';
              c_Edibatec_Bouche_Diametre   = 'DIAMETRE';
  {         } c_Edibatec_Bouche_DPMini     = 'DEPRESSION_MINIMALE'; //
  {         } c_Edibatec_Bouche_DPMaxi     = 'DEPRESSION_MAXIMALE'; //

  { ************************************************************  Entr��s d'air ****************************************************** }
  { B023BAA } c_Edibatec_EA_TypeEA = 'TYPE_D''ENTREE D''AIR'; // Autor�glable|Hygror�glable|VMC gaz (auto et thermor�glable)|Autre
  { B023BEA } c_Edibatec_EA_TypeHygro  = 'TYPE_HYGRO'; // A|B|Clim|Gaz|Autre
  { B023BAC } c_Edibatec_EA_Piece      = 'DESTINATION'; // Cuisine|WC|Pi�ce Humide|Salle de bains|Salles d'eau|Autre pi�ce
  { B023BAV } c_Edibatec_EA_QMini      = 'DEBIT_MINIMAL'; // Q Mini nominal
  { B023BAX } c_Edibatec_EA_QMaxi      = 'DEBIT_MAXIMAL'; // Q Maxi nominal
  { B023BEI } c_Edibatec_EA_QPointe    = 'DEBIT_DE_POINTE'; // Q Pointe
  { B023BBD } c_Edibatec_EA_PMini      = 'DEPRESSION_MINIMALE'; // Pression minimale de fonctionnement
  { B023BBE } c_Edibatec_EA_PMaxi      = 'DEPRESSION_MAXIMALE'; // Pression maximale de fonctionnement
  { B023BBR } c_Edibatec_EA_Dnew       = 'DNEW'; // Isolement acoustique de la EA obtenue sans aucun dispositif acoustique en option (dB)
  { B023BBU } c_Edibatec_EA_Dnew_Isol  = 'DNEW_ISOL'; // Isolement acoustique de la EA obtenue avec dispositif acoustique (dB)
  { B023BFD } c_Edibatec_EA_Bandes_Dne = 'BANDES_DNE';
  { B023BDZ } c_Edibatec_EA_Bandes_Lw  = 'BANDES_LW2';
              c_Edibatec_EA_Reglage_Min= 'REGLAGE_MIN';
              c_Edibatec_EA_Reglage_Max= 'REGLAGE_MAX';
              c_Edibatec_EA_Coef_A     = 'COEF_A';
              c_Edibatec_EA_Coef_B     = 'COEF_B';
              c_Edibatec_EA_Coef_C     = 'COEF_C';
              c_Edibatec_EA_Coef_D     = 'COEF_D';
              c_Edibatec_EA_Dzeta      = 'DZETA';
              c_Edibatec_EA_Diametre   = 'DIAMETRE';
  {         } c_Edibatec_EA_DPMini     = 'DEPRESSION_MINIMALE'; //
  {         } c_Edibatec_EA_DPMaxi     = 'DEPRESSION_MAXIMALE'; //
  { ************************************************************  Regulateurs de d�bit ****************************************************** }
  {         } c_Edibatec_RegulateurDebit_QMini      = 'DEBIT_MINI'; // Q Mini nominal
  {         } c_Edibatec_RegulateurDebit_QMaxi      = 'DEBIT_MAXI'; // Q Mini nominal
  {         } c_Edibatec_RegulateurDebit_PMini      = 'DEPRESSION_MINI'; // Pression minimale de fonctionnement
  {         } c_Edibatec_RegulateurDebit_PMaxi      = 'DEPRESSION_MAXI'; // Pression maximale de fonctionnement
              c_Edibatec_RegulateurDebit_Dzeta      = 'DZETA';
  { ************************************************************  Silencieux ****************************************************** }
  {         } c_Edibatec_Silencieux_Matiere         = 'MATIERE'; //
  {         } c_Edibatec_Silencieux_Diametre        = 'DIAMETRE'; //
  {         } c_Edibatec_Silencieux_Spectre         = 'SPECTRE'; //
  { ************************************************************  Registr****************************************************** }
  {         } c_Edibatec_Registre_Diametre        = 'DIAMETRE'; //
  {         } c_Edibatec_Registre_DebitMini       = 'DEBIT_MIN'; //
  {         } c_Edibatec_Registre_DebitMaxi       = 'DEBIT_MAX'; //
  {         } c_Edibatec_Registre_DPMini          = 'DP_MINIMAL'; //
  {         } c_Edibatec_Registre_DPMaxi          = 'DP_MAXIMAL'; //
  {         } c_Edibatec_Registre_ReglageMini     = 'REGLAGE_MINIMAL'; //
  {         } c_Edibatec_Registre_ReglageMaxi     = 'REGLAGE_MAXIMAL'; //
  {         } c_Edibatec_Registre_Coef_A          = 'COEFF_A'; //
  {         } c_Edibatec_Registre_Coef_B          = 'COEFF_B'; //
  {         } c_Edibatec_Registre_Coef_C          = 'COEFF_C'; //
  {         } c_Edibatec_Registre_Coef_D          = 'COEFF_D'; //
  { ************************************************************  Conduits circulaires B093 ******************************************************* }
  { B093BAA } c_Edibatec_Gaine_Diametre  = 'DIAMETRE_INT';
              c_Edibatec_Gaine_Matiere   = 'MATIERE';           // Acier galvanis�|Aluminium|Veloduct
              c_Edibatec_Gaine_Longueur  = 'LONGUEUR_DE_LA_BARRE';
  { ************************************************************  Silencieux a�rauliques B318 ***************************************************** }
  { B318BAB } c_Edibatec_PiegeASon_Diametre  = 'DIAMETRE_INT';
  { ************************************************************  Coudes a�rauliques B088 ********************************************************* }
  { B088BAE } c_Edibatec_Coude_Diametre = 'HAUT_LARG_DIAM';
  { B088BAC } c_Edibatec_Coude_Angle    = 'ANGLE';   // 30�|45�|60�|90�
              c_Edibatec_Coude_Matiere  = 'MATIERE'; // Acier galvanis�|Aluminium|Veloduct
  { ************************************************************  Confluences a�rauliques B089 **************************************************** }
  { B089BAE } c_Edibatec_Confluence_DiametreRect  = 'HAUT_LARG_DIAM_R';
  { B089BAI } c_Edibatec_Confluence_DiametreLat   = 'HAUT_LARG_DIAM_L';
  { B089BAC } c_Edibatec_Confluence_Angle         = 'ANGLE';           // Equerre(90�)|Oblique(45�)|Plat(180�)
  { B089BAN } c_Edibatec_Confluence_TypCollecteur = 'TYPE_COLLECTEUR'; // Equerre(90�)|Oblique(45�)|Plat(180�)
              c_Edibatec_Confluence_TypConfluence = 'TYPE_DE_CONFLUENCE'; // Culotte|Culotte horizontale|Culotte verticale|Piquage|Piquage sur plat|Croix|T�|Collecteur raccord d'�tage
              c_Edibatec_Confluence_NbSorties     = 'NOMBRE_DE_SORTIE';
              c_Edibatec_Confluence_Matiere       = 'MATIERE';           // Acier galvanis�|Aluminium|Veloduct

  { ************************************************************  Accessoires a�rauliques B003 **************************************************** }
  { B003BAD } c_Edibatec_Accessoire_Diametre = 'HAUT_LARG_DIAM';
              c_Edibatec_Accessoire_Matiere  = 'MATIERE'; // Acier galvanis�|Aluminium|Veloduct
  { ************************************************************  Clapets coupe-feu B078 ********************************************************** }
  { B078BAE } c_Edibatec_CoupeFeu_Diametre = 'HAUTEUR_MINI_DIAM';
              c_Edibatec_CoupeFeu_DebitMini= 'DEBIT_MINI';
              c_Edibatec_CoupeFeu_DebitMaxi= 'DEBIT_MAXI';
              c_Edibatec_CoupeFeu_Dzeta    = 'DZETA';

  { ************************************************************  R�ductions a�rauliques B090 ***************************************************** }
  { B090BAE } c_Edibatec_Reduction_GrandDiametre = 'GRAND_DIAMETRE';
  { B090BAG } c_Edibatec_Reduction_PetitDiametre = 'PETIT_DIAMETRE';
              c_Edibatec_Reduction_TypeReduction = 'TYPE_REDUCTION'; // Plate|Conique
              c_Edibatec_Reduction_Matiere       = 'MATIERE';     // Acier galvanis�|Aluminium|Veloduct
              c_Edibatec_Reduction_Connectique   = 'CONNECTIQUE'; //
              c_Edibatec_Reduction_Genre         = 'GENRE'; //              
  
  { ************************************************************  Sorties toiture B341 ************************************************************ }
  { B341BAA } c_Edibatec_SortieToiture_Diametre = 'DIAMETRE';
  {         } c_Edibatec_SortieToiture_Dzeta    = 'DZETA';
  { ************************************************************  Caissons B050 ******************************************************************* }
  {  }        c_Edibatec_Caisson_DiametreRejet             = 'DIAMETRE_REJET';
  {  }        c_Edibatec_Caisson_DiametreAspiration        = 'DIAMETRE_ASPIRATION';
  { B050BAX } c_Edibatec_Caisson_ClassementFeu             = 'CLASSEMENT_FEU';
  { B050BAK } c_Edibatec_Caisson_CourbeDebitPressionMini   = 'COURBE_CARACT_V_MIN'; // B�zier
  { B050BAM } c_Edibatec_Caisson_CourbeDebitPressionMaxi   = 'COURBE_CARACT_V_MAX'; // B�zier
              c_Edibatec_Caisson_CourbeDebitPressionInter  = 'COURBE_CARACT_V_INTER';
              c_Edibatec_Caisson_CourbeDebitPuissanceMini  = 'COURBE_PUISSANCE_ABSMIN';
              c_Edibatec_Caisson_CourbeDebitPuissanceMaxi  = 'COURBE_PUISSANCE_ABSMAX';
              c_Edibatec_Caisson_CourbeDebitPuissanceInter = 'COURBE_PUISSANCE_ABSINTER';
  { B050BAH } c_Edibatec_Caisson_DebitMaxi                 = 'DEBIT_MAXIMAL';
  { B050BAG } c_Edibatec_Caisson_DebitMini                 = 'DEBIT_MINIMAL';
              c_Edibatec_Caisson_VitesseMini               = 'VITESSE_MINIMALE';
              c_Edibatec_Caisson_VitesseMaxi               = 'VITESSE_MAXIMALE';
              c_Edibatec_Caisson_TypeVentilateur           = 'TYPE_VENTILATEUR';
              c_Edibatec_Caisson_Entrainement              = 'ENTRAINEMENT';
              c_Edibatec_Caisson_Alimentation              = 'ALIMENTATION';
              c_Edibatec_Caisson_TypeDeMoteur              = 'TYPE_DE_MOTEUR';
              c_Edibatec_Caisson_Depressostat              = 'DEPRESSOSTAT___INTER_MONT';
              c_Edibatec_Caisson_PositionAspiration        = 'POSITION_ASPIRATION';
              c_Edibatec_Caisson_PositionRefoulement       = 'POSITION_REFOULEMENT';
              c_Edibatec_Caisson_Spectre                   = 'SPECTRE';
  { ************************************************************  Associations ********************************************************************* }
              c_Edibatec_Associations_RefProduit       = 'DESIGNATION_ARTICLE';
              c_Edibatec_Associations_CodeAssociation  = 'CODE_ACCESSOIRE';
              c_Edibatec_Associations_RefAssociation   = 'DESIGNATION_ACCESSOIRE';
              c_Edibatec_Associations_Qte              = 'QTE'; 

  c_Edibatec_Caisson_Type_Caisson   = 1;
  c_Edibatec_Caisson_Type_Tourelle  = 2;

  c_Edibatec_Caisson_Entrainement_Direct       = 1;
  c_Edibatec_Caisson_Entrainement_DirectPoulie = 2;

  c_Edibatec_Caisson_Alimentation_MonoPhase = 1;
  c_Edibatec_Caisson_Alimentation_TriPhase  = 2;

  c_Edibatec_Caisson_Moteur_1Vitesse = 1;
  c_Edibatec_Caisson_Moteur_2Vitesse = 2;

  c_Edibatec_Caisson_Depressostat_OUI = 1;
  c_Edibatec_Caisson_Depressostat_NON = 2;

  c_Edibatec_Caisson_Agrement_NonAgree      = 1;
  c_Edibatec_Caisson_Agrement_Agree4001h2C4 = 2;
  c_Edibatec_Caisson_Agrement_Agree4001h    = 3;
  c_Edibatec_Caisson_Agrement_Agree4002h    = 4;

  c_Edibatec_Caisson_Agrement_Libelle :
        array [c_Edibatec_Caisson_Agrement_NonAgree..c_Edibatec_Caisson_Agrement_Agree4002h] of String =
        ('Non agr��', 'Agr�� 400� 1/2h - C4', 'Agr�� 400� 1h', 'Agr�� 400� 2h');


  c_CaissonSelect_3Courbes            = 1;
  c_CaissonSelect_PageFct             = 2;
  c_CaissonSelect_DebitCst            = 3;
  c_CaissonSelect_PlageEtCourbeLimite = 4;

  { Connexion � l'outil catalogues de vincent }
Function ConnexionEdibatec: Boolean;
{ Consultation des banques dans l'outil de vincent }
Procedure ConsultationEdibatec;
{ Choix d'un produit dans l'outil de vincent }
Function ChoisirProduit(_CodeEdibatec: String; _ToutesClasses: Boolean): String;
{ Choix d'une gamme dans l'outil de vincent }
Function ChoixGammeDansClasse(_DansClasse: String; _OldGamme: String = ''): String;


Implementation
Uses
  CatalBdd, AccesBdd,
  Ventil_FIREBIRD, Ventil_Declarations;

{ ************************************************************************************************************************************************* }
Function ConnexionEdibatec: Boolean;
{
var
_tableACCRESO : TTableDeDonneesBBS;
_tableASSOCIA : TTableDeDonneesBBS;
_tableCAISVEN : TTableDeDonneesBBS;
_tableCNDCIRC : TTableDeDonneesBBS;
_tableCONFAER : TTableDeDonneesBBS;
_tableCOUDAER : TTableDeDonneesBBS;
_tableFOURAER : TTableDeDonneesBBS;
_tableREGSTRA : TTableDeDonneesBBS;
_tableREG_DEB : TTableDeDonneesBBS;
_tableSILENCA : TTableDeDonneesBBS;
_tableSORTOIT : TTableDeDonneesBBS;
_tableVMCENTR : TTableDeDonneesBBS;
_tableVMCEXTR : TTableDeDonneesBBS;
}
Begin
{$IFDEF CALADAIR_VMC}
Config.Code_Fabricant := 'CAL';
Config.Nom_Fabricant := 'CALADAIR';
{$ELSE}
{$IFDEF LINDAB_VMC}
Config.Code_Fabricant := 'LIN';
Config.Nom_Fabricant := 'LINDAB';
{$ELSE}
  {$IFDEF FRANCEAIR_COLLECTAIR}
  Config.Code_Fabricant := 'F_A';
  Config.Nom_Fabricant := 'FRANCEAIR';
  {$ELSE}
        {$IFDEF OUESTVENTIL}
        Config.Code_Fabricant := '001';
        Config.Nom_Fabricant := 'OUESTVENTIL';
        {$ELSE}
                {$IFDEF ANJOS_OPTIMA3D}
                Config.Code_Fabricant := 'ANJ';
                Config.Nom_Fabricant := 'ANJOS';
                {$ELSE}
                Config.Code_Fabricant := 'VIM';
                Config.Nom_Fabricant := 'VIM';
                {$ENDIF}
        {$ENDIF}
  {$ENDIF}
{$ENDIF}
{$ENDIF}
  Config.TypeBase := FireBird;
  Config.Autorise_Banque:= True;
  Config.Autorise_Banque_Personelle:=False;
  Config.Autorise_Catalogue:=False;
  Config.SaisieEdibatec:=False;                                              
  Config.ConsultationUniquement := False;
  Config.TypeDico := Dico_BBS;

{$IfDef AERAU_CLIMAWIN}

        {$IfDef BIM_METRE}
          Config.NomBaseBBS := 'Ventil_DATA.FDB';
          Config.NomBaseBBS := 'BANQUE_FAB_BBS.BBS';
        {$Else}
          Config.NomBaseBBS := 'BANQUE_FAB_BBS.BBS';
        {$EndIf}

{$Else}
  Config.NomBaseBBS := 'Ventil_DATA.FDB';
{$EndIf}
  Config.MultiFabricant := False;
  Result := Connection_DataBase(ExtractFilePath(Application.Exename) + {$IfDef AERAU_CLIMAWIN}'Banques\'{$Else}'Bibliotheque\'{$EndIf}, '', '', '', Config.Autorise_Banque,Config.Autorise_Banque_Personelle,Config.Autorise_Catalogue, True, '', '');
{
_tableACCRESO := TTableDeDonneesBBS.Create(Base_Banque, 'ACCRESO');
  With _tableACCRESO.Donnees Do Begin
    FieldDefs.Add('CODE_CLASSE', ftString, 7, True);
    FieldDefs.Add('CODE_FABRICANT', ftString, 3, True);
    FieldDefs.Add('CODE_GAMME', ftString, 2, True);
    FieldDefs.Add('CODE_PRODUIT', ftString, 15, True);    
    IndexDefs.Add('CLE_ACCRESO', 'CODE_CLASSE;CODE_FABRICANT;CODE_GAMME;CODE_PRODUIT', [ixPrimary, ixUnique]);
  End;
_tableACCRESO.Initialisation;

_tableASSOCIA := TTableDeDonneesBBS.Create(Base_Banque, 'ASSOCIA');
  With _tableASSOCIA.Donnees Do Begin
    FieldDefs.Add('CODE_CLASSE', ftString, 7, True);
    FieldDefs.Add('CODE_FABRICANT', ftString, 3, True);
    FieldDefs.Add('CODE_GAMME', ftString, 2, True);
    FieldDefs.Add('CODE_PRODUIT', ftString, 15, True);
    IndexDefs.Add('CLE_ASSOCIA', 'CODE_CLASSE;CODE_FABRICANT;CODE_GAMME;CODE_PRODUIT', [ixPrimary, ixUnique]);
  End;
_tableASSOCIA.Initialisation;

_tableCAISVEN := TTableDeDonneesBBS.Create(Base_Banque, 'CAISVEN');
  With _tableCAISVEN.Donnees Do Begin
    FieldDefs.Add('CODE_CLASSE', ftString, 7, True);
    FieldDefs.Add('CODE_FABRICANT', ftString, 3, True);
    FieldDefs.Add('CODE_GAMME', ftString, 2, True);
    FieldDefs.Add('CODE_PRODUIT', ftString, 15, True);
    IndexDefs.Add('CLE_CAISVEN', 'CODE_CLASSE;CODE_FABRICANT;CODE_GAMME;CODE_PRODUIT', [ixPrimary, ixUnique]);
  End;
_tableCAISVEN.Initialisation;

_tableCNDCIRC := TTableDeDonneesBBS.Create(Base_Banque, 'CNDCIRC');
  With _tableCNDCIRC.Donnees Do Begin
    FieldDefs.Add('CODE_CLASSE', ftString, 7, True);
    FieldDefs.Add('CODE_FABRICANT', ftString, 3, True);
    FieldDefs.Add('CODE_GAMME', ftString, 2, True);
    FieldDefs.Add('CODE_PRODUIT', ftString, 15, True);
    IndexDefs.Add('CLE_CNDCIRC', 'CODE_CLASSE;CODE_FABRICANT;CODE_GAMME;CODE_PRODUIT', [ixPrimary, ixUnique]);
  End;
_tableCNDCIRC.Initialisation;

_tableCONFAER := TTableDeDonneesBBS.Create(Base_Banque, 'CONFAER');
  With _tableCONFAER.Donnees Do Begin
    FieldDefs.Add('CODE_CLASSE', ftString, 7, True);
    FieldDefs.Add('CODE_FABRICANT', ftString, 3, True);
    FieldDefs.Add('CODE_GAMME', ftString, 2, True);
    FieldDefs.Add('CODE_PRODUIT', ftString, 15, True);
    IndexDefs.Add('CLE_CONFAER', 'CODE_CLASSE;CODE_FABRICANT;CODE_GAMME;CODE_PRODUIT', [ixPrimary, ixUnique]);
  End;
_tableCONFAER.Initialisation;

_tableCOUDAER := TTableDeDonneesBBS.Create(Base_Banque, 'COUDAER');
  With _tableCOUDAER.Donnees Do Begin
    FieldDefs.Add('CODE_CLASSE', ftString, 7, True);
    FieldDefs.Add('CODE_FABRICANT', ftString, 3, True);
    FieldDefs.Add('CODE_GAMME', ftString, 2, True);
    FieldDefs.Add('CODE_PRODUIT', ftString, 15, True);
    IndexDefs.Add('CLE_COUDAER', 'CODE_CLASSE;CODE_FABRICANT;CODE_GAMME;CODE_PRODUIT', [ixPrimary, ixUnique]);
  End;
_tableCOUDAER.Initialisation;

_tableFOURAER := TTableDeDonneesBBS.Create(Base_Banque, 'FOURAER');
  With _tableFOURAER.Donnees Do Begin
    FieldDefs.Add('CODE_CLASSE', ftString, 7, True);
    FieldDefs.Add('CODE_FABRICANT', ftString, 3, True);
    FieldDefs.Add('CODE_GAMME', ftString, 2, True);
    FieldDefs.Add('CODE_PRODUIT', ftString, 15, True);
    IndexDefs.Add('CLE_FOURAER', 'CODE_CLASSE;CODE_FABRICANT;CODE_GAMME;CODE_PRODUIT', [ixPrimary, ixUnique]);
  End;
_tableFOURAER.Initialisation;

_tableREGSTRA := TTableDeDonneesBBS.Create(Base_Banque, 'REGSTRA');
  With _tableREGSTRA.Donnees Do Begin
    FieldDefs.Add('CODE_CLASSE', ftString, 7, True);
    FieldDefs.Add('CODE_FABRICANT', ftString, 3, True);
    FieldDefs.Add('CODE_GAMME', ftString, 2, True);
    FieldDefs.Add('CODE_PRODUIT', ftString, 15, True);
    IndexDefs.Add('CLE_REGSTRA', 'CODE_CLASSE;CODE_FABRICANT;CODE_GAMME;CODE_PRODUIT', [ixPrimary, ixUnique]);
  End;
_tableREGSTRA.Initialisation;

_tableREG_DEB := TTableDeDonneesBBS.Create(Base_Banque, 'REG_DEB');
  With _tableREG_DEB.Donnees Do Begin
    FieldDefs.Add('CODE_CLASSE', ftString, 7, True);
    FieldDefs.Add('CODE_FABRICANT', ftString, 3, True);
    FieldDefs.Add('CODE_GAMME', ftString, 2, True);
    FieldDefs.Add('CODE_PRODUIT', ftString, 15, True);
    IndexDefs.Add('CLE_REG_DEB', 'CODE_CLASSE;CODE_FABRICANT;CODE_GAMME;CODE_PRODUIT', [ixPrimary, ixUnique]);
  End;
_tableREG_DEB.Initialisation;

_tableSILENCA := TTableDeDonneesBBS.Create(Base_Banque, 'SILENCA');
  With _tableSILENCA.Donnees Do Begin
    FieldDefs.Add('CODE_CLASSE', ftString, 7, True);
    FieldDefs.Add('CODE_FABRICANT', ftString, 3, True);
    FieldDefs.Add('CODE_GAMME', ftString, 2, True);
    FieldDefs.Add('CODE_PRODUIT', ftString, 15, True);
    IndexDefs.Add('CLE_SILENCA', 'CODE_CLASSE;CODE_FABRICANT;CODE_GAMME;CODE_PRODUIT', [ixPrimary, ixUnique]);
  End;
_tableSILENCA.Initialisation;

_tableSORTOIT := TTableDeDonneesBBS.Create(Base_Banque, 'SORTOIT');
  With _tableSORTOIT.Donnees Do Begin
    FieldDefs.Add('CODE_CLASSE', ftString, 7, True);
    FieldDefs.Add('CODE_FABRICANT', ftString, 3, True);
    FieldDefs.Add('CODE_GAMME', ftString, 2, True);
    FieldDefs.Add('CODE_PRODUIT', ftString, 15, True);
    IndexDefs.Add('CLE_SORTOIT', 'CODE_CLASSE;CODE_FABRICANT;CODE_GAMME;CODE_PRODUIT', [ixPrimary, ixUnique]);
  End;
_tableSORTOIT.Initialisation;

_tableVMCENTR := TTableDeDonneesBBS.Create(Base_Banque, 'VMCENTR');
  With _tableVMCENTR.Donnees Do Begin
    FieldDefs.Add('CODE_CLASSE', ftString, 7, True);
    FieldDefs.Add('CODE_FABRICANT', ftString, 3, True);
    FieldDefs.Add('CODE_GAMME', ftString, 2, True);
    FieldDefs.Add('CODE_PRODUIT', ftString, 15, True);
    IndexDefs.Add('CLE_VMCENTR', 'CODE_CLASSE;CODE_FABRICANT;CODE_GAMME;CODE_PRODUIT', [ixPrimary, ixUnique]);
  End;
_tableVMCENTR.Initialisation;

_tableVMCEXTR := TTableDeDonneesBBS.Create(Base_Banque, 'VMCEXTR');
  With _tableVMCEXTR.Donnees Do Begin
    FieldDefs.Add('CODE_CLASSE', ftString, 7, True);
    FieldDefs.Add('CODE_FABRICANT', ftString, 3, True);
    FieldDefs.Add('CODE_GAMME', ftString, 2, True);
    FieldDefs.Add('CODE_PRODUIT', ftString, 15, True);
    IndexDefs.Add('CLE_VMCEXTR', 'CODE_CLASSE;CODE_FABRICANT;CODE_GAMME;CODE_PRODUIT', [ixPrimary, ixUnique]);
  End;
_tableVMCEXTR.Initialisation;
}
End;
{ ************************************************************************************************************************************************* }
Procedure ConsultationEdibatec;
Begin
Config.ConsultationUniquement := True;
Config.PiecesJointesLectureSeule := True;
  If EdibatecDispo Then LancementCatalogue(
                                           Config.Autorise_Banque_Personelle,
                                           Config.Autorise_Catalogue,
                                           c_Edibatec_CodesClassesPourCatalogue,  //_Classe
                                           '',              //_Fabricant_Prioritaire
                                           False,           //_Electricit�
                                                False,           //_Selection_Affichee
                                           False,           //_Selection_Gamme_Possible
                                           False,           //_Selection_Produit_Possible
                                           0,               //_Nombre_Selection_Max
                                           Nil,             //_Selection
                                           Nil,             //_Interpretation
                                           True,            //_Grandes_Icone
                                           Nil,
                                           Application.Icon, // Icone Affichee
                                           False // Pour que la classe choisie soit developee
                                          );
End;
{ ************************************************************************************************************************************************* }
Function ChoisirProduit(_CodeEdibatec: String; _ToutesClasses: Boolean): String;
Var
  m_Retour     : TCatSelection;
  m_CodeClasse : String;
Begin
Try
  Result := '';
  If EdibatecDispo Then Begin
    m_Retour       := TCatSelection.Create;
    m_Retour.Codes := _CodeEdibatec;

  
    Connection_Catalogue(Etude.Fichier);

    If _ToutesClasses Then m_CodeClasse := c_Edibatec_CodesClassesPourCatalogue
                      Else m_CodeClasse := Copy(_CodeEdibatec, 1, 7);
    m_Retour := LancementCatalogue(

                                           Config.Autorise_Banque_Personelle,
                                           Config.Autorise_Catalogue,
                                           m_CodeClasse,//c_Edibatec_CodesClassesPourCatalogue,
                                           '',              //_Fabricant_Prioritaire
                                           False,           //_Electricit�
                                           true,           //_Selection_Affichee
                                           False,           //_Selection_Gamme_Possible
                                           true,           //_Selection_Produit_Possible
                                           1,               //_Nombre_Selection_Max
                                           m_Retour,             //_Selection
                                           Nil,             //_Interpretation
                                           True,            //_Grandes_Icone
                                           Nil,
                                           Application.Icon, // Icone Affichee
                                           False, // Pour que la classe choisie soit developee

nil,//_Aide: TSpeedButton = Nil;
false,//_ConsultDico: Boolean = False;
false,//_TriClasse: Boolean = False;
false,//_Selection_ClimaWin: Boolean = False;
false//_Sortie_Esc: Boolean = False)
{
                                 Config.Autorise_Banque_Personelle,
                                 True,
                                 m_CodeClasse, // _Classe
                                 '',           //_Fabricant_Prioritaire
                                 False,        //_Electricit�
                                 True,         //_Selection_Affichee
                                 False,        //_Selection_Gamme_Possible
                                 True,         //_Selection_Produit_Possible
                                 1,            //_Nombre_Selection_Max
                                 m_Retour,     //_Selection
                                 Nil,          //_Interpretation
                                 True,         //_Grandes_Icone
                                 Nil,          // Passage de la procedure qui va bien a vinvin
                                 Application.Icon, // Icone Affichee
                                 Not _ToutesClasses // Pour que la classe choisie soit developee
}
                                 );
    If m_Retour <> Nil Then Result := m_Retour.Codes;

  End;
Except End;
End;
{ ************************************************************************************************************************************************* }
Function ChoixGammeDansClasse(_DansClasse: String; _OldGamme: String = ''): String;
Var
  m_Code     : String;
  m_Retour   : TCatalObjectList;
Begin
  m_Code      := _OldGamme;
  If EdibatecDispo Then Begin
    If _OldGamme <> '' Then m_Retour := ChoixGamme('S�lection d''une gamme', 'Veuillez s�lectionner une gamme de ' + NomClasse(_DansClasse), _OldGamme)
                       Else m_Retour := ChoixGamme('S�lection d''une gamme', 'Veuillez s�lectionner une gamme de ' + NomClasse(_DansClasse), _DansClasse);
    If m_Retour <> Nil Then m_Code := m_Retour.Code;
    FreeAndNil(m_Retour);
  End;
  Result := m_Code;
End;
{ ************************************************************************************************************************************************* }
End.
