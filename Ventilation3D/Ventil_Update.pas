unit Ventil_Update;

{$Include 'Ventil_Directives.pas'}

interface

uses
  Classes,
  XMLIntF,
  CAD_Containers_Base;

  type

  TFichierDate = class(TStringList)
  private
    Fichier : String;
  end;

  TFichiersDates = class(TTypedListAutoCreateDestroy<TFichierDate>);

  TVentilSearchUpdate = class
  private
    Function GetStringListFromServer(_RelativePath : String) : TStringList;
    Function GetConfigXML(_ConfigVersion : String) : IXMLDocument;
    Function GetUpdateDatePackage(_PackageName : String; _FichierDate : String; _FichiersDates : TFichiersDates) : TDate;
    Function CompareElementsPackage(_FichierReference : String; _FichierDate : String; _FichierPackage : String; _FichiersDates : TFichiersDates) : Boolean;
    function GetInternalAttribute(_InternalNode : IXMLNode; _Attribut : String) : String;
    function GetInternalNode(_InternalNode : IXMLNode; _Noeud : String) : IXMLNode;
    function GetSousNode(_Node : IXMLNode; _Noeuds : array of String) : IXMLNode;
    function GetNodeValue(_Node : IXMLNode; _Noeuds : array of String) : String;
    function GetFichierReference(_Node : IXMLNode) : String;
    function GetFichierDate(_Node : IXMLNode) : String;
    function GetFichierPackage(_Node : IXMLNode) : String;
    Function TestPackage(_PackageNode : IXMLNode; _InheritsNodeList : IXMLNodeList; _FichiersDates : TFichiersDates) : Boolean;
    Function ParcoursPackagesUpdate(_XMLDoc : IXMLDocument; _FichiersDates : TFichiersDates) : Boolean;
    Function RechercheMaj(_ForceKillApp : Boolean = False) : Boolean; //renvoie True si une mise � jour est pr�sente, sinon renvoie False
  end;

  Procedure RechercheMaj(_ForceKillApp : Boolean = False);
  Function IsConnected:Boolean;
  Procedure LancerUpdate;

implementation
Uses
  Forms,
  Windows,
  SysUtils,
  XMLDoc,
  Ventil_Commun,
  clDownLoader,
  BBS_Progress,
  CAD_ViloUtils,
  Variants,
  BbsgTec;

{ **************************************************************************** }
Procedure RechercheMaj(_ForceKillApp : Boolean = False);
Var
  SearchUpdate : TVentilSearchUpdate;
  HasUpdate : Boolean;
begin
    if DelphiRuning then
      exit;
  SearchUpdate := TVentilSearchUpdate.Create;
  HasUpdate := SearchUpdate.RechercheMaj(_ForceKillApp);
  SearchUpdate.Destroy;
    if HasUpdate and _ForceKillApp then
      Application.Terminate;
end;
{ **************************************************************************** }
Function IsConnected:Boolean;
Begin
//Result := DetectionConnection;
Result := IsOnline;
End;
{ **************************************************************************** }
Procedure LancerUpdate;
var
  Proch : THandle;
  PId   : Cardinal;
Begin

  BBSupdater_LaunchMAJ(False, False, True);

  PId    := GetProcessId('BBSUpdater.exe');
  Proch  := OpenProcess(PROCESS_ALL_ACCESS ,true,PId); //handle du process

  SetWindowPos(Proch, HWND_TOPMOST, 0, 0, 0, 0,
  SWP_NOMOVE or SWP_NOSIZE );
  Closehandle(Proch);
end;
{ **************************************************************************** }

{ **************************************************************************** }
{ **********************    TVentilSearchUpdate     ************************** }
{ **************************************************************************** }
Function TVentilSearchUpdate.GetStringListFromServer(_RelativePath : String) : TStringList;
Var
  FclDOWN  : TclDownLoader;
begin
  Result := TStringList.Create;

    try
      FclDOWN := TclDownLoader.Create(nil);
      FclDOWN.URL := 'https://www.bbs-logiciels.com/Updater/' + _RelativePath;
      FclDOWN.UserName := 'BBSUpdater';
      FclDOWN.Password := '35381027-4DAE-4820-B8F9-B3AD4830F1B2';
      FclDOWN.TryCount := 1;
      FclDOWN.ThreadCount := 1;
      FclDOWN.TimeOut := 5000;
      FclDOWN.DataStream := TMemoryStream.Create;
      FclDOWN.Start(false);

      FclDOWN.DataStream.Seek( 0 , soBeginning);

        if FclDOWN.DataStream.Size > 0 then
          Result.LoadFromStream( FclDOWN.DataStream );

      FclDOWN.Free;
    except
    end;
end;
{ **************************************************************************** }
Function TVentilSearchUpdate.GetConfigXML(_ConfigVersion : String) : IXMLDocument;
Var
        tempMemorySteam : TMemoryStream;
        mstl     : tstringlist;
        LastVersion : String;
Begin
  mstl := GetStringListFromServer('Config_' + _ConfigVersion + '.xml');
    if mstl.Count > 0 then
      Result := TXMLDocument.Create(Nil)
    else
      Begin
      Result := nil;
      Exit;
      End;

  Result.LoadFromXML(mstl.Text);
  Result.Active := True;
  FreeAndNil(mstl);

  LastVersion := GetNodeValue(Result.DocumentElement, ['inherits', 'BBSUpdater', 'Serveur', 'Version']);

  if Trim(LastVersion) = '' then
    Exit;
  if LastVersion > _ConfigVersion then
    begin
    Result := Nil;
    Result := GetConfigXML(LastVersion);
    end;
End;
{ **************************************************************************** }
Function TVentilSearchUpdate.GetUpdateDatePackage(_PackageName : String; _FichierDate : String; _FichiersDates : TFichiersDates) : TDate;
Var
  cpt : Integer;
  tempFichierDate : TFichierDate;
  tempContenuFichierDate : TStringList;
  mDateStr : String;
begin
  Result := -1;
  tempFichierDate := Nil;

    //on v�rifie si on a d�j� t�l�charg� le fichier date en question (�vite de multiples t�l�chargements inutiles)
    for cpt := 0 to _FichiersDates.Count - 1 do
        if _FichiersDates[cpt].Fichier = _FichierDate then
          begin
          tempFichierDate := _FichiersDates[cpt];
          Break;
          end;

    //Si aucune correspondance dans la liste, on ajoute le fichier date en r�cup�rant son contenu sur le serveur
    if tempFichierDate = Nil then
      begin
      tempFichierDate := TFichierDate.Create;
      tempFichierDate.Fichier := _FichierDate;
      tempContenuFichierDate := GetStringListFromServer(_FichierDate);
      tempFichierDate.AddStrings(tempContenuFichierDate);
      FreeAndNil(tempContenuFichierDate);
      _FichiersDates.Add(tempFichierDate);
      end;

  //on r�cup�re la date de mise � jour du package
  FormatSettings.DateSeparator:='/';
  _PackageName := UpperCase(_PackageName);
  for cpt := 0 to tempFichierDate.Count - 1 do
      if (Trim(tempFichierDate[cpt]) <> '') then
        if UpperCase(GetSubStr(tempFichierDate[cpt], 1, '@')) = _PackageName then
          begin
            try // Pour g�rer le pb de lecture d'un fichier dates.txt avec une date incorrecte.
            mDateStr := GetSubStr(tempFichierDate[cpt], 2, '@');
            Result := StrToDate(mDateStr);
            Exit;
            Except
            end;
          end;


end;
{ **************************************************************************** }
Function TVentilSearchUpdate.CompareElementsPackage(_FichierReference : String; _FichierDate : String; _FichierPackage : String; _FichiersDates : TFichiersDates) : Boolean;
var
  PackageName : String;
  DatePackage : TDate;
begin
  Result := False;
    if (_FichierReference = '') or (_FichierDate = '') or (_FichierPackage = '') or (Pos('BBSUpdater', _FichierReference) > 0) then
      Exit;

  //on formatte la cha�ne comme �a nous arrange pour la suite des traitements
  _FichierReference := StrReplace(_FichierReference, '/', '\', True, True);
    if (pos('\', _FichierReference) = 1) then
      _FichierReference := StrReplace(_FichierReference, '\', '', True, False);

  //Si le fichier r�f�rence n'est pas pr�sent, c'est qu'il y'a un nouveau package de disponible (ou que l'utilisateur l'a supprim�)
  if Not(Ventil_FichierSurDisque(_FichierReference)) then
    begin
    Result := True;
    Exit;
    end;

  PackageName := StrReplace(_FichierPackage, '/', '\', True, True);
  PackageName := ExtractFileName(PackageName);
  DatePackage := GetUpdateDatePackage(PackageName, _FichierDate, _FichiersDates);

    //Si pas de date de package, on ne peut pas faire grand chose...
    if DatePackage = -1 then
      Exit;

  Result := (Trunc(DatePackage) > Trunc(FileDateToDateTime(FileAge(_FichierReference))));
end;
{ **************************************************************************** }
function TVentilSearchUpdate.GetInternalAttribute(_InternalNode : IXMLNode; _Attribut : String) : String;
Var
  cptInternal : Integer;
begin
  Result := '';
  _Attribut := UpperCase(_Attribut);
    for cptInternal := 0 to _InternalNode.AttributeNodes.Count - 1 do
        if UpperCase(_InternalNode.AttributeNodes.Get(cptInternal).NodeName) = _Attribut then
          begin
          Result := _InternalNode.AttributeNodes.Get(cptInternal).NodeValue;
          Exit;
          end;
end;
{ **************************************************************************** }
function TVentilSearchUpdate.GetInternalNode(_InternalNode : IXMLNode; _Noeud : String) : IXMLNode;
Var
  cptInternal : Integer;
begin
  Result := Nil;
  _Noeud := UpperCase(_Noeud);
    for cptInternal := 0 to _InternalNode.ChildNodes.Count - 1 do
        if UpperCase(_InternalNode.ChildNodes.Get(cptInternal).NodeName) = _Noeud then
          begin
          Result := _InternalNode.ChildNodes.Get(cptInternal);
          Exit;
          end;
end;
{ **************************************************************************** }
function TVentilSearchUpdate.GetSousNode(_Node : IXMLNode; _Noeuds : array of String) : IXMLNode;
var
  cpt : Integer;
begin
  Result := _Node;
    for cpt := Low(_Noeuds) to High(_Noeuds) do
      begin
      Result := GetInternalNode(Result, _Noeuds[cpt]);
        if Result = Nil then
          Exit;
      end;

end;
{ **************************************************************************** }
function TVentilSearchUpdate.GetNodeValue(_Node : IXMLNode; _Noeuds : array of String) : String;
var
  tempNode : IXMLNode;
begin
  Result := '';
  tempNode := GetSousNode(_Node, _Noeuds);
    if tempNode <> Nil then
      Result := tempNode.NodeValue;

end;
{ **************************************************************************** }
function TVentilSearchUpdate.GetFichierReference(_Node : IXMLNode) : String;
begin
  Result := GetNodeValue(_Node, ['Local', 'Fichier']);
end;
{ **************************************************************************** }
function TVentilSearchUpdate.GetFichierDate(_Node : IXMLNode) : String;
begin
  Result := GetNodeValue(_Node, ['Serveur', 'Date', 'Fichier']);
end;
{ **************************************************************************** }
function TVentilSearchUpdate.GetFichierPackage(_Node : IXMLNode) : String;
begin
  Result := GetNodeValue(_Node, ['Serveur', 'Fichier']);
end;
{ **************************************************************************** }
Function TVentilSearchUpdate.TestPackage(_PackageNode : IXMLNode; _InheritsNodeList : IXMLNodeList; _FichiersDates : TFichiersDates) : Boolean;
var
  FichierReference : String;
  FichierDate : String;
  FichierPackage : String;
  NomBaliseInherit : String;

begin
  Result := False;
  NomBaliseInherit := '';
  FichierReference := '';
  FichierPackage := '';
  FichierDate := '';

  NomBaliseInherit := GetInternalAttribute(_PackageNode, 'inherits');

  FichierReference := GetFichierReference(_PackageNode);
  FichierDate := GetFichierDate(_PackageNode);
  FichierPackage := GetFichierPackage(_PackageNode);

    if (NomBaliseInherit <> '') then
      begin
        if (FichierReference = '') then
          FichierReference := GetFichierReference(_InheritsNodeList[NomBaliseInherit]);
        if (FichierDate = '') then
          FichierDate := GetFichierDate(_InheritsNodeList[NomBaliseInherit]);
        if (FichierPackage = '') then
          FichierPackage := GetFichierPackage(_InheritsNodeList[NomBaliseInherit]);
      end;

  Result := CompareElementsPackage(FichierReference, FichierDate, FichierPackage, _FichiersDates);
end;
{ **************************************************************************** }
Function TVentilSearchUpdate.ParcoursPackagesUpdate(_XMLDoc : IXMLDocument; _FichiersDates : TFichiersDates) : Boolean;
Var
  NomApplication : String;
  cpt : Integer;
  _PackagesNodeList : IXMLNodeList;
  _InheritsNodeList : IXMLNodeList;
  _tmpNode          : IXMLNode;
begin
  Result := False;
  NomApplication := ExtractFileName(Application.ExeName);
  NomApplication := changeFileExt(NomApplication,'');

  _tmpNode := GetSousNode(_XMLDoc.DocumentElement, ['Programmes', NomApplication, 'Fichiers']);
    if _tmpNode <> Nil then
      _PackagesNodeList := _tmpNode.ChildNodes
    else
      Exit;
  _tmpNode := GetSousNode(_XMLDoc.DocumentElement, ['inherits']);
    if _tmpNode <> Nil then
      _InheritsNodeList := _tmpNode.ChildNodes
    else
      Exit;

    for cpt := 0 to _PackagesNodeList.Count - 1 do
      begin
      Result := TestPackage(_PackagesNodeList[cpt], _InheritsNodeList, _FichiersDates);
        if Result then
          Exit;
      end;

  _PackagesNodeList := Nil;
  _InheritsNodeList := Nil;
end;
{ **************************************************************************** }
Function TVentilSearchUpdate.RechercheMaj(_ForceKillApp : Boolean = False) : Boolean;
Var
  VersionUpdater : String;
  TempConfigFile : IXMLDocument;
  FichiersDates : TFichiersDates;
Begin

  Result := False;
  try
  FichiersDates := TFichiersDates.Create;
  FBeginProgress(100, 'Recherche des mises � jour', true, '', true);
    if not IsConnected then
      exit;

  VersionUpdater := ApplicationVersion('BBSUpdater.exe');
  TempConfigFile := GetConfigXML(VersionUpdater);

    if TempConfigFile = Nil then
      Exit;



  Result := ParcoursPackagesUpdate(TempConfigFile, FichiersDates);
  TempConfigFile := Nil;


  if Result then
    begin
      if _ForceKillApp then
        begin
        Application.MessageBox(PChar('Une mise � jour est disponible, veuillez la t�l�charger.'), '', MB_OK + MB_DEFBUTTON2);
        LancerUpdate;
        end
      else
      if Application.MessageBox(PChar('Une mise � jour est disponible, souhaitez-vous la t�l�charger?'), '', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IdYes then
        LancerUpdate;
    end;

    finally
      FichiersDates.Destroy;
      FEndProgress();
    end;
End;
{ **************************************************************************** }
end.
