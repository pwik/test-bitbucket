Unit Ventil_SaisieMemo;
          
{$Include 'Ventil_Directives.pas'}

Interface

Uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
     Buttons, ExtCtrls,

     Ventil_Types;

Type
  TFic_SaisieMemo = class(TForm)
    Bevel1: TBevel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Memo: TMemo;
  Private
    { Private declarations }
  Public
    { Public declarations }
  End;

  Function InputQueryMemo(_Caption: String; Var _Text: TStringList): Boolean;

Var
  Fic_SaisieMemo: TFic_SaisieMemo;

Implementation

{$R *.dfm}

{ ************************************************************************************************************************************************** }

Function InputQueryMemo(_Caption: String; Var _Text: TStringList): Boolean;
Var
  NoLigne : SmallInt;
Begin
  If Fic_SaisieMemo = Nil Then Application.CreateForm(TFic_SaisieMemo, Fic_SaisieMemo);
  Fic_SaisieMemo.Memo.Clear;
        for NoLigne := 0 to _Text.Count - 1 do
                Fic_SaisieMemo.Memo.Lines.Add(_Text[NoLigne]);
  Fic_SaisieMemo.Caption   := _Caption;
  If Fic_SaisieMemo.ShowModal = Id_Ok Then
  Begin
        for NoLigne := 0 to Fic_SaisieMemo.Memo.Lines.Count - 1 do
                _Text.Add(Fic_SaisieMemo.Memo.Lines[NoLigne]);
    _Text.Assign(Fic_SaisieMemo.Memo.Lines);
    Result := True;
  End Else Result := False;
End;
{ ************************************************************************************************************************************************** }


{ ************************************************************************************************************************************************** }

End.
