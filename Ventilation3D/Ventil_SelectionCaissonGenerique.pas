Unit Ventil_SelectionCaissonGenerique;

{$Include 'Ventil_Directives.pas'}

Interface

Uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, Dialogs, Buttons, ExtCtrls, Grids, BaseGrid, AdvGrid, ComCtrls, AdvEdit,
     Math,
     Ventil_Caisson, Ventil_Bezier, Ventil_EdibatecProduits,
     DBClient, AdvObj;

Const
  Cst_PanelCriteres    = 1;
  Cst_PanelCaisson     = 2;
  Cst_PanelAccessoires = 3;
  Cst_PanelAnnexe      = 4;


Type
{ ************************************************************************************************************************************************** }
  TDlg_SelectionCaissonGenerique = class(TForm)
    PanelBas: TPanel;
    Btn_Cancel: TBitBtn;
    Btn_Ok: TBitBtn;
    Panel_Selection: TPanel;
    Panel3: TPanel;
    GroupBox6: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label_DebitMini: TLabel;
    Label_DebitMaxi: TLabel;
    Grid_Selection: TAdvStringGrid;
    Bevel1: TBevel;
    Label8: TLabel;
    Label_DpMax: TLabel;
    Label_DpMin: TLabel;
    Label9: TLabel;
    procedure Grid_SelectionSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure Grid_SelectionRowChanging(Sender: TObject; OldRow, NewRow: Integer; var Allow: Boolean);
    procedure Grid_InfosCaissonCanEditCell(Sender: TObject; ARow, ACol: Integer; var CanEdit: Boolean);
    procedure Radio_ColliersClick(Sender: TObject);
    procedure Grid_ColliersSuppIsFixedCell(Sender: TObject; ARow, ACol: Integer; var IsFixed: Boolean);
    procedure Grid_BavettesIsFixedCell(Sender: TObject; ARow, ACol: Integer; var IsFixed: Boolean);
    procedure CheckBox_UniverselClick(Sender: TObject);
    procedure CheckBox_125100Click(Sender: TObject);
    procedure CheckBox_BandePerfClick(Sender: TObject);
    procedure CheckBox_NonAgreeClick(Sender: TObject);
    procedure Grid_SelectionCanEditCell(Sender: TObject; ARow,
      ACol: Integer; var CanEdit: Boolean);
  Private
    ListeCaissons    : TStringList;
    CaissonSelection : TEdibatec_Caisson;
    CaissonReseau    : TVentil_Caisson;
    FPanel_Actif     : TPanel;
    NonAgreePossible : Boolean;
    CanContinueAfterSelection : Boolean;
    Procedure InitFiche;
    Procedure Enable_NonAgree;
    Procedure EnableControls;
    Procedure GoToSelection;
    Procedure AffecteAssociations;
    Procedure GoToPreselection;
    Procedure GenereListeCaissons;
    Procedure AffListeCaissons;
    function  GetPanelActif: TPanel;
    procedure SetPanelActif(const Value: TPanel);
    Function  CaissonValide(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_Options(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_Fonctionnement(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_Selection3Courbes(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_SelectionPlageFonctionnement(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_SelectionDebitConstantOld(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_SelectionDebitConstant(_Caisson: TEdibatec_Caisson) : Boolean;    
    Function  Caisson_Valide_SelectionPlageFonctionnementEtCourbeLimite(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_Options_TypeCaisson(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_Options_Entrainement(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_Options_Alimentation(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_Options_TypeMoteur(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_Options_Depressostat(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_Options_Agrementation(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_PositionsSorties(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_DebitMiniMaxi(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_DPMiniMaxi(_Caisson: TEdibatec_Caisson) : Boolean;    

    Procedure AffDebitsCaisson(_Caisson: TVentil_Caisson);
    Function IsProduitASSOPAPV(m_ProduitAsso: TEdibatec_Association) : Boolean;
    Function IsCaissonSelectionFromGammeHUCF : boolean;
    Function IsCaissonSelectionFromGammeJBEBorJBHBorJBEBECO(_Caisson: TEdibatec_Caisson) : boolean;
  Public
    Property Panel_Actif : TPanel Read GetPanelActif write SetPanelActif;
  End;


{ ************************************************************************************************************************************************** }
  TSelectionCaisson_SaveOptions = Class
    CanGet                              : Boolean;
    OldReferenceCaisson                 : String;
    //Crit�res De S�lection Du Ventilateur
    CheckBox_Caisson_Checked            : Boolean;
    CheckBox_Tourelle_Checked           : Boolean;
    CheckBox_Direct_Checked             : Boolean;
    CheckBox_Poulie_Checked             : Boolean;
    CheckBox_UneVitesse_Checked         : Boolean;
    CheckBoxDeuxVitesses_Checked        : Boolean;
    CheckBox_NonAgree_Checked           : Boolean;
    CheckBox_40012_Checked              : Boolean;
    CheckBox_MonoPhase_Checked          : Boolean;
    CheckBox_TriPhase_Checked           : Boolean;
//    Radio_Depressostat_ItemIndex        : Integer;
//    Radio_VentilEco_ItemIndex           : Integer;
//    Radio_EcoEtDepr_ItemIndex           : Integer;
    CheckBoxEco_Checked                 : Boolean;
    CheckBoxNEcoADepr_Checked           : Boolean;
    CheckBoxNEcoSDepr_Checked           : Boolean;

    CheckBox_DTU_Checked                : Boolean;
    RadioBezier_ItemIndex               : integer;
    //Accessoire Associ�s Au Ventilateur
    ListAccessoireVentilo_Qte           : Array of Integer;
    //Accessoires Annexes
    //-->Montage
    Radio_Adhesif_ItemIndex             : integer;
    Edit_Vis13_Text                     : String;
    Edit_Vis19_Text                     : String;
    Edit_M0_Text                        : String;
    Edit_M1_Text                        : String;
    Check_IsolantMur_Checked            : Boolean;
    Check_Fourreaux_Checked             : Boolean;
    //-->Bavettes et colliers
    Radio_Colliers_ItemIndex            : integer;
    Bavette_OuiNon                      : Boolean;
    Bavette_125_Qte                     : Integer;
    Bavette_160_Qte                     : Integer;
    Bavette_200_Qte                     : Integer;
    Bavette_250_Qte                     : Integer;
    Bavette_315_Qte                     : Integer;
    Bavette_355_Qte                     : Integer;
    Bavette_400_Qte                     : Integer;
    Bavette_450_Qte                     : Integer;
    Bavette_500_Qte                     : Integer;
    Bavette_560_Qte                     : Integer;
    Bavette_630_Qte                     : Integer;
    Bavette_710_Qte                     : Integer;
    //-->Supports Universels
    CheckBox_Universel_Checked          : Boolean;
    CheckBox_125300_Checked             : Boolean;
    CheckBox_160300_Checked             : Boolean;
    CheckBox_125100_Checked             : Boolean;
    CheckBox_160100_Checked             : Boolean;
    CheckBox_ColliersSuppIsoles_Checked : Boolean;
    Edit_NbSupp160300_Text              : String;
    Edit_NbSupp125300_Text              : String;
    Edit_NbSupp160100_Text              : String;
    Edit_NbSupp125100_Text              : String;
    Edit_NbSuppUni_Text                 : String;
    CheckBox_BandePerf_Checked          : Boolean;
    Edit_NbRlxBandePerforee_Text        : String;
    Collier_125_Qte                     : Integer;
    Collier_160_Qte                     : Integer;
    Collier_200_Qte                     : Integer;
    Collier_250_Qte                     : Integer;
    Collier_315_Qte                     : Integer;
    Collier_355_Qte                     : Integer;
    Collier_400_Qte                     : Integer;
    Collier_450_Qte                     : Integer;
    Collier_500_Qte                     : Integer;
    Collier_560_Qte                     : Integer;
    Collier_630_Qte                     : Integer;
    Collier_710_Qte                     : Integer;
    Collier_800_Qte                     : Integer;
    Collier_900_Qte                     : Integer;
    Private
    Public
        Procedure Sauvegarder(_Donnees : TClientDataSet);
        Procedure Ouvrir(_Donnees : TClientDataSet);        
        Constructor Create;
  End;

  Function CalculVitessePoint(_Point: TPointDebitPression; _Caisson: TEdibatec_Caisson; _Mini: Boolean ): Real;

Var
  Dlg_SelectionCaissonGenerique: TDlg_SelectionCaissonGenerique;


Function SelectionDuCaissonGenerique(Const _Caisson: TVentil_Caisson; _CaissonEdibatec: TEdibatec_Caisson): TEdibatec_Caisson;

implementation
Uses
  Ventil_Edibatec, Ventil_Bouche, Ventil_Const, Ventil_Declarations, Ventil_Commun, Ventil_Troncon, Ventil_TronconDessin, Ventil_SortieToiture, Ventil_Utils,
  Ventil_TeSouche,
  BBScad_Interface_Aeraulique,
  BBS_Message;
{$R *.dfm}


{ **************************************************************************************************************************************************** }
Function CalculDpCourbe(Const _Point: TPointDebitPression; Const _SurCourbe: TPointsControleBezier): Real;
Var
  m_P1Courbe,
  m_P2Courbe: TPointDebitPression;
  a, b      : Real;
Begin
  EncadrePoint(_SurCourbe, _Point, m_P1Courbe, m_P2Courbe);
  a := (m_P1Courbe.DP - m_P2Courbe.DP) / (m_P1Courbe.Q - m_P2Courbe.Q);
  b := m_P1Courbe.DP - a * m_P1Courbe.Q;
  Result := a * _Point.Q + b;
  If Result < 1 Then Result := 0;
End;
{ **************************************************************************************************************************************************** }
Function CalculQCourbe(Const _Point: TPointDebitPression; Const _SurCourbe: TPointsControleBezier): Real;
Var
  m_P1Courbe,
  m_P2Courbe: TPointDebitPression;
  a, b      : Real;
Begin
  EncadrePoint(_SurCourbe, _Point, m_P1Courbe, m_P2Courbe);
  a := (m_P1Courbe.DP - m_P2Courbe.DP) / (m_P1Courbe.Q - m_P2Courbe.Q);
  b := m_P1Courbe.DP - a * m_P1Courbe.Q;
  Result := (_Point.DP - b) / a;
  If Result < 1 Then Result := 0;
End;
{ **************************************************************************************************************************************************** }
Function CalculVitessePoint(_Point: TPointDebitPression; _Caisson: TEdibatec_Caisson; _Mini: Boolean): Real;
Var
  m_P1Courbe,
  m_P2Courbe: TPointDebitPression;
  a, b, c   : Real;
  Delta     : Real; // Pour l'equation cQ� - aQ - b = 0;
  R1, R2    : Real;
  QRes      : Real;
  Courbe    : String;
Begin

    If _Mini Then EncadrePoint(StringToPointsBezier_11Points(_Caisson.ListeDebitsMinis, _Caisson.ListeDPMinis), _Point, m_P1Courbe, m_P2Courbe)
             Else EncadrePoint(StringToPointsBezier_11Points(_Caisson.ListeDebitsMaxis, _Caisson.ListeDPMaxis), _Point, m_P1Courbe, m_P2Courbe);

  
  a := (m_P1Courbe.DP - m_P2Courbe.DP) / (m_P1Courbe.Q - m_P2Courbe.Q);
  b := m_P1Courbe.DP - a * m_P1Courbe.Q;
  c := _Point.DP / (Sqr(_Point.Q));
  Delta := Sqr(a) + (4 * b * c);
  R1 := (a - Sqrt(Delta)) / (2 * c);
  R2 := (a + Sqrt(Delta)) / (2 * c);
  QRes := Max(R1, R2);
  If _Mini Then Result := _Caisson.VitesseMini * ( _Point.Q / QRes)
           Else Result := _Caisson.VitesseMaxi * ( _Point.Q / QRes );
End;
{ **************************************************************************************************************************************************** }
Function SelectionDuCaissonGenerique(Const _Caisson: TVentil_Caisson; _CaissonEdibatec: TEdibatec_Caisson): TEdibatec_Caisson;
Var
  m_No: Integer;
Begin

InterfaceCAD.Aero_Vilo2Fredo_HighlightObjects(nil);

        if Etude <> nil then
                Etude.AutoSaveAutorisee := False;
  Etude.Batiment.Reseaux.CalculerQuantitatif;
  Dlg_SelectionCaissonGenerique := TDlg_SelectionCaissonGenerique.Create(Application);
  Dlg_SelectionCaissonGenerique.CaissonSelection := _CaissonEdibatec;
  Dlg_SelectionCaissonGenerique.CaissonReseau    := _Caisson;
  Dlg_SelectionCaissonGenerique.InitFiche;
//  Etude.SaveOptions_SelectionCaisson.InitOptionsFiche(Dlg_SelectionCaissonGenerique);
  If Dlg_SelectionCaissonGenerique.ShowModal = idOk Then Begin
    Result := Dlg_SelectionCaissonGenerique.CaissonSelection;
    {  Association du produit caisson et ses accessoires}
    _Caisson.ProduitAssocie := Dlg_SelectionCaissonGenerique.CaissonSelection;
//  _Caisson.Associations.InitFrom(_Caisson.ProduitAssocie.Associations);
//    Dlg_SelectionCaisson.Grid_Accessoires.SortByColumn(0);

    For m_No := 0 To _Caisson.FilsCalcul.Count - 1 do
        if _Caisson.FilsCalcul[m_No].InheritsFrom(TVentil_SortieToit) then
                TVentil_SortieToit(_Caisson.FilsCalcul[m_No]).MiseAJourDiametre;

    For m_No := 0 To _Caisson.Fils.Count - 1 do
        Begin
                if TVentil_TronconDessin_SortieCaisson(_Caisson.Fils[m_No]).TypeSortie = c_CaissonSortie_Refoulement then
                        Begin
                        TVentil_TronconDessin_SortieCaisson(_Caisson.Fils[m_No]).Diametre := Result.DiametreRejet;
                        End
                else
                        Begin
                        TVentil_TronconDessin_SortieCaisson(_Caisson.Fils[m_No]).Diametre := Result.DiametreAspiration;
                        End;
        TVentil_TronconDessin_SortieCaisson(_Caisson.Fils[m_No]).LienCad.IAero_Vilo_Base_SetDiametre(TVentil_TronconDessin_SortieCaisson(_Caisson.Fils[m_No]).Diametre);
//        Aero_Vilo2Fredo_UpdateChangementsDeDiametre(Nil);
        End;

    _Caisson.Associations.InitFrom(_Caisson.ProduitAssocie.Associations);

//    Etude.SaveOptions_SelectionCaisson.SaveOptionsFiche(Dlg_SelectionCaissonGenerique);

//modif dtu
                Etude.DeltaCalculeMax := abs(_Caisson.DPVentilateur.MinAQmax - 20 - _Caisson.ProduitAssocie.DpCalcQMax);
                Etude.DeltaCalculeMin := abs(_Caisson.DPVentilateur.MinAQmin - 20 - _Caisson.ProduitAssocie.DpCalcQMin);
{
        if not Dlg_SelectionCaisson.CheckBox_DTU.Checked then
                Begin
                Etude.DeltaCalculeMax := abs(_Caisson.DPVentilateur.MinAQmax - 20 - _Caisson.ProduitAssocie.DpCalcQMax);
                Etude.DeltaCalculeMin := abs(_Caisson.DPVentilateur.MinAQmin - 20 - _Caisson.ProduitAssocie.DpCalcQMin);
                End
        Else
                Begin
                Etude.DeltaCalculeMax := abs(_Caisson.DPVentilateur.MinAQmax - _Caisson.ProduitAssocie.DpCalcQMax);
                Etude.DeltaCalculeMin := abs(_Caisson.DPVentilateur.MinAQmin - _Caisson.ProduitAssocie.DpCalcQMin);
                End;
}
    if Etude.SelectedObject <> nil then
        Etude.SelectedObject.AfficheResultats(Etude.GridRes);
  End Else Begin
    _Caisson.ProduitAssocie := Nil;
    Result := nil;
  End;
  FreeAndNil(Dlg_SelectionCaissonGenerique);
        if Etude <> nil then
                Begin
                Etude.Calculer;
                Etude.AutoSaveAutorisee := True;
                End;
End;
{ **************************************************************************************************************************************************** }

{ ************************************************************************ TDlg_SelectionCaisson ****************************************************** }
Procedure TDlg_SelectionCaissonGenerique.EnableControls;
Begin                       
  Btn_Ok.Enabled        := True;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaissonGenerique.InitFiche;
Var
  m_NoComp : Integer;
Begin
  Enable_NonAgree;
  Height   := 395;
  Width    := Panel_Selection.Width + 5;
  Position := poScreenCenter;
  For m_NoComp := 0 To ComponentCount - 1 Do If Components[m_NoComp].InheritsFrom(TPanel) Then Begin
    If (TPanel(Components[m_NoComp]).Tag In [Cst_PanelCriteres..Cst_PanelAnnexe]) And (TPanel(Components[m_NoComp]) <> FPanel_Actif) Then
      TPanel(Components[m_NoComp]).Left := 0;
      TPanel(Components[m_NoComp]).Top  := 0;
  End;

  Panel_Actif := Panel_Selection;
  AutoSize := True;
  EnableControls;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaissonGenerique.Enable_NonAgree;
Var
  m_No : Integer;
Begin
  NonAgreePossible := True;
  For m_No := 0 To Etude.Batiment.Composants.Count - 1 Do
    If (Etude.Batiment.Composants[m_No].Caisson = CaissonReseau) And (Etude.Batiment.Composants[m_No] Is TVentil_Bouche) Then
      NonAgreePossible := NonAgreePossible And (TVentil_Bouche(Etude.Batiment.Composants[m_No]).Has_PareFlamme = c_Oui);
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaissonGenerique.GoToSelection;
var
        CanContinue  : Boolean;
        MsgRegistres : String;
        m_NoRegistre : Integer;

        ListTronconsRegistresCad : TInterfaceList;
Begin


CanContinue := False;
{
  if not(CheckBox_DTU.Checked) then
        Grid_Selection.HideColumn(4);
}
//  If Panel_Actif = Panel_Preselection Then
  Begin

    CanContinue := False;
    GenereListeCaissons;
    If Etude.Batiment.Reseaux.HasDiametreHorsVitesse then
        BBS_ShowMessage('Certains tron�ons sont en dehors des vitesses limites.', mtError, [mbOK], 0)
    Else
    If (Etude.Batiment.Reseaux.ListeBesoinRegistre <> nil) and (Etude.Batiment.Reseaux.ListeBesoinRegistre.Count > 0) Then
        Begin
        Hide;
        Close;
        ListTronconsRegistresCad := TInterfaceList.Create;
        MsgRegistres := 'Certains tron�ons n�cessitent la pause de registre aupr�s des objets s�lectionn�s :';
                For m_NoRegistre := 0 to Etude.Batiment.Reseaux.ListeBesoinRegistre.Count - 1 Do
                        Begin
                        MsgRegistres := MsgRegistres +#13 + '- ' + Etude.Batiment.Reseaux.ListeBesoinRegistre[m_NoRegistre].Reference;
                                if Etude.Batiment.Reseaux.ListeBesoinRegistre[m_NoRegistre].InheritsFrom(TVentil_TeSouche) then
                                        MsgRegistres := MsgRegistres + ' (en amont)'
                                else
                                        MsgRegistres := MsgRegistres + ' (en aval)';
                        ListTronconsRegistresCad.Add(Etude.Batiment.Reseaux.ListeBesoinRegistre[m_NoRegistre].LienCad);
                        End;

        InterfaceCAD.Aero_Vilo2Fredo_HighlightObjects(ListTronconsRegistresCad);

        BBS_ShowMessage(MsgRegistres , mtError, [mbOK], 0);
        FreeAndNil(ListTronconsRegistresCad);
        End
    Else
    If ListeCaissons.Count > 0 Then
        Begin
        AffListeCaissons;
        CanContinue := True;
        End
    Else
        Begin
        BBS_ShowMessage('Aucun caisson ne correspond au crit�res de s�lection.', mtError, [mbOK], 0);
        End;

        Etude.DTUActive := True;
        Etude.Calculer;

  End;

  Panel_Actif := Panel_Selection;
  If CanContinue Then
        Begin
        Grid_Selection.Visible := True;
        CanContinueAfterSelection := True;
        Grid_Selection.SortByColumn(0);
        End
  else
        Begin
        AffDebitsCaisson(CaissonReseau);        
        Grid_Selection.Visible := False;
        CanContinueAfterSelection := False;
        End
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaissonGenerique.AffListeCaissons;
Var
  m_NoCaisson : Integer;
  m_Row       : Integer;
  m_Col       : Integer;
Begin
        if Etude.Batiment.TypeBat = c_BatimentTertiaire then
                Grid_Selection.ColCount := 4
        else
                Grid_Selection.ColCount := 5;

  For m_Row := 1 To Grid_Selection.RowCount - 1 Do Begin
    Grid_Selection.Objects[0, m_Row] := nil;
    For m_Col := 0 To Grid_Selection.ColCount - 1 Do Grid_Selection.Cells[m_Col, m_Row] := '';
  End;
  Grid_Selection.RowCount := 2;
  For m_NoCaisson := 0 To ListeCaissons.Count - 1 Do
  Begin
    If m_NoCaisson >= Grid_Selection.RowCount - 1 Then Grid_Selection.RowCount := Grid_Selection.RowCount + 1;
    Grid_Selection.Objects[0, m_NoCaisson + 1] := TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]);
    Grid_Selection.Cells[0, m_NoCaisson + 1] := TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).Reference;
//    Grid_Selection.Cells[1, m_NoCaisson + 1] := Float2Str(TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).Prix, 2) + ' �';
    Grid_Selection.Cells[1, m_NoCaisson + 1] := Float2Str(TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).Prix, 2);
//    Grid_Selection.Cells[2, m_NoCaisson + 1] := Float2Str(TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).DpCalcQMax, 0) + ' Pa';
    Grid_Selection.Cells[2, m_NoCaisson + 1] := Float2Str(TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).DpCalcQMax, 0);
//    Grid_Selection.Cells[2, m_NoCaisson + 1] := Float2Str(TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).DpCalcQMin, 0) + ' Pa';
    If TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).Entrainement = c_Edibatec_Caisson_Entrainement_Direct Then
      Grid_Selection.Cells[3, m_NoCaisson + 1] := TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).Position
    Else
    if TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).TypeSelection = c_CaissonSelect_DebitCst then
        Grid_Selection.Cells[3, m_NoCaisson + 1] := TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).Position
    else
        Grid_Selection.Cells[3, m_NoCaisson + 1] := TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).Position  + ' tr/min';
    Grid_Selection.Cells[4, m_NoCaisson + 1] := Float2Str(TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).CoeffRT, 2);
    If ListeCaissons.Objects[m_NoCaisson] = CaissonSelection Then Grid_Selection.Row := m_NoCaisson + 1;
  End;


  If IsNil(CaissonSelection) Then CaissonSelection := TEdibatec_Caisson(Grid_Selection.Objects[0, 1]);
  AffDebitsCaisson(CaissonReseau);
  Grid_Selection.SortByColumn(0);


  Grid_Selection.HideColumn(4);

    For m_NoCaisson := 0 To ListeCaissons.Count - 1 Do
        if TEdibatec_Caisson(Grid_Selection.Objects[0, m_NoCaisson + 1]).CodeProduit = Etude.SaveOptions_SelectionCaisson.OldReferenceCaisson then
                Begin
                Grid_Selection.Row := m_NoCaisson + 1;
                Break;
                End;
  
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaissonGenerique.GenereListeCaissons;
Var
  m_NoGamme   : Integer;
  m_NoProduit : Integer;
  m_Gamme     : TEdibatec_Gamme;
Begin
  FreeAndNil(ListeCaissons);
  ListeCaissons := TStringList.Create;
  If BaseEdibatec.Classe(c_Edibatec_Caisson).Gammes.Count > 0 Then Begin
    For m_NoGamme := 0 To BaseEdibatec.Classe(c_Edibatec_Caisson).Gammes.Count - 1 Do Begin
      m_Gamme := BaseEdibatec.Gamme(BaseEdibatec.IndexOf(c_Edibatec_Caisson), m_NoGamme);
      For m_NoProduit := 0 To m_Gamme.Produits.Count - 1 Do
      If CaissonValide(TEdibatec_Caisson(m_Gamme.Produits.Objects[m_NoProduit])) Then
        ListeCaissons.AddObject(TEdibatec_Caisson(m_Gamme.Produits.Objects[m_NoProduit]).CodeProduit, TEdibatec_Caisson(m_Gamme.Produits.Objects[m_NoProduit]));
    End;
  End;
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaissonGenerique.GetPanelActif: TPanel;
Begin
  Result := FPanel_Actif;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaissonGenerique.SetPanelActif(const Value: TPanel);
Var
  m_NoComp : Integer;
Begin
  FPanel_Actif := Value;
  FPanel_Actif.Show;
  For m_NoComp := 0 To ComponentCount - 1 Do If Components[m_NoComp].InheritsFrom(TPanel) Then Begin
    If (TPanel(Components[m_NoComp]).Tag In [Cst_PanelCriteres..Cst_PanelAnnexe]) And (TPanel(Components[m_NoComp]) <> FPanel_Actif) Then
      TPanel(Components[m_NoComp]).Hide;
  End;
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaissonGenerique.CaissonValide(_Caisson: TEdibatec_Caisson): Boolean;
Begin
  Result := Caisson_Valide_Options(_Caisson) and Caisson_Valide_Fonctionnement(_Caisson) and Caisson_Valide_DPMiniMaxi(_Caisson);
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaissonGenerique.Caisson_Valide_Options(_Caisson: TEdibatec_Caisson) : Boolean;
Begin
  Result := Caisson_Valide_Options_TypeCaisson(_Caisson);
  Result:=  Result And Caisson_Valide_Options_Entrainement(_Caisson);
  Result:=  Result And Caisson_Valide_Options_Alimentation(_Caisson);
  Result:=  Result And Caisson_Valide_Options_TypeMoteur(_Caisson);
  Result:=  Result And Caisson_Valide_Options_Depressostat(_Caisson);
  Result:=  Result And Caisson_Valide_Options_Agrementation(_Caisson);
  Result:=  Result And Caisson_Valide_DebitMiniMaxi(_Caisson);

  Result:=  Result And Caisson_Valide_PositionsSorties(_Caisson);

End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaissonGenerique.Caisson_Valide_Options_TypeCaisson(_Caisson: TEdibatec_Caisson) : Boolean;
Var
  m_Valid : Boolean;
Begin
  m_Valid := (Str2Int(CaissonReseau.Chiff_TYPE_VENTILATEUR[c_Edibatec_Caisson_Type_Caisson - 1]) = c_OUI) And (_Caisson.TypeVentilateur = c_Edibatec_Caisson_Type_Caisson);
  m_Valid := m_Valid Or
            ((Str2Int(CaissonReseau.Chiff_TYPE_VENTILATEUR[c_Edibatec_Caisson_Type_Tourelle - 1]) = c_OUI) And (_Caisson.TypeVentilateur = c_Edibatec_Caisson_Type_Tourelle));
  Result := m_Valid;
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaissonGenerique.Caisson_Valide_Options_Entrainement(_Caisson: TEdibatec_Caisson) : Boolean;
Var
  m_Valid : Boolean;
Begin
  m_Valid := (Str2Int(CaissonReseau.Chiff_ENTRAINEMENT[c_Edibatec_Caisson_Entrainement_Direct - 1]) = c_OUI) And (_Caisson.Entrainement = c_Edibatec_Caisson_Entrainement_Direct);
  m_Valid := m_Valid Or
            ((Str2Int(CaissonReseau.Chiff_ENTRAINEMENT[c_Edibatec_Caisson_Entrainement_DirectPoulie - 1]) = c_OUI) And (_Caisson.Entrainement = c_Edibatec_Caisson_Entrainement_DirectPoulie));
  Result := m_Valid;
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaissonGenerique.Caisson_Valide_Options_Alimentation(_Caisson: TEdibatec_Caisson) : Boolean;
Var
  m_Valid : Boolean;
Begin
  m_Valid := (Str2Int(CaissonReseau.Chiff_ALIMENTATION[c_Edibatec_Caisson_Alimentation_MonoPhase - 1]) = c_OUI) And (_Caisson.Alimentation = c_Edibatec_Caisson_Alimentation_MonoPhase);
  m_Valid := m_Valid Or
            ((Str2Int(CaissonReseau.Chiff_ALIMENTATION[c_Edibatec_Caisson_Alimentation_TriPhase - 1]) = c_OUI) And (_Caisson.Alimentation = c_Edibatec_Caisson_Alimentation_TriPhase));
  Result := m_Valid;
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaissonGenerique.Caisson_Valide_Options_TypeMoteur(_Caisson: TEdibatec_Caisson) : Boolean;
Var
  m_Valid : Boolean;
Begin
  m_Valid := (Str2Int(CaissonReseau.Chiff_VITESSE[c_Edibatec_Caisson_Moteur_1Vitesse - 1]) = c_OUI) And (_Caisson.TypeDeMoteur = c_Edibatec_Caisson_Moteur_1Vitesse);
  m_Valid := m_Valid Or
            ((Str2Int(CaissonReseau.Chiff_VITESSE[c_Edibatec_Caisson_Moteur_2Vitesse - 1]) = c_OUI) And (_Caisson.TypeDeMoteur = c_Edibatec_Caisson_Moteur_2Vitesse));
  Result := m_Valid;
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaissonGenerique.Caisson_Valide_Options_Depressostat(_Caisson: TEdibatec_Caisson) : Boolean;
Var
  m_Valid : Boolean;
  m_Valid2 : Boolean;
  m_Valid3 : Boolean;
Begin

  m_Valid  := (CaissonReseau.Chiff_VENTILECO = c_Avec) And (_Caisson.TypeSelection = c_CaissonSelect_DebitCst);
  m_Valid2 := (CaissonReseau.Chiff_NECOADEPR = c_Avec) And ((_Caisson.AvecDepressostat = c_Edibatec_Caisson_Depressostat_OUI) And Not(_Caisson.TypeSelection = c_CaissonSelect_DebitCst));
  m_Valid3 := (CaissonReseau.Chiff_NECOSDEPR = c_Avec) And ((_Caisson.AvecDepressostat = c_Edibatec_Caisson_Depressostat_NON) And Not(_Caisson.TypeSelection = c_CaissonSelect_DebitCst));

  Result := m_Valid Or m_Valid2 Or m_Valid3;

{
  m_Valid := (CaissonReseau.Chiff_DEPRESSOSTAT in [c_Tous, c_Avec]) And (_Caisson.AvecDepressostat = c_Edibatec_Caisson_Depressostat_OUI);
  m_Valid := m_Valid Or
            ((CaissonReseau.Chiff_DEPRESSOSTAT in [c_Tous, c_Sans]) And (_Caisson.AvecDepressostat = c_Edibatec_Caisson_Depressostat_NON));

  m_Valid2 :=  (CaissonReseau.Chiff_VENTILECO in [c_Tous,  c_Avec]) And ((_Caisson.AvecDepressostat = c_Edibatec_Caisson_Depressostat_OUI) And (_Caisson.TypeSelection = c_CaissonSelect_DebitCst));
  m_Valid2 :=  m_Valid2 Or
              ((CaissonReseau.Chiff_VENTILECO in [c_Tous,  c_Sans]) And Not((_Caisson.AvecDepressostat = c_Edibatec_Caisson_Depressostat_OUI) And (_Caisson.TypeSelection = c_CaissonSelect_DebitCst)));

  Result := m_Valid And m_Valid2;
}
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaissonGenerique.Caisson_Valide_Options_Agrementation(_Caisson: TEdibatec_Caisson) : Boolean;
Var
  m_Valid : Boolean;
Begin
  m_Valid := (Str2Int(CaissonReseau.Chiff_AGREMENT[c_Edibatec_Caisson_Agrement_NonAgree - 1]) = c_OUI) And (_Caisson.ClassementFeu = c_Edibatec_Caisson_Agrement_Libelle[c_Edibatec_Caisson_Agrement_NonAgree]);
  m_Valid := m_Valid Or
            ((Str2Int(CaissonReseau.Chiff_AGREMENT[c_Edibatec_Caisson_Agrement_Agree4001h2C4 - 1]) = c_OUI) And ((_Caisson.ClassementFeu = c_Edibatec_Caisson_Agrement_Libelle[c_Edibatec_Caisson_Agrement_Agree4001h2C4]) or
                                                                                                               ((_Caisson.ClassementFeu = c_Edibatec_Caisson_Agrement_Libelle[c_Edibatec_Caisson_Agrement_Agree4001h]) and not((Etude.Batiment.TypeBat = c_BatimentTertiaire) and (Etude.Batiment.TypeTertiaire = c_TertiaireVMC))) or
                                                                                                               ((_Caisson.ClassementFeu = c_Edibatec_Caisson_Agrement_Libelle[c_Edibatec_Caisson_Agrement_Agree4002h]) and not((Etude.Batiment.TypeBat = c_BatimentTertiaire) and (Etude.Batiment.TypeTertiaire = c_TertiaireVMC))))
                                                                                                               );
  Result := m_Valid;
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaissonGenerique.Caisson_Valide_PositionsSorties(_Caisson: TEdibatec_Caisson) : Boolean;

        procedure MirroirSorties(var MyArray : Array of Integer);
        Var
                Cpt : Integer;
        Begin
                For Cpt := 0 To Length(MyArray) - 1 Do
                        Begin
                                Case MyArray[Cpt] of
                                        1 : MyArray[Cpt] := 6;
                                        2 : ;
                                        3 : ;
                                        4 : ;
                                        5 : ;
                                        6 : MyArray[Cpt] := 1;
                                End;
                        End;
        End;

        procedure InverseSorties180(var MyArray : Array of Integer);
        Var
                Cpt : Integer;
        Begin
                For Cpt := 0 To Length(MyArray) - 1 Do
                        Begin
                                Case MyArray[Cpt] of
                                        1 : MyArray[Cpt] := 6;
                                        2 : MyArray[Cpt] := 5;
                                        3 : ;
                                        4 : ;
                                        5 : MyArray[Cpt] := 2;
                                        6 : MyArray[Cpt] := 1;
                                End;
                        End;
        End;

        procedure InverseSorties90(var MyArray : Array of Integer);
        Var
                Cpt : Integer;
        Begin
                For Cpt := 0 To Length(MyArray) - 1 Do
                        Begin
                                Case MyArray[Cpt] of
                                        1 : MyArray[Cpt] := 2;
                                        2 : MyArray[Cpt] := 6;
                                        3 : ;
                                        4 : MyArray[Cpt] := 4;
                                        5 : MyArray[Cpt] := 1;
                                        6 : MyArray[Cpt] := 5;
                                End;
                        End;
        End;

        Function TestSorties(MyArray, ArrayCaisson : Array Of Integer) : Boolean;
        Var
                cptCR       : Integer;
                cpt_C       : Integer;
                CanContinue : Boolean;
        Begin
        Result := True;
                for cptCR := 0 to Length(MyArray) - 1 do
                        Begin
                        CanContinue := False;
                                for cpt_C := 0 to Length(ArrayCaisson) - 1 do
                                        if MyArray[cptCR] =ArrayCaisson[cpt_C] then
                                                begin
                                                CanContinue := true;
                                                break;
                                                end;
                                if CanContinue = false then
                                        Begin
                                        Result := False;
                                        Exit;
                                        End;
                        End;
        Result := True;
        End;


        Function TestEnLigne(MyArrayA, MyArrayB : Array of Integer) : Boolean;
        Var
                Cpt : Integer;
                AlignPetitsCotes : Integer;
                AlignGrandsCotes : Integer;
        Begin
        Result := False;
        AlignPetitsCotes := 0;
        AlignGrandsCotes := 0;

                For Cpt := 0 To Length(MyArrayA) - 1 Do
                        Begin
                                Case MyArrayA[Cpt] of
                                        1 : inc(AlignPetitsCotes);
                                        2 : inc(AlignGrandsCotes);
                                        3 : ;
                                        4 : ;
                                        5 : inc(AlignGrandsCotes);
                                        6 : inc(AlignPetitsCotes);
                                End;
                        End;

                For Cpt := 0 To Length(MyArrayB) - 1 Do
                        Begin
                                Case MyArrayB[Cpt] of
                                        1 : inc(AlignPetitsCotes);
                                        2 : inc(AlignGrandsCotes);
                                        3 : ;
                                        4 : ;
                                        5 : inc(AlignGrandsCotes);
                                        6 : inc(AlignPetitsCotes);
                                End;
                        End;                        

        Result := (AlignPetitsCotes = 2) Or (AlignGrandsCotes = 2);
        End;

        Function TestEnLigneBis(MyArrayA: Array of Integer) : Boolean;
        Var
                Cpt : Integer;
                AlignPetitsCotes : Integer;
                AlignGrandsCotes : Integer;
        Begin
        Result := False;
        AlignPetitsCotes := 0;
        AlignGrandsCotes := 0;

                For Cpt := 0 To Length(MyArrayA) - 1 Do
                        Begin
                                Case MyArrayA[Cpt] of
                                        1 : inc(AlignPetitsCotes);
                                        2 : inc(AlignGrandsCotes);
                                        3 : ;
                                        4 : ;
                                        5 : inc(AlignGrandsCotes);
                                        6 : inc(AlignPetitsCotes);
                                End;
                        End;

        Result := (AlignPetitsCotes = 2) Or (AlignGrandsCotes = 2);
        End;

Var
  m_NoSortie            : Integer;

  _PositionsAspi        : Array Of Integer;
  _PositionsRefoulement : Array Of Integer;
  _nbAspi               : integer;
  _nbRefoulement        : integer;



Begin

result := false;

        if (Length(_Caisson.PositionAspiration) = 0) or (Length(_Caisson.PositionRefoulement) = 0) then
                Begin
                result := False;
                exit;
                End;


_nbRefoulement := 0;
_nbAspi        := 0;

    For m_NoSortie := 0 To CaissonReseau.FilsCalcul.Count - 1 Do
        Begin
                If CaissonReseau.FilsCalcul[m_NoSortie].VersSortieToit Then
                        inc(_nbRefoulement)
                else
                        inc(_nbAspi);
        End;

        if (Length(_Caisson.PositionAspiration) >= _nbAspi) and (Length(_Caisson.PositionRefoulement) >= _nbRefoulement) then
                Begin
                result := true;
//                exit;
                End;

//exit;

        if Not(Result) then
                Exit;
SetLength(_PositionsAspi, 0);
SetLength(_PositionsRefoulement, 0);


    //R�cup�ration des sorties du caisson
    For m_NoSortie := 0 To CaissonReseau.Fils.Count - 1 Do
        Begin                                                                                                                                                                               
                If (CaissonReseau.Fils[m_NoSortie].Fils <> nil) And (CaissonReseau.Fils[m_NoSortie].Fils.count > 0) and (TVentil_TronconDessin_SortieCaisson(CaissonReseau.Fils[m_NoSortie]).TypeSortie = c_CaissonSortie_Refoulement) Then
                        Begin
                        SetLength(_PositionsRefoulement, Length(_PositionsRefoulement) + 1);
                        _PositionsRefoulement[Length(_PositionsRefoulement) - 1] := TVentil_TronconDessin_SortieCaisson(CaissonReseau.Fils[m_NoSortie]).GetNumeroSortieVIM;
                        End
              else                                                                      
                If (CaissonReseau.Fils[m_NoSortie].Fils <> nil) And (CaissonReseau.Fils[m_NoSortie].Fils.count > 0) and (TVentil_TronconDessin_SortieCaisson(CaissonReseau.Fils[m_NoSortie]).TypeSortie = c_CaissonSortie_Aspiration) Then
                        Begin
                        SetLength(_PositionsAspi, Length(_PositionsAspi) + 1);
                        _PositionsAspi[Length(_PositionsAspi) - 1] := TVentil_TronconDessin_SortieCaisson(CaissonReseau.Fils[m_NoSortie]).GetNumeroSortieVIM;
                        End;
        End;


//test de la postion des sorties
Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);

        //si le test est ok, pas la peine de continuer
        if Result Then
                Exit;

//inversion de sens du caisson (car � la saisie, le caisson �tant sym�trique, on conna�t le haut/bas du caisson, mais pas l'orientation)                
InverseSorties180(_PositionsRefoulement);
InverseSorties180(_PositionsAspi);

//nouveau test de position avec les sorties invers�es
Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);

        if Result Then
                Exit;

        if TestEnLigne(_PositionsAspi, _PositionsRefoulement) then
                Begin
                InverseSorties90(_PositionsRefoulement);
                InverseSorties90(_PositionsAspi);
                Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);

                        if Result Then
                                Exit;

                InverseSorties180(_PositionsRefoulement);
                InverseSorties180(_PositionsAspi);

                Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);

                        if Result Then
                                Exit;

                End
        Else
                Begin
                InverseSorties90(_PositionsRefoulement);
                InverseSorties90(_PositionsAspi);
                Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);

                        if Result Then
                                Exit;

                InverseSorties90(_PositionsRefoulement);
                InverseSorties90(_PositionsAspi);
                Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);

                        if Result Then
                                Exit;

                InverseSorties90(_PositionsRefoulement);
                InverseSorties90(_PositionsAspi);
                Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);

                        if Result Then
                                Exit;

                MirroirSorties(_PositionsRefoulement);
                MirroirSorties(_PositionsAspi);

                Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);

                        if Result Then
                                Exit;

                InverseSorties90(_PositionsRefoulement);
                InverseSorties90(_PositionsAspi);
                Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);

                        if Result Then
                                Exit;

                InverseSorties90(_PositionsRefoulement);
                InverseSorties90(_PositionsAspi);
                Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);

                        if Result Then
                                Exit;

                InverseSorties90(_PositionsRefoulement);
                InverseSorties90(_PositionsAspi);
                Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);
                
                        if Result Then
                                Exit;
                End;


        //ajout pour mail du 08/01/2013
        If IsCaissonSelectionFromGammeJBEBorJBHBorJBEBECO(_Caisson) then
                Begin
                Result := TestEnLigneBis(_PositionsAspi);
                        if Result then
                                Exit;
                End;



End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaissonGenerique.Caisson_Valide_DebitMiniMaxi(_Caisson: TEdibatec_Caisson) : Boolean;
Begin
  Result := True; // (Etude.Batiment.Reseaux.GetCaisson.DebitMini > _Caisson.DebitMinimal) And (Etude.Batiment.Reseaux.GetCaisson.DebitMaxi < _Caisson.DebitMaximal);
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaissonGenerique.Caisson_Valide_DPMiniMaxi(_Caisson: TEdibatec_Caisson) : Boolean;
var
        Supp20 : integer;
Begin

result := true;

                Supp20 := 0;

result := result and (_Caisson.DpCalcQMin <= min(CaissonReseau.DPVentilateur.MaxAQmax, CaissonReseau.DPVentilateur.MaxAQmin) - Supp20);
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaissonGenerique.Caisson_Valide_Selection3Courbes(_Caisson: TEdibatec_Caisson) : Boolean;
Var
  DpCourbeAQmin: Real;
  DpCourbeAQmax: Real;
  m_NoCourbe   : Integer;
  CaissonValide: Boolean;
  m_Str        : String;
  m_Stl1,
  m_Stl2,
  m_Stl3       : TStringList;
  PtsBezier         : TPointsControleBezier;
  m_PointQMinDpMin  : TPointDebitPression;
  m_PointQMaxDpMin  : TPointDebitPression;
  m_PointQMinDpMax  : TPointDebitPression;
  m_PointQMaxDpMax  : TPointDebitPression;
  m_Cond1           : Boolean;
  m_CondDTU         : Boolean;
  PuissCourbeAQmin: Real;
  PuissCourbeAQmax: Real;
Begin
  DpCourbeAQMax := 0;
  DpCourbeAQMin := 0;
  _Caisson.Position := '';

  m_PointQMaxDpMin.Q  := Etude.Batiment.Reseaux.GetCaisson.DebitMaxi;
  m_PointQMaxDpMin.DP := CaissonReseau.DPVentilateur.MinAQmax;
  m_PointQMaxDpMax.Q  := Etude.Batiment.Reseaux.GetCaisson.DebitMaxi;
  m_PointQMaxDpMax.DP := CaissonReseau.DPVentilateur.MaxAQmax;

  m_PointQMinDpMin.Q  := Etude.Batiment.Reseaux.GetCaisson.DebitMini;
  m_PointQMinDpMin.DP := CaissonReseau.DPVentilateur.MinAQmin;
  m_PointQMinDpMax.Q  := Etude.Batiment.Reseaux.GetCaisson.DebitMini;
  m_PointQMinDpMax.DP := CaissonReseau.DPVentilateur.MaxAQmin;

//modif dtu
//  if Not CheckBox_DTU.Checked then
        Begin
        m_PointQMaxDpMin.DP := m_PointQMaxDpMin.DP - 20;
        m_PointQMaxDpMax.DP := m_PointQMaxDpMax.DP - 20;

        m_PointQMinDpMin.DP := m_PointQMinDpMin.DP - 20;
        m_PointQMinDpMax.DP := m_PointQMinDpMax.DP - 20;
        End;

  For m_NoCourbe := 1 To 3 Do
  Begin
    Case m_NoCourbe Of
      1: Begin
        m_Str := _Caisson.CourbeDebitPressionMini;
        m_Stl1 := _Caisson.ListeDebitsMinis;
        m_Stl2 := _Caisson.ListeDPMinis;
        m_Stl3 := _Caisson.ListePuissMinis;
      End;
      2: Begin
        m_Str := _Caisson.CourbeDebitPressionInter;
        m_Stl1 := _Caisson.ListeDebitsInt;
        m_Stl2 := _Caisson.ListeDPInt;
        m_Stl3 := _Caisson.ListePuissInt;
      End;
      3: Begin
        m_Str := _Caisson.CourbeDebitPressionMaxi;
        m_Stl1 := _Caisson.ListeDebitsMaxis;
        m_Stl2 := _Caisson.ListeDPMaxis;
        m_Stl3 := _Caisson.ListePuissMaxis;
      End;
    Else Begin
        m_Stl1 := nil;
        m_Stl2 := nil;
        m_Stl3 := nil;
        m_Str  := '';
      End;
    End;

    If (m_Stl1 <> nil) And (m_Stl1.Count > 0) And (m_Stl2 <> nil) And (m_Stl2.Count > 0) {And (m_Str <> '') }Then
    Begin
      PtsBezier := StringToPointsBezier_11points(m_Stl1, m_Stl2);
      DpCourbeAQMax := CalculDpCourbe(m_PointQMaxDpMax, PtsBezier);
      DpCourbeAQMin := CalculDpCourbe(m_PointQMinDpMax, PtsBezier);


      if Etude.Batiment.TypeBat = c_BatimentTertiaire then
        m_CondDTU := (m_PointQMaxDpMin.DP <= DpCourbeAQmax) And (DpCourbeAQmax <= m_PointQMaxDpMax.DP);

{
        m_Cond1   := (DpCourbeAQMin <= m_PointQMinDpMax.DP);

//        m_Cond1 := m_Cond1 and (_Caisson.DpCalcQMax <= DpCourbeAQMax);

                if (Etude.Batiment.TypeBat <> c_BatimentTertiaire) then
                        Begin
//                        m_Cond1 := m_Cond1 and (DpCourbeAQMin <= _Caisson.DpCalcQMin);
                                                                                    
                        m_Cond1 := m_Cond1 and(m_PointQminDpMin.DP <= DpCourbeAQMin);
                        End;

}

        m_Cond1   := (m_PointQMaxDpMin.DP <= DpCourbeAQMax);
        m_Cond1 := m_Cond1 and (DpCourbeAQMax <= m_PointQMaxDpMax.DP);

        //ajout de la condition DTU. Selon S. Bergeron, pas de contr�le sur d�bit mini si non conformit� au DTU
                if (Etude.Batiment.TypeBat <> c_BatimentTertiaire) then
                        Begin
                        m_Cond1 := m_Cond1 and(m_PointQMinDpMin.DP <= DpCourbeAQMin);
                        m_Cond1 := m_Cond1 and (DpCourbeAQMin <= m_PointQMinDpMax.DP);
                        End;



                CaissonValide := m_Cond1 And m_CondDTU
    End
    Else CaissonValide := False;        

    CaissonValide := CaissonValide And Not IsNan(DpCourbeAQMax) And (DpCourbeAQMax <> 0);

    If CaissonValide Then
    Begin
      Case m_NoCourbe Of
        1: _Caisson.Position := _Caisson.PositionMini;
        2: _Caisson.Position := _Caisson.PositionInter;
        3: _Caisson.Position := _Caisson.PositionMaxi;
      End;
      _Caisson.PositionChoisie := m_NoCourbe;
      _Caisson.Selection11Points := True;
      _Caisson.CoeffRT := CaissonReseau.CoeffRt(_Caisson);
      _Caisson.DpCalcQMax  := DpCourbeAQMax;
      _Caisson.DpCalcQMin  := DpCourbeAQMin;


      If RadioBezier.ItemIndex = 0 Then PtsBezier := StringToPointsBezier_4points(m_Str)
                                   Else PtsBezier := StringToPointsBezier_11points(m_Stl1, m_Stl3);

      PuissCourbeAQMax := CalculDpCourbe(m_PointQMaxDpMax, PtsBezier);
      PuissCourbeAQMin := CalculDpCourbe(m_PointQMinDpMax, PtsBezier);

{
       CaissonValide := PuissCourbeAQMax <= DpCourbeAQMax;
                if (Etude.Batiment.TypeBat <> c_BatimentTertiaire) then
                        CaissonValide :=  CaissonValide And (PuissCourbeAQMin <= DpCourbeAQMin);
}

      _Caisson.PuissCalcQMax  := PuissCourbeAQMax;
      _Caisson.PuissCalcQMin  := PuissCourbeAQMin;

      _Caisson.CourbeDebitCalc := m_Stl1;
      _Caisson.CourbeDpCalc    := m_Stl2;
      _Caisson.CourbePuissCalc := m_Stl3;
      
        if CaissonValide then
                Break;
    End;
  End;
  Result := CaissonValide;
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaissonGenerique.Caisson_Valide_SelectionPlageFonctionnement(_Caisson: TEdibatec_Caisson) : Boolean;
Var
  m_Point   : TPointDebitPression;
  Cond1, Cond2: Boolean;
  Vit_DpMaxQMax,
  Vit_DpmaxQmin,
  Vit_DpMinQMax,
  Vit_DpminQmin: Real;
  VMinTrouvee, VMaxTrouvee: Real;
  cpt : Integer;
  MyVitesse : Real;
  myDP      : Real;
  myPUI     : Real;
  ValMaxAQMax : Real;
  ValMinAQMax : Real;
  ValMaxAQMin : Real;
  ValMinAQMin : Real;
  Ratio : Real;
Begin
  _Caisson.Position := '';
  m_Point.Dp := 0;

  ValMaxAQMax := CaissonReseau.DPVentilateur.MaxAQmax;
  ValMinAQMax := CaissonReseau.DPVentilateur.MinAQmax;
  ValMaxAQMin := CaissonReseau.DPVentilateur.MaxAQmin;
  ValMinAQMin := CaissonReseau.DPVentilateur.MinAQmin;

//modif dtu
//        if not CheckBox_DTU.Checked then
                Begin
                ValMaxAQMax := ValMaxAQMax - 20;
                ValMinAQMax := ValMinAQMax - 20;
                ValMaxAQMin := ValMaxAQMin - 20;
                ValMinAQMin := ValMinAQMin - 20;
                End;

  { Vitesses des 2 points � d�bit maxi }
  m_Point.Q := Etude.Batiment.Reseaux.GetCaisson.DebitMaxi;
  m_Point.DP:= ValMaxAQMax;
  Vit_DpMaxQMax := CalculVitessePoint(m_Point, _Caisson, true);
  m_Point.DP:= ValMinAQMax;
  Vit_DpMinQMax := CalculVitessePoint(m_Point, _Caisson, true);

  { Vitesses des 2 points � d�bit mini }
  m_Point.Q := Etude.Batiment.Reseaux.GetCaisson.DebitMini;
  m_Point.DP:= ValMaxAQmin;
  Vit_DpMaxQMin := CalculVitessePoint(m_Point, _Caisson, true);
  m_Point.DP:= ValMinAQmin;
  Vit_DpMinQMin := CalculVitessePoint(m_Point, _Caisson, true);

  Cond1 := (Vit_DpMaxQMax > 0) And (Vit_DpMinQMax > 0);

  if CheckBox_DTU.Checked then
          Cond2 := (Vit_DpMaxQMin > 0) And (Vit_DpMinQMin > 0)
  else
          Cond2 := true;


{
  if CheckBox_DTU.Checked then
        begin
          // On prend comme vitesse mini le max des Vit_DpminQmin, Vit_DpminQmax et Vmin caisson
          VMinTrouvee := Max(_Caisson.VitesseMini, Max(Vit_DpMinQMin, Vit_DpMinQMax));
          // On prend comme vitesse maxi le min des vmax et Vmax caisson
          VMaxTrouvee := Min(_Caisson.VitesseMaxi, Min(Vit_DpMaxQMin, Vit_DpmaxQmax));
        end
  else
}  
        begin
          // On prend comme vitesse mini le max des Vit_DpminQmin, Vit_DpminQmax et Vmin caisson
          VMinTrouvee := Max(_Caisson.VitesseMini, Vit_DpMinQMax);
          // On prend comme vitesse maxi le min des vmax et Vmax caisson
          VMaxTrouvee := Min(_Caisson.VitesseMaxi, Vit_DpmaxQmax);
        end;

  Result := Cond1 And Cond2 And (VMinTrouvee <= VMaxTrouvee);
  If Result Then Begin
    // Puis on calcule la dp r�elle pour le point � QMin
    _Caisson.DpCalcQMin := ValMinAQmin * Sqr(VMinTrouvee/Vit_DpMinQMin);
    // Puis on calcule la dp r�elle pour le point � QMax
  _Caisson.DpCalcQMax := ValMinAQmax * Sqr(VMinTrouvee/Vit_DpMinQMax);
    _Caisson.Position := Float2Str(VMinTrouvee, 0);
    _Caisson.Selection11Points :=  RadioBezier.ItemIndex = 1;
    If _Caisson.DpCalcQMax < ValMinAQmax Then Result := False;
//    _Caisson.CoeffRT  := CaissonReseau.CoeffRt(_Caisson);


    _Caisson.CourbeDebitCalc := TStringList.Create;
    _Caisson.CourbeDpCalc := TStringList.Create;
    _Caisson.CourbePuissCalc := TStringList.Create;

        For Cpt := 0 to _Caisson.ListeDebitsMinis.Count - 1 do
                Begin

                _Caisson.CourbeDebitCalc.add(_Caisson.ListeDebitsMinis[Cpt]);

                m_Point.Q := Str2Float(_Caisson.ListeDebitsMinis[Cpt]);
                m_Point.DP:= Str2Float(_Caisson.ListeDPMinis[Cpt]);
                MyVitesse := CalculVitessePoint(m_Point, _Caisson, true);


                MyDP := m_Point.DP * Sqr(VMinTrouvee/MyVitesse);
                _Caisson.CourbeDpCalc.Add(Float2Str(MyDP, 2));
                MyPUI := Str2Float(_Caisson.ListePuissMinis[Cpt]) * Sqr(VMinTrouvee/MyVitesse);
                _Caisson.CourbePuissCalc.Add(Float2Str(MyPUI, 2));


                Ratio := (MyDP - Str2Float(_Caisson.ListeDPMinis[Cpt])) / (Str2Float(_Caisson.ListeDPMaxis[Cpt]) - Str2Float(_Caisson.ListeDPMinis[Cpt]));
                Ratio := Max(0, Ratio);

                _Caisson.CourbeDebitCalc[Cpt] := Float2Str(Str2Float(_Caisson.ListeDebitsMinis[Cpt]) + (Str2Float(_Caisson.ListeDebitsMaxis[Cpt]) - Str2Float(_Caisson.ListeDebitsMinis[Cpt])) * Ratio, 0);
                _Caisson.CourbeDpCalc[Cpt]    := Float2Str(Str2Float(_Caisson.ListeDPMinis[Cpt])    + (Str2Float(_Caisson.ListeDPMaxis[Cpt])     - Str2Float(_Caisson.ListeDPMinis[Cpt])) * Ratio, 0);
                _Caisson.CourbePuissCalc[Cpt] := Float2Str(Str2Float(_Caisson.ListePuissMinis[Cpt]) + (Str2Float(_Caisson.ListePuissMaxis[Cpt])  - Str2Float(_Caisson.ListePuissMinis[Cpt])) * Ratio, 0);

                End;

 _Caisson.CoeffRT  := CaissonReseau.CoeffRt(_Caisson, _Caisson.CourbeDebitCalc, _Caisson.CourbePuissCalc);

  End;
End;
{ **************************************************************************************************************************************************** }
Function  TDlg_SelectionCaissonGenerique.Caisson_Valide_SelectionDebitConstantOld(_Caisson: TEdibatec_Caisson) : Boolean;
        Procedure GetEquationDroite(_Pt1X, _Pt1Y, _Pt2X, _Pt2Y : Real; var _Coef, _Origine : Real);
        Begin
        _Coef :=  (_Pt2Y - _Pt1Y) / (_Pt2X - _Pt1X);
        _Origine := _Pt1Y - _Coef * _Pt1X;
        End;

        Function CalculeDebitSurDroite(_Pt1Debit, _pt1DP, _Pt2Debit, Pt2DP, _ValDP : Real) : Real;
        var
                CoefDir : Real;
                OrdonneeOrigine : Real;
        Begin
        GetEquationDroite(_Pt1Debit, _pt1DP, _Pt2Debit, Pt2DP, CoefDir, OrdonneeOrigine);
        Result := (_ValDP - OrdonneeOrigine) / CoefDir;
        End;

        Function CalculeDPSurDroite(_Pt1Debit, _pt1DP, _Pt2Debit, Pt2DP, _ValDebit : Real) : Real;
        var
                CoefDir : Real;
                OrdonneeOrigine : Real;
        Begin
        GetEquationDroite(_Pt1Debit, _pt1DP, _Pt2Debit, Pt2DP, CoefDir, OrdonneeOrigine);
        Result := CoefDir * _ValDebit + OrdonneeOrigine;
        End;


        Function DetermineDPAQCible(ListeDebits, ListeDPS : TStringList; ValeurAEncadrer, ValeurRemplacement : Real) : Real;
        var
                MyNoPoint : Integer;
        Begin
        Result := ValeurRemplacement;
                for MyNoPoint := 1 to ListeDebits.Count - 1 do
                        Begin
                                if Str2Float(ListeDebits[MyNoPoint]) > ValeurAEncadrer then
                                        Begin
                                                if MyNoPoint > 0 then
                                                        Result := CalculeDPSurDroite(Str2Float(ListeDebits[MyNoPoint]), Str2Float(ListeDPS[MyNoPoint]),
                                                                                                Str2Float(ListeDebits[MyNoPoint - 1]), Str2Float(ListeDPS[MyNoPoint - 1]),
                                                                                                ValeurAEncadrer)
                                                else
                                                        Result := ValeurRemplacement;
                                        Break;
                                        End;
                        End;
        End;

        Procedure TesteTranche(Var _Tranche : Integer; Var _DP : Real; _Q : Real);
        Begin
                if _Tranche = - 1 then
                        Begin
                                if _DP < Str2Float(_Caisson.ListeDPMinis[0]) then
                                        Begin
                                        _Tranche := 1;
                                        _DP := CalculeDPSurDroite(Str2Float(_Caisson.ListeDebitsMinis[_Tranche - 1]), Str2Float(_Caisson.ListeDPMinis[_Tranche - 1]), Str2Float(_Caisson.ListeDebitsMaxis[_Tranche - 1]) , Str2Float(_Caisson.ListeDPMaxis[_Tranche - 1]), _Q);
                                        End
                        Else
                                if _DP > Str2Float(_Caisson.ListeDPMinis[_Caisson.ListeDPMinis.Count - 1]) then
                                        Begin
                                        _Tranche := (_Caisson.ListeDPMinis.Count - 1);
                                        _DP := CalculeDPSurDroite(Str2Float(_Caisson.ListeDebitsMinis[_Tranche]), Str2Float(_Caisson.ListeDPMinis[_Tranche]), Str2Float(_Caisson.ListeDebitsMaxis[_Tranche]) , Str2Float(_Caisson.ListeDPMaxis[_Tranche]), _Q);
                                        End;
                        End;
        End;

        Function CalculeReglage(_Tranche, _NbPositionReglage : Integer; _DPCible : Real) : Real;
        Begin
        Result := 0.5 * Round(_NbPositionReglage * (_DPCible - Str2Float(_Caisson.ListeDPMinis[_Tranche - 1])) / (Str2Float(_Caisson.ListeDPMaxis[_Tranche - 1]) - Str2Float(_Caisson.ListeDPMinis[_Tranche - 1])) + 0.5);
        End;

var

        _NoPoint               : Integer;

        QMinCible              : Real;
        DPMinQMinCible         : Real;
        DPMaxQMinCible         : Real;
        QMaxCible              : Real;
        DPMinQMaxCible         : Real;
        DPMaxQMaxCible         : Real;

        QMinPosMin             : Real;
        QMinPosMax             : Real;
        QMaxPosMin             : Real;
        QMaxPosMax             : Real;

        DPPosMinAQMinCible     : Real;
        DPPosMaxAQMinCible     : Real;
        DPPosMinAQMaxCible     : Real;
        DPPosMaxAQMaxCible     : Real;

        PQMinCibleN1PosMin     : TPointDebitPression;
        PQMinCibleN1PosMax     : TPointDebitPression;
        PQMinCibleN2PosMin     : TPointDebitPression;
        PQMinCibleN2PosMax     : TPointDebitPression;

        PQMaxCibleN1PosMin     : TPointDebitPression;
        PQMaxCibleN1PosMax     : TPointDebitPression;
        PQMaxCibleN2PosMin     : TPointDebitPression;
        PQMaxCibleN2PosMax     : TPointDebitPression;

        DPN1AQminCible         : Real;
        DPN2AQminCible         : Real;

        DPN1AQmaxCible         : Real;
        DPN2AQmaxCible         : Real;

        TrancheDPMinQMinCible  : Integer;
        TrancheDPMaxQMinCible  : Integer;
        TrancheDPMinQMaxCible  : Integer;
        TrancheDPMaxQMaxCible  : Integer;

        PositionMini           : Real;
        PositionMaxi           : Real;
        NbPositionReglage      : Integer;
        
        ReglageDPMinQMinCible  : Real;
        ReglageDPMaxQMinCible  : Real;
        ReglageDPMinQMaxCible  : Real;
        ReglageDPMaxQMaxCible  : Real;

        ReglageMin             : Real;
        ReglageMax             : Real;
        Reglage                : Real;

        CourbeDebit            : TStringList;
        CourbeDP               : TStringList;
        CourbePuissance        : TStringList;

        TempDebit              : Real;
        TempDP                 : Real;
        TempPuissance          : Real;

        ResultatDPQminCible    : Real;
        ResultatDPQmaxCible    : Real;
        ResultatPQminCible     : Real;
        ResultatPQmaxCible     : Real;
Begin

result := false;
ResultatDPQmaxCible := 0;
ResultatDPQminCible := 0;
ResultatPQmaxCible := 0;
ResultatPQminCible := 0;


QMinCible              := CaissonReseau.Reseau_DebitMinimum;
DPMinQMinCible         := CaissonReseau.DPVentilateur.MinAQmin;
DPMaxQMinCible         := CaissonReseau.DPVentilateur.MaxAQmin;
QMaxCible              := CaissonReseau.Reseau_DebitMaximum;
DPMinQMaxCible         := CaissonReseau.DPVentilateur.MinAQmax;
DPMaxQMaxCible         := CaissonReseau.DPVentilateur.MaxAQmax;

//modif dtu
//        if Not CheckBox_DTU.Checked then
                Begin
                DPMinQMinCible         := DPMinQMinCible - 20;
                DPMaxQMinCible         := DPMaxQMinCible - 20;
                DPMinQMaxCible         := DPMinQMaxCible - 20;
                DPMaxQMaxCible         := DPMaxQMaxCible - 20;
                End;


QMinPosMin := Str2Float(_Caisson.ListeDebitsMinis[0]);
QMaxPosMin := Str2Float(_Caisson.ListeDebitsMinis[0]);
        for _NoPoint := 0 to _Caisson.ListeDebitsMinis.Count - 1 do
                Begin
                QMinPosMin := Min(QMinPosMin, Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]));
                QMaxPosMin := Max(QMaxPosMin, Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]));
                End;

QMinPosMax := Str2Float(_Caisson.ListeDebitsMaxis[0]);
QMaxPosMax := Str2Float(_Caisson.ListeDebitsMaxis[0]);
        for _NoPoint := 0 to _Caisson.ListeDebitsMaxis.Count - 1 do
                Begin
                QMinPosMax := Min(QMinPosMax, Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint]));
                QMaxPosMax := Max(QMaxPosMax, Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint]));
                End;

//ajout de la condition DTU. Selon S. Bergeron, pas de contr�le sur d�bit mini si non conformit� au DTU
if (CheckBox_DTU.Checked) then
begin
if (Min(QMinPosMin, QMinPosMax) <= QMinCible) and (QMinCible <= Max(QMaxPosMin, QMaxPosMax)) then
        Begin
        //on continue
        End
else
        Begin
        Result := False;
        exit;
        End;
end;        

DPMinQMinCible := Max(DPMinQMinCible, DetermineDPAQCible(_Caisson.ListeDebitsMinis, _Caisson.ListeDPMinis, QMinCible, 0));
DPMaxQMinCible := Min(DPMaxQMinCible, DetermineDPAQCible(_Caisson.ListeDebitsMaxis, _Caisson.ListeDPMaxis, QMinCible, 30000000000000000));
DPMinQMaxCible := Max(DPMinQMaxCible, DetermineDPAQCible(_Caisson.ListeDebitsMinis, _Caisson.ListeDPMinis, QMaxCible, 0));
DPMaxQMaxCible := Min(DPMaxQMaxCible, DetermineDPAQCible(_Caisson.ListeDebitsMaxis, _Caisson.ListeDPMaxis, QMaxCible, 30000000000000000));


TrancheDPMinQMinCible := - 1;
TrancheDPMaxQMinCible := - 1;
TrancheDPMinQMaxCible := - 1;
TrancheDPMaxQMaxCible := - 1;

        for _NoPoint := 0 to _Caisson.ListePuissMinis.Count - 1 do
                Begin
                        if _NoPoint < _Caisson.ListePuissMinis.Count - 1 then
                                Begin
                                        if (Min(Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]), Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint])) <= QMinCible) And
                                           (QMinCible <= Max(Str2Float(_Caisson.ListeDebitsMinis[_NoPoint + 1]), Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint + 1]))) then
                                                Begin
                                                PQMinCibleN1PosMin.DP := Str2Float(_Caisson.ListeDPMinis[_NoPoint]);
                                                PQMinCibleN1PosMin.Q  := Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]);
                                                PQMinCibleN1PosMax.DP := Str2Float(_Caisson.ListeDPMaxis[_NoPoint]);
                                                PQMinCibleN1PosMax.Q  := Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint]);
                                                PQMinCibleN2PosMin.DP := Str2Float(_Caisson.ListeDPMinis[_NoPoint + 1]);
                                                PQMinCibleN2PosMin.Q  := Str2Float(_Caisson.ListeDebitsMinis[_NoPoint + 1]);
                                                PQMinCibleN2PosMax.DP := Str2Float(_Caisson.ListeDPMaxis[_NoPoint + 1]);
                                                PQMinCibleN2PosMax.Q  := Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint + 1]);

                                                DPN1AQminCible := CalculeDPSurDroite(PQMinCibleN1PosMin.Q, PQMinCibleN1PosMin.DP, PQMinCibleN1PosMax.Q, PQMinCibleN1PosMax.DP, QMinCible);
                                                DPN2AQminCible := CalculeDPSurDroite(PQMinCibleN2PosMin.Q, PQMinCibleN2PosMin.DP, PQMinCibleN2PosMax.Q, PQMinCibleN2PosMax.DP, QMinCible);

                                                        if (DPN1AQminCible <= DPMinQMinCible) and
                                                           (DPMinQMinCible <= DPN2AQminCible) then
                                                                Begin
                                                                TrancheDPMinQMinCible := Min(10, Max(1, _NoPoint + 1));
                                                                End;

                                                        if (DPN1AQminCible <= DPMaxQMinCible) and
                                                           (DPMaxQMinCible <= DPN2AQminCible) then
                                                                Begin
                                                                TrancheDPMaxQMinCible := Min(10, Max(1, _NoPoint + 1));
                                                                End;

                                                End;

                                        if (Min(Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]), Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint])) <= QMaxCible) And
                                           (QMaxCible <= Max(Str2Float(_Caisson.ListeDebitsMinis[_NoPoint + 1]), Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint + 1]))) then
                                                Begin
                                                PQMaxCibleN1PosMin.DP := Str2Float(_Caisson.ListeDPMinis[_NoPoint]);
                                                PQMaxCibleN1PosMin.Q  := Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]);
                                                PQMaxCibleN1PosMax.DP := Str2Float(_Caisson.ListeDPMaxis[_NoPoint]);
                                                PQMaxCibleN1PosMax.Q  := Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint]);
                                                PQMaxCibleN2PosMin.DP := Str2Float(_Caisson.ListeDPMinis[_NoPoint + 1]);
                                                PQMaxCibleN2PosMin.Q  := Str2Float(_Caisson.ListeDebitsMinis[_NoPoint + 1]);
                                                PQMaxCibleN2PosMax.DP := Str2Float(_Caisson.ListeDPMaxis[_NoPoint + 1]);
                                                PQMaxCibleN2PosMax.Q  := Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint + 1]);

                                                DPN1AQmaxCible := CalculeDPSurDroite(PQMaxCibleN1PosMin.Q, PQMaxCibleN1PosMin.DP, PQMaxCibleN1PosMax.Q, PQMaxCibleN1PosMax.DP, QMaxCible);
                                                DPN2AQmaxCible := CalculeDPSurDroite(PQMaxCibleN2PosMin.Q, PQMaxCibleN2PosMin.DP, PQMaxCibleN2PosMax.Q, PQMaxCibleN2PosMax.DP, QMaxCible);

                                                        if (DPN1AQmaxCible <= DPMinQMaxCible) and
                                                           (DPMinQMinCible <= DPN2AQmaxCible) then
                                                                Begin
                                                                TrancheDPMinQMaxCible := Min(10, Max(1, _NoPoint + 1));
                                                                End;

                                                        if (DPN1AQmaxCible <= DPMaxQMaxCible) and
                                                           (DPMaxQMaxCible <= DPN2AQmaxCible) then
                                                                Begin
                                                                TrancheDPMaxQMaxCible := Min(10, Max(1, _NoPoint + 1));
                                                                End;


                                                End;
                                End;


                End;

TesteTranche(TrancheDPMinQMinCible, DPMinQMinCible, QMinCible);
TesteTranche(TrancheDPMaxQMinCible, DPMaxQMinCible, QMinCible);
TesteTranche(TrancheDPMinQMaxCible, DPMinQMaxCible, QMaxCible);
TesteTranche(TrancheDPMaxQMaxCible, DPMaxQMaxCible, QMaxCible);

PositionMini := Str2Float(Copy(StringReplace(_Caisson.PositionMini, ',', '.', [rfReplaceAll]), 4, Length(_Caisson.PositionMini) - 3));
PositionMaxi := Str2Float(Copy(StringReplace(_Caisson.PositionMaxi, ',', '.', [rfReplaceAll]), 4, Length(_Caisson.PositionMaxi) - 3));
NbPositionReglage := round(2*(PositionMaxi - PositionMini));

if (TrancheDPMinQMinCible = -1) or
   (TrancheDPMaxQMinCible = -1) or
   (TrancheDPMinQMaxCible = -1) or
   (TrancheDPMaxQMaxCible = -1) then exit;

ReglageDPMinQMinCible := CalculeReglage(TrancheDPMinQMinCible, NbPositionReglage, DPMinQMinCible);
ReglageDPMaxQMinCible := CalculeReglage(TrancheDPMaxQMinCible, NbPositionReglage, DPMaxQMinCible);
ReglageDPMinQMaxCible := CalculeReglage(TrancheDPMinQMaxCible, NbPositionReglage, DPMinQMaxCible);
ReglageDPMaxQMaxCible := CalculeReglage(TrancheDPMaxQMaxCible, NbPositionReglage, DPMaxQMaxCible);

//Modification valid�e par Stephane Bergeron
if not(CheckBox_DTU.Checked) then
begin
ReglageMin := Max(ReglageDPMinQMaxCible, PositionMini);
ReglageMax := Min(ReglageDPMaxQMaxCible, PositionMaxi);
end
else
begin
ReglageMin := Max(Max(ReglageDPMinQMinCible, ReglageDPMinQMaxCible), PositionMini);
ReglageMax := Min(Min(ReglageDPMaxQMinCible, ReglageDPMaxQMaxCible), PositionMaxi);
end;
        if ReglageMin <= ReglageMax then
                Reglage := ReglageMin
        else
                Begin
                Result := False;
                Exit;
                End;


CourbeDebit     := TStringList.Create;
CourbeDP        := TStringList.Create;
CourbePuissance := TStringList.Create;

        for _NoPoint := 0 to _Caisson.ListePuissMinis.Count - 1 do
                Begin
                TempDebit := Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]) + Reglage / (0.5 * NbPositionReglage) * (Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint]) - Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]));
                CourbeDebit.Add(Float2Str(TempDebit, 0));
                TempDP := Str2Float(_Caisson.ListeDPMinis[_NoPoint]) + Reglage / (0.5 * NbPositionReglage) * (Str2Float(_Caisson.ListeDPMaxis[_NoPoint]) - Str2Float(_Caisson.ListeDPMinis[_NoPoint]));
                CourbeDP.Add(Float2Str(TempDP, 0));
                TempPuissance := Str2Float(_Caisson.ListePuissMinis[_NoPoint]) + Reglage / (0.5 * NbPositionReglage) * (Str2Float(_Caisson.ListePuissMaxis[_NoPoint]) - Str2Float(_Caisson.ListePuissMinis[_NoPoint]));
                CourbePuissance.Add(Float2Str(TempPuissance, 0));
                End;

                if (Etude.Batiment.TypeBat <> c_BatimentTertiaire) then
                        if Str2Float(CourbeDebit[0]) > QMinCible then
                                exit;

        if Str2Float(CourbeDebit[CourbeDebit.Count - 1]) < QMaxCible then
                exit;

        for _NoPoint := 1 to CourbeDebit.Count - 1 do
                Begin
                        if Str2Float(CourbeDebit[_NoPoint]) >= QMinCible then
                                Begin
                                ResultatDPQminCible := Str2Float(CourbeDP[_NoPoint - 1]) + (QMinCible - Str2Float(CourbeDebit[_NoPoint - 1])) / (Str2Float(CourbeDebit[_NoPoint]) - Str2Float(CourbeDebit[_NoPoint - 1])) * (Str2Float(CourbeDP[_NoPoint]) - Str2Float(CourbeDP[_NoPoint - 1]));
                                ResultatPQminCible  := Str2Float(CourbePuissance[_NoPoint - 1]) + (QMinCible - Str2Float(CourbeDebit[_NoPoint - 1])) / (Str2Float(CourbeDebit[_NoPoint]) - Str2Float(CourbeDebit[_NoPoint - 1])) * (Str2Float(CourbePuissance[_NoPoint]) - Str2Float(CourbePuissance[_NoPoint - 1]));
                                Break;
                                End;
                End;

        for _NoPoint := 1 to CourbeDebit.Count - 1 do
                Begin
                        if Str2Float(CourbeDebit[_NoPoint]) >= QMaxCible then
                                Begin
                                ResultatDPQmaxCible := Str2Float(CourbeDP[_NoPoint - 1]) + (QMaxCible - Str2Float(CourbeDebit[_NoPoint - 1])) / (Str2Float(CourbeDebit[_NoPoint]) - Str2Float(CourbeDebit[_NoPoint - 1])) * (Str2Float(CourbeDP[_NoPoint]) - Str2Float(CourbeDP[_NoPoint - 1]));
                                ResultatPQmaxCible  := Str2Float(CourbePuissance[_NoPoint - 1]) + (QMaxCible - Str2Float(CourbeDebit[_NoPoint - 1])) / (Str2Float(CourbeDebit[_NoPoint]) - Str2Float(CourbeDebit[_NoPoint - 1])) * (Str2Float(CourbePuissance[_NoPoint]) - Str2Float(CourbePuissance[_NoPoint - 1]));
                                Break;
                                End;
                End;

Result := True;

_Caisson.Selection11Points :=  RadioBezier.ItemIndex = 1;
_Caisson.DpCalcQMax  := ResultatDPQmaxCible;
_Caisson.DpCalcQMin  := ResultatDPQminCible;
        if Int(Reglage)=Reglage then
        _Caisson.Position := Float2Str(Reglage, 0)
        else
        _Caisson.Position := Float2Str(Reglage, 1);

_Caisson.CoeffRT := CaissonReseau.CoeffRt(_Caisson, CourbeDebit, CourbePuissance);
_Caisson.Position := 'Pos ' + _Caisson.Position;

_Caisson.PuissCalcQMax  := ResultatPQmaxCible;
_Caisson.PuissCalcQMin  := ResultatPQminCible;

_Caisson.CourbeDpCalc    := CourbeDP;
_Caisson.CourbeDebitCalc := CourbeDebit;
_Caisson.CourbePuissCalc := CourbePuissance;

{
CourbeDebit.Destroy;
CourbeDP.Destroy;
CourbePuissance.Destroy;
}
End;
{ **************************************************************************************************************************************************** }
Function  TDlg_SelectionCaissonGenerique.Caisson_Valide_SelectionDebitConstant(_Caisson: TEdibatec_Caisson) : Boolean;

        Function DeltaPe(_Q : Real) : Real;
        Begin
        result := ( _Caisson.Ventil_PressionConstante.EA_X3 * Power(_Q, 3) + _Caisson.Ventil_PressionConstante.EA_X2 * Power(_Q, 2) + _Caisson.Ventil_PressionConstante.EA_X1 * _Q + _Caisson.Ventil_PressionConstante.EA_X0) / Power(10, 18);
        End;

        Function DeltaPmin(_Q : Real) : Real;
        Begin
        result :=  ( _Caisson.Ventil_PressionConstante.MinA_X1 * _Q +  _Caisson.Ventil_PressionConstante.MinA_X0) / Power(10, 18);
        End;

        Function Pe(_Q : Real) : Real;
        Begin
        result := ( _Caisson.Ventil_PressionConstante.EP_X2 * Power(_Q, 2) + _Caisson.Ventil_PressionConstante.EP_X1 * _Q + _Caisson.Ventil_PressionConstante.EP_X0) / Power(10, 18);        
        End;

        Function Pmin(_Q : Real) : Real;
        Begin
        result := ( _Caisson.Ventil_PressionConstante.MinP_X2 * Power(_Q, 2) + _Caisson.Ventil_PressionConstante.MinP_X1 * _Q + _Caisson.Ventil_PressionConstante.MinP_X0) / Power(10, 18);
        End;

        Function Rp(_Qp, _Deltap : Real) : Real;
        Begin
        result := _Deltap -  DeltaPmin(_Qp);
        End;

Var
        ConditionReglage       : Boolean;

        QMinCible              : Real;
        DPMinQMinCible         : Real;
        DPMaxQMinCible         : Real;
        QMaxCible              : Real;
        DPMinQMaxCible         : Real;
        DPMaxQMaxCible         : Real;

        QpCible                : Real;
        DeltapCible            : Real;

        RpCalcul               : Real;
        DeltaPeCalcul          : Real;
        DeltaPeCalculQmin          : Real;
        DeltaPeCalculQmax          : Real;
        PeCalcul               : Real;
        DeltaPminCalcul        : Real;
        PminCalcul             : Real;
        PpCalcul               : Real;
        PpCalculQmin               : Real;
        PpCalculQmax               : Real;

CourbeDebit     : TStringList;
CourbeDP        : TStringList;
CourbePuissance : TStringList;
_NoPoint : Integer;
TempDebit              : Single;
TempDP                 : Single;
TempPuissance          : Single;
Pas                    : Single;
Ratio                  : Real;

Begin

Result := False;

QMinCible              := CaissonReseau.Reseau_DebitMinimum;
DPMinQMinCible         := CaissonReseau.DPVentilateur.MinAQmin - 20;
DPMaxQMinCible         := CaissonReseau.DPVentilateur.MaxAQmin - 20;
QMaxCible              := CaissonReseau.Reseau_DebitMaximum;
DPMinQMaxCible         := CaissonReseau.DPVentilateur.MinAQmax - 20;
DPMaxQMaxCible         := CaissonReseau.DPVentilateur.MaxAQmax - 20;



//if CheckBox_DTU.Checked then
begin
ConditionReglage       := False;
QpCible                := QMaxCible;
DeltapCible            := DPMinQMaxCible;



        if (_Caisson.Ventil_PressionConstante.QMin <= QpCible) and ( QpCible <= _Caisson.Ventil_PressionConstante.QMax) then
                Begin
                RpCalcul := Rp(QpCible, DeltapCible);
                        if (0 <= RpCalcul) and (RpCalcul <> (_Caisson.Ventil_PressionConstante.RMax - _Caisson.Ventil_PressionConstante.RMin) ) then
                                ConditionReglage := True;
                End;


        if not(ConditionReglage){ and CheckBox_DTU.Checked }then
                Exit;

DeltaPeCalcul    := DeltaPe(QpCible);
PeCalcul         := Pe(QpCible);
DeltaPminCalcul  := DeltaPmin(QpCible);
PminCalcul       := Pmin(QpCible);

//demande modif Bergeron du 10/02/2012
//PpCalcul := PminCalcul + (PeCalcul - PminCalcul) * (DeltapCible  - DeltaPminCalcul) / (DeltaPeCalcul  - DeltaPminCalcul);
PpCalcul := PminCalcul + (PeCalcul - PminCalcul) * Power((DeltapCible  - DeltaPminCalcul) / (DeltaPeCalcul  - DeltaPminCalcul), 3/2);

//DeltaPeCalculQmax := DeltaPeCalcul;
DeltaPeCalculQmax := DeltaPminCalcul + RpCalcul;
PpCalculQmax := PpCalcul;

end;

ConditionReglage       := False;
QpCible                := QMinCible;
DeltapCible            := DPMinQMinCible;



        if (_Caisson.Ventil_PressionConstante.QMin <= QpCible) and ( QpCible <= _Caisson.Ventil_PressionConstante.QMax) then
                Begin
//                RpCalcul := Rp(QpCible, DelatpCible);
                        if (0 <= RpCalcul) and (RpCalcul <> (_Caisson.Ventil_PressionConstante.RMax - _Caisson.Ventil_PressionConstante.RMin) ) then
                                ConditionReglage := True;
                End;


        if not(ConditionReglage) and CheckBox_DTU.Checked  then
                Exit;

DeltaPeCalcul    := DeltaPe(QpCible);
PeCalcul         := Pe(QpCible);
DeltaPminCalcul  := DeltaPmin(QpCible);
PminCalcul       := Pmin(QpCible);

//demande modif Bergeron du 10/02/2012
//PpCalcul := PminCalcul + (PeCalcul - PminCalcul) * (DeltapCible  - DeltaPminCalcul) / (DeltaPeCalcul  - DeltaPminCalcul);
PpCalcul := PminCalcul + (PeCalcul - PminCalcul) * Power((DeltapCible  - DeltaPminCalcul) / (DeltaPeCalcul  - DeltaPminCalcul), 3/2);

//DeltaPeCalculQmin := DeltaPeCalcul;
DeltaPeCalculQmin := DeltaPminCalcul + RpCalcul;
//DeltaPeCalculQmax := DeltaPmin(QMaxCible) + RpCalcul;
PpCalculQmin := PpCalcul;



Pas := (_Caisson.Ventil_PressionConstante.QMax - _Caisson.Ventil_PressionConstante.QMin) / 10;
CourbeDebit     := TStringList.Create;
CourbeDP        := TStringList.Create;
CourbePuissance := TStringList.Create;

Ratio := -1;

        for _NoPoint := 0 to 11 - 1 do
                Begin
{
                TempDebit := _Caisson.Ventil_PressionConstante.QMin + _NoPoint * Pas;

                DeltaPminCalcul  := DeltaPmin(TempDebit);
                DeltaPeCalcul    := DeltaPe(TempDebit);
                
                PminCalcul       := Pmin(TempDebit);
                PeCalcul         := Pe(TempDebit);


                _Caisson.ListeDebitsMinis[_NoPoint] := Float2Str(TempDebit, 2);
                _Caisson.ListeDebitsMaxis[_NoPoint] := _Caisson.ListeDebitsMinis[_NoPoint];

                _Caisson.ListeDPMinis[_NoPoint] := Float2Str(DeltaPminCalcul, 2);
                _Caisson.ListeDPMaxis[_NoPoint] := Float2Str(DeltaPeCalcul, 2);

                _Caisson.ListePuissMinis[_NoPoint] := Float2Str(PminCalcul, 2);
                _Caisson.ListePuissMaxis[_NoPoint] := Float2Str(PeCalcul, 2);


                PpCalcul := PminCalcul + (PeCalcul - PminCalcul) * (DeltapCible  - DeltaPminCalcul) / (DeltaPeCalcul  - DeltaPminCalcul);

                TempDP := DeltaPminCalcul + RpCalcul ;
                TempPuissance := PpCalcul;

                CourbeDebit.Add(Float2Str(TempDebit, 0));
                CourbeDP.Add(Float2Str(TempDP, 0));
                CourbePuissance.Add(Float2Str(TempPuissance, 0));

                Ratio := (TempDP - Str2Float(_Caisson.ListeDPMinis[_NoPoint])) / (Str2Float(_Caisson.ListeDPMaxis[_NoPoint]) - Str2Float(_Caisson.ListeDPMinis[_NoPoint]));
                Ratio := Max(0, Ratio);

                CourbeDebit[_NoPoint]     := Float2Str(Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]) + (Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint]) - Str2Float(_Caisson.ListeDebitsMinis[_NoPoint])) * Ratio, 0);
                CourbeDP[_NoPoint]        := Float2Str(Str2Float(_Caisson.ListeDPMinis[_NoPoint])     + (Str2Float(_Caisson.ListeDPMaxis[_NoPoint])     - Str2Float(_Caisson.ListeDPMinis[_NoPoint])) * Ratio, 0);
                CourbePuissance[_NoPoint] := Float2Str(Str2Float(_Caisson.ListePuissMinis[_NoPoint])  + (Str2Float(_Caisson.ListePuissMaxis[_NoPoint])  - Str2Float(_Caisson.ListePuissMinis[_NoPoint])) * Ratio, 0);


}

                TempDebit := Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]);

                DeltaPminCalcul  := StrToFloat(_Caisson.ListeDPMinis[_NoPoint]);
                DeltaPeCalcul    := StrToFloat(_Caisson.ListeDPMaxis[_NoPoint]);

                PminCalcul       := StrToFloat(_Caisson.ListePuissMinis[_NoPoint]);
                PeCalcul         := StrToFloat(_Caisson.ListePuissMaxis[_NoPoint]);

//                DeltapCible := DeltaPmin(TempDebit);

//demande modif Bergeron du 10/02/2012
//                PpCalcul := PminCalcul + (PeCalcul - PminCalcul) * (DeltapCible  - DeltaPminCalcul) / (DeltaPeCalcul  - DeltaPminCalcul);
                PpCalcul := PminCalcul + (PeCalcul - PminCalcul) * Power((DeltapCible  - DeltaPminCalcul) / (DeltaPeCalcul  - DeltaPminCalcul), 3/2);

                TempDP := DeltaPminCalcul + RpCalcul ;
                TempPuissance := PpCalcul;

                CourbeDebit.Add(Float2Str(TempDebit, 0));
                CourbeDP.Add(Float2Str(TempDP, 0));
                CourbePuissance.Add(Float2Str(TempPuissance, 0));


                Ratio := (TempDP - Str2Float(_Caisson.ListeDPMinis[_NoPoint])) / (Str2Float(_Caisson.ListeDPMaxis[_NoPoint]) - Str2Float(_Caisson.ListeDPMinis[_NoPoint]));
                Ratio := Max(0, Ratio);

                CourbeDebit[_NoPoint]     := Float2Str(Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]) + (Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint]) - Str2Float(_Caisson.ListeDebitsMinis[_NoPoint])) * Ratio, 0);
                CourbeDP[_NoPoint]        := Float2Str(Str2Float(_Caisson.ListeDPMinis[_NoPoint])     + (Str2Float(_Caisson.ListeDPMaxis[_NoPoint])     - Str2Float(_Caisson.ListeDPMinis[_NoPoint])) * Ratio, 0);
//demande modif Bergeron du 10/02/2012                
//                CourbePuissance[_NoPoint] := Float2Str(Str2Float(_Caisson.ListePuissMinis[_NoPoint])  + (Str2Float(_Caisson.ListePuissMaxis[_NoPoint])  - Str2Float(_Caisson.ListePuissMinis[_NoPoint])) * Power(Ratio, 3/2), 0);
                CourbePuissance[_NoPoint] := Float2Str(Str2Float(_Caisson.ListePuissMinis[_NoPoint])  + (Str2Float(_Caisson.ListePuissMaxis[_NoPoint])  - Str2Float(_Caisson.ListePuissMinis[_NoPoint])) * Power(Ratio, 3/2), 0);



                End;


                if (Etude.Batiment.TypeBat <> c_BatimentTertiaire)  and CheckBox_DTU.Checked  then
                        if Str2Float(CourbeDebit[0]) > QMinCible then
                                exit;

        if Str2Float(CourbeDebit[CourbeDebit.Count - 1]) < QMaxCible then
                exit;


if DeltaPmin(QMinCible) > DeltaPeCalculQmin then exit;
if DeltaPe(QMinCible) < DeltaPeCalculQmin then exit;

if DeltaPmin(QMaxCible) > DeltaPeCalculQmax then exit;
if DeltaPe(QMaxCible) < DeltaPeCalculQmax then exit;



Result := True;
_Caisson.Selection11Points :=  RadioBezier.ItemIndex = 1;
_Caisson.DpCalcQMax  := DeltaPeCalculQmax;
_Caisson.DpCalcQMin  := DeltaPeCalculQmin;

_Caisson.Position := '';

_Caisson.CoeffRT := CaissonReseau.CoeffRt(_Caisson, CourbeDebit, CourbePuissance);
//_Caisson.CoeffRT := CaissonReseau.CoeffRt(_Caisson);
//_Caisson.Position := 'Pos ' + _Caisson.Position;

_Caisson.PuissCalcQMax  := PpCalculQmax;        
_Caisson.PuissCalcQMin  := PpCalculQmin;


_Caisson.CourbeDpCalc    := CourbeDP;
_Caisson.CourbeDebitCalc := CourbeDebit;
_Caisson.CourbePuissCalc := CourbePuissance;




End;
{ **************************************************************************************************************************************************** }
Function  TDlg_SelectionCaissonGenerique.Caisson_Valide_SelectionPlageFonctionnementEtCourbeLimite(_Caisson: TEdibatec_Caisson) : Boolean;
Var
        CaissonValide : Boolean;
        DpCourbeAQmin: Real;
        DpCourbeAQmax: Real;
        m_PointQMinDpMax       : TPointDebitPression;
        m_PointQMaxDpMax       : TPointDebitPression;
        PtsBezier              : TPointsControleBezier;
Begin
Result := Caisson_Valide_SelectionPlageFonctionnement(_Caisson);

        if Result = false then
                exit;

m_PointQMinDpMax.Q  := Etude.Batiment.Reseaux.GetCaisson.DebitMini;
m_PointQMinDpMax.DP := CaissonReseau.DPVentilateur.MaxAQmin;
m_PointQMaxDpMax.Q  := Etude.Batiment.Reseaux.GetCaisson.DebitMaxi;
m_PointQMaxDpMax.DP := CaissonReseau.DPVentilateur.MaxAQmax;

//modif dtu
//        if not CheckBox_DTU.Checked then
                Begin
                m_PointQMinDpMax.DP := m_PointQMinDpMax.DP - 20;
                m_PointQMaxDpMax.DP := m_PointQMaxDpMax.DP - 20;
                End;

PtsBezier := StringToPointsBezier_11points(_Caisson.ListeDebitsLimites, _Caisson.ListeDPLimites);
DpCourbeAQMax := CalculDpCourbe(m_PointQMaxDpMax, PtsBezier);
DpCourbeAQMin := CalculDpCourbe(m_PointQMinDpMax, PtsBezier);

CaissonValide :=  Not IsNan(DpCourbeAQMax) And (DpCourbeAQMax <> 0) and (_Caisson.DpCalcQMax <= DpCourbeAQMax);
        if (Etude.Batiment.TypeBat <> c_BatimentTertiaire) then
                CaissonValide :=  CaissonValide and (_Caisson.DpCalcQMin >= DpCourbeAQMin);

        if CaissonValide then
                Begin
                Result := true;
                _Caisson.DpCalcQMax  := DpCourbeAQMax;
                _Caisson.DpCalcQMin  := DpCourbeAQMin;

                _Caisson.CourbeDpCalc    := _Caisson.ListeDPLimites;
                _Caisson.CourbeDebitCalc := _Caisson.ListeDebitsLimites;
//manque la courbe PuissancesLimites 
                _Caisson.CourbePuissCalc := _Caisson.ListePuissMinis;
                End;
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaissonGenerique.Caisson_Valide_Fonctionnement(_Caisson: TEdibatec_Caisson) : Boolean;
Begin
  Case _Caisson.TypeSelection Of
    c_CaissonSelect_3Courbes            : Result := Caisson_Valide_Selection3Courbes(_Caisson);
    c_CaissonSelect_PageFct             : Result := Caisson_Valide_SelectionPlageFonctionnement(_Caisson);
    c_CaissonSelect_DebitCst            : Result := Caisson_Valide_SelectionDebitConstant(_Caisson);
    c_CaissonSelect_PlageEtCourbeLimite : Result := Caisson_Valide_SelectionPlageFonctionnementEtCourbeLimite(_Caisson);
    Else Result := False;
  End;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaissonGenerique.AffDebitsCaisson(_Caisson: TVentil_Caisson);
var
        AfficheMinAQMin : Real;
        AfficheMinAQMax : Real;
Begin
        if Etude.Batiment.TypeBat = c_BatimentTertiaire then
                Label_DebitMini.Caption := '--- m�/h'
        else
                Label_DebitMini.Caption := Float2Str(_Caisson.Reseau_DebitMinimum, 0) + ' m�/h';

AfficheMinAQMin := _Caisson.DPVentilateur.MinAQmin;
AfficheMinAQMax := _Caisson.DPVentilateur.MinAQmax;
//modif dtu
//        if not CheckBox_DTU.Checked then
                Begin
                AfficheMinAQMin := AfficheMinAQMin - 20;
                AfficheMinAQMax := AfficheMinAQMax - 20;
                End;

  Label_DebitMaxi.Caption := Float2Str(_Caisson.Reseau_DebitMaximum, 0) + ' m�/h';
  Label_DpMax.Caption := Float2Str(AfficheMinAQMin, 0) + ' Pa';
  Label_DpMin.Caption := Float2Str(AfficheMinAQMax, 0) + ' Pa';
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaissonGenerique.Grid_SelectionSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
Begin
  CaissonSelection := TEdibatec_Caisson(Grid_Selection.Objects[0, ARow]);
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaissonGenerique.Grid_SelectionRowChanging(Sender: TObject; OldRow, NewRow: Integer; var Allow: Boolean);
Begin
  If Grid_Selection.Objects[0, NewRow] <> Nil Then Begin
    If Not Label_DebitMini.Visible Then Label_DebitMini.Show;
    If Not Label_DebitMaxi.Visible Then Label_DebitMaxi.Show;
  End Else Begin
    Label_DebitMini.Hide;
    Label_DebitMaxi.Hide;
  End;
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaissonGenerique.IsProduitASSOPAPV(m_ProduitAsso : TEdibatec_Association) : Boolean;
var
        m_NoPAPV : Integer;
Begin
result := false;
if m_ProduitAsso = nil then exit;

        for m_NoPAPV := Low(Tab_PAPV) to High(Tab_PAPV) do
                If m_ProduitAsso.VraiProduit.CodeProduit = Tab_PAPV[m_NoPAPV] Then
                        Begin
                        Result := true;
                        exit;
                        End;

End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaissonGenerique.IsCaissonSelectionFromGammeHUCF : boolean;
Begin
result := false;
if CaissonSelection = nil then exit;

        if Copy(CaissonSelection.CodeProduit, 1, 12) = cst_CaissonGammeHUCF then
                result := true;
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaissonGenerique.IsCaissonSelectionFromGammeJBEBorJBHBorJBEBECO(_Caisson: TEdibatec_Caisson) : boolean;
Begin
result := false;
if _Caisson = nil then exit;

        if (Copy(_Caisson.CodeProduit, 1, 12) = cst_CaissonGammeJBEBJBHBJBEBECO1) or (Copy(_Caisson.CodeProduit, 1, 12) = cst_CaissonGammeJBEBJBHBJBEBECO2) then
                result := true;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaissonGenerique.AffecteAssociations;
Var
//  I: Integer;
  m_ProduitAsso: TEdibatec_Association;
  m_NoAsso     : Integer;
  m_CurRow     : Integer;
  m_Col, m_Row : Integer;
Begin
  CaissonSelection.GetAssociation;
  For m_Row := 1 To Grid_Accessoires.RowCount - 1 Do Begin
   Grid_Accessoires.Objects[0, m_Row] := nil;
   For m_Col := 0 To Grid_Accessoires.ColCount - 1 Do Grid_Accessoires.Cells[m_Col, m_Row] := '';
  End;
  Grid_Accessoires.RowCount := 2;
  m_CurRow := 1;
  For m_NoAsso := 0 To CaissonSelection.Associations.Count - 1 Do Begin
    m_ProduitAsso := TEdibatec_Association(CaissonSelection.Associations.Produit(m_NoAsso));
    If m_CurRow >= Grid_Accessoires.RowCount Then Grid_Accessoires.RowCount := Grid_Accessoires.RowCount + 1;
    Grid_Accessoires.Objects[0, m_CurRow] := m_ProduitAsso;
    Grid_Accessoires.Cells[0, m_CurRow] := m_ProduitAsso.ProduitAssocie;
    Grid_Accessoires.Cells[1, m_CurRow] := m_ProduitAsso.RefProduitAssocie;
    Grid_Accessoires.Ints[2, m_CurRow]  := 0;
    Inc(m_CurRow);
  End;

//  Etude.SaveOptions_SelectionCaisson.InitOptionsAccessoiresAssociesFiche(Dlg_SelectionCaisson);

        if IsCaissonSelectionFromGammeHUCF and CaissonReseau.HasSortieToit then
                For m_NoAsso := 1 To Grid_Accessoires.RowCount - 1 do
                        if (Grid_Accessoires.Objects[0, m_NoAsso] <> nil) and IsProduitASSOPAPV(TEdibatec_Association(Grid_Accessoires.Objects[0, m_NoAsso])) then
                                Grid_Accessoires.Ints[2, m_NoAsso]  := max(1, Grid_Accessoires.Ints[2, m_NoAsso]);

End;
{ **************************************************************************************************************************************************** }
procedure TDlg_SelectionCaissonGenerique.Grid_SelectionCanEditCell(Sender: TObject;
  ARow, ACol: Integer; var CanEdit: Boolean);
begin
  CanEdit := False
end;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaissonGenerique.Grid_InfosCaissonCanEditCell(Sender: TObject; ARow, ACol: Integer; var CanEdit: Boolean);
Begin
  CanEdit := (Grid_Accessoires.Objects[0, Arow] <> nil) And (ACol = 2);
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaissonGenerique.Radio_ColliersClick(Sender: TObject);
Begin
  Radio_ColliersSupp.Enabled := Radio_Colliers.ItemIndex > 0;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaissonGenerique.Grid_ColliersSuppIsFixedCell(Sender: TObject; ARow, ACol: Integer; var IsFixed: Boolean);
Begin
  IsFixed := (ACol In [0, 2, 4, 6]) Or ( (ACol = 7) And (ARow In [3,4])) ;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaissonGenerique.Grid_BavettesIsFixedCell(Sender: TObject; ARow, ACol: Integer; var IsFixed: Boolean);
Begin
  IsFixed := (ACol In [0, 2, 4, 6]);
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaissonGenerique.CheckBox_UniverselClick(Sender: TObject);
Begin
  Grid_ColliersSupp.Enabled := CheckBox_Universel.Checked;
  Edit_NbSuppUni.Visible := CheckBox_Universel.Checked;
  CheckBox_ColliersSuppIsoles.Visible := CheckBox_Universel.Checked;;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaissonGenerique.CheckBox_125100Click(Sender: TObject);
Begin

  CheckBox_BandePerf.Enabled := true;

  Edit_NbSupp125100.Visible  := CheckBox_125100.Checked;
  Edit_NbSupp160100.Visible  := CheckBox_160100.Checked;
  Edit_NbSupp125300.Visible  := CheckBox_125300.Checked;
  Edit_NbSupp160300.Visible  := CheckBox_160300.Checked;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaissonGenerique.CheckBox_BandePerfClick(Sender: TObject);
Begin
  Edit_NbRlxBandePerforee.Visible := CheckBox_BandePerf.Checked;
  StaticText1.Visible             := CheckBox_BandePerf.Checked;
  Text_LongueurBandePerf.Visible  := CheckBox_BandePerf.Checked;
End;
{ **************************************************************************************************************************************************** }
procedure TDlg_SelectionCaissonGenerique.CheckBox_NonAgreeClick(Sender: TObject);
begin

        if (Etude.Batiment.TypeBat = c_BatimentTertiaire) then
                if ( Etude.Batiment.TypeTertiaire = c_TertiaireVMC) and NonAgreePossible then
                        Begin
                                if not (CheckBox_40012.Checked) then
                                        Begin
                                        CheckBox_NonAgree.Checked := True;
                                        CheckBox_NonAgree.Enabled := False;
                                        End
                                Else
                                        CheckBox_NonAgree.Enabled := True;
                        End

end;
{ **************************************************************************************************************************************************** }
Constructor TSelectionCaisson_SaveOptions.Create;
Begin
CanGet := false;
End;
{ **************************************************************************************************************************************************** }
Procedure TSelectionCaisson_SaveOptions.Sauvegarder(_Donnees : TClientDataSet);
var
        TempString : String;
        cpt        : Integer;
Begin

        With _Donnees Do
                Begin
                FieldByName(UpperCase('SC_CanGet')).AsInteger := Bool2Int(CanGet);
                FieldByName(UpperCase('SC_OldReferenceCaisson')).AsWideString := Trim(OldReferenceCaisson);
                //Crit�res De S�lection Du Ventilateur
                FieldByName(UpperCase('SC_CB_Caisson_Checked')).AsInteger := Bool2Int(CheckBox_Caisson_Checked);
                FieldByName(UpperCase('SC_CB_Tourelle_Checked')).AsInteger := Bool2Int(CheckBox_Tourelle_Checked);
                FieldByName(UpperCase('SC_CB_Direct_Checked')).AsInteger := Bool2Int(CheckBox_Direct_Checked);
                FieldByName(UpperCase('SC_CB_Poulie_Checked')).AsInteger := Bool2Int(CheckBox_Poulie_Checked);
                FieldByName(UpperCase('SC_CB_UneVitesse_Checked')).AsInteger := Bool2Int(CheckBox_UneVitesse_Checked);
                FieldByName(UpperCase('SC_CBDeuxVitesses_Checked')).AsInteger := Bool2Int(CheckBoxDeuxVitesses_Checked);
                FieldByName(UpperCase('SC_CB_NonAgree_Checked')).AsInteger := Bool2Int(CheckBox_NonAgree_Checked);
                FieldByName(UpperCase('SC_CB_40012_Checked')).AsInteger := Bool2Int(CheckBox_40012_Checked);
                FieldByName(UpperCase('SC_CB_MonoPhase_Checked')).AsInteger := Bool2Int(CheckBox_MonoPhase_Checked);
                FieldByName(UpperCase('SC_CB_TriPhase_Checked')).AsInteger := Bool2Int(CheckBox_TriPhase_Checked);
//                FieldByName(UpperCase('SC_R_Depressostat_II')).AsInteger := Radio_Depressostat_ItemIndex;
//                FieldByName(UpperCase('SC_R_VentilEco_II')).AsInteger := Radio_VentilEco_ItemIndex;
//                FieldByName(UpperCase('SC_R_EcoEtDepr')).AsInteger := Radio_EcoEtDepr_ItemIndex;
                FieldByName(UpperCase('SC_CB_ECO_Checked')).AsInteger := Bool2Int(CheckBoxEco_Checked);
                FieldByName(UpperCase('SC_CB_NECOADEPR_Checked')).AsInteger := Bool2Int(CheckBoxNEcoADepr_Checked);
                FieldByName(UpperCase('SC_CB_NECOSDEPR_Checked')).AsInteger := Bool2Int(CheckBoxNEcoSDepr_Checked);

                FieldByName(UpperCase('SC_CB_DTU_Checked')).AsInteger := Bool2Int(CheckBox_DTU_Checked);
                FieldByName(UpperCase('SC_RadioBezier_ItemIndex')).AsInteger := RadioBezier_ItemIndex;
                //Accessoire Associ�s Au Ventilateur
                        TempString := '';
                                for cpt := 0 to Length(ListAccessoireVentilo_Qte) - 1 do
                                 Begin
                                 TempString := TempString + IntToStr(ListAccessoireVentilo_Qte[cpt]);
                                        if cpt <> Length(ListAccessoireVentilo_Qte) then
                                                TempString := TempString + '|';
                                 End;
                        FieldByName(UpperCase('SC_ListAccessoireVentilo_Qte')).AsWideString := TempString;
                //Accessoires Annexes
                //-->Montage
                FieldByName(UpperCase('SC_Radio_Adhesif_ItemIndex')).AsInteger := Radio_Adhesif_ItemIndex;
                FieldByName(UpperCase('SC_E_Vis13_Text')).AsWideString := Edit_Vis13_Text;
                FieldByName(UpperCase('SC_E_Vis19_Text')).AsWideString := Edit_Vis19_Text;
                FieldByName(UpperCase('SC_E_M0_Text')).AsWideString := Edit_M0_Text;
                FieldByName(UpperCase('SC_E_M1_Text')).AsWideString := Edit_M1_Text;
                FieldByName(UpperCase('SC_Check_IsolantMur_Checked')).AsInteger := Bool2Int(Check_IsolantMur_Checked);
                FieldByName(UpperCase('SC_Check_Fourreaux_Checked')).AsInteger := Bool2Int(Check_Fourreaux_Checked);
                //-->Bavettes et colliers
                FieldByName(UpperCase('SC_Radio_Colliers_ItemIndex')).AsInteger := Radio_Colliers_ItemIndex;
                FieldByName(UpperCase('SC_Bavette_OUINON')).AsInteger  := Bool2Int(Bavette_OuiNon);
                FieldByName(UpperCase('SC_Bavette_125_Qte')).AsInteger := Bavette_125_Qte;
                FieldByName(UpperCase('SC_Bavette_160_Qte')).AsInteger := Bavette_160_Qte;
                FieldByName(UpperCase('SC_Bavette_200_Qte')).AsInteger := Bavette_200_Qte;
                FieldByName(UpperCase('SC_Bavette_250_Qte')).AsInteger := Bavette_250_Qte;
                FieldByName(UpperCase('SC_Bavette_315_Qte')).AsInteger := Bavette_315_Qte;
                FieldByName(UpperCase('SC_Bavette_355_Qte')).AsInteger := Bavette_355_Qte;
                FieldByName(UpperCase('SC_Bavette_400_Qte')).AsInteger := Bavette_400_Qte;
                FieldByName(UpperCase('SC_Bavette_450_Qte')).AsInteger := Bavette_450_Qte;
                FieldByName(UpperCase('SC_Bavette_500_Qte')).AsInteger := Bavette_500_Qte;
                FieldByName(UpperCase('SC_Bavette_560_Qte')).AsInteger := Bavette_560_Qte;
                FieldByName(UpperCase('SC_Bavette_630_Qte')).AsInteger := Bavette_630_Qte;
                FieldByName(UpperCase('SC_Bavette_710_Qte')).AsInteger := Bavette_710_Qte;
                //-->Supports Universels
                FieldByName(UpperCase('SC_CB_Universel_Checked')).AsInteger := Bool2Int(CheckBox_Universel_Checked);
                FieldByName(UpperCase('SC_CB_125300_Checked')).AsInteger := Bool2Int(CheckBox_125300_Checked);
                FieldByName(UpperCase('SC_CB_160300_Checked')).AsInteger := Bool2Int(CheckBox_160300_Checked);
                FieldByName(UpperCase('SC_CB_125100_Checked')).AsInteger := Bool2Int(CheckBox_125100_Checked);
                FieldByName(UpperCase('SC_CB_160100_Checked')).AsInteger := Bool2Int(CheckBox_160100_Checked);
                FieldByName(UpperCase('SC_CB_ColliersSI_Checked')).AsInteger := Bool2Int(CheckBox_ColliersSuppIsoles_Checked);
                FieldByName(UpperCase('SC_E_NbSupp160300_Text')).AsWideString := Edit_NbSupp160300_Text;
                FieldByName(UpperCase('SC_E_NbSupp125300_Text')).AsWideString := Edit_NbSupp125300_Text;
                FieldByName(UpperCase('SC_E_NbSupp160100_Text')).AsWideString := Edit_NbSupp160100_Text;
                FieldByName(UpperCase('SC_E_NbSupp125100_Text')).AsWideString := Edit_NbSupp125100_Text;
                FieldByName(UpperCase('SC_E_NbSuppUni_Text')).AsWideString := Edit_NbSuppUni_Text;
                FieldByName(UpperCase('SC_CB_BandePerf_Checked')).AsInteger := Bool2Int(CheckBox_BandePerf_Checked);
                FieldByName(UpperCase('SC_E_NbRlxBandePerforee_Text')).AsWideString := Edit_NbRlxBandePerforee_Text;
                FieldByName(UpperCase('SC_Collier_125_Qte')).AsInteger := Collier_125_Qte;
                FieldByName(UpperCase('SC_Collier_160_Qte')).AsInteger := Collier_160_Qte;
                FieldByName(UpperCase('SC_Collier_200_Qte')).AsInteger := Collier_200_Qte;
                FieldByName(UpperCase('SC_Collier_250_Qte')).AsInteger := Collier_250_Qte;
                FieldByName(UpperCase('SC_Collier_315_Qte')).AsInteger := Collier_315_Qte;
                FieldByName(UpperCase('SC_Collier_355_Qte')).AsInteger := Collier_355_Qte;
                FieldByName(UpperCase('SC_Collier_400_Qte')).AsInteger := Collier_400_Qte;
                FieldByName(UpperCase('SC_Collier_450_Qte')).AsInteger := Collier_450_Qte;
                FieldByName(UpperCase('SC_Collier_500_Qte')).AsInteger := Collier_500_Qte;
                FieldByName(UpperCase('SC_Collier_560_Qte')).AsInteger := Collier_560_Qte;
                FieldByName(UpperCase('SC_Collier_630_Qte')).AsInteger := Collier_630_Qte;
                FieldByName(UpperCase('SC_Collier_710_Qte')).AsInteger := Collier_710_Qte;
                FieldByName(UpperCase('SC_Collier_800_Qte')).AsInteger := Collier_800_Qte;
                FieldByName(UpperCase('SC_Collier_900_Qte')).AsInteger := Collier_900_Qte;
                End;

End;
{ **************************************************************************************************************************************************** }
Procedure TSelectionCaisson_SaveOptions.Ouvrir(_Donnees : TClientDataSet);
var
        TempStringList : TStringList;
        cpt            : integer;
Begin

        With _Donnees Do
                Begin
                CanGet := Int2Bool(FieldByName(UpperCase('SC_CanGet')).AsInteger);
                OldReferenceCaisson := Trim(FieldByName(UpperCase('SC_OldReferenceCaisson')).AsWideString);
                //Crit�res De S�lection Du Ventilateur
                CheckBox_Caisson_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_Caisson_Checked')).AsInteger);
                CheckBox_Tourelle_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_Tourelle_Checked')).AsInteger);
                CheckBox_Direct_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_Direct_Checked')).AsInteger);
                CheckBox_Poulie_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_Poulie_Checked')).AsInteger);
                CheckBox_UneVitesse_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_UneVitesse_Checked')).AsInteger);
                CheckBoxDeuxVitesses_Checked := Int2Bool(FieldByName(UpperCase('SC_CBDeuxVitesses_Checked')).AsInteger);
                CheckBox_NonAgree_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_NonAgree_Checked')).AsInteger);
                CheckBox_40012_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_40012_Checked')).AsInteger);
                CheckBox_MonoPhase_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_MonoPhase_Checked')).AsInteger);
                CheckBox_TriPhase_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_TriPhase_Checked')).AsInteger);
                try
//                Radio_Depressostat_ItemIndex := FieldByName(UpperCase('SC_R_Depressostat_II')).AsInteger;
//                Radio_VentilEco_ItemIndex := FieldByName(UpperCase('SC_R_VentilEco_II')).AsInteger;
//                Radio_EcoEtDepr_ItemIndex := FieldByName(UpperCase('SC_R_EcoEtDepr')).AsInteger;
                CheckBoxEco_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_ECO_Checked')).AsInteger);
                CheckBoxNEcoADepr_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_NECOADEPR_Checked')).AsInteger);
                CheckBoxNEcoSDepr_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_NECOSDEPR_Checked')).AsInteger);
                except
                //permet de r�cup�rer les anciennes sauvegardes
//                Radio_Depressostat_ItemIndex := FieldByName(UpperCase('SC_CB_Depressostat_Checked')).AsInteger + 1;
//                Radio_VentilEco_ItemIndex := FieldByName(UpperCase('SC_CB_VentilEco_Checked')).AsInteger + 1;

                end;
                CheckBox_DTU_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_DTU_Checked')).AsInteger);
                RadioBezier_ItemIndex := FieldByName(UpperCase('SC_RadioBezier_ItemIndex')).AsInteger;
                //Accessoire Associ�s Au Ventilateur
                        TempStringList := TStringList.Create;
                        TempStringList:=  StrToStringList(StringReplace(FieldByName(UpperCase('SC_ListAccessoireVentilo_Qte')).AsWideString, ' ', '', [rfReplaceAll, rfIgnoreCase]), '|');
                        SetLength(ListAccessoireVentilo_Qte, TempStringList.Count);
                                for cpt := 0 to TempStringList.Count - 1 do
                                        ListAccessoireVentilo_Qte[cpt] := Str2Int(TempStringList[cpt]);
                        FreeAndNil(TempStringList);
                //Accessoires Annexes
                //-->Montage
                Radio_Adhesif_ItemIndex := FieldByName(UpperCase('SC_Radio_Adhesif_ItemIndex')).AsInteger;
                Edit_Vis13_Text := FieldByName(UpperCase('SC_E_Vis13_Text')).AsWideString;
                Edit_Vis19_Text := FieldByName(UpperCase('SC_E_Vis19_Text')).AsWideString;
                Edit_M0_Text := FieldByName(UpperCase('SC_E_M0_Text')).AsWideString;
                Edit_M1_Text := FieldByName(UpperCase('SC_E_M1_Text')).AsWideString;
                Check_IsolantMur_Checked := Int2Bool(FieldByName(UpperCase('SC_Check_IsolantMur_Checked')).AsInteger);
                Check_Fourreaux_Checked := Int2Bool(FieldByName(UpperCase('SC_Check_Fourreaux_Checked')).AsInteger);
                //-->Bavettes et colliers
                Radio_Colliers_ItemIndex := FieldByName(UpperCase('SC_Radio_Colliers_ItemIndex')).AsInteger;
                Bavette_OuiNon  := Int2Bool(FieldByName(UpperCase('SC_Bavette_OUINON')).AsInteger);
                Bavette_125_Qte := FieldByName(UpperCase('SC_Bavette_125_Qte')).AsInteger;
                Bavette_160_Qte := FieldByName(UpperCase('SC_Bavette_160_Qte')).AsInteger;
                Bavette_200_Qte := FieldByName(UpperCase('SC_Bavette_200_Qte')).AsInteger;
                Bavette_250_Qte := FieldByName(UpperCase('SC_Bavette_250_Qte')).AsInteger;
                Bavette_315_Qte := FieldByName(UpperCase('SC_Bavette_315_Qte')).AsInteger;
                Bavette_355_Qte := FieldByName(UpperCase('SC_Bavette_355_Qte')).AsInteger;
                Bavette_400_Qte := FieldByName(UpperCase('SC_Bavette_400_Qte')).AsInteger;
                Bavette_450_Qte := FieldByName(UpperCase('SC_Bavette_450_Qte')).AsInteger;
                Bavette_500_Qte := FieldByName(UpperCase('SC_Bavette_500_Qte')).AsInteger;
                Bavette_560_Qte := FieldByName(UpperCase('SC_Bavette_560_Qte')).AsInteger;
                Bavette_630_Qte := FieldByName(UpperCase('SC_Bavette_630_Qte')).AsInteger;
                Bavette_710_Qte := FieldByName(UpperCase('SC_Bavette_710_Qte')).AsInteger;
                //-->Supports Universels
                CheckBox_Universel_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_Universel_Checked')).AsInteger);
                CheckBox_125300_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_125300_Checked')).AsInteger);
                CheckBox_160300_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_160300_Checked')).AsInteger);
                CheckBox_125100_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_125100_Checked')).AsInteger);
                CheckBox_160100_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_160100_Checked')).AsInteger);
                CheckBox_ColliersSuppIsoles_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_ColliersSI_Checked')).AsInteger);
                Edit_NbSupp160300_Text := FieldByName(UpperCase('SC_E_NbSupp160300_Text')).AsWideString;
                Edit_NbSupp125300_Text := FieldByName(UpperCase('SC_E_NbSupp125300_Text')).AsWideString;
                Edit_NbSupp160100_Text := FieldByName(UpperCase('SC_E_NbSupp160100_Text')).AsWideString;
                Edit_NbSupp125100_Text := FieldByName(UpperCase('SC_E_NbSupp125100_Text')).AsWideString;
                Edit_NbSuppUni_Text := FieldByName(UpperCase('SC_E_NbSuppUni_Text')).AsWideString;
                CheckBox_BandePerf_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_BandePerf_Checked')).AsInteger);
                Edit_NbRlxBandePerforee_Text := FieldByName(UpperCase('SC_E_NbRlxBandePerforee_Text')).AsWideString;
                Collier_125_Qte := FieldByName(UpperCase('SC_Collier_125_Qte')).AsInteger;
                Collier_160_Qte := FieldByName(UpperCase('SC_Collier_160_Qte')).AsInteger;
                Collier_200_Qte := FieldByName(UpperCase('SC_Collier_200_Qte')).AsInteger;
                Collier_250_Qte := FieldByName(UpperCase('SC_Collier_250_Qte')).AsInteger;
                Collier_315_Qte := FieldByName(UpperCase('SC_Collier_315_Qte')).AsInteger;
                Collier_355_Qte := FieldByName(UpperCase('SC_Collier_355_Qte')).AsInteger;
                Collier_400_Qte := FieldByName(UpperCase('SC_Collier_400_Qte')).AsInteger;
                Collier_450_Qte := FieldByName(UpperCase('SC_Collier_450_Qte')).AsInteger;
                Collier_500_Qte := FieldByName(UpperCase('SC_Collier_500_Qte')).AsInteger;
                Collier_560_Qte := FieldByName(UpperCase('SC_Collier_560_Qte')).AsInteger;
                Collier_630_Qte := FieldByName(UpperCase('SC_Collier_630_Qte')).AsInteger;
                Collier_710_Qte := FieldByName(UpperCase('SC_Collier_710_Qte')).AsInteger;
                Collier_800_Qte := FieldByName(UpperCase('SC_Collier_800_Qte')).AsInteger;
                Collier_900_Qte := FieldByName(UpperCase('SC_Collier_900_Qte')).AsInteger;
                End;

End;

{ **************************************************************************************************************************************************** }
end.
