Unit Ventil_EdibatecChoixGamme;
                
{$Include 'Ventil_Directives.pas'}

Interface

Uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls,

  Ventil_EdibatecProduits;

Type
  TFic_SelectionGamme = class(TForm)
    Bevel1: TBevel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    ListBox_Gammes: TListBox;
  private
    Procedure FillListes(_CodeGamme: TEdibatec_CodeGamme);
  public
    { Public declarations }
  end;

Var
  Fic_SelectionGamme: TFic_SelectionGamme;

Function SelectionDuneGamme(_CodeGamme: TEdibatec_CodeGamme): TEdibatec_CodeGamme;

Implementation
Uses
  Ventil_Declarations,
  Ventil_Edibatec;

{$R *.dfm}

{ ************************************************************************************************************************************************* }
Function SelectionDuneGamme(_CodeGamme: TEdibatec_CodeGamme): TEdibatec_CodeGamme;
Begin
  Result := _CodeGamme;
  Application.CreateForm(TFic_SelectionGamme, Fic_SelectionGamme);
  Fic_SelectionGamme.FillListes(_CodeGamme);
  if Fic_SelectionGamme.ShowModal = id_Ok Then
    Result := TEdibatec_Gamme(Fic_SelectionGamme.ListBox_Gammes.Items.Objects[Fic_SelectionGamme.ListBox_Gammes.ItemIndex]).CodeGamme
  Else If Length(_CodeGamme) = 12 Then Result := _CodeGamme
                                  Else Result := '';
  FreeAndNil(Fic_SelectionGamme);
End;
{ ************************************************************************************************************************************************* }

{ ************************************************************************************************************************************************* }
{ TFic_SelectionGamme }
Procedure TFic_SelectionGamme.FillListes(_CodeGamme: TEdibatec_CodeGamme);
Var
  m_Classe : TEdibatec_Classe;
  m_NoGamme: Integer;
  m_Select : Boolean;
Begin
  m_Classe := BaseEdibatec.Classe(Copy(_CodeGamme, 1, 7));
  m_Select := False;
  Caption := 'S�lection: ' + m_Classe.Reference;
  For m_NoGamme := 0 To m_Classe.Gammes.Count - 1 Do Begin
    ListBox_Gammes.AddItem(m_Classe.Gammes.Gamme(m_NoGamme).Reference, m_Classe.Gammes.Gamme(m_NoGamme));
    If m_Classe.Gammes.Gamme(m_NoGamme).CodeGamme = Copy(_CodeGamme, 1, 12) Then Begin
      ListBox_Gammes.ItemIndex := m_NoGamme;
      m_Select := True;
    End;
  End;
  If Not m_Select And (ListBox_Gammes.Count > 0) Then ListBox_Gammes.ItemIndex := 0;
End;
{ ************************************************************************************************************************************************* }

end.
