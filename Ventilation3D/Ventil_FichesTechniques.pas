Unit Ventil_FichesTechniques;
         
{$Include 'Ventil_Directives.pas'}

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Db,
  Dialogs, OleCtrls, SHDocVw, ComCtrls, StdCtrls, Buttons, ExtCtrls,
  AccesBdd, ImgList, System.ImageList;

Type
  TFic_FTK = class(TForm)
    Panel3: TPanel;
    BitBtn1: TBitBtn;
    Tree: TTreeView;
    Browser_Ftk: TWebBrowser;
    ImageList1: TImageList;
    procedure TreeChange(Sender: TObject; Node: TTreeNode);
  Private
    Function Initialisation: Boolean;
    Procedure FillCategorie(Const _Attach2: TTreeNode; Const _TableDonnees: TTableDeDonneesBBS);
  Public
    { D�clarations publiques }
  End;

Var
  Fic_FTK: TFic_FTK;

  Procedure Consultation_FichesTech;

Implementation
Uses
  BbsgFonc,
  Ventil_Firebird, Ventil_Utils;

{$R *.dfm}

{ ************************************************************************************************************************************************** }
Procedure Consultation_FichesTech;
Begin
  Application.CreateForm(TFic_FTK, Fic_FTK);
  If Fic_FTK.Initialisation Then Fic_FTK.ShowModal
                            Else MessageDlg('Aucune fiche technique n''est disponible', mtError, [mbOK], 0);
  FreeAndNil(Fic_FTK);
End;
{ ************************************************************************************************************************************************** }
{ ******************************************************************** TFic_FTK ******************************************************************** }
{ ************************************************************************************************************************************************** }
Procedure TFic_FTK.FillCategorie(const _Attach2: TTreeNode; const _TableDonnees: TTableDeDonneesBBS);
Var
  CurNode    : TTreeNode;
  Stream     : TMemoryStream;
Begin
  _TableDonnees.Ouvrir;
  _TableDonnees.Donnees.First;
  While Not _TableDonnees.Donnees.Eof Do Begin
    Stream := TMemoryStream.Create;
    (_TableDonnees.Donnees.FieldByName('FTK') As TBlobField).SaveToStream(Stream);
    CurNode := Tree.Items.AddChildObjectFirst(_Attach2, _TableDonnees.Donnees.FieldByName('NOM').AsString, Stream);
    CurNode.ImageIndex := 2;
    _TableDonnees.Donnees.Next;
  End;
  _TableDonnees.Fermer;

  if _Attach2.Count = 0 then
        _Attach2.Delete;
End;
{ ************************************************************************************************************************************************** }
Function TFic_FTK.Initialisation: Boolean;
Var
  PereFTK        : TTreeNode;
  PereBouches    : TTreeNode;
  PereAutres     : TTreeNode;
  PereEA         : TTreeNode;
  PereSilencieux : TTreeNode;
  PereCaissons   : TTreeNode;
  PerePriseRejet : TTreeNode;
  PereGrilles    : TTreeNode;
  PereComposants : TTreeNode;
Begin
  Result := IsNotNil(Base_FTK);
  If Result Then Begin
    PereFTK     := Tree.Items.AddChildObject(nil, 'Fiches techniques', Nil); PereFTK.ImageIndex := 1;
    PereBouches := Tree.Items.AddChildObject(PereFTK, 'Bouches d''extraction', Nil); PereBouches.ImageIndex := 1;
    PereEA      := Tree.Items.AddChildObject(PereFTK, 'Entr�es d''air', Nil); PereEA.ImageIndex := 1;
    PereAutres  := Tree.Items.AddChildObject(PereFTK, 'Autres', Nil); PereAutres.ImageIndex := 1;
    PereSilencieux  := Tree.Items.AddChildObject(PereFTK, 'Silencieux pour entr�es d''air', Nil); PereSilencieux.ImageIndex := 1;
    PereCaissons := Tree.Items.AddChildObject(PereFTK, 'Caissons de ventilation', Nil); PereCaissons.ImageIndex := 1;
    PerePriseRejet := Tree.Items.AddChildObject(PereFTK, 'Prises et rejets d''air', Nil); PerePriseRejet.ImageIndex := 1;
    PereGrilles := Tree.Items.AddChildObject(PereFTK, 'Grilles', Nil); PereGrilles.ImageIndex := 1;
    PereComposants := Tree.Items.AddChildObject(PereFTK, 'Composants de r�seaux', Nil); PereComposants.ImageIndex := 1;
    FillCategorie(PereBouches    , Table_FTK_Bouches);
    FillCategorie(PereEA         , Table_FTK_Entrees);
    FillCategorie(PereAutres     , Table_FTK_Autres);
    FillCategorie(PereSilencieux , Table_FTK_Silencieux);
    FillCategorie(PereCaissons   , Table_FTK_Caissons);
    FillCategorie(PerePriseRejet   , Table_FTK_PriseRejet);
    FillCategorie(PereGrilles   , Table_FTK_Grilles);
    FillCategorie(PereComposants   , Table_FTK_Composants);

    Tree.FullExpand;
    Tree.AlphaSort(True);
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TFic_FTK.TreeChange(Sender: TObject; Node: TTreeNode);
Var
  NomFic: String;
Begin
  If Node.Data <> nil Then Begin
    NomFic := TempRep + IntToStr(Random(1000)) + 'FTK.pdf';
    TMemoryStream(Node.Data).SaveToFile(NomFic);
    Browser_Ftk.Navigate2(NomFic);
  End Else Browser_Ftk.Navigate2(ExtractFilePath(Application.Exename) + 'Bibliotheque\Vide.htm');
End;
{ ************************************************************************************************************************************************** }

End.
