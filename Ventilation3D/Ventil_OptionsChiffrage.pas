unit Ventil_OptionsChiffrage;
             
{$Include 'Ventil_Directives.pas'}

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, Math,
  Buttons, ExtCtrls, Grids, BaseGrid, advObj, AdvGrid, ComCtrls;

type
  TDlg_OptionsChiffrage = class(TForm)
    Bevel1: TBevel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    Grid_Gammes: TAdvStringGrid;
    procedure Grid_GammesGetEditorType(Sender: TObject; ACol,
      ARow: Integer; var AEditor: TEditorType);
  private
    Procedure FillGridGammes;
  public
    { Public declarations }
  end;

Procedure SetOptionsChiffrage;

Var
  Dlg_OptionsChiffrage: TDlg_OptionsChiffrage;

Implementation
Uses
  Ventil_Declarations, Ventil_Const, Ventil_Edibatec_Utils;


{$R *.dfm}


{ ************************************************************************************************************************************************** }
Procedure SetOptionsChiffrage;
Begin
  Application.CreateForm(TDlg_OptionsChiffrage, Dlg_OptionsChiffrage);
  Dlg_OptionsChiffrage.FillGridGammes;
  If Dlg_OptionsChiffrage.ShowModal = Id_Ok Then Begin

  End;
  FreeAndNil(Dlg_OptionsChiffrage);
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_OptionsChiffrage.FillGridGammes;
Begin
  Grid_Gammes.Clear;
{
  Grid_Gammes.RowCount := Max(15, High(GammesChiffrage));
  For m_NoGamme := Low(GammesChiffrage) To High(GammesChiffrage) Do Begin
    Grid_Gammes.Cells[0, m_NoGamme - 1] := c_TabGammesChiffrage[m_NoGamme];
    If GammesChiffrage[m_NoGamme] <> '' Then
    Grid_Gammes.Cells[1, m_NoGamme - 1] := NomGammeEdibatec(GammesChiffrage[m_NoGamme]);
  End;
}
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_OptionsChiffrage.Grid_GammesGetEditorType(Sender: TObject; ACol, ARow: Integer; var AEditor: TEditorType);
Begin
  AEditor := edEditBtn;
End;
{ ************************************************************************************************************************************************** }

end.
