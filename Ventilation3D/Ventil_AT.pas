
{*******************************************************************************************}
{                                                                                           }
{                                  Liaison de donn�es XML                                   }
{                                                                                           }
{         G�n�r� le : 16/12/2019 11:41:51                                                   }
{       G�n�r� depuis : D:\D�veloppement BBS\DELPHEXE\Commun\AT_Ventilation\AT_VENTIL.xsd   }
{                                                                                           }
{*******************************************************************************************}

unit VENTIL_AT;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ D�cl. Forward }

  IXMLAvis_Technique_Ventilation = interface;
  IXMLListe_AT_Ancien = interface;
  IXMLUsages = interface;
  IXMLCaracteristiques = interface;
  IXMLATS = interface;
  IXMLAT = interface;
  IXMLPieceHumide = interface;
  IXMLTypeEA = interface;
  IXMLCONFIGS = interface;
  IXMLCONFIG = interface;
  IXMLSingularit�s = interface;
  IXMLDEBIT_RT = interface;
  IXMLCdeps = interface;
  IXMLCrdbnrs = interface;
  IXMLLOCAUX = interface;
  IXMLPieceSeche = interface;
  IXMLEntree_Solution = interface;
  IXMLEquipements = interface;
  IXMLEquipements_Bouches = interface;
  IXMLEquipements_Bouches_Type_Bouche = interface;
  IXMLReferences = interface;
  IXMLEquipements_Entrees = interface;
  IXMLEquipements_Entrees_Type_Entree = interface;
  IXMLSolutions = interface;
  IXMLType_Solution = interface;
  IXMLConfig_Solution = interface;
  IXMLConfig_SolutionList = interface;
  IXMLStructure_Entree = interface;
  IXMLStructure_EntreeList = interface;
  IXMLExtracteurs = interface;
  IXMLType_Extracteur = interface;
  IXMLPuissances_Extracteurs = interface;
  IXMLATS_PUISSANCES = interface;
  IXMLAT_PUISSANCES = interface;
  IXMLCaisson = interface;
  IXMLCaissonList = interface;
  IXMLCONFIG_PUISSANCES = interface;
  IXMLCONFIG_PUISSANCESList = interface;

{ IXMLAvis_Technique_Ventilation }

  IXMLAvis_Technique_Ventilation = interface(IXMLNode)
    ['{35151560-DA63-4DA4-9F95-1705EF1DBD93}']
    { Accesseurs de propri�t�s }
    function Get_Titre_AT: UnicodeString;
    function Get_Titulaire: UnicodeString;
    function Get_Code_Titulaire: UnicodeString;
    function Get_Industriel: UnicodeString;
    function Get_Code_Industriel: UnicodeString;
    function Get_Num_AT: UnicodeString;
    function Get_Liste_AT_Ancien: IXMLListe_AT_Ancien;
    function Get_Date_Application: UnicodeString;
    function Get_Usages: IXMLUsages;
    function Get_Caracteristiques: IXMLCaracteristiques;
    function Get_NB_AT: Integer;
    function Get_ATS: IXMLATS;
    function Get_Equipements: IXMLEquipements;
    function Get_Puissances_Extracteurs: IXMLPuissances_Extracteurs;
    procedure Set_Titre_AT(Value: UnicodeString);
    procedure Set_Titulaire(Value: UnicodeString);
    procedure Set_Code_Titulaire(Value: UnicodeString);
    procedure Set_Industriel(Value: UnicodeString);
    procedure Set_Code_Industriel(Value: UnicodeString);
    procedure Set_Num_AT(Value: UnicodeString);
    procedure Set_Date_Application(Value: UnicodeString);
    procedure Set_NB_AT(Value: Integer);
    { M�thodes & propri�t�s }
    property Titre_AT: UnicodeString read Get_Titre_AT write Set_Titre_AT;
    property Titulaire: UnicodeString read Get_Titulaire write Set_Titulaire;
    property Code_Titulaire: UnicodeString read Get_Code_Titulaire write Set_Code_Titulaire;
    property Industriel: UnicodeString read Get_Industriel write Set_Industriel;
    property Code_Industriel: UnicodeString read Get_Code_Industriel write Set_Code_Industriel;
    property Num_AT: UnicodeString read Get_Num_AT write Set_Num_AT;
    property Liste_AT_Ancien: IXMLListe_AT_Ancien read Get_Liste_AT_Ancien;
    property Date_Application: UnicodeString read Get_Date_Application write Set_Date_Application;
    property Usages: IXMLUsages read Get_Usages;
    property Caracteristiques: IXMLCaracteristiques read Get_Caracteristiques;
    property NB_AT: Integer read Get_NB_AT write Set_NB_AT;
    property ATS: IXMLATS read Get_ATS;
    property Equipements: IXMLEquipements read Get_Equipements;
    property Puissances_Extracteurs: IXMLPuissances_Extracteurs read Get_Puissances_Extracteurs;
  end;

{ IXMLListe_AT_Ancien }

  IXMLListe_AT_Ancien = interface(IXMLNodeCollection)
    ['{3021083B-A554-4C58-9E7D-609168957F5B}']
    { Accesseurs de propri�t�s }
    function Get_Num_AT_Ancien(Index: Integer): UnicodeString;
    { M�thodes & propri�t�s }
    function Add(const Num_AT_Ancien: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const Num_AT_Ancien: UnicodeString): IXMLNode;
    property Num_AT_Ancien[Index: Integer]: UnicodeString read Get_Num_AT_Ancien; default;
  end;

{ IXMLUsages }

  IXMLUsages = interface(IXMLNode)
    ['{8AED0C27-8957-4C91-A4E7-6760AE666A8C}']
    { Accesseurs de propri�t�s }
    function Get_Collectif: Boolean;
    function Get_Individuel: Boolean;
    function Get_Hotel: Boolean;
    procedure Set_Collectif(Value: Boolean);
    procedure Set_Individuel(Value: Boolean);
    procedure Set_Hotel(Value: Boolean);
    { M�thodes & propri�t�s }
    property Collectif: Boolean read Get_Collectif write Set_Collectif;
    property Individuel: Boolean read Get_Individuel write Set_Individuel;
    property Hotel: Boolean read Get_Hotel write Set_Hotel;
  end;

{ IXMLCaracteristiques }

  IXMLCaracteristiques = interface(IXMLNode)
    ['{EBA25CAA-9853-4761-A1CB-F9E1627AA47F}']
    { Accesseurs de propri�t�s }
    function Get_Double_Flux: Boolean;
    function Get_Autor�glable: Boolean;
    function Get_Hygroreglable: Boolean;
    function Get_Basse_Pression: Boolean;
    function Get_Type_Extraction: UnicodeString;
    procedure Set_Double_Flux(Value: Boolean);
    procedure Set_Autor�glable(Value: Boolean);
    procedure Set_Hygroreglable(Value: Boolean);
    procedure Set_Basse_Pression(Value: Boolean);
    procedure Set_Type_Extraction(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property Double_Flux: Boolean read Get_Double_Flux write Set_Double_Flux;
    property Autor�glable: Boolean read Get_Autor�glable write Set_Autor�glable;
    property Hygroreglable: Boolean read Get_Hygroreglable write Set_Hygroreglable;
    property Basse_Pression: Boolean read Get_Basse_Pression write Set_Basse_Pression;
    property Type_Extraction: UnicodeString read Get_Type_Extraction write Set_Type_Extraction;
  end;

{ IXMLATS }

  IXMLATS = interface(IXMLNodeCollection)
    ['{818053AD-62B7-4659-AB58-DA31125A7687}']
    { Accesseurs de propri�t�s }
    function Get_AT(Index: Integer): IXMLAT;
    { M�thodes & propri�t�s }
    function Add: IXMLAT;
    function Insert(const Index: Integer): IXMLAT;
    property AT[Index: Integer]: IXMLAT read Get_AT; default;
  end;

{ IXMLAT }

  IXMLAT = interface(IXMLNode)
    ['{C8098C95-51F2-4707-831D-38D4639E5263}']
    { Accesseurs de propri�t�s }
    function Get_REF_AT: UnicodeString;
    function Get_LIBELLE: UnicodeString;
    function Get_HYGRO_A: Boolean;
    function Get_HYGRO_B1: Boolean;
    function Get_HYGRO_B2: Boolean;
    function Get_Coupe_Tirage_Complementaire: IXMLPieceHumide;
    function Get_GAZ: Boolean;
    function Get_TypeEA: IXMLTypeEA;
    function Get_Optimisation: Boolean;
    function Get_NB_CONFIG: Integer;
    function Get_CONFIGS: IXMLCONFIGS;
    procedure Set_REF_AT(Value: UnicodeString);
    procedure Set_LIBELLE(Value: UnicodeString);
    procedure Set_HYGRO_A(Value: Boolean);
    procedure Set_HYGRO_B1(Value: Boolean);
    procedure Set_HYGRO_B2(Value: Boolean);
    procedure Set_GAZ(Value: Boolean);
    procedure Set_Optimisation(Value: Boolean);
    procedure Set_NB_CONFIG(Value: Integer);
    { M�thodes & propri�t�s }
    property REF_AT: UnicodeString read Get_REF_AT write Set_REF_AT;
    property LIBELLE: UnicodeString read Get_LIBELLE write Set_LIBELLE;
    property HYGRO_A: Boolean read Get_HYGRO_A write Set_HYGRO_A;
    property HYGRO_B1: Boolean read Get_HYGRO_B1 write Set_HYGRO_B1;
    property HYGRO_B2: Boolean read Get_HYGRO_B2 write Set_HYGRO_B2;
    property Coupe_Tirage_Complementaire: IXMLPieceHumide read Get_Coupe_Tirage_Complementaire;
    property GAZ: Boolean read Get_GAZ write Set_GAZ;
    property TypeEA: IXMLTypeEA read Get_TypeEA;
    property Optimisation: Boolean read Get_Optimisation write Set_Optimisation;
    property NB_CONFIG: Integer read Get_NB_CONFIG write Set_NB_CONFIG;
    property CONFIGS: IXMLCONFIGS read Get_CONFIGS;
  end;

{ IXMLPieceHumide }

  IXMLPieceHumide = interface(IXMLNode)
    ['{EFFDF99C-3E5A-4526-B7ED-E903733AEBDB}']
    { Accesseurs de propri�t�s }
    function Get_Code: UnicodeString;
    function Get_Qvrep: Single;
    procedure Set_Code(Value: UnicodeString);
    procedure Set_Qvrep(Value: Single);
    { M�thodes & propri�t�s }
    property Code: UnicodeString read Get_Code write Set_Code;
    property Qvrep: Single read Get_Qvrep write Set_Qvrep;
  end;

{ IXMLTypeEA }

  IXMLTypeEA = interface(IXMLNode)
    ['{29D42D47-0348-454C-AC80-6F83AC8928CD}']
    { Accesseurs de propri�t�s }
    function Get_Presence_EA_Fixes: Boolean;
    function Get_Presence_EA_Autoreglables: Boolean;
    function Get_Dp1: Integer;
    function Get_Dp2: Integer;
    function Get_R_f: Single;
    procedure Set_Presence_EA_Fixes(Value: Boolean);
    procedure Set_Presence_EA_Autoreglables(Value: Boolean);
    procedure Set_Dp1(Value: Integer);
    procedure Set_Dp2(Value: Integer);
    procedure Set_R_f(Value: Single);
    { M�thodes & propri�t�s }
    property Presence_EA_Fixes: Boolean read Get_Presence_EA_Fixes write Set_Presence_EA_Fixes;
    property Presence_EA_Autoreglables: Boolean read Get_Presence_EA_Autoreglables write Set_Presence_EA_Autoreglables;
    property Dp1: Integer read Get_Dp1 write Set_Dp1;
    property Dp2: Integer read Get_Dp2 write Set_Dp2;
    property R_f: Single read Get_R_f write Set_R_f;
  end;

{ IXMLCONFIGS }

  IXMLCONFIGS = interface(IXMLNodeCollection)
    ['{DD8C3A9A-4E75-4226-9D93-CD527E514805}']
    { Accesseurs de propri�t�s }
    function Get_CONFIG(Index: Integer): IXMLCONFIG;
    { M�thodes & propri�t�s }
    function Add: IXMLCONFIG;
    function Insert(const Index: Integer): IXMLCONFIG;
    property CONFIG[Index: Integer]: IXMLCONFIG read Get_CONFIG; default;
  end;

{ IXMLCONFIG }

  IXMLCONFIG = interface(IXMLNode)
    ['{BF69FBC6-A165-4BFE-BDC0-4770BACF893B}']
    { Accesseurs de propri�t�s }
    function Get_Type_Logement: Integer;
    function Get_Singularit�s: IXMLSingularit�s;
    function Get_Nb_Sdb_WC: Integer;
    function Get_Nb_Sdb: Integer;
    function Get_Nb_WC: Integer;
    function Get_Nb_Sde: Integer;
    function Get_DEBIT_RT: IXMLDEBIT_RT;
    function Get_LOCAUX: IXMLLOCAUX;
    procedure Set_Type_Logement(Value: Integer);
    procedure Set_Nb_Sdb_WC(Value: Integer);
    procedure Set_Nb_Sdb(Value: Integer);
    procedure Set_Nb_WC(Value: Integer);
    procedure Set_Nb_Sde(Value: Integer);
    { M�thodes & propri�t�s }
    property Type_Logement: Integer read Get_Type_Logement write Set_Type_Logement;
    property Singularit�s: IXMLSingularit�s read Get_Singularit�s;
    property Nb_Sdb_WC: Integer read Get_Nb_Sdb_WC write Set_Nb_Sdb_WC;
    property Nb_Sdb: Integer read Get_Nb_Sdb write Set_Nb_Sdb;
    property Nb_WC: Integer read Get_Nb_WC write Set_Nb_WC;
    property Nb_Sde: Integer read Get_Nb_Sde write Set_Nb_Sde;
    property DEBIT_RT: IXMLDEBIT_RT read Get_DEBIT_RT;
    property LOCAUX: IXMLLOCAUX read Get_LOCAUX;
  end;

{ IXMLSingularit�s }

  IXMLSingularit�s = interface(IXMLNode)
    ['{F98F6CE1-19E5-4EE4-A812-05AEEC50906B}']
    { Accesseurs de propri�t�s }
    function Get_Config_Optimisee: Boolean;
    function Get_Changement_Bouche: Boolean;
    function Get_EA_Fixes: Boolean;
    function Get_EA_Autor�glables: Boolean;
    procedure Set_Config_Optimisee(Value: Boolean);
    procedure Set_Changement_Bouche(Value: Boolean);
    procedure Set_EA_Fixes(Value: Boolean);
    procedure Set_EA_Autor�glables(Value: Boolean);
    { M�thodes & propri�t�s }
    property Config_Optimisee: Boolean read Get_Config_Optimisee write Set_Config_Optimisee;
    property Changement_Bouche: Boolean read Get_Changement_Bouche write Set_Changement_Bouche;
    property EA_Fixes: Boolean read Get_EA_Fixes write Set_EA_Fixes;
    property EA_Autor�glables: Boolean read Get_EA_Autor�glables write Set_EA_Autor�glables;
  end;

{ IXMLDEBIT_RT }

  IXMLDEBIT_RT = interface(IXMLNode)
    ['{3CBE7A4E-5D24-4D9F-B8E6-8AD72EC21DC5}']
    { Accesseurs de propri�t�s }
    function Get_Cdeps: IXMLCdeps;
    function Get_Crdbnrs: IXMLCrdbnrs;
    function Get_Qv_Rep: Single;
    function Get_Qv_Sou: Single;
    function Get_Smea_RT2012: Single;
    function Get_Smea_Existant: Single;
    function Get_Module_1: Single;
    function Get_Module_2: Single;
    function Get_Qsupp_Sdb: Single;
    function Get_Qsupp_WC: Single;
    function Get_Qsupp_Sdb_WC: Single;
    function Get_Qsupp_Cellier: Single;
    function Get_SmeaMoins_Sdb: Single;
    function Get_SmeaMoins_Sdb_M1: Single;
    function Get_SmeaMoins_Sdb_M2: Single;
    function Get_SmeaMoins_WC: Single;
    function Get_SmeaMoins_WC_M1: Single;
    function Get_SmeaMoins_WC_M2: Single;
    function Get_SmeaMoins_Sdb_WC: Single;
    function Get_SmeaMoins_Sdb_WC_M1: Single;
    function Get_SmeaMoins_Sdb_WC_M2: Single;
    function Get_SmeaMoins_Cellier: Single;
    function Get_SmeaMoins_Cellier_M1: Single;
    function Get_SmeaMoins_Cellier_M2: Single;
    function Get_Ext_min: Single;
    function Get_Ext_max: Single;
    function Get_Borne_min: Single;
    procedure Set_Qv_Rep(Value: Single);
    procedure Set_Qv_Sou(Value: Single);
    procedure Set_Smea_RT2012(Value: Single);
    procedure Set_Smea_Existant(Value: Single);
    procedure Set_Module_1(Value: Single);
    procedure Set_Module_2(Value: Single);
    procedure Set_Qsupp_Sdb(Value: Single);
    procedure Set_Qsupp_WC(Value: Single);
    procedure Set_Qsupp_Sdb_WC(Value: Single);
    procedure Set_Qsupp_Cellier(Value: Single);
    procedure Set_SmeaMoins_Sdb(Value: Single);
    procedure Set_SmeaMoins_Sdb_M1(Value: Single);
    procedure Set_SmeaMoins_Sdb_M2(Value: Single);
    procedure Set_SmeaMoins_WC(Value: Single);
    procedure Set_SmeaMoins_WC_M1(Value: Single);
    procedure Set_SmeaMoins_WC_M2(Value: Single);
    procedure Set_SmeaMoins_Sdb_WC(Value: Single);
    procedure Set_SmeaMoins_Sdb_WC_M1(Value: Single);
    procedure Set_SmeaMoins_Sdb_WC_M2(Value: Single);
    procedure Set_SmeaMoins_Cellier(Value: Single);
    procedure Set_SmeaMoins_Cellier_M1(Value: Single);
    procedure Set_SmeaMoins_Cellier_M2(Value: Single);
    procedure Set_Ext_min(Value: Single);
    procedure Set_Ext_max(Value: Single);
    procedure Set_Borne_min(Value: Single);
    { M�thodes & propri�t�s }
    property Cdeps: IXMLCdeps read Get_Cdeps;
    property Crdbnrs: IXMLCrdbnrs read Get_Crdbnrs;
    property Qv_Rep: Single read Get_Qv_Rep write Set_Qv_Rep;
    property Qv_Sou: Single read Get_Qv_Sou write Set_Qv_Sou;
    property Smea_RT2012: Single read Get_Smea_RT2012 write Set_Smea_RT2012;
    property Smea_Existant: Single read Get_Smea_Existant write Set_Smea_Existant;
    property Module_1: Single read Get_Module_1 write Set_Module_1;
    property Module_2: Single read Get_Module_2 write Set_Module_2;
    property Qsupp_Sdb: Single read Get_Qsupp_Sdb write Set_Qsupp_Sdb;
    property Qsupp_WC: Single read Get_Qsupp_WC write Set_Qsupp_WC;
    property Qsupp_Sdb_WC: Single read Get_Qsupp_Sdb_WC write Set_Qsupp_Sdb_WC;
    property Qsupp_Cellier: Single read Get_Qsupp_Cellier write Set_Qsupp_Cellier;
    property SmeaMoins_Sdb: Single read Get_SmeaMoins_Sdb write Set_SmeaMoins_Sdb;
    property SmeaMoins_Sdb_M1: Single read Get_SmeaMoins_Sdb_M1 write Set_SmeaMoins_Sdb_M1;
    property SmeaMoins_Sdb_M2: Single read Get_SmeaMoins_Sdb_M2 write Set_SmeaMoins_Sdb_M2;
    property SmeaMoins_WC: Single read Get_SmeaMoins_WC write Set_SmeaMoins_WC;
    property SmeaMoins_WC_M1: Single read Get_SmeaMoins_WC_M1 write Set_SmeaMoins_WC_M1;
    property SmeaMoins_WC_M2: Single read Get_SmeaMoins_WC_M2 write Set_SmeaMoins_WC_M2;
    property SmeaMoins_Sdb_WC: Single read Get_SmeaMoins_Sdb_WC write Set_SmeaMoins_Sdb_WC;
    property SmeaMoins_Sdb_WC_M1: Single read Get_SmeaMoins_Sdb_WC_M1 write Set_SmeaMoins_Sdb_WC_M1;
    property SmeaMoins_Sdb_WC_M2: Single read Get_SmeaMoins_Sdb_WC_M2 write Set_SmeaMoins_Sdb_WC_M2;
    property SmeaMoins_Cellier: Single read Get_SmeaMoins_Cellier write Set_SmeaMoins_Cellier;
    property SmeaMoins_Cellier_M1: Single read Get_SmeaMoins_Cellier_M1 write Set_SmeaMoins_Cellier_M1;
    property SmeaMoins_Cellier_M2: Single read Get_SmeaMoins_Cellier_M2 write Set_SmeaMoins_Cellier_M2;
    property Ext_min: Single read Get_Ext_min write Set_Ext_min;
    property Ext_max: Single read Get_Ext_max write Set_Ext_max;
    property Borne_min: Single read Get_Borne_min write Set_Borne_min;
  end;

{ IXMLCdeps }

  IXMLCdeps = interface(IXMLNodeCollection)
    ['{0E147019-112F-43D9-9654-698D23078BA0}']
    { Accesseurs de propri�t�s }
    function Get_Cdep(Index: Integer): Single;
    { M�thodes & propri�t�s }
    function Add(const Cdep: Single): IXMLNode;
    function Insert(const Index: Integer; const Cdep: Single): IXMLNode;
    property Cdep[Index: Integer]: Single read Get_Cdep; default;
  end;

{ IXMLCrdbnrs }

  IXMLCrdbnrs = interface(IXMLNodeCollection)
    ['{9CBF1E40-5BE6-4EAA-9F35-E381B70D241F}']
    { Accesseurs de propri�t�s }
    function Get_Crdbnr(Index: Integer): Single;
    { M�thodes & propri�t�s }
    function Add(const Crdbnr: Single): IXMLNode;
    function Insert(const Index: Integer; const Crdbnr: Single): IXMLNode;
    property Crdbnr[Index: Integer]: Single read Get_Crdbnr; default;
  end;

{ IXMLLOCAUX }

  IXMLLOCAUX = interface(IXMLNode)
    ['{9BE192B8-6F12-4A68-8E33-C386CC6F5567}']
    { Accesseurs de propri�t�s }
    function Get_Cuisine: IXMLPieceHumide;
    function Get_Salle_de_bain_WC: IXMLPieceHumide;
    function Get_Salle_de_bain_WC2: IXMLPieceHumide;
    function Get_Salle_de_bain_WC_supp: IXMLPieceHumide;
    function Get_Salle_de_bain: IXMLPieceHumide;
    function Get_Salle_de_bain2: IXMLPieceHumide;
    function Get_Salle_de_bain_supp: IXMLPieceHumide;
    function Get_WC_Unique: IXMLPieceHumide;
    function Get_WC_Multiple: IXMLPieceHumide;
    function Get_WC_Supp: IXMLPieceHumide;
    function Get_Salle_eau_cellier: IXMLPieceHumide;
    function Get_Salle_eau_cellier_supp: IXMLPieceHumide;
    function Get_Salon: IXMLPieceSeche;
    function Get_Chambre: IXMLPieceSeche;
    { M�thodes & propri�t�s }
    property Cuisine: IXMLPieceHumide read Get_Cuisine;
    property Salle_de_bain_WC: IXMLPieceHumide read Get_Salle_de_bain_WC;
    property Salle_de_bain_WC2: IXMLPieceHumide read Get_Salle_de_bain_WC2;
    property Salle_de_bain_WC_supp: IXMLPieceHumide read Get_Salle_de_bain_WC_supp;
    property Salle_de_bain: IXMLPieceHumide read Get_Salle_de_bain;
    property Salle_de_bain2: IXMLPieceHumide read Get_Salle_de_bain2;
    property Salle_de_bain_supp: IXMLPieceHumide read Get_Salle_de_bain_supp;
    property WC_Unique: IXMLPieceHumide read Get_WC_Unique;
    property WC_Multiple: IXMLPieceHumide read Get_WC_Multiple;
    property WC_Supp: IXMLPieceHumide read Get_WC_Supp;
    property Salle_eau_cellier: IXMLPieceHumide read Get_Salle_eau_cellier;
    property Salle_eau_cellier_supp: IXMLPieceHumide read Get_Salle_eau_cellier_supp;
    property Salon: IXMLPieceSeche read Get_Salon;
    property Chambre: IXMLPieceSeche read Get_Chambre;
  end;

{ IXMLPieceSeche }

  IXMLPieceSeche = interface(IXMLNode)
    ['{8EFD40A6-7740-46B1-8046-EB87443D6B9D}']
    { Accesseurs de propri�t�s }
    function Get_Entree_Solution: IXMLEntree_Solution;
    { M�thodes & propri�t�s }
    property Entree_Solution: IXMLEntree_Solution read Get_Entree_Solution;
  end;

{ IXMLEntree_Solution }

  IXMLEntree_Solution = interface(IXMLNode)
    ['{5FDAA24E-97F0-45C6-8EDE-A01FB5E7F17C}']
    { Accesseurs de propri�t�s }
    function Get_Code: UnicodeString;
    procedure Set_Code(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property Code: UnicodeString read Get_Code write Set_Code;
  end;

{ IXMLEquipements }

  IXMLEquipements = interface(IXMLNode)
    ['{7BB84049-8CFF-4DE9-940A-D7AE5B1053CC}']
    { Accesseurs de propri�t�s }
    function Get_Bouches: IXMLEquipements_Bouches;
    function Get_Entrees: IXMLEquipements_Entrees;
    function Get_Solutions: IXMLSolutions;
    function Get_Extracteurs: IXMLExtracteurs;
    { M�thodes & propri�t�s }
    property Bouches: IXMLEquipements_Bouches read Get_Bouches;
    property Entrees: IXMLEquipements_Entrees read Get_Entrees;
    property Solutions: IXMLSolutions read Get_Solutions;
    property Extracteurs: IXMLExtracteurs read Get_Extracteurs;
  end;

{ IXMLEquipements_Bouches }

  IXMLEquipements_Bouches = interface(IXMLNodeCollection)
    ['{271CA2B2-71B3-4C28-99ED-22CD38952A8E}']
    { Accesseurs de propri�t�s }
    function Get_Type_Bouche(Index: Integer): IXMLEquipements_Bouches_Type_Bouche;
    { M�thodes & propri�t�s }
    function Add: IXMLEquipements_Bouches_Type_Bouche;
    function Insert(const Index: Integer): IXMLEquipements_Bouches_Type_Bouche;
    property Type_Bouche[Index: Integer]: IXMLEquipements_Bouches_Type_Bouche read Get_Type_Bouche; default;
  end;

{ IXMLEquipements_Bouches_Type_Bouche }

  IXMLEquipements_Bouches_Type_Bouche = interface(IXMLNode)
    ['{B484B631-C2CD-482C-8B84-6DEFCEFA1FE0}']
    { Accesseurs de propri�t�s }
    function Get_Code: UnicodeString;
    function Get_References: IXMLReferences;
    function Get_Qmin: Single;
    function Get_QminF: Single;
    function Get_QminLimite: Single;
    function Get_QmaxF: Single;
    function Get_QmaxLimite: Single;
    procedure Set_Code(Value: UnicodeString);
    procedure Set_Qmin(Value: Single);
    procedure Set_QminF(Value: Single);
    procedure Set_QminLimite(Value: Single);
    procedure Set_QmaxF(Value: Single);
    procedure Set_QmaxLimite(Value: Single);
    { M�thodes & propri�t�s }
    property Code: UnicodeString read Get_Code write Set_Code;
    property References: IXMLReferences read Get_References;
    property Qmin: Single read Get_Qmin write Set_Qmin;
    property QminF: Single read Get_QminF write Set_QminF;
    property QminLimite: Single read Get_QminLimite write Set_QminLimite;
    property QmaxF: Single read Get_QmaxF write Set_QmaxF;
    property QmaxLimite: Single read Get_QmaxLimite write Set_QmaxLimite;
  end;

{ IXMLReferences }

  IXMLReferences = interface(IXMLNodeCollection)
    ['{9D4C1A42-2CC7-45CF-924E-9C65C4F0E6D6}']
    { Accesseurs de propri�t�s }
    function Get_Reference(Index: Integer): UnicodeString;
    { M�thodes & propri�t�s }
    function Add(const Reference: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const Reference: UnicodeString): IXMLNode;
    property Reference[Index: Integer]: UnicodeString read Get_Reference; default;
  end;

{ IXMLEquipements_Entrees }

  IXMLEquipements_Entrees = interface(IXMLNodeCollection)
    ['{E1C40EC8-7001-4D08-9023-445369591DF2}']
    { Accesseurs de propri�t�s }
    function Get_Type_Entree(Index: Integer): IXMLEquipements_Entrees_Type_Entree;
    { M�thodes & propri�t�s }
    function Add: IXMLEquipements_Entrees_Type_Entree;
    function Insert(const Index: Integer): IXMLEquipements_Entrees_Type_Entree;
    property Type_Entree[Index: Integer]: IXMLEquipements_Entrees_Type_Entree read Get_Type_Entree; default;
  end;

{ IXMLEquipements_Entrees_Type_Entree }

  IXMLEquipements_Entrees_Type_Entree = interface(IXMLNode)
    ['{65078F72-C445-43C0-A18A-E5D3569FE130}']
    { Accesseurs de propri�t�s }
    function Get_Code: UnicodeString;
    function Get_References: IXMLReferences;
    function Get_EA_min: Single;
    function Get_EA_max: Single;
    procedure Set_Code(Value: UnicodeString);
    procedure Set_EA_min(Value: Single);
    procedure Set_EA_max(Value: Single);
    { M�thodes & propri�t�s }
    property Code: UnicodeString read Get_Code write Set_Code;
    property References: IXMLReferences read Get_References;
    property EA_min: Single read Get_EA_min write Set_EA_min;
    property EA_max: Single read Get_EA_max write Set_EA_max;
  end;

{ IXMLSolutions }

  IXMLSolutions = interface(IXMLNodeCollection)
    ['{F09BD370-75D4-4910-A72A-D9634289A0C5}']
    { Accesseurs de propri�t�s }
    function Get_Type_Solution(Index: Integer): IXMLType_Solution;
    { M�thodes & propri�t�s }
    function Add: IXMLType_Solution;
    function Insert(const Index: Integer): IXMLType_Solution;
    property Type_Solution[Index: Integer]: IXMLType_Solution read Get_Type_Solution; default;
  end;

{ IXMLType_Solution }

  IXMLType_Solution = interface(IXMLNode)
    ['{7B3BEE0E-6BC5-4A3B-8D19-BC414591C59A}']
    { Accesseurs de propri�t�s }
    function Get_Code_Solution: UnicodeString;
    function Get_Config_Solution: IXMLConfig_SolutionList;
    procedure Set_Code_Solution(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property Code_Solution: UnicodeString read Get_Code_Solution write Set_Code_Solution;
    property Config_Solution: IXMLConfig_SolutionList read Get_Config_Solution;
  end;

{ IXMLConfig_Solution }

  IXMLConfig_Solution = interface(IXMLNode)
    ['{B3FDD41C-F5BC-4787-A0A3-9FC4095FE275}']
    { Accesseurs de propri�t�s }
    function Get_Solution_Libelle: UnicodeString;
    function Get_Solution_Libre: Single;
    function Get_Entree: IXMLStructure_EntreeList;
    procedure Set_Solution_Libelle(Value: UnicodeString);
    procedure Set_Solution_Libre(Value: Single);
    { M�thodes & propri�t�s }
    property Solution_Libelle: UnicodeString read Get_Solution_Libelle write Set_Solution_Libelle;
    property Solution_Libre: Single read Get_Solution_Libre write Set_Solution_Libre;
    property Entree: IXMLStructure_EntreeList read Get_Entree;
  end;

{ IXMLConfig_SolutionList }

  IXMLConfig_SolutionList = interface(IXMLNodeCollection)
    ['{1A642B83-B81F-4306-A4BB-175033DE729C}']
    { M�thodes & propri�t�s }
    function Add: IXMLConfig_Solution;
    function Insert(const Index: Integer): IXMLConfig_Solution;

    function Get_Item(Index: Integer): IXMLConfig_Solution;
    property Items[Index: Integer]: IXMLConfig_Solution read Get_Item; default;
  end;

{ IXMLStructure_Entree }

  IXMLStructure_Entree = interface(IXMLNode)
    ['{B32A6E76-33C2-4924-AB34-1F533AF4FEEC}']
    { Accesseurs de propri�t�s }
    function Get_Code: UnicodeString;
    function Get_Nombre: Integer;
    procedure Set_Code(Value: UnicodeString);
    procedure Set_Nombre(Value: Integer);
    { M�thodes & propri�t�s }
    property Code: UnicodeString read Get_Code write Set_Code;
    property Nombre: Integer read Get_Nombre write Set_Nombre;
  end;

{ IXMLStructure_EntreeList }

  IXMLStructure_EntreeList = interface(IXMLNodeCollection)
    ['{67151396-EA10-4BC8-B9FF-EB11EF8DA5E2}']
    { M�thodes & propri�t�s }
    function Add: IXMLStructure_Entree;
    function Insert(const Index: Integer): IXMLStructure_Entree;

    function Get_Item(Index: Integer): IXMLStructure_Entree;
    property Items[Index: Integer]: IXMLStructure_Entree read Get_Item; default;
  end;

{ IXMLExtracteurs }

  IXMLExtracteurs = interface(IXMLNodeCollection)
    ['{287B3D5F-1D1D-450E-8F69-2C5AC9192CD4}']
    { Accesseurs de propri�t�s }
    function Get_Type_Extracteur(Index: Integer): IXMLType_Extracteur;
    { M�thodes & propri�t�s }
    function Add: IXMLType_Extracteur;
    function Insert(const Index: Integer): IXMLType_Extracteur;
    property Type_Extracteur[Index: Integer]: IXMLType_Extracteur read Get_Type_Extracteur; default;
  end;

{ IXMLType_Extracteur }

  IXMLType_Extracteur = interface(IXMLNode)
    ['{DE5592D4-A4A9-4EA5-A068-7B871D8CE141}']
    { Accesseurs de propri�t�s }
    function Get_References: IXMLReferences;
    function Get_N_Cdep: Single;
    function Get_N_Crdbnr: Single;
    function Get_Libelle_Cdep: UnicodeString;
    procedure Set_N_Cdep(Value: Single);
    procedure Set_N_Crdbnr(Value: Single);
    procedure Set_Libelle_Cdep(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property References: IXMLReferences read Get_References;
    property N_Cdep: Single read Get_N_Cdep write Set_N_Cdep;
    property N_Crdbnr: Single read Get_N_Crdbnr write Set_N_Crdbnr;
    property Libelle_Cdep: UnicodeString read Get_Libelle_Cdep write Set_Libelle_Cdep;
  end;

{ IXMLPuissances_Extracteurs }

  IXMLPuissances_Extracteurs = interface(IXMLNode)
    ['{997B8C2D-58CD-4D4D-9571-579AB1E731C2}']
    { Accesseurs de propri�t�s }
    function Get_ATS_PUISSANCES: IXMLATS_PUISSANCES;
    { M�thodes & propri�t�s }
    property ATS_PUISSANCES: IXMLATS_PUISSANCES read Get_ATS_PUISSANCES;
  end;

{ IXMLATS_PUISSANCES }

  IXMLATS_PUISSANCES = interface(IXMLNodeCollection)
    ['{DB98D10D-1BCC-4348-81BC-E44949581F28}']
    { Accesseurs de propri�t�s }
    function Get_AT_PUISSANCES(Index: Integer): IXMLAT_PUISSANCES;
    { M�thodes & propri�t�s }
    function Add: IXMLAT_PUISSANCES;
    function Insert(const Index: Integer): IXMLAT_PUISSANCES;
    property AT_PUISSANCES[Index: Integer]: IXMLAT_PUISSANCES read Get_AT_PUISSANCES; default;
  end;

{ IXMLAT_PUISSANCES }

  IXMLAT_PUISSANCES = interface(IXMLNode)
    ['{3E5B3A25-A75B-468E-8085-14BAE05C0484}']
    { Accesseurs de propri�t�s }
    function Get_REFERENCE_AT: UnicodeString;
    function Get_Caisson: IXMLCaissonList;
    procedure Set_REFERENCE_AT(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property REFERENCE_AT: UnicodeString read Get_REFERENCE_AT write Set_REFERENCE_AT;
    property Caisson: IXMLCaissonList read Get_Caisson;
  end;

{ IXMLCaisson }

  IXMLCaisson = interface(IXMLNode)
    ['{9F2EAE8D-017B-4AB3-89DA-EFBFFDF1508E}']
    { Accesseurs de propri�t�s }
    function Get_Reference_Caisson: UnicodeString;
    function Get_CONFIG_PUISSANCES: IXMLCONFIG_PUISSANCESList;
    procedure Set_Reference_Caisson(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property Reference_Caisson: UnicodeString read Get_Reference_Caisson write Set_Reference_Caisson;
    property CONFIG_PUISSANCES: IXMLCONFIG_PUISSANCESList read Get_CONFIG_PUISSANCES;
  end;

{ IXMLCaissonList }

  IXMLCaissonList = interface(IXMLNodeCollection)
    ['{71AF541F-B00C-4B26-8556-8492C3FB4642}']
    { M�thodes & propri�t�s }
    function Add: IXMLCaisson;
    function Insert(const Index: Integer): IXMLCaisson;

    function Get_Item(Index: Integer): IXMLCaisson;
    property Items[Index: Integer]: IXMLCaisson read Get_Item; default;
  end;

{ IXMLCONFIG_PUISSANCES }

  IXMLCONFIG_PUISSANCES = interface(IXMLNode)
    ['{077801B8-3DAB-4F13-907F-03429B983F07}']
    { Accesseurs de propri�t�s }
    function Get_Type_Logement_Puissances: Integer;
    function Get_Nbre_Sdb_WC: Integer;
    function Get_Nbre_Sdb: Integer;
    function Get_Nbre_WC: Integer;
    function Get_Nbre_SdE: Integer;
    function Get_Puissance_Qv: Single;
    procedure Set_Type_Logement_Puissances(Value: Integer);
    procedure Set_Nbre_Sdb_WC(Value: Integer);
    procedure Set_Nbre_Sdb(Value: Integer);
    procedure Set_Nbre_WC(Value: Integer);
    procedure Set_Nbre_SdE(Value: Integer);
    procedure Set_Puissance_Qv(Value: Single);
    { M�thodes & propri�t�s }
    property Type_Logement_Puissances: Integer read Get_Type_Logement_Puissances write Set_Type_Logement_Puissances;
    property Nbre_Sdb_WC: Integer read Get_Nbre_Sdb_WC write Set_Nbre_Sdb_WC;
    property Nbre_Sdb: Integer read Get_Nbre_Sdb write Set_Nbre_Sdb;
    property Nbre_WC: Integer read Get_Nbre_WC write Set_Nbre_WC;
    property Nbre_SdE: Integer read Get_Nbre_SdE write Set_Nbre_SdE;
    property Puissance_Qv: Single read Get_Puissance_Qv write Set_Puissance_Qv;
  end;

{ IXMLCONFIG_PUISSANCESList }

  IXMLCONFIG_PUISSANCESList = interface(IXMLNodeCollection)
    ['{E99056E1-4875-4AE9-A13D-B65BD3B07279}']
    { M�thodes & propri�t�s }
    function Add: IXMLCONFIG_PUISSANCES;
    function Insert(const Index: Integer): IXMLCONFIG_PUISSANCES;

    function Get_Item(Index: Integer): IXMLCONFIG_PUISSANCES;
    property Items[Index: Integer]: IXMLCONFIG_PUISSANCES read Get_Item; default;
  end;

{ D�cl. Forward }

  TXMLAvis_Technique_Ventilation = class;
  TXMLListe_AT_Ancien = class;
  TXMLUsages = class;
  TXMLCaracteristiques = class;
  TXMLATS = class;
  TXMLAT = class;
  TXMLPieceHumide = class;
  TXMLTypeEA = class;
  TXMLCONFIGS = class;
  TXMLCONFIG = class;
  TXMLSingularit�s = class;
  TXMLDEBIT_RT = class;
  TXMLCdeps = class;
  TXMLCrdbnrs = class;
  TXMLLOCAUX = class;
  TXMLPieceSeche = class;
  TXMLEntree_Solution = class;
  TXMLEquipements = class;
  TXMLEquipements_Bouches = class;
  TXMLEquipements_Bouches_Type_Bouche = class;
  TXMLReferences = class;
  TXMLEquipements_Entrees = class;
  TXMLEquipements_Entrees_Type_Entree = class;
  TXMLSolutions = class;
  TXMLType_Solution = class;
  TXMLConfig_Solution = class;
  TXMLConfig_SolutionList = class;
  TXMLStructure_Entree = class;
  TXMLStructure_EntreeList = class;
  TXMLExtracteurs = class;
  TXMLType_Extracteur = class;
  TXMLPuissances_Extracteurs = class;
  TXMLATS_PUISSANCES = class;
  TXMLAT_PUISSANCES = class;
  TXMLCaisson = class;
  TXMLCaissonList = class;
  TXMLCONFIG_PUISSANCES = class;
  TXMLCONFIG_PUISSANCESList = class;

{ TXMLAvis_Technique_Ventilation }

  TXMLAvis_Technique_Ventilation = class(TXMLNode, IXMLAvis_Technique_Ventilation)
  protected
    { IXMLAvis_Technique_Ventilation }
    function Get_Titre_AT: UnicodeString;
    function Get_Titulaire: UnicodeString;
    function Get_Code_Titulaire: UnicodeString;
    function Get_Industriel: UnicodeString;
    function Get_Code_Industriel: UnicodeString;
    function Get_Num_AT: UnicodeString;
    function Get_Liste_AT_Ancien: IXMLListe_AT_Ancien;
    function Get_Date_Application: UnicodeString;
    function Get_Usages: IXMLUsages;
    function Get_Caracteristiques: IXMLCaracteristiques;
    function Get_NB_AT: Integer;
    function Get_ATS: IXMLATS;
    function Get_Equipements: IXMLEquipements;
    function Get_Puissances_Extracteurs: IXMLPuissances_Extracteurs;
    procedure Set_Titre_AT(Value: UnicodeString);
    procedure Set_Titulaire(Value: UnicodeString);
    procedure Set_Code_Titulaire(Value: UnicodeString);
    procedure Set_Industriel(Value: UnicodeString);
    procedure Set_Code_Industriel(Value: UnicodeString);
    procedure Set_Num_AT(Value: UnicodeString);
    procedure Set_Date_Application(Value: UnicodeString);
    procedure Set_NB_AT(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLListe_AT_Ancien }

  TXMLListe_AT_Ancien = class(TXMLNodeCollection, IXMLListe_AT_Ancien)
  protected
    { IXMLListe_AT_Ancien }
    function Get_Num_AT_Ancien(Index: Integer): UnicodeString;
    function Add(const Num_AT_Ancien: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const Num_AT_Ancien: UnicodeString): IXMLNode;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLUsages }

  TXMLUsages = class(TXMLNode, IXMLUsages)
  protected
    { IXMLUsages }
    function Get_Collectif: Boolean;
    function Get_Individuel: Boolean;
    function Get_Hotel: Boolean;
    procedure Set_Collectif(Value: Boolean);
    procedure Set_Individuel(Value: Boolean);
    procedure Set_Hotel(Value: Boolean);
  end;

{ TXMLCaracteristiques }

  TXMLCaracteristiques = class(TXMLNode, IXMLCaracteristiques)
  protected
    { IXMLCaracteristiques }
    function Get_Double_Flux: Boolean;
    function Get_Autor�glable: Boolean;
    function Get_Hygroreglable: Boolean;
    function Get_Basse_Pression: Boolean;
    function Get_Type_Extraction: UnicodeString;
    procedure Set_Double_Flux(Value: Boolean);
    procedure Set_Autor�glable(Value: Boolean);
    procedure Set_Hygroreglable(Value: Boolean);
    procedure Set_Basse_Pression(Value: Boolean);
    procedure Set_Type_Extraction(Value: UnicodeString);
  end;

{ TXMLATS }

  TXMLATS = class(TXMLNodeCollection, IXMLATS)
  protected
    { IXMLATS }
    function Get_AT(Index: Integer): IXMLAT;
    function Add: IXMLAT;
    function Insert(const Index: Integer): IXMLAT;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLAT }

  TXMLAT = class(TXMLNode, IXMLAT)
  protected
    { IXMLAT }
    function Get_REF_AT: UnicodeString;
    function Get_LIBELLE: UnicodeString;
    function Get_HYGRO_A: Boolean;
    function Get_HYGRO_B1: Boolean;
    function Get_HYGRO_B2: Boolean;
    function Get_Coupe_Tirage_Complementaire: IXMLPieceHumide;
    function Get_GAZ: Boolean;
    function Get_TypeEA: IXMLTypeEA;
    function Get_Optimisation: Boolean;
    function Get_NB_CONFIG: Integer;
    function Get_CONFIGS: IXMLCONFIGS;
    procedure Set_REF_AT(Value: UnicodeString);
    procedure Set_LIBELLE(Value: UnicodeString);
    procedure Set_HYGRO_A(Value: Boolean);
    procedure Set_HYGRO_B1(Value: Boolean);
    procedure Set_HYGRO_B2(Value: Boolean);
    procedure Set_GAZ(Value: Boolean);
    procedure Set_Optimisation(Value: Boolean);
    procedure Set_NB_CONFIG(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPieceHumide }

  TXMLPieceHumide = class(TXMLNode, IXMLPieceHumide)
  protected
    { IXMLPieceHumide }
    function Get_Code: UnicodeString;
    function Get_Qvrep: Single;
    procedure Set_Code(Value: UnicodeString);
    procedure Set_Qvrep(Value: Single);
  end;

{ TXMLTypeEA }

  TXMLTypeEA = class(TXMLNode, IXMLTypeEA)
  protected
    { IXMLTypeEA }
    function Get_Presence_EA_Fixes: Boolean;
    function Get_Presence_EA_Autoreglables: Boolean;
    function Get_Dp1: Integer;
    function Get_Dp2: Integer;
    function Get_R_f: Single;
    procedure Set_Presence_EA_Fixes(Value: Boolean);
    procedure Set_Presence_EA_Autoreglables(Value: Boolean);
    procedure Set_Dp1(Value: Integer);
    procedure Set_Dp2(Value: Integer);
    procedure Set_R_f(Value: Single);
  end;

{ TXMLCONFIGS }

  TXMLCONFIGS = class(TXMLNodeCollection, IXMLCONFIGS)
  protected
    { IXMLCONFIGS }
    function Get_CONFIG(Index: Integer): IXMLCONFIG;
    function Add: IXMLCONFIG;
    function Insert(const Index: Integer): IXMLCONFIG;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCONFIG }

  TXMLCONFIG = class(TXMLNode, IXMLCONFIG)
  protected
    { IXMLCONFIG }
    function Get_Type_Logement: Integer;
    function Get_Singularit�s: IXMLSingularit�s;
    function Get_Nb_Sdb_WC: Integer;
    function Get_Nb_Sdb: Integer;
    function Get_Nb_WC: Integer;
    function Get_Nb_Sde: Integer;
    function Get_DEBIT_RT: IXMLDEBIT_RT;
    function Get_LOCAUX: IXMLLOCAUX;
    procedure Set_Type_Logement(Value: Integer);
    procedure Set_Nb_Sdb_WC(Value: Integer);
    procedure Set_Nb_Sdb(Value: Integer);
    procedure Set_Nb_WC(Value: Integer);
    procedure Set_Nb_Sde(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSingularit�s }

  TXMLSingularit�s = class(TXMLNode, IXMLSingularit�s)
  protected
    { IXMLSingularit�s }
    function Get_Config_Optimisee: Boolean;
    function Get_Changement_Bouche: Boolean;
    function Get_EA_Fixes: Boolean;
    function Get_EA_Autor�glables: Boolean;
    procedure Set_Config_Optimisee(Value: Boolean);
    procedure Set_Changement_Bouche(Value: Boolean);
    procedure Set_EA_Fixes(Value: Boolean);
    procedure Set_EA_Autor�glables(Value: Boolean);
  end;

{ TXMLDEBIT_RT }

  TXMLDEBIT_RT = class(TXMLNode, IXMLDEBIT_RT)
  protected
    { IXMLDEBIT_RT }
    function Get_Cdeps: IXMLCdeps;
    function Get_Crdbnrs: IXMLCrdbnrs;
    function Get_Qv_Rep: Single;
    function Get_Qv_Sou: Single;
    function Get_Smea_RT2012: Single;
    function Get_Smea_Existant: Single;
    function Get_Module_1: Single;
    function Get_Module_2: Single;
    function Get_Qsupp_Sdb: Single;
    function Get_Qsupp_WC: Single;
    function Get_Qsupp_Sdb_WC: Single;
    function Get_Qsupp_Cellier: Single;
    function Get_SmeaMoins_Sdb: Single;
    function Get_SmeaMoins_Sdb_M1: Single;
    function Get_SmeaMoins_Sdb_M2: Single;
    function Get_SmeaMoins_WC: Single;
    function Get_SmeaMoins_WC_M1: Single;
    function Get_SmeaMoins_WC_M2: Single;
    function Get_SmeaMoins_Sdb_WC: Single;
    function Get_SmeaMoins_Sdb_WC_M1: Single;
    function Get_SmeaMoins_Sdb_WC_M2: Single;
    function Get_SmeaMoins_Cellier: Single;
    function Get_SmeaMoins_Cellier_M1: Single;
    function Get_SmeaMoins_Cellier_M2: Single;
    function Get_Ext_min: Single;
    function Get_Ext_max: Single;
    function Get_Borne_min: Single;
    procedure Set_Qv_Rep(Value: Single);
    procedure Set_Qv_Sou(Value: Single);
    procedure Set_Smea_RT2012(Value: Single);
    procedure Set_Smea_Existant(Value: Single);
    procedure Set_Module_1(Value: Single);
    procedure Set_Module_2(Value: Single);
    procedure Set_Qsupp_Sdb(Value: Single);
    procedure Set_Qsupp_WC(Value: Single);
    procedure Set_Qsupp_Sdb_WC(Value: Single);
    procedure Set_Qsupp_Cellier(Value: Single);
    procedure Set_SmeaMoins_Sdb(Value: Single);
    procedure Set_SmeaMoins_Sdb_M1(Value: Single);
    procedure Set_SmeaMoins_Sdb_M2(Value: Single);
    procedure Set_SmeaMoins_WC(Value: Single);
    procedure Set_SmeaMoins_WC_M1(Value: Single);
    procedure Set_SmeaMoins_WC_M2(Value: Single);
    procedure Set_SmeaMoins_Sdb_WC(Value: Single);
    procedure Set_SmeaMoins_Sdb_WC_M1(Value: Single);
    procedure Set_SmeaMoins_Sdb_WC_M2(Value: Single);
    procedure Set_SmeaMoins_Cellier(Value: Single);
    procedure Set_SmeaMoins_Cellier_M1(Value: Single);
    procedure Set_SmeaMoins_Cellier_M2(Value: Single);
    procedure Set_Ext_min(Value: Single);
    procedure Set_Ext_max(Value: Single);
    procedure Set_Borne_min(Value: Single);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCdeps }

  TXMLCdeps = class(TXMLNodeCollection, IXMLCdeps)
  protected
    { IXMLCdeps }
    function Get_Cdep(Index: Integer): Single;
    function Add(const Cdep: Single): IXMLNode;
    function Insert(const Index: Integer; const Cdep: Single): IXMLNode;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCrdbnrs }

  TXMLCrdbnrs = class(TXMLNodeCollection, IXMLCrdbnrs)
  protected
    { IXMLCrdbnrs }
    function Get_Crdbnr(Index: Integer): Single;
    function Add(const Crdbnr: Single): IXMLNode;
    function Insert(const Index: Integer; const Crdbnr: Single): IXMLNode;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLLOCAUX }

  TXMLLOCAUX = class(TXMLNode, IXMLLOCAUX)
  protected
    { IXMLLOCAUX }
    function Get_Cuisine: IXMLPieceHumide;
    function Get_Salle_de_bain_WC: IXMLPieceHumide;
    function Get_Salle_de_bain_WC2: IXMLPieceHumide;
    function Get_Salle_de_bain_WC_supp: IXMLPieceHumide;
    function Get_Salle_de_bain: IXMLPieceHumide;
    function Get_Salle_de_bain2: IXMLPieceHumide;
    function Get_Salle_de_bain_supp: IXMLPieceHumide;
    function Get_WC_Unique: IXMLPieceHumide;
    function Get_WC_Multiple: IXMLPieceHumide;
    function Get_WC_Supp: IXMLPieceHumide;
    function Get_Salle_eau_cellier: IXMLPieceHumide;
    function Get_Salle_eau_cellier_supp: IXMLPieceHumide;
    function Get_Salon: IXMLPieceSeche;
    function Get_Chambre: IXMLPieceSeche;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPieceSeche }

  TXMLPieceSeche = class(TXMLNode, IXMLPieceSeche)
  protected
    { IXMLPieceSeche }
    function Get_Entree_Solution: IXMLEntree_Solution;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEntree_Solution }

  TXMLEntree_Solution = class(TXMLNode, IXMLEntree_Solution)
  protected
    { IXMLEntree_Solution }
    function Get_Code: UnicodeString;
    procedure Set_Code(Value: UnicodeString);
  end;

{ TXMLEquipements }

  TXMLEquipements = class(TXMLNode, IXMLEquipements)
  protected
    { IXMLEquipements }
    function Get_Bouches: IXMLEquipements_Bouches;
    function Get_Entrees: IXMLEquipements_Entrees;
    function Get_Solutions: IXMLSolutions;
    function Get_Extracteurs: IXMLExtracteurs;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEquipements_Bouches }

  TXMLEquipements_Bouches = class(TXMLNodeCollection, IXMLEquipements_Bouches)
  protected
    { IXMLEquipements_Bouches }
    function Get_Type_Bouche(Index: Integer): IXMLEquipements_Bouches_Type_Bouche;
    function Add: IXMLEquipements_Bouches_Type_Bouche;
    function Insert(const Index: Integer): IXMLEquipements_Bouches_Type_Bouche;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEquipements_Bouches_Type_Bouche }

  TXMLEquipements_Bouches_Type_Bouche = class(TXMLNode, IXMLEquipements_Bouches_Type_Bouche)
  protected
    { IXMLEquipements_Bouches_Type_Bouche }
    function Get_Code: UnicodeString;
    function Get_References: IXMLReferences;
    function Get_Qmin: Single;
    function Get_QminF: Single;
    function Get_QminLimite: Single;
    function Get_QmaxF: Single;
    function Get_QmaxLimite: Single;
    procedure Set_Code(Value: UnicodeString);
    procedure Set_Qmin(Value: Single);
    procedure Set_QminF(Value: Single);
    procedure Set_QminLimite(Value: Single);
    procedure Set_QmaxF(Value: Single);
    procedure Set_QmaxLimite(Value: Single);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLReferences }

  TXMLReferences = class(TXMLNodeCollection, IXMLReferences)
  protected
    { IXMLReferences }
    function Get_Reference(Index: Integer): UnicodeString;
    function Add(const Reference: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const Reference: UnicodeString): IXMLNode;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEquipements_Entrees }

  TXMLEquipements_Entrees = class(TXMLNodeCollection, IXMLEquipements_Entrees)
  protected
    { IXMLEquipements_Entrees }
    function Get_Type_Entree(Index: Integer): IXMLEquipements_Entrees_Type_Entree;
    function Add: IXMLEquipements_Entrees_Type_Entree;
    function Insert(const Index: Integer): IXMLEquipements_Entrees_Type_Entree;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEquipements_Entrees_Type_Entree }

  TXMLEquipements_Entrees_Type_Entree = class(TXMLNode, IXMLEquipements_Entrees_Type_Entree)
  protected
    { IXMLEquipements_Entrees_Type_Entree }
    function Get_Code: UnicodeString;
    function Get_References: IXMLReferences;
    function Get_EA_min: Single;
    function Get_EA_max: Single;
    procedure Set_Code(Value: UnicodeString);
    procedure Set_EA_min(Value: Single);
    procedure Set_EA_max(Value: Single);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSolutions }

  TXMLSolutions = class(TXMLNodeCollection, IXMLSolutions)
  protected
    { IXMLSolutions }
    function Get_Type_Solution(Index: Integer): IXMLType_Solution;
    function Add: IXMLType_Solution;
    function Insert(const Index: Integer): IXMLType_Solution;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLType_Solution }

  TXMLType_Solution = class(TXMLNode, IXMLType_Solution)
  private
    FConfig_Solution: IXMLConfig_SolutionList;
  protected
    { IXMLType_Solution }
    function Get_Code_Solution: UnicodeString;
    function Get_Config_Solution: IXMLConfig_SolutionList;
    procedure Set_Code_Solution(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLConfig_Solution }

  TXMLConfig_Solution = class(TXMLNode, IXMLConfig_Solution)
  private
    FEntree: IXMLStructure_EntreeList;
  protected
    { IXMLConfig_Solution }
    function Get_Solution_Libelle: UnicodeString;
    function Get_Solution_Libre: Single;
    function Get_Entree: IXMLStructure_EntreeList;
    procedure Set_Solution_Libelle(Value: UnicodeString);
    procedure Set_Solution_Libre(Value: Single);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLConfig_SolutionList }

  TXMLConfig_SolutionList = class(TXMLNodeCollection, IXMLConfig_SolutionList)
  protected
    { IXMLConfig_SolutionList }
    function Add: IXMLConfig_Solution;
    function Insert(const Index: Integer): IXMLConfig_Solution;

    function Get_Item(Index: Integer): IXMLConfig_Solution;
  end;

{ TXMLStructure_Entree }

  TXMLStructure_Entree = class(TXMLNode, IXMLStructure_Entree)
  protected
    { IXMLStructure_Entree }
    function Get_Code: UnicodeString;
    function Get_Nombre: Integer;
    procedure Set_Code(Value: UnicodeString);
    procedure Set_Nombre(Value: Integer);
  end;

{ TXMLStructure_EntreeList }

  TXMLStructure_EntreeList = class(TXMLNodeCollection, IXMLStructure_EntreeList)
  protected
    { IXMLStructure_EntreeList }
    function Add: IXMLStructure_Entree;
    function Insert(const Index: Integer): IXMLStructure_Entree;

    function Get_Item(Index: Integer): IXMLStructure_Entree;
  end;

{ TXMLExtracteurs }

  TXMLExtracteurs = class(TXMLNodeCollection, IXMLExtracteurs)
  protected
    { IXMLExtracteurs }
    function Get_Type_Extracteur(Index: Integer): IXMLType_Extracteur;
    function Add: IXMLType_Extracteur;
    function Insert(const Index: Integer): IXMLType_Extracteur;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLType_Extracteur }

  TXMLType_Extracteur = class(TXMLNode, IXMLType_Extracteur)
  protected
    { IXMLType_Extracteur }
    function Get_References: IXMLReferences;
    function Get_N_Cdep: Single;
    function Get_N_Crdbnr: Single;
    function Get_Libelle_Cdep: UnicodeString;
    procedure Set_N_Cdep(Value: Single);
    procedure Set_N_Crdbnr(Value: Single);
    procedure Set_Libelle_Cdep(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPuissances_Extracteurs }

  TXMLPuissances_Extracteurs = class(TXMLNode, IXMLPuissances_Extracteurs)
  protected
    { IXMLPuissances_Extracteurs }
    function Get_ATS_PUISSANCES: IXMLATS_PUISSANCES;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLATS_PUISSANCES }

  TXMLATS_PUISSANCES = class(TXMLNodeCollection, IXMLATS_PUISSANCES)
  protected
    { IXMLATS_PUISSANCES }
    function Get_AT_PUISSANCES(Index: Integer): IXMLAT_PUISSANCES;
    function Add: IXMLAT_PUISSANCES;
    function Insert(const Index: Integer): IXMLAT_PUISSANCES;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLAT_PUISSANCES }

  TXMLAT_PUISSANCES = class(TXMLNode, IXMLAT_PUISSANCES)
  private
    FCaisson: IXMLCaissonList;
  protected
    { IXMLAT_PUISSANCES }
    function Get_REFERENCE_AT: UnicodeString;
    function Get_Caisson: IXMLCaissonList;
    procedure Set_REFERENCE_AT(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCaisson }

  TXMLCaisson = class(TXMLNode, IXMLCaisson)
  private
    FCONFIG_PUISSANCES: IXMLCONFIG_PUISSANCESList;
  protected
    { IXMLCaisson }
    function Get_Reference_Caisson: UnicodeString;
    function Get_CONFIG_PUISSANCES: IXMLCONFIG_PUISSANCESList;
    procedure Set_Reference_Caisson(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCaissonList }

  TXMLCaissonList = class(TXMLNodeCollection, IXMLCaissonList)
  protected
    { IXMLCaissonList }
    function Add: IXMLCaisson;
    function Insert(const Index: Integer): IXMLCaisson;

    function Get_Item(Index: Integer): IXMLCaisson;
  end;

{ TXMLCONFIG_PUISSANCES }

  TXMLCONFIG_PUISSANCES = class(TXMLNode, IXMLCONFIG_PUISSANCES)
  protected
    { IXMLCONFIG_PUISSANCES }
    function Get_Type_Logement_Puissances: Integer;
    function Get_Nbre_Sdb_WC: Integer;
    function Get_Nbre_Sdb: Integer;
    function Get_Nbre_WC: Integer;
    function Get_Nbre_SdE: Integer;
    function Get_Puissance_Qv: Single;
    procedure Set_Type_Logement_Puissances(Value: Integer);
    procedure Set_Nbre_Sdb_WC(Value: Integer);
    procedure Set_Nbre_Sdb(Value: Integer);
    procedure Set_Nbre_WC(Value: Integer);
    procedure Set_Nbre_SdE(Value: Integer);
    procedure Set_Puissance_Qv(Value: Single);
  end;

{ TXMLCONFIG_PUISSANCESList }

  TXMLCONFIG_PUISSANCESList = class(TXMLNodeCollection, IXMLCONFIG_PUISSANCESList)
  protected
    { IXMLCONFIG_PUISSANCESList }
    function Add: IXMLCONFIG_PUISSANCES;
    function Insert(const Index: Integer): IXMLCONFIG_PUISSANCES;

    function Get_Item(Index: Integer): IXMLCONFIG_PUISSANCES;
  end;

{ Fonctions globales }

function GetAvis_Technique_Ventilation(Doc: IXMLDocument): IXMLAvis_Technique_Ventilation;
function LoadAvis_Technique_Ventilation(const FileName: string): IXMLAvis_Technique_Ventilation;
function NewAvis_Technique_Ventilation: IXMLAvis_Technique_Ventilation;

const
  TargetNamespace = '';

implementation

{ Fonctions globales }

function GetAvis_Technique_Ventilation(Doc: IXMLDocument): IXMLAvis_Technique_Ventilation;
begin
  Result := Doc.GetDocBinding('Avis_Technique_Ventilation', TXMLAvis_Technique_Ventilation, TargetNamespace) as IXMLAvis_Technique_Ventilation;
end;

function LoadAvis_Technique_Ventilation(const FileName: string): IXMLAvis_Technique_Ventilation;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Avis_Technique_Ventilation', TXMLAvis_Technique_Ventilation, TargetNamespace) as IXMLAvis_Technique_Ventilation;
end;

function NewAvis_Technique_Ventilation: IXMLAvis_Technique_Ventilation;
begin
  Result := NewXMLDocument.GetDocBinding('Avis_Technique_Ventilation', TXMLAvis_Technique_Ventilation, TargetNamespace) as IXMLAvis_Technique_Ventilation;
end;

{ TXMLAvis_Technique_Ventilation }

procedure TXMLAvis_Technique_Ventilation.AfterConstruction;
begin
  RegisterChildNode('Liste_AT_Ancien', TXMLListe_AT_Ancien);
  RegisterChildNode('Usages', TXMLUsages);
  RegisterChildNode('Caracteristiques', TXMLCaracteristiques);
  RegisterChildNode('ATS', TXMLATS);
  RegisterChildNode('Equipements', TXMLEquipements);
  RegisterChildNode('Puissances_Extracteurs', TXMLPuissances_Extracteurs);
  inherited;
end;

function TXMLAvis_Technique_Ventilation.Get_Titre_AT: UnicodeString;
begin
  Result := ChildNodes['Titre_AT'].Text;
end;

procedure TXMLAvis_Technique_Ventilation.Set_Titre_AT(Value: UnicodeString);
begin
  ChildNodes['Titre_AT'].NodeValue := Value;
end;

function TXMLAvis_Technique_Ventilation.Get_Titulaire: UnicodeString;
begin
  Result := ChildNodes['Titulaire'].Text;
end;

procedure TXMLAvis_Technique_Ventilation.Set_Titulaire(Value: UnicodeString);
begin
  ChildNodes['Titulaire'].NodeValue := Value;
end;

function TXMLAvis_Technique_Ventilation.Get_Code_Titulaire: UnicodeString;
begin
  Result := ChildNodes['Code_Titulaire'].Text;
end;

procedure TXMLAvis_Technique_Ventilation.Set_Code_Titulaire(Value: UnicodeString);
begin
  ChildNodes['Code_Titulaire'].NodeValue := Value;
end;

function TXMLAvis_Technique_Ventilation.Get_Industriel: UnicodeString;
begin
  Result := ChildNodes['Industriel'].Text;
end;

procedure TXMLAvis_Technique_Ventilation.Set_Industriel(Value: UnicodeString);
begin
  ChildNodes['Industriel'].NodeValue := Value;
end;

function TXMLAvis_Technique_Ventilation.Get_Code_Industriel: UnicodeString;
begin
  Result := ChildNodes['Code_Industriel'].Text;
end;

procedure TXMLAvis_Technique_Ventilation.Set_Code_Industriel(Value: UnicodeString);
begin
  ChildNodes['Code_Industriel'].NodeValue := Value;
end;

function TXMLAvis_Technique_Ventilation.Get_Num_AT: UnicodeString;
begin
  Result := ChildNodes['Num_AT'].Text;
end;

procedure TXMLAvis_Technique_Ventilation.Set_Num_AT(Value: UnicodeString);
begin
  ChildNodes['Num_AT'].NodeValue := Value;
end;

function TXMLAvis_Technique_Ventilation.Get_Liste_AT_Ancien: IXMLListe_AT_Ancien;
begin
  Result := ChildNodes['Liste_AT_Ancien'] as IXMLListe_AT_Ancien;
end;

function TXMLAvis_Technique_Ventilation.Get_Date_Application: UnicodeString;
begin
  Result := ChildNodes['Date_Application'].Text;
end;

procedure TXMLAvis_Technique_Ventilation.Set_Date_Application(Value: UnicodeString);
begin
  ChildNodes['Date_Application'].NodeValue := Value;
end;

function TXMLAvis_Technique_Ventilation.Get_Usages: IXMLUsages;
begin
  Result := ChildNodes['Usages'] as IXMLUsages;
end;

function TXMLAvis_Technique_Ventilation.Get_Caracteristiques: IXMLCaracteristiques;
begin
  Result := ChildNodes['Caracteristiques'] as IXMLCaracteristiques;
end;

function TXMLAvis_Technique_Ventilation.Get_NB_AT: Integer;
begin
  Result := ChildNodes['NB_AT'].NodeValue;
end;

procedure TXMLAvis_Technique_Ventilation.Set_NB_AT(Value: Integer);
begin
  ChildNodes['NB_AT'].NodeValue := Value;
end;

function TXMLAvis_Technique_Ventilation.Get_ATS: IXMLATS;
begin
  Result := ChildNodes['ATS'] as IXMLATS;
end;

function TXMLAvis_Technique_Ventilation.Get_Equipements: IXMLEquipements;
begin
  Result := ChildNodes['Equipements'] as IXMLEquipements;
end;

function TXMLAvis_Technique_Ventilation.Get_Puissances_Extracteurs: IXMLPuissances_Extracteurs;
begin
  Result := ChildNodes['Puissances_Extracteurs'] as IXMLPuissances_Extracteurs;
end;

{ TXMLListe_AT_Ancien }

procedure TXMLListe_AT_Ancien.AfterConstruction;
begin
  ItemTag := 'Num_AT_Ancien';
  ItemInterface := IXMLNode;
  inherited;
end;

function TXMLListe_AT_Ancien.Get_Num_AT_Ancien(Index: Integer): UnicodeString;
begin
  Result := List[Index].Text;
end;

function TXMLListe_AT_Ancien.Add(const Num_AT_Ancien: UnicodeString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := Num_AT_Ancien;
end;

function TXMLListe_AT_Ancien.Insert(const Index: Integer; const Num_AT_Ancien: UnicodeString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := Num_AT_Ancien;
end;

{ TXMLUsages }

function TXMLUsages.Get_Collectif: Boolean;
begin
  Result := ChildNodes['Collectif'].NodeValue;
end;

procedure TXMLUsages.Set_Collectif(Value: Boolean);
begin
  ChildNodes['Collectif'].NodeValue := Value;
end;

function TXMLUsages.Get_Individuel: Boolean;
begin
  Result := ChildNodes['Individuel'].NodeValue;
end;

procedure TXMLUsages.Set_Individuel(Value: Boolean);
begin
  ChildNodes['Individuel'].NodeValue := Value;
end;

function TXMLUsages.Get_Hotel: Boolean;
begin
  Result := ChildNodes['Hotel'].NodeValue;
end;

procedure TXMLUsages.Set_Hotel(Value: Boolean);
begin
  ChildNodes['Hotel'].NodeValue := Value;
end;

{ TXMLCaracteristiques }

function TXMLCaracteristiques.Get_Double_Flux: Boolean;
begin
  Result := ChildNodes['Double_Flux'].NodeValue;
end;

procedure TXMLCaracteristiques.Set_Double_Flux(Value: Boolean);
begin
  ChildNodes['Double_Flux'].NodeValue := Value;
end;

function TXMLCaracteristiques.Get_Autor�glable: Boolean;
begin
  Result := ChildNodes['Autor�glable'].NodeValue;
end;

procedure TXMLCaracteristiques.Set_Autor�glable(Value: Boolean);
begin
  ChildNodes['Autor�glable'].NodeValue := Value;
end;

function TXMLCaracteristiques.Get_Hygroreglable: Boolean;
begin
  Result := ChildNodes['Hygroreglable'].NodeValue;
end;

procedure TXMLCaracteristiques.Set_Hygroreglable(Value: Boolean);
begin
  ChildNodes['Hygroreglable'].NodeValue := Value;
end;

function TXMLCaracteristiques.Get_Basse_Pression: Boolean;
begin
  Result := ChildNodes['Basse_Pression'].NodeValue;
end;

procedure TXMLCaracteristiques.Set_Basse_Pression(Value: Boolean);
begin
  ChildNodes['Basse_Pression'].NodeValue := Value;
end;

function TXMLCaracteristiques.Get_Type_Extraction: UnicodeString;
begin
  Result := ChildNodes['Type_Extraction'].Text;
end;

procedure TXMLCaracteristiques.Set_Type_Extraction(Value: UnicodeString);
begin
  ChildNodes['Type_Extraction'].NodeValue := Value;
end;

{ TXMLATS }

procedure TXMLATS.AfterConstruction;
begin
  RegisterChildNode('AT', TXMLAT);
  ItemTag := 'AT';
  ItemInterface := IXMLAT;
  inherited;
end;

function TXMLATS.Get_AT(Index: Integer): IXMLAT;
begin
  Result := List[Index] as IXMLAT;
end;

function TXMLATS.Add: IXMLAT;
begin
  Result := AddItem(-1) as IXMLAT;
end;

function TXMLATS.Insert(const Index: Integer): IXMLAT;
begin
  Result := AddItem(Index) as IXMLAT;
end;

{ TXMLAT }

procedure TXMLAT.AfterConstruction;
begin
  RegisterChildNode('Coupe_Tirage_Complementaire', TXMLPieceHumide);
  RegisterChildNode('Type-EA', TXMLTypeEA);
  RegisterChildNode('CONFIGS', TXMLCONFIGS);
  inherited;
end;

function TXMLAT.Get_REF_AT: UnicodeString;
begin
  Result := ChildNodes['REF_AT'].Text;
end;

procedure TXMLAT.Set_REF_AT(Value: UnicodeString);
begin
  ChildNodes['REF_AT'].NodeValue := Value;
end;

function TXMLAT.Get_LIBELLE: UnicodeString;
begin
  Result := ChildNodes['LIBELLE'].Text;
end;

procedure TXMLAT.Set_LIBELLE(Value: UnicodeString);
begin
  ChildNodes['LIBELLE'].NodeValue := Value;
end;

function TXMLAT.Get_HYGRO_A: Boolean;
begin
  Result := ChildNodes['HYGRO_A'].NodeValue;
end;

procedure TXMLAT.Set_HYGRO_A(Value: Boolean);
begin
  ChildNodes['HYGRO_A'].NodeValue := Value;
end;

function TXMLAT.Get_HYGRO_B1: Boolean;
begin
  Result := ChildNodes['HYGRO_B1'].NodeValue;
end;

procedure TXMLAT.Set_HYGRO_B1(Value: Boolean);
begin
  ChildNodes['HYGRO_B1'].NodeValue := Value;
end;

function TXMLAT.Get_HYGRO_B2: Boolean;
begin
  Result := ChildNodes['HYGRO_B2'].NodeValue;
end;

procedure TXMLAT.Set_HYGRO_B2(Value: Boolean);
begin
  ChildNodes['HYGRO_B2'].NodeValue := Value;
end;

function TXMLAT.Get_Coupe_Tirage_Complementaire: IXMLPieceHumide;
begin
  Result := ChildNodes['Coupe_Tirage_Complementaire'] as IXMLPieceHumide;
end;

function TXMLAT.Get_GAZ: Boolean;
begin
  Result := ChildNodes['GAZ'].NodeValue;
end;

procedure TXMLAT.Set_GAZ(Value: Boolean);
begin
  ChildNodes['GAZ'].NodeValue := Value;
end;

function TXMLAT.Get_TypeEA: IXMLTypeEA;
begin
  Result := ChildNodes['Type-EA'] as IXMLTypeEA;
end;

function TXMLAT.Get_Optimisation: Boolean;
begin
  Result := ChildNodes['Optimisation'].NodeValue;
end;

procedure TXMLAT.Set_Optimisation(Value: Boolean);
begin
  ChildNodes['Optimisation'].NodeValue := Value;
end;

function TXMLAT.Get_NB_CONFIG: Integer;
begin
  Result := ChildNodes['NB_CONFIG'].NodeValue;
end;

procedure TXMLAT.Set_NB_CONFIG(Value: Integer);
begin
  ChildNodes['NB_CONFIG'].NodeValue := Value;
end;

function TXMLAT.Get_CONFIGS: IXMLCONFIGS;
begin
  Result := ChildNodes['CONFIGS'] as IXMLCONFIGS;
end;

{ TXMLPieceHumide }

function TXMLPieceHumide.Get_Code: UnicodeString;
begin
  Result := ChildNodes['Code'].Text;
end;

procedure TXMLPieceHumide.Set_Code(Value: UnicodeString);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

function TXMLPieceHumide.Get_Qvrep: Single;
begin
  Result := ChildNodes['Qvrep'].NodeValue;
end;

procedure TXMLPieceHumide.Set_Qvrep(Value: Single);
begin
  ChildNodes['Qvrep'].NodeValue := Value;
end;

{ TXMLTypeEA }

function TXMLTypeEA.Get_Presence_EA_Fixes: Boolean;
begin
  Result := ChildNodes['Presence_EA_Fixes'].NodeValue;
end;

procedure TXMLTypeEA.Set_Presence_EA_Fixes(Value: Boolean);
begin
  ChildNodes['Presence_EA_Fixes'].NodeValue := Value;
end;

function TXMLTypeEA.Get_Presence_EA_Autoreglables: Boolean;
begin
  Result := ChildNodes['Presence_EA_Autoreglables'].NodeValue;
end;

procedure TXMLTypeEA.Set_Presence_EA_Autoreglables(Value: Boolean);
begin
  ChildNodes['Presence_EA_Autoreglables'].NodeValue := Value;
end;

function TXMLTypeEA.Get_Dp1: Integer;
begin
  Result := ChildNodes['Dp1'].NodeValue;
end;

procedure TXMLTypeEA.Set_Dp1(Value: Integer);
begin
  ChildNodes['Dp1'].NodeValue := Value;
end;

function TXMLTypeEA.Get_Dp2: Integer;
begin
  Result := ChildNodes['Dp2'].NodeValue;
end;

procedure TXMLTypeEA.Set_Dp2(Value: Integer);
begin
  ChildNodes['Dp2'].NodeValue := Value;
end;

function TXMLTypeEA.Get_R_f: Single;
begin
  Result := ChildNodes['R_f'].NodeValue;
end;

procedure TXMLTypeEA.Set_R_f(Value: Single);
begin
  ChildNodes['R_f'].NodeValue := Value;
end;

{ TXMLCONFIGS }

procedure TXMLCONFIGS.AfterConstruction;
begin
  RegisterChildNode('CONFIG', TXMLCONFIG);
  ItemTag := 'CONFIG';
  ItemInterface := IXMLCONFIG;
  inherited;
end;

function TXMLCONFIGS.Get_CONFIG(Index: Integer): IXMLCONFIG;
begin
  Result := List[Index] as IXMLCONFIG;
end;

function TXMLCONFIGS.Add: IXMLCONFIG;
begin
  Result := AddItem(-1) as IXMLCONFIG;
end;

function TXMLCONFIGS.Insert(const Index: Integer): IXMLCONFIG;
begin
  Result := AddItem(Index) as IXMLCONFIG;
end;

{ TXMLCONFIG }

procedure TXMLCONFIG.AfterConstruction;
begin
  RegisterChildNode('Singularit�s', TXMLSingularit�s);
  RegisterChildNode('DEBIT_RT', TXMLDEBIT_RT);
  RegisterChildNode('LOCAUX', TXMLLOCAUX);
  inherited;
end;

function TXMLCONFIG.Get_Type_Logement: Integer;
begin
  Result := ChildNodes['Type_Logement'].NodeValue;
end;

procedure TXMLCONFIG.Set_Type_Logement(Value: Integer);
begin
  ChildNodes['Type_Logement'].NodeValue := Value;
end;

function TXMLCONFIG.Get_Singularit�s: IXMLSingularit�s;
begin
  Result := ChildNodes['Singularit�s'] as IXMLSingularit�s;
end;

function TXMLCONFIG.Get_Nb_Sdb_WC: Integer;
begin
  Result := ChildNodes['Nb_Sdb_WC'].NodeValue;
end;

procedure TXMLCONFIG.Set_Nb_Sdb_WC(Value: Integer);
begin
  ChildNodes['Nb_Sdb_WC'].NodeValue := Value;
end;

function TXMLCONFIG.Get_Nb_Sdb: Integer;
begin
  Result := ChildNodes['Nb_Sdb'].NodeValue;
end;

procedure TXMLCONFIG.Set_Nb_Sdb(Value: Integer);
begin
  ChildNodes['Nb_Sdb'].NodeValue := Value;
end;

function TXMLCONFIG.Get_Nb_WC: Integer;
begin
  Result := ChildNodes['Nb_WC'].NodeValue;
end;

procedure TXMLCONFIG.Set_Nb_WC(Value: Integer);
begin
  ChildNodes['Nb_WC'].NodeValue := Value;
end;

function TXMLCONFIG.Get_Nb_Sde: Integer;
begin
  Result := ChildNodes['Nb_Sde'].NodeValue;
end;

procedure TXMLCONFIG.Set_Nb_Sde(Value: Integer);
begin
  ChildNodes['Nb_Sde'].NodeValue := Value;
end;

function TXMLCONFIG.Get_DEBIT_RT: IXMLDEBIT_RT;
begin
  Result := ChildNodes['DEBIT_RT'] as IXMLDEBIT_RT;
end;

function TXMLCONFIG.Get_LOCAUX: IXMLLOCAUX;
begin
  Result := ChildNodes['LOCAUX'] as IXMLLOCAUX;
end;

{ TXMLSingularit�s }

function TXMLSingularit�s.Get_Config_Optimisee: Boolean;
begin
  Result := ChildNodes['Config_Optimisee'].NodeValue;
end;

procedure TXMLSingularit�s.Set_Config_Optimisee(Value: Boolean);
begin
  ChildNodes['Config_Optimisee'].NodeValue := Value;
end;

function TXMLSingularit�s.Get_Changement_Bouche: Boolean;
begin
  Result := ChildNodes['Changement_Bouche'].NodeValue;
end;

procedure TXMLSingularit�s.Set_Changement_Bouche(Value: Boolean);
begin
  ChildNodes['Changement_Bouche'].NodeValue := Value;
end;

function TXMLSingularit�s.Get_EA_Fixes: Boolean;
begin
  Result := ChildNodes['EA_Fixes'].NodeValue;
end;

procedure TXMLSingularit�s.Set_EA_Fixes(Value: Boolean);
begin
  ChildNodes['EA_Fixes'].NodeValue := Value;
end;

function TXMLSingularit�s.Get_EA_Autor�glables: Boolean;
begin
  Result := ChildNodes['EA_Autor�glables'].NodeValue;
end;

procedure TXMLSingularit�s.Set_EA_Autor�glables(Value: Boolean);
begin
  ChildNodes['EA_Autor�glables'].NodeValue := Value;
end;

{ TXMLDEBIT_RT }

procedure TXMLDEBIT_RT.AfterConstruction;
begin
  RegisterChildNode('Cdeps', TXMLCdeps);
  RegisterChildNode('Crdbnrs', TXMLCrdbnrs);
  inherited;
end;

function TXMLDEBIT_RT.Get_Cdeps: IXMLCdeps;
begin
  Result := ChildNodes['Cdeps'] as IXMLCdeps;
end;

function TXMLDEBIT_RT.Get_Crdbnrs: IXMLCrdbnrs;
begin
  Result := ChildNodes['Crdbnrs'] as IXMLCrdbnrs;
end;

function TXMLDEBIT_RT.Get_Qv_Rep: Single;
begin
  Result := ChildNodes['Qv_Rep'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_Qv_Rep(Value: Single);
begin
  ChildNodes['Qv_Rep'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_Qv_Sou: Single;
begin
  Result := ChildNodes['Qv_Sou'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_Qv_Sou(Value: Single);
begin
  ChildNodes['Qv_Sou'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_Smea_RT2012: Single;
begin
  Result := ChildNodes['Smea_RT2012'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_Smea_RT2012(Value: Single);
begin
  ChildNodes['Smea_RT2012'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_Smea_Existant: Single;
begin
  Result := ChildNodes['Smea_Existant'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_Smea_Existant(Value: Single);
begin
  ChildNodes['Smea_Existant'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_Module_1: Single;
begin
  Result := ChildNodes['Module_1'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_Module_1(Value: Single);
begin
  ChildNodes['Module_1'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_Module_2: Single;
begin
  Result := ChildNodes['Module_2'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_Module_2(Value: Single);
begin
  ChildNodes['Module_2'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_Qsupp_Sdb: Single;
begin
  Result := ChildNodes['Qsupp_Sdb'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_Qsupp_Sdb(Value: Single);
begin
  ChildNodes['Qsupp_Sdb'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_Qsupp_WC: Single;
begin
  Result := ChildNodes['Qsupp_WC'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_Qsupp_WC(Value: Single);
begin
  ChildNodes['Qsupp_WC'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_Qsupp_Sdb_WC: Single;
begin
  Result := ChildNodes['Qsupp_Sdb_WC'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_Qsupp_Sdb_WC(Value: Single);
begin
  ChildNodes['Qsupp_Sdb_WC'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_Qsupp_Cellier: Single;
begin
  Result := ChildNodes['Qsupp_Cellier'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_Qsupp_Cellier(Value: Single);
begin
  ChildNodes['Qsupp_Cellier'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_SmeaMoins_Sdb: Single;
begin
  Result := ChildNodes['SmeaMoins_Sdb'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_SmeaMoins_Sdb(Value: Single);
begin
  ChildNodes['SmeaMoins_Sdb'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_SmeaMoins_Sdb_M1: Single;
begin
  Result := ChildNodes['SmeaMoins_Sdb_M1'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_SmeaMoins_Sdb_M1(Value: Single);
begin
  ChildNodes['SmeaMoins_Sdb_M1'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_SmeaMoins_Sdb_M2: Single;
begin
  Result := ChildNodes['SmeaMoins_Sdb_M2'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_SmeaMoins_Sdb_M2(Value: Single);
begin
  ChildNodes['SmeaMoins_Sdb_M2'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_SmeaMoins_WC: Single;
begin
  Result := ChildNodes['SmeaMoins_WC'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_SmeaMoins_WC(Value: Single);
begin
  ChildNodes['SmeaMoins_WC'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_SmeaMoins_WC_M1: Single;
begin
  Result := ChildNodes['SmeaMoins_WC_M1'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_SmeaMoins_WC_M1(Value: Single);
begin
  ChildNodes['SmeaMoins_WC_M1'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_SmeaMoins_WC_M2: Single;
begin
  Result := ChildNodes['SmeaMoins_WC_M2'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_SmeaMoins_WC_M2(Value: Single);
begin
  ChildNodes['SmeaMoins_WC_M2'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_SmeaMoins_Sdb_WC: Single;
begin
  Result := ChildNodes['SmeaMoins_Sdb_WC'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_SmeaMoins_Sdb_WC(Value: Single);
begin
  ChildNodes['SmeaMoins_Sdb_WC'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_SmeaMoins_Sdb_WC_M1: Single;
begin
  Result := ChildNodes['SmeaMoins_Sdb_WC_M1'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_SmeaMoins_Sdb_WC_M1(Value: Single);
begin
  ChildNodes['SmeaMoins_Sdb_WC_M1'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_SmeaMoins_Sdb_WC_M2: Single;
begin
  Result := ChildNodes['SmeaMoins_Sdb_WC_M2'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_SmeaMoins_Sdb_WC_M2(Value: Single);
begin
  ChildNodes['SmeaMoins_Sdb_WC_M2'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_SmeaMoins_Cellier: Single;
begin
  Result := ChildNodes['SmeaMoins_Cellier'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_SmeaMoins_Cellier(Value: Single);
begin
  ChildNodes['SmeaMoins_Cellier'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_SmeaMoins_Cellier_M1: Single;
begin
  Result := ChildNodes['SmeaMoins_Cellier_M1'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_SmeaMoins_Cellier_M1(Value: Single);
begin
  ChildNodes['SmeaMoins_Cellier_M1'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_SmeaMoins_Cellier_M2: Single;
begin
  Result := ChildNodes['SmeaMoins_Cellier_M2'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_SmeaMoins_Cellier_M2(Value: Single);
begin
  ChildNodes['SmeaMoins_Cellier_M2'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_Ext_min: Single;
begin
  Result := ChildNodes['Ext_min'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_Ext_min(Value: Single);
begin
  ChildNodes['Ext_min'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_Ext_max: Single;
begin
  Result := ChildNodes['Ext_max'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_Ext_max(Value: Single);
begin
  ChildNodes['Ext_max'].NodeValue := Value;
end;

function TXMLDEBIT_RT.Get_Borne_min: Single;
begin
  Result := ChildNodes['Borne_min'].NodeValue;
end;

procedure TXMLDEBIT_RT.Set_Borne_min(Value: Single);
begin
  ChildNodes['Borne_min'].NodeValue := Value;
end;

{ TXMLCdeps }

procedure TXMLCdeps.AfterConstruction;
begin
  ItemTag := 'Cdep';
  ItemInterface := IXMLNode;
  inherited;
end;

function TXMLCdeps.Get_Cdep(Index: Integer): Single;
begin
  Result := List[Index].NodeValue;
end;

function TXMLCdeps.Add(const Cdep: Single): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := Cdep;
end;

function TXMLCdeps.Insert(const Index: Integer; const Cdep: Single): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := Cdep;
end;

{ TXMLCrdbnrs }

procedure TXMLCrdbnrs.AfterConstruction;
begin
  ItemTag := 'Crdbnr';
  ItemInterface := IXMLNode;
  inherited;
end;

function TXMLCrdbnrs.Get_Crdbnr(Index: Integer): Single;
begin
  Result := List[Index].NodeValue;
end;

function TXMLCrdbnrs.Add(const Crdbnr: Single): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := Crdbnr;
end;

function TXMLCrdbnrs.Insert(const Index: Integer; const Crdbnr: Single): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := Crdbnr;
end;

{ TXMLLOCAUX }

procedure TXMLLOCAUX.AfterConstruction;
begin
  RegisterChildNode('Cuisine', TXMLPieceHumide);
  RegisterChildNode('Salle_de_bain_WC', TXMLPieceHumide);
  RegisterChildNode('Salle_de_bain_WC2', TXMLPieceHumide);
  RegisterChildNode('Salle_de_bain_WC_supp', TXMLPieceHumide);
  RegisterChildNode('Salle_de_bain', TXMLPieceHumide);
  RegisterChildNode('Salle_de_bain2', TXMLPieceHumide);
  RegisterChildNode('Salle_de_bain_supp', TXMLPieceHumide);
  RegisterChildNode('WC_Unique', TXMLPieceHumide);
  RegisterChildNode('WC_Multiple', TXMLPieceHumide);
  RegisterChildNode('WC_Supp', TXMLPieceHumide);
  RegisterChildNode('Salle_eau_cellier', TXMLPieceHumide);
  RegisterChildNode('Salle_eau_cellier_supp', TXMLPieceHumide);
  RegisterChildNode('Salon', TXMLPieceSeche);
  RegisterChildNode('Chambre', TXMLPieceSeche);
  inherited;
end;

function TXMLLOCAUX.Get_Cuisine: IXMLPieceHumide;
begin
  Result := ChildNodes['Cuisine'] as IXMLPieceHumide;
end;

function TXMLLOCAUX.Get_Salle_de_bain_WC: IXMLPieceHumide;
begin
  Result := ChildNodes['Salle_de_bain_WC'] as IXMLPieceHumide;
end;

function TXMLLOCAUX.Get_Salle_de_bain_WC2: IXMLPieceHumide;
begin
  Result := ChildNodes['Salle_de_bain_WC2'] as IXMLPieceHumide;
end;

function TXMLLOCAUX.Get_Salle_de_bain_WC_supp: IXMLPieceHumide;
begin
  Result := ChildNodes['Salle_de_bain_WC_supp'] as IXMLPieceHumide;
end;

function TXMLLOCAUX.Get_Salle_de_bain: IXMLPieceHumide;
begin
  Result := ChildNodes['Salle_de_bain'] as IXMLPieceHumide;
end;

function TXMLLOCAUX.Get_Salle_de_bain2: IXMLPieceHumide;
begin
  Result := ChildNodes['Salle_de_bain2'] as IXMLPieceHumide;
end;

function TXMLLOCAUX.Get_Salle_de_bain_supp: IXMLPieceHumide;
begin
  Result := ChildNodes['Salle_de_bain_supp'] as IXMLPieceHumide;
end;

function TXMLLOCAUX.Get_WC_Unique: IXMLPieceHumide;
begin
  Result := ChildNodes['WC_Unique'] as IXMLPieceHumide;
end;

function TXMLLOCAUX.Get_WC_Multiple: IXMLPieceHumide;
begin
  Result := ChildNodes['WC_Multiple'] as IXMLPieceHumide;
end;

function TXMLLOCAUX.Get_WC_Supp: IXMLPieceHumide;
begin
  Result := ChildNodes['WC_Supp'] as IXMLPieceHumide;
end;

function TXMLLOCAUX.Get_Salle_eau_cellier: IXMLPieceHumide;
begin
  Result := ChildNodes['Salle_eau_cellier'] as IXMLPieceHumide;
end;

function TXMLLOCAUX.Get_Salle_eau_cellier_supp: IXMLPieceHumide;
begin
  Result := ChildNodes['Salle_eau_cellier_supp'] as IXMLPieceHumide;
end;

function TXMLLOCAUX.Get_Salon: IXMLPieceSeche;
begin
  Result := ChildNodes['Salon'] as IXMLPieceSeche;
end;

function TXMLLOCAUX.Get_Chambre: IXMLPieceSeche;
begin
  Result := ChildNodes['Chambre'] as IXMLPieceSeche;
end;

{ TXMLPieceSeche }

procedure TXMLPieceSeche.AfterConstruction;
begin
  RegisterChildNode('Entree_Solution', TXMLEntree_Solution);
  inherited;
end;

function TXMLPieceSeche.Get_Entree_Solution: IXMLEntree_Solution;
begin
  Result := ChildNodes['Entree_Solution'] as IXMLEntree_Solution;
end;

{ TXMLEntree_Solution }

function TXMLEntree_Solution.Get_Code: UnicodeString;
begin
  Result := ChildNodes['Code'].Text;
end;

procedure TXMLEntree_Solution.Set_Code(Value: UnicodeString);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

{ TXMLEquipements }

procedure TXMLEquipements.AfterConstruction;
begin
  RegisterChildNode('Bouches', TXMLEquipements_Bouches);
  RegisterChildNode('Entrees', TXMLEquipements_Entrees);
  RegisterChildNode('Solutions', TXMLSolutions);
  RegisterChildNode('Extracteurs', TXMLExtracteurs);
  inherited;
end;

function TXMLEquipements.Get_Bouches: IXMLEquipements_Bouches;
begin
  Result := ChildNodes['Bouches'] as IXMLEquipements_Bouches;
end;

function TXMLEquipements.Get_Entrees: IXMLEquipements_Entrees;
begin
  Result := ChildNodes['Entrees'] as IXMLEquipements_Entrees;
end;

function TXMLEquipements.Get_Solutions: IXMLSolutions;
begin
  Result := ChildNodes['Solutions'] as IXMLSolutions;
end;

function TXMLEquipements.Get_Extracteurs: IXMLExtracteurs;
begin
  Result := ChildNodes['Extracteurs'] as IXMLExtracteurs;
end;

{ TXMLEquipements_Bouches }

procedure TXMLEquipements_Bouches.AfterConstruction;
begin
  RegisterChildNode('Type_Bouche', TXMLEquipements_Bouches_Type_Bouche);
  ItemTag := 'Type_Bouche';
  ItemInterface := IXMLEquipements_Bouches_Type_Bouche;
  inherited;
end;

function TXMLEquipements_Bouches.Get_Type_Bouche(Index: Integer): IXMLEquipements_Bouches_Type_Bouche;
begin
  Result := List[Index] as IXMLEquipements_Bouches_Type_Bouche;
end;

function TXMLEquipements_Bouches.Add: IXMLEquipements_Bouches_Type_Bouche;
begin
  Result := AddItem(-1) as IXMLEquipements_Bouches_Type_Bouche;
end;

function TXMLEquipements_Bouches.Insert(const Index: Integer): IXMLEquipements_Bouches_Type_Bouche;
begin
  Result := AddItem(Index) as IXMLEquipements_Bouches_Type_Bouche;
end;

{ TXMLEquipements_Bouches_Type_Bouche }

procedure TXMLEquipements_Bouches_Type_Bouche.AfterConstruction;
begin
  RegisterChildNode('References', TXMLReferences);
  inherited;
end;

function TXMLEquipements_Bouches_Type_Bouche.Get_Code: UnicodeString;
begin
  Result := ChildNodes['Code'].Text;
end;

procedure TXMLEquipements_Bouches_Type_Bouche.Set_Code(Value: UnicodeString);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

function TXMLEquipements_Bouches_Type_Bouche.Get_References: IXMLReferences;
begin
  Result := ChildNodes['References'] as IXMLReferences;
end;

function TXMLEquipements_Bouches_Type_Bouche.Get_Qmin: Single;
begin
  Result := ChildNodes['Qmin'].NodeValue;
end;

procedure TXMLEquipements_Bouches_Type_Bouche.Set_Qmin(Value: Single);
begin
  ChildNodes['Qmin'].NodeValue := Value;
end;

function TXMLEquipements_Bouches_Type_Bouche.Get_QminF: Single;
begin
  Result := ChildNodes['QminF'].NodeValue;
end;

procedure TXMLEquipements_Bouches_Type_Bouche.Set_QminF(Value: Single);
begin
  ChildNodes['QminF'].NodeValue := Value;
end;

function TXMLEquipements_Bouches_Type_Bouche.Get_QminLimite: Single;
begin
  Result := ChildNodes['QminLimite'].NodeValue;
end;

procedure TXMLEquipements_Bouches_Type_Bouche.Set_QminLimite(Value: Single);
begin
  ChildNodes['QminLimite'].NodeValue := Value;
end;

function TXMLEquipements_Bouches_Type_Bouche.Get_QmaxF: Single;
begin
  Result := ChildNodes['QmaxF'].NodeValue;
end;

procedure TXMLEquipements_Bouches_Type_Bouche.Set_QmaxF(Value: Single);
begin
  ChildNodes['QmaxF'].NodeValue := Value;
end;

function TXMLEquipements_Bouches_Type_Bouche.Get_QmaxLimite: Single;
begin
  Result := ChildNodes['QmaxLimite'].NodeValue;
end;

procedure TXMLEquipements_Bouches_Type_Bouche.Set_QmaxLimite(Value: Single);
begin
  ChildNodes['QmaxLimite'].NodeValue := Value;
end;

{ TXMLReferences }

procedure TXMLReferences.AfterConstruction;
begin
  ItemTag := 'Reference';
  ItemInterface := IXMLNode;
  inherited;
end;

function TXMLReferences.Get_Reference(Index: Integer): UnicodeString;
begin
  Result := List[Index].Text;
end;

function TXMLReferences.Add(const Reference: UnicodeString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := Reference;
end;

function TXMLReferences.Insert(const Index: Integer; const Reference: UnicodeString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := Reference;
end;

{ TXMLEquipements_Entrees }

procedure TXMLEquipements_Entrees.AfterConstruction;
begin
  RegisterChildNode('Type_Entree', TXMLEquipements_Entrees_Type_Entree);
  ItemTag := 'Type_Entree';
  ItemInterface := IXMLEquipements_Entrees_Type_Entree;
  inherited;
end;

function TXMLEquipements_Entrees.Get_Type_Entree(Index: Integer): IXMLEquipements_Entrees_Type_Entree;
begin
  Result := List[Index] as IXMLEquipements_Entrees_Type_Entree;
end;

function TXMLEquipements_Entrees.Add: IXMLEquipements_Entrees_Type_Entree;
begin
  Result := AddItem(-1) as IXMLEquipements_Entrees_Type_Entree;
end;

function TXMLEquipements_Entrees.Insert(const Index: Integer): IXMLEquipements_Entrees_Type_Entree;
begin
  Result := AddItem(Index) as IXMLEquipements_Entrees_Type_Entree;
end;

{ TXMLEquipements_Entrees_Type_Entree }

procedure TXMLEquipements_Entrees_Type_Entree.AfterConstruction;
begin
  RegisterChildNode('References', TXMLReferences);
  inherited;
end;

function TXMLEquipements_Entrees_Type_Entree.Get_Code: UnicodeString;
begin
  Result := ChildNodes['Code'].Text;
end;

procedure TXMLEquipements_Entrees_Type_Entree.Set_Code(Value: UnicodeString);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

function TXMLEquipements_Entrees_Type_Entree.Get_References: IXMLReferences;
begin
  Result := ChildNodes['References'] as IXMLReferences;
end;

function TXMLEquipements_Entrees_Type_Entree.Get_EA_min: Single;
begin
  Result := ChildNodes['EA_min'].NodeValue;
end;

procedure TXMLEquipements_Entrees_Type_Entree.Set_EA_min(Value: Single);
begin
  ChildNodes['EA_min'].NodeValue := Value;
end;

function TXMLEquipements_Entrees_Type_Entree.Get_EA_max: Single;
begin
  Result := ChildNodes['EA_max'].NodeValue;
end;

procedure TXMLEquipements_Entrees_Type_Entree.Set_EA_max(Value: Single);
begin
  ChildNodes['EA_max'].NodeValue := Value;
end;

{ TXMLSolutions }

procedure TXMLSolutions.AfterConstruction;
begin
  RegisterChildNode('Type_Solution', TXMLType_Solution);
  ItemTag := 'Type_Solution';
  ItemInterface := IXMLType_Solution;
  inherited;
end;

function TXMLSolutions.Get_Type_Solution(Index: Integer): IXMLType_Solution;
begin
  Result := List[Index] as IXMLType_Solution;
end;

function TXMLSolutions.Add: IXMLType_Solution;
begin
  Result := AddItem(-1) as IXMLType_Solution;
end;

function TXMLSolutions.Insert(const Index: Integer): IXMLType_Solution;
begin
  Result := AddItem(Index) as IXMLType_Solution;
end;

{ TXMLType_Solution }

procedure TXMLType_Solution.AfterConstruction;
begin
  RegisterChildNode('Config_Solution', TXMLConfig_Solution);
  FConfig_Solution := CreateCollection(TXMLConfig_SolutionList, IXMLConfig_Solution, 'Config_Solution') as IXMLConfig_SolutionList;
  inherited;
end;

function TXMLType_Solution.Get_Code_Solution: UnicodeString;
begin
  Result := ChildNodes['Code_Solution'].Text;
end;

procedure TXMLType_Solution.Set_Code_Solution(Value: UnicodeString);
begin
  ChildNodes['Code_Solution'].NodeValue := Value;
end;

function TXMLType_Solution.Get_Config_Solution: IXMLConfig_SolutionList;
begin
  Result := FConfig_Solution;
end;

{ TXMLConfig_Solution }

procedure TXMLConfig_Solution.AfterConstruction;
begin
  RegisterChildNode('Entree', TXMLStructure_Entree);
  FEntree := CreateCollection(TXMLStructure_EntreeList, IXMLStructure_Entree, 'Entree') as IXMLStructure_EntreeList;
  inherited;
end;

function TXMLConfig_Solution.Get_Solution_Libelle: UnicodeString;
begin
  Result := ChildNodes['Solution_Libelle'].Text;
end;

procedure TXMLConfig_Solution.Set_Solution_Libelle(Value: UnicodeString);
begin
  ChildNodes['Solution_Libelle'].NodeValue := Value;
end;

function TXMLConfig_Solution.Get_Solution_Libre: Single;
begin
  Result := ChildNodes['Solution_Libre'].NodeValue;
end;

procedure TXMLConfig_Solution.Set_Solution_Libre(Value: Single);
begin
  ChildNodes['Solution_Libre'].NodeValue := Value;
end;

function TXMLConfig_Solution.Get_Entree: IXMLStructure_EntreeList;
begin
  Result := FEntree;
end;

{ TXMLConfig_SolutionList }

function TXMLConfig_SolutionList.Add: IXMLConfig_Solution;
begin
  Result := AddItem(-1) as IXMLConfig_Solution;
end;

function TXMLConfig_SolutionList.Insert(const Index: Integer): IXMLConfig_Solution;
begin
  Result := AddItem(Index) as IXMLConfig_Solution;
end;

function TXMLConfig_SolutionList.Get_Item(Index: Integer): IXMLConfig_Solution;
begin
  Result := List[Index] as IXMLConfig_Solution;
end;

{ TXMLStructure_Entree }

function TXMLStructure_Entree.Get_Code: UnicodeString;
begin
  Result := ChildNodes['Code'].Text;
end;

procedure TXMLStructure_Entree.Set_Code(Value: UnicodeString);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

function TXMLStructure_Entree.Get_Nombre: Integer;
begin
  Result := ChildNodes['Nombre'].NodeValue;
end;

procedure TXMLStructure_Entree.Set_Nombre(Value: Integer);
begin
  ChildNodes['Nombre'].NodeValue := Value;
end;

{ TXMLStructure_EntreeList }

function TXMLStructure_EntreeList.Add: IXMLStructure_Entree;
begin
  Result := AddItem(-1) as IXMLStructure_Entree;
end;

function TXMLStructure_EntreeList.Insert(const Index: Integer): IXMLStructure_Entree;
begin
  Result := AddItem(Index) as IXMLStructure_Entree;
end;

function TXMLStructure_EntreeList.Get_Item(Index: Integer): IXMLStructure_Entree;
begin
  Result := List[Index] as IXMLStructure_Entree;
end;

{ TXMLExtracteurs }

procedure TXMLExtracteurs.AfterConstruction;
begin
  RegisterChildNode('Type_Extracteur', TXMLType_Extracteur);
  ItemTag := 'Type_Extracteur';
  ItemInterface := IXMLType_Extracteur;
  inherited;
end;

function TXMLExtracteurs.Get_Type_Extracteur(Index: Integer): IXMLType_Extracteur;
begin
  Result := List[Index] as IXMLType_Extracteur;
end;

function TXMLExtracteurs.Add: IXMLType_Extracteur;
begin
  Result := AddItem(-1) as IXMLType_Extracteur;
end;

function TXMLExtracteurs.Insert(const Index: Integer): IXMLType_Extracteur;
begin
  Result := AddItem(Index) as IXMLType_Extracteur;
end;

{ TXMLType_Extracteur }

procedure TXMLType_Extracteur.AfterConstruction;
begin
  RegisterChildNode('References', TXMLReferences);
  inherited;
end;

function TXMLType_Extracteur.Get_References: IXMLReferences;
begin
  Result := ChildNodes['References'] as IXMLReferences;
end;

function TXMLType_Extracteur.Get_N_Cdep: Single;
begin
  Result := ChildNodes['N_Cdep'].NodeValue;
end;

procedure TXMLType_Extracteur.Set_N_Cdep(Value: Single);
begin
  ChildNodes['N_Cdep'].NodeValue := Value;
end;

function TXMLType_Extracteur.Get_N_Crdbnr: Single;
begin
  Result := ChildNodes['N_Crdbnr'].NodeValue;
end;

procedure TXMLType_Extracteur.Set_N_Crdbnr(Value: Single);
begin
  ChildNodes['N_Crdbnr'].NodeValue := Value;
end;

function TXMLType_Extracteur.Get_Libelle_Cdep: UnicodeString;
begin
  Result := ChildNodes['Libelle_Cdep'].Text;
end;

procedure TXMLType_Extracteur.Set_Libelle_Cdep(Value: UnicodeString);
begin
  ChildNodes['Libelle_Cdep'].NodeValue := Value;
end;

{ TXMLPuissances_Extracteurs }

procedure TXMLPuissances_Extracteurs.AfterConstruction;
begin
  RegisterChildNode('ATS_PUISSANCES', TXMLATS_PUISSANCES);
  inherited;
end;

function TXMLPuissances_Extracteurs.Get_ATS_PUISSANCES: IXMLATS_PUISSANCES;
begin
  Result := ChildNodes['ATS_PUISSANCES'] as IXMLATS_PUISSANCES;
end;

{ TXMLATS_PUISSANCES }

procedure TXMLATS_PUISSANCES.AfterConstruction;
begin
  RegisterChildNode('AT_PUISSANCES', TXMLAT_PUISSANCES);
  ItemTag := 'AT_PUISSANCES';
  ItemInterface := IXMLAT_PUISSANCES;
  inherited;
end;

function TXMLATS_PUISSANCES.Get_AT_PUISSANCES(Index: Integer): IXMLAT_PUISSANCES;
begin
  Result := List[Index] as IXMLAT_PUISSANCES;
end;

function TXMLATS_PUISSANCES.Add: IXMLAT_PUISSANCES;
begin
  Result := AddItem(-1) as IXMLAT_PUISSANCES;
end;

function TXMLATS_PUISSANCES.Insert(const Index: Integer): IXMLAT_PUISSANCES;
begin
  Result := AddItem(Index) as IXMLAT_PUISSANCES;
end;

{ TXMLAT_PUISSANCES }

procedure TXMLAT_PUISSANCES.AfterConstruction;
begin
  RegisterChildNode('Caisson', TXMLCaisson);
  FCaisson := CreateCollection(TXMLCaissonList, IXMLCaisson, 'Caisson') as IXMLCaissonList;
  inherited;
end;

function TXMLAT_PUISSANCES.Get_REFERENCE_AT: UnicodeString;
begin
  Result := ChildNodes['REFERENCE_AT'].Text;
end;

procedure TXMLAT_PUISSANCES.Set_REFERENCE_AT(Value: UnicodeString);
begin
  ChildNodes['REFERENCE_AT'].NodeValue := Value;
end;

function TXMLAT_PUISSANCES.Get_Caisson: IXMLCaissonList;
begin
  Result := FCaisson;
end;

{ TXMLCaisson }

procedure TXMLCaisson.AfterConstruction;
begin
  RegisterChildNode('CONFIG_PUISSANCES', TXMLCONFIG_PUISSANCES);
  FCONFIG_PUISSANCES := CreateCollection(TXMLCONFIG_PUISSANCESList, IXMLCONFIG_PUISSANCES, 'CONFIG_PUISSANCES') as IXMLCONFIG_PUISSANCESList;
  inherited;
end;

function TXMLCaisson.Get_Reference_Caisson: UnicodeString;
begin
  Result := ChildNodes['Reference_Caisson'].Text;
end;

procedure TXMLCaisson.Set_Reference_Caisson(Value: UnicodeString);
begin
  ChildNodes['Reference_Caisson'].NodeValue := Value;
end;

function TXMLCaisson.Get_CONFIG_PUISSANCES: IXMLCONFIG_PUISSANCESList;
begin
  Result := FCONFIG_PUISSANCES;
end;

{ TXMLCaissonList }

function TXMLCaissonList.Add: IXMLCaisson;
begin
  Result := AddItem(-1) as IXMLCaisson;
end;

function TXMLCaissonList.Insert(const Index: Integer): IXMLCaisson;
begin
  Result := AddItem(Index) as IXMLCaisson;
end;

function TXMLCaissonList.Get_Item(Index: Integer): IXMLCaisson;
begin
  Result := List[Index] as IXMLCaisson;
end;

{ TXMLCONFIG_PUISSANCES }

function TXMLCONFIG_PUISSANCES.Get_Type_Logement_Puissances: Integer;
begin
  Result := ChildNodes['Type_Logement_Puissances'].NodeValue;
end;

procedure TXMLCONFIG_PUISSANCES.Set_Type_Logement_Puissances(Value: Integer);
begin
  ChildNodes['Type_Logement_Puissances'].NodeValue := Value;
end;

function TXMLCONFIG_PUISSANCES.Get_Nbre_Sdb_WC: Integer;
begin
  Result := ChildNodes['Nbre_Sdb_WC'].NodeValue;
end;

procedure TXMLCONFIG_PUISSANCES.Set_Nbre_Sdb_WC(Value: Integer);
begin
  ChildNodes['Nbre_Sdb_WC'].NodeValue := Value;
end;

function TXMLCONFIG_PUISSANCES.Get_Nbre_Sdb: Integer;
begin
  Result := ChildNodes['Nbre_Sdb'].NodeValue;
end;

procedure TXMLCONFIG_PUISSANCES.Set_Nbre_Sdb(Value: Integer);
begin
  ChildNodes['Nbre_Sdb'].NodeValue := Value;
end;

function TXMLCONFIG_PUISSANCES.Get_Nbre_WC: Integer;
begin
  Result := ChildNodes['Nbre_WC'].NodeValue;
end;

procedure TXMLCONFIG_PUISSANCES.Set_Nbre_WC(Value: Integer);
begin
  ChildNodes['Nbre_WC'].NodeValue := Value;
end;

function TXMLCONFIG_PUISSANCES.Get_Nbre_SdE: Integer;
begin
  Result := ChildNodes['Nbre_SdE'].NodeValue;
end;

procedure TXMLCONFIG_PUISSANCES.Set_Nbre_SdE(Value: Integer);
begin
  ChildNodes['Nbre_SdE'].NodeValue := Value;
end;

function TXMLCONFIG_PUISSANCES.Get_Puissance_Qv: Single;
begin
  Result := ChildNodes['Puissance_Qv'].NodeValue;
end;

procedure TXMLCONFIG_PUISSANCES.Set_Puissance_Qv(Value: Single);
begin
  ChildNodes['Puissance_Qv'].NodeValue := Value;
end;

{ TXMLCONFIG_PUISSANCESList }

function TXMLCONFIG_PUISSANCESList.Add: IXMLCONFIG_PUISSANCES;
begin
  Result := AddItem(-1) as IXMLCONFIG_PUISSANCES;
end;

function TXMLCONFIG_PUISSANCESList.Insert(const Index: Integer): IXMLCONFIG_PUISSANCES;
begin
  Result := AddItem(Index) as IXMLCONFIG_PUISSANCES;
end;

function TXMLCONFIG_PUISSANCESList.Get_Item(Index: Integer): IXMLCONFIG_PUISSANCES;
begin
  Result := List[Index] as IXMLCONFIG_PUISSANCES;
end;

end.