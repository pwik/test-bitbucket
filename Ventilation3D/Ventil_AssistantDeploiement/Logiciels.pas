unit Logiciels;

interface

uses
  Classes,
  CAD_Containers_Base,
  PackagesCommuns,
  PackagesLogiciels,
  Serveurs,
  Config_XML;

type

  TPropriete = class
  private
    Function IsExeName : Boolean;
    Function IsNomCompletLogiciel : Boolean;
    Function IsNomIndustriel : Boolean;
    Function IsIDPatchNote : Boolean;
    Function IsLocalPath : Boolean;
    Function IsIDSetup : Boolean;
  public
    XML : IXMLPropriete
  end;

  TListeProprietes = Class(TTypedListAutoCreateDestroy<TPropriete>)
  private
    Function GetID_ExeName : String;
    Function GetID_NomCompletLogiciel : String;
    Function GetID_NomIndustriel : String;
    Function GetID_IDPatchNote : String;
    Function GetID_LocalPath : String;
    Function GetID_IDSetup : String;
  end;

  TOption = class
  private
    Function HasFichierApplication : Boolean;
    Function GetFichierApplication(_PathLogiciel : String) : TStringList;
  public
    XML : IXMLOption
  end;

  TListeOptions = TTypedListAutoCreateDestroy<TOption>;

  TModeCreateLogiciel = (mclNew, mclLoad, mclClone);

  TLogiciel = class
    constructor Create;
    Destructor Destroy; override;
  private
    Serveurs : TServeurs;
    Options : TListeOptions;
    Proprietes : TListeProprietes;
    PackagesCommuns : TListePackagesCommuns;
    Function GetPathLogicielLocal : String;
    Function GetPathDownloadClient(_SlashFin : Boolean) : String;
    Function GetPathDownloadDev(_BackSlashFin : Boolean) : String;
    Function GetPathFolderUpdate(_BackSlashFin : Boolean) : String;
    Function GetPathFolderFabricantSetup(_BackSlashFin : Boolean) : String;
    Function GetPathFolderSetup(_BackSlashFin : Boolean) : String;
    Function GetPathFolderBinariesSetup(_BackSlashFin : Boolean) : String;
    Function GetPathFolderInstallateurSetup(_BackSlashFin : Boolean) : String;
    Function GetPathSetup : String;
    Function GetPathFolderBackUp(_BackSlashFin : Boolean) : String;
    Function GetPathBackUpSetup(_Version : String; _Date : String) : String;
    Function GetPathBackUpPackage(_Version : String; _Date : String) : String;
    function GetPathFichierDateBackUpSetup : String;
    function GetPathFichierDateBackUpPackage : String;
    Function GetPathFolderBackUpExtractPath(_BackSlashFin : Boolean) : String;
    Function GetPathISS : String;
    function GetPathFichierDateUpdater : String;
    function GetPathInnoUnp : String;
    procedure GetDatesPackagesSurServeur;
    procedure UpdateServeurSetupPackages;
    procedure UpdateServeurUpdaterPackages;
    procedure UpdateServeurUpdaterDates;
    procedure SetNomCourtLogiciel(_Value : String);
    procedure SetNomLongLogiciel(_Value : String);
    procedure SetNomIndustriel(_Value : String);
    procedure SetIDSetup(_Value : String);
    Function GetFichiersApplication(_PathLogiciel : String) : TStringList;
  public
    XML : IXMLSoftware;
    PackagesLogiciels : TListePackagesLogiciels;
    Procedure GenereScriptISS;
    Procedure GenerationSetup;
    procedure SaveSetup;
    procedure RecupPackagesCommuns;
    procedure AddVersionFichierDate(_DateFile : String; _FullPathElt : String; _DateElt : String; _VersionElt : String);
    procedure ExtractAndZipSetup(_ProcessVisible : Boolean);
    procedure DeploiementUpdate(_Setup : Boolean; _Updater : Boolean; _GenereISS : Boolean; _SaveSetup : Boolean; _SaveZip : Boolean; _SilentExtract : Boolean);
    function UploadLastSetupSurServeurDownload : String;
    function GetURLPatchNoteDev : String;
    function GetURLPatchNoteClient : String;
    function UploadVersionDevSurServeurDownload : String;
    function GetNomLongLogiciel : String;
    function GetNomCourtLogiciel : String;
    function GetNomIndustriel : String;
    function GetIDPatchNote : Integer;
    function GetRepertoireLocalLogiciel : String;
    function ExistProperty(_ID : String) : Boolean;
    function GetProperty(_ID : String) : Variant;
    function ExistPropertyChaineISS(_ID : String) : Boolean;
    function GetPropertyChaineISS(_ID : String) : String;
    function ExistPackageCommun(_ID : String) : Boolean;
    function GetPackageCommun(_ID : String) : Boolean;
    procedure SetPackageCommun(_ID : String; _Value : Boolean);
    function ExistOption(_ID : String) : Boolean;
    function GetOption(_ID : String) : Boolean;
    function ExistOptionChaineISS(_ID : String) : Boolean;
    function GetOptionChaineISS(_ID : String) : String;
    procedure SetProperty(_ID : String; _Value : Variant);
    procedure SetOption(_ID : String; _Value : Boolean);
    procedure GenereNewLogiciel(_XMLSoftware : IXMLSoftware; _Options : TListeOptions; _Proprietes : TListeProprietes; _PackagesLogiciels : TListePackagesLogiciels; _PackagesCommuns : TListePackagesCommuns; _Serveurs : TServeurs; _Mode : TModeCreateLogiciel);
  end;

  TListeLogiciels = Class(TTypedListAutoCreateDestroy<TLogiciel>)
    constructor Create;
  private
    FIndexLogicielCourant : Integer;
    Procedure Set_IndexLogicielCourant(_Value : Integer);
  public
    function LogicielCourant : TLogiciel;
    function GenereNewLogiciel(_XMLSoftware : IXMLSoftware; _Options : TListeOptions; _Proprietes : TListeProprietes; _PackagesLogiciels : TListePackagesLogiciels; _PackagesCommuns : TListePackagesCommuns; _Serveurs : TServeurs; _Mode : TModeCreateLogiciel) : TLogiciel;
    procedure SupprimeLogiciel(_XMLSoftwares : IXMLSoftwares; _Index : Integer);
    Property IndexLogicielCourant : Integer read FIndexLogicielCourant write Set_IndexLogicielCourant;
  end;


implementation

uses
  XMLDoc, XMLIntf,
  Zip,
  CAD_ViloUtils,
  BBS_Progress,
  BBS_Message,
  Vcl.Dialogs,
  Vcl.Controls,
  ServiceWebSetup,
  Utils,
  SysUtils;

//*****************************************************************************
//##############################################################################
//# TPropriete                                                                 #
//##############################################################################
//******************************************************************************
Function TPropriete.IsExeName : Boolean;
begin
  Result := False;
    if XML.IsExeName <> '' then
      Result := StrToBool(XML.IsExeName);
end;
//******************************************************************************
Function TPropriete.IsNomCompletLogiciel : Boolean;
begin
  Result := False;
    if XML.IsNomCompletLogiciel <> '' then
      Result := StrToBool(XML.IsNomCompletLogiciel);
end;
//******************************************************************************
Function TPropriete.IsNomIndustriel : Boolean;
begin
  Result := False;
    if XML.IsNomIndustriel <> '' then
      Result := StrToBool(XML.IsNomIndustriel);
end;
//******************************************************************************
Function TPropriete.IsIDPatchNote : Boolean;
begin
  Result := False;
    if XML.IsIDPatchNote <> '' then
      Result := StrToBool(XML.IsIDPatchNote);
end;
//******************************************************************************
Function TPropriete.IsLocalPath : Boolean;
begin
  Result := False;
    if XML.IsLocalPath <> '' then
      Result := StrToBool(XML.IsLocalPath);
end;
//******************************************************************************
Function TPropriete.IsIDSetup : Boolean;
begin
  Result := False;
    if XML.IsIDSetup <> '' then
      Result := StrToBool(XML.IsIDSetup);
end;
//******************************************************************************

//*****************************************************************************
//##############################################################################
//# TListeProprietes                                                           #
//##############################################################################
//******************************************************************************
Function TListeProprietes.GetID_ExeName : String;
Var
  cpt : Integer;
begin
  Result := '';
    for cpt := 0 to Count - 1 do
      if Items[cpt].IsExeName then
        begin
        Result := Items[cpt].XML.ID;
        end;
end;
//*****************************************************************************
Function TListeProprietes.GetID_NomCompletLogiciel : String;
Var
  cpt : Integer;
begin
  Result := '';
    for cpt := 0 to Count - 1 do
      if Items[cpt].IsNomCompletLogiciel then
        begin
        Result := Items[cpt].XML.ID;
        end;
end;
//*****************************************************************************
Function TListeProprietes.GetID_NomIndustriel : String;
Var
  cpt : Integer;
begin
  Result := '';
    for cpt := 0 to Count - 1 do
      if Items[cpt].IsNomIndustriel then
        begin
        Result := Items[cpt].XML.ID;
        end;
end;
//*****************************************************************************
Function TListeProprietes.GetID_IDPatchNote : String;
Var
  cpt : Integer;
begin
  Result := '';
    for cpt := 0 to Count - 1 do
      if Items[cpt].IsIDPatchNote then
        begin
        Result := Items[cpt].XML.ID;
        end;
end;
//*****************************************************************************
Function TListeProprietes.GetID_LocalPath : String;
Var
  cpt : Integer;
begin
  Result := '';
    for cpt := 0 to Count - 1 do
      if Items[cpt].IsLocalPath then
        begin
        Result := Items[cpt].XML.ID;
        end;
end;
//*****************************************************************************
Function TListeProprietes.GetID_IDSetup : String;
Var
  cpt : Integer;
begin
  Result := '';
    for cpt := 0 to Count - 1 do
      if Items[cpt].IsIDSetup then
        begin
        Result := Items[cpt].XML.ID;
        end;
end;
//*****************************************************************************

//*****************************************************************************
//##############################################################################
//# TOption                                                                    #
//##############################################################################
//******************************************************************************
Function TOption.HasFichierApplication : Boolean;
begin
  Result := (XML.FichiersPackageApplication.Fichiers.Count > 0) or (XML.FichiersPackageApplication.Repertoires.Count > 0);
end;
//*****************************************************************************
Function TOption.GetFichierApplication(_PathLogiciel : String) : TStringList;
Var
  cpt : Integer;
  FilesDirectory : TStringList;
  DirectoryDirectory : TStringList;
  cptFilesDirectory : Integer;
begin
  Result := TStringList.Create;

  for cpt := 0 to XML.FichiersPackageApplication.Fichiers.Count - 1 do
    Result.Add(XML.FichiersPackageApplication.Fichiers[cpt].Cible + XML.FichiersPackageApplication.Fichiers[cpt].Valeur);

  for cpt := 0 to XML.FichiersPackageApplication.Repertoires.Count - 1 do
    begin
    FilesDirectory := TStringList.Create;
    DirectoryDirectory := TStringList.Create;
    Util_ScruterRepertoire(_PathLogiciel + XML.FichiersPackageApplication.Repertoires[cpt].Valeur, FilesDirectory, DirectoryDirectory);
      for cptFilesDirectory := 0 to FilesDirectory.Count - 1 do
        begin
        FilesDirectory[cptFilesDirectory] := StrReplace(FilesDirectory[cptFilesDirectory], _PathLogiciel, '');
        Result.Add(FilesDirectory[cptFilesDirectory]);
        end;
    FilesDirectory.Destroy;
    DirectoryDirectory.Destroy;
    end;
end;
//*****************************************************************************
//##############################################################################
//# TLogiciel                                                                  #
//##############################################################################
//******************************************************************************
constructor TLogiciel.Create;
begin
  PackagesLogiciels := TListePackagesLogiciels.Create;
end;
//******************************************************************************
Destructor TLogiciel.Destroy;
begin
  PackagesLogiciels.Destroy;
end;
//******************************************************************************
procedure TLogiciel.SetNomCourtLogiciel(_Value : String);
Var
  tempID : String;
begin
  tempID := Proprietes.GetID_ExeName;
    if tempID <> '' then
      SetProperty(tempID, _Value);
end;
//******************************************************************************
procedure TLogiciel.SetNomLongLogiciel(_Value : String);
Var
  tempID : String;
begin
  tempID := Proprietes.GetID_NomCompletLogiciel;
    if tempID <> '' then
      SetProperty(tempID, _Value);
end;
//******************************************************************************
procedure TLogiciel.SetNomIndustriel(_Value : String);
Var
  tempID : String;
begin
  tempID := Proprietes.GetID_NomIndustriel;
    if tempID <> '' then
      SetProperty(tempID, _Value);
end;
//******************************************************************************
procedure TLogiciel.SetIDSetup(_Value : String);
Var
  tempID : String;
begin
  tempID := Proprietes.GetID_IDSetup;
    if tempID <> '' then
      SetProperty(tempID, _Value);
end;
//******************************************************************************
Function TLogiciel.GetFichiersApplication(_PathLogiciel : String) : TStringList;
Var
  cpt : Integer;
begin
  Result := TStringList.Create;
    for cpt := 0 to Options.Count - 1 do
      if  GetOption(Options[cpt].XML.ID) and Options[cpt].HasFichierApplication then
        Result.AddStrings(Options[cpt].GetFichierApplication(_PathLogiciel));
end;
//******************************************************************************
procedure TLogiciel.DeploiementUpdate(_Setup : Boolean; _Updater : Boolean; _GenereISS : Boolean; _SaveSetup : Boolean; _SaveZip : Boolean; _SilentExtract : Boolean);
Var
  CanUpdate : Boolean;
begin
  if _Setup then
    begin
    CanUpdate := True;
      if Not(Util_RepertoireSurDisque(GetPathFolderSetup(False))) then
        begin
        CanUpdate := False;
          if BBS_ShowMessage('R�pertoire non pr�sent sur le serveur de Setup. Le cr�er?',  mtConfirmation, [mbYes,mbNo] ,0) = mrYes then
            begin
              if Not(Util_RepertoireSurDisque(GetPathFolderFabricantSetup(False))) then
                            Util_CreateDirectory(GetPathFolderFabricantSetup(False));
              if Not(Util_RepertoireSurDisque(GetPathFolderSetup(False))) then
                            Util_CreateDirectory(GetPathFolderSetup(False));
              if Not(Util_RepertoireSurDisque(GetPathFolderBinariesSetup(False))) then
                            Util_CreateDirectory(GetPathFolderBinariesSetup(False));
              if Not(Util_RepertoireSurDisque(GetPathFolderInstallateurSetup(False))) then
                            Util_CreateDirectory(GetPathFolderInstallateurSetup(False));
            end;
        end;

      if CanUpdate then
        begin
          if _SaveSetup then
            SaveSetup;
          if _SaveZip then
            ExtractAndZipSetup(Not(_SilentExtract));
          if _GenereISS then
            GenereScriptISS;
        UpdateServeurSetupPackages;
        GenerationSetup;
        end;

    end;

  if _Updater then
    begin
    CanUpdate := True;
      if Not(Util_RepertoireSurDisque(GetPathFolderUpdate(False))) then
          if BBS_ShowMessage('R�pertoire non pr�sent sur le serveur d''update. Le cr�er?',  mtConfirmation, [mbYes,mbNo] ,0) = mrYes then
            begin
            Util_CreateDirectory(GetPathFolderUpdate(False));
            Util_CreateDirectory(GetPathFolderUpdate(True) + 'Logs');
            end
          else
            CanUpdate := False;

      if CanUpdate then
        begin
        UpdateServeurUpdaterDates;
        UpdateServeurUpdaterPackages;
        end;
    end;
end;
//******************************************************************************
function TLogiciel.UploadLastSetupSurServeurDownload : String;
Var
  Zip : TZipFile;
  VersionLogiciel : String;
  DateLogiciel : String;
  tempFileName : String;
  tempString : String;
begin
  FBeginProgress(100, 'Upload du dernier Setup sur le serveur de t�l�chargement', True, '', True);
  VersionLogiciel := Util_ApplicationVersion(GetPathSetup);
  DateLogiciel := Util_FileDate(GetPathSetup);
  tempFileName := 'Setup' + GetNomCourtLogiciel + '-' + VersionLogiciel + '-' + DateLogiciel + '.zip';
  try
  Zip := TZipFile.Create;
  Zip.Open(GetPathDownloadDev(True) + tempFileName, zmWrite);
  tempString := GetPathSetup;
  tempString := StrReplace(tempString , '\', '/', False, True);
  Zip.Add(tempString, 'Setup' + GetNomCourtLogiciel + '.exe');
  Zip.Close;
  finally
  Zip.Free;
  end;
  Result := GetPathDownloadClient(True) + tempFileName;
  FEndProgress;
end;
//******************************************************************************
function TLogiciel.UploadVersionDevSurServeurDownload : String;
Var
  tempPackageApplication : TPackageLogiciel;
  FichiersOptionApplication : TStringList;
  Zip : TZipFile;
  cpt : Integer;
  VersionLogiciel : String;
  DateLogiciel : String;
  tempFileName : String;
begin
  FBeginProgress(100, 'Upload de la version de d�veloppement sur le serveur de t�l�chargement', True, '', True);
  VersionLogiciel := Util_ApplicationVersion(GetPathLogicielLocal);
  DateLogiciel := Util_FileDate(GetPathLogicielLocal);
  tempFileName := GetNomCourtLogiciel + '-' + VersionLogiciel + '-' + DateLogiciel + '.zip';
  FichiersOptionApplication := GetFichiersApplication(GetRepertoireLocalLogiciel + '\');
  tempPackageApplication := PackagesLogiciels.GetPackageApplication;
  tempPackageApplication.FichiersTraitementSpecial.Clear;
  tempPackageApplication.FichiersTraitementSpecial.Add(GetNomCourtLogiciel + '.exe');
  tempPackageApplication.FichiersTraitementSpecial.AddStrings(FichiersOptionApplication);
  try
  Zip := TZipFile.Create;
  Zip.Open(GetPathDownloadDev(True) + tempFileName, zmWrite);
  PackagesLogiciels.UpdateVersionDev(Zip, GetRepertoireLocalLogiciel);
    for cpt := 0 to PackagesCommuns.Count - 1 do
      if GetPackageCommun(PackagesCommuns[cpt].XML.ID) then
        PackagesCommuns[cpt].ZipFichiersPourVersionDev(Serveurs.XML.Serveur_Setup, Zip);
  FichiersOptionApplication.Destroy;

  Zip.Close;
  finally
  Zip.Free;
  end;
  Result := GetPathDownloadClient(True) + tempFileName;
  FEndProgress;
end;
//******************************************************************************
function TLogiciel.GetURLPatchNoteDev : String;
begin
  Result := Serveurs.XML.Serveur_PatchNote_Dev + IntToStr(GetIDPatchNote);
end;
//******************************************************************************
function TLogiciel.GetURLPatchNoteClient : String;
begin
  Result := Serveurs.XML.Serveur_PatchNote_Client + GetNomCourtLogiciel;
end;
//******************************************************************************
function TLogiciel.GetNomLongLogiciel : String;
Var
  tempID : String;
begin
  Result := '';
  tempID := Proprietes.GetID_NomCompletLogiciel;
    if tempID <> '' then
      Result := GetProperty(tempID);
end;
//******************************************************************************
function TLogiciel.GetNomCourtLogiciel : String;
Var
  tempID : String;
begin
  Result := '';
  tempID := Proprietes.GetID_ExeName;
    if tempID <> '' then
      Result := GetProperty(tempID);
end;
//******************************************************************************
function TLogiciel.GetNomIndustriel : String;
Var
  tempID : String;
begin
  Result := '';
  tempID := Proprietes.GetID_NomIndustriel;
    if tempID <> '' then
      Result := GetProperty(tempID);
end;
//******************************************************************************
function TLogiciel.GetIDPatchNote : Integer;
Var
  tempID : String;
  tempValue : String;
begin
  Result := 0;
  tempID := Proprietes.GetID_IDPatchNote;
    if tempID <> '' then
      begin
      tempValue := GetProperty(tempID);
        if Trim(tempValue) <> '' then
          Result := StrToInt(GetProperty(tempID));
      end;
end;
//******************************************************************************
function TLogiciel.GetRepertoireLocalLogiciel : String;
Var
  tempID : String;
begin
  Result := '';
  tempID := Proprietes.GetID_LocalPath;
    if tempID <> '' then
      Result := GetProperty(tempID);
end;
//******************************************************************************
function TLogiciel.ExistProperty(_ID : String) : Boolean;
Var
  cpt : Integer;
begin
  Result := False;
  for cpt := 0 to XML.Proprietes_Logiciel.Count - 1 do
    if UpperCase(XML.Proprietes_Logiciel[cpt].ID) = UpperCase(_ID) then
      begin
        Result := True;
        Exit;
      end;
end;
//******************************************************************************
function TLogiciel.GetProperty(_ID : String) : Variant;
Var
  cpt : Integer;
begin
  for cpt := 0 to XML.Proprietes_Logiciel.Count - 1 do
    if UpperCase(XML.Proprietes_Logiciel[cpt].ID) = UpperCase(_ID) then
      begin
        Result := XML.Proprietes_Logiciel[cpt].Valeur;
        Exit;
      end;
end;
//******************************************************************************
function TLogiciel.ExistPropertyChaineISS(_ID : String) : Boolean;
Var
  cpt : Integer;
begin
  Result := False;
  for cpt := 0 to Proprietes.Count - 1 do
    if UpperCase(Proprietes[cpt].XML.ID) = UpperCase(_ID) then
      begin
        Result := Proprietes[cpt].XML.ChaineISS <> '';
        Exit;
      end;
end;
//******************************************************************************
function TLogiciel.GetPropertyChaineISS(_ID : String) : String;
Var
  cpt : Integer;
begin
  Result := '';
  for cpt := 0 to Proprietes.Count - 1 do
    if UpperCase(Proprietes[cpt].XML.ID) = UpperCase(_ID) then
      begin
        Result := Proprietes[cpt].XML.ChaineISS;
        Exit;
      end;
end;
//******************************************************************************
function TLogiciel.ExistPackageCommun(_ID : String) : Boolean;
Var
  cpt : Integer;
begin
  Result := False;
  for cpt := 0 to XML.Packages_Communs_Logiciel.Count - 1 do
    if UpperCase(XML.Packages_Communs_Logiciel[cpt].ID) = UpperCase(_ID) then
      begin
        Result := True;
        Exit;
      end;
end;
//******************************************************************************
function TLogiciel.GetPackageCommun(_ID : String) : Boolean;
Var
  cpt : Integer;
begin
  for cpt := 0 to XML.Packages_Communs_Logiciel.Count - 1 do
    if UpperCase(XML.Packages_Communs_Logiciel[cpt].ID) = UpperCase(_ID) then
      begin
        if XML.Packages_Communs_Logiciel[cpt].Valeur = '' then
          Result := False
        else
          Result := StrToBool(XML.Packages_Communs_Logiciel[cpt].Valeur);
        Exit;
      end;
end;
//******************************************************************************
procedure TLogiciel.SetPackageCommun(_ID : String; _Value : Boolean);
Var
  cpt : Integer;
begin
  for cpt := 0 to XML.Packages_Communs_Logiciel.Count - 1 do
    if UpperCase(XML.Packages_Communs_Logiciel[cpt].ID) = UpperCase(_ID) then
      begin
      XML.Packages_Communs_Logiciel[cpt].Valeur := BoolToStr(_Value);
      Exit;
      end;
end;
//******************************************************************************
function TLogiciel.ExistOption(_ID : String) : Boolean;
Var
  cpt : Integer;
begin
  Result := False;
  for cpt := 0 to XML.Options_Logiciel.Count - 1 do
    if UpperCase(XML.Options_Logiciel[cpt].ID) = UpperCase(_ID) then
      begin
        Result := True;
        Exit;
      end;
end;
//******************************************************************************
function TLogiciel.GetOption(_ID : String) : Boolean;
Var
  cpt : Integer;
begin
  for cpt := 0 to XML.Options_Logiciel.Count - 1 do
    if UpperCase(XML.Options_Logiciel[cpt].ID) = UpperCase(_ID) then
      begin
        if XML.Options_Logiciel[cpt].Valeur = '' then
          Result := False
        else
          Result := StrToBool(XML.Options_Logiciel[cpt].Valeur);
        Exit;
      end;
end;
//******************************************************************************
function TLogiciel.ExistOptionChaineISS(_ID : String) : Boolean;
Var
  cpt : Integer;
begin
  Result := False;
  for cpt := 0 to Options.Count - 1 do
    if UpperCase(Options[cpt].XML.ID) = UpperCase(_ID) then
      begin
        Result := Options[cpt].XML.ChaineISS <> '';
        Exit;
      end;
end;
//******************************************************************************
function TLogiciel.GetOptionChaineISS(_ID : String) : String;
Var
  cpt : Integer;
begin
  Result := '';
  for cpt := 0 to Options.Count - 1 do
    if UpperCase(Options[cpt].XML.ID) = UpperCase(_ID) then
      begin
        Result := Options[cpt].XML.ChaineISS;
        Exit;
      end;
end;
//******************************************************************************
procedure TLogiciel.SetProperty(_ID : String; _Value : Variant);
Var
  cpt : Integer;
begin
  for cpt := 0 to XML.Proprietes_Logiciel.Count - 1 do
    if UpperCase(XML.Proprietes_Logiciel[cpt].ID) = UpperCase(_ID) then
      begin
      XML.Proprietes_Logiciel[cpt].Valeur := _Value;
      Exit;
      end;
end;
//******************************************************************************
procedure TLogiciel.SetOption(_ID : String; _Value : Boolean);
Var
  cpt : Integer;
begin
  for cpt := 0 to XML.Options_Logiciel.Count - 1 do
    if UpperCase(XML.Options_Logiciel[cpt].ID) = UpperCase(_ID) then
      begin
      XML.Options_Logiciel[cpt].Valeur := BoolToStr(_Value);
      Exit;
      end;
end;
//******************************************************************************
procedure TLogiciel.GenereNewLogiciel(_XMLSoftware : IXMLSoftware; _Options : TListeOptions; _Proprietes : TListeProprietes; _PackagesLogiciels : TListePackagesLogiciels; _PackagesCommuns : TListePackagesCommuns; _Serveurs : TServeurs; _Mode : TModeCreateLogiciel);
Var
  cpt : Integer;
  GUID: TGUID;
  Retour : String;
begin
  XML := _XMLSoftware;
  Options := _Options;
  Proprietes := _Proprietes;
  Serveurs := _Serveurs;
  PackagesCommuns := _PackagesCommuns;
    for cpt := 0 to _Proprietes.Count - 1 do
      if Not(ExistProperty(_Proprietes[cpt].XML.ID)) then
        XML.Proprietes_Logiciel.Add.ID := _Proprietes[cpt].XML.ID;

    if _Mode = mclNew then
      begin
      XML.ModeRelease := BoolToStr(True);
      SetNomCourtLogiciel('NouveauLogiciel');
      SetNomLongLogiciel('Nouveau logiciel');
      SetNomIndustriel('Industriel');
      end
    else
    if _Mode = mclClone then
      begin
      SetNomCourtLogiciel(GetNomCourtLogiciel + ' (copie)');
      SetNomLongLogiciel(GetNomLongLogiciel + ' (copie)');
      SetNomIndustriel(GetNomIndustriel + ' (copie)');
      end;

    if _Mode in [mclNew, mclClone] then
      begin
      CreateGuid(GUID);
      Retour := GUIDToString(GUID);
      Retour := StrReplace(Retour, '{', '');
      Retour := StrReplace(Retour, '}', '');
      SetIDSetup(Retour);
      end;

    for cpt := 0 to _Options.Count - 1 do
      if Not(ExistOption(_Options[cpt].XML.ID)) then
        XML.Options_Logiciel.Add.ID := _Options[cpt].XML.ID;

    for cpt := 0 to _PackagesCommuns.Count - 1 do
      if Not(ExistPackageCommun(_PackagesCommuns[cpt].XML.ID)) then
        XML.Packages_Communs_Logiciel.Add.ID := _PackagesCommuns[cpt].XML.ID;

    for cpt := 0 to _PackagesLogiciels.Count - 1 do
      PackagesLogiciels.AddItem.XML := _PackagesLogiciels[cpt].XML;

  PackagesLogiciels.AjoutePackagesXMLSpecifiques(XML.Packages_Generaux);
  GetDatesPackagesSurServeur;
end;
//******************************************************************************
Function TLogiciel.GetPathLogicielLocal : String;
begin
  Result := GetRepertoireLocalLogiciel + '\' + GetNomCourtLogiciel + '.exe';
end;
//******************************************************************************
Function TLogiciel.GetPathDownloadClient(_SlashFin : Boolean) : String;
begin
  Result := Serveurs.XML.Serveur_Download_Client + GetNomIndustriel + '/' + GetNomCourtLogiciel;
    if _SlashFin then
      Result := Result + '/';
end;
//******************************************************************************
Function TLogiciel.GetPathDownloadDev(_BackSlashFin : Boolean) : String;
begin
  Result := Serveurs.XML.Serveur_Download_Dev + GetNomIndustriel + '\' + GetNomCourtLogiciel;
   Util_ForceCreateDirectories(Result);
    if _BackSlashFin then
      Result := Result + '\';
end;
//******************************************************************************
Function TLogiciel.GetPathFolderUpdate(_BackSlashFin : Boolean) : String;
begin
  Result := Serveurs.XML.Serveur_Update + GetNomCourtLogiciel;
    if _BackSlashFin then
      Result := Result + '\';
end;
//******************************************************************************
Function TLogiciel.GetPathFolderFabricantSetup(_BackSlashFin : Boolean) : String;
begin
  Result := Serveurs.XML.Serveur_Setup + 'Fabricants\' + GetNomIndustriel;
    if _BackSlashFin then
      Result := Result + '\';
end;
//******************************************************************************
Function TLogiciel.GetPathFolderSetup(_BackSlashFin : Boolean) : String;
begin
  Result := GetPathFolderFabricantSetup(True) + GetNomCourtLogiciel;
    if _BackSlashFin then
      Result := Result + '\';
end;
//******************************************************************************
Function TLogiciel.GetPathFolderBinariesSetup(_BackSlashFin : Boolean) : String;
begin
  Result := GetPathFolderSetup(True) + 'Fichiers';
    if _BackSlashFin then
      Result := Result + '\';
end;
//******************************************************************************
Function TLogiciel.GetPathFolderInstallateurSetup(_BackSlashFin : Boolean) : String;
begin
  Result := GetPathFolderSetup(True) + 'Fichiers Installateur';
    if _BackSlashFin then
      Result := Result + '\';
end;
//******************************************************************************
Function TLogiciel.GetPathSetup : String;
begin
  Result := GetPathFolderInstallateurSetup(True) + 'Setup' + GetNomCourtLogiciel + '.exe';
end;
//******************************************************************************
Function TLogiciel.GetPathFolderBackUp(_BackSlashFin : Boolean) : String;
begin
  Result := Serveurs.XML.Serveur_BackUp + 'Fabricants\' + GetNomIndustriel + '\' + GetNomCourtLogiciel;
  Util_ForceCreateDirectories(Result);
    if _BackSlashFin then
      Result := Result + '\';
end;
//******************************************************************************
Function TLogiciel.GetPathBackUpSetup(_Version : String; _Date : String) : String;
begin
  Result := GetPathFolderBackUp(true) + 'Setup' + GetNomCourtLogiciel + '-' + _Version +  '-' + _Date + '.exe';
end;
//******************************************************************************
Function TLogiciel.GetPathBackUpPackage(_Version : String; _Date : String) : String;
begin
  Result := GetPathFolderBackUp(true) + GetNomCourtLogiciel + '-' + _Version +  '-' + _Date + '.zip';
end;
//******************************************************************************
function TLogiciel.GetPathFichierDateBackUpSetup : String;
begin
  Result := GetPathFolderBackUp(True) + 'DatesSetup.txt';
end;
//******************************************************************************
function TLogiciel.GetPathFichierDateBackUpPackage : String;
begin
  Result := GetPathFolderBackUp(True) + 'DatesPackage.txt';
end;
//******************************************************************************
Function TLogiciel.GetPathFolderBackUpExtractPath(_BackSlashFin : Boolean) : String;
begin
  Result := GetPathFolderBackUp(True) + '_Unpacked';
    if _BackSlashFin then
      Result := Result + '\';
end;
//******************************************************************************
Function TLogiciel.GetPathISS : String;
begin
  Result := GetPathFolderInstallateurSetup(True) + GetNomCourtLogiciel + '.iss';
end;
//******************************************************************************
Procedure TLogiciel.GenereScriptISS;
Var
  cpt : Integer;
  STL : TStringList;
  VersionLogiciel : String;
Const
  cst_Define = '#define ';
  cst_ScriptPrincipal = '#include ''..\..\..\Commun\Aeraulique\Scripts\Principal.iss''';
begin

  FBeginProgress(100, 'G�n�ration du script ISS', True, '', True);
  STL := TStringList.Create;

  STL.Add('; Script g�n�r� par l''assistant de d�ploiement');
  STL.Add('');

    for cpt := 0 to XML.Proprietes_Logiciel.Count - 1 do
      if ExistPropertyChaineISS(XML.Proprietes_Logiciel[cpt].ID) then
        STL.Add(cst_Define + GetPropertyChaineISS(XML.Proprietes_Logiciel[cpt].ID) + ' ''' + XML.Proprietes_Logiciel[cpt].Valeur + '''');

  VersionLogiciel := Util_ApplicationVersion(GetPathLogicielLocal);
  STL.Add(cst_Define + 'VersionLogiciel ''' + VersionLogiciel + '''');

  STL.Add('');

    for cpt := 0 to XML.Options_Logiciel.Count - 1 do
      if (Trim(XML.Options_Logiciel[cpt].Valeur) <> '') and StrToBool(XML.Options_Logiciel[cpt].Valeur) then
        if ExistOptionChaineISS(XML.Options_Logiciel[cpt].ID) then
          STL.Add(cst_Define + GetOptionChaineISS(XML.Options_Logiciel[cpt].ID));

  STL.Add('');

  STL.Add(cst_ScriptPrincipal);
  STL.WriteBOM := False;
  STL.SaveToFile(GetPathISS, TEncoding.UTF8);

  STL.Destroy;
  FEndProgress;

end;
//******************************************************************************
Procedure TLogiciel.GenerationSetup;
var
  Service: ICallBat;
  IdProcessus : Cardinal;
  test : String;
  RetourStartISS : Integer;
begin
  Service := GetICallBat();
  try
//pour tests
//    RetourStartISS := Service.StartISS('\\Multi\Installations\Fabricants\TestRegisFabricant\TestRegisLogiciel\Fichiers Installateur\TestRegisLogiciel.iss', IdProcessus);
    RetourStartISS := Service.StartISS(GetPathISS, IdProcessus);
      case RetourStartISS of
        0  :
            begin
            FBeginProgress(100, 'G�n�ration du Setup', True, '', True);

            //au bout d'une seconde, on v�rifie que le process est en cours.
            //Si ce n'est pas le cas, il est tr�s probable que le Setup a �chou�
            Sleep(1000);
              if Service.EnCours(IdProcessus) <> 0 then
                begin
                FEndProgress;
                BBS_ShowMessage('Il semble que le Setup ne soit pas valide (probl�me de script / fichiers).');
                end
              else
                begin
                  //on v�rifie toutes les 1 secondes si le Setup est bien g�n�r�
                  while Service.EnCours(IdProcessus) = 0 do
                    Sleep(1000);
                FEndProgress;
                BBS_ShowMessage('G�n�ration du Setup termin�e.');
                end;
            end;
        -2 : BBS_ShowMessage('Le fichier "' + GetPathISS + '" est introuvable sur le serveur.');
        -1 : BBS_ShowMessage('La g�n�ration du Setup a �chou� (Probl�me dans la cr�ation du processus du service web).');
      end;

  finally
  Service := Nil;
  end;
end;
//******************************************************************************
procedure TLogiciel.SaveSetup;
Var
  VersionLogiciel : String;
  DateLogiciel : String;
  tempSaveName : String;
begin
  VersionLogiciel := Util_ApplicationVersion(GetPathSetup);
  DateLogiciel := Util_FileDate(GetPathSetup);
  tempSaveName := GetPathBackUpSetup(VersionLogiciel, DateLogiciel);
  Util_CopyFile(GetPathSetup, tempSaveName, true);
  AddVersionFichierDate(GetPathFichierDateBackUpSetup, tempSaveName, DateLogiciel, VersionLogiciel);
end;
//******************************************************************************
procedure TLogiciel.RecupPackagesCommuns;
Var
  cpt : Integer;
begin
FBeginProgress(100, 'R�cup�ration des packages communs', True, '', True);
  for cpt := 0 to PackagesCommuns.Count - 1 do
    if GetPackageCommun(PackagesCommuns[cpt].XML.ID) then
      PackagesCommuns[cpt].RecupereFichiers(Serveurs.XML.Serveur_Setup, GetRepertoireLocalLogiciel);
FEndProgress;
end;
//******************************************************************************
procedure TLogiciel.AddVersionFichierDate(_DateFile : String; _FullPathElt : String; _DateElt : String; _VersionElt : String);
var
  stl : TStringList;
  tempChaine : String;
Const
  Separateur = ';';
begin
  tempChaine := ExtractFileName(_FullPathElt) + Separateur + _DateElt + Separateur + _VersionElt;
  stl := TStringList.Create;
    if Util_FichierSurDisque(_DateFile) then
      begin
      stl.LoadFromFile(_DateFile);
        //Si la dernier update correspond � celui qu'on veut ajouter, on annule tout
        if (stl.Count > 0) and (Trim(UpperCase(stl[0])) = Trim(UpperCase(tempChaine))) then
          begin
          stl.Destroy;
          Exit;
          end;
      end;
  stl.Insert(0, ExtractFileName(_FullPathElt) + Separateur + _DateElt + Separateur + _VersionElt);
  stl.SaveToFile(_DateFile);
  stl.Destroy;
end;
//******************************************************************************
procedure TLogiciel.ExtractAndZipSetup(_ProcessVisible : Boolean);
Var
  ExtractPath : String;
//  tempDate : String;
  FilesDirectory : TStringList;
  DirectoryDirectory : TStringList;
  cptFilesDirectory : integer;
  Zip : TZipFile;
  VersionLogiciel : String;
  DateLogiciel : String;
  tempSaveName : String;
  tempString : String;
Const
  //-x -> Extraire
  //-y -> R�pondre automatiquement oui � tout (ex : overwrite de fichier)
  //-m -> Extraire les fichiers embarqu�s (fichier de licence, bandeau, etc)
  //-d -> Chemin d'extraction
  ParametresExtraction = '-x -y -m -d';
begin

  //on initialise les chemins
  ExtractPath := GetPathFolderBackUpExtractPath(False);
  VersionLogiciel := Util_ApplicationVersion(GetPathSetup);
  DateLogiciel := Util_FileDate(GetPathSetup);
  tempSaveName := GetPathBackUpPackage(VersionLogiciel, DateLogiciel);

  //on supprime le r�pertoire de d�compression si il existe
    if Util_RepertoireSurDisque(ExtractPath) then
      Util_DeleteFullDirectory(ExtractPath);

  //on lance l'extraction du setup
  FBeginProgress(100, 'Extraction du Setup', false, '', true);
  Util_ExecuteProcess(GetPathInnoUnp, ParametresExtraction + '"' + ExtractPath + '" "' + GetPathSetup + '"', Not(_ProcessVisible), True);
  FEndProgress;
  BBS_ShowMessage('Extraction termin�e');

  try
  //on zip le contenu du r�pertoire {app} situ� dans le r�pertoire d'extraction
  Zip := TZipFile.Create;
  Zip.Open(tempSaveName, zmWrite);
  FilesDirectory := TStringList.Create;
  DirectoryDirectory := TStringList.Create;
  Util_ScruterRepertoire(ExtractPath + '\{app}', FilesDirectory, DirectoryDirectory);
    for cptFilesDirectory := 0 to FilesDirectory.Count - 1 do
      begin
      FilesDirectory[cptFilesDirectory] := StrReplace(FilesDirectory[cptFilesDirectory], ExtractPath + '\{app}\', '', False);
      tempString := FilesDirectory[cptFilesDirectory];
      tempString := StrReplace(tempString , '\', '/', False, True);
      Zip.Add(ExtractPath + '\{app}\' + FilesDirectory[cptFilesDirectory], tempString);
      end;
  FilesDirectory.Destroy;
  DirectoryDirectory.Destroy;
  Zip.Close;

  finally
  Zip.Free;
  end;

 //on supprime le r�pertoire de d�compression
 Util_DeleteFullDirectory(ExtractPath);

  AddVersionFichierDate(GetPathFichierDateBackUpPackage, tempSaveName, DateLogiciel, VersionLogiciel);
end;
//******************************************************************************
function TLogiciel.GetPathFichierDateUpdater : String;
begin
  Result := GetPathFolderUpdate(True) + 'Dates.txt';
  if Not(Util_FichierSurDisque(Result)) then
    Result := '';
end;
//******************************************************************************
function TLogiciel.GetPathInnoUnp : String;
begin
  Result := Serveurs.XML.Serveur_Setup + 'Z-Automation\innounp\innounp.exe';
end;
//******************************************************************************
procedure TLogiciel.GetDatesPackagesSurServeur;
Var
  stl : TStringList;
  FichierDate : String;
begin
  FichierDate := GetPathFichierDateUpdater;

    if FichierDate = '' then
      Exit;

  stl := TStringList.Create;
  stl.LoadFromFile(FichierDate);
  PackagesLogiciels.UpdateSaveDatePackages(stl);
  stl.Destroy;
end;
//******************************************************************************
procedure TLogiciel.UpdateServeurSetupPackages;
Var
  tempPackageApplication : TPackageLogiciel;
  FichiersOptionApplication : TStringList;
  tempForceDate : Boolean;
begin
  FBeginProgress(100, 'Upload des binaries du Setup', True, '', True);
  FichiersOptionApplication := GetFichiersApplication(GetRepertoireLocalLogiciel + '\');
  tempPackageApplication := PackagesLogiciels.GetPackageApplication;
  tempPackageApplication.FichiersTraitementSpecial.Clear;
  tempPackageApplication.FichiersTraitementSpecial.Add(GetNomCourtLogiciel + '.exe');
//  tempPackageApplication.FichiersTraitementSpecial.AddStrings(FichiersOptionApplication);
  tempForceDate := Trim(GetPathFichierDateUpdater) <> '';
  PackagesLogiciels.UpdateServeurSetupPackages(GetPathFolderBinariesSetup(True), GetRepertoireLocalLogiciel, tempForceDate);
  FichiersOptionApplication.Destroy;
  FEndProgress;
end;
//******************************************************************************
procedure TLogiciel.UpdateServeurUpdaterPackages;
Var
  tempPackageApplication : TPackageLogiciel;
  FichiersOptionApplication : TStringList;
begin
  FBeginProgress(100, 'Mise � jour des packages d''update', True, '', True);
  FichiersOptionApplication := GetFichiersApplication(GetRepertoireLocalLogiciel + '\');
  tempPackageApplication := PackagesLogiciels.GetPackageApplication;
  tempPackageApplication.FichiersTraitementSpecial.Clear;
  tempPackageApplication.FichiersTraitementSpecial.Add(GetNomCourtLogiciel + '.exe');
  tempPackageApplication.FichiersTraitementSpecial.AddStrings(FichiersOptionApplication);
  PackagesLogiciels.UpdateServeurUpdaterPackages(GetPathFolderUpdate(True), GetRepertoireLocalLogiciel);
  FichiersOptionApplication.Destroy;
  FEndProgress;
end;
//******************************************************************************
procedure TLogiciel.UpdateServeurUpdaterDates;
Var
  stl : TStringList;
  FichierDate : String;
begin

  FichierDate := GetPathFichierDateUpdater;

    if FichierDate = '' then
      begin
      Util_CreateFile(GetPathFolderUpdate(True) + 'Dates.txt');
      FichierDate := GetPathFichierDateUpdater;
      end;

  stl := PackagesLogiciels.GetContenuFichierDate;
  stl.SaveToFile(FichierDate);
  stl.Destroy;

end;
//******************************************************************************

//*****************************************************************************
//##############################################################################
//# TListeLogiciels                                                            #
//##############################################################################
//******************************************************************************
constructor TListeLogiciels.Create;
begin
  Inherited;
  IndexLogicielCourant := 0;
end;
//******************************************************************************
Procedure TListeLogiciels.Set_IndexLogicielCourant(_Value : Integer);
begin
  if Count = 0 then
    FIndexLogicielCourant := -1
  else
  if (_Value < 0) or (_Value >= Count) then
    FIndexLogicielCourant := 0
  else
    FIndexLogicielCourant := _Value;
end;
//******************************************************************************
function TListeLogiciels.LogicielCourant : TLogiciel;
begin
  if (IndexLogicielCourant < 0) or (IndexLogicielCourant > Count - 1) then
    Result := Nil
  else
    Result := Items[IndexLogicielCourant];
end;
//******************************************************************************
function TListeLogiciels.GenereNewLogiciel(_XMLSoftware : IXMLSoftware; _Options : TListeOptions; _Proprietes : TListeProprietes; _PackagesLogiciels : TListePackagesLogiciels; _PackagesCommuns : TListePackagesCommuns; _Serveurs : TServeurs; _Mode : TModeCreateLogiciel) : TLogiciel;
begin
  Result := AddItem;
  Result.GenereNewLogiciel(_XMLSoftware, _Options, _Proprietes, _PackagesLogiciels, _PackagesCommuns, _Serveurs, _Mode);
  IndexLogicielCourant := Count - 1;
end;
//******************************************************************************
procedure TListeLogiciels.SupprimeLogiciel(_XMLSoftwares : IXMLSoftwares; _Index : Integer);
begin
  if _Index = IndexLogicielCourant then
    IndexLogicielCourant := 0;
  _XMLSoftwares.Remove(Items[_Index].XML);
  Delete(_Index);
  IndexLogicielCourant := IndexLogicielCourant;
end;
//******************************************************************************
end.
