program Ventil_AssistantDeploiement;

uses
  Vcl.Forms,

  CAD_Containers_Base                           in '..\..\CADv2\Commun\CAD_Containers_Base.pas',
  CAD_ViloUtils                                 in '..\..\CADv2\Commun\CAD_ViloUtils.pas',
  HelpersCommun                                 in '..\..\CADv2\Commun\HelpersCommun.pas',
  BBScad_Interface_Commun                       in '..\..\CADv2\Interfaces\BBScad_Interface_Commun.pas',

  BbsgGenericForm in '..\..\Bbscommun2005\Bbsg\BbsgGenericForm.pas',
  BBS_Progress in '..\..\Bbscommun2005\Utils\BBS_Progress.pas' {Fic_Progress},
  BBS_Message in '..\..\BbsCommun2005\Utils\BBS_Message.pas' {Fic_Message},
  Main in 'Main.pas' {FormAssistantDeploiement},
  config_XML in 'config_XML.pas',
  Logiciels in 'Logiciels.pas',
  PackagesCommuns in 'PackagesCommuns.pas',
  Utils in 'Utils.pas',
  PackagesLogiciels in 'PackagesLogiciels.pas',
  Serveurs in 'Serveurs.pas',
  ServiceWebSetup in 'ServiceWebSetup.pas';

{*.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
//  Application.ModalPopupMode := pmAuto;
  Application.CreateForm(TFormAssistantDeploiement, FormAssistantDeploiement);
  Application.Run;
end.
