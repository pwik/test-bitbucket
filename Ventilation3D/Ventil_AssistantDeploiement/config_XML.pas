
{********************************************************************************************************}
{                                                                                                        }
{                                         Liaison de donn�es XML                                         }
{                                                                                                        }
{         G�n�r� le : 16/03/2016 14:41:33                                                                }
{       G�n�r� depuis : D:\Developpement\DelphSou\Ventilation3D\Ventil_AssistantDeploiement\config.xsd   }
{                                                                                                        }
{********************************************************************************************************}

unit config_XML;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ D�cl. Forward }

  IXMLRoot = interface;
  IXMLConfiguration = interface;
  IXMLServeurs = interface;
  IXMLPackages_Communs = interface;
  IXMLPackage = interface;
  IXMLFichiers = interface;
  IXMLFichier = interface;
  IXMLRepertoires = interface;
  IXMLRepertoire = interface;
  IXMLPackages_Generaux = interface;
  IXMLPackage_General = interface;
  IXMLOptions = interface;
  IXMLOption = interface;
  IXMLFichiersPackageApplication = interface;
  IXMLProprietes = interface;
  IXMLPropriete = interface;
  IXMLPreferencesUtilitaire = interface;
  IXMLSoftwares = interface;
  IXMLSoftware = interface;
  IXMLProprietes_Logiciel = interface;
  IXMLPropriete_Logiciel = interface;
  IXMLOptions_Logiciel = interface;
  IXMLOption_Logiciel = interface;
  IXMLPackages_Communs_Logiciel = interface;
  IXMLPackage_Commun_Logiciel = interface;

{ IXMLRoot }

  IXMLRoot = interface(IXMLNode)
    ['{4C5D05EB-B0BA-4BDB-9E25-6772585EB12E}']
    { Accesseurs de propri�t�s }
    function Get_Configuration: IXMLConfiguration;
    function Get_Softwares: IXMLSoftwares;
    { M�thodes & propri�t�s }
    property Configuration: IXMLConfiguration read Get_Configuration;
    property Softwares: IXMLSoftwares read Get_Softwares;
  end;

{ IXMLConfiguration }

  IXMLConfiguration = interface(IXMLNode)
    ['{BAB3AFE5-914F-4705-B70E-8B457930D4FC}']
    { Accesseurs de propri�t�s }
    function Get_Serveurs: IXMLServeurs;
    function Get_Packages_Communs: IXMLPackages_Communs;
    function Get_Packages_Generaux: IXMLPackages_Generaux;
    function Get_Options: IXMLOptions;
    function Get_Proprietes: IXMLProprietes;
    function Get_PreferencesUtilitaire: IXMLPreferencesUtilitaire;
    { M�thodes & propri�t�s }
    property Serveurs: IXMLServeurs read Get_Serveurs;
    property Packages_Communs: IXMLPackages_Communs read Get_Packages_Communs;
    property Packages_Generaux: IXMLPackages_Generaux read Get_Packages_Generaux;
    property Options: IXMLOptions read Get_Options;
    property Proprietes: IXMLProprietes read Get_Proprietes;
    property PreferencesUtilitaire: IXMLPreferencesUtilitaire read Get_PreferencesUtilitaire;
  end;

{ IXMLServeurs }

  IXMLServeurs = interface(IXMLNode)
    ['{5459D4CA-EA54-4E01-A2E6-B7E70E6A03F1}']
    { Accesseurs de propri�t�s }
    function Get_Serveur_Setup: UnicodeString;
    function Get_Serveur_Update: UnicodeString;
    function Get_Serveur_PatchNote_Dev: UnicodeString;
    function Get_Serveur_PatchNote_Client: UnicodeString;
    function Get_Serveur_BackUp: UnicodeString;
    function Get_Serveur_Download_Dev: UnicodeString;
    function Get_Serveur_Download_Client: UnicodeString;
    procedure Set_Serveur_Setup(Value: UnicodeString);
    procedure Set_Serveur_Update(Value: UnicodeString);
    procedure Set_Serveur_PatchNote_Dev(Value: UnicodeString);
    procedure Set_Serveur_PatchNote_Client(Value: UnicodeString);
    procedure Set_Serveur_BackUp(Value: UnicodeString);
    procedure Set_Serveur_Download_Dev(Value: UnicodeString);
    procedure Set_Serveur_Download_Client(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property Serveur_Setup: UnicodeString read Get_Serveur_Setup write Set_Serveur_Setup;
    property Serveur_Update: UnicodeString read Get_Serveur_Update write Set_Serveur_Update;
    property Serveur_PatchNote_Dev: UnicodeString read Get_Serveur_PatchNote_Dev write Set_Serveur_PatchNote_Dev;
    property Serveur_PatchNote_Client: UnicodeString read Get_Serveur_PatchNote_Client write Set_Serveur_PatchNote_Client;
    property Serveur_BackUp: UnicodeString read Get_Serveur_BackUp write Set_Serveur_BackUp;
    property Serveur_Download_Dev: UnicodeString read Get_Serveur_Download_Dev write Set_Serveur_Download_Dev;
    property Serveur_Download_Client: UnicodeString read Get_Serveur_Download_Client write Set_Serveur_Download_Client;
  end;

{ IXMLPackages_Communs }

  IXMLPackages_Communs = interface(IXMLNodeCollection)
    ['{13440DF2-12FB-4D99-AB19-D4FCF94DAE53}']
    { Accesseurs de propri�t�s }
    function Get_Package(Index: Integer): IXMLPackage;
    { M�thodes & propri�t�s }
    function Add: IXMLPackage;
    function Insert(const Index: Integer): IXMLPackage;
    property Package[Index: Integer]: IXMLPackage read Get_Package; default;
  end;

{ IXMLPackage }

  IXMLPackage = interface(IXMLNode)
    ['{4738B919-30A9-4E60-8540-FBA8228CDA30}']
    { Accesseurs de propri�t�s }
    function Get_ID: UnicodeString;
    function Get_Nom: UnicodeString;
    function Get_Cible: UnicodeString;
    function Get_Fichiers: IXMLFichiers;
    function Get_Repertoires: IXMLRepertoires;
    procedure Set_ID(Value: UnicodeString);
    procedure Set_Nom(Value: UnicodeString);
    procedure Set_Cible(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property ID: UnicodeString read Get_ID write Set_ID;
    property Nom: UnicodeString read Get_Nom write Set_Nom;
    property Cible: UnicodeString read Get_Cible write Set_Cible;
    property Fichiers: IXMLFichiers read Get_Fichiers;
    property Repertoires: IXMLRepertoires read Get_Repertoires;
  end;

{ IXMLFichiers }

  IXMLFichiers = interface(IXMLNodeCollection)
    ['{489AE27C-4408-42E9-B46B-EF8525CC99F8}']
    { Accesseurs de propri�t�s }
    function Get_Fichier(Index: Integer): IXMLFichier;
    { M�thodes & propri�t�s }
    function Add: IXMLFichier;
    function Insert(const Index: Integer): IXMLFichier;
    property Fichier[Index: Integer]: IXMLFichier read Get_Fichier; default;
  end;

{ IXMLFichier }

  IXMLFichier = interface(IXMLNode)
    ['{E4790C44-2782-4CDA-B34D-D24B42458D0D}']
    { Accesseurs de propri�t�s }
    function Get_Cible: UnicodeString;
    function Get_Valeur: UnicodeString;
    procedure Set_Cible(Value: UnicodeString);
    procedure Set_Valeur(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property Cible: UnicodeString read Get_Cible write Set_Cible;
    property Valeur: UnicodeString read Get_Valeur write Set_Valeur;
  end;

{ IXMLRepertoires }

  IXMLRepertoires = interface(IXMLNodeCollection)
    ['{62B793A9-8C67-4EB3-8F90-520782E93832}']
    { Accesseurs de propri�t�s }
    function Get_Repertoire(Index: Integer): IXMLRepertoire;
    { M�thodes & propri�t�s }
    function Add: IXMLRepertoire;
    function Insert(const Index: Integer): IXMLRepertoire;
    property Repertoire[Index: Integer]: IXMLRepertoire read Get_Repertoire; default;
  end;

{ IXMLRepertoire }

  IXMLRepertoire = interface(IXMLNode)
    ['{775690DA-A1B3-4D5C-8587-22D3CEC021B7}']
    { Accesseurs de propri�t�s }
    function Get_Cible: UnicodeString;
    function Get_Valeur: UnicodeString;
    procedure Set_Cible(Value: UnicodeString);
    procedure Set_Valeur(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property Cible: UnicodeString read Get_Cible write Set_Cible;
    property Valeur: UnicodeString read Get_Valeur write Set_Valeur;
  end;

{ IXMLPackages_Generaux }

  IXMLPackages_Generaux = interface(IXMLNodeCollection)
    ['{67679603-4777-451E-AE66-F74A3C22E1EA}']
    { Accesseurs de propri�t�s }
    function Get_Package_General(Index: Integer): IXMLPackage_General;
    { M�thodes & propri�t�s }
    function Add: IXMLPackage_General;
    function Insert(const Index: Integer): IXMLPackage_General;
    property Package_General[Index: Integer]: IXMLPackage_General read Get_Package_General; default;
  end;

{ IXMLPackage_General }

  IXMLPackage_General = interface(IXMLNode)
    ['{0BB6386B-4B8B-4E94-8557-18ECB8BD9CB3}']
    { Accesseurs de propri�t�s }
    function Get_Nom: UnicodeString;
    function Get_Cible: UnicodeString;
    function Get_IsPackageApplication: UnicodeString;
    function Get_Fichiers: IXMLFichiers;
    function Get_Repertoires: IXMLRepertoires;
    procedure Set_Nom(Value: UnicodeString);
    procedure Set_Cible(Value: UnicodeString);
    procedure Set_IsPackageApplication(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property Nom: UnicodeString read Get_Nom write Set_Nom;
    property Cible: UnicodeString read Get_Cible write Set_Cible;
    property IsPackageApplication: UnicodeString read Get_IsPackageApplication write Set_IsPackageApplication;
    property Fichiers: IXMLFichiers read Get_Fichiers;
    property Repertoires: IXMLRepertoires read Get_Repertoires;
  end;

{ IXMLOptions }

  IXMLOptions = interface(IXMLNodeCollection)
    ['{A4E0FEB6-C24E-4FB1-86A6-AD14C8AFA71A}']
    { Accesseurs de propri�t�s }
    function Get_Option(Index: Integer): IXMLOption;
    { M�thodes & propri�t�s }
    function Add: IXMLOption;
    function Insert(const Index: Integer): IXMLOption;
    property Option[Index: Integer]: IXMLOption read Get_Option; default;
  end;

{ IXMLOption }

  IXMLOption = interface(IXMLNode)
    ['{0D3A6A72-4A5A-4932-8592-947F750DD413}']
    { Accesseurs de propri�t�s }
    function Get_ID: UnicodeString;
    function Get_Nom: UnicodeString;
    function Get_ChaineISS: UnicodeString;
    function Get_FichiersPackageApplication: IXMLFichiersPackageApplication;
    procedure Set_ID(Value: UnicodeString);
    procedure Set_Nom(Value: UnicodeString);
    procedure Set_ChaineISS(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property ID: UnicodeString read Get_ID write Set_ID;
    property Nom: UnicodeString read Get_Nom write Set_Nom;
    property ChaineISS: UnicodeString read Get_ChaineISS write Set_ChaineISS;
    property FichiersPackageApplication: IXMLFichiersPackageApplication read Get_FichiersPackageApplication;
  end;

{ IXMLFichiersPackageApplication }

  IXMLFichiersPackageApplication = interface(IXMLNode)
    ['{081B9DA6-5966-4A54-AC35-95D79D3F84E0}']
    { Accesseurs de propri�t�s }
    function Get_Fichiers: IXMLFichiers;
    function Get_Repertoires: IXMLRepertoires;
    { M�thodes & propri�t�s }
    property Fichiers: IXMLFichiers read Get_Fichiers;
    property Repertoires: IXMLRepertoires read Get_Repertoires;
  end;

{ IXMLProprietes }

  IXMLProprietes = interface(IXMLNodeCollection)
    ['{C18FEC41-9036-4932-8E87-63711FBDFB4F}']
    { Accesseurs de propri�t�s }
    function Get_Propriete(Index: Integer): IXMLPropriete;
    { M�thodes & propri�t�s }
    function Add: IXMLPropriete;
    function Insert(const Index: Integer): IXMLPropriete;
    property Propriete[Index: Integer]: IXMLPropriete read Get_Propriete; default;
  end;

{ IXMLPropriete }

  IXMLPropriete = interface(IXMLNode)
    ['{0DA1B71F-70C5-40B3-99A3-5989E952A73B}']
    { Accesseurs de propri�t�s }
    function Get_ID: UnicodeString;
    function Get_Nom: UnicodeString;
    function Get_ChaineISS: UnicodeString;
    function Get_IsExeName: UnicodeString;
    function Get_IsNomCompletLogiciel: UnicodeString;
    function Get_IsNomIndustriel: UnicodeString;
    function Get_IsIDPatchNote: UnicodeString;
    function Get_IsLocalPath: UnicodeString;
    function Get_IsIDSetup: UnicodeString;
    function Get_Edit_Type: UnicodeString;
    procedure Set_ID(Value: UnicodeString);
    procedure Set_Nom(Value: UnicodeString);
    procedure Set_ChaineISS(Value: UnicodeString);
    procedure Set_IsExeName(Value: UnicodeString);
    procedure Set_IsNomCompletLogiciel(Value: UnicodeString);
    procedure Set_IsNomIndustriel(Value: UnicodeString);
    procedure Set_IsIDPatchNote(Value: UnicodeString);
    procedure Set_IsLocalPath(Value: UnicodeString);
    procedure Set_IsIDSetup(Value: UnicodeString);
    procedure Set_Edit_Type(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property ID: UnicodeString read Get_ID write Set_ID;
    property Nom: UnicodeString read Get_Nom write Set_Nom;
    property ChaineISS: UnicodeString read Get_ChaineISS write Set_ChaineISS;
    property IsExeName: UnicodeString read Get_IsExeName write Set_IsExeName;
    property IsNomCompletLogiciel: UnicodeString read Get_IsNomCompletLogiciel write Set_IsNomCompletLogiciel;
    property IsNomIndustriel: UnicodeString read Get_IsNomIndustriel write Set_IsNomIndustriel;
    property IsIDPatchNote: UnicodeString read Get_IsIDPatchNote write Set_IsIDPatchNote;
    property IsLocalPath: UnicodeString read Get_IsLocalPath write Set_IsLocalPath;
    property IsIDSetup: UnicodeString read Get_IsIDSetup write Set_IsIDSetup;
    property Edit_Type: UnicodeString read Get_Edit_Type write Set_Edit_Type;
  end;

{ IXMLPreferencesUtilitaire }

  IXMLPreferencesUtilitaire = interface(IXMLNode)
    ['{FE4A4219-24F4-4959-8B8D-E8BE9CF2B0A0}']
    { Accesseurs de propri�t�s }
    function Get_AskSaveConfigFileOnExit: UnicodeString;
    function Get_AutoSaveConfigFileOnExit: UnicodeString;
    function Get_BackUpConfigFile: UnicodeString;
    function Get_BackUpSetup: UnicodeString;
    function Get_ExtractSetup: UnicodeString;
    function Get_ExtractSilencieux: UnicodeString;
    function Get_GenerateISSForSetupExecution: UnicodeString;
    function Get_SaveConfigLogicielBeforeISSForSetup: UnicodeString;
    function Get_SaveFileConfigFileOnSetupExecution: UnicodeString;
    procedure Set_AskSaveConfigFileOnExit(Value: UnicodeString);
    procedure Set_AutoSaveConfigFileOnExit(Value: UnicodeString);
    procedure Set_BackUpConfigFile(Value: UnicodeString);
    procedure Set_BackUpSetup(Value: UnicodeString);
    procedure Set_ExtractSetup(Value: UnicodeString);
    procedure Set_ExtractSilencieux(Value: UnicodeString);
    procedure Set_GenerateISSForSetupExecution(Value: UnicodeString);
    procedure Set_SaveConfigLogicielBeforeISSForSetup(Value: UnicodeString);
    procedure Set_SaveFileConfigFileOnSetupExecution(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property AskSaveConfigFileOnExit: UnicodeString read Get_AskSaveConfigFileOnExit write Set_AskSaveConfigFileOnExit;
    property AutoSaveConfigFileOnExit: UnicodeString read Get_AutoSaveConfigFileOnExit write Set_AutoSaveConfigFileOnExit;
    property BackUpConfigFile: UnicodeString read Get_BackUpConfigFile write Set_BackUpConfigFile;
    property BackUpSetup: UnicodeString read Get_BackUpSetup write Set_BackUpSetup;
    property ExtractSetup: UnicodeString read Get_ExtractSetup write Set_ExtractSetup;
    property ExtractSilencieux: UnicodeString read Get_ExtractSilencieux write Set_ExtractSilencieux;
    property GenerateISSForSetupExecution: UnicodeString read Get_GenerateISSForSetupExecution write Set_GenerateISSForSetupExecution;
    property SaveConfigLogicielBeforeISSForSetup: UnicodeString read Get_SaveConfigLogicielBeforeISSForSetup write Set_SaveConfigLogicielBeforeISSForSetup;
    property SaveFileConfigFileOnSetupExecution: UnicodeString read Get_SaveFileConfigFileOnSetupExecution write Set_SaveFileConfigFileOnSetupExecution;
  end;

{ IXMLSoftwares }

  IXMLSoftwares = interface(IXMLNodeCollection)
    ['{6362E3BE-774A-4A48-B77A-72BB86D11D37}']
    { Accesseurs de propri�t�s }
    function Get_Software(Index: Integer): IXMLSoftware;
    { M�thodes & propri�t�s }
    function Add: IXMLSoftware;
    function Insert(const Index: Integer): IXMLSoftware;
    property Software[Index: Integer]: IXMLSoftware read Get_Software; default;
  end;

{ IXMLSoftware }

  IXMLSoftware = interface(IXMLNode)
    ['{5B3E0577-7ADF-427C-8755-9097DDEBD61D}']
    { Accesseurs de propri�t�s }
    function Get_Proprietes_Logiciel: IXMLProprietes_Logiciel;
    function Get_Options_Logiciel: IXMLOptions_Logiciel;
    function Get_Packages_Generaux: IXMLPackages_Generaux;
    function Get_Packages_Communs_Logiciel: IXMLPackages_Communs_Logiciel;
    function Get_ModeRelease: UnicodeString;
    procedure Set_ModeRelease(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property Proprietes_Logiciel: IXMLProprietes_Logiciel read Get_Proprietes_Logiciel;
    property Options_Logiciel: IXMLOptions_Logiciel read Get_Options_Logiciel;
    property Packages_Generaux: IXMLPackages_Generaux read Get_Packages_Generaux;
    property Packages_Communs_Logiciel: IXMLPackages_Communs_Logiciel read Get_Packages_Communs_Logiciel;
    property ModeRelease: UnicodeString read Get_ModeRelease write Set_ModeRelease;
  end;

{ IXMLProprietes_Logiciel }

  IXMLProprietes_Logiciel = interface(IXMLNodeCollection)
    ['{5E6C5B1A-FAB8-415E-B2E8-BA4A5EB2E68A}']
    { Accesseurs de propri�t�s }
    function Get_Propriete_Logiciel(Index: Integer): IXMLPropriete_Logiciel;
    { M�thodes & propri�t�s }
    function Add: IXMLPropriete_Logiciel;
    function Insert(const Index: Integer): IXMLPropriete_Logiciel;
    property Propriete_Logiciel[Index: Integer]: IXMLPropriete_Logiciel read Get_Propriete_Logiciel; default;
  end;

{ IXMLPropriete_Logiciel }

  IXMLPropriete_Logiciel = interface(IXMLNode)
    ['{F596AF13-988D-41BF-8915-F8372286F9B8}']
    { Accesseurs de propri�t�s }
    function Get_ID: UnicodeString;
    function Get_Valeur: UnicodeString;
    procedure Set_ID(Value: UnicodeString);
    procedure Set_Valeur(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property ID: UnicodeString read Get_ID write Set_ID;
    property Valeur: UnicodeString read Get_Valeur write Set_Valeur;
  end;

{ IXMLOptions_Logiciel }

  IXMLOptions_Logiciel = interface(IXMLNodeCollection)
    ['{FE9074A9-B867-4548-A694-8B01F8BD3BC7}']
    { Accesseurs de propri�t�s }
    function Get_Option_Logiciel(Index: Integer): IXMLOption_Logiciel;
    { M�thodes & propri�t�s }
    function Add: IXMLOption_Logiciel;
    function Insert(const Index: Integer): IXMLOption_Logiciel;
    property Option_Logiciel[Index: Integer]: IXMLOption_Logiciel read Get_Option_Logiciel; default;
  end;

{ IXMLOption_Logiciel }

  IXMLOption_Logiciel = interface(IXMLNode)
    ['{32EB8ED3-3F00-416C-9880-2906675B8F20}']
    { Accesseurs de propri�t�s }
    function Get_ID: UnicodeString;
    function Get_Valeur: UnicodeString;
    procedure Set_ID(Value: UnicodeString);
    procedure Set_Valeur(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property ID: UnicodeString read Get_ID write Set_ID;
    property Valeur: UnicodeString read Get_Valeur write Set_Valeur;
  end;

{ IXMLPackages_Communs_Logiciel }

  IXMLPackages_Communs_Logiciel = interface(IXMLNodeCollection)
    ['{B419EFEC-867E-4CEE-88CC-1E203AE64BED}']
    { Accesseurs de propri�t�s }
    function Get_Package_Commun_Logiciel(Index: Integer): IXMLPackage_Commun_Logiciel;
    { M�thodes & propri�t�s }
    function Add: IXMLPackage_Commun_Logiciel;
    function Insert(const Index: Integer): IXMLPackage_Commun_Logiciel;
    property Package_Commun_Logiciel[Index: Integer]: IXMLPackage_Commun_Logiciel read Get_Package_Commun_Logiciel; default;
  end;

{ IXMLPackage_Commun_Logiciel }

  IXMLPackage_Commun_Logiciel = interface(IXMLNode)
    ['{0AF98A6B-92AB-401C-AE00-54C894A00439}']
    { Accesseurs de propri�t�s }
    function Get_ID: UnicodeString;
    function Get_Valeur: UnicodeString;
    procedure Set_ID(Value: UnicodeString);
    procedure Set_Valeur(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property ID: UnicodeString read Get_ID write Set_ID;
    property Valeur: UnicodeString read Get_Valeur write Set_Valeur;
  end;

{ D�cl. Forward }

  TXMLRoot = class;
  TXMLConfiguration = class;
  TXMLServeurs = class;
  TXMLPackages_Communs = class;
  TXMLPackage = class;
  TXMLFichiers = class;
  TXMLFichier = class;
  TXMLRepertoires = class;
  TXMLRepertoire = class;
  TXMLPackages_Generaux = class;
  TXMLPackage_General = class;
  TXMLOptions = class;
  TXMLOption = class;
  TXMLFichiersPackageApplication = class;
  TXMLProprietes = class;
  TXMLPropriete = class;
  TXMLPreferencesUtilitaire = class;
  TXMLSoftwares = class;
  TXMLSoftware = class;
  TXMLProprietes_Logiciel = class;
  TXMLPropriete_Logiciel = class;
  TXMLOptions_Logiciel = class;
  TXMLOption_Logiciel = class;
  TXMLPackages_Communs_Logiciel = class;
  TXMLPackage_Commun_Logiciel = class;

{ TXMLRoot }

  TXMLRoot = class(TXMLNode, IXMLRoot)
  protected
    { IXMLRoot }
    function Get_Configuration: IXMLConfiguration;
    function Get_Softwares: IXMLSoftwares;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLConfiguration }

  TXMLConfiguration = class(TXMLNode, IXMLConfiguration)
  protected
    { IXMLConfiguration }
    function Get_Serveurs: IXMLServeurs;
    function Get_Packages_Communs: IXMLPackages_Communs;
    function Get_Packages_Generaux: IXMLPackages_Generaux;
    function Get_Options: IXMLOptions;
    function Get_Proprietes: IXMLProprietes;
    function Get_PreferencesUtilitaire: IXMLPreferencesUtilitaire;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLServeurs }

  TXMLServeurs = class(TXMLNode, IXMLServeurs)
  protected
    { IXMLServeurs }
    function Get_Serveur_Setup: UnicodeString;
    function Get_Serveur_Update: UnicodeString;
    function Get_Serveur_PatchNote_Dev: UnicodeString;
    function Get_Serveur_PatchNote_Client: UnicodeString;
    function Get_Serveur_BackUp: UnicodeString;
    function Get_Serveur_Download_Dev: UnicodeString;
    function Get_Serveur_Download_Client: UnicodeString;
    procedure Set_Serveur_Setup(Value: UnicodeString);
    procedure Set_Serveur_Update(Value: UnicodeString);
    procedure Set_Serveur_PatchNote_Dev(Value: UnicodeString);
    procedure Set_Serveur_PatchNote_Client(Value: UnicodeString);
    procedure Set_Serveur_BackUp(Value: UnicodeString);
    procedure Set_Serveur_Download_Dev(Value: UnicodeString);
    procedure Set_Serveur_Download_Client(Value: UnicodeString);
  end;

{ TXMLPackages_Communs }

  TXMLPackages_Communs = class(TXMLNodeCollection, IXMLPackages_Communs)
  protected
    { IXMLPackages_Communs }
    function Get_Package(Index: Integer): IXMLPackage;
    function Add: IXMLPackage;
    function Insert(const Index: Integer): IXMLPackage;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPackage }

  TXMLPackage = class(TXMLNode, IXMLPackage)
  protected
    { IXMLPackage }
    function Get_ID: UnicodeString;
    function Get_Nom: UnicodeString;
    function Get_Cible: UnicodeString;
    function Get_Fichiers: IXMLFichiers;
    function Get_Repertoires: IXMLRepertoires;
    procedure Set_ID(Value: UnicodeString);
    procedure Set_Nom(Value: UnicodeString);
    procedure Set_Cible(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLFichiers }

  TXMLFichiers = class(TXMLNodeCollection, IXMLFichiers)
  protected
    { IXMLFichiers }
    function Get_Fichier(Index: Integer): IXMLFichier;
    function Add: IXMLFichier;
    function Insert(const Index: Integer): IXMLFichier;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLFichier }

  TXMLFichier = class(TXMLNode, IXMLFichier)
  protected
    { IXMLFichier }
    function Get_Cible: UnicodeString;
    function Get_Valeur: UnicodeString;
    procedure Set_Cible(Value: UnicodeString);
    procedure Set_Valeur(Value: UnicodeString);
  end;

{ TXMLRepertoires }

  TXMLRepertoires = class(TXMLNodeCollection, IXMLRepertoires)
  protected
    { IXMLRepertoires }
    function Get_Repertoire(Index: Integer): IXMLRepertoire;
    function Add: IXMLRepertoire;
    function Insert(const Index: Integer): IXMLRepertoire;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRepertoire }

  TXMLRepertoire = class(TXMLNode, IXMLRepertoire)
  protected
    { IXMLRepertoire }
    function Get_Cible: UnicodeString;
    function Get_Valeur: UnicodeString;
    procedure Set_Cible(Value: UnicodeString);
    procedure Set_Valeur(Value: UnicodeString);
  end;

{ TXMLPackages_Generaux }

  TXMLPackages_Generaux = class(TXMLNodeCollection, IXMLPackages_Generaux)
  protected
    { IXMLPackages_Generaux }
    function Get_Package_General(Index: Integer): IXMLPackage_General;
    function Add: IXMLPackage_General;
    function Insert(const Index: Integer): IXMLPackage_General;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPackage_General }

  TXMLPackage_General = class(TXMLNode, IXMLPackage_General)
  protected
    { IXMLPackage_General }
    function Get_Nom: UnicodeString;
    function Get_Cible: UnicodeString;
    function Get_IsPackageApplication: UnicodeString;
    function Get_Fichiers: IXMLFichiers;
    function Get_Repertoires: IXMLRepertoires;
    procedure Set_Nom(Value: UnicodeString);
    procedure Set_Cible(Value: UnicodeString);
    procedure Set_IsPackageApplication(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLOptions }

  TXMLOptions = class(TXMLNodeCollection, IXMLOptions)
  protected
    { IXMLOptions }
    function Get_Option(Index: Integer): IXMLOption;
    function Add: IXMLOption;
    function Insert(const Index: Integer): IXMLOption;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLOption }

  TXMLOption = class(TXMLNode, IXMLOption)
  protected
    { IXMLOption }
    function Get_ID: UnicodeString;
    function Get_Nom: UnicodeString;
    function Get_ChaineISS: UnicodeString;
    function Get_FichiersPackageApplication: IXMLFichiersPackageApplication;
    procedure Set_ID(Value: UnicodeString);
    procedure Set_Nom(Value: UnicodeString);
    procedure Set_ChaineISS(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLFichiersPackageApplication }

  TXMLFichiersPackageApplication = class(TXMLNode, IXMLFichiersPackageApplication)
  protected
    { IXMLFichiersPackageApplication }
    function Get_Fichiers: IXMLFichiers;
    function Get_Repertoires: IXMLRepertoires;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLProprietes }

  TXMLProprietes = class(TXMLNodeCollection, IXMLProprietes)
  protected
    { IXMLProprietes }
    function Get_Propriete(Index: Integer): IXMLPropriete;
    function Add: IXMLPropriete;
    function Insert(const Index: Integer): IXMLPropriete;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPropriete }

  TXMLPropriete = class(TXMLNode, IXMLPropriete)
  protected
    { IXMLPropriete }
    function Get_ID: UnicodeString;
    function Get_Nom: UnicodeString;
    function Get_ChaineISS: UnicodeString;
    function Get_IsExeName: UnicodeString;
    function Get_IsNomCompletLogiciel: UnicodeString;
    function Get_IsNomIndustriel: UnicodeString;
    function Get_IsIDPatchNote: UnicodeString;
    function Get_IsLocalPath: UnicodeString;
    function Get_IsIDSetup: UnicodeString;
    function Get_Edit_Type: UnicodeString;
    procedure Set_ID(Value: UnicodeString);
    procedure Set_Nom(Value: UnicodeString);
    procedure Set_ChaineISS(Value: UnicodeString);
    procedure Set_IsExeName(Value: UnicodeString);
    procedure Set_IsNomCompletLogiciel(Value: UnicodeString);
    procedure Set_IsNomIndustriel(Value: UnicodeString);
    procedure Set_IsIDPatchNote(Value: UnicodeString);
    procedure Set_IsLocalPath(Value: UnicodeString);
    procedure Set_IsIDSetup(Value: UnicodeString);
    procedure Set_Edit_Type(Value: UnicodeString);
  end;

{ TXMLPreferencesUtilitaire }

  TXMLPreferencesUtilitaire = class(TXMLNode, IXMLPreferencesUtilitaire)
  protected
    { IXMLPreferencesUtilitaire }
    function Get_AskSaveConfigFileOnExit: UnicodeString;
    function Get_AutoSaveConfigFileOnExit: UnicodeString;
    function Get_BackUpConfigFile: UnicodeString;
    function Get_BackUpSetup: UnicodeString;
    function Get_ExtractSetup: UnicodeString;
    function Get_ExtractSilencieux: UnicodeString;
    function Get_GenerateISSForSetupExecution: UnicodeString;
    function Get_SaveConfigLogicielBeforeISSForSetup: UnicodeString;
    function Get_SaveFileConfigFileOnSetupExecution: UnicodeString;
    procedure Set_AskSaveConfigFileOnExit(Value: UnicodeString);
    procedure Set_AutoSaveConfigFileOnExit(Value: UnicodeString);
    procedure Set_BackUpConfigFile(Value: UnicodeString);
    procedure Set_BackUpSetup(Value: UnicodeString);
    procedure Set_ExtractSetup(Value: UnicodeString);
    procedure Set_ExtractSilencieux(Value: UnicodeString);
    procedure Set_GenerateISSForSetupExecution(Value: UnicodeString);
    procedure Set_SaveConfigLogicielBeforeISSForSetup(Value: UnicodeString);
    procedure Set_SaveFileConfigFileOnSetupExecution(Value: UnicodeString);
  end;

{ TXMLSoftwares }

  TXMLSoftwares = class(TXMLNodeCollection, IXMLSoftwares)
  protected
    { IXMLSoftwares }
    function Get_Software(Index: Integer): IXMLSoftware;
    function Add: IXMLSoftware;
    function Insert(const Index: Integer): IXMLSoftware;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSoftware }

  TXMLSoftware = class(TXMLNode, IXMLSoftware)
  protected
    { IXMLSoftware }
    function Get_Proprietes_Logiciel: IXMLProprietes_Logiciel;
    function Get_Options_Logiciel: IXMLOptions_Logiciel;
    function Get_Packages_Generaux: IXMLPackages_Generaux;
    function Get_Packages_Communs_Logiciel: IXMLPackages_Communs_Logiciel;
    function Get_ModeRelease: UnicodeString;
    procedure Set_ModeRelease(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLProprietes_Logiciel }

  TXMLProprietes_Logiciel = class(TXMLNodeCollection, IXMLProprietes_Logiciel)
  protected
    { IXMLProprietes_Logiciel }
    function Get_Propriete_Logiciel(Index: Integer): IXMLPropriete_Logiciel;
    function Add: IXMLPropriete_Logiciel;
    function Insert(const Index: Integer): IXMLPropriete_Logiciel;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPropriete_Logiciel }

  TXMLPropriete_Logiciel = class(TXMLNode, IXMLPropriete_Logiciel)
  protected
    { IXMLPropriete_Logiciel }
    function Get_ID: UnicodeString;
    function Get_Valeur: UnicodeString;
    procedure Set_ID(Value: UnicodeString);
    procedure Set_Valeur(Value: UnicodeString);
  end;

{ TXMLOptions_Logiciel }

  TXMLOptions_Logiciel = class(TXMLNodeCollection, IXMLOptions_Logiciel)
  protected
    { IXMLOptions_Logiciel }
    function Get_Option_Logiciel(Index: Integer): IXMLOption_Logiciel;
    function Add: IXMLOption_Logiciel;
    function Insert(const Index: Integer): IXMLOption_Logiciel;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLOption_Logiciel }

  TXMLOption_Logiciel = class(TXMLNode, IXMLOption_Logiciel)
  protected
    { IXMLOption_Logiciel }
    function Get_ID: UnicodeString;
    function Get_Valeur: UnicodeString;
    procedure Set_ID(Value: UnicodeString);
    procedure Set_Valeur(Value: UnicodeString);
  end;

{ TXMLPackages_Communs_Logiciel }

  TXMLPackages_Communs_Logiciel = class(TXMLNodeCollection, IXMLPackages_Communs_Logiciel)
  protected
    { IXMLPackages_Communs_Logiciel }
    function Get_Package_Commun_Logiciel(Index: Integer): IXMLPackage_Commun_Logiciel;
    function Add: IXMLPackage_Commun_Logiciel;
    function Insert(const Index: Integer): IXMLPackage_Commun_Logiciel;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPackage_Commun_Logiciel }

  TXMLPackage_Commun_Logiciel = class(TXMLNode, IXMLPackage_Commun_Logiciel)
  protected
    { IXMLPackage_Commun_Logiciel }
    function Get_ID: UnicodeString;
    function Get_Valeur: UnicodeString;
    procedure Set_ID(Value: UnicodeString);
    procedure Set_Valeur(Value: UnicodeString);
  end;

{ Fonctions globales }

function Getroot(Doc: IXMLDocument): IXMLRoot;
function Loadroot(const FileName: string): IXMLRoot;
function Newroot: IXMLRoot;

const
  TargetNamespace = '';

implementation

{ Fonctions globales }

function Getroot(Doc: IXMLDocument): IXMLRoot;
begin
  Result := Doc.GetDocBinding('root', TXMLRoot, TargetNamespace) as IXMLRoot;
end;

function Loadroot(const FileName: string): IXMLRoot;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('root', TXMLRoot, TargetNamespace) as IXMLRoot;
end;

function Newroot: IXMLRoot;
begin
  Result := NewXMLDocument.GetDocBinding('root', TXMLRoot, TargetNamespace) as IXMLRoot;
end;

{ TXMLRoot }

procedure TXMLRoot.AfterConstruction;
begin
  RegisterChildNode('Configuration', TXMLConfiguration);
  RegisterChildNode('Softwares', TXMLSoftwares);
  inherited;
end;

function TXMLRoot.Get_Configuration: IXMLConfiguration;
begin
  Result := ChildNodes['Configuration'] as IXMLConfiguration;
end;

function TXMLRoot.Get_Softwares: IXMLSoftwares;
begin
  Result := ChildNodes['Softwares'] as IXMLSoftwares;
end;

{ TXMLConfiguration }

procedure TXMLConfiguration.AfterConstruction;
begin
  RegisterChildNode('Serveurs', TXMLServeurs);
  RegisterChildNode('Packages_Communs', TXMLPackages_Communs);
  RegisterChildNode('Packages_Generaux', TXMLPackages_Generaux);
  RegisterChildNode('Options', TXMLOptions);
  RegisterChildNode('Proprietes', TXMLProprietes);
  RegisterChildNode('PreferencesUtilitaire', TXMLPreferencesUtilitaire);
  inherited;
end;

function TXMLConfiguration.Get_Serveurs: IXMLServeurs;
begin
  Result := ChildNodes['Serveurs'] as IXMLServeurs;
end;

function TXMLConfiguration.Get_Packages_Communs: IXMLPackages_Communs;
begin
  Result := ChildNodes['Packages_Communs'] as IXMLPackages_Communs;
end;

function TXMLConfiguration.Get_Packages_Generaux: IXMLPackages_Generaux;
begin
  Result := ChildNodes['Packages_Generaux'] as IXMLPackages_Generaux;
end;

function TXMLConfiguration.Get_Options: IXMLOptions;
begin
  Result := ChildNodes['Options'] as IXMLOptions;
end;

function TXMLConfiguration.Get_Proprietes: IXMLProprietes;
begin
  Result := ChildNodes['Proprietes'] as IXMLProprietes;
end;

function TXMLConfiguration.Get_PreferencesUtilitaire: IXMLPreferencesUtilitaire;
begin
  Result := ChildNodes['PreferencesUtilitaire'] as IXMLPreferencesUtilitaire;
end;

{ TXMLServeurs }

function TXMLServeurs.Get_Serveur_Setup: UnicodeString;
begin
  Result := ChildNodes['Serveur_Setup'].Text;
end;

procedure TXMLServeurs.Set_Serveur_Setup(Value: UnicodeString);
begin
  ChildNodes['Serveur_Setup'].NodeValue := Value;
end;

function TXMLServeurs.Get_Serveur_Update: UnicodeString;
begin
  Result := ChildNodes['Serveur_Update'].Text;
end;

procedure TXMLServeurs.Set_Serveur_Update(Value: UnicodeString);
begin
  ChildNodes['Serveur_Update'].NodeValue := Value;
end;

function TXMLServeurs.Get_Serveur_PatchNote_Dev: UnicodeString;
begin
  Result := ChildNodes['Serveur_PatchNote_Dev'].Text;
end;

procedure TXMLServeurs.Set_Serveur_PatchNote_Dev(Value: UnicodeString);
begin
  ChildNodes['Serveur_PatchNote_Dev'].NodeValue := Value;
end;

function TXMLServeurs.Get_Serveur_PatchNote_Client: UnicodeString;
begin
  Result := ChildNodes['Serveur_PatchNote_Client'].Text;
end;

procedure TXMLServeurs.Set_Serveur_PatchNote_Client(Value: UnicodeString);
begin
  ChildNodes['Serveur_PatchNote_Client'].NodeValue := Value;
end;

function TXMLServeurs.Get_Serveur_BackUp: UnicodeString;
begin
  Result := ChildNodes['Serveur_BackUp'].Text;
end;

procedure TXMLServeurs.Set_Serveur_BackUp(Value: UnicodeString);
begin
  ChildNodes['Serveur_BackUp'].NodeValue := Value;
end;

function TXMLServeurs.Get_Serveur_Download_Dev: UnicodeString;
begin
  Result := ChildNodes['Serveur_Download_Dev'].Text;
end;

procedure TXMLServeurs.Set_Serveur_Download_Dev(Value: UnicodeString);
begin
  ChildNodes['Serveur_Download_Dev'].NodeValue := Value;
end;

function TXMLServeurs.Get_Serveur_Download_Client: UnicodeString;
begin
  Result := ChildNodes['Serveur_Download_Client'].Text;
end;

procedure TXMLServeurs.Set_Serveur_Download_Client(Value: UnicodeString);
begin
  ChildNodes['Serveur_Download_Client'].NodeValue := Value;
end;

{ TXMLPackages_Communs }

procedure TXMLPackages_Communs.AfterConstruction;
begin
  RegisterChildNode('Package', TXMLPackage);
  ItemTag := 'Package';
  ItemInterface := IXMLPackage;
  inherited;
end;

function TXMLPackages_Communs.Get_Package(Index: Integer): IXMLPackage;
begin
  Result := List[Index] as IXMLPackage;
end;

function TXMLPackages_Communs.Add: IXMLPackage;
begin
  Result := AddItem(-1) as IXMLPackage;
end;

function TXMLPackages_Communs.Insert(const Index: Integer): IXMLPackage;
begin
  Result := AddItem(Index) as IXMLPackage;
end;

{ TXMLPackage }

procedure TXMLPackage.AfterConstruction;
begin
  RegisterChildNode('Fichiers', TXMLFichiers);
  RegisterChildNode('Repertoires', TXMLRepertoires);
  inherited;
end;

function TXMLPackage.Get_ID: UnicodeString;
begin
  Result := ChildNodes['ID'].Text;
end;

procedure TXMLPackage.Set_ID(Value: UnicodeString);
begin
  ChildNodes['ID'].NodeValue := Value;
end;

function TXMLPackage.Get_Nom: UnicodeString;
begin
  Result := ChildNodes['Nom'].Text;
end;

procedure TXMLPackage.Set_Nom(Value: UnicodeString);
begin
  ChildNodes['Nom'].NodeValue := Value;
end;

function TXMLPackage.Get_Cible: UnicodeString;
begin
  Result := ChildNodes['Cible'].Text;
end;

procedure TXMLPackage.Set_Cible(Value: UnicodeString);
begin
  ChildNodes['Cible'].NodeValue := Value;
end;

function TXMLPackage.Get_Fichiers: IXMLFichiers;
begin
  Result := ChildNodes['Fichiers'] as IXMLFichiers;
end;

function TXMLPackage.Get_Repertoires: IXMLRepertoires;
begin
  Result := ChildNodes['Repertoires'] as IXMLRepertoires;
end;

{ TXMLFichiers }

procedure TXMLFichiers.AfterConstruction;
begin
  RegisterChildNode('Fichier', TXMLFichier);
  ItemTag := 'Fichier';
  ItemInterface := IXMLFichier;
  inherited;
end;

function TXMLFichiers.Get_Fichier(Index: Integer): IXMLFichier;
begin
  Result := List[Index] as IXMLFichier;
end;

function TXMLFichiers.Add: IXMLFichier;
begin
  Result := AddItem(-1) as IXMLFichier;
end;

function TXMLFichiers.Insert(const Index: Integer): IXMLFichier;
begin
  Result := AddItem(Index) as IXMLFichier;
end;

{ TXMLFichier }

function TXMLFichier.Get_Cible: UnicodeString;
begin
  Result := ChildNodes['Cible'].Text;
end;

procedure TXMLFichier.Set_Cible(Value: UnicodeString);
begin
  ChildNodes['Cible'].NodeValue := Value;
end;

function TXMLFichier.Get_Valeur: UnicodeString;
begin
  Result := ChildNodes['Valeur'].Text;
end;

procedure TXMLFichier.Set_Valeur(Value: UnicodeString);
begin
  ChildNodes['Valeur'].NodeValue := Value;
end;

{ TXMLRepertoires }

procedure TXMLRepertoires.AfterConstruction;
begin
  RegisterChildNode('Repertoire', TXMLRepertoire);
  ItemTag := 'Repertoire';
  ItemInterface := IXMLRepertoire;
  inherited;
end;

function TXMLRepertoires.Get_Repertoire(Index: Integer): IXMLRepertoire;
begin
  Result := List[Index] as IXMLRepertoire;
end;

function TXMLRepertoires.Add: IXMLRepertoire;
begin
  Result := AddItem(-1) as IXMLRepertoire;
end;

function TXMLRepertoires.Insert(const Index: Integer): IXMLRepertoire;
begin
  Result := AddItem(Index) as IXMLRepertoire;
end;

{ TXMLRepertoire }

function TXMLRepertoire.Get_Cible: UnicodeString;
begin
  Result := ChildNodes['Cible'].Text;
end;

procedure TXMLRepertoire.Set_Cible(Value: UnicodeString);
begin
  ChildNodes['Cible'].NodeValue := Value;
end;

function TXMLRepertoire.Get_Valeur: UnicodeString;
begin
  Result := ChildNodes['Valeur'].Text;
end;

procedure TXMLRepertoire.Set_Valeur(Value: UnicodeString);
begin
  ChildNodes['Valeur'].NodeValue := Value;
end;

{ TXMLPackages_Generaux }

procedure TXMLPackages_Generaux.AfterConstruction;
begin
  RegisterChildNode('Package_General', TXMLPackage_General);
  ItemTag := 'Package_General';
  ItemInterface := IXMLPackage_General;
  inherited;
end;

function TXMLPackages_Generaux.Get_Package_General(Index: Integer): IXMLPackage_General;
begin
  Result := List[Index] as IXMLPackage_General;
end;

function TXMLPackages_Generaux.Add: IXMLPackage_General;
begin
  Result := AddItem(-1) as IXMLPackage_General;
end;

function TXMLPackages_Generaux.Insert(const Index: Integer): IXMLPackage_General;
begin
  Result := AddItem(Index) as IXMLPackage_General;
end;

{ TXMLPackage_General }

procedure TXMLPackage_General.AfterConstruction;
begin
  RegisterChildNode('Fichiers', TXMLFichiers);
  RegisterChildNode('Repertoires', TXMLRepertoires);
  inherited;
end;

function TXMLPackage_General.Get_Nom: UnicodeString;
begin
  Result := ChildNodes['Nom'].Text;
end;

procedure TXMLPackage_General.Set_Nom(Value: UnicodeString);
begin
  ChildNodes['Nom'].NodeValue := Value;
end;

function TXMLPackage_General.Get_Cible: UnicodeString;
begin
  Result := ChildNodes['Cible'].Text;
end;

procedure TXMLPackage_General.Set_Cible(Value: UnicodeString);
begin
  ChildNodes['Cible'].NodeValue := Value;
end;

function TXMLPackage_General.Get_IsPackageApplication: UnicodeString;
begin
  Result := ChildNodes['IsPackageApplication'].Text;
end;

procedure TXMLPackage_General.Set_IsPackageApplication(Value: UnicodeString);
begin
  ChildNodes['IsPackageApplication'].NodeValue := Value;
end;

function TXMLPackage_General.Get_Fichiers: IXMLFichiers;
begin
  Result := ChildNodes['Fichiers'] as IXMLFichiers;
end;

function TXMLPackage_General.Get_Repertoires: IXMLRepertoires;
begin
  Result := ChildNodes['Repertoires'] as IXMLRepertoires;
end;

{ TXMLOptions }

procedure TXMLOptions.AfterConstruction;
begin
  RegisterChildNode('Option', TXMLOption);
  ItemTag := 'Option';
  ItemInterface := IXMLOption;
  inherited;
end;

function TXMLOptions.Get_Option(Index: Integer): IXMLOption;
begin
  Result := List[Index] as IXMLOption;
end;

function TXMLOptions.Add: IXMLOption;
begin
  Result := AddItem(-1) as IXMLOption;
end;

function TXMLOptions.Insert(const Index: Integer): IXMLOption;
begin
  Result := AddItem(Index) as IXMLOption;
end;

{ TXMLOption }

procedure TXMLOption.AfterConstruction;
begin
  RegisterChildNode('FichiersPackageApplication', TXMLFichiersPackageApplication);
  inherited;
end;

function TXMLOption.Get_ID: UnicodeString;
begin
  Result := ChildNodes['ID'].Text;
end;

procedure TXMLOption.Set_ID(Value: UnicodeString);
begin
  ChildNodes['ID'].NodeValue := Value;
end;

function TXMLOption.Get_Nom: UnicodeString;
begin
  Result := ChildNodes['Nom'].Text;
end;

procedure TXMLOption.Set_Nom(Value: UnicodeString);
begin
  ChildNodes['Nom'].NodeValue := Value;
end;

function TXMLOption.Get_ChaineISS: UnicodeString;
begin
  Result := ChildNodes['ChaineISS'].Text;
end;

procedure TXMLOption.Set_ChaineISS(Value: UnicodeString);
begin
  ChildNodes['ChaineISS'].NodeValue := Value;
end;

function TXMLOption.Get_FichiersPackageApplication: IXMLFichiersPackageApplication;
begin
  Result := ChildNodes['FichiersPackageApplication'] as IXMLFichiersPackageApplication;
end;

{ TXMLFichiersPackageApplication }

procedure TXMLFichiersPackageApplication.AfterConstruction;
begin
  RegisterChildNode('Fichiers', TXMLFichiers);
  RegisterChildNode('Repertoires', TXMLRepertoires);
  inherited;
end;

function TXMLFichiersPackageApplication.Get_Fichiers: IXMLFichiers;
begin
  Result := ChildNodes['Fichiers'] as IXMLFichiers;
end;

function TXMLFichiersPackageApplication.Get_Repertoires: IXMLRepertoires;
begin
  Result := ChildNodes['Repertoires'] as IXMLRepertoires;
end;

{ TXMLProprietes }

procedure TXMLProprietes.AfterConstruction;
begin
  RegisterChildNode('Propriete', TXMLPropriete);
  ItemTag := 'Propriete';
  ItemInterface := IXMLPropriete;
  inherited;
end;

function TXMLProprietes.Get_Propriete(Index: Integer): IXMLPropriete;
begin
  Result := List[Index] as IXMLPropriete;
end;

function TXMLProprietes.Add: IXMLPropriete;
begin
  Result := AddItem(-1) as IXMLPropriete;
end;

function TXMLProprietes.Insert(const Index: Integer): IXMLPropriete;
begin
  Result := AddItem(Index) as IXMLPropriete;
end;

{ TXMLPropriete }

function TXMLPropriete.Get_ID: UnicodeString;
begin
  Result := ChildNodes['ID'].Text;
end;

procedure TXMLPropriete.Set_ID(Value: UnicodeString);
begin
  ChildNodes['ID'].NodeValue := Value;
end;

function TXMLPropriete.Get_Nom: UnicodeString;
begin
  Result := ChildNodes['Nom'].Text;
end;

procedure TXMLPropriete.Set_Nom(Value: UnicodeString);
begin
  ChildNodes['Nom'].NodeValue := Value;
end;

function TXMLPropriete.Get_ChaineISS: UnicodeString;
begin
  Result := ChildNodes['ChaineISS'].Text;
end;

procedure TXMLPropriete.Set_ChaineISS(Value: UnicodeString);
begin
  ChildNodes['ChaineISS'].NodeValue := Value;
end;

function TXMLPropriete.Get_IsExeName: UnicodeString;
begin
  Result := ChildNodes['IsExeName'].Text;
end;

procedure TXMLPropriete.Set_IsExeName(Value: UnicodeString);
begin
  ChildNodes['IsExeName'].NodeValue := Value;
end;

function TXMLPropriete.Get_IsNomCompletLogiciel: UnicodeString;
begin
  Result := ChildNodes['IsNomCompletLogiciel'].Text;
end;

procedure TXMLPropriete.Set_IsNomCompletLogiciel(Value: UnicodeString);
begin
  ChildNodes['IsNomCompletLogiciel'].NodeValue := Value;
end;

function TXMLPropriete.Get_IsNomIndustriel: UnicodeString;
begin
  Result := ChildNodes['IsNomIndustriel'].Text;
end;

procedure TXMLPropriete.Set_IsNomIndustriel(Value: UnicodeString);
begin
  ChildNodes['IsNomIndustriel'].NodeValue := Value;
end;

function TXMLPropriete.Get_IsIDPatchNote: UnicodeString;
begin
  Result := ChildNodes['IsIDPatchNote'].Text;
end;

procedure TXMLPropriete.Set_IsIDPatchNote(Value: UnicodeString);
begin
  ChildNodes['IsIDPatchNote'].NodeValue := Value;
end;

function TXMLPropriete.Get_IsLocalPath: UnicodeString;
begin
  Result := ChildNodes['IsLocalPath'].Text;
end;

procedure TXMLPropriete.Set_IsLocalPath(Value: UnicodeString);
begin
  ChildNodes['IsLocalPath'].NodeValue := Value;
end;

function TXMLPropriete.Get_IsIDSetup: UnicodeString;
begin
  Result := ChildNodes['IsIDSetup'].Text;
end;

procedure TXMLPropriete.Set_IsIDSetup(Value: UnicodeString);
begin
  ChildNodes['IsIDSetup'].NodeValue := Value;
end;

function TXMLPropriete.Get_Edit_Type: UnicodeString;
begin
  Result := ChildNodes['Edit_Type'].Text;
end;

procedure TXMLPropriete.Set_Edit_Type(Value: UnicodeString);
begin
  ChildNodes['Edit_Type'].NodeValue := Value;
end;

{ TXMLPreferencesUtilitaire }

function TXMLPreferencesUtilitaire.Get_AskSaveConfigFileOnExit: UnicodeString;
begin
  Result := ChildNodes['AskSaveConfigFileOnExit'].Text;
end;

procedure TXMLPreferencesUtilitaire.Set_AskSaveConfigFileOnExit(Value: UnicodeString);
begin
  ChildNodes['AskSaveConfigFileOnExit'].NodeValue := Value;
end;

function TXMLPreferencesUtilitaire.Get_AutoSaveConfigFileOnExit: UnicodeString;
begin
  Result := ChildNodes['AutoSaveConfigFileOnExit'].Text;
end;

procedure TXMLPreferencesUtilitaire.Set_AutoSaveConfigFileOnExit(Value: UnicodeString);
begin
  ChildNodes['AutoSaveConfigFileOnExit'].NodeValue := Value;
end;

function TXMLPreferencesUtilitaire.Get_BackUpConfigFile: UnicodeString;
begin
  Result := ChildNodes['BackUpConfigFile'].Text;
end;

procedure TXMLPreferencesUtilitaire.Set_BackUpConfigFile(Value: UnicodeString);
begin
  ChildNodes['BackUpConfigFile'].NodeValue := Value;
end;

function TXMLPreferencesUtilitaire.Get_BackUpSetup: UnicodeString;
begin
  Result := ChildNodes['BackUpSetup'].Text;
end;

procedure TXMLPreferencesUtilitaire.Set_BackUpSetup(Value: UnicodeString);
begin
  ChildNodes['BackUpSetup'].NodeValue := Value;
end;

function TXMLPreferencesUtilitaire.Get_ExtractSetup: UnicodeString;
begin
  Result := ChildNodes['ExtractSetup'].Text;
end;

procedure TXMLPreferencesUtilitaire.Set_ExtractSetup(Value: UnicodeString);
begin
  ChildNodes['ExtractSetup'].NodeValue := Value;
end;

function TXMLPreferencesUtilitaire.Get_ExtractSilencieux: UnicodeString;
begin
  Result := ChildNodes['ExtractSilencieux'].Text;
end;

procedure TXMLPreferencesUtilitaire.Set_ExtractSilencieux(Value: UnicodeString);
begin
  ChildNodes['ExtractSilencieux'].NodeValue := Value;
end;

function TXMLPreferencesUtilitaire.Get_GenerateISSForSetupExecution: UnicodeString;
begin
  Result := ChildNodes['GenerateISSForSetupExecution'].Text;
end;

procedure TXMLPreferencesUtilitaire.Set_GenerateISSForSetupExecution(Value: UnicodeString);
begin
  ChildNodes['GenerateISSForSetupExecution'].NodeValue := Value;
end;

function TXMLPreferencesUtilitaire.Get_SaveConfigLogicielBeforeISSForSetup: UnicodeString;
begin
  Result := ChildNodes['SaveConfigLogicielBeforeISSForSetup'].Text;
end;

procedure TXMLPreferencesUtilitaire.Set_SaveConfigLogicielBeforeISSForSetup(Value: UnicodeString);
begin
  ChildNodes['SaveConfigLogicielBeforeISSForSetup'].NodeValue := Value;
end;

function TXMLPreferencesUtilitaire.Get_SaveFileConfigFileOnSetupExecution: UnicodeString;
begin
  Result := ChildNodes['SaveFileConfigFileOnSetupExecution'].Text;
end;

procedure TXMLPreferencesUtilitaire.Set_SaveFileConfigFileOnSetupExecution(Value: UnicodeString);
begin
  ChildNodes['SaveFileConfigFileOnSetupExecution'].NodeValue := Value;
end;

{ TXMLSoftwares }

procedure TXMLSoftwares.AfterConstruction;
begin
  RegisterChildNode('Software', TXMLSoftware);
  ItemTag := 'Software';
  ItemInterface := IXMLSoftware;
  inherited;
end;

function TXMLSoftwares.Get_Software(Index: Integer): IXMLSoftware;
begin
  Result := List[Index] as IXMLSoftware;
end;

function TXMLSoftwares.Add: IXMLSoftware;
begin
  Result := AddItem(-1) as IXMLSoftware;
end;

function TXMLSoftwares.Insert(const Index: Integer): IXMLSoftware;
begin
  Result := AddItem(Index) as IXMLSoftware;
end;

{ TXMLSoftware }

procedure TXMLSoftware.AfterConstruction;
begin
  RegisterChildNode('Proprietes_Logiciel', TXMLProprietes_Logiciel);
  RegisterChildNode('Options_Logiciel', TXMLOptions_Logiciel);
  RegisterChildNode('Packages_Generaux', TXMLPackages_Generaux);
  RegisterChildNode('Packages_Communs_Logiciel', TXMLPackages_Communs_Logiciel);
  inherited;
end;

function TXMLSoftware.Get_Proprietes_Logiciel: IXMLProprietes_Logiciel;
begin
  Result := ChildNodes['Proprietes_Logiciel'] as IXMLProprietes_Logiciel;
end;

function TXMLSoftware.Get_Options_Logiciel: IXMLOptions_Logiciel;
begin
  Result := ChildNodes['Options_Logiciel'] as IXMLOptions_Logiciel;
end;

function TXMLSoftware.Get_Packages_Generaux: IXMLPackages_Generaux;
begin
  Result := ChildNodes['Packages_Generaux'] as IXMLPackages_Generaux;
end;

function TXMLSoftware.Get_Packages_Communs_Logiciel: IXMLPackages_Communs_Logiciel;
begin
  Result := ChildNodes['Packages_Communs_Logiciel'] as IXMLPackages_Communs_Logiciel;
end;

function TXMLSoftware.Get_ModeRelease: UnicodeString;
begin
  Result := ChildNodes['ModeRelease'].Text;
end;

procedure TXMLSoftware.Set_ModeRelease(Value: UnicodeString);
begin
  ChildNodes['ModeRelease'].NodeValue := Value;
end;

{ TXMLProprietes_Logiciel }

procedure TXMLProprietes_Logiciel.AfterConstruction;
begin
  RegisterChildNode('Propriete_Logiciel', TXMLPropriete_Logiciel);
  ItemTag := 'Propriete_Logiciel';
  ItemInterface := IXMLPropriete_Logiciel;
  inherited;
end;

function TXMLProprietes_Logiciel.Get_Propriete_Logiciel(Index: Integer): IXMLPropriete_Logiciel;
begin
  Result := List[Index] as IXMLPropriete_Logiciel;
end;

function TXMLProprietes_Logiciel.Add: IXMLPropriete_Logiciel;
begin
  Result := AddItem(-1) as IXMLPropriete_Logiciel;
end;

function TXMLProprietes_Logiciel.Insert(const Index: Integer): IXMLPropriete_Logiciel;
begin
  Result := AddItem(Index) as IXMLPropriete_Logiciel;
end;

{ TXMLPropriete_Logiciel }

function TXMLPropriete_Logiciel.Get_ID: UnicodeString;
begin
  Result := ChildNodes['ID'].Text;
end;

procedure TXMLPropriete_Logiciel.Set_ID(Value: UnicodeString);
begin
  ChildNodes['ID'].NodeValue := Value;
end;

function TXMLPropriete_Logiciel.Get_Valeur: UnicodeString;
begin
  Result := ChildNodes['Valeur'].Text;
end;

procedure TXMLPropriete_Logiciel.Set_Valeur(Value: UnicodeString);
begin
  ChildNodes['Valeur'].NodeValue := Value;
end;

{ TXMLOptions_Logiciel }

procedure TXMLOptions_Logiciel.AfterConstruction;
begin
  RegisterChildNode('Option_Logiciel', TXMLOption_Logiciel);
  ItemTag := 'Option_Logiciel';
  ItemInterface := IXMLOption_Logiciel;
  inherited;
end;

function TXMLOptions_Logiciel.Get_Option_Logiciel(Index: Integer): IXMLOption_Logiciel;
begin
  Result := List[Index] as IXMLOption_Logiciel;
end;

function TXMLOptions_Logiciel.Add: IXMLOption_Logiciel;
begin
  Result := AddItem(-1) as IXMLOption_Logiciel;
end;

function TXMLOptions_Logiciel.Insert(const Index: Integer): IXMLOption_Logiciel;
begin
  Result := AddItem(Index) as IXMLOption_Logiciel;
end;

{ TXMLOption_Logiciel }

function TXMLOption_Logiciel.Get_ID: UnicodeString;
begin
  Result := ChildNodes['ID'].Text;
end;

procedure TXMLOption_Logiciel.Set_ID(Value: UnicodeString);
begin
  ChildNodes['ID'].NodeValue := Value;
end;

function TXMLOption_Logiciel.Get_Valeur: UnicodeString;
begin
  Result := ChildNodes['Valeur'].Text;
end;

procedure TXMLOption_Logiciel.Set_Valeur(Value: UnicodeString);
begin
  ChildNodes['Valeur'].NodeValue := Value;
end;

{ TXMLPackages_Communs_Logiciel }

procedure TXMLPackages_Communs_Logiciel.AfterConstruction;
begin
  RegisterChildNode('Package_Commun_Logiciel', TXMLPackage_Commun_Logiciel);
  ItemTag := 'Package_Commun_Logiciel';
  ItemInterface := IXMLPackage_Commun_Logiciel;
  inherited;
end;

function TXMLPackages_Communs_Logiciel.Get_Package_Commun_Logiciel(Index: Integer): IXMLPackage_Commun_Logiciel;
begin
  Result := List[Index] as IXMLPackage_Commun_Logiciel;
end;

function TXMLPackages_Communs_Logiciel.Add: IXMLPackage_Commun_Logiciel;
begin
  Result := AddItem(-1) as IXMLPackage_Commun_Logiciel;
end;

function TXMLPackages_Communs_Logiciel.Insert(const Index: Integer): IXMLPackage_Commun_Logiciel;
begin
  Result := AddItem(Index) as IXMLPackage_Commun_Logiciel;
end;

{ TXMLPackage_Commun_Logiciel }

function TXMLPackage_Commun_Logiciel.Get_ID: UnicodeString;
begin
  Result := ChildNodes['ID'].Text;
end;

procedure TXMLPackage_Commun_Logiciel.Set_ID(Value: UnicodeString);
begin
  ChildNodes['ID'].NodeValue := Value;
end;

function TXMLPackage_Commun_Logiciel.Get_Valeur: UnicodeString;
begin
  Result := ChildNodes['Valeur'].Text;
end;

procedure TXMLPackage_Commun_Logiciel.Set_Valeur(Value: UnicodeString);
begin
  ChildNodes['Valeur'].NodeValue := Value;
end;

end.
