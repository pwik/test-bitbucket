// ************************************************************************ //
// Les types d�clar�s dans ce fichier ont �t� g�n�r�s � partir de donn�es lues
// depuis le fichier WSDL d�crit ci-dessous :
// WSDL     : http://multi/WebServices/CallBat.exe/wsdl/ICallBat
// Version  : 1.0
// (10/03/2016 13:35:12 - - $Rev: 68735 $)
// ************************************************************************ //

unit ServiceWebSetup;

interface

uses Soap.InvokeRegistry, Soap.SOAPHTTPClient, System.Types, Soap.XSBuiltIns;

type

  // ************************************************************************ //
  // Les types suivants mentionn�s dans le document WSDL ne sont pas repr�sent�s
  // dans ce fichier. Ce sont des alias[@] d'autres types repr�sent�s ou alors ils �taient r�f�renc�s
  // mais jamais[!] d�clar�s dans le document. Les types de la derni�re cat�gorie
  // sont en principe mapp�s sur des types Embarcadero ou XML pr�d�finis/connus. Toutefois, ils peuvent aussi 
  // signaler des documents WSDL incorrects n'ayant pas r�ussi � d�clarer ou importer un type de sch�ma.
  // ************************************************************************ //
  // !:int             - "http://www.w3.org/2001/XMLSchema"[]
  // !:unsignedInt     - "http://www.w3.org/2001/XMLSchema"[]
  // !:string          - "http://www.w3.org/2001/XMLSchema"[]


  // ************************************************************************ //
  // Espace de nommage : urn:CallBatIntf-ICallBat
  // soapAction : urn:CallBatIntf-ICallBat#%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : rpc
  // utiliser   : encoded
  // Liaison : ICallBatbinding
  // service   : ICallBatservice
  // port      : ICallBatPort
  // URL       : http://multi/WebServices/CallBat.exe/soap/ICallBat
  // ************************************************************************ //
  ICallBat = interface(IInvokable)
  ['{A4F1B4AF-B68D-D263-8B6D-7226AECD9DCC}']
    function  Start(const quoi: string; var PiD: Cardinal): Integer; stdcall;
    function  StartISS(const quoi: string; var PiD: Cardinal): Integer; stdcall;
    function  EnCours(const PiD: Cardinal): Integer; stdcall;
  end;

function GetICallBat(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): ICallBat;


implementation
  uses System.SysUtils;

function GetICallBat(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): ICallBat;
const
  defWSDL = 'http://multi/WebServices/CallBat.exe/wsdl/ICallBat';
  defURL  = 'http://multi/WebServices/CallBat.exe/soap/ICallBat';
  defSvc  = 'ICallBatservice';
  defPrt  = 'ICallBatPort';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as ICallBat);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  { ICallBat }
  InvRegistry.RegisterInterface(TypeInfo(ICallBat), 'urn:CallBatIntf-ICallBat', '');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(ICallBat), 'urn:CallBatIntf-ICallBat#%operationName%');

end.