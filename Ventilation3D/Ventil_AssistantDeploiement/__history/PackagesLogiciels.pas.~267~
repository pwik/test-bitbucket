unit PackagesLogiciels;

interface
uses
  Classes,
  CAD_Containers,
  Zip,
  Config_XML;


type
{
  TZipFileHelper = class helper for TZipFile
    procedure Delete(FileName: string);
    Procedure Clear;
  end;
}
  TPackageLogiciel = class
    constructor Create;
    Destructor Destroy; override;
  private
    function GenereChaineFichierDate : String;
    procedure ZipXMLPackage(_Zip : TZipFile; _XMLPackage : IXMLPackage_General; _LocalPath : String; _DeletePathCible : Boolean);
    procedure ZipFichiersTraitementSpecial(_Zip : TZipFile; _LocalPath : String);
    procedure AddBinariesXMLPackage(_XMLPackage : IXMLPackage_General; _LocalPath : String; _Adresse : String; _ForceDateFichiers : Boolean);
    procedure AddBinariesFichiersTraitementSpecial(_LocalPath : String; _Adresse : String; _ForceDateFichiers : Boolean);
    procedure UpdateServeurSetup(_Adresse : String; _LocalPath : String; _ForceDateFichiers : Boolean);
    procedure UpdateServeurUpdater(_Adresse : String; _LocalPath : String);
    procedure UpdateVersionDev(_Zip : TZipFile; _LocalPath : String);
    function IsPackageApplication : Boolean;
  public
    XML : IXMLPackage_General;
    XMLSpecifique : IXMLPackage_General;
    FichiersTraitementSpecial : TStringList;
    Checked : Boolean;
    NewDate : Extended;
    SaveDate : Extended;
  end;

  TListePackagesLogiciels = Class(TTypedList<TPackageLogiciel>)
  public
    Function GetPackageApplication : TPackageLogiciel;
    procedure AjoutePackagesXMLSpecifiques(_XMLPackagesSpecifiques : IXMLPackages_Generaux);
    procedure UpdateSaveDatePackages(_stl : TStringList);
    function GetContenuFichierDate : TStringList;
    procedure UpdateServeurSetupPackages(_Adresse : String; _LocalPath : String; _ForceDateFichiers : Boolean);
    procedure UpdateServeurUpdaterPackages(_Adresse : String; _LocalPath : String);
    procedure UpdateVersionDev(_Zip : TZipFile; _LocalPath : String);
  private
  end;

implementation

uses
  Utils,
  CAD_ViloUtils,
  SysUtils;

//*****************************************************************************
//##############################################################################
//# TPackageLogiciel                                                           #
//##############################################################################
//******************************************************************************
constructor TPackageLogiciel.Create;
begin
  Checked := False;
  NewDate := Date;
  SaveDate := 0;
  FichiersTraitementSpecial := TStringList.Create;
end;
//******************************************************************************
Destructor TPackageLogiciel.Destroy;
begin
  FichiersTraitementSpecial.Destroy;
end;
//******************************************************************************
Function TPackageLogiciel.GenereChaineFichierDate : String;
Var
  WorkingDate : Extended;
begin
    if Checked then
      WorkingDate := NewDate
    else
      WorkingDate := SaveDate;

  Result := UpperCase(XML.Nom + '.ZIP') + '@' + FormatDateTime('dd/mm/yyyy', WorkingDate);;
end;
//******************************************************************************
procedure TPackageLogiciel.ZipXMLPackage(_Zip : TZipFile; _XMLPackage : IXMLPackage_General; _LocalPath : String; _DeletePathCible : Boolean);
Var
  cpt : Integer;
  tempSTL : TStringList;
  FilesDirectory : TStringList;
  DirectoryDirectory : TStringList;
  cptFilesDirectory : Integer;
  tempString : String;
begin

  if _XMLPackage = Nil then
    Exit;

    for cpt := 0 to _XMLPackage.Fichiers.Count - 1 do
      begin
      tempString := _XMLPackage.Fichiers[cpt].Valeur;
        if _DeletePathCible then
          tempString := StrReplace(tempString, _XMLPackage.Fichiers[cpt].Cible, '', false);
      tempString := StrReplace(tempString , '\', '/', False, True);
      _Zip.Add(_LocalPath + _XMLPackage.Fichiers[cpt].Valeur, tempString);
      end;
    for cpt := 0 to _XMLPackage.Repertoires.Count - 1 do
      begin
      tempSTL := TStringList.Create;
      tempSTL.Delimiter := '\';
      tempSTL.DelimitedText := _XMLPackage.Repertoires[cpt].Valeur;
      FilesDirectory := TStringList.Create;
      DirectoryDirectory := TStringList.Create;
      Util_ScruterRepertoire(_LocalPath + _XMLPackage.Repertoires[cpt].Valeur, FilesDirectory, DirectoryDirectory);
        for cptFilesDirectory := 0 to FilesDirectory.Count - 1 do
          begin
          FilesDirectory[cptFilesDirectory] := StrReplace(FilesDirectory[cptFilesDirectory], _LocalPath + _XMLPackage.Repertoires[cpt].Cible, '', False);
          tempString := _XMLPackage.Repertoires[cpt].Cible + FilesDirectory[cptFilesDirectory];
          tempString := StrReplace(tempString , '\', '/', False, True);
          _Zip.Add(_LocalPath + _XMLPackage.Repertoires[cpt].Cible + FilesDirectory[cptFilesDirectory], tempString);
          end;
      FilesDirectory.Destroy;
      DirectoryDirectory.Destroy;
      tempSTL.Destroy;
      end;
end;
//******************************************************************************
procedure TPackageLogiciel.ZipFichiersTraitementSpecial(_Zip : TZipFile; _LocalPath : String);
Var
  cpt : Integer;
  tempString : String;
begin
    for cpt := 0 to FichiersTraitementSpecial.Count - 1 do
      begin
      tempString := FichiersTraitementSpecial[cpt];
      tempString := StrReplace(tempString , '\', '/', False, True);
      _Zip.Add(_LocalPath + FichiersTraitementSpecial[cpt], tempString);
      end;
end;
//******************************************************************************
procedure TPackageLogiciel.AddBinariesXMLPackage(_XMLPackage : IXMLPackage_General; _LocalPath : String; _Adresse : String; _ForceDateFichiers : Boolean);
Var
  cpt : Integer;
  tempSTL : TStringList;
  FilesDirectory : TStringList;
  DirectoryDirectory : TStringList;
  cptFilesDirectory : Integer;
  tempString : String;
begin

  if _XMLPackage = Nil then
    Exit;

    for cpt := 0 to _XMLPackage.Fichiers.Count - 1 do
      begin
      tempString := _XMLPackage.Fichiers[cpt].Valeur;
      tempString := StrReplace(tempString, _XMLPackage.Fichiers[cpt].Cible, '', false);
      tempString := ExtractFilePath(tempString);
      Util_CopyFile(_LocalPath + _XMLPackage.Fichiers[cpt].Valeur, _Adresse + _XMLPackage.Fichiers[cpt].Cible + tempString, false, True, True, NewDate);
      end;
    for cpt := 0 to _XMLPackage.Repertoires.Count - 1 do
      begin
      tempSTL := TStringList.Create;
      tempSTL.Delimiter := '\';
      tempSTL.DelimitedText := _XMLPackage.Repertoires[cpt].Valeur;
      FilesDirectory := TStringList.Create;
      DirectoryDirectory := TStringList.Create;
      Util_ScruterRepertoire(_LocalPath + _XMLPackage.Repertoires[cpt].Valeur, FilesDirectory, DirectoryDirectory);
        for cptFilesDirectory := 0 to FilesDirectory.Count - 1 do
          begin
          FilesDirectory[cptFilesDirectory] := StrReplace(FilesDirectory[cptFilesDirectory], _LocalPath + _XMLPackage.Repertoires[cpt].Cible, '', False);
          Util_CopyFile(_LocalPath + _XMLPackage.Repertoires[cpt].Cible + FilesDirectory[cptFilesDirectory], _Adresse + _XMLPackage.Repertoires[cpt].Cible + ExtractFilePath(FilesDirectory[cptFilesDirectory]), false, True, True, NewDate);
          end;
      FilesDirectory.Destroy;
      DirectoryDirectory.Destroy;
      tempSTL.Destroy;
      end;
end;
//******************************************************************************
procedure TPackageLogiciel.AddBinariesFichiersTraitementSpecial(_LocalPath : String; _Adresse : String; _ForceDateFichiers : Boolean);
Var
  cpt : Integer;
begin
    for cpt := 0 to FichiersTraitementSpecial.Count - 1 do
      Util_CopyFile(_LocalPath + FichiersTraitementSpecial[cpt], _Adresse, false, True, True, NewDate);
end;
//******************************************************************************
procedure TPackageLogiciel.UpdateServeurSetup(_Adresse : String; _LocalPath : String; _ForceDateFichiers : Boolean);
Var
  tempLocalPath : String;
begin
  if Checked then
    begin
    tempLocalPath := _LocalPath + '\';

    AddBinariesXMLPackage(XML, tempLocalPath, _Adresse, _ForceDateFichiers);
    AddBinariesXMLPackage(XMLSpecifique, tempLocalPath, _Adresse, _ForceDateFichiers);
    AddBinariesFichiersTraitementSpecial(tempLocalPath, _Adresse, _ForceDateFichiers);

    end;
end;
//******************************************************************************
procedure TPackageLogiciel.UpdateServeurUpdater(_Adresse : String; _LocalPath : String);
Var
  Zip : TZipFile;
  tempNomPackage : String;
  tempLocalPath : String;
begin
  if Checked then
    begin
    tempNomPackage := _Adresse + UpperCase(XML.Nom) + '.ZIP';
    tempLocalPath := _LocalPath + '\';
    Util_DeleteFile(tempNomPackage);
    Zip := TZipFile.Create;

      try
      Zip.Open(tempNomPackage, zmWrite);
      ZipXMLPackage(Zip, XML, tempLocalPath, False);
      ZipXMLPackage(Zip, XMLSpecifique, tempLocalPath, False);
      ZipFichiersTraitementSpecial(Zip, tempLocalPath);
      Zip.Close;

      finally
      Zip.Free;
      end;

    end;
end;
//******************************************************************************
procedure TPackageLogiciel.UpdateVersionDev(_Zip : TZipFile; _LocalPath : String);
Var
  tempLocalPath : String;
begin
  tempLocalPath := _LocalPath + '\';
  ZipXMLPackage(_Zip, XML, tempLocalPath, True);
  ZipXMLPackage(_Zip, XMLSpecifique, tempLocalPath, True);
  ZipFichiersTraitementSpecial(_Zip, tempLocalPath);

end;
//******************************************************************************
function TPackageLogiciel.IsPackageApplication : Boolean;
begin
  Result := StrToBool(XML.IsPackageApplication);
end;
//******************************************************************************
//*****************************************************************************
//##############################################################################
//# TListePackagesLogiciels                                                    #
//##############################################################################
//******************************************************************************
Function TListePackagesLogiciels.GetPackageApplication : TPackageLogiciel;
Var
  cpt : Integer;
begin
  for cpt := 0 to Count - 1 do
    if Items[cpt].IsPackageApplication then
      begin
      Result := Items[cpt];
      Exit;
      end;
end;
//******************************************************************************
procedure TListePackagesLogiciels.AjoutePackagesXMLSpecifiques(_XMLPackagesSpecifiques : IXMLPackages_Generaux);
Var
  cpt : Integer;
  cpt2 : Integer;
begin
    if _XMLPackagesSpecifiques = Nil then
      Exit;

    for cpt := 0 to _XMLPackagesSpecifiques.Count - 1 do
      for cpt2 := 0 to Count - 1 do
        if UpperCase(_XMLPackagesSpecifiques[cpt].Nom) = UpperCase(Items[cpt2].XML.Nom) then
          begin
          Items[cpt2].XMLSpecifique := _XMLPackagesSpecifiques[cpt];
          break;
          end;

end;
//******************************************************************************
procedure TListePackagesLogiciels.UpdateSaveDatePackages(_stl : TStringList);
Var
  cpt : Integer;
  cptPackage : Integer;
  stlPackage : TStringList;
begin

    for cpt := 0 to _stl.Count - 1 do
      if Trim(_stl[cpt]) <> '' then
        begin
        stlPackage := TStringList.Create;
        stlPackage.Delimiter := '@';
        stlPackage.DelimitedText := _stl[cpt];;
        stlPackage[0] := StrReplace(stlPackage[0], '.ZIP', '', False);
          for cptPackage := 0 to Count - 1 do
            if UpperCase(stlPackage[0]) = UpperCase(Items[cptPackage].XML.Nom) then
              begin
              Items[cptPackage].SaveDate := StrToDate(stlPackage[1]);
              Items[cptPackage].NewDate := Items[cptPackage].SaveDate;
              end;
        stlPackage.Destroy;
        end;
end;
//******************************************************************************
function TListePackagesLogiciels.GetContenuFichierDate : TStringList;
Var
  cpt : Integer;
begin
  Result := TStringList.Create;
    for cpt := 0 to Count - 1 do
      Result.Add(Items[cpt].GenereChaineFichierDate);

end;
//******************************************************************************
procedure TListePackagesLogiciels.UpdateServeurSetupPackages(_Adresse : String; _LocalPath : String; _ForceDateFichiers : Boolean);
Var
  cpt : Integer;
begin
    for cpt := 0 to Count - 1 do
      Items[cpt].UpdateServeurSetup(_Adresse, _LocalPath, _ForceDateFichiers);

end;
//******************************************************************************
procedure TListePackagesLogiciels.UpdateServeurUpdaterPackages(_Adresse : String; _LocalPath : String);
Var
  cpt : Integer;
begin
    for cpt := 0 to Count - 1 do
      Items[cpt].UpdateServeurUpdater(_Adresse, _LocalPath);

end;
//******************************************************************************
procedure TListePackagesLogiciels.UpdateVersionDev(_Zip : TZipFile; _LocalPath : String);
Var
  cpt : Integer;
begin
    for cpt := 0 to Count - 1 do
      Items[cpt].UpdateVersionDev(_Zip, _LocalPath);

end;
//******************************************************************************
end.
