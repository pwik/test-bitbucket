Unit Ventil_ParametresGeneraux;
    
{$Include 'Ventil_Directives.pas'}

Interface

Uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Grids, BaseGrid, AdvGrid, Ventil_Etude, AsgLinks,
  AsgMemo, AdvObj;

Type
  TDlg_ParametresGeneraux = class(TForm)
    Grid_Properties: TAdvStringGrid;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    MemoEditLink1: TMemoEditLink;
    procedure Grid_PropertiesGetEditorType(Sender: TObject; ACol,
      ARow: Integer; var AEditor: TEditorType);
  Private
    FEtude: TVentil_Etude;
    Procedure MajEtude;
    Procedure AffEtude;
  Public
    { Public declarations }
  End;

Var
  Dlg_ParametresGeneraux: TDlg_ParametresGeneraux;


  Procedure SetParamsEtude(_Etude: TVentil_Etude);

Implementation
Uses Ventil_Types;

{$R *.dfm}

{ ************************************************************************************************************************************************** }
Procedure SetParamsEtude(_Etude: TVentil_Etude);
Begin
  Application.CreateForm(TDlg_ParametresGeneraux, Dlg_ParametresGeneraux);
  Dlg_ParametresGeneraux.FEtude := _Etude;
  Dlg_ParametresGeneraux.AffEtude;
  If Dlg_ParametresGeneraux.ShowModal = idOk Then Dlg_ParametresGeneraux.MajEtude;
  FreeAndNil(Dlg_ParametresGeneraux);
End;
{ ************************************************************************************************************************************************** }

{ TDlg_ParametresGeneraux }
Procedure TDlg_ParametresGeneraux.AffEtude;
Begin
  If FEtude = nil Then Exit;
  {$IfDef MVN_MVNAIR}
  {$Else}
  Grid_Properties.RowHeights[0] := 0;
  {$EndIf}
  Grid_Properties.Cells[1, 0] := FEtude.RefDossier;
  Grid_Properties.Cells[1, 1] := FEtude.Reference;
  Grid_Properties.Dates[1, 2] := FEtude.DateEtude;
  Grid_Properties.Cells[1, 3] := FEtude.Descriptif;
  Grid_Properties.Cells[1, 4] := FEtude.MOuvrage;
  Grid_Properties.Cells[1, 5] := FEtude.MOeuvre;
  Grid_Properties.Cells[1, 6] := FEtude.Installateur;
  Grid_Properties.Cells[1, 7] := FEtude.Objet;
  Grid_Properties.Cells[1, 8] := FEtude.Remarques.Text;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_ParametresGeneraux.MajEtude;
Begin
  If FEtude = nil Then Exit;
  FEtude.RefDossier     := Grid_Properties.Cells[1, 0];
  FEtude.Reference      := Grid_Properties.Cells[1, 1];
  FEtude.DateEtude      := Grid_Properties.Dates[1, 2];
  FEtude.Descriptif     := Grid_Properties.Cells[1, 3];
  FEtude.MOuvrage       := Grid_Properties.Cells[1, 4];
  FEtude.MOeuvre        := Grid_Properties.Cells[1, 5];
  FEtude.Installateur   := Grid_Properties.Cells[1, 6];
  FEtude.Objet          := Grid_Properties.Cells[1, 7];
  FEtude.Remarques.Text := Grid_Properties.Cells[1, 8];
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_ParametresGeneraux.Grid_PropertiesGetEditorType(Sender: TObject; ACol, ARow: Integer; var AEditor: TEditorType);
Begin
  AEditor := edNone;
  If ACol = 1 Then Case ARow Of
    2: AEditor := edDateEdit;
    8: Begin
        AEditor := edCustom;
        Grid_Properties.EditLink := MemoEditLink1;
       End;
    Else AEditor := edNormal;
  End;
End;
{ ************************************************************************************************************************************************** }

End.
