unit Unit2;

interface

uses

  Vcl.Controls, Vcl.Forms, System.Classes, Vcl.StdCtrls,
  Ventil_ConsultationSystemes, Vcl.ExtCtrls;

type
  TMainForm = class(TForm)
    btnFilesAndClear: TButton;
    PanelAT: TPanel;
    Panel2: TPanel;
    btnFilesAndKeep: TButton;
    btnFoldersAndClear: TButton;
    btnFoldersAndKeep: TButton;
    btnExportExcel: TButton;
    procedure btnFilesAndClearClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnFilesAndKeepClick(Sender: TObject);
    procedure btnFoldersAndClearClick(Sender: TObject);
    procedure btnFoldersAndKeepClick(Sender: TObject);
    procedure btnExportExcelClick(Sender: TObject);
  private
    FDlg_ConsultationSystemes : TDlg_ConsultationSystemes;
    procedure LoadXMLS(_OpenFolder : Boolean; _ClearExistants : Boolean);
    procedure Initialisation;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}


uses
  System.IOUtils,
  Types,
  Dialogs,
  BbsgTec,
  Vcl.FileCtrl,
  System.SysUtils,
  Ventil_EdibatecProduits,
  Ventil_Declarations,
  Ventil_SystemeVMC;

procedure TMainForm.Initialisation;
begin
  FormatSettings.DecimalSeparator := '.';
//  ConnexionEdibatec;
//  Connection_Logiciel;
  FabricantsVMC := TEdibatec_ListeFabricants.Create;

//  BaseEdibatec := TEdibatec_ListeClasses.Create;

  SystemesVMC := TListeSystemes.Create;

  FDlg_ConsultationSystemes := TDlg_ConsultationSystemes.Create(Nil);
  FDlg_ConsultationSystemes.PageControl_Systemes.Parent := PanelAT;

end;

procedure TMainForm.btnExportExcelClick(Sender: TObject);
var
  tmpSaveDialog : TSaveDialog;
begin

    if SystemesVMC.Count = 0 then
      Exit;

  tmpSaveDialog := TSaveDialog.Create(Nil);
  tmpSaveDialog.Filter:='Fichier XLS|*.XLS';
  tmpSaveDialog.DefaultExt := 'XLS';
  tmpSaveDialog.FileName := FDlg_ConsultationSystemes.PageControl_Systemes.ActivePage.Caption + '.xls';
  tmpSaveDialog.FileName := MakeValidFilename(tmpSaveDialog.FileName, true);
    If tmpSaveDialog.Execute Then
      FDlg_ConsultationSystemes.AdvGridExcelIO1.XLSExport(tmpSaveDialog.FileName);
  FreeAndNil(tmpSaveDialog);
end;

procedure TMainForm.btnFilesAndClearClick(Sender: TObject);
begin
  LoadXMLS(False, True);
End;

procedure TMainForm.btnFilesAndKeepClick(Sender: TObject);
begin
  LoadXMLS(False, False);
end;

procedure TMainForm.btnFoldersAndClearClick(Sender: TObject);
begin
  LoadXMLS(True, True);
end;

procedure TMainForm.btnFoldersAndKeepClick(Sender: TObject);
begin
  LoadXMLS(True, False);
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  Initialisation;
end;


procedure TMainForm.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FDlg_ConsultationSystemes);
end;

procedure TMainForm.LoadXMLS(_OpenFolder : Boolean; _ClearExistants : Boolean);
  procedure DeleteElement(var A: TStringDynArray; const Index: Cardinal);
  var
    ALength: Cardinal;
    i: Cardinal;
  begin
    ALength := Length(A);
    Assert(ALength > 0);
    Assert(Index < ALength);
    for i := Index + 1 to ALength - 1 do
      A[i - 1] := A[i];
    SetLength(A, ALength - 1);
  end;
Var
  tmpOpenDialog : TOpenDialog;
  tmpStringList : TStringList;
  cpt : Integer;
  Dir : string;
  RetourFiles : TStringDynArray;
begin

    if _OpenFolder then
      begin
      Dir := ExtractFilePath(Application.Exename);
        if SelectDirectory(Dir, [], 0) then
          begin
          tmpStringList := TStringList.Create;
          RetourFiles := TDirectory.GetFiles(Dir, '*.xml', TSearchOption.soAllDirectories);

            for cpt := High(RetourFiles) downto Low(RetourFiles) do
              if (UpperCase(TPath.GetFileNameWithoutExtension(RetourFiles[cpt])) = 'CORRESPONDANCES') or
                 (UpperCase(TPath.GetFileNameWithoutExtension(RetourFiles[cpt])) = 'CHIFFRAGE') then
                DeleteElement(RetourFiles, cpt);

            for cpt := Low(RetourFiles) to High(RetourFiles) do
              tmpStringList.Add(RetourFiles[cpt]);
          end;
      end
    else
      begin
      tmpOpenDialog := TOpenDialog.Create(Nil);
      tmpOpenDialog.Filter:='Fichier XML|*.XML';
      tmpOpenDialog.Options := tmpOpenDialog.Options + [ofAllowMultiSelect];
        If tmpOpenDialog.Execute Then
          begin
          tmpStringList := TStringList.Create;
            for cpt := 0 to tmpOpenDialog.Files.Count - 1 do
              tmpStringList.Add(tmpOpenDialog.Files[cpt]);

          end;
      FreeAndNil(tmpOpenDialog);
      end;


    if tmpStringList <> Nil then
      begin
        if _ClearExistants then
          SystemesVMC.Clear;

      SystemesVMC.Load(tmpStringList);
      FreeAndNil(tmpStringList);


      FDlg_ConsultationSystemes.Initialisation(True);
      end;
End;


end.
