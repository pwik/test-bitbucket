object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'AT_Viewer'
  ClientHeight = 549
  ClientWidth = 1061
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PanelAT: TPanel
    Left = 0
    Top = 41
    Width = 1061
    Height = 508
    Align = alClient
    Caption = 'PanelAT'
    ShowCaption = False
    TabOrder = 0
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 1061
    Height = 41
    Align = alTop
    Caption = 'Panel2'
    ShowCaption = False
    TabOrder = 1
    object btnFilesAndClear: TButton
      Left = 32
      Top = 0
      Width = 220
      Height = 35
      Caption = 'Charger fichiers et supprimer AT charg'#233's'
      TabOrder = 0
      OnClick = btnFilesAndClearClick
    end
    object btnFilesAndKeep: TButton
      Left = 280
      Top = 0
      Width = 220
      Height = 35
      Caption = 'Charger fichiers et garder AT existants'
      TabOrder = 1
      OnClick = btnFilesAndKeepClick
    end
    object btnFoldersAndClear: TButton
      Left = 528
      Top = 0
      Width = 220
      Height = 35
      Caption = 'Charger r'#233'pertoire et supprimer AT existants'
      TabOrder = 2
      OnClick = btnFoldersAndClearClick
    end
    object btnFoldersAndKeep: TButton
      Left = 776
      Top = 0
      Width = 220
      Height = 35
      Caption = 'Charger r'#233'pertoire et garder AT existants'
      TabOrder = 3
      OnClick = btnFoldersAndKeepClick
    end
    object btnExportExcel: TButton
      Left = 1024
      Top = 0
      Width = 220
      Height = 35
      Caption = 'Exporter l'#39'AT en cours au format Excel'
      TabOrder = 4
      OnClick = btnExportExcelClick
    end
  end
end
