Unit Ventil_TourelleTete;
  
{$Include 'Ventil_Directives.pas'}

Interface
Uses
  Classes, SysUtils, advGrid, BaseGrid, AdvObj, Dialogs, Math,
  BBScad_Interface_Aeraulique,
  XMLDoc,
  XMLIntf,
  AccesBDD,
  Ventil_Const,
  Ventil_Logement,
  Ventil_ChiffrageTypes,
  Ventil_SelectionCaisson,
  Ventil_Caisson,
  Ventil_Types, Ventil_Troncon, Ventil_TronconDessin, Ventil_TeSouche, Ventil_EdibatecProduits, Ventil_Utils;

Type



{ ************************************************************************************************************************************************** }
  TVentil_TourelleTete = Class(TVentil_Caisson, IAero_Fredo_TourelleTete)
    Constructor Create; Override;
    Destructor Destroy; Override;
  Private
  Public
    class Function FamilleObjet : String; Override;

    Procedure UpdateDimensionsCaisson; Override;
    Procedure Load; Override;
    Function  AfficheQuestion(_NoQuestion: Integer): Boolean; Override;
    Function GetTypeSortieTourelleXMLVIM : Integer;
  Protected
    Procedure CalculerQuantitatifInternalCaisson; Override;
    Function  ImageAssociee: String; Override;
    function GetLibIDTableDeDonnees : String; Override;
    function GetTableDeDonnees : TTableDeDonneesBBS; Override;
    Procedure Save; Override;
    Function  InternalLienCADCaisson : IAero_Caisson_Base; Override;
    class Function NomParDefaut : String; Override;
  End;



{ ************************************************************************************************************************************************** }



Implementation
Uses
  BbsgTec,
  Ventil_TourelleBifurcation,
  DateUtils,
  Ventil_Firebird,
  Grids;

{ ******************************************************************* TVentil_TourelleTete ***************************************************************** }
Function TVentil_TourelleTete.GetTypeSortieTourelleXMLVIM : Integer;
begin
  if HasChildrenClass(tventil_TourelleBifurcation) then
    begin
      if GetFirstChildrenClass(tventil_TourelleBifurcation).HasParentClass(TVentil_TronconDessin_Conduit) then
        Result := c_TourelleTete_TypeSortieXMLVIM_PlenumIndirect
      else
        Result := c_TourelleTete_TypeSortieXMLVIM_PlenumDirect;
    end
  else
    Result := c_TourelleTete_TypeSortieXMLVIM_SansPlenum;
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleTete.CalculerQuantitatifInternalCaisson;
begin
    {$IFDEF  VIM_SELECTIONCAISSON}
    Inherited;
    {$ENDIF}
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleTete.UpdateDimensionsCaisson;
begin

end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleTete.Load;
begin
  Inherited;
end;
{ ************************************************************************************************************************************************** }
Function  TVentil_TourelleTete.AfficheQuestion(_NoQuestion: Integer): Boolean;
begin
  Result := Inherited;
    if _NoQuestion in [c_QCaisson_Position, c_QCaisson_TypeCaisson, c_QCaisson_SaisieDimensions, c_QCaisson_DiametreExtract, c_QCaisson_DiametreRefoul] then
      Result := False;
end;
{ ************************************************************************************************************************************************** }
function TVentil_TourelleTete.GetLibIDTableDeDonnees : String;
begin
  Result := 'ID_TOURTETE';
end;
{ ************************************************************************************************************************************************** }
function TVentil_TourelleTete.GetTableDeDonnees : TTableDeDonneesBBS;
begin
  Result := Table_Etude_TourelleTete;
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleTete.Save;
begin
  Inherited;
end;
{ ************************************************************************************************************************************************** }
Function  TVentil_TourelleTete.InternalLienCADCaisson : IAero_Caisson_Base;
begin
Result := (LienCad.IAero_Vilo_Base_Pere As IAero_EXT_Tourelle);
end;
{ ************************************************************************************************************************************************** }
Constructor TVentil_TourelleTete.Create;
Begin
  Inherited;
  SaisieDimensions := c_Non;
End;
{ ************************************************************************************************************************************************** }
class Function TVentil_TourelleTete.NomParDefaut : String;
Begin
Result := 'Tourelle';
End;
{ ************************************************************************************************************************************************** }
class Function TVentil_TourelleTete.FamilleObjet : String;
Begin
  Result := 'Tourelle';
End;
{ ************************************************************************************************************************************************** }
Function TVentil_TourelleTete.ImageAssociee: String;
Begin
    Result := 'TOURELLETETE.jpg';
End;
{ ************************************************************************************************************************************************** }
Destructor TVentil_TourelleTete.Destroy;
Begin
  Inherited;
End;
{ ******************************************************************* \TVentil_TourelleTete **************************************************************** }




End.
