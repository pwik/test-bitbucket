object Dlg_SelectionCaisson: TDlg_SelectionCaisson
  Left = 373
  Top = 151
  ActiveControl = CheckBox_Universel
  BorderStyle = bsDialog
  Caption = 'Options de chiffrage'
  ClientHeight = 734
  ClientWidth = 1154
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object PanelBas: TPanel
    Left = 0
    Top = 680
    Width = 1154
    Height = 54
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 1154
      Height = 3
      Align = alTop
      Shape = bsTopLine
      Style = bsRaised
    end
    object Btn_Cancel: TBitBtn
      Left = 8
      Top = 18
      Width = 113
      Height = 33
      Cancel = True
      Caption = '&Annuler'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000C40E0000C40E00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF00009A00009AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00009A0000
        9AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF6B6B6B6B6B6BFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FF6B6B6B6B6B6BFF00FFFF00FFFF00FFFF00FFFF00FF
        00009A174AFD103BF400009AFF00FFFF00FFFF00FFFF00FF00009A002CF80030
        FC00009AFF00FFFF00FFFF00FFFF00FF6B6B6BA8A8A8A0A0A06B6B6BFF00FFFF
        00FFFF00FFFF00FF6B6B6B9A9A9A9C9C9C6B6B6BFF00FFFF00FFFF00FFFF00FF
        00009A1A47F81A4CFF123BF100009AFF00FFFF00FF00009A012DF60132FF002A
        F300009AFF00FFFF00FFFF00FFFF00FF6B6B6BA7A7A7AAAAAA9F9F9F6B6B6BFF
        00FFFF00FF6B6B6B9999999E9E9E9797976B6B6BFF00FFFF00FFFF00FFFF00FF
        FF00FF00009A1C47F61B4DFF143EF400009A00009A002DF80134FF032BF20000
        9AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF6B6B6BA7A7A7ABABABA2A2A26B
        6B6B6B6B6B9A9A9A9E9E9E9898986B6B6BFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF00009A1D48F61D50FF103DFB0431FE0132FF002CF600009AFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF6B6B6BA7A7A7ACACACA3
        A3A39F9F9F9E9E9E9999996B6B6BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FF00009A1A48F91342FF0C3CFF0733F600009AFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF6B6B6BA7A7A7A7
        A7A7A3A3A39C9C9C6B6B6BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FF00009A214EFC1D4BFF1847FF1743F600009AFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF6B6B6BACACACAC
        ACACA9A9A9A4A4A46B6B6BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF00009A2E5BF92C5FFF224DF8204BF82355FF1B46F600009AFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF6B6B6BB1B1B1B3B3B3AB
        ABABAAAAAAAFAFAFA6A6A66B6B6BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF00009A3664FA386BFF2D59F400009A00009A224CF42558FF1D49F60000
        9AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF6B6B6BB6B6B6B9B9B9AEAEAE6B
        6B6B6B6B6BA9A9A9B0B0B0A7A7A76B6B6BFF00FFFF00FFFF00FFFF00FFFF00FF
        00009A4071FA4274FF325DF100009AFF00FFFF00FF00009A224DF1275AFF204C
        F800009AFF00FFFF00FFFF00FFFF00FF6B6B6BBBBBBBBEBEBEAFAFAF6B6B6BFF
        00FFFF00FF6B6B6BA7A7A7B1B1B1AAAAAA6B6B6BFF00FFFF00FFFF00FFFF00FF
        00009A497AFC3B66F300009AFF00FFFF00FFFF00FFFF00FF00009A2550F42655
        FA00009AFF00FFFF00FFFF00FFFF00FF6B6B6BC0C0C0B5B5B56B6B6BFF00FFFF
        00FFFF00FFFF00FF6B6B6BAAAAAAAEAEAE6B6B6BFF00FFFF00FFFF00FFFF00FF
        FF00FF00009A00009AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00009A0000
        9AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF6B6B6B6B6B6BFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FF6B6B6B6B6B6BFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      ModalResult = 2
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 0
    end
    object Btn_Ok: TBitBtn
      Left = 345
      Top = 18
      Width = 113
      Height = 33
      Caption = '&Ok'
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000C40E0000C40E00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FF006600006600FF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF656565656565FF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF0066001EB2311FB133006600FF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF6565659A9A9A9A9A9A65
        6565FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF00660031C24F22B7381AB02D21B437006600FF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF656565ABABAB9E9E9E9797979C
        9C9C656565FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        00660047D36D3BCB5E25A83B0066001BA92E1DB132006600FF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FF656565BFBFBFB5B5B598989865656594
        9494999999656565FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF006600
        4FD67953DE7F31B54D006600FF00FF006600179D271EAE31006600FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF656565C4C4C4CACACAA5A5A5656565FF00FF65
        65658C8C8C989898656565FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        00660041C563006600FF00FFFF00FFFF00FFFF00FF00660019AA2B006600FF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FF656565B5B5B5656565FF00FFFF00FFFF
        00FFFF00FF656565939393656565FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF006600FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF006600149D210066
        00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF656565FF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FF6565658A8A8A656565FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0066
        00006600FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FF656565656565FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FF006600006600FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FF656565656565FF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      ModalResult = 1
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 1
    end
    object Btn_Precedent: TBitBtn
      Left = 119
      Top = 18
      Width = 113
      Height = 33
      Cancel = True
      Caption = '&Pr'#233'c'#233'dent'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000330B0000330B00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFC86000983000FF00FFFF00FFFF00FFFF00
        FFC86000983000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC86000F8
        E0B0983000FF00FFFF00FFFF00FFC86000F8E0B0983000FF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFC86000F8E0B0C86000983000FF00FFFF00FFC86000F8E0
        B0C86000983000FF00FFFF00FFFF00FFFF00FFFF00FFC86000F8E0B0F89800C8
        6000983000FF00FFC86000F8E0B0F89800C86000983000FF00FFFF00FFFF00FF
        FF00FFC86000F8E0B0F89E13F89B09C86000983000C86000F8E0B0F89E13F89B
        09C86000983000FF00FFFF00FFFF00FFC86000F8E0B0F8AA39F8A426F8A11CC8
        6000983000F8E0B0F8AA39F8A426F8A11CC86000983000FF00FFFF00FFC86000
        F8E0B0F8B762F8B04CF8AA39F8A730C86000983000F8B762F8B04CF8AA39F8A7
        30C86000983000FF00FFC86000F8E0B0F8C58EF8BE78F8B762F8B04CF8AD43C8
        6000983000F8BE78F8B762F8B04CF8AD43C86000983000FF00FFC86000F8E0B0
        F8C898F8C58EF8BE78F8B762F8B357C86000983000F8C58EF8BE78F8B762F8B3
        57C86000983000FF00FFFF00FFC86000F8E0B0F8C898F8C58EF8BE78F8BA6DC8
        6000983000F8C898F8C58EF8BE78F8BA6DC86000983000FF00FFFF00FFFF00FF
        C86000F8E0B0F8C898F8C58EF8C183C86000983000F8E0B0F8C898F8C58EF8C1
        83C86000983000FF00FFFF00FFFF00FFFF00FFC86000F8E0B0F8C898F8C898C8
        6000983000C86000F8E0B0F8C898F8C898C86000983000FF00FFFF00FFFF00FF
        FF00FFFF00FFC86000F8E0B0F8C898C86000983000FF00FFC86000F8E0B0F8C8
        98C86000983000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC86000F8E0B0C8
        6000983000FF00FFFF00FFC86000F8E0B0C86000983000FF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFC86000F8E0B0983000FF00FFFF00FFFF00FFC860
        00F8E0B0983000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC8
        6000983000FF00FFFF00FFFF00FFFF00FFC86000983000FF00FF}
      ParentFont = False
      TabOrder = 2
      OnClick = Btn_PrecedentClick
    end
    object Btn_Suivant: TBitBtn
      Left = 231
      Top = 18
      Width = 113
      Height = 33
      Cancel = True
      Caption = '&Suivant'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000230B0000230B00000000000000000000FF00FFC86000
        983000FF00FFFF00FFFF00FFFF00FFC86000983000FF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFC86000C86000983000FF00FFFF00FFFF00FFC8
        6000C86000983000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC86000
        F8E0B0C86000983000FF00FFFF00FFC86000F8E0B0C86000983000FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFC86000F8E0B0F8AC3DC86000983000FF00FFC8
        6000F8E0B0F8AC3DC86000983000FF00FFFF00FFFF00FFFF00FFFF00FFC86000
        F8E0B0F8B45AF8AC3DC86000983000C86000F8E0B0F8B45AF8AC3DC860009830
        00FF00FFFF00FFFF00FFFF00FFC86000F8E0B0F8B969F8B45BF8AC3DC86000C8
        6000F8E0B0F8B969F8B45BF8AC3DC86000983000FF00FFFF00FFFF00FFC86000
        F8E0B0F8BE7AF8BA6CF8B55FF8AC3DC86000F8E0B0F8BE7AF8BA6CF8B55FF8AC
        3DC86000983000FF00FFFF00FFC86000F8E0B0F8C48CF8BF7DF8BB70F8B660C8
        6000F8E0B0F8C48CF8BF7DF8BB70F8B660F8AC3DC86000983000FF00FFC86000
        F8E0B0F8C795F8C48BF8BF7DF8BB6DC86000F8E0B0F8C795F8C48BF8BF7DF8BB
        6DF8B660C86000983000FF00FFC86000F8E0B0F8C797F8C796F8C58DF8BF7DC8
        6000F8E0B0F8C797F8C796F8C58DF8BF7DC86000983000FF00FFFF00FFC86000
        F8E0B0F8C898F8C898F8C796C86000C86000F8E0B0F8C898F8C898F8C796C860
        00983000FF00FFFF00FFFF00FFC86000F8E0B0F8C898F8C898C86000983000C8
        6000F8E0B0F8C898F8C898C86000983000FF00FFFF00FFFF00FFFF00FFC86000
        F8E0B0F8C898C86000983000FF00FFC86000F8E0B0F8C898C86000983000FF00
        FFFF00FFFF00FFFF00FFFF00FFC86000F8E0B0C86000983000FF00FFFF00FFC8
        6000F8E0B0C86000983000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC86000
        C86000983000FF00FFFF00FFFF00FFC86000C86000983000FF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFC86000983000FF00FFFF00FFFF00FFFF00FFC8
        6000983000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      ParentFont = False
      TabOrder = 3
      OnClick = Btn_SuivantClick
    end
    object Btn_RecalculAccesoires: TBitBtn
      Left = 457
      Top = 18
      Width = 113
      Height = 33
      Caption = '&Recalcul'
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000EE0E0000EE0E00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FF31C66B08AD1808841008841008AD1831C66BFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF08AD1808841008841008
        841008841008841008841008AD18FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF08AD1808841008AD1831C66BFF00FFFF00FF31C66B08AD1808841008AD
        18FF00FFFF00FFFF00FFFF00FFFF00FF31C66B08841008AD18FF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FF08AD1808841031C66BFF00FFFF00FFFF00FFFF00FF
        08AD18088410FF00FFFF00FFD68C42943100D68C42FF00FFFF00FF31C66B0884
        1008AD18FF00FFFF00FFFF00FFFF00FF088410088410FF00FFFF00FFCE6B00B4
        5D00943100FF00FFFF00FFFF00FF088410088410FF00FFFF00FFFF00FFFF00FF
        088410088410FF00FFFF00FFD68C42CE6B00D68C42FF00FFFF00FFFF00FF0884
        10088410FF00FFFF00FFFF00FFFF00FF08AD1808841031C66BFF00FFFF00FF08
        8410FF00FFFF00FFFF00FFFF00FF08841008AD18FF00FFFF00FFFF00FFFF00FF
        31C66B08841008AD18FF00FFFF00FF088410088410FF00FFFF00FFFF00FF0884
        10FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF08AD1808841008AD1831C66B08
        841008AD18088410FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF08AD1808841008841008AD1808AD1808AD18088410FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF31C66B08AD1808
        841008AD18088410FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FF088410088410FF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF08
        8410FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      ParentFont = False
      TabOrder = 4
      OnClick = Btn_RecalculAccesoiresClick
    end
  end
  object Panel_Preselection: TPanel
    Tag = 1
    Left = 0
    Top = 0
    Width = 577
    Height = 332
    BevelOuter = bvNone
    TabOrder = 1
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 577
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Crit'#232'res de s'#233'lection du ventilateur'
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object GroupBox1: TGroupBox
      Left = 36
      Top = 87
      Width = 237
      Height = 58
      Caption = 'Type de ventilateur'
      TabOrder = 1
      object CheckBox_Caisson: TCheckBox
        Left = 8
        Top = 16
        Width = 97
        Height = 17
        Caption = 'Caissons'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object CheckBox_Tourelle: TCheckBox
        Left = 8
        Top = 32
        Width = 169
        Height = 17
        Caption = 'Tourelles standard'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
    end
    object GroupBox2: TGroupBox
      Left = 36
      Top = 153
      Width = 237
      Height = 54
      Caption = 'Entra'#238'nement'
      TabOrder = 2
      object CheckBox_Direct: TCheckBox
        Left = 8
        Top = 16
        Width = 169
        Height = 17
        Caption = 'Accouplement direct'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object CheckBox_Poulie: TCheckBox
        Left = 8
        Top = 32
        Width = 169
        Height = 17
        Caption = 'Poulie/courroie'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
    end
    object GroupBox3: TGroupBox
      Left = 36
      Top = 216
      Width = 237
      Height = 57
      Caption = 'Vitesse'
      TabOrder = 3
      object CheckBox_UneVitesse: TCheckBox
        Left = 8
        Top = 16
        Width = 169
        Height = 17
        Caption = 'Une vitesse'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object CheckBoxDeuxVitesses: TCheckBox
        Left = 8
        Top = 32
        Width = 169
        Height = 17
        Caption = 'Multi vitesses'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
    end
    object GroupBox4: TGroupBox
      Left = 308
      Top = 87
      Width = 237
      Height = 58
      Caption = 'Agr'#233'ment'
      TabOrder = 4
      object CheckBox_NonAgree: TCheckBox
        Left = 8
        Top = 16
        Width = 97
        Height = 17
        Caption = 'Non agr'#233#233
        Checked = True
        State = cbChecked
        TabOrder = 0
        OnClick = CheckBox_NonAgreeClick
      end
      object CheckBox_40012: TCheckBox
        Left = 8
        Top = 32
        Width = 169
        Height = 17
        Caption = 'Agr'#233#233' 400'#176' 1/2h et plus'
        Checked = True
        State = cbChecked
        TabOrder = 1
        OnClick = CheckBox_NonAgreeClick
      end
    end
    object GroupBox5: TGroupBox
      Left = 308
      Top = 154
      Width = 237
      Height = 54
      Caption = 'Alimentation'
      TabOrder = 5
      object CheckBox_Monophase: TCheckBox
        Left = 8
        Top = 16
        Width = 169
        Height = 17
        Caption = 'Monophas'#233
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object CheckBox_Triphase: TCheckBox
        Left = 8
        Top = 32
        Width = 169
        Height = 17
        Caption = 'Triphas'#233
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
    end
    object CheckBox_DTU: TCheckBox
      Left = 311
      Top = 304
      Width = 178
      Height = 17
      Caption = 'Respecter la conformit'#233' au DTU'
      Checked = True
      State = cbChecked
      TabOrder = 6
    end
    object RadioBezier: TRadioGroup
      Left = 35
      Top = 285
      Width = 238
      Height = 41
      Caption = 'Courbes des caissons'
      Columns = 2
      ItemIndex = 1
      Items.Strings = (
        '4 points'
        '11 points')
      TabOrder = 7
      Visible = False
    end
    object GroupBoxVentilEcoEtDepressostat: TGroupBox
      Left = 308
      Top = 218
      Width = 237
      Height = 71
      Caption = 'Ventil. Eco et Depr + Inter mont'#233's'
      TabOrder = 8
      object CheckBoxEco: TCheckBox
        Left = 8
        Top = 16
        Width = 169
        Height = 17
        Caption = 'Ventil. Eco'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object CheckBoxNEcoADepr: TCheckBox
        Left = 8
        Top = 32
        Width = 225
        Height = 17
        Caption = 'Ventil. non Eco avec Depr. + inter. mont'#233's'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
      object CheckBoxNEcoSDepr: TCheckBox
        Left = 8
        Top = 48
        Width = 225
        Height = 17
        Caption = 'Ventil. non Eco sans Depr. + inter. mont'#233's'
        Checked = True
        State = cbChecked
        TabOrder = 2
      end
    end
  end
  object Panel_Selection: TPanel
    Tag = 2
    Left = 576
    Top = 0
    Width = 577
    Height = 332
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 577
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      Caption = 'S'#233'lection du ventilateur'
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object GroupBox6: TGroupBox
      Left = 0
      Top = 28
      Width = 577
      Height = 98
      Caption = 'R'#233'seau'
      TabOrder = 1
      object Label_Lib_1: TLabel
        Left = 56
        Top = 13
        Width = 58
        Height = 13
        Caption = 'Label_Lib_1'
      end
      object Label_Lib_2: TLabel
        Left = 56
        Top = 29
        Width = 58
        Height = 13
        Caption = 'Label_Lib_2'
      end
      object Label_Val_1: TLabel
        Left = 154
        Top = 13
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Label_Val_1'
      end
      object Label_Val_2: TLabel
        Left = 154
        Top = 29
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Label_Val_2'
      end
      object Label_Lib_6: TLabel
        Left = 336
        Top = 13
        Width = 58
        Height = 13
        Caption = 'Label_Lib_6'
      end
      object Label_Val_6: TLabel
        Left = 434
        Top = 13
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Label_Val_6'
      end
      object Label_Val_7: TLabel
        Left = 434
        Top = 29
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Label_Val_7'
      end
      object Label_Lib_7: TLabel
        Left = 336
        Top = 29
        Width = 58
        Height = 13
        Caption = 'Label_Lib_7'
      end
      object Label_Lib_3: TLabel
        Left = 56
        Top = 45
        Width = 58
        Height = 13
        Caption = 'Label_Lib_3'
      end
      object Label_Val_3: TLabel
        Left = 154
        Top = 45
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Label_Val_3'
      end
      object Label_Lib_8: TLabel
        Left = 336
        Top = 45
        Width = 58
        Height = 13
        Caption = 'Label_Lib_8'
      end
      object Label_Val_8: TLabel
        Left = 434
        Top = 45
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Label_Val_8'
      end
      object Label_Lib_9: TLabel
        Left = 336
        Top = 61
        Width = 58
        Height = 13
        Caption = 'Label_Lib_9'
      end
      object Label_Val_9: TLabel
        Left = 434
        Top = 61
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Label_Val_9'
      end
      object Image_DPVent: TImage
        Left = 528
        Top = 40
        Width = 13
        Height = 13
        Cursor = crHelp
        Center = True
        Picture.Data = {
          07544269746D61700A040000424D0A040000000000000A030000280000001000
          000010000000010008000000000000010000120B0000120B0000B5000000B500
          000000000000FFFFFF00CDCCCF00FF00FF00C7C4C700C2C0C200D1CCCD00D9D4
          D400D5D1D100FDFBFB00FEFDFD00FDFCFC00C7C0BF00FBF6F500C4B7B400C8BC
          B900FAF6F500F8F4F300D8C5BE00C8B7B100FAF2EF00F9F1EE00CEB0A400E8D2
          C900EFD9D000EED8CF00F2E0D900ECE0DB00E8CCBF00E8CDC100EACFC300E6CD
          C200E4CBC000DCC4B900EBD3C800E9D1C600E6CEC300ECD4C900E3CDC300EED7
          CD00EED8CE00D6C2B900DFCBC200DBC7BE00F2E0D800EFDED600F7EEEA00F6ED
          E900C95B2200C95C2300CD602700CC602800CC653000CD673200CD693500D08F
          6E00D7967400CF967900D29B7F00D7A08500E6B39900F2D4C500E5C9BB00F0D5
          C700DCC3B700E9CFC200E5D0C600F7E7DF00F5E7E000F4E8E200FAF0EB00F6EC
          E700F6F4F300C7551900CC612800CB612800CA612900CB622A00CD642C00CB64
          2D00CD652E00CC652E00CC662F00CE683000CC673000CD673100CC673100CD69
          3300CD6A3300CD6A3400CD6A3500CF6F3900D0703B00CF6F3C00D0724000D275
          4100D0744300D1754400D3784800D48B6400D58C6500D68E6800D8916B00E0A2
          8100DDA28300E4AF9200E0AB8F00E3AF9300E6B49700E6B39800E9BBA100E9BC
          A400ECC6B000EDC7B300F6E1D500F2E2D900F4E7E000D1723900D2753D00D377
          4100D4794300D57D4700D7814A00D6804A00E5AE8B00E9BA9E00ECC5AE00F7E6
          DC00FDF8F500D4793C00D6804600D7834C00DB8B5500DA8B5500DD946500E19E
          6F00E4AC8700EFCAB000FEFBF900D9854900DB8D5300E09B6700E8B28B00E8B6
          9000EABD9C00EEC9AD00F7E2D200F8E7DA00FEFAF700FDF9F600DF965C00E8B6
          8E00EFC8A800F5DCC700E4A46A00EFCAA900F0CEB000EEC39A00F1D0AE00F2D2
          B300FAEEE200FDF8F300FDF9F500F4F1EE00FFFEFD00FEFDFC00F0CEA800F1D0
          AD00F3D4B400F9EADA00FBF2E700FFFFFE00FBFFFF00F4FAFD00F6FBFF00F1F6
          FA00EAEBEC00E8ECF100E4E7EC00DFE1E500CBCBCE00030303030324262A2B21
          3E03030303030303031D1BB0B20706B40C133E03030303031EA3AD1F38646337
          16050E400303031C11AC684B50723D4A4E39040F3E030345016B3052366F3C56
          54323A0212031E0A7F5C35345E6D675157555929083E18017D755F586294465D
          4C5A333BB3201A0188827B785B71013F604F4D65B1422C01978C848379777001
          43613166AF1728019C968D87857A767E0169496AAE232809AA9A99AB8F8B817C
          016E537348230374019F9D01958991808A866C012D0303251401A7A6A0A2A193
          8E90012F2D030303251501A9A89E9B9892012E410303030303254710A5A4A50B
          0D444403030303030303032222192725250303030303}
        Transparent = True
        OnClick = Image_DPVentClick
      end
      object Label_Lib_4: TLabel
        Left = 56
        Top = 61
        Width = 58
        Height = 13
        Caption = 'Label_Lib_4'
      end
      object Label_Lib_5: TLabel
        Left = 56
        Top = 77
        Width = 58
        Height = 13
        Caption = 'Label_Lib_5'
      end
      object Label_Lib_10: TLabel
        Left = 336
        Top = 77
        Width = 64
        Height = 13
        Caption = 'Label_Lib_10'
      end
      object Label_Val_10: TLabel
        Left = 428
        Top = 77
        Width = 64
        Height = 13
        Alignment = taRightJustify
        Caption = 'Label_Val_10'
      end
      object Label_Val_4: TLabel
        Left = 154
        Top = 61
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Label_Val_4'
      end
      object Label_Val_5: TLabel
        Left = 154
        Top = 77
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Label_Val_5'
      end
      object Label_Sep_1: TLabel
        Left = 132
        Top = 13
        Width = 3
        Height = 13
        Caption = ':'
      end
      object Label_Sep_2: TLabel
        Left = 132
        Top = 29
        Width = 3
        Height = 13
        Caption = ':'
      end
      object Label_Sep_3: TLabel
        Left = 132
        Top = 45
        Width = 3
        Height = 13
        Caption = ':'
      end
      object Label_Sep_4: TLabel
        Left = 132
        Top = 61
        Width = 3
        Height = 13
        Caption = ':'
      end
      object Label_Sep_5: TLabel
        Left = 132
        Top = 77
        Width = 3
        Height = 13
        Caption = ':'
      end
      object Label_Sep_6: TLabel
        Left = 415
        Top = 13
        Width = 3
        Height = 13
        Caption = ':'
      end
      object Label_Sep_7: TLabel
        Left = 415
        Top = 29
        Width = 3
        Height = 13
        Caption = ':'
      end
      object Label_Sep_8: TLabel
        Left = 415
        Top = 45
        Width = 3
        Height = 13
        Caption = ':'
      end
      object Label_Sep_9: TLabel
        Left = 415
        Top = 61
        Width = 3
        Height = 13
        Caption = ':'
      end
      object Label_Sep_10: TLabel
        Left = 415
        Top = 77
        Width = 3
        Height = 13
        Caption = ':'
      end
    end
    object Grid_Selection: TAdvStringGrid
      Left = 0
      Top = 132
      Width = 577
      Height = 200
      Cursor = crDefault
      Align = alBottom
      Ctl3D = False
      DefaultRowHeight = 21
      DrawingStyle = gdsClassic
      FixedCols = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goColSizing, goRowSelect]
      ParentCtl3D = False
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 2
      OnSelectCell = Grid_SelectionSelectCell
      HoverRowCells = [hcNormal, hcSelected]
      OnRowChanging = Grid_SelectionRowChanging
      OnClickSort = Grid_SelectionClickSort
      OnCanEditCell = Grid_SelectionCanEditCell
      ActiveCellFont.Charset = DEFAULT_CHARSET
      ActiveCellFont.Color = clWindowText
      ActiveCellFont.Height = -11
      ActiveCellFont.Name = 'Segoe UI'
      ActiveCellFont.Style = [fsBold]
      Bands.Active = True
      ColumnHeaders.Strings = (
        'D'#233'signation'
        'Prix ('#8364')'
        'DP (Pa)'
        'Vitesse'
        'RT2005')
      ColumnSize.Stretch = True
      ColumnSize.StretchColumn = 0
      ControlLook.FixedGradientHoverFrom = clGray
      ControlLook.FixedGradientHoverTo = clWhite
      ControlLook.FixedGradientDownFrom = clGray
      ControlLook.FixedGradientDownTo = clSilver
      ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
      ControlLook.DropDownHeader.Font.Color = clWindowText
      ControlLook.DropDownHeader.Font.Height = -11
      ControlLook.DropDownHeader.Font.Name = 'Tahoma'
      ControlLook.DropDownHeader.Font.Style = []
      ControlLook.DropDownHeader.Visible = True
      ControlLook.DropDownHeader.Buttons = <>
      ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
      ControlLook.DropDownFooter.Font.Color = clWindowText
      ControlLook.DropDownFooter.Font.Height = -11
      ControlLook.DropDownFooter.Font.Name = 'MS Sans Serif'
      ControlLook.DropDownFooter.Font.Style = []
      ControlLook.DropDownFooter.Visible = True
      ControlLook.DropDownFooter.Buttons = <>
      Filter = <>
      FilterDropDown.Font.Charset = DEFAULT_CHARSET
      FilterDropDown.Font.Color = clWindowText
      FilterDropDown.Font.Height = -11
      FilterDropDown.Font.Name = 'MS Sans Serif'
      FilterDropDown.Font.Style = []
      FilterDropDownClear = '(All)'
      FilterEdit.TypeNames.Strings = (
        'Starts with'
        'Ends with'
        'Contains'
        'Not contains'
        'Equal'
        'Not equal'
        'Clear')
      FixedColWidth = 303
      FixedFont.Charset = DEFAULT_CHARSET
      FixedFont.Color = clWindowText
      FixedFont.Height = -11
      FixedFont.Name = 'Segoe UI'
      FixedFont.Style = [fsBold]
      Flat = True
      FloatFormat = '%.2f'
      PrintSettings.DateFormat = 'dd/mm/yyyy'
      PrintSettings.Font.Charset = DEFAULT_CHARSET
      PrintSettings.Font.Color = clWindowText
      PrintSettings.Font.Height = -11
      PrintSettings.Font.Name = 'Segoe UI'
      PrintSettings.Font.Style = []
      PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
      PrintSettings.FixedFont.Color = clWindowText
      PrintSettings.FixedFont.Height = -11
      PrintSettings.FixedFont.Name = 'Segoe UI'
      PrintSettings.FixedFont.Style = []
      PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
      PrintSettings.HeaderFont.Color = clWindowText
      PrintSettings.HeaderFont.Height = -11
      PrintSettings.HeaderFont.Name = 'Segoe UI'
      PrintSettings.HeaderFont.Style = []
      PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
      PrintSettings.FooterFont.Color = clWindowText
      PrintSettings.FooterFont.Height = -11
      PrintSettings.FooterFont.Name = 'Segoe UI'
      PrintSettings.FooterFont.Style = []
      PrintSettings.PageNumSep = '/'
      ScrollBarAlways = saVert
      SearchFooter.FindNextCaption = 'Find next'
      SearchFooter.FindPrevCaption = 'Find previous'
      SearchFooter.Font.Charset = DEFAULT_CHARSET
      SearchFooter.Font.Color = clWindowText
      SearchFooter.Font.Height = -11
      SearchFooter.Font.Name = 'Segoe UI'
      SearchFooter.Font.Style = []
      SearchFooter.HighLightCaption = 'Highlight'
      SearchFooter.HintClose = 'Close'
      SearchFooter.HintFindNext = 'Find next occurence'
      SearchFooter.HintFindPrev = 'Find previous occurence'
      SearchFooter.HintHighlight = 'Highlight occurences'
      SearchFooter.MatchCaseCaption = 'Match case'
      SortSettings.DefaultFormat = ssAutomatic
      SortSettings.Show = True
      Version = '7.2.8.0'
      ColWidths = (
        303
        63
        64
        64
        64)
      RowHeights = (
        21
        21
        21
        21
        21
        21
        21
        21
        21
        21)
    end
  end
  object Panel_Accessoires: TPanel
    Tag = 3
    Left = 576
    Top = 346
    Width = 578
    Height = 332
    BevelOuter = bvNone
    TabOrder = 3
    object Label_Ventilateur: TLabel
      Left = 16
      Top = 48
      Width = 409
      Height = 13
      AutoSize = False
      Caption = 'Label_Ventilateur'
    end
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 578
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Accessoires associ'#233's au ventilateur'
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object Grid_InfosCaisson: TAdvStringGrid
      Left = 121
      Top = 72
      Width = 335
      Height = 65
      Cursor = crDefault
      ColCount = 3
      Ctl3D = False
      DefaultColWidth = 150
      DefaultRowHeight = 21
      DrawingStyle = gdsClassic
      RowCount = 3
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goEditing]
      ParentCtl3D = False
      ParentFont = False
      ScrollBars = ssNone
      TabOrder = 1
      HoverRowCells = [hcNormal, hcSelected]
      ActiveCellFont.Charset = DEFAULT_CHARSET
      ActiveCellFont.Color = clWindowText
      ActiveCellFont.Height = -11
      ActiveCellFont.Name = 'Segoe UI'
      ActiveCellFont.Style = [fsBold]
      Bands.Active = True
      ColumnHeaders.Strings = (
        ''
        'Nb ouies connect'#233'es'
        'Diam'#232'tre')
      ControlLook.FixedGradientHoverFrom = clGray
      ControlLook.FixedGradientHoverTo = clWhite
      ControlLook.FixedGradientDownFrom = clGray
      ControlLook.FixedGradientDownTo = clSilver
      ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
      ControlLook.DropDownHeader.Font.Color = clWindowText
      ControlLook.DropDownHeader.Font.Height = -11
      ControlLook.DropDownHeader.Font.Name = 'Tahoma'
      ControlLook.DropDownHeader.Font.Style = []
      ControlLook.DropDownHeader.Visible = True
      ControlLook.DropDownHeader.Buttons = <>
      ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
      ControlLook.DropDownFooter.Font.Color = clWindowText
      ControlLook.DropDownFooter.Font.Height = -11
      ControlLook.DropDownFooter.Font.Name = 'MS Sans Serif'
      ControlLook.DropDownFooter.Font.Style = []
      ControlLook.DropDownFooter.Visible = True
      ControlLook.DropDownFooter.Buttons = <>
      DefaultEditor = edNumeric
      Filter = <>
      FilterDropDown.Font.Charset = DEFAULT_CHARSET
      FilterDropDown.Font.Color = clWindowText
      FilterDropDown.Font.Height = -11
      FilterDropDown.Font.Name = 'MS Sans Serif'
      FilterDropDown.Font.Style = []
      FilterDropDownClear = '(All)'
      FilterEdit.TypeNames.Strings = (
        'Starts with'
        'Ends with'
        'Contains'
        'Not contains'
        'Equal'
        'Not equal'
        'Clear')
      FixedColWidth = 100
      FixedFont.Charset = DEFAULT_CHARSET
      FixedFont.Color = clWindowText
      FixedFont.Height = -11
      FixedFont.Name = 'Segoe UI'
      FixedFont.Style = [fsBold]
      Flat = True
      FloatFormat = '%.2f'
      MouseActions.DirectEdit = True
      PrintSettings.DateFormat = 'dd/mm/yyyy'
      PrintSettings.Font.Charset = DEFAULT_CHARSET
      PrintSettings.Font.Color = clWindowText
      PrintSettings.Font.Height = -11
      PrintSettings.Font.Name = 'Segoe UI'
      PrintSettings.Font.Style = []
      PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
      PrintSettings.FixedFont.Color = clWindowText
      PrintSettings.FixedFont.Height = -11
      PrintSettings.FixedFont.Name = 'Segoe UI'
      PrintSettings.FixedFont.Style = []
      PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
      PrintSettings.HeaderFont.Color = clWindowText
      PrintSettings.HeaderFont.Height = -11
      PrintSettings.HeaderFont.Name = 'Segoe UI'
      PrintSettings.HeaderFont.Style = []
      PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
      PrintSettings.FooterFont.Color = clWindowText
      PrintSettings.FooterFont.Height = -11
      PrintSettings.FooterFont.Name = 'Segoe UI'
      PrintSettings.FooterFont.Style = []
      PrintSettings.PageNumSep = '/'
      RowHeaders.Strings = (
        ''
        'Aspiration'
        'Rejet')
      SearchFooter.FindNextCaption = 'Find next'
      SearchFooter.FindPrevCaption = 'Find previous'
      SearchFooter.Font.Charset = DEFAULT_CHARSET
      SearchFooter.Font.Color = clWindowText
      SearchFooter.Font.Height = -11
      SearchFooter.Font.Name = 'Segoe UI'
      SearchFooter.Font.Style = []
      SearchFooter.HighLightCaption = 'Highlight'
      SearchFooter.HintClose = 'Close'
      SearchFooter.HintFindNext = 'Find next occurence'
      SearchFooter.HintFindPrev = 'Find previous occurence'
      SearchFooter.HintHighlight = 'Highlight occurences'
      SearchFooter.MatchCaseCaption = 'Match case'
      ShowSelection = False
      SortSettings.DefaultFormat = ssAutomatic
      Version = '7.2.8.0'
      ColWidths = (
        100
        125
        108)
      RowHeights = (
        21
        21
        21)
    end
    object Grid_Accessoires: TAdvStringGrid
      Left = 0
      Top = 140
      Width = 578
      Height = 192
      Cursor = crDefault
      Align = alBottom
      ColCount = 3
      Ctl3D = False
      DefaultColWidth = 150
      DefaultRowHeight = 21
      DrawingStyle = gdsClassic
      FixedCols = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goColSizing, goEditing]
      ParentCtl3D = False
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 2
      HoverRowCells = [hcNormal, hcSelected]
      OnCanEditCell = Grid_InfosCaissonCanEditCell
      ActiveCellFont.Charset = DEFAULT_CHARSET
      ActiveCellFont.Color = clWindowText
      ActiveCellFont.Height = -11
      ActiveCellFont.Name = 'Segoe UI'
      ActiveCellFont.Style = [fsBold]
      Bands.Active = True
      ColumnHeaders.Strings = (
        'Accessoire'
        'R'#233'f'#233'rence commerciale'
        'Quantit'#233)
      ColumnSize.Stretch = True
      ColumnSize.StretchColumn = 1
      ControlLook.FixedGradientHoverFrom = clGray
      ControlLook.FixedGradientHoverTo = clWhite
      ControlLook.FixedGradientDownFrom = clGray
      ControlLook.FixedGradientDownTo = clSilver
      ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
      ControlLook.DropDownHeader.Font.Color = clWindowText
      ControlLook.DropDownHeader.Font.Height = -11
      ControlLook.DropDownHeader.Font.Name = 'Tahoma'
      ControlLook.DropDownHeader.Font.Style = []
      ControlLook.DropDownHeader.Visible = True
      ControlLook.DropDownHeader.Buttons = <>
      ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
      ControlLook.DropDownFooter.Font.Color = clWindowText
      ControlLook.DropDownFooter.Font.Height = -11
      ControlLook.DropDownFooter.Font.Name = 'MS Sans Serif'
      ControlLook.DropDownFooter.Font.Style = []
      ControlLook.DropDownFooter.Visible = True
      ControlLook.DropDownFooter.Buttons = <>
      DefaultEditor = edNumeric
      Filter = <>
      FilterDropDown.Font.Charset = DEFAULT_CHARSET
      FilterDropDown.Font.Color = clWindowText
      FilterDropDown.Font.Height = -11
      FilterDropDown.Font.Name = 'MS Sans Serif'
      FilterDropDown.Font.Style = []
      FilterDropDownClear = '(All)'
      FilterEdit.TypeNames.Strings = (
        'Starts with'
        'Ends with'
        'Contains'
        'Not contains'
        'Equal'
        'Not equal'
        'Clear')
      FixedColWidth = 150
      FixedFont.Charset = DEFAULT_CHARSET
      FixedFont.Color = clWindowText
      FixedFont.Height = -11
      FixedFont.Name = 'Segoe UI'
      FixedFont.Style = [fsBold]
      Flat = True
      FloatFormat = '%.2f'
      MouseActions.DirectEdit = True
      PrintSettings.DateFormat = 'dd/mm/yyyy'
      PrintSettings.Font.Charset = DEFAULT_CHARSET
      PrintSettings.Font.Color = clWindowText
      PrintSettings.Font.Height = -11
      PrintSettings.Font.Name = 'Segoe UI'
      PrintSettings.Font.Style = []
      PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
      PrintSettings.FixedFont.Color = clWindowText
      PrintSettings.FixedFont.Height = -11
      PrintSettings.FixedFont.Name = 'Segoe UI'
      PrintSettings.FixedFont.Style = []
      PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
      PrintSettings.HeaderFont.Color = clWindowText
      PrintSettings.HeaderFont.Height = -11
      PrintSettings.HeaderFont.Name = 'Segoe UI'
      PrintSettings.HeaderFont.Style = []
      PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
      PrintSettings.FooterFont.Color = clWindowText
      PrintSettings.FooterFont.Height = -11
      PrintSettings.FooterFont.Name = 'Segoe UI'
      PrintSettings.FooterFont.Style = []
      PrintSettings.PageNumSep = '/'
      ScrollBarAlways = saVert
      SearchFooter.FindNextCaption = 'Find next'
      SearchFooter.FindPrevCaption = 'Find previous'
      SearchFooter.Font.Charset = DEFAULT_CHARSET
      SearchFooter.Font.Color = clWindowText
      SearchFooter.Font.Height = -11
      SearchFooter.Font.Name = 'Segoe UI'
      SearchFooter.Font.Style = []
      SearchFooter.HighLightCaption = 'Highlight'
      SearchFooter.HintClose = 'Close'
      SearchFooter.HintFindNext = 'Find next occurence'
      SearchFooter.HintFindPrev = 'Find previous occurence'
      SearchFooter.HintHighlight = 'Highlight occurences'
      SearchFooter.MatchCaseCaption = 'Match case'
      ShowSelection = False
      SortSettings.DefaultFormat = ssAutomatic
      SortSettings.Show = True
      Version = '7.2.8.0'
      ColWidths = (
        150
        205
        204)
      RowHeights = (
        21
        21
        21
        21
        21
        21
        21
        21
        21
        21)
    end
  end
  object Panel_AccessoiresAnnexes: TPanel
    Tag = 4
    Left = 2
    Top = 346
    Width = 577
    Height = 332
    BevelOuter = bvNone
    TabOrder = 4
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 577
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Accessoires annexes'
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 41
      Width = 577
      Height = 291
      ActivePage = TabSheetSupportsTelescopiques
      Align = alClient
      TabOrder = 1
      OnChange = PageControl1Change
      object TabSheetMontage: TTabSheet
        Caption = 'Montage'
        object Radio_Adhesif: TRadioGroup
          Left = 8
          Top = 8
          Width = 545
          Height = 49
          Caption = 'Adhesif'
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Adh'#233'sif PVC'
            'Adh'#233'sif Aluminium')
          TabOrder = 0
        end
        object GroupBox7: TGroupBox
          Left = 8
          Top = 69
          Width = 545
          Height = 57
          Caption = 'Visserie'
          TabOrder = 1
          object Label3: TLabel
            Left = 20
            Top = 30
            Width = 73
            Height = 13
            Caption = 'Vis H7 4.2 x 13'
            Transparent = True
          end
          object Label4: TLabel
            Left = 276
            Top = 30
            Width = 73
            Height = 13
            Caption = 'Vis H7 4.2 x 19'
            Transparent = True
          end
          object Edit_VIS13: TAdvEdit
            Left = 104
            Top = 24
            Width = 33
            Height = 21
            EditAlign = eaRight
            EditType = etNumeric
            EmptyTextStyle = []
            Flat = True
            FocusColor = clBtnFace
            LabelFont.Charset = DEFAULT_CHARSET
            LabelFont.Color = clWindowText
            LabelFont.Height = -11
            LabelFont.Name = 'Segoe UI'
            LabelFont.Style = []
            Lookup.Color = clBtnFace
            Lookup.Font.Charset = DEFAULT_CHARSET
            Lookup.Font.Color = clWindowText
            Lookup.Font.Height = -11
            Lookup.Font.Name = 'Arial'
            Lookup.Font.Style = []
            Lookup.Separator = ';'
            BorderStyle = bsNone
            Color = clBtnFace
            ParentColor = True
            TabOrder = 0
            Text = '0'
            Transparent = True
            Visible = True
            Version = '3.3.2.0'
          end
          object Edit_VIS19: TAdvEdit
            Left = 360
            Top = 24
            Width = 33
            Height = 21
            EditAlign = eaRight
            EditType = etNumeric
            EmptyTextStyle = []
            Flat = True
            FocusColor = clBtnFace
            LabelFont.Charset = DEFAULT_CHARSET
            LabelFont.Color = clWindowText
            LabelFont.Height = -11
            LabelFont.Name = 'Segoe UI'
            LabelFont.Style = []
            Lookup.Font.Charset = DEFAULT_CHARSET
            Lookup.Font.Color = clWindowText
            Lookup.Font.Height = -11
            Lookup.Font.Name = 'Arial'
            Lookup.Font.Style = []
            Lookup.Separator = ';'
            BorderStyle = bsNone
            Color = clBtnFace
            TabOrder = 1
            Text = '0'
            Transparent = True
            Visible = True
            Version = '3.3.2.0'
          end
        end
        object GroupBox8: TGroupBox
          Left = 8
          Top = 140
          Width = 545
          Height = 94
          Caption = 'Isolants et fourreaux'
          TabOrder = 2
          object Label5: TLabel
            Left = 11
            Top = 30
            Width = 95
            Height = 13
            Caption = 'Isolant externe M0'
            Transparent = True
          end
          object Label6: TLabel
            Left = 277
            Top = 30
            Width = 95
            Height = 13
            Caption = 'Isolant externe M1'
            Transparent = True
          end
          object Edit_M0: TAdvEdit
            Left = 104
            Top = 24
            Width = 33
            Height = 21
            EditAlign = eaRight
            EditType = etNumeric
            EmptyTextStyle = []
            Flat = True
            FocusColor = clBtnFace
            LabelFont.Charset = DEFAULT_CHARSET
            LabelFont.Color = clWindowText
            LabelFont.Height = -11
            LabelFont.Name = 'Segoe UI'
            LabelFont.Style = []
            Lookup.Color = clBtnFace
            Lookup.Font.Charset = DEFAULT_CHARSET
            Lookup.Font.Color = clWindowText
            Lookup.Font.Height = -11
            Lookup.Font.Name = 'Arial'
            Lookup.Font.Style = []
            Lookup.Separator = ';'
            BorderStyle = bsNone
            Color = clBtnFace
            ParentColor = True
            TabOrder = 0
            Text = '0'
            Transparent = True
            Visible = True
            Version = '3.3.2.0'
          end
          object Edit_M1: TAdvEdit
            Left = 368
            Top = 24
            Width = 33
            Height = 21
            EditAlign = eaRight
            EditType = etNumeric
            EmptyTextStyle = []
            Flat = True
            FocusColor = clBtnFace
            LabelFont.Charset = DEFAULT_CHARSET
            LabelFont.Color = clWindowText
            LabelFont.Height = -11
            LabelFont.Name = 'Segoe UI'
            LabelFont.Style = []
            Lookup.Font.Charset = DEFAULT_CHARSET
            Lookup.Font.Color = clWindowText
            Lookup.Font.Height = -11
            Lookup.Font.Name = 'Arial'
            Lookup.Font.Style = []
            Lookup.Separator = ';'
            BorderStyle = bsNone
            Color = clBtnFace
            TabOrder = 1
            Text = '0'
            Transparent = True
            Visible = True
            Version = '3.3.2.0'
          end
          object Check_IsolantMur: TCheckBox
            Left = 8
            Top = 64
            Width = 161
            Height = 17
            Caption = 'Isolant pour travers'#233'e de mur'
            TabOrder = 2
          end
          object Check_Fourreaux: TCheckBox
            Left = 278
            Top = 64
            Width = 161
            Height = 17
            Caption = 'Fourreaux lisses'
            TabOrder = 3
          end
        end
      end
      object TabSheetBavettEtColliers: TTabSheet
        Caption = 'Bavettes et colliers'
        ImageIndex = 1
        object GroupBox9: TGroupBox
          Left = 0
          Top = 80
          Width = 561
          Height = 132
          Caption = 'Bavettes'
          TabOrder = 0
          object Grid_Bavettes: TAdvStringGrid
            Left = 76
            Top = 38
            Width = 402
            Height = 86
            Cursor = crDefault
            ColCount = 8
            Ctl3D = False
            DefaultRowHeight = 21
            DrawingStyle = gdsClassic
            RowCount = 4
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goColSizing, goEditing]
            ParentCtl3D = False
            ParentFont = False
            ScrollBars = ssBoth
            TabOrder = 0
            HoverRowCells = [hcNormal, hcSelected]
            OnIsFixedCell = Grid_BavettesIsFixedCell
            ActiveCellFont.Charset = DEFAULT_CHARSET
            ActiveCellFont.Color = clWindowText
            ActiveCellFont.Height = -11
            ActiveCellFont.Name = 'Segoe UI'
            ActiveCellFont.Style = [fsBold]
            AutoSize = True
            Bands.Active = True
            ColumnHeaders.Strings = (
              'Diam'#232'tre'
              'Qt'#233
              'Diam'#232'tre'
              'Qt'#233
              'Diam'#232'tre'
              'Qt'#233
              'Diam'#232'tre'
              'Qt'#233)
            ControlLook.FixedGradientHoverFrom = clGray
            ControlLook.FixedGradientHoverTo = clWhite
            ControlLook.FixedGradientDownFrom = clGray
            ControlLook.FixedGradientDownTo = clSilver
            ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
            ControlLook.DropDownHeader.Font.Color = clWindowText
            ControlLook.DropDownHeader.Font.Height = -11
            ControlLook.DropDownHeader.Font.Name = 'Tahoma'
            ControlLook.DropDownHeader.Font.Style = []
            ControlLook.DropDownHeader.Visible = True
            ControlLook.DropDownHeader.Buttons = <>
            ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
            ControlLook.DropDownFooter.Font.Color = clWindowText
            ControlLook.DropDownFooter.Font.Height = -11
            ControlLook.DropDownFooter.Font.Name = 'MS Sans Serif'
            ControlLook.DropDownFooter.Font.Style = []
            ControlLook.DropDownFooter.Visible = True
            ControlLook.DropDownFooter.Buttons = <>
            DefaultEditor = edNumeric
            Filter = <>
            FilterDropDown.Font.Charset = DEFAULT_CHARSET
            FilterDropDown.Font.Color = clWindowText
            FilterDropDown.Font.Height = -11
            FilterDropDown.Font.Name = 'MS Sans Serif'
            FilterDropDown.Font.Style = []
            FilterDropDownClear = '(All)'
            FilterEdit.TypeNames.Strings = (
              'Starts with'
              'Ends with'
              'Contains'
              'Not contains'
              'Equal'
              'Not equal'
              'Clear')
            FixedColWidth = 60
            FixedFont.Charset = DEFAULT_CHARSET
            FixedFont.Color = clWindowText
            FixedFont.Height = -11
            FixedFont.Name = 'Segoe UI'
            FixedFont.Style = [fsBold]
            Flat = True
            FloatFormat = '%.2f'
            MouseActions.DirectEdit = True
            PrintSettings.DateFormat = 'dd/mm/yyyy'
            PrintSettings.Font.Charset = DEFAULT_CHARSET
            PrintSettings.Font.Color = clWindowText
            PrintSettings.Font.Height = -11
            PrintSettings.Font.Name = 'Segoe UI'
            PrintSettings.Font.Style = []
            PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
            PrintSettings.FixedFont.Color = clWindowText
            PrintSettings.FixedFont.Height = -11
            PrintSettings.FixedFont.Name = 'Segoe UI'
            PrintSettings.FixedFont.Style = []
            PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
            PrintSettings.HeaderFont.Color = clWindowText
            PrintSettings.HeaderFont.Height = -11
            PrintSettings.HeaderFont.Name = 'Segoe UI'
            PrintSettings.HeaderFont.Style = []
            PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
            PrintSettings.FooterFont.Color = clWindowText
            PrintSettings.FooterFont.Height = -11
            PrintSettings.FooterFont.Name = 'Segoe UI'
            PrintSettings.FooterFont.Style = []
            PrintSettings.PageNumSep = '/'
            RowHeaders.Strings = (
              'Diam'#232'tre'
              #216' 125'
              #216' 160'
              #216' 200'
              #216' 250'
              #216' 315'
              #216' 355'
              '')
            SearchFooter.FindNextCaption = 'Find next'
            SearchFooter.FindPrevCaption = 'Find previous'
            SearchFooter.Font.Charset = DEFAULT_CHARSET
            SearchFooter.Font.Color = clWindowText
            SearchFooter.Font.Height = -11
            SearchFooter.Font.Name = 'Segoe UI'
            SearchFooter.Font.Style = []
            SearchFooter.HighLightCaption = 'Highlight'
            SearchFooter.HintClose = 'Close'
            SearchFooter.HintFindNext = 'Find next occurence'
            SearchFooter.HintFindPrev = 'Find previous occurence'
            SearchFooter.HintHighlight = 'Highlight occurences'
            SearchFooter.MatchCaseCaption = 'Match case'
            ShowSelection = False
            SortSettings.DefaultFormat = ssAutomatic
            Version = '7.2.8.0'
            ColWidths = (
              60
              30
              59
              30
              59
              30
              59
              30)
            RowHeights = (
              21
              21
              21
              21)
          end
          object CheckBoxCompteBavettes: TCheckBox
            Left = 8
            Top = 16
            Width = 129
            Height = 17
            Caption = 'Bavettes Oui / Non'
            Checked = True
            State = cbChecked
            TabOrder = 1
            OnClick = CheckBoxCompteBavettesClick
          end
        end
        object Radio_Colliers: TRadioGroup
          Left = 0
          Top = 12
          Width = 561
          Height = 35
          Caption = 'Colliers d'#39#233'tage'
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'Non quantifi'#233's'
            'Non isol'#233's'
            'Isol'#233's')
          TabOrder = 1
          OnClick = Radio_ColliersClick
        end
      end
      object TabSheetSupportsTelescopiques: TTabSheet
        Caption = 'Supports t'#233'lescopiques'
        ImageIndex = 2
        object GroupBox10: TGroupBox
          Left = 0
          Top = 2
          Width = 553
          Height = 105
          Caption = 'Supports t'#233'lescopiques'
          TabOrder = 0
          object CheckBox_Universel: TCheckBox
            Left = 8
            Top = 18
            Width = 144
            Height = 17
            Caption = 'Universels (avec colliers)'
            TabOrder = 0
            OnClick = CheckBox_UniverselClick
          end
          object CheckBox_125300: TCheckBox
            Left = 8
            Top = 67
            Width = 215
            Height = 17
            Caption = 'SCTZ 125 - Embrase 300x300 (avec BP)'
            TabOrder = 1
            OnClick = CheckBox_125100Click
          end
          object CheckBox_160300: TCheckBox
            Left = 8
            Top = 84
            Width = 218
            Height = 17
            Caption = 'SCTZ 160 - Embrase 300x300 (avec BP)'
            TabOrder = 2
            OnClick = CheckBox_125100Click
          end
          object CheckBox_125100: TCheckBox
            Left = 8
            Top = 35
            Width = 219
            Height = 17
            Caption = 'SCTZ 125 - Embrase 100x150 (avec BP)'
            TabOrder = 3
            OnClick = CheckBox_125100Click
          end
          object CheckBox_160100: TCheckBox
            Left = 8
            Top = 51
            Width = 220
            Height = 17
            Caption = 'SCTZ 160 - Embrase 100x150 (avec BP)'
            TabOrder = 4
            OnClick = CheckBox_125100Click
          end
          object Edit_NbSupp160300: TAdvEdit
            Left = 241
            Top = 82
            Width = 33
            Height = 17
            EditAlign = eaRight
            EditType = etNumeric
            EmptyTextStyle = []
            Flat = True
            FocusColor = clBtnFace
            LabelFont.Charset = DEFAULT_CHARSET
            LabelFont.Color = clWindowText
            LabelFont.Height = -11
            LabelFont.Name = 'Segoe UI'
            LabelFont.Style = []
            Lookup.Color = clBtnFace
            Lookup.Font.Charset = DEFAULT_CHARSET
            Lookup.Font.Color = clWindowText
            Lookup.Font.Height = -11
            Lookup.Font.Name = 'Arial'
            Lookup.Font.Style = []
            Lookup.Separator = ';'
            BorderStyle = bsNone
            Color = clBtnFace
            ParentColor = True
            TabOrder = 5
            Text = '0'
            Transparent = True
            Visible = False
            Version = '3.3.2.0'
          end
          object Edit_NbSupp125300: TAdvEdit
            Left = 241
            Top = 66
            Width = 33
            Height = 17
            EditAlign = eaRight
            EditType = etNumeric
            EmptyTextStyle = []
            Flat = True
            FocusColor = clBtnFace
            LabelFont.Charset = DEFAULT_CHARSET
            LabelFont.Color = clWindowText
            LabelFont.Height = -11
            LabelFont.Name = 'Segoe UI'
            LabelFont.Style = []
            Lookup.Color = clBtnFace
            Lookup.Font.Charset = DEFAULT_CHARSET
            Lookup.Font.Color = clWindowText
            Lookup.Font.Height = -11
            Lookup.Font.Name = 'Arial'
            Lookup.Font.Style = []
            Lookup.Separator = ';'
            BorderStyle = bsNone
            Color = clBtnFace
            ParentColor = True
            TabOrder = 6
            Text = '0'
            Transparent = True
            Visible = False
            Version = '3.3.2.0'
          end
          object Edit_NbSupp160100: TAdvEdit
            Left = 241
            Top = 49
            Width = 33
            Height = 17
            EditAlign = eaRight
            EditType = etNumeric
            EmptyTextStyle = []
            Flat = True
            FocusColor = clBtnFace
            LabelFont.Charset = DEFAULT_CHARSET
            LabelFont.Color = clWindowText
            LabelFont.Height = -11
            LabelFont.Name = 'Segoe UI'
            LabelFont.Style = []
            Lookup.Color = clBtnFace
            Lookup.Font.Charset = DEFAULT_CHARSET
            Lookup.Font.Color = clWindowText
            Lookup.Font.Height = -11
            Lookup.Font.Name = 'Arial'
            Lookup.Font.Style = []
            Lookup.Separator = ';'
            BorderStyle = bsNone
            Color = clBtnFace
            ParentColor = True
            TabOrder = 7
            Text = '0'
            Transparent = True
            Visible = False
            Version = '3.3.2.0'
          end
          object Edit_NbSupp125100: TAdvEdit
            Left = 241
            Top = 32
            Width = 33
            Height = 17
            EditAlign = eaRight
            EditType = etNumeric
            EmptyTextStyle = []
            Flat = True
            FocusColor = clBtnFace
            LabelFont.Charset = DEFAULT_CHARSET
            LabelFont.Color = clWindowText
            LabelFont.Height = -11
            LabelFont.Name = 'Segoe UI'
            LabelFont.Style = []
            Lookup.Color = clBtnFace
            Lookup.Font.Charset = DEFAULT_CHARSET
            Lookup.Font.Color = clWindowText
            Lookup.Font.Height = -11
            Lookup.Font.Name = 'Arial'
            Lookup.Font.Style = []
            Lookup.Separator = ';'
            BorderStyle = bsNone
            Color = clBtnFace
            ParentColor = True
            TabOrder = 8
            Text = '0'
            Transparent = True
            Visible = False
            Version = '3.3.2.0'
          end
          object Edit_NbSuppUni: TAdvEdit
            Left = 241
            Top = 16
            Width = 33
            Height = 17
            EditAlign = eaRight
            EditType = etNumeric
            EmptyTextStyle = []
            Flat = True
            FocusColor = clBtnFace
            LabelFont.Charset = DEFAULT_CHARSET
            LabelFont.Color = clWindowText
            LabelFont.Height = -11
            LabelFont.Name = 'Segoe UI'
            LabelFont.Style = []
            Lookup.Color = clBtnFace
            Lookup.Font.Charset = DEFAULT_CHARSET
            Lookup.Font.Color = clWindowText
            Lookup.Font.Height = -11
            Lookup.Font.Name = 'Arial'
            Lookup.Font.Style = []
            Lookup.Separator = ';'
            BorderStyle = bsNone
            Color = clBtnFace
            ParentColor = True
            TabOrder = 9
            Text = '0'
            Visible = False
            Version = '3.3.2.0'
          end
          object CheckBox_ColliersSuppIsoles: TCheckBox
            Left = 402
            Top = 19
            Width = 122
            Height = 17
            Caption = 'Avec colliers isol'#233's'
            TabOrder = 10
          end
        end
        object Radio_ColliersSupp: TGroupBox
          Left = 0
          Top = 136
          Width = 553
          Height = 127
          Caption = 'Colliers suppl'#233'mentaires'
          TabOrder = 1
          object Grid_ColliersSupp: TAdvStringGrid
            Left = 67
            Top = 17
            Width = 402
            Height = 107
            Cursor = crDefault
            ColCount = 8
            Ctl3D = False
            DefaultRowHeight = 21
            DrawingStyle = gdsClassic
            Enabled = False
            RowCount = 5
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goColSizing, goEditing]
            ParentCtl3D = False
            ParentFont = False
            ScrollBars = ssBoth
            TabOrder = 0
            HoverRowCells = [hcNormal, hcSelected]
            OnIsFixedCell = Grid_ColliersSuppIsFixedCell
            ActiveCellFont.Charset = DEFAULT_CHARSET
            ActiveCellFont.Color = clWindowText
            ActiveCellFont.Height = -11
            ActiveCellFont.Name = 'Segoe UI'
            ActiveCellFont.Style = [fsBold]
            AutoSize = True
            Bands.Active = True
            ColumnHeaders.Strings = (
              'Diam'#232'tre'
              'Qt'#233
              'Diam'#232'tre'
              'Qt'#233
              'Diam'#232'tre'
              'Qt'#233
              'Diam'#232'tre'
              'Qt'#233)
            ControlLook.FixedGradientHoverFrom = clGray
            ControlLook.FixedGradientHoverTo = clWhite
            ControlLook.FixedGradientDownFrom = clGray
            ControlLook.FixedGradientDownTo = clSilver
            ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
            ControlLook.DropDownHeader.Font.Color = clWindowText
            ControlLook.DropDownHeader.Font.Height = -11
            ControlLook.DropDownHeader.Font.Name = 'Tahoma'
            ControlLook.DropDownHeader.Font.Style = []
            ControlLook.DropDownHeader.Visible = True
            ControlLook.DropDownHeader.Buttons = <>
            ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
            ControlLook.DropDownFooter.Font.Color = clWindowText
            ControlLook.DropDownFooter.Font.Height = -11
            ControlLook.DropDownFooter.Font.Name = 'MS Sans Serif'
            ControlLook.DropDownFooter.Font.Style = []
            ControlLook.DropDownFooter.Visible = True
            ControlLook.DropDownFooter.Buttons = <>
            DefaultEditor = edNumeric
            Filter = <>
            FilterDropDown.Font.Charset = DEFAULT_CHARSET
            FilterDropDown.Font.Color = clWindowText
            FilterDropDown.Font.Height = -11
            FilterDropDown.Font.Name = 'MS Sans Serif'
            FilterDropDown.Font.Style = []
            FilterDropDownClear = '(All)'
            FilterEdit.TypeNames.Strings = (
              'Starts with'
              'Ends with'
              'Contains'
              'Not contains'
              'Equal'
              'Not equal'
              'Clear')
            FixedColWidth = 60
            FixedFont.Charset = DEFAULT_CHARSET
            FixedFont.Color = clWindowText
            FixedFont.Height = -11
            FixedFont.Name = 'Segoe UI'
            FixedFont.Style = [fsBold]
            Flat = True
            FloatFormat = '%.2f'
            MouseActions.DirectEdit = True
            PrintSettings.DateFormat = 'dd/mm/yyyy'
            PrintSettings.Font.Charset = DEFAULT_CHARSET
            PrintSettings.Font.Color = clWindowText
            PrintSettings.Font.Height = -11
            PrintSettings.Font.Name = 'Segoe UI'
            PrintSettings.Font.Style = []
            PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
            PrintSettings.FixedFont.Color = clWindowText
            PrintSettings.FixedFont.Height = -11
            PrintSettings.FixedFont.Name = 'Segoe UI'
            PrintSettings.FixedFont.Style = []
            PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
            PrintSettings.HeaderFont.Color = clWindowText
            PrintSettings.HeaderFont.Height = -11
            PrintSettings.HeaderFont.Name = 'Segoe UI'
            PrintSettings.HeaderFont.Style = []
            PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
            PrintSettings.FooterFont.Color = clWindowText
            PrintSettings.FooterFont.Height = -11
            PrintSettings.FooterFont.Name = 'Segoe UI'
            PrintSettings.FooterFont.Style = []
            PrintSettings.PageNumSep = '/'
            RowHeaders.Strings = (
              'Diam'#232'tre'
              #216' 125'
              #216' 160'
              #216' 200'
              #216' 250'
              #216' 315'
              #216' 355'
              '')
            SearchFooter.FindNextCaption = 'Find next'
            SearchFooter.FindPrevCaption = 'Find previous'
            SearchFooter.Font.Charset = DEFAULT_CHARSET
            SearchFooter.Font.Color = clWindowText
            SearchFooter.Font.Height = -11
            SearchFooter.Font.Name = 'Segoe UI'
            SearchFooter.Font.Style = []
            SearchFooter.HighLightCaption = 'Highlight'
            SearchFooter.HintClose = 'Close'
            SearchFooter.HintFindNext = 'Find next occurence'
            SearchFooter.HintFindPrev = 'Find previous occurence'
            SearchFooter.HintHighlight = 'Highlight occurences'
            SearchFooter.MatchCaseCaption = 'Match case'
            ShowSelection = False
            SortSettings.DefaultFormat = ssAutomatic
            Version = '7.2.8.0'
            ColWidths = (
              60
              30
              59
              30
              59
              30
              59
              30)
            RowHeights = (
              21
              21
              21
              21
              21)
          end
        end
        object GroupBox_BandePerf: TGroupBox
          Left = 2
          Top = 105
          Width = 551
          Height = 32
          Caption = 'Bande perfor'#233'e'
          TabOrder = 2
          object Label7: TLabel
            Left = 214
            Top = 10
            Width = 45
            Height = 13
            Caption = 'rouleaux'
          end
          object CheckBox_BandePerf: TCheckBox
            Left = 128
            Top = 8
            Width = 65
            Height = 17
            Caption = 'Utiliser'
            Enabled = False
            TabOrder = 0
            OnClick = CheckBox_BandePerfClick
          end
          object Edit_NbRlxBandePerforee: TAdvEdit
            Left = 265
            Top = 8
            Width = 33
            Height = 21
            EditAlign = eaRight
            EditType = etNumeric
            EmptyTextStyle = []
            Flat = True
            FocusColor = clBtnFace
            LabelFont.Charset = DEFAULT_CHARSET
            LabelFont.Color = clWindowText
            LabelFont.Height = -11
            LabelFont.Name = 'Segoe UI'
            LabelFont.Style = []
            Lookup.Color = clBtnFace
            Lookup.Font.Charset = DEFAULT_CHARSET
            Lookup.Font.Color = clWindowText
            Lookup.Font.Height = -11
            Lookup.Font.Name = 'Arial'
            Lookup.Font.Style = []
            Lookup.Separator = ';'
            BorderStyle = bsNone
            Color = clBtnFace
            ParentColor = True
            TabOrder = 1
            Text = '0'
            Transparent = True
            Visible = True
            Version = '3.3.2.0'
          end
          object Text_LongueurBandePerf: TStaticText
            Left = 368
            Top = 10
            Width = 22
            Height = 17
            Caption = '( m)'
            TabOrder = 2
          end
          object StaticText1: TStaticText
            Left = 318
            Top = 9
            Width = 49
            Height = 17
            Caption = 'rouleaux'
            TabOrder = 3
          end
        end
      end
      object TabSheetTrappesDeVisite: TTabSheet
        Caption = 'Trappes de visite'
        ImageIndex = 3
        object Grid_TrappesVisite: TAdvStringGrid
          Left = 75
          Top = 22
          Width = 402
          Height = 107
          Cursor = crDefault
          ColCount = 8
          Ctl3D = False
          DefaultRowHeight = 21
          DrawingStyle = gdsClassic
          RowCount = 5
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goColSizing, goEditing]
          ParentCtl3D = False
          ParentFont = False
          ScrollBars = ssBoth
          TabOrder = 0
          HoverRowCells = [hcNormal, hcSelected]
          OnIsFixedCell = Grid_TrappesVisiteIsFixedCell
          ActiveCellFont.Charset = DEFAULT_CHARSET
          ActiveCellFont.Color = clWindowText
          ActiveCellFont.Height = -11
          ActiveCellFont.Name = 'Segoe UI'
          ActiveCellFont.Style = [fsBold]
          AutoSize = True
          Bands.Active = True
          ColumnHeaders.Strings = (
            'Diam'#232'tre'
            'Qt'#233
            'Diam'#232'tre'
            'Qt'#233
            'Diam'#232'tre'
            'Qt'#233
            'Diam'#232'tre'
            'Qt'#233)
          ControlLook.FixedGradientHoverFrom = clGray
          ControlLook.FixedGradientHoverTo = clWhite
          ControlLook.FixedGradientDownFrom = clGray
          ControlLook.FixedGradientDownTo = clSilver
          ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
          ControlLook.DropDownHeader.Font.Color = clWindowText
          ControlLook.DropDownHeader.Font.Height = -11
          ControlLook.DropDownHeader.Font.Name = 'Tahoma'
          ControlLook.DropDownHeader.Font.Style = []
          ControlLook.DropDownHeader.Visible = True
          ControlLook.DropDownHeader.Buttons = <>
          ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
          ControlLook.DropDownFooter.Font.Color = clWindowText
          ControlLook.DropDownFooter.Font.Height = -11
          ControlLook.DropDownFooter.Font.Name = 'MS Sans Serif'
          ControlLook.DropDownFooter.Font.Style = []
          ControlLook.DropDownFooter.Visible = True
          ControlLook.DropDownFooter.Buttons = <>
          DefaultEditor = edNumeric
          Filter = <>
          FilterDropDown.Font.Charset = DEFAULT_CHARSET
          FilterDropDown.Font.Color = clWindowText
          FilterDropDown.Font.Height = -11
          FilterDropDown.Font.Name = 'MS Sans Serif'
          FilterDropDown.Font.Style = []
          FilterDropDown.TextChecked = 'Checked'
          FilterDropDown.TextUnChecked = 'Unchecked'
          FilterDropDownClear = '(All)'
          FilterEdit.TypeNames.Strings = (
            'Starts with'
            'Ends with'
            'Contains'
            'Not contains'
            'Equal'
            'Not equal'
            'Clear')
          FixedColWidth = 60
          FixedFont.Charset = DEFAULT_CHARSET
          FixedFont.Color = clWindowText
          FixedFont.Height = -11
          FixedFont.Name = 'Segoe UI'
          FixedFont.Style = [fsBold]
          Flat = True
          FloatFormat = '%.2f'
          MouseActions.DirectEdit = True
          PrintSettings.DateFormat = 'dd/mm/yyyy'
          PrintSettings.Font.Charset = DEFAULT_CHARSET
          PrintSettings.Font.Color = clWindowText
          PrintSettings.Font.Height = -11
          PrintSettings.Font.Name = 'Segoe UI'
          PrintSettings.Font.Style = []
          PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
          PrintSettings.FixedFont.Color = clWindowText
          PrintSettings.FixedFont.Height = -11
          PrintSettings.FixedFont.Name = 'Segoe UI'
          PrintSettings.FixedFont.Style = []
          PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
          PrintSettings.HeaderFont.Color = clWindowText
          PrintSettings.HeaderFont.Height = -11
          PrintSettings.HeaderFont.Name = 'Segoe UI'
          PrintSettings.HeaderFont.Style = []
          PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
          PrintSettings.FooterFont.Color = clWindowText
          PrintSettings.FooterFont.Height = -11
          PrintSettings.FooterFont.Name = 'Segoe UI'
          PrintSettings.FooterFont.Style = []
          PrintSettings.PageNumSep = '/'
          RowHeaders.Strings = (
            'Diam'#232'tre'
            #216' 125'
            #216' 160'
            #216' 200'
            #216' 250'
            #216' 315'
            #216' 355'
            '')
          SearchFooter.FindNextCaption = 'Find next'
          SearchFooter.FindPrevCaption = 'Find previous'
          SearchFooter.Font.Charset = DEFAULT_CHARSET
          SearchFooter.Font.Color = clWindowText
          SearchFooter.Font.Height = -11
          SearchFooter.Font.Name = 'Segoe UI'
          SearchFooter.Font.Style = []
          SearchFooter.HighLightCaption = 'Highlight'
          SearchFooter.HintClose = 'Close'
          SearchFooter.HintFindNext = 'Find next occurence'
          SearchFooter.HintFindPrev = 'Find previous occurence'
          SearchFooter.HintHighlight = 'Highlight occurences'
          SearchFooter.MatchCaseCaption = 'Match case'
          ShowSelection = False
          SortSettings.DefaultFormat = ssAutomatic
          Version = '7.2.8.0'
          ColWidths = (
            60
            30
            59
            30
            59
            30
            59
            30)
          RowHeights = (
            21
            21
            21
            21
            21)
        end
      end
      object TabSheetActhysTerrasse: TTabSheet
        Caption = 'Terrasse / Comble'
        ImageIndex = 4
        object LabelCableSuspensionRapide: TLabel
          Left = 278
          Top = 16
          Width = 47
          Height = 13
          Caption = 'par barre'
        end
        object LabelCollierUniverselTerrasse: TLabel
          Left = 278
          Top = 40
          Width = 47
          Height = 13
          Caption = 'par barre'
        end
        object CheckBoxACT_PST: TCheckBox
          Left = 18
          Top = 63
          Width = 175
          Height = 17
          Caption = 'Pied Support Terrasse (PST)'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object CheckBoxACT_CablesSuspensionRapide: TCheckBox
          Left = 18
          Top = 16
          Width = 175
          Height = 17
          Caption = 'C'#226'bles de suspension rapide'
          TabOrder = 1
        end
        object AdvEditACT_CablesSuspensionRapide: TAdvEdit
          Left = 225
          Top = 16
          Width = 33
          Height = 17
          EditAlign = eaRight
          EditType = etNumeric
          EmptyTextStyle = []
          Flat = True
          FocusColor = clBtnFace
          LabelFont.Charset = DEFAULT_CHARSET
          LabelFont.Color = clWindowText
          LabelFont.Height = -11
          LabelFont.Name = 'Segoe UI'
          LabelFont.Style = []
          Lookup.Color = clBtnFace
          Lookup.Font.Charset = DEFAULT_CHARSET
          Lookup.Font.Color = clWindowText
          Lookup.Font.Height = -11
          Lookup.Font.Name = 'Arial'
          Lookup.Font.Style = []
          Lookup.Separator = ';'
          BorderStyle = bsNone
          Color = clBtnFace
          ParentColor = True
          TabOrder = 2
          Text = '1'
          Visible = True
          Version = '3.3.2.0'
        end
        object AdvEditACT_CollierUniverselTerrasse: TAdvEdit
          Left = 225
          Top = 40
          Width = 33
          Height = 17
          EditAlign = eaRight
          EditType = etNumeric
          EmptyTextStyle = []
          Flat = True
          FocusColor = clBtnFace
          LabelFont.Charset = DEFAULT_CHARSET
          LabelFont.Color = clWindowText
          LabelFont.Height = -11
          LabelFont.Name = 'Segoe UI'
          LabelFont.Style = []
          Lookup.Color = clBtnFace
          Lookup.Font.Charset = DEFAULT_CHARSET
          Lookup.Font.Color = clWindowText
          Lookup.Font.Height = -11
          Lookup.Font.Name = 'Arial'
          Lookup.Font.Style = []
          Lookup.Separator = ';'
          BorderStyle = bsNone
          Color = clBtnFace
          ParentColor = True
          TabOrder = 3
          Text = '1'
          Visible = True
          Version = '3.3.2.0'
        end
        object CheckBoxACT_CollierUniverselTerrasse: TCheckBox
          Left = 18
          Top = 40
          Width = 175
          Height = 17
          Caption = 'Collier Universel (CU)'
          Checked = True
          State = cbChecked
          TabOrder = 4
          OnClick = CheckBoxACT_CollierUniverselTerrasseClick
        end
        object CheckBoxACT_TrappesDeVisite: TCheckBox
          Left = 18
          Top = 85
          Width = 175
          Height = 17
          Caption = 'Trappes de visite'
          Checked = True
          State = cbChecked
          TabOrder = 5
        end
        object CheckBoxACT_BandeAdhesive: TCheckBox
          Left = 18
          Top = 108
          Width = 175
          Height = 17
          Caption = 'Bande adh'#233'sive (ROA)'
          TabOrder = 6
        end
      end
      object TabSheetActhysColonnes: TTabSheet
        Caption = 'Colonnes'
        ImageIndex = 5
        object LabelCollierUniverselColonne: TLabel
          Left = 271
          Top = 137
          Width = 47
          Height = 13
          Caption = 'par barre'
        end
        object RadioGroupACT_TeSouche: TRadioGroup
          Left = 11
          Top = 11
          Width = 313
          Height = 73
          Caption = 'T'#233' souche'
          ItemIndex = 1
          Items.Strings = (
            'Caisson piquage '#224' joint CP'
            'Caisson piquage '#224' joint acoustique / a'#233'raulique CP2A'
            'T'#233' bouch'#233)
          TabOrder = 0
        end
        object CheckBoxACT_FourreauDeTraversee: TCheckBox
          Left = 11
          Top = 90
          Width = 175
          Height = 17
          Caption = 'Fourreau de Travers'#233'e (FT)'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
        object CheckBoxACT_Joint: TCheckBox
          Left = 11
          Top = 113
          Width = 230
          Height = 17
          Caption = 'Joints  de Travers'#233' de Dalle (JTD) adh'#233'sif'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object CheckBoxACT_CollierUniverselColonne: TCheckBox
          Left = 11
          Top = 136
          Width = 175
          Height = 17
          Caption = 'Collier Universel (CU)'
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
        object AdvEditACT_CollierUniverselColonne: TAdvEdit
          Left = 218
          Top = 136
          Width = 33
          Height = 17
          EditAlign = eaRight
          EditType = etNumeric
          EmptyTextStyle = []
          Flat = True
          FocusColor = clBtnFace
          LabelFont.Charset = DEFAULT_CHARSET
          LabelFont.Color = clWindowText
          LabelFont.Height = -11
          LabelFont.Name = 'Segoe UI'
          LabelFont.Style = []
          Lookup.Color = clBtnFace
          Lookup.Font.Charset = DEFAULT_CHARSET
          Lookup.Font.Color = clWindowText
          Lookup.Font.Height = -11
          Lookup.Font.Name = 'Arial'
          Lookup.Font.Style = []
          Lookup.Separator = ';'
          BorderStyle = bsNone
          Color = clBtnFace
          ParentColor = True
          TabOrder = 4
          Text = '1'
          Visible = True
          Version = '3.3.2.0'
        end
        object RadioGroupACT_Terminaux: TRadioGroup
          Left = 11
          Top = 159
          Width = 313
          Height = 50
          Caption = 'Terminaux'
          ItemIndex = 1
          Items.Strings = (
            'RT-Flex 10-30 cm'
            'Conduit rigide')
          TabOrder = 5
        end
      end
      object TabSheetActhysTrainasse: TTabSheet
        Caption = 'Trainasses / T2A'
        ImageIndex = 6
        object RadioGroupACT_LongueurBarreT2A: TRadioGroup
          Left = 11
          Top = 11
          Width = 313
          Height = 50
          Caption = 'Longueur de barre T2A'
          ItemIndex = 0
          Items.Strings = (
            '2m'
            '5m')
          TabOrder = 0
        end
      end
    end
  end
end
