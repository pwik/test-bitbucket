Unit VentilUpdate_Utils;

Interface
Uses
  SysUtils, Types, Forms, WinInet, ShellApi, Windows, Classes, Registry,
  Controls, TlHelp32,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdFTP;

Const
  Cst_Nouveautes = 'Nouveautes.txt';
  Cst_Dates      = 'Dates.txt';
{$IFDEF VIM_OPTAIR}
  Cst_UploadLog = False;
  CSt_NbElementsDispos = 5;

  //anciens param�tres de connexion
//  Cst_FTP        = {'88.191.25.49'; //}'ftp.bbs-slama.com';
//  Cst_Login      = 'optair';
//  Cst_Psswd      = 'h5s$9wx';

//param�tres avec permissions 555
  Cst_FTP        = {'88.191.25.49'; //}'www.optair.fr';
  Cst_Login      = 'UserOptairBBS';
  Cst_Psswd      = 'h5s$9wx';

//param�tres avec permissions 777
//  Cst_FTP        = {'88.191.25.49'; //}'www.optair.fr';
//  Cst_Login      = 'AdminOptairBBS';
//  Cst_Psswd      = 'ZQ7H17';

  Cst_ElementsTelechargeables : Array [1..CSt_NbElementsDispos, 1..3] Of String =
    (
     ('OPTAIR', 'OPTAIR.EXE', 'OPTAIR.ZIP'),
     ('DEFINITION DES BANQUES DE DONNEES', 'STRUCTURECATALOGUE.FDB', 'STRUCTURECATALOGUE.ZIP'),
     ('PRODUITS', 'BIBLIOTHEQUE\VENTIL_DATA.FDB', 'VENTIL_DATA.ZIP'),
     ('BIBLIOTHEQUE', 'BIBLIOTHEQUE\BIBLIOTHEQUE.FDB', 'BIBLIOTHEQUE.ZIP'),
     ('AIDE', 'hlp\Accueil.htm', 'HLP.ZIP')
    );
{$ENDIF}

{$IFDEF MVN_MVNAIR}
  Cst_UploadLog = True;
  CSt_NbElementsDispos = 5;

//param�tres avec permissions 555
  Cst_FTP        = 'ftp.bbs-slama.com';
  Cst_Login      = 'mvn';
  Cst_Psswd      = '6fgmfb';

  Cst_ElementsTelechargeables : Array [1..CSt_NbElementsDispos, 1..3] Of String =
    (
     ('MVNAIR', 'MVNAIR.EXE', 'MVNAIR.ZIP'),
     ('DEFINITION DES BANQUES DE DONNEES', 'STRUCTURECATALOGUE.FDB', 'STRUCTURECATALOGUE.ZIP'),
     ('PRODUITS', 'BIBLIOTHEQUE\VENTIL_DATA.FDB', 'VENTIL_DATA.ZIP'),
     ('BIBLIOTHEQUE', 'BIBLIOTHEQUE\BIBLIOTHEQUE.FDB', 'BIBLIOTHEQUE.ZIP'),
     ('AIDE', 'hlp\Accueil.htm', 'HLP.ZIP')
    );
{$ENDIF}


{$IFDEF ANJOS_OPTIMA3D}
  Cst_UploadLog = True;
  CSt_NbElementsDispos = 4;

  Cst_FTP        = 'ftp.bbs-slama.com';
  Cst_Login      = 'anjos';
  Cst_Psswd      = '6fgmfb';

  Cst_ElementsTelechargeables : Array [1..CSt_NbElementsDispos, 1..3] Of String =
    (
     ('OPTIMA3D', 'OPTIMA3D.EXE', 'OPTIMA3D.ZIP'),
     ('DEFINITION DES BANQUES DE DONNEES', 'STRUCTURECATALOGUE.FDB', 'STRUCTURECATALOGUE.ZIP'),
     ('PRODUITS', 'BIBLIOTHEQUE\VENTIL_DATA.FDB', 'VENTIL_DATA.ZIP'),
     ('BIBLIOTHEQUE', 'BIBLIOTHEQUE\BIBLIOTHEQUE.FDB', 'BIBLIOTHEQUE.ZIP')
    );
{$ENDIF}

{$IFDEF CALADAIR_VMC}
  Cst_UploadLog = True;
  CSt_NbElementsDispos = 4;

  Cst_FTP        = 'ftp.bbs-slama.com';
  Cst_Login      = 'caladair';
  Cst_Psswd      = '6fgmfb';

  Cst_ElementsTelechargeables : Array [1..CSt_NbElementsDispos, 1..3] Of String =
    (
     ('BATIVENT', 'BATIVENT.EXE', 'BATIVENT.ZIP'),
     ('DEFINITION DES BANQUES DE DONNEES', 'STRUCTURECATALOGUE.FDB', 'STRUCTURECATALOGUE.ZIP'),
     ('PRODUITS', 'BIBLIOTHEQUE\VENTIL_DATA.FDB', 'VENTIL_DATA.ZIP'),
     ('BIBLIOTHEQUE', 'BIBLIOTHEQUE\BIBLIOTHEQUE.FDB', 'BIBLIOTHEQUE.ZIP')
    );
{$ENDIF}

{$IFDEF LINDAB_VMC}
  Cst_UploadLog = True;
  CSt_NbElementsDispos = 4;

  Cst_FTP        = 'ftp.bbs-slama.com';
  Cst_Login      = 'lindab';
  Cst_Psswd      = '6fgmfb';

  Cst_ElementsTelechargeables : Array [1..CSt_NbElementsDispos, 1..3] Of String =
    (
     ('LINDAB VMC', 'LINDABVMC.EXE', 'LINDABVMC.ZIP'),
     ('DEFINITION DES BANQUES DE DONNEES', 'STRUCTURECATALOGUE.FDB', 'STRUCTURECATALOGUE.ZIP'),
     ('PRODUITS', 'BIBLIOTHEQUE\VENTIL_DATA.FDB', 'VENTIL_DATA.ZIP'),
     ('BIBLIOTHEQUE', 'BIBLIOTHEQUE\BIBLIOTHEQUE.FDB', 'BIBLIOTHEQUE.ZIP')
    );
{$ENDIF}

{$IFDEF ACTHYS_DIMVMBP}
  Cst_UploadLog = True;
  CSt_NbElementsDispos = 4;

  Cst_FTP        = 'ftp.bbs-slama.com';
  Cst_Login      = 'acthys';
  Cst_Psswd      = '6fgmfb';

  Cst_ElementsTelechargeables : Array [1..CSt_NbElementsDispos, 1..3] Of String =
    (
     ('DIMVMBP', 'DIMVMBP.EXE', 'DIMVMBP.ZIP'),
     ('DEFINITION DES BANQUES DE DONNEES', 'STRUCTURECATALOGUE.FDB', 'STRUCTURECATALOGUE.ZIP'),
     ('PRODUITS', 'BIBLIOTHEQUE\VENTIL_DATA.FDB', 'VENTIL_DATA.ZIP'),
     ('BIBLIOTHEQUE', 'BIBLIOTHEQUE\BIBLIOTHEQUE.FDB', 'BIBLIOTHEQUE.ZIP')
    );
{$ENDIF}

{$IFDEF FADIS_VMC}
  Cst_UploadLog = True;
  CSt_NbElementsDispos = 4;

  Cst_FTP        = 'ftp.bbs-slama.com';
  Cst_Login      = 'fadis';
  Cst_Psswd      = '6fgmfb';

  Cst_ElementsTelechargeables : Array [1..CSt_NbElementsDispos, 1..3] Of String =
    (
     ('FADIS VMC', 'FADISVMC.EXE', 'FADISVMC.ZIP'),
     ('DEFINITION DES BANQUES DE DONNEES', 'STRUCTURECATALOGUE.FDB', 'STRUCTURECATALOGUE.ZIP'),
     ('PRODUITS', 'BIBLIOTHEQUE\VENTIL_DATA.FDB', 'VENTIL_DATA.ZIP'),
     ('BIBLIOTHEQUE', 'BIBLIOTHEQUE\BIBLIOTHEQUE.FDB', 'BIBLIOTHEQUE.ZIP')
    );
{$ENDIF}

{$IFDEF FRANCEAIR_AIRGIVMC}
  Cst_UploadLog = True;
  CSt_NbElementsDispos = 4;

  Cst_FTP        = 'ftp.bbs-slama.com';
  Cst_Login      = 'franceair';
  Cst_Psswd      = '6fgmfb';

  Cst_ElementsTelechargeables : Array [1..CSt_NbElementsDispos, 1..3] Of String =
    (
     ('AIRGIVMC', 'AIRGIVMC.EXE', 'AIRGIVMC.ZIP'),
     ('DEFINITION DES BANQUES DE DONNEES', 'STRUCTURECATALOGUE.FDB', 'STRUCTURECATALOGUE.ZIP'),
     ('PRODUITS', 'BIBLIOTHEQUE\VENTIL_DATA.FDB', 'VENTIL_DATA.ZIP'),
     ('BIBLIOTHEQUE', 'BIBLIOTHEQUE\BIBLIOTHEQUE.FDB', 'BIBLIOTHEQUE.ZIP')
    );
{$ENDIF}

Type
  TObjet_Maj = Class
    Libelle    : String;
    NomDisque  : String;
    NomFTP     : String;                                                               
    DateDisque : TDate;
    DateFTP    : TDate;
    OutOfDate  : Boolean;
    Nouveautes : TStringList;
    Downloaded : Boolean;
    Constructor Create(_Libelle, _NomDisque, _NomFTP: String; _Infos, _Dates: TStringList; _DateDisque: TDate);
    Destructor Destroy; Override;
  Private

  End;

Function BbsRenameFile(_Source, _Destination: String): Boolean;
Function BbsCopyFile(_Source, _Destination: String): Boolean;
Function BbsDeleteFile(_Source: String): Word;
Procedure DetruitRepertoireComplet(_Rep : String);

Function Str2StringList(Chaine: WideString; Separateur: WideString): TStringList;
Function GetInfoElementTelechargeableFromFileName(_FileName : String; _Item : Integer) : String;


{ Connexions internet }
Function DetectionConnection:Boolean;
Function FichierSurDisque(_Fichier: String): Boolean;
Function WindowsTempRep: String;
Function SplitSpace(_2Split: String): TStringList;
Function GetProcessId(ProgName:string) : Cardinal;
Function KillProcess(ProgName:String) : Boolean;

{ <------ Pour le ftp }
Function  FTP_Online : Boolean;
Function  FTP_Connect(NFTP: TIDFTP; _ModeSSL: Boolean): Boolean;
Function CanFTP_Connect: Boolean;
Procedure FTP_DisConnect;
Function  FTP_FileList : Boolean;
Function  FTP_IndexFile(FileName: String): SmallInt;
Function  FTP_Download(FileName: String; Dest: String): Boolean;
Function  FTP_ReceivePacket(Sent : Integer; Total: Integer): String;
Function  FTP_InfosFichier(_ForFile: String):TStringList;
Function  FTP_String2Mois(_MoisChaine: String): String;
Function GetDateFTP(_NomFTP: String; _ListeDates: TstringList): TDate;
Function LoadLastDownloadDate(_File: String; _NomFTP: String; _Dates: TStringList; _UpdateHorsConnexion : Boolean = False): TDate;
Procedure SaveDownloadsDate(_File: String);
{ ------> }

Var
  FTP_Climawin : TIdFTP;
  FileList     : TStringList;

Implementation

{ ************************************************************************************************************************************************** }
Procedure SaveDownloadsDate(_File: String);
Var
  Registre : TRegistry;
  {$IfDef VIM_OPTAIR}
  Chemin : String ;
  {$EndIf}
Begin
  Registre:=Tregistry.Create;
  {$IfDef VIM_OPTAIR}
  Chemin := StringReplace(ExtractFilePath(Application.Exename), '\', '_', [rfReplaceAll]);
  Chemin := StringReplace(Chemin, '/', '_', [rfReplaceAll]);
  {$EndIf}
  Try
    Registre.RootKey:=HKEY_CURRENT_USER;
    If Registre.OpenKey('\Software\BBS Slama\VENTIL_DIM\Update' {$IfDef VIM_OPTAIR} + '\' + Chemin{$EndIf},True) Then Begin
      Registre.WriteDate(ExtractFileName(_File), Now);
      Registre.CloseKey;
    End;
  Except End;
  FreeAndNil(Registre);
End;
{ ************************************************************************************************************************************************** }
Function LoadLastDownloadDate(_File: String; _NomFTP: String; _Dates: TStringList; _UpdateHorsConnexion : Boolean = False): TDate;
Var
  Registre : TRegistry;
  {$IfDef VIM_OPTAIR}
  Chemin : String ;
  {$EndIf}
Begin

if _UpdateHorsConnexion then
        Begin
        Result := 0;
        Exit;
        End;

  Registre:= Tregistry.Create;
  Result  := GetDateFTP(_NomFTP, _Dates) - 1;
  {$IfDef VIM_OPTAIR}
  Chemin := StringReplace(ExtractFilePath(Application.Exename), '\', '_', [rfReplaceAll]);
  Chemin := StringReplace(Chemin, '/', '_', [rfReplaceAll]);
  {$EndIf}
  Try
    Registre.RootKey:=HKEY_CURRENT_USER;
    If Registre.OpenKey('\Software\BBS Slama\VENTIL_DIM\Update\'  {$IfDef VIM_OPTAIR} + Chemin + '\' {$EndIf},True) Then Begin
      Result := Registre.ReadDate(ExtractFileName(_File));
      Registre.CloseKey;
    End;
  Except
    If Registre.OpenKey('\Software\BBS Slama\VENTIL_DIM\Update' {$IfDef VIM_OPTAIR} + '\' + Chemin{$EndIf},True) Then Begin
      Registre.WriteDate(ExtractFileName(_File), Result);
      Registre.CloseKey;
    End;
  End;
  Try
    If Uppercase(ExtractFileExt(_File)) = UpperCase('.EXE') Then Result := FileDateToDateTime(FileAge(_File));
  Except
//    Result := Now - 1;
    Result := 0;
  End;
  FreeAndNil(Registre);
End;
{ ************************************************************************************************************************************************** }
Function BbsRenameFile(_Source, _Destination: String): Boolean;
Var
  fos : TSHFileOpStruct;
Begin
  FillChar(fos, SizeOf(fos), 0);
  With fos Do Begin
    wFunc := FO_RENAME;
    pFrom := PChar(_Source + #0);
    pTO   := PChar(_Destination + #0);
    fFlags:= FO_RENAME Or FOF_SILENT;
  End;
  Result := (0 = ShFileOperation(fos));
End;
{ ************************************************************************************************************************************************** }
Function BbsCopyFile(_Source, _Destination: String): Boolean;
Var
  fos : TSHFileOpStruct;
Begin
  FillChar(fos, SizeOf(fos), 0);
  With fos Do Begin
    wFunc := FO_COPY;
    pFrom := PChar(_Source + #0);
    pTO   := PChar(_Destination + #0);
    fFlags:= FOF_NOCONFIRMATION + FOF_SILENT;
  End;
  Result := (0 = ShFileOperation(fos));
End;
{ ************************************************************************************************************************************************** }
Function BbsDeleteFile(_Source: String): Word;
Var
  fos : TSHFileOpStruct;
Begin
  FillChar(fos, SizeOf(fos), 0);
  With fos Do Begin
    wFunc := FO_DELETE;
    pFrom := PChar(_Source + #0);
    fFlags:= FO_DELETE Or FOF_NOCONFIRMATION Or FOF_SILENT;
  End;
  Result := ShFileOperation(fos);
End;
{ ************************************************************************************************************************************************** }
Procedure DetruitRepertoireComplet(_Rep : String);
Var
  sr: TSearchRec;
Begin
  If FindFirst(IncludeTRailingPathDelimiter(_Rep) + '*.*', faAnyFile, sr) = 0 Then Begin
    Repeat
       BBSDeleteFile(IncludeTRailingPathDelimiter(_Rep) + sr.Name);
    Until FindNext(sr) <> 0;
    SysUtils.FindClose(sr);
    RemoveDir(_rep);
  End;
End;
{ ************************************************************************************************************************************************** }
Function Str2StringList(Chaine: WideString; Separateur: WideString): TStringList;
Var
  Indice  : Integer;
  Lst     : TStringList;
  StrTemp : WideString;
Begin
  StrTemp := Chaine;
  Lst := TStringList.Create;
  If Chaine[Length(StrTemp)] <> Separateur Then StrTemp := StrTemp + Separateur;
  While Pos(Separateur, StrTemp) <> 0 Do
  Begin
    Indice := Pos(Separateur, StrTemp);
    Lst.Add(Copy(StrTemp, 0, Indice - 1));
    StrTemp := Copy(StrTemp, Indice + 1, Length(StrTemp));
  End;
  Result := Lst;
End;
{ ************************************************************************************************************************************************** }
Function GetInfoElementTelechargeableFromFileName(_FileName : String; _Item : Integer) : String;
var
        Elt : Integer;
Begin
Result := '';
        For Elt := 1 To CSt_NbElementsDispos Do
                Begin
                        If UpperCase(Cst_ElementsTelechargeables[Elt, 3]) = ExtractFileName(UpperCase(_FileName)) then
                                Begin
                                Result := Cst_ElementsTelechargeables[Elt, _Item];
                                Exit;
                                End;
                End;
End;
{ ************************************************************************************************************************************************** }
Function DetectionConnection:Boolean;
Var DwFlags: DWord;
    ResultConnection,
    ResultAppel:Boolean;
Begin
   DwFlags:= Internet_Connection_Modem + Internet_Connection_Lan + Internet_Connection_Proxy;
   ResultConnection := InternetGetConnectedState(@DwFlags,0);
   If Not ResultConnection Then Begin
     ResultAppel:= InternetAutodial(0,Application.Handle);
     If ResultAppel Then ResultConnection := InternetGetConnectedState(@DwFlags,0);
   End;
   DetectionConnection:=ResultConnection;
End;
{ **************************************************************************** }
Function FichierSurDisque(_Fichier: String): Boolean;
Var
  sr: TSearchRec;
Begin
  Result := FindFirst(_Fichier, faAnyFile - faDirectory, sr) = 0;
End;
{ ************************************************************************************************************************************************** }
Function FTP_Connect(NFTP: TIDFTP; _ModeSSL: Boolean): Boolean;
Var
  NetDispo : Boolean;
Begin
  FTP_Climawin := NFTP;
  If FTP_Climawin.Connected Then Result := True
  Else Begin
    NetDispo := DetectionConnection;
    Result := NetDispo;
    If NetDispo Then Begin
     FTP_Climawin.Username := Cst_Login;
     FTP_Climawin.Password := Cst_Psswd;
     FTP_Climawin.Host     := Cst_FTP;
     FTP_Climawin.Passive  := True;
     Try
     FTP_Climawin.Connect;
     Except
       Result := False;
     End;
    End;
  End;
End;
{ **************************************************************************** }
Function CanFTP_Connect: Boolean;
Begin
  Result := False;
  If DetectionConnection Then Begin
    FTP_Climawin := TIdFTP.Create(nil);
    FTP_Climawin.Username := Cst_Login;
    FTP_Climawin.Password := Cst_Psswd;
    FTP_Climawin.Host := cst_FTP;
    FTP_Climawin.Passive := True;
    Result := True;
    Try FTP_Climawin.Connect; Except result := False; End;
  End;
End;
{ ************************************************************************************************************************************************** }
Function  FTP_Online : Boolean;
Begin
  Result := (FTP_Climawin <> Nil) And (FTP_Climawin.Connected);
End;
{ ************************************************************************************************************************************************** }
Procedure FTP_DisConnect;
Begin
  If FTP_Online Then FTP_Climawin.Disconnect;
End;
{ ************************************************************************************************************************************************** }
Function FTP_FileList: Boolean;
Begin
  Result := False;
  If FTP_Online Then Begin
    FileList := TStringList.Create;
    FTP_Climawin.List(FileList, '*.zip', False);
    Result := FileList.Count > 0;
  End;
End;
{ ************************************************************************************************************************************************** }
Function  FTP_IndexFile(FileName: String): SmallInt;
Var
  Idx : SmallInt;
  F1  : String;
  F2  : String;
Begin
  Result := - 1;
  F1  := UpperCase(FileName);
  If FTP_Online And (FileList <> Nil) Then
    For Idx := 0 To FileList.Count - 1 Do
    Begin
      F2 := UpperCase(FileList[Idx]);
      If F1 = F2 Then
      Begin
        Result := Idx;
        Break;
      End;
    End;
End;
{ ************************************************************************************************************************************************** }
Function  FTP_Download(FileName: String; Dest: String): Boolean;
Begin
  If FTP_Online Then
        begin
        FTP_Climawin.Get(FileName, Dest + Filename, True);
        end;
  Result := True;
End;
{ ************************************************************************************************************************************************** }
Function FTP_ReceivePacket(Sent : Integer; Total: Integer): String;
Begin
 Result := IntToStr(Sent) + ' ko sur ' + IntToStr(Total) + ' ko transf�r�s';
End;
{ ************************************************************************************************************************************************** }
Function  FTP_InfosFichier(_ForFile: String):TStringList;
Var
  m_Stl   : TStringList;
  m_NoFic : Integer;
Begin
  m_Stl  := TStringList.Create;
  Result := m_Stl;
  For m_NoFic := 0 To FileList.Count - 1 Do Begin
    m_Stl := SplitSpace(FileList[m_NoFic]);
//ancien code en delphi 6... pourquoi???
//    If UpperCase(m_Stl[8]) = UpperCase(_ForFile) Then Begin
    If UpperCase(m_Stl[0]) = UpperCase(_ForFile) Then Begin
      Result := m_Stl;
      Break;
    End;
  End;
End;
{ ************************************************************************************************************************************************** }
Function  FTP_String2Mois(_MoisChaine: String): String;
Const Cst_Mois : Array[1..12] Of String = ('JAN', 'FEB', 'MAR',
                                           'APR', 'MAY', 'JUN',
                                           'JUL', 'AUG', 'SEP',
                                           'OCT', 'NOV', 'DEC');
Var
  m_No : Integer;
  m_Str: String;
Begin
  m_Str := '01';
  For m_No := 1 To 12 Do If Cst_Mois[m_No] = UpperCase(_MoisChaine) Then Begin
    m_Str := IntToStr(m_No);
    If length(m_Str) = 1 Then m_Str := '0' + m_Str;
  End;
  Result := m_Str;
End;
{ ************************************************************************************************************************************************** }
{ R�pertoire temporaire de windows }
Function WindowsTempRep: String;
Var
  TempDir : Array[0..MAX_PATH] Of AnsiChar;
  nSize   : DWord;
Begin
  nSize := SizeOf(TempDir);
  GetTempPathA(nSize, @TempDir);
  result := TempDir;
End;
{ ************************************************************************************************************************************************** }
Function SplitSpace(_2Split: String): TStringList;
Var
  Indice  : Integer;
  Lst     : TStringList;
  StrTemp : WideString;
  InSpace : Boolean;
Begin
  Lst := TStringList.Create;
  Indice := 1;
  StrTemp := '';
  InSpace := False;
  While Indice <= Length(_2Split) Do Begin
    If _2Split[Indice] <> ' ' Then begin
      InSpace := False;
      StrTemp := StrTemp + _2Split[Indice];
    End Else Begin
      If Not InSpace Then Begin
        Lst.Add(StrTemp);
        StrTemp := '';
      End;
      InSpace := True;
    End;
    Inc(Indice);
  End;
  Lst.Add(StrTemp);
  Result := Lst;
End;
{ ************************************************************************************************************************************************** }
//fonction qui retourne l'id d'un process fournit en parametre
Function GetProcessId(ProgName:string):cardinal;
Var  Snaph:thandle;
     Proc:tprocessentry32;
     PId:cardinal;
Begin
  PId:=0;
  Proc.dwSize:=sizeof(Proc);
  Snaph:=createtoolhelp32snapshot(TH32CS_SNAPALL,0);  //recupere un capture de process
  process32first(Snaph,Proc);  //premier process de la list
  if UpperCase(extractfilename(Proc.szExeFile))=UpperCase(ProgName) then  //test pour savoir si le process correspond
     PId:=Proc.th32ProcessID // recupere l'id du process
  else
    begin
      while process32next(Snaph,Proc) do  //dans le cas contraire du test on continue � cherche le process en question
        begin
           if UpperCase(extractfilename(Proc.szExeFile))=UpperCase(ProgName) then
              PId:=Proc.th32ProcessID;
        end;
   end;
  Closehandle(Snaph);
  Result:=PId;
End;
{ ************************************************************************************************************************************************** }
Function KillProcess(ProgName:String): Boolean;
Var
  Proch : THandle;
  PId   : Cardinal;
Begin
  PId    := GetProcessId(ProgName);
  Proch  := OpenProcess(PROCESS_ALL_ACCESS ,true,PId); //handle du process
  Result :=  terminateprocess(Proch,PId);
  Closehandle(Proch);
End;
{ ************************************************************************************************************************************************** }

{ TObjet_Maj }
constructor TObjet_Maj.Create(_Libelle, _NomDisque, _NomFTP: String; _Infos, _Dates: TStringList; _DateDisque: TDate);
Var
  m_Chemin : String;
begin
  m_Chemin  := ExtractFilePath(Application.Exename);
  Libelle   := _Libelle;
  NomDisque := _NomDisque;
  NomFTP    := _NomFTP;
  Nouveautes := TStringList.Create;
  DateDisque := _DateDisque;
  DateFTP    := GetDateFTP(NomFTP, _Dates);// StrToDate(FTP_String2Mois( _Infos[5]) + '/' + _Infos[6]);
  Downloaded := False; 
end;
{ ************************************************************************************************************************************************** }
destructor TObjet_Maj.Destroy;
begin
  FreeAndNil(Nouveautes);
  inherited;
end;
{ ************************************************************************************************************************************************** }
Function GetDateFTP(_NomFTP: String; _ListeDates: TstringList): TDate;
Var
  m_No : Integer;
  m_Stl: TstringList;
Begin
  Result := Now;
  For m_No := 0 To _ListeDates.Count - 1 Do Begin
    m_Stl := Str2StringList(_ListeDates[m_No], '@');
    If UpperCase(m_Stl[0]) = _NomFTP Then Begin
      Result := StrToDate(m_Stl[1]);
      Break;
    End;
  End;
  FreeAndNil(m_Stl);
End;
{ ************************************************************************************************************************************************** }

End.
