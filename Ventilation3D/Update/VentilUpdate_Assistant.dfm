object Form1: TForm1
  Left = 535
  Top = 277
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'T'#233'l'#233'chargement des mises '#224' jour'
  ClientHeight = 103
  ClientWidth = 569
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 208
    Top = 80
    Width = 3
    Height = 13
  end
  object Memo1: TMemo
    Left = 8
    Top = 8
    Width = 185
    Height = 89
    BorderStyle = bsNone
    Color = clBtnFace
    Enabled = False
    TabOrder = 0
  end
  object Progress: TProgressBar
    Left = 208
    Top = 40
    Width = 353
    Height = 25
    Smooth = True
    TabOrder = 1
  end
  object FTP1: TIdFTP
    OnWork = FTP1Work
    OnWorkBegin = FTP1WorkBegin
    OnWorkEnd = FTP1WorkEnd
    IPVersion = Id_IPv4
    Passive = True
    NATKeepAlive.UseKeepAlive = False
    NATKeepAlive.IdleTimeMS = 0
    NATKeepAlive.IntervalMS = 0
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    Left = 336
    Top = 8
  end
end
