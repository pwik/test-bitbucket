unit Unit1;


interface

uses
  CAD_Main, Commun, MesClasses, ImageEx,
  Windows, Messages, Types, Dialogs, Forms, ExtCtrls, Controls, StdCtrls, editlist, Classes, Menus, ComCtrls, Graphics,
  HTMLabel, htmlhint,
  GDIPCustomItem, CustomItemsContainer, AdvHorizontalPolyList, AdvShape, AdvPanel, AdvSmoothPanel,
  AdvOfficeTabSet, AdvOfficeTabSetStylers, AdvOfficePager, AdvOfficePagerStylers, AdvGrid, AdvObj,
  AdvGlassButton,
  AdvMenus, AdvMenuStylers, 
  
  ceflib, cefvcl, jpeg, ImgList, Grids, BaseGrid, pngimage;

const
  WM_SHOWASYNCH = WM_USER + 1;

type
  TFormCuisiNote = class(TForm)
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    ImageList1: TImageList;
    AdvSmoothPanel2: TAdvSmoothPanel;
    AdvSmoothPanel3: TAdvSmoothPanel;
    AdvShape1: TAdvShape;
    AdvSmoothPanel_SubLocal: TAdvSmoothPanel;
    ImageEx_Projet: TImageEx;
    ImageEx_Ventilation: TImageEx;
    ImageEx_Local: TImageEx;
    ImageEx_Piece: TImageEx;
    ImageEx_Cuisson: TImageEx;
    ImageEx_Hottes: TImageEx;
    ImageEx_Diffuseurs: TImageEx;
    ImageEx_Open: TImageEx;
    ImageEx_Save: TImageEx;
    ImageEx_Exit: TImageEx;
    ImageFA: TImage;
    AdvSmoothPanel_Top: TAdvSmoothPanel;
    AdvSmoothPanel_Local_CAD: TAdvSmoothPanel;
    LOCAL_PanelCAD: TPanel;
    LOCAL_AdvHorizontalPolyListElts: TAdvHorizontalPolyList;
    AdvSmoothPanel_Projet: TAdvSmoothPanel;
    PageControl: TPageControl;
    TabSheet_Projet: TTabSheet;
    TabSheet_Local: TTabSheet;
    LOCAL_AdvPanelGroupRight: TAdvPanelGroup;
    AdvSmoothPanel_Local_Carac: TAdvSmoothPanel;
    LOCAL_PanelCaracteristiques: TPanel;
    LOCAL_LabelCaracteristiques: TLabel;
    TabSheet_Reseaux: TTabSheet;
    ImageEx_LocalPrev: TImageEx;
    ImageEx_LocalSuiv: TImageEx;
    AdvSmoothPanelToolbar: TAdvSmoothPanel;
    AdvSmoothPanel_Reseaux: TAdvSmoothPanel;
    ImageEx_Recent: TImageEx;
    ImageEx_SaveAs: TImageEx;
    ImageEx_CAD_Select: TImageEx;
    ImageEx_CAD_Rotate: TImageEx;
    ImageEx_CAD_ZoomIn: TImageEx;
    ImageEx_CAD_ZoomOut: TImageEx;
    ImageEx_CAD_Pan: TImageEx;
    ImageEx_CAD_Arbo: TImageEx;
    ImageEx_Undo: TImageEx;
    ImageEx_CAD_ZoomAll: TImageEx;
    Panel1: TPanel;
    Panel2: TPanel;
    ImageExCollaps: TImageEx;
    ImageExUnCollaps: TImageEx;
    FontDialogMurs: TFontDialog;
    FontDialogObjets: TFontDialog;
    ImageEx_CAD_Params: TImageEx;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Panel3: TPanel;
    LabelPROJET: TLabel;
    Label1: TLabel;
    Label15: TLabel;
    Label11: TLabel;
    Projet_Nom: TEdit;
    Projet_Lieu: TEdit;
    Projet_Locaux: TEditListBox;
    Projet_Locaux_Add: TImageEx;
    Projet_Locaux_Del: TImageEx;
    Projet_Locaux_Dup: TImageEx;
    Panel4: TPanel;
    Label7: TLabel;
    Redacteur_Interlocuteur: TEdit;
    HTMLabel3: THTMLabel;
    HTMLabel4: THTMLabel;
    Redacteur_Entreprise: TEdit;
    Redacteur_Departement: TEdit;
    Redacteur_Commune: TEdit;
    Destinataire_Entreprise: TEdit;
    Destinataire_Departement: TEdit;
    Destinataire_Commune: TEdit;
    Destinataire_Interlocuteur: TEdit;
    ImageEx_CAD_23D: TImageEx;
    Label37: TLabel;
    AdvShape8: TAdvShape;
    AdvShape3: TAdvShape;
    Label38: TLabel;
    HTMLabel1: THTMLabel;
    Label20: TLabel;
    Label21: TLabel;
    Meteo_Altitude: TEdit;
    HTMLabel5: THTMLabel;
    Meteo_Zone: TEdit;
    Projet_Dep: TComboBox;
    HTMLHint1: THTMLHint;
    ImageEx_RESEAUX_Add: TImageEx;
    AdvOfficePager_Reseau: TAdvOfficePager;
    AdvOfficePage_Reseau_Recup: TAdvOfficePage;
    AdvOfficePage_Reseau_Ventilo: TAdvOfficePage;
    AdvOfficePage_Reseau_Ilots: TAdvOfficePage;
    AdvOfficePagerOfficeStyler2: TAdvOfficePagerOfficeStyler;
    Label_Reseau23: TLabel;
    ComboBox_Recup: TComboBox;
    Label_Reseau8: TLabel;
    AdvOfficeTabSet_Reseaux: TAdvOfficeTabSet;
    AdvOfficeTabSetOfficeStyler1: TAdvOfficeTabSetOfficeStyler;
    AdvSmoothPanel1: TAdvSmoothPanel;
    AdvSmoothPanelIlots: TAdvSmoothPanel;
    AdvSmoothPanel5: TAdvSmoothPanel;
    AdvSmoothPanel6: TAdvSmoothPanel;
    Grid_Ilots: TAdvStringGrid;
    AdvStringGrid_Ventilo: TAdvStringGrid;
    Label3: TLabel;
    Edit_Ventilo_PdCclient: TEdit;
    CheckBox_Ventilo_Isole: TCheckBox;
    Label8: TLabel;
    ComboBox_Ventilo_MonoBi: TComboBox;
    HTMLabel6: THTMLabel;
    HTMLabel7: THTMLabel;
    CheckBox_Ventilo_HTMLabel: THTMLabel;
    AdvOfficePage_Reseau_Conso: TAdvOfficePage;
    AdvSmoothPanel7: TAdvSmoothPanel;
    HTMLabel9: THTMLabel;
    AdvOfficePage_Reseau_Syno: TAdvOfficePage;
    AdvSmoothPanel8: TAdvSmoothPanel;
    PaintBox_Synopt: TPaintBox;
    AdvSmoothPanel9: TAdvSmoothPanel;
    Label5: TLabel;
    Label42: TLabel;
    Label19: TLabel;
    Label18: TLabel;
    Label17: TLabel;
    Label16: TLabel;
    Label14: TLabel;
    Label12: TLabel;
    Label10: TLabel;
    HTMLabel2: THTMLabel;
    AdvPopupMenu1: TAdvPopupMenu;
    AdvMenuOfficeStyler1: TAdvMenuOfficeStyler;
    ComboBoxVentilo: TAdvSmoothPanel;
    PopupMenuRecent: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    PopupMenuParamsCAD: TAdvPopupMenu;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    ComboBoxPages: TAdvSmoothPanel;
    AdvPopupMenu2: TAdvPopupMenu;
    Fonctionnement_Hivernal_Du: TComboBox;
    Fonctionnement_Hivernal_Au: TComboBox;
    ImageRecup: TImage;
    Fonctionnement_Annuel: TEdit;
    Fonctionnement_Hebdomadaire: TEdit;
    Fonctionnement_Journalier_De: TEdit;
    Fonctionnement_Journalier_A: TEdit;
    Fonctionnement_VacancesHiver: TEdit;
    ImageEx_New: TImageEx;
    Panel_Koox: TPanel;
    Label_Reseau22: TLabel;
    Panel_Lago: TPanel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Edit_TCEauSortie: TEdit;
    Edit_BesoinsEC: TEdit;
    Edit_Pompe: TCheckBox;
    Label13: TLabel;
    Panel5: TPanel;
    Panel6: TPanel;
    Label25: TLabel;
    Combo_TAirExtrait_Lago: TComboBox;
    Combo_TCEauDeVille: TComboBox;
    Panel_Actinys: TPanel;
    Label36: TLabel;
    Combo_Actinys_Ilot: TComboBox;
    Label30: TLabel;
    Meteo_TCminHiver: TEdit;
    Label9: TLabel;
    Projet_Date: TDateTimePicker;
    Label35: TLabel;
    HTMLabel8: THTMLabel;
    LOCAL_ScrollBox1: TScrollBox;
    Label6: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Lago_Journalier_De: TEdit;
    Lago_Journalier_A: TEdit;
    Label41: TLabel;
    Lago_Annuel: TEdit;
    ScrollBox1: TScrollBox;
    Label_Reseau6: TLabel;
    ScrollBox2: TScrollBox;
    Panel8: TPanel;
    Fonctionnement_HTMLabel: THTMLabel;
    ImageExApropos: TImageEx;
    Chromium1 : TChromium;
    Label2: TLabel;
    Projet_RSD_Nbr: TEdit;
    Label43: TLabel;
    Projet_RSD_Debit: TEdit;
    Label44: TLabel;
    Projet_RSD_Relais: TCheckBox;
    HTMLabel11: THTMLabel;
    Label45: TLabel;
    ImageEx_Recapitulatif: TImageEx;
    Panel7: TPanel;
    HTMLabel_Recup: THTMLabel;
    Commentaires: TMemo;
    Projet_Locaux_Aide: TImageEx;
    HTMLabel10: THTMLabel;
    ImageLogo: TImage;
    Combo_TAmbiante: TComboBox;
    Combo_TAirExtrait_KooX: TComboBox;
    Combo_Humidite: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure PanelToolbarResize(Sender: TObject);
    procedure LOCAL_AdvPanelGroupRightMouseLeave(Sender: TObject);
    procedure LOCAL_AdvPanelGroupRightMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure ImageEx_CAD_ArboClick(Sender: TObject);
    procedure ImageEx_CAD_SelectClick(Sender: TObject);
    procedure ImageEx_CAD_RotateClick(Sender: TObject);
    procedure ImageEx_CAD_PanClick(Sender: TObject);
    procedure ImageEx_CAD_ZoomInClick(Sender: TObject);
    procedure ImageEx_CAD_ZoomOutClick(Sender: TObject);
    procedure ImageEx_CAD_ZoomAllClick(Sender: TObject);
    procedure ImageEx_CAD_ParamsClick(Sender: TObject);
    procedure ImageEx_OpenClick(Sender: TObject);
    procedure ImageEx_RecentClick(Sender: TObject);
    procedure ImageEx_SaveClick(Sender: TObject);
    procedure ImageEx_SaveAsClick(Sender: TObject);
    procedure ImageEx_ProjetClick(Sender: TObject);
    procedure ImageEx_LocalClick(Sender: TObject);
    procedure ImageEx_ConsommationClick(Sender: TObject);
    procedure ImageEx_SynoptiqueClick(Sender: TObject);
    procedure ImageEx_PieceClick(Sender: TObject);
    procedure ImageEx_CuissonClick(Sender: TObject);
    procedure ImageEx_HottesClick(Sender: TObject);
    procedure ImageEx_DiffuseursClick(Sender: TObject);
    procedure ImageEx_LocalPrevClick(Sender: TObject);
    procedure ImageEx_LocalSuivClick(Sender: TObject);
    procedure ImageEx_ExitClick(Sender: TObject);
    procedure FontDialogMursApply(Sender: TObject; Wnd: HWND);
    procedure ImageExCollapsClick(Sender: TObject);
    procedure Policedecotationdesmurs1Click(Sender: TObject);
    procedure Policedecotationdesobjets1Click(Sender: TObject);
    procedure ImageEx_CAD_23DClick(Sender: TObject);
    procedure ImageEx_RESEAUX_AddClick(Sender: TObject);
    procedure FontDialogMursClose(Sender: TObject);
    procedure AdvOfficePager_ReseauResize(Sender: TObject);
    procedure AdvOfficeTabSet_ReseauxTabClose(Sender: TObject; TabIndex: Integer; var Allow: Boolean);
    procedure AdvOfficeTabSet_ReseauxChange(Sender: TObject);
    procedure AdvOfficePager_ReseauChange(Sender: TObject);
    procedure LOCAL_AdvHorizontalPolyListEltsMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure LOCAL_AdvHorizontalPolyListEltsMouseEnter(Sender: TObject);
    procedure Grid_IlotsGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure Grid_IlotsCanClickCell(Sender: TObject; ARow, ACol: Integer; var Allow: Boolean);
    procedure ImageEx_VentilationClick(Sender: TObject);
    procedure ComboBoxVentiloClick(Sender: TObject);
    procedure ImageEx_RecapitulatifClick(Sender: TObject);
    procedure ImageEx_NewClick(Sender: TObject);
    procedure ImageFAClick(Sender: TObject);
    procedure ImageExAproposClick(Sender: TObject);
    procedure LOCAL_AdvHorizontalPolyListEltsItemReorder(Sender: TObject; AItem, ADropItem: TCustomItem; var Allow: Boolean);
    procedure LOCAL_AdvHorizontalPolyListEltsItemSelect(Sender: TObject; Item: TCustomItem; var Allow: Boolean);
//DCEF3    procedure Chromium1BeforeResourceLoad(Sender: TObject; const browser: ICefBrowser; const frame: ICefFrame; const request: ICefRequest; out Result: Boolean);
    procedure Chromium1BeforeResourceLoad(Sender: TObject; const browser: ICefBrowser; const request: ICefRequest; var redirectUrl: ustring; var resourceStream: ICefStreamReader; const response: ICefResponse; loadFlags: Integer; out Result: Boolean);
    procedure Chromium1BeforeMenu(Sender: TObject; const browser: ICefBrowser; const menuInfo: PCefMenuInfo; out Result: Boolean);
    procedure Projet_Locaux_AideClick(Sender: TObject);
  private
    FProjet : TCAD_Projet;
    FOldMode : TMode;
    FFichierCourant : string;
    FLockCount : integer;
    FFichiersRecents : TStringList;
    FSaved : boolean;
    FCuisiNoteRoot : TCAD_CuisiNoteRoot;
    FormCAD : TFormCAD;
    FDefaults : record
      Projet : record
        Redacteur_Entreprise,
        Redacteur_Departement,
        Redacteur_Commune,
        Redacteur_Interlocuteur : string;
      end;
    end;

    procedure Lock;
    procedure UnLock;
    procedure RefreshLock;
    function ChangeMode(Mode : TMode; Sender : TObject = nil; ModeFrom : TMode = mStartBegin; SousMode : TMode = mStartBegin; Root : TObject = nil) : TModeSortie;
    procedure SetFichierCourant(Valeur : string);
    procedure New;
    function Save(Fichier : string) : boolean;
    function Load(Fichier : string) : boolean;
    procedure AddRecent(Fichier : string);
    procedure RecentClick(Sender : TObject);
    procedure OpenFile(Fichier : string);
    procedure MouseLeaveCAD(Sender : TObject);
    procedure AdvGlassButtonMajLocal;
    procedure LinkProjet(Projet : TCAD_Projet);
    procedure CreatePDF;
    procedure CreatePDF_CallbackDessin(Tag : integer; DC : HDC; Left, Right, Top, Bottom : integer);

    procedure ShowAsynch(var Msg : TMessage); message WM_SHOWASYNCH;

    property FichierCourant : string read FFichierCourant write SetFichierCourant;
  protected
  public
    IsDestroying : boolean;
    property Projet : TCAD_Projet read FProjet;
  end;

var
  FormCuisiNote : TFormCuisiNote;


implementation
{$R *.dfm}

uses
//  EEurekalog_Vilo, ExceptionLog, ETypes, eCore,
  AdvGDIPicture,
  Variants,
  VPE,
  ShellApi,
  About,
  LoginEspacePro,
  ALXmlDoc,
  SysUtils,
  CAD_Objets_General, GLColor,
  BBS_Progress,
  CAD_Commun, CAD_Objets_Interfaces, CAD_ViloUtils, 
  DLL, XLS;


//#####################################################################################################################################
//# TForm1                                                                                                                            #
//#####################################################################################################################################
//*************************************************************************************************************************************
procedure TFormCuisiNote.FormCreate(Sender: TObject);
var i : integer;
begin
  vImageLogo := ImageLogo;
  ALXMLDoc_CaseSensitiveNodeSearch := false;
  IsDestroying := false;
  Application.ShowMainForm := false;
  FormCAD := nil;
  FProjet := nil;
  FFichiersRecents := nil;
  FOldMode := mPDF;
  FLockCount := 0;
  vAppPath := ExtractFilePath(Application.ExeName);

  HTMLabel_Recup.WantSpacingBeforeFirstLine := false;
  Fonctionnement_HTMLabel.WantSpacingBeforeFirstLine := false;
  
  LOCAL_AdvHorizontalPolyListElts.List.Appearance.Normal.BorderColor := RGB(11, 141, 181);
  LOCAL_AdvHorizontalPolyListElts.List.Appearance.Selected.Assign(LOCAL_AdvHorizontalPolyListElts.List.Appearance.Normal);
  LOCAL_AdvHorizontalPolyListElts.List.Appearance.Down.Assign(LOCAL_AdvHorizontalPolyListElts.List.Appearance.Normal);
  LOCAL_AdvHorizontalPolyListElts.List.Appearance.Hovered.Assign(LOCAL_AdvHorizontalPolyListElts.List.Appearance.Normal);
  LOCAL_AdvHorizontalPolyListElts.List.Appearance.Hovered.BorderColor := RGB(255, 128, 0);

  LOCAL_AdvHorizontalPolyListElts.List.Appearance.NormalFont.Assign(Font);
  LOCAL_AdvHorizontalPolyListElts.List.Appearance.NormalFont.Size := 7;
  LOCAL_AdvHorizontalPolyListElts.List.Appearance.SelectedFont.Assign(LOCAL_AdvHorizontalPolyListElts.List.Appearance.NormalFont);
  LOCAL_AdvHorizontalPolyListElts.List.Appearance.DownFont.Assign(LOCAL_AdvHorizontalPolyListElts.List.Appearance.NormalFont);
  LOCAL_AdvHorizontalPolyListElts.List.Appearance.HoveredFont.Assign(LOCAL_AdvHorizontalPolyListElts.List.Appearance.NormalFont);

  if DelphiRuning then begin
    ImageFA.Cursor := crHandPoint;
    ImageFA.OnClick := ImageFAClick;
  end;

  Chromium1.Browser.MainFrame.LoadUrl(cURL_PUB);
  ChangeMode(mStartBegin);

  for i:=0 to PageControl.PageCount-1 do PageControl.Pages[i].TabVisible := false;
  PageControl.ActivePage := TabSheet_Projet;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.FormShow(Sender: TObject);
begin
  PostMessage(Handle, WM_SHOWASYNCH, 0, 0);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ShowAsynch(var Msg : TMessage);
begin
  ChangeMode(mCommandLine);
  Projet_Nom.SetFocus;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := (FLockCount = 0) and (ChangeMode(mSaveCurrent) <> cmAbort);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ChangeMode(mDestroyBegin);
  Action := caFree;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_NewClick(Sender: TObject);
begin
  if (FLockCount = 0) and (ChangeMode(mSaveCurrent) <> cmAbort) then begin
    New;
    ChangeMode(mProjet);
    FProjet.ToForm;
  end;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_OpenClick(Sender: TObject);
begin
  if (FLockCount = 0) and (ChangeMode(mSaveCurrent) <> cmAbort) then begin
    OpenDialog1.Filter := 'Etude ' + cLogiciel + '|*.' + cExtension;
    if OpenDialog1.Execute then begin
      if (FileExtension(OpenDialog1.FileName) = cExtension) then begin
        OpenFile(OpenDialog1.FileName);
      end else begin
        AfficheMessage('Le fichier<br>' + '<b><u>' + ExtractFileName(OpenDialog1.FileName) + '</b></u><br>' + 'n''est pas une �tude ' + cLogiciel, MB_OK + MB_ICONERROR);
      end;
    end;
  end;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_RecentClick(Sender: TObject);
var P : TPoint;
begin
  P := TControl(Sender).ClientToScreen(Point(0, TControl(Sender).Height));
  PopupMenuRecent.Popup(P.X, P.Y);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_SaveClick(Sender: TObject);
begin
  if FileExists(FichierCourant) then Save(FichierCourant)
                                else ImageEx_SaveAs.OnClick(nil);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_SaveAsClick(Sender: TObject);
begin
  SaveDialog1.Filter := 'Etude ' + cLogiciel + '|*.' + cExtension;
  SaveDialog1.FileName := String2Filename(FProjet.Nom) + '.' + cExtension;
  if FileExists(FichierCourant) then SaveDialog1.InitialDir := ExtractFilePath(FichierCourant);
  if SaveDialog1.Execute then begin
    SaveDialog1.FileName := ForceFileExtension(SaveDialog1.FileName, cExtension);
    if Save(SaveDialog1.FileName) then begin
      FichierCourant := SaveDialog1.FileName;
      FSaved := true;
    end;
  end;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.AdvGlassButtonMajLocal;
begin
  ImageEx_Local.Caption := FProjet.Local_Courrant.Nom;
  ImageEx_LocalPrev.Visible := not FProjet.Local_CourrantIsFirst;
  ImageEx_LocalSuiv.Visible := not FProjet.Local_CourrantIsLast;
end;                                                      
//*************************************************************************************************************************************
procedure TFormCuisiNote.Lock;
begin
  inc(FLockCount);
  RefreshLock;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.UnLock;
begin
  dec(FLockCount);
  RefreshLock;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.RefreshLock;
begin
  AdvSmoothPanel_Top.Enabled := (FLockCount = 0);
  PageControl.Enabled := AdvSmoothPanel_Top.Enabled;
  if AdvSmoothPanel_Top.Enabled then PopCursor
                                else PushCursor(crAppStart);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.LinkProjet(Projet : TCAD_Projet);
begin
  if (Projet.Parent = nil) then FCuisiNoteRoot.AddChild(Projet);
  Projet.Affaire_Locaux_CallBack := AdvGlassButtonMajLocal;
  Projet.AssignControls([AdvSmoothPanel_Projet, AdvSmoothPanel_Reseaux]);
  Projet.AfficheMessage := AfficheMessage;
end;
//*************************************************************************************************************************************
function TFormCuisiNote.ChangeMode(Mode : TMode; Sender : TObject = nil; ModeFrom : TMode = mStartBegin; SousMode : TMode = mStartBegin; Root : TObject = nil) : TModeSortie;
  //---------------------------------------
  procedure Reset;
  var i : integer;
  begin
    ImageEx_Projet.DrawClicked := false;
    ImageEx_Local.DrawClicked := false;
    ImageEx_Piece.DrawClicked := false;
    ImageEx_Cuisson.DrawClicked := false;
    ImageEx_Hottes.DrawClicked := false;
    ImageEx_Diffuseurs.DrawClicked := false;
    ImageEx_Ventilation.DrawClicked := false;
    AdvSmoothPanel_SubLocal.Enabled := false;
  end;
  //---------------------------------------
var i : integer;
    F : TForm;
    Doc : TALXMLDocument;
    N, N1 : TALXMLNode;
    FormLoginEspacePro : TFormLoginEspacePro;
    M : TMode;
    Allowed : boolean;
    S : TMemoryStream;
begin
  Result := cmNone;       

  case Mode of
    //Start --------------------------------------------------------------------------------------------------------
    mStartBegin : begin
      ChangeMode(Succ(Mode));
    end;

    //avant le LoginEP pour pouvoir verifier la version de la BDD
    mLoadXML : begin
      PushCursor(crAppStart);
      Doc := TALXMLDocument.Create(nil);
      try
        S := ZIP_File2Stream(vAppPath + cBDD_Path + cBDD_ZIP, cBDD_File, false);
        Doc.LoadFromStream(S);
        S.Destroy;
        vBDDs_Objets := Doc.Node.ChildNodes.FindNode('Root');
        vBDDs_Ver := vBDDs_Objets.ChildNodes.NodeByIndex[0].ChildValues['Version'];
        PushCursor(crDefault);
        ChangeMode(Succ(Mode));
      except
        PushCursor(crDefault);
        AfficheMessage('Erreur au chargement des banques de donn�es XML.<br>' + cLogiciel + ' va fermer ...', MB_OK + MB_ICONERROR);
        Doc.Destroy;
        ChangeMode(mDestroyEnd);
        Application.Terminate;
        Exit;
      end;
    end;

    mLoginEP : begin
      Application.CreateForm(TFormLoginEspacePro, FormLoginEspacePro);
      if (FormLoginEspacePro.ShowModal = mrOk) then begin
        FormLoginEspacePro.Destroy;
      end else begin
        FormLoginEspacePro.Destroy;
        Application.Terminate;
        exit;
      end;

      ChangeMode(Succ(Mode));
    end;

    mLoadBDDs : begin
      FBeginProgress2(2, 'Initialisation des outils de calcul', true, 'XLS', false, ImageLogo);

      //XLS
      if not XLS_Init then begin
        FEndProgress;
        AfficheMessage('Erreur au chargement des banques de donn�es XLS.<br>' + cLogiciel + ' va fermer ...', MB_OK + MB_ICONERROR);
        ChangeMode(mDestroyEnd);
        Application.Terminate;
        Exit;
      end;

      //DLL
      FStepProgress('DLL');
      if not DLL_Init then begin
        FEndProgress;
        AfficheMessage('Erreur au chargement des DLL de calculs.<br>' + cLogiciel + ' va fermer ...', MB_OK + MB_ICONERROR);
        ChangeMode(mDestroyEnd);
        Application.Terminate;
        Exit;
      end;

      Sleep(500);
      FStepProgress;
      Sleep(100);
      FEndProgress;
      ChangeMode(Succ(Mode));
    end;

    mCreate : begin
      RegisterExt('.' + cExtension, cLogiciel, 'Etude ' + cLogiciel, Application.ExeName, 0);
      FFichiersRecents := TStringList.Create;

      //CAD
      FormCAD := TFormCAD.Create(self);
      FormCAD.Attach2Parent(LOCAL_PanelCAD, true);
      OnMouseWheel := FormCAD.OnMouseWheel;
      FormCAD.ViewportHorizontal.OnMouseLeave := MouseLeaveCAD;
      FormCAD.SetBackgroundColor(LOCAL_PanelCAD.Color);
      FormCAD.GLDummyHUD.Visible := false;
      FormCAD.Action_Grille_Active.Checked := false;
      FormCAD.Action_Grille_Visible.Checked := false;
      FormCAD.AccrochesDisabled := false;
      FormCAD.GLXYZGrid1.Visible := false;
      ImageEx_CAD_Select.OnClick(nil);
      ImageEx_CAD_Arbo.Visible := DelphiRuning;
      ImageEx_CAD_23D.Visible := DelphiRuning;                              

      //Global
      AdvOfficeTabSet_Reseaux.AdvOfficeTabs.Clear;
      AdvOfficePager_Reseau.ActivePageIndex := 0;
      AdvOfficePager_Reseau.OnChange(AdvOfficePager_Reseau);

      //CAD
      FCuisiNoteRoot := TCAD_CuisiNoteRoot.Create;
      FCuisiNoteRoot.Visible := true;
      FormCAD.Global_Root.AddChild(FCuisiNoteRoot);
      ChangeMode(Succ(Mode));
    end;

    mLoadIni : begin
      //valeurs par defaut
      FontDialogMurs.Font.Color := ConvertColorVector(clrBlack);
      FontDialogObjets.Font.Color := ConvertColorVector(clrOrange);
      SetBounds(Left, Top, 0, 0);

      //lecture
      Doc := TALXMLDocument.Create(nil);
      if FileExists(vAppPath + cIni_Fichier) then begin
        Doc.LoadFromFile(vAppPath + cIni_Fichier);

        //Fen�tre
        N := Doc.DocumentElement.ChildNodes.FindNode(cIni_Fenetre);
        if (N <> nil) then begin
          WindowState := TWindowState(N.Attributes[cIni_Fenetre_WindowState]);
          case WindowState of
            wsNormal : SetBounds(N.Attributes[cIni_Fenetre_Left], N.Attributes[cIni_Fenetre_Top], N.Attributes[cIni_Fenetre_Width], N.Attributes[cIni_Fenetre_Height]);
          end;
        end;

        //Fichiers r�cents
        PopupMenuRecent.Items.Clear;
        N := Doc.DocumentElement.ChildNodes.FindNode(cIni_Recents);
        if (N <> nil) then begin
          N := N.ChildNodes.Last;
          while (N <> nil) do begin
            if FileExists(XMLAttribut2String(N, cIni_Recents_Att)) then AddRecent(XMLAttribut2String(N, cIni_Recents_Att));
            N := N.PreviousSibling;
          end;
        end;

        //Fonts
        N := Doc.DocumentElement.ChildNodes.FindNode(cIni_Couleurs);
        if (N <> nil) then begin
          N1 := N.ChildNodes.FindNode(cIni_Couleurs_FontMurs);
          if (N1 <> nil) then StrToFont(XMLAttribut2String(N1, 'font'), FontDialogMurs.Font);
          N1 := N.ChildNodes.FindNode(cIni_Couleurs_FontObjs);
          if (N1 <> nil) then StrToFont(XMLAttribut2String(N1, 'font'), FontDialogObjets.Font);
        end;

        //R�dacteur
        N := Doc.DocumentElement.ChildNodes.FindNode(cIni_Redacteur);
        if (N <> nil) then begin
          FDefaults.Projet.Redacteur_Entreprise    := XMLNode2String(N, cIni_Redacteur_Entreprise);
          FDefaults.Projet.Redacteur_Departement   := XMLNode2String(N, cIni_Redacteur_Departement);
          FDefaults.Projet.Redacteur_Commune       := XMLNode2String(N, cIni_Redacteur_Commune);
          FDefaults.Projet.Redacteur_Interlocuteur := XMLNode2String(N, cIni_Redacteur_Interlocuteur);
        end;
      end;
      Doc.Destroy;

      ChangeMode(Succ(Mode));
    end;

    mStartEnd : begin
      Application.ShowMainForm := true;
      AdvOfficePager_ReseauResize(nil);
    end;

    mCommandLine : begin
      if (ParamCount > 0) and
         FileExists(ParamStr(1)) and
         (FileExtension(ParamStr(1)) = cExtension) and
         (FLockCount = 0) and
         (ChangeMode(mSaveCurrent) <> cmAbort)
        then OpenFile(ParamStr(1))
        else New;
    end;

    //End ----------------------------------------------------------------------------------------------------------
    
    mDestroyBegin : begin
      IsDestroying := true;
      FProjet.Locked := true;
      FormCAD.UnSelectAll(false);
      ChangeMode(Succ(Mode));
    end;

    mSaveIni : begin
      Doc := TALXMLDocument.Create(nil);
      Doc.Options := [];
      Doc.Active := true;
      Doc.Encoding := cAlXMLUTF8EncodingStr;
      N := Doc.AddChild('Root');

      //Fen�tre
      N := N.AddChild(cIni_Fenetre);
      N.Attributes[cIni_Fenetre_WindowState] := WindowState;
      case WindowState of
        wsNormal : begin
          N.Attributes[cIni_Fenetre_Left] := Left;
          N.Attributes[cIni_Fenetre_Top] := Top;
          N.Attributes[cIni_Fenetre_Width] := Width;
          N.Attributes[cIni_Fenetre_Height] := Height;
        end;
      end;

      //Fichiers r�cents
      if (FFichiersRecents.Count > 0) then begin
        N := Doc.DocumentElement.AddChild(cIni_Recents);
        for i:=0 to FFichiersRecents.Count-1 do String2XMLAttribut(N, 'Recent_' + IntToStr(i+1), cIni_Recents_Att, FFichiersRecents[i]);
      end;

      //Fonts
      N := Doc.DocumentElement.AddChild(cIni_Couleurs);
      String2XMLAttribut(N, cIni_Couleurs_FontMurs, 'font', FontToStr(FontDialogMurs.Font));
      String2XMLAttribut(N, cIni_Couleurs_FontObjs, 'font', FontToStr(FontDialogObjets.Font));

      //R�dacteur
      N := Doc.DocumentElement.AddChild(cIni_Redacteur);
      String2XMLNode(N, cIni_Redacteur_Entreprise, FProjet.ValeurIdx_Typed[cProjet_Redacteur_Entreprise]);
      String2XMLNode(N, cIni_Redacteur_Departement, FProjet.ValeurIdx_Typed[cProjet_Redacteur_Departement]);
      String2XMLNode(N, cIni_Redacteur_Commune, FProjet.ValeurIdx_Typed[cProjet_Redacteur_Commune]);
      String2XMLNode(N, cIni_Redacteur_Interlocuteur, FProjet.ValeurIdx_Typed[cProjet_Redacteur_Interlocuteur]);

      //fin
      Doc.SaveToFile(vAppPath + cIni_Fichier);
      Doc.Destroy;
      ChangeMode(Succ(Mode));
    end;

    mFreeBDDs : begin
      vBDDs_Objets.OwnerDocument.Destroy;
      ChangeMode(Succ(Mode));
    end;

    mDestroyEnd : begin
      FFichiersRecents.Free;
      F := FormCAD;
      FormCAD := nil;
      F.Free;
    end;

    //--------------------------------------------------------------------------------------------------------------

    mSaveCurrent : begin
      if (FProjet <> nil) and FProjet.Modded then begin
        case AfficheMessage('<b>Sauvegarder</b> l''�tude courante avant de continuer ?', MB_YESNOCANCEL + MB_ICONQUESTION) of
          ID_YES : begin
            FSaved := false;
            ImageEx_Save.OnClick(nil);
            if not FSaved then Result := cmAbort;
          end;
          ID_CANCEL : Result := cmAbort;
        end;
      end;
    end;

    //Else ---------------------------------------------------------------------------------------------------------

    else if (Mode <> FOldMode) then begin
      Allowed := true;
      FormCAD.UnSelectAll;

      case FOldMode of
        mProjet : if (Mode <> mPDF) then begin
          if (FProjet.ZoneClimatique = '') then begin
            AfficheMessage('D�partement du projet non renseign�', MB_OK + MB_ICONERROR);
            Mode := mNone;
            Allowed := false;
          end;
        end;
      end;

      case Mode of
        mProjet : begin
          Reset;
          PageControl.ActivePage := TabSheet_Projet;
          ImageEx_Projet.DrawClicked := true;
          FProjet.ToForm;
        end;

        mLocal : begin
          Reset;
          ChangeMode(FProjet.Local_Courrant.Phase);
          FProjet.Local_Courrant.ToForm;
          PageControl.ActivePage := TabSheet_Local;
          ImageEx_Local.DrawClicked := true;
          AdvSmoothPanel_SubLocal.Enabled := true;
          case FProjet.Local_Courrant.Phase of
            mArchi      : ImageEx_Piece.DrawClicked := true;
            mCuisson    : ImageEx_Cuisson.DrawClicked := true;
            mHottes     : ImageEx_Hottes.DrawClicked := true;
            mDiffuseurs : ImageEx_Diffuseurs.DrawClicked := true;
          end;
        end;

        mLocalPrec : begin
          FProjet.Local_Dec;
          FOldMode := Mode;
          ChangeMode(mLocal);
          Mode := FOldMode; //pour la suite
        end;

        mLocalSuiv : begin
          FProjet.Local_Inc;
          FOldMode := Mode;
          ChangeMode(mLocal);
          Mode := FOldMode; //pour la suite
        end;

        mArchi : begin
          if (Sender <> nil) then begin
            FProjet.Local_Courrant.Phase := mArchi;
            Allowed := (FProjet.Local_Courrant.Phase = mArchi);
            FOldMode := Mode;
            ChangeMode(mLocal);
          end;
        end;

        mCuisson : begin
          if (Sender <> nil) then begin
            FProjet.Local_Courrant.Phase := mCuisson;
            Allowed := (FProjet.Local_Courrant.Phase = mCuisson);
            FOldMode := Mode;
            ChangeMode(mLocal);
          end;
        end;

        mHottes : begin
          if (Sender <> nil) then begin
            FProjet.Local_Courrant.Phase := mHottes;
            Allowed := (FProjet.Local_Courrant.Phase = mHottes);
            FOldMode := Mode;
            ChangeMode(mLocal);
          end;
        end;

        mDiffuseurs : begin
          if (Sender <> nil) then begin
            FProjet.Local_Courrant.Phase := mDiffuseurs;
            Allowed := (FProjet.Local_Courrant.Phase = mDiffuseurs);
            FOldMode := Mode;
            ChangeMode(mLocal);
          end;
        end;

        mReseaux : begin
{
          //on force la saisie compl�te des locaux avant d'acc�der aux r�seaux
          for i:=0 to FProjet.Local_Count-1 do begin
            if (not FProjet.Local(i).Diffuseur_TousSaisis) or
               (FProjet.Local(i).Phase < mHottes) then
            begin
              AfficheMessage('Pour passer � cette �tape il faut terminer la saisie (hottes/diffuseurs) du local "' + FProjet.Local(i).Nom + '".', MB_OK + MB_ICONEXCLAMATION);
              Allowed := false;
              FOldMode := mLocal;
              Break;
            end;
          end;
          if Allowed then begin
}
            Reset;
            ImageEx_Ventilation.DrawClicked := true;
            FProjet.Reseau_Activate(AdvOfficeTabSet_Reseaux.AdvOfficeTabs[AdvOfficeTabSet_Reseaux.ActiveTabIndex]);
            PageControl.ActivePage := TabSheet_Reseaux;
//          end;
        end;

        mPDF : begin
          Reset;
//          PageControl.ActivePage := TabSheet_PDF;
          Allowed := false;
          CreatePDF;
        end;
      end;

      if not Allowed then begin
        M := FOldMode;
        FOldMode := mNone;
        ChangeMode(M);
      end else begin
        FOldMode := Mode;
      end;
    end;
  end;
end;
//*************************************************************************************************************************************
type TTypeUDO = (udoCAD, udoSYNOPTIQUE, udoIMAGE);
     TTagDessin = record
       TypeUDO : TTypeUDO;

       //udoSYNOPTIQUE
       Reseau : TBase_Reseau;
       Page   : integer;

       //udoCAD
       Local : TBase_Local;

       //udoIMAGE
       ImageStr : string;
       Image    : TImage;
       Centered : boolean;
     end;
     PTagDessin = ^TTagDessin;
     TSetField_Flags = (sffValeur, sffSubIndent, sffTabed, sffItalic, sffSubIndentNoPuce);
const cMarge = 1;
procedure TFormCuisiNote.CreatePDF;
var VPE : TVPE;
  //--------------------------------------------------
  function TagCAD(Local : TBase_Local) : PTagDessin;
  begin
    System.New(Result);
    ZeroMemory(Result, SizeOf(TTagDessin));
    Result.TypeUDO := udoCAD;
    Result.Local   := Local;
  end;
  //--------------------------------------------------
  function TagSYNOPTIQUE(Reseau : TBase_Reseau; Page : integer) : PTagDessin;
  begin
    System.New(Result);
    ZeroMemory(Result, SizeOf(TTagDessin));
    Result.TypeUDO := udoSYNOPTIQUE;
    Result.Reseau  := Reseau;
    Result.Page    := Page;
  end;
  //--------------------------------------------------
  function TagIMAGE(ImageStr : string; Image : TImage; Centered : boolean) : PTagDessin;
  begin
    System.New(Result);
    ZeroMemory(Result, SizeOf(TTagDessin));
    Result.TypeUDO  := udoIMAGE;
    Result.ImageStr := ImageStr;
    Result.Image    := Image;
    Result.Centered := Centered;
  end;
  //--------------------------------------------------
  function ComputeField(Lignes : array of OleVariant; MainIndent : boolean = false) : string;
  var i, j : integer;
      Valeur, Puce : string;
      TabedS : integer;
      Visible,
      Format,
      SubIndent,
      Tabed,
      Italic : boolean;
  begin
    Result := '';
    i := 0;
    while (i <= High(Lignes)) do begin
      case VarType(Lignes[i]) of
        varOleStr : begin
          Valeur := '';
          Visible := true;
          Format := false;
          SubIndent := false;
          Tabed := false;
          TabedS := 0;
          Italic := false;
          j := i+1;
          while (j <= High(Lignes)) do begin
            case VarType(Lignes[j]) of
              varOleStr  : Break;
              varBoolean : Visible := boolean(Lignes[j]);
              varInteger : case TSetField_Flags(Lignes[j]) of
                             sffValeur : begin
                               inc(j);
                               Valeur := string(Lignes[j]);
                               if (Valeur = '') then Valeur := '-';
                             end;
                             sffSubIndent,
                             sffSubIndentNoPuce : begin
                               SubIndent := true;
                               Format := true;
                               case TSetField_Flags(Lignes[j]) of
                                 sffSubIndent       : Puce := 'o ';
                                 sffSubIndentNoPuce : Puce := '';
                               end;
                             end;
                             sffTabed : begin
                               Tabed := true;
                               inc(j);
                               TabedS := integer(Lignes[j]);
                               Format := true;
                             end;
                             sffItalic : begin
                               Italic := true;
                               Format := true;
                             end;
                           end;
            end;
            inc(j);
          end;
          if Visible then begin
            if (Result = '') then begin
              if MainIndent then Result := '\line {\li200 ';
            end else begin
              Result := Result + ' \line ';
            end;
            if Format then Result := Result + '{';
            if Tabed then Result := Result + '\tx' + IntToStr(TabedS) + ' ';
            if Italic then Result := Result + '\i ';
            if SubIndent then begin
              if MainIndent then Result := Result + '\li400 ' + Puce
                            else Result := Result + '\li200 ' + Puce;
            end;
            if (Valeur <> '') then begin
              if Tabed then Result := Result + string(Lignes[i]) + '\tab : ' + Valeur
                       else Result := Result + string(Lignes[i]) + ' : ' + Valeur;
            end else begin
              Result := Result + StrReplace(string(Lignes[i]), #13#10, '\line ', false, true, false);
            end;

            if Format then Result := Result + '}';
          end;
          i := j;
        end;
        else inc(i);
      end;
    end;
    if MainIndent then Result := Result + ' \line }';
  end;
  //--------------------------------------------------
  procedure Header(TexteBasDroite : string = '');
  begin
    //logo
    VPE.Dessin(TagIMAGE('', ImageLogo, false), prLeftMargin, prLeftMargin, prTopMargin, prTopMargin, 0, 5, 0, 1.15);

    //logiciel � droite
    VPE.WriteRTF('\i ' + cLogiciel + ', version ' + cVersion, false, hRIGHT, prCenter, prRightMargin, prLastBottomMarge, prFree, 0, 0, 0, 0);

    //texte � gauche
    VPE.WriteRTF(ComputeField(['Projet', sffItalic, sffValeur, Projet.Nom, sffTabed, 1000,
                               'Date', sffItalic, sffValeur, Date2Str(Now, 'dd MMMM YYYY'), sffTabed, 1000,
                               'Destinataire', sffItalic, sffValeur, Projet.ValeurIdx_Typed[cProjet_Destinataire_Entreprise], sffTabed, 1000
                              ]), false, hLEFT, prLeftMargin, prRightMargin, prLastTop, prFree, 0, 0, 0, 0);

    //texte � droite
    if (TexteBasDroite <> '') then VPE.WriteRTF('\line \line ' + TexteBasDroite, false, hRIGHT, prCenter, prRightMargin, prLastTop, prLastBottom, 0, 0, 0, 0);

    //ligne horizontale
    VPE.LigneHorizontale(prLastBottom, 0);
  end;
  //--------------------------------------------------
  function Formate(d : double; Format : TTypeResultat) : string;
  begin
    Result := Float2Str(d, cTypeResultat_Digits[Format]) + ' ' + cTypeResultat_Unite[Format];
  end;
  //--------------------------------------------------
  function Choice(const ValTrue, ValFalse : string; Test : boolean) : string;
  begin
    if Test then Result := ValTrue
            else Result := ValFalse;
  end;
  //--------------------------------------------------
  procedure RecAnnexe(Obj : TObject; Liste : TIntegerList);
  var i : integer;
      t : TTypeVentilo;
  begin
    if Obj.InheritsFrom(TCAD_BDD) and
       (XML2String(TCAD_BDD(Obj).IndexBDD, 'FICHETEC') <> '') and
       (Liste.IndexOf(TCAD_BDD(Obj).IndexBDD) = -1) then Liste.Add(TCAD_BDD(Obj).IndexBDD);

    //ventilateurs
    if Obj.InheritsFrom(TCAD_Projet) then
      for i:=0 to TCAD_Projet(Obj).Reseau_Count-1 do
        for t:=Low(TTypeVentilo) to High(TTypeVentilo) do
          if (TCAD_Projet(Obj).Reseau(i).ValeurIdxIndexed[cReseau_Ventilo_Modele, Ord(t)] <> '') and
             (XML2String(TCAD_Projet(Obj).Reseau(i).FCalculsVentilo.Solutions[t][TCAD_Projet(Obj).Reseau(i).IndexVentiloChoisi(t)].Index, 'FICHETEC') <> '') and
             (Liste.IndexOf(TCAD_Projet(Obj).Reseau(i).FCalculsVentilo.Solutions[t][TCAD_Projet(Obj).Reseau(i).IndexVentiloChoisi(t)].Index) = -1)
            then Liste.Add(TCAD_Projet(Obj).Reseau(i).FCalculsVentilo.Solutions[t][TCAD_Projet(Obj).Reseau(i).IndexVentiloChoisi(t)].Index);

    if Obj.InheritsFrom(TObjetCAD_Base) then
      for i:=0 to TObjetCAD_Base(Obj).Count-1 do
        RecAnnexe(TObjetCAD_Base(Obj)[i], Liste);
  end;
  //----------------------------------------
var i, j, k, Friteuses, NbrDiff, IdxDiff : integer;
    s, s2 : string;
    SL : TStringList;
    Ilot : TCAD_Hotte;
    Infos : TValueList;
    t : TTypeVentilo;
    b, b2, b3 : boolean;
    Locaux : TList;
    Ids : TIntegerList;
    d : double;
begin
  Lock;
  FBeginProgress2(8, 'G�n�ration du rapport PDF', true, '', false, ImageLogo);
  VPE := TVPE.Create;
  VPE.OpenDoc(Self, CreatePDF_CallbackDessin);

  //page de garde
  FStepProgress('Page de garde');
  VPE.NewPage(VORIENT_PORTRAIT, cMarge);
  Header;
  VPE.WriteRTF(ComputeField(['{\b \fs32 Rapport de s�lection}',
                             '',
                             'Logiciel ' + cLogiciel,
                             '',
                             Projet.Nom,
                             'Calcul du ' + Projet.ValeurIdx_Typed[cProjet_Date]
                            ]), true, hCENTER, prLeftMargin, prRightMargin, prLastBottom, prFree, 0, 0, 4, 0);
  VPE.WriteRTF(ComputeField(['{\b \fs18 R�dacteur du projet}',
                             '',
                             'Soci�t�', sffValeur, Projet.ValeurIdx_Typed[cProjet_Redacteur_Entreprise], sffTabed, 2000,
                             'D�partement', sffValeur, Projet.ValeurIdx_Typed[cProjet_Redacteur_Departement], sffTabed, 2000,
                             'Commune', sffValeur, Projet.ValeurIdx_Typed[cProjet_Redacteur_Commune], sffTabed, 2000,
                             'Interlocuteur', sffValeur, Projet.ValeurIdx_Typed[cProjet_Redacteur_Interlocuteur], sffTabed, 2000
                            ]), true, hLEFT, prLeftMargin, prCenter, prLastBottom, prFree, 0, -0.2, 1.5, 0);
  VPE.WriteRTF(ComputeField(['{\b \fs18 Destinataire du projet}',
                             '',
                             'Soci�t�', sffValeur, Projet.ValeurIdx_Typed[cProjet_Destinataire_Entreprise], sffTabed, 2000,
                             'D�partement', sffValeur, Projet.ValeurIdx_Typed[cProjet_Destinataire_Departement], sffTabed, 2000,
                             'Commune', sffValeur, Projet.ValeurIdx_Typed[cProjet_Destinataire_Commune], sffTabed, 2000,
                             'Interlocuteur', sffValeur, Projet.ValeurIdx_Typed[cProjet_Destinataire_Interlocuteur], sffTabed, 2000
                            ]), true, hLEFT, prCenter, prRightMargin, prLastTop, prFree, 0.2, 0, 0, 0);
  VPE.WriteRTF(ComputeField(['{\b \fs18 Caract�ristiques du projet}',
                             '',
                             '{\ul G�ographie}',
                               'Lieu', sffValeur, Projet.ValeurIdx_Typed[cProjet_Lieu], sffSubIndent, sffTabed, 2000,
                               'D�partement', sffValeur, Projet.ValeurIdx_Typed[cProjet_Dep], sffSubIndent, sffTabed, 2000,
                               'Zone climatique', sffValeur, Projet.ValeurIdx_Typed[cProjet_Meteo_Zone], sffSubIndent, sffTabed, 2000,
                               'Altitude', sffValeur, Projet.ValeurIdx_Typed[cProjet_Meteo_Altitude], sffSubIndent, sffTabed, 2000,
                             '',
                             '{\ul Fonctionnement}',
                               'Annuel', sffValeur, Projet.ValeurIdx_Typed[cProjet_Fonctionnement_Annuel] + ' jours/an', sffSubIndent, sffTabed, 2000,
                               'Hebdomadaire', sffValeur, Projet.ValeurIdx_Typed[cProjet_Fonctionnement_Hebdomadaire] + ' jours/semaine', sffSubIndent, sffTabed, 2000,
                               'Journalier', sffValeur, 'de ' + Projet.ValeurIdx_Typed[cProjet_Fonctionnement_Journalier_De] + 'H � ' + Projet.ValeurIdx_Typed[cProjet_Fonctionnement_Journalier_A] + 'H', sffSubIndent, sffTabed, 2000,
                               'P�riode hivernale', sffValeur, 'de ' + Projet.ValeurIdx_Typed[cProjet_Fonctionnement_Hivernal_Du] + ' � ' + Projet.ValeurIdx_Typed[cProjet_Fonctionnement_Hivernal_Au], sffSubIndent, sffTabed, 2000,
                               'Vacances d''hiver', sffValeur, Projet.ValeurIdx_Typed[cProjet_Fonctionnement_VacancesHiver] + ' semaine(s)', sffSubIndent, sffTabed, 2000,
                             '',
                             '{\ul M�thode r�glementaire RSD64-2 r�v. du 20/10/83}',
                               'Nombre de repas', sffValeur, Projet.ValeurIdx_Typed[cProjet_RSD_Nbr], sffSubIndent, sffTabed, 2000,
                               'Cuisine relais', sffValeur, Projet.ValeurIdx_Typed[cProjet_RSD_Relais], sffSubIndent, sffTabed, 2000,
                               'D�bit d''air neuf', sffValeur, Projet.ValeurIdx_Typed[cProjet_RSD_Debit], sffSubIndent, sffTabed, 2000
                            ]), true, hLEFT, prLeftMargin, prCenter, prLastBottom, prFree, 0, -0.2, 0.4, 0);
  s := '';
  for i:=0 to Projet.Local_Count-1 do s := s + Projet.Local(i).Nom + ' \line ';
  VPE.WriteRTF(ComputeField(['{\b \fs18 Autres informations}',
                             '',
                             '{\ul Locaux du projet}',
                             s, sffSubIndentNoPuce,
                             '{\ul Commentaires}',
                             Projet.ValeurIdx[cProjet_Commentaires], sffSubIndentNoPuce
                            ]), true, hLEFT, prCenter, prRightMargin, prLastTop, prLastBottom, 0.2, 0, 0, 0);
  VPE.WriteRTF(ComputeField(['{\colortbl; \red255\green0\blue0; \red0\green0\blue255;}' +
                             '{\qc ' +
                             '{\cf1 Le calculateur ' + cLogiciel + ' est mis � votre disposition pour vous permettre d''obtenir une estimation des besoins aerauliques dans le domaine de la cuisine. ' +
                             'Les r�sultats ne constituent jamais une �tude de l''installation, le client ayant la responsabilit� de faire appel � un ing�nieur sp�cialiste, pour ce type de prestations.' +
                             '}',
                             '',
                             'France Air peut r�pondre � vos questions sur la cuisine professionnelle',
                             'par t�l�phone au 0820 820 626',
                             'par courriel sur {\cf2 \ul cuisine@france-air.com}',
                             '',
                             'Pour obtenir un devis ou des informations sur ce rapport,',
                             'vous pouvez le faire suivre � votre commercial France Air du Comptoir de l''air de votre r�gion.' +
                             '}'
                            ]), true, hLEFT, prLeftMargin, prRightMargin, prLastBottom, prFree, 0, 0, 4, 0);

  //locaux
  FStepProgress('Locaux');
  FBeginProgress(Projet.Local_Count);
  for i:=0 to Projet.Local_Count-1 do begin
    FStepProgress(Projet.Local(i).Nom);
    
    //dessin
    VPE.NewPage(VORIENT_LANDSCAPE, cMarge);
    Header;
    VPE.WriteRTF('\b \fs20 ' + Projet.Local(i).Nom, true, hCENTER);
    VPE.Dessin(TagCAD(Projet.Local(i)), prLeftMargin, prRightMargin, prLastBottom, prBottomMargin, 0, 0, 0.05, -1);

    //infos
    VPE.NewPage(VORIENT_PORTRAIT, cMarge);
    Header;
    VPE.WriteRTF('\b \fs20 ' + Projet.Local(i).Nom, true, hCENTER);
    case TMethodeCalcul(Projet.Local(i).LocalCAD.ValeurIdx[cLocal_Methode_de_calcul]) of
      mcSimplifiee : VPE.WriteRTF('M�thode de calcul des d�bits d''extraction : Simplifi�e.');
      mcDetaillee  : VPE.WriteRTF('M�thode de calcul des d�bits d''extraction : D�taill�e VDI.');
      else VPE.WriteRTF('M�thode de calcul des d�bits d''extraction : non renseign�e.');
    end;
    case TPorteCoupeFeu(Projet.Local(i).LocalCAD.ValeurIdx[cLocal_Porte_coupe_feu]) of
      pcfOui : VPE.WriteRTF('Cuisine {\ul avec} porte coupe feu ouvrant sur les locaux de distribution ouverts au public (au sens de la r�glementation ERP type GC8).');
      pcfNon : VPE.WriteRTF('Cuisine {\ul sans} porte coupe feu ouvrant sur les locaux de distribution ouverts au public.');
    end;
    IdxDiff := Projet.Local(i).Diffuseur_IdBase;
    NbrDiff := Projet.Local(i).Diffuseur_NbrNecessaire;
    VPE.WriteRTF(ComputeField(['{\b \fs18 D�bits mis en oeuvre dans le local}',
                               '',
                               'Extraction', sffValeur, Projet.Local(i).LocalCAD.ValeurIdx_Typed[cLocalCAD_DebitEXT], sffTabed, 3000,
                               'Compensation via hottes', sffValeur, Projet.Local(i).LocalCAD.ValeurIdx_Typed[cLocalCAD_DebitCOMP_Hottes], sffTabed, 3000,
                               'Compensation via diffuseurs', sffValeur, Projet.Local(i).LocalCAD.ValeurIdx_Typed[cLocalCAD_DebitCOMP_Diffs], sffTabed, 3000,
                               'Induction', sffValeur, Projet.Local(i).LocalCAD.ValeurIdx_Typed[cLocalCAD_DebitIND], sffTabed, 3000,
                               '', (NbrDiff > 0),
                               'Diffuseurs', (NbrDiff > 0),
                                 'Type', sffValeur, XML2String(IdxDiff, 'INFOBULLE'), (NbrDiff > 0), sffSubIndent, sffTabed, 3000,
                                 'Nombre', sffValeur, NbrDiff, (NbrDiff > 0), sffSubIndent, sffTabed, 3000,
                                 'D�bit unitaire', sffValeur, Formate(Projet.Local(i).LocalCAD.ValeurIdx[cLocalCAD_DebitCOMP_Diffs] / NbrDiff, rM3Heure), (NbrDiff > 0), sffSubIndent, sffTabed, 3000,
                                 'Perte de charge', sffValeur, Formate(XLS_Diffuseur_PdC(Projet.Local(i).Diffuseur_DebitNominal, Projet.Local(i).Diffuseur_DebitGlobal, Projet.Local(i).Diffuseur_NbrNecessaire), rPa), (NbrDiff > 0), sffSubIndent, sffTabed, 3000
                              ]), true, hLEFT, prCenter, prRightMargin, prLastBottomMarge, prFree, 0.2, 0, 0, 0);
    VPE.WriteRTF(ComputeField(['{\b \fs18 Caract�ristiques du local}',
                               '',
                               'Surface', sffValeur, Projet.Local(i).LocalCAD.ValeurIdx_Typed[cLocalCAD_Surface], sffTabed, 3000,
                               'Hauteur', sffValeur, Projet.Local(i).LocalCAD.ValeurIdx_Typed[cLocalCAD_Hauteur], sffTabed, 3000,
                               'Volume', sffValeur, Float2Str(Projet.Local(i).LocalCAD.Volume, 0) + ' m�', sffTabed, 3000,
                               'Taux de brassage', sffValeur, Projet.Local(i).LocalCAD.ValeurIdx_Typed[cLocalCAD_TauxBrassage], sffTabed, 3000,
                               'D�bit d''air transf�r�', sffValeur, Projet.Local(i).LocalCAD.ValeurIdx_Typed[cLocal_Taux_de_transfert], sffTabed, 3000,
                               'Coefficient de simultan�it�', sffValeur, Projet.Local(i).LocalCAD.ValeurIdx_Typed[cLocal_CoefSimul], Projet.Local(i).LocalCAD.VariableIdx(cLocal_CoefSimul).Visible, sffTabed, 3000
                              ]), true, hLEFT, prLeftMargin, prCenter, prLastTop, prLastBottom, 0, -0.2, 0, 0);

    //ilots
    for j:=0 to Projet.Local(i).LocalCAD.Roots[rHottes].Count-1 do begin
      VPE.NewPage(VORIENT_PORTRAIT, cMarge);
      Header('\b \i ' + Projet.Local(i).Nom);

      Ilot := TCAD_Hotte(Projet.Local(i).LocalCAD.Roots[rHottes][j]);
      VPE.WriteRTF('\qc \b \fs18 ' + Ilot.NomIlot + ' (' + Ilot.Nom + ')', false, hLEFT, prLeftMargin, prRightMargin, prLastBottomMarge, prFree, 0, 0, 0, 0);

      //caract�ristiques
      VPE.WriteRTF(ComputeField(['{\b \fs18 Caract�ristiques de l''�lot}',
                                 '',
                                 'Profondeur', sffValeur, Ilot.ValeurIdx_Typed[cObjetDL_Profondeur], sffTabed, 3000,
                                 'Largeur', sffValeur, Ilot.ValeurIdx_Typed[cObjetDL_Largeur], sffTabed, 3000, not Ilot.IsProlonged,
                                 'Largeur', sffValeur, Float2Str(Ilot.LargeurProlonged * 1000, 0) + ' mm (dont  ' + Float2Str(Ilot.LargeurProlongations * 1000, 0) + ' mm de prolongations)', sffTabed, 3000, Ilot.IsProlonged,
                                 'Hauteur', sffValeur, Ilot.ValeurIdx_Typed[cObjetDL_Hauteur], sffTabed, 3000,
                                 '�l�vation', sffValeur, Ilot.ValeurIdx_Typed[cObjetDL_Elevation], sffTabed, 3000,
                                 'Poids', sffValeur, XML2String(Ilot.IndexBDD, 'POIDS') + ' kg', sffTabed, 3000
                                ]), true);
      Infos := TValueList.Create;
      Ilot.Accessoires(Ilot.IndexBDD, Infos);
      VPE.WriteRTF(ComputeField(['{\b \fs18 Caract�ristiques de la hotte}',
                                 '',
                                 'Positionnement', sffValeur, 'hotte centrale', Ilot.CotesTousLibres, sffTabed, 3000,
                                 'Positionnement', sffValeur, 'hotte adoss�e', not Ilot.CotesTousLibres, sffTabed, 3000,

                                 'Mod�le', sffValeur, XML2String(Ilot.IndexBDD, 'LIBELLE'), sffTabed, 3000, (Ilot.FCalculs.Interne.ProlongationG + Ilot.FCalculs.Interne.ProlongationD <= 0),
                                 'Code article', sffValeur, XML2String(Ilot.IndexBDD, 'CODEARTICLE'), sffTabed, 3000, (Ilot.FCalculs.Interne.ProlongationG + Ilot.FCalculs.Interne.ProlongationD <= 0),
                                 'Mod�le', sffValeur, 'Hotte sp�ciale, consulter France Air', sffTabed, 3000, (Ilot.FCalculs.Interne.ProlongationG + Ilot.FCalculs.Interne.ProlongationD > 0),

                                 '',
                                 'Piquage d''extraction',                                                                            (hEXT in Ilot.TypeHotte),
                                   'diam�tre', sffValeur, '355 mm',                                    sffSubIndent, sffTabed, 3000, (hEXT in Ilot.TypeHotte),
                                   'nombre',   sffValeur, XML2String(Ilot.IndexBDD, 'DIAMEXTRA355'),   sffSubIndent, sffTabed, 3000, (hEXT in Ilot.TypeHotte),

                                 'Piquage de compensation',                                                                          (hCOMP in Ilot.TypeHotte),
                                   'diam�tre', sffValeur, '250 mm',                                    sffSubIndent, sffTabed, 3000, (hCOMP in Ilot.TypeHotte) and (XML2Integer(Ilot.IndexBDD, 'DIAMINSUF250') > 0),
                                   'nombre',   sffValeur, XML2String(Ilot.IndexBDD, 'DIAMINSUF250'),   sffSubIndent, sffTabed, 3000, (hCOMP in Ilot.TypeHotte) and (XML2Integer(Ilot.IndexBDD, 'DIAMINSUF250') > 0),
                                   'diam�tre', sffValeur, '315 mm',                                    sffSubIndent, sffTabed, 3000, (hCOMP in Ilot.TypeHotte) and (not (hIND in Ilot.TypeHotte)) and (XML2Integer(Ilot.IndexBDD, 'DIAMINSUF315') > 0),
                                   'nombre',   sffValeur, XML2String(Ilot.IndexBDD, 'DIAMINSUF315'),   sffSubIndent, sffTabed, 3000, (hCOMP in Ilot.TypeHotte) and (not (hIND in Ilot.TypeHotte)) and (XML2Integer(Ilot.IndexBDD, 'DIAMINSUF315') > 0),
                                   'diam�tre', sffValeur, '200 mm',                                    sffSubIndent, sffTabed, 3000, (hCOMP in Ilot.TypeHotte) and (XML2Integer(Ilot.IndexBDD, 'DIAMCOMPENS200') > 0),
                                   'nombre',   sffValeur, XML2String(Ilot.IndexBDD, 'DIAMCOMPENS200'), sffSubIndent, sffTabed, 3000, (hCOMP in Ilot.TypeHotte) and (XML2Integer(Ilot.IndexBDD, 'DIAMCOMPENS200') > 0),

                                 'Piquage d''induction',                                                                              (hIND in Ilot.TypeHotte),
                                   'diam�tre', sffValeur, '315 mm',                                    sffSubIndent, sffTabed, 3000, (hIND in Ilot.TypeHotte) and (XML2Integer(Ilot.IndexBDD, 'DIAMINSUF315') > 0),
                                   'nombre',   sffValeur, XML2String(Ilot.IndexBDD, 'DIAMINSUF315'),   sffSubIndent, sffTabed, 3000, (hIND in Ilot.TypeHotte) and (XML2Integer(Ilot.IndexBDD, 'DIAMINSUF315') > 0),
                                 '',
                                 'Vitesse p�riph�rique', sffValeur, Float2Str(XML2Single(Ilot.IndexBDD, 'VIT_ENTRAINEMENT'), 2) + ' m/s', TMethodeCalcul(Projet.Local(i).LocalCAD.ValeurIdx[cLocal_Methode_de_calcul]) = mcSimplifiee, sffTabed, 3000,
                                 '{\b D�bit d''air d''extraction', sffValeur, Formate(Ilot.FCalculs.Interne.Q[cEXT].Debit, rM3Heure) + '}', sffTabed, 3000,
                                 'Perte de charge d''extraction', sffValeur, Formate(Ilot.FCalculs.Interne.Q[cEXT].PdC, rPa), sffTabed, 3000,
                                 '{\b D�bit d''air de compensation', sffValeur, Formate(Ilot.FCalculs.Interne.Q[cCOMP].DebitReel, rM3Heure) + '}', hCOMP in Ilot.TypeHotte, sffTabed, 3000,
                                 'Perte de charge de compensation', sffValeur, Formate(Ilot.FCalculs.Interne.Q[cCOMP].PdC, rPa), hCOMP in Ilot.TypeHotte, sffTabed, 3000,
                                 '{\b D�bit d''air d''induction', sffValeur, Formate(Ilot.FCalculs.Interne.Q[cIND].Debit, rM3Heure) + '}', hIND in Ilot.TypeHotte, sffTabed, 3000,
                                 'Perte de charge d''induction', sffValeur, Formate(Ilot.FCalculs.Interne.Q[cIND].PdC, rPa), hIND in Ilot.TypeHotte, sffTabed, 3000,
                                 'Nombre de filtre(s)', sffValeur, XML2String(Ilot.IndexBDD, 'FILTREGALVA'), XML2Integer(Ilot.IndexBDD, 'FILTREGALVA') > 0, sffTabed, 3000,
                                 'Nombre de filtre(s)', sffValeur, XML2String(Ilot.IndexBDD, 'FILTRECHOC'), XML2Integer(Ilot.IndexBDD, 'FILTRECHOC') > 0, sffTabed, 3000,
                                 'Nombre de plaques d''obturation', sffValeur, XML2String(Ilot.IndexBDD, 'PLAQUEOBTU'), XML2Integer(Ilot.IndexBDD, 'PLAQUEOBTU') > 0, sffTabed, 3000,
                                 'Echange plaque pour filtre', sffValeur, IntToStr(Ilot.FCalculs.XLS.PlaquesEchangees), sffTabed, 3000,
                                 'Echange filtre galva/choc', sffValeur, IntToStr(Ilot.FCalculs.XLS.SwapGalvaChoc), sffTabed, 3000,
                                 'Nombre de luminaire(s)', sffValeur, XML2String(Ilot.IndexBDD, 'NBLUMIN') + ' (code article ' + XML2String(Ilot.IndexBDD, 'LUMINAIRE') + ')', XML2Integer(Ilot.IndexBDD, 'NBLUMIN') > 0, sffTabed, 3000,
                                 Infos.Str(' \line ', '{\tx3000 ', '\tab : ', '}', '', '', true, '{\b ', '}', '')
                                ]), true, hLEFT, prLeftMargin, prRightMargin, prLastBottomMarge, prFree, 0, 0, 0, 0);
      Infos.Destroy;

      //image
      VPE.StorePos;
      s := XML2String(Ilot.IndexBDD, 'IMAGE3D');
      if Ilot.FCalculs.Interne.Pyrosafe then s := FileNameSansExtension(s, true) + ' pyrosafe.' + FileExtension(s);
      VPE.Dessin(TagIMAGE(s, nil, true), prRightMargin, prRightMargin, prLastTop, prLastTop, -5, -0.1, +0.1, +5);
      VPE.RestorePos;

      //composition
      VPE.GridFirst([VPE.Cellule('\b Mat�riel install� \line', prLeftMargin, 0, prLeftMargin, 5),
                     VPE.Cellule('\b Energie', prLastRight, 0, prLastRight, 2),
                     VPE.Cellule('\b Puissance'),
                     VPE.Cellule('\b Puissance \line sensible'),
                     VPE.Cellule('\b Puissance \line latente'),
                     VPE.Cellule('\b Largeur'),
                     VPE.Cellule('\b Profondeur'),
                     VPE.Cellule('\b Hauteur')
                    ]);
      for k:=0 to Ilot.EltsIlot.Count-1 do begin
        VPE.GridNext([TCAD_EltCuissonBase(Ilot.EltsIlot[k]).Nom,
                      TCAD_EltCuissonBase(Ilot.EltsIlot[k]).ValeurIdx_Typed[cEltCuisson_Energie],
                      TCAD_EltCuissonBase(Ilot.EltsIlot[k]).ValeurIdx_Typed[cEltCuisson_Puissance],
                      TCAD_EltCuissonBase(Ilot.EltsIlot[k]).ValeurIdx_Typed[cEltCuisson_PuissanceSensible],
                      TCAD_EltCuissonBase(Ilot.EltsIlot[k]).ValeurIdx_Typed[cEltCuisson_PuissanceLatente],
                      TCAD_EltCuissonBase(Ilot.EltsIlot[k]).ValeurIdx_Typed[cObjetDL_Largeur],
                      TCAD_EltCuissonBase(Ilot.EltsIlot[k]).ValeurIdx_Typed[cObjetDL_Profondeur],
                      TCAD_EltCuissonBase(Ilot.EltsIlot[k]).ValeurIdx_Typed[cObjetDL_Hauteur]
                     ]);
      end;

      //s�curit� incendie
      Friteuses := 0;
      for k:=0 to Ilot.EltsIlot.Count-1 do
        if StrContains(XML2String(TCAD_EltCuissonBase(Ilot.EltsIlot[k]).IndexBDD, 'IMAGE2D'), 'friteuse') then
          inc(Friteuses);
      if (Friteuses > 0) then VPE.WriteRTF(ComputeField(['{\ul S�curit� incendie :}',
                                                         '',
                                                         'Pyrosafe sur friteuse, int�gr� � la hotte.',
                                                         'Conform�ment � l''article GC8 du r�glement de s�curit� dans les ERP, le syst�me pyrosafe int�gr� � la hotte permet de d�tecter et d''�teindre tout feu ' +
                                                         'de bain d''huile pour des installations comprenant 1 � 3 friteuses et ce pour des hottes de longueur de 1 � 6 m.',
                                                         'Ce syst�me automatique et autonome d''extinction incendie offre une protection simple et efficace de friteuses professionnelles.',
                                                         'Il permet de d�tecter et d''�teindre rapidement un feu d''huile de cuisson.',
                                                         'La tuyauterie d''�mission est int�gr�e � la hotte. Une cha�ne inox �quip�e de fusibles est install�e dans la hotte d�clenche et automatiquement le syst�me en cas de feu.',
                                                         'Type de hotte', sffValeur, XML2String(Ilot.IndexBDD, 'LIBELLE'), sffTabed, 1700,
                                                         'Nombre de friteuse(s)', sffValeur, IntToStr(Friteuses), sffTabed, 1700,
                                                         'Code article pyrosafe\tab : consulter France Air.', sffTabed, 1700
                                                        ]));
    end;
  end;
  FEndProgress;
  
  //r�seaux
  FStepProgress('R�seaux');
  FBeginProgress(Projet.Reseau_Count);
  VPE.NewPage(VORIENT_PORTRAIT, cMarge);
  Header;
  b3 := true;
  for i:=0 to Projet.Reseau_Count-1 do begin
    FStepProgress('R�seau ' + Projet.Reseau(i).Nom);

    Projet.Reseau(i).RefreshHottes(true);
    VPE.SplitterStart;
    while true do begin
      if b3 then VPE.WriteRTF('\b \fs20 R�seau de ventilation ' + Projet.Reseau(i).Nom, true, hCENTER)
            else VPE.WriteRTF('\b \fs20 R�seau de ventilation ' + Projet.Reseau(i).Nom, true, hCENTER, prLeftMargin, prRightMargin, prLastBottomMarge, prFree, 0, 0, 0.5, 0);
      b3 := false;
      
      for t:=Low(TTypeVentilo) to High(TTypeVentilo) do begin
        Locaux := TList.Create;
        b := false;
        b2 := false;

        //hottes
        for j:=0 to Projet.Reseau(i).Hottes.Count-1 do begin
          case t of
            cEXT,
            cCOMP : b2 := true;
            cIND : b2 := (hIND in TCAD_Hotte(Projet.Reseau(i).Hottes[j]).TypeHotte);
          end;
          if (not b) and b2 then begin
            b := true;
            VPE.GridFirst([VPE.Cellule('\b ' + cTypeVentiloStr[t], prLeftMargin, 0, prLeftMargin, 2.2),
                           VPE.Cellule('', prLastRight, 0, prLastRight, 6.3),
                           VPE.Cellule('\b code', prLastRight, 0, prLastRight, 1.5),
                           VPE.Cellule('\b qt�'),
                           VPE.Cellule('\b d�bit'),
                           VPE.Cellule('\b pdc'),
                           VPE.Cellule('\b conso.'),
                           VPE.Cellule('\b bruit Lw'),
                           VPE.Cellule('\b vitesse')
                          ]);
          end;
          if (TTypeHotte(t) in TCAD_Hotte(Projet.Reseau(i).Hottes[j]).TypeHotte) then begin
            VPE.GridNext([TCAD_Hotte(Projet.Reseau(i).Hottes[j]).NomIlot,
                          TCAD_Hotte(Projet.Reseau(i).Hottes[j]).LocalBASE.Nom,
                          '',
                          '',
                          Formate(TCAD_Hotte(Projet.Reseau(i).Hottes[j]).FCalculs.Interne.Q[t].DebitReel, rM3Heure),
                          Formate(TCAD_Hotte(Projet.Reseau(i).Hottes[j]).FCalculs.Interne.Q[t].PdC, rPa),
                          '',
                          '',
                          ''
                         ]);
          end;
          if (Locaux.IndexOf(TCAD_Hotte(Projet.Reseau(i).Hottes[j]).LocalBASE) = -1) then Locaux.Add(TCAD_Hotte(Projet.Reseau(i).Hottes[j]).LocalBASE);
        end;

        if b then begin
          //r�seau client
          VPE.GridNext(['r�seau',
                        '',
                        '',
                        '',
                        '',
                        Formate(Projet.Reseau(i).ValeurIdxIndexed[cReseau_Ventilo_PdCclient, Ord(t)], rPa),
                        '',
                        '',
                        ''
                       ]);

          //r�cup�rateur
          case t of
            cEXT : begin
              case TTypeRecup(Projet.Reseau(i).ValeurIdx[cReseau_Recup_Model]) of
                trNone : VPE.GridNext(['r�cup�rateur',
                                       'pas de r�cup�rateur impact�',
                                       '',
                                       '',
                                       '',
                                       '',
                                       '',
                                       '',
                                       ''
                                      ]);
                trKoox : VPE.GridNext(['r�cup�rateur',
                                       '',
                                       '',
                                       '',
                                       '',
                                       Formate(Projet.Reseau(i).FCalculsRecup.Koox.CalculsDLL.OUTDPespulsa, rPa),
                                       '',
                                       '',
                                       ''
                                      ]);
                trLago : VPE.GridNext(['r�cup�rateur',
                                       '',
                                       '',
                                       '',
                                       '',
                                       Formate(Projet.Reseau(i).FCalculsRecup.Lago.Calculs.PdCsurAir, rPa),
                                       '',
                                       '',
                                       ''
                                      ]);
              end;
            end;

            cCOMP : begin
              for j:=0 to Locaux.Count-1 do begin
                VPE.GridNext(['diffusion',
                              TBase_Local(Locaux[j]).Nom,
                              '',
                              '',
                              Formate(TBase_Local(Locaux[j]).Diffuseur_DebitRestant, rM3Heure),
                              Formate(XLS_Diffuseur_PdC(TBase_Local(Locaux[j]).Diffuseur_DebitNominal, TBase_Local(Locaux[j]).Diffuseur_DebitGlobal, TBase_Local(Locaux[j]).Diffuseur_NbrNecessaire), rPa),
                              '',
                              '',
                              ''
                             ]);
              end;
              case TTypeRecup(Projet.Reseau(i).ValeurIdx[cReseau_Recup_Model]) of
                trNone : VPE.GridNext(['r�cup�rateur',
                                       'pas de r�cup�rateur impact�',
                                       '',
                                       '',
                                       '',
                                       '',
                                       '',
                                       '',
                                       ''
                                      ]);
                trKoox : VPE.GridNext(['r�cup�rateur',
                                       '',
                                       '',
                                       '',
                                       '',
                                       Formate(Projet.Reseau(i).FCalculsRecup.Koox.CalculsDLL.OUTDPrinnovo, rPa),
                                       '',
                                       '',
                                       ''
                                      ]);
              end;
            end;
          end;

          //ventilateur
          if (Projet.Reseau(i).ValeurIdxIndexed[cReseau_Ventilo_Modele, Ord(t)] <> '') then begin
            VPE.GridNext(['ventilateur',
                          XML2String(Projet.Reseau(i).FCalculsVentilo.Solutions[t][Projet.Reseau(i).IndexVentiloChoisi(t)].Index, 'LIBELLE'),
                          '',
                          '',
                          Formate(Projet.Reseau(i).FCalculsVentilo.Parametres[t].Debit, rM3Heure),
                          Formate(Projet.Reseau(i).FCalculsVentilo.Solutions[t][Projet.Reseau(i).IndexVentiloChoisi(t)].Pression, rPa),
                          Formate(Projet.Reseau(i).FCalculsVentilo.Solutions[t][Projet.Reseau(i).IndexVentiloChoisi(t)].Conso, rWh),
                          Formate(Projet.Reseau(i).FCalculsVentilo.Solutions[t][Projet.Reseau(i).IndexVentiloChoisi(t)].Bruit, rdBA),
                          Formate(XML2Integer(Projet.Reseau(i).FCalculsVentilo.Solutions[Projet.Reseau(i).FCalculsVentilo.TypeVentilo][Projet.Reseau(i).IndexVentiloChoisi(t)].Index, 'VITESSE'), rVitRot)
                         ]);
            SL := TStringList.Create;
            TBase_WithPanel.TrouveAccessoires(Projet.Reseau(i).FCalculsVentilo.Solutions[t][Projet.Reseau(i).IndexVentiloChoisi(t)].Index, ['ACCESSOIRES', 'ACCESSOIRESELECT'], SL);
            if (SL.Count > 0) then begin
              for j:=0 to SL.Count-1 do begin
                VPE.GridNext([Choice('accessoires', '', j=0),
                              Decoup(SL[j], ' (', 0),
                              Decoup(Decoup(SL[j], ' (', 1), ')', 0),
                              '',
                              '',
                              '',
                              '',
                              '',
                              ''
                             ]);
              end;
            end;
            SL.Destroy;
          end else begin
            VPE.GridNext(['ventilateur',
                          'non s�lectionn�',
                          '',
                          '',
                          '',
                          '',
                          '',
                          '',
                          ''
                         ]);
          end;
        end;
        Locaux.Destroy;
      end;

      //le texte est-il pass� sur la page actuelle ?
      if not VPE.SplitterEnd(prBottomMargin, -1) then begin
        //non => nouvelle page + redessin
        VPE.SplitterRewind;
        VPE.NewPage(VORIENT_PORTRAIT, cMarge);
        Header;
        b3 := true;
      end else begin
        //oui => suite ...
        Break;
      end;
    end;
  end;
  FEndProgress;

  //r�cup�rateurs
  FStepProgress('R�cup�rateurs');
  FBeginProgress(Projet.Reseau_Count);
  for i:=0 to Projet.Reseau_Count-1 do begin
    FStepProgress('R�seau ' + Projet.Reseau(i).Nom);

    VPE.NewPage(VORIENT_PORTRAIT, cMarge);
    Header;
    VPE.WriteRTF('\b \fs20 R�cup�ration d''�nergie - ' + 'R�seau ' + Projet.Reseau(i).Nom, true, hCENTER);
    VPE.WriteRTF('Calcul r�alis� sur la T�C minimale de r�f�rence par rapport � la zone g�ographique et l''altitude sur les tables m�t�o.', false);
    Infos := TValueList.Create;
    Projet.Reseau(i).RecupInfosCalcul(Infos);
    VPE.WriteRTF(ComputeField(['{\ul Conditions de fonctionnement :}', TTypeRecup(Projet.Reseau(i).ValeurIdx[cReseau_Recup_Model]) <> trNone,

                               //Koox + Lago + Actinys
                               'Temp�rature ambiante', sffValeur, Projet.Reseau(i).ValeurIdx_Typed[cReseau_Recup_TCambiante], sffSubIndent, sffTabed, 6000, TTypeRecup(Projet.Reseau(i).ValeurIdx[cReseau_Recup_Model]) in [trKoox, trLago, trActinys],

                               //Koox
                               'Temp�rature de l''air extrait', sffValeur, Projet.Reseau(i).ValeurIdx_Typed[cReseau_Recup_TCextrait_KooX], sffSubIndent, sffTabed, 6000, TTypeRecup(Projet.Reseau(i).ValeurIdx[cReseau_Recup_Model]) = trKoox,
                               'Humidit� relative de l''air extrait', sffValeur, Projet.Reseau(i).ValeurIdx_Typed[cReseau_Recup_Hextrait], sffSubIndent, sffTabed, 6000, TTypeRecup(Projet.Reseau(i).ValeurIdx[cReseau_Recup_Model]) = trKoox,

                               //Lago
                               'Temp�rature de l''air extrait', sffValeur, Projet.Reseau(i).ValeurIdx_Typed[cReseau_Recup_TCextrait_Lago], sffSubIndent, sffTabed, 6000, TTypeRecup(Projet.Reseau(i).ValeurIdx[cReseau_Recup_Model]) = trLago,
                               'Besoins en ECS', sffValeur, Projet.Reseau(i).ValeurIdx_Typed[cReseau_Recup_BesoinsECS], sffSubIndent, sffTabed, 6000, TTypeRecup(Projet.Reseau(i).ValeurIdx[cReseau_Recup_Model]) = trLago,
                               'T�C de l''eau du r�seau urbain', sffValeur, Projet.Reseau(i).ValeurIdx_Typed[cReseau_Recup_TCEauDeVille], sffSubIndent, sffTabed, 6000, TTypeRecup(Projet.Reseau(i).ValeurIdx[cReseau_Recup_Model]) = trLago,
                               'T�C de l''eau en sortie du Lago', sffValeur, Projet.Reseau(i).ValeurIdx_Typed[cReseau_Recup_TCEauSortie], sffSubIndent, sffTabed, 6000, TTypeRecup(Projet.Reseau(i).ValeurIdx[cReseau_Recup_Model]) = trLago,
                               'Pompe secondaire', sffValeur, Projet.Reseau(i).ValeurIdx_Typed[cReseau_Recup_Pompe], sffSubIndent, sffTabed, 6000, TTypeRecup(Projet.Reseau(i).ValeurIdx[cReseau_Recup_Model]) = trLago,
                               'Jours dans l''ann�e', sffValeur, Projet.Reseau(i).ValeurIdx_Typed[cReseau_Recup_Lago_Annuel], sffSubIndent, sffTabed, 6000, TTypeRecup(Projet.Reseau(i).ValeurIdx[cReseau_Recup_Model]) = trLago,
                               'Fonctionnement journalier', sffValeur, 'de ' + Projet.Reseau(i).ValeurIdx_Typed[cReseau_Recup_Lago_Journalier_De] + 'H � ' + Projet.Reseau(i).ValeurIdx_Typed[cReseau_Recup_Lago_Journalier_A] + 'H', sffSubIndent, sffTabed, 6000, TTypeRecup(Projet.Reseau(i).ValeurIdx[cReseau_Recup_Model]) = trLago,

                               //R�sultats
                               '',
                               Infos.Str(' \line ', '{\tx6000 ', '\tab : ', '}', '{\ul ', '}', true, '{\b ', '}', '\li200 o ', 'puce')
                              ]));
    VPE.WriteRTF(ComputeField(['Informations sur le calcul :',
                               Infos.Str(' \line ', '', '', '', '', '', false, '', '', '', '{\li200 ', '}', true)
                              ]));

    Infos.Destroy;
    VPE.Dessin(TagIMAGE(XML2String(Projet.Reseau(i).FCalculsRecup.Index, 'IMAGE3D'), nil, true), prCenter, prCenter, prLastBottomMarge, prBottomMargin, -4, +4, 0, 0);
  end;
  FEndProgress;

  //consommations
  FStepProgress('Consommations');
  VPE.NewPage(VORIENT_PORTRAIT, cMarge);
  Header;
  VPE.WriteRTF('\b \fs20 Consommation �nerg�tique annuelle', true, hCENTER);
  VPE.WriteRTF('Il s''agit ici de faire un calcul estimatif des gains de consommation non pas sur la base de la T�C minimale de r�f�rence, mais sur les T�C aux heures de fonctionnement relev�es par m�t�o France.');

  VPE.WriteRTF(ComputeField(['{\ul Conditions de fonctionnement :}',
                             'Nombre de jours de fonctionnement', sffValeur, Projet.ValeurIdx_Typed[cProjet_Fonctionnement_Annuel] + ' jours/an', sffSubIndent, sffTabed, 6000,
                             'Nombre d''heures par jour', sffValeur, 'de ' + Projet.ValeurIdx_Typed[cProjet_Fonctionnement_Journalier_De] + 'h � ' + Projet.ValeurIdx_Typed[cProjet_Fonctionnement_Journalier_A] + 'h', sffSubIndent, sffTabed, 6000,
                             'Nomnre de jours par semaine', sffValeur, Projet.ValeurIdx_Typed[cProjet_Fonctionnement_Hebdomadaire], sffSubIndent, sffTabed, 6000,
                             'P�riode hivernale', sffValeur, 'de ' + Projet.ValeurIdx_Typed[cProjet_Fonctionnement_Hivernal_Du] + ' � ' + Projet.ValeurIdx_Typed[cProjet_Fonctionnement_Hivernal_Au], sffSubIndent, sffTabed, 6000,
                             'Vacances d''hiver', sffValeur, Projet.ValeurIdx_Typed[cProjet_Fonctionnement_VacancesHiver] + ' semaine(s)', sffSubIndent, sffTabed, 6000
                            ]));
  FBeginProgress(Projet.Reseau_Count);
  for i:=0 to Projet.Reseau_Count-1 do begin
    FStepProgress('R�seau ' + Projet.Reseau(i).Nom);

    Infos := TValueList.Create;
    Projet.Reseau(i).ConsoInfosCalcul(Infos, d);
    VPE.WriteRTF(ComputeField(['{\b \fs20 R�seau ' + Projet.Reseau(i).Nom + ' (consommation annuelle totale = ' + Float2Str(d, cTypeResultat_Digits[rMWHan]) + ' ' + cTypeResultat_Unite[rMWHan]  + ')}',
                               '',
                               Infos.Str(' \line ', '{\li200 \tx6000 o ', '\tab : ', '}', '{\ul ', '}', true, '', '', '', 'puce')
                              ]), true);
    if (i = Projet.Reseau_Count-1) then
      VPE.WriteRTF(ComputeField(['Informations sur le calcul :',
                                 Infos.Str(' \line ', '', '', '', '', '', false, '', '', '', '{\li200 ', '}', true)
                                ]));
    Infos.Destroy;
  end;
  FEndProgress;

  //synoptiques
  FStepProgress('Synoptiques');
  FBeginProgress(Projet.Reseau_Count);
  for i:=0 to Projet.Reseau_Count-1 do begin
    FStepProgress('R�seau ' + Projet.Reseau(i).Nom);

    for j:=1 to Projet.Reseau(i).Synoptique_NbrPages do begin
      VPE.NewPage(VORIENT_LANDSCAPE, cMarge);
      Header;
      VPE.WriteRTF('\b \fs20 Synoptique du r�seau ' + Projet.Reseau(i).Nom + ' (page '+IntToStr(j)+'/'+IntToStr(Projet.Reseau(i).Synoptique_NbrPages)+')', true, hCENTER);
      VPE.Dessin(TagSYNOPTIQUE(Projet.Reseau(i), j), prLeftMargin, prRightMargin, prLastBottom, prBottomMargin, 0, 0, 0.05, -1);
    end;
  end;
  FEndProgress;

  //annexes
  FStepProgress('Annexes');
  Ids := TIntegerList.Create;
  RecAnnexe(Projet, Ids);
  if (Ids.Count > 0) then begin
    VPE.NewPage(VORIENT_PORTRAIT, cMarge);
    Header;
    VPE.WriteRTF('\b \fs20 Annexes techniques', true, hCENTER);
    while (Ids.Count > 0) do begin
      i := Ids.Count-1;
      s2 := XML2String(Ids[i], 'FICHETEC');
      s := '';
      while (i >= 0) do begin
        if StrEqual(s2, XML2String(Ids[i], 'FICHETEC')) then begin
          s := s + '{\b ' + XML2String(Ids[i], 'LIBELLE') + '} (code produit : ' + XML2String(Ids[i], 'INFOBULLE') + ') \line ';
          Ids.Delete(i);
        end;
        dec(i);
      end;
      VPE.WriteRTF(s + 'Fiche technique : {\colortbl; \red0\green0\blue255;}{\ul \cf1' + s2 + '}', false, hLEFT, prLeftMargin, prRightMargin, prLastBottom, prFree, 0, 0, 0.5, 0);
    end;
  end;
  Ids.Destroy;

  //footers
  FStepProgress('Ecriture du PDF');
  for i:=1 to VPE.PageCount do begin
    VPE.SetCurrentPage(i);
    VPE.WriteRTF('Compte : ' + TFormLoginEspacePro.Login, false, hLEFT, prLeftMargin, prRightMargin, prBottomMargin, prBottomMargin, 0, 0, -0.6, 0);
    VPE.WriteRTF('Page ' + IntToStr(i) + '/' + IntToStr(VPE.PageCount), false, hRIGHT, prLeftMargin, prRightMargin, prBottomMargin, prBottomMargin, 0, 0, -0.6, 0);
    VPE.LigneHorizontale(prLastTop, -0.1);
  end;

  //PDF
  s := TempPath + String2Filename(Projet.Nom) + '.pdf';
  VPE.WriteDoc(s);
  VPE.CloseDoc;
  VPE.Destroy;
  FEndProgress;
  ShellExecute(0, 'open', PChar(s), '', '', SW_SHOWNORMAL);
  UnLock;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.CreatePDF_CallbackDessin(Tag : integer; DC : HDC; Left, Right, Top, Bottom : integer);
var Roots : TList;
    VC : TViloCanvas;
    T : PTagDessin;
    Img : TAdvGDIPPicture;
begin
  T := PTagDessin(Tag);
  case T.TypeUDO of
    udoSYNOPTIQUE : begin
      T.Reseau.Synoptique_Dessiner(DC, Left, Right, Top, Bottom, T.Page);
    end;

    udoCAD : begin
      Roots := TList.Create;
      Roots.Add(T.Local);
      VC := TViloCanvas.Create(DC);
      FormCAD.Print(Roots, VC, Rect(Left, Top, Right, Bottom), rm2D_Z, true);
      VC.Destroy;
      Roots.Destroy;
    end;

    udoIMAGE : begin
      if (T.ImageStr <> '') then
        Img := ZIP_ZIP2Image(ZIP_Images, T.ImageStr, true)
      else
      if (T.Image <> nil) then begin
        Img := TAdvGDIPPicture.Create(nil);
        Img.Picture.Assign(T.Image.Picture);
      end else Exit;
      Img.Left := Left;
      Img.Top := Top;
      Img.AutoSize := false;
      Img.Height := Bottom-Top;
      Img.Width := Right-Left;
      Img.Stretch := true;
      Img.Proportional := true;
      Img.Center := T.Centered;
      Img.Perform(WM_PAINT, WParam(DC), 0);
      Img.Destroy;
    end;
  end;
  Dispose(T);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.SetFichierCourant(Valeur : string);
begin
  if (FichierCourant <> Valeur) then begin
    FFichierCourant := Valeur;
    Caption := cLogiciel + ' v' + cVersion + ' : ' + FFichierCourant;
  end;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.New;
begin
  Lock;

  FProjet.Free;
  FProjet := TCAD_Projet.Create;
  LinkProjet(FProjet);
  FProjet.ValeurIdx[cProjet_Redacteur_Entreprise]    := FDefaults.Projet.Redacteur_Entreprise;
  FProjet.ValeurIdx[cProjet_Redacteur_Departement]   := FDefaults.Projet.Redacteur_Departement;
  FProjet.ValeurIdx[cProjet_Redacteur_Commune]       := FDefaults.Projet.Redacteur_Commune;
  FProjet.ValeurIdx[cProjet_Redacteur_Interlocuteur] := FDefaults.Projet.Redacteur_Interlocuteur;
  Projet_Locaux_Add.OnClick(nil);
  FProjet.Reseau_Activate(FProjet.Reseau_Add);
  ChangeMode(mProjet);
  FProjet.HasBeenUnModifiedWithChilds;
  FichierCourant := '�tude non enregistr�e ...';

  UnLock;
end;
//*************************************************************************************************************************************
function TFormCuisiNote.Save(Fichier : string) : boolean;
var S : TMemoryStream;
begin
  Result := false;
  Lock;
  S := TMemoryStream.Create;
  try
    vLastError := '';
    Result := FProjet.Save(S);
    if Result then begin
      DeleteFile(Fichier);
      ZIP_Stream2ZIP(S, Fichier, cEtudeZip, zclNormal);
      AddRecent(Fichier);
      AfficheMessage('Sauvegarde r�ussie', MB_OK + MB_ICONINFORMATION);
    end else raise Exception.Create(vLastError);
  except
    on E:Exception do AfficheMessage('Erreur de sauvegarde :<br>' + E.Message, MB_OK + MB_ICONERROR);
  end;
  S.Destroy;
  UnLock;
end;
//*************************************************************************************************************************************
function TFormCuisiNote.Load(Fichier : string) : boolean;
var Projet : TCAD_Projet;
    S : TMemoryStream;
begin
  Result := false;
  Lock;
  FBeginProgress2(3, 'Chargement de l''�tude', false, 'd�compression', false, ImageLogo);
  try
    //d�compression
    vLastError := '';
    S := ZIP_File2Stream(Fichier, cEtudeZip, false);

    //cr�ation d'un projet temporaire
    FStepProgress('lecture');
    Projet := TCAD_Projet.Create;
    LinkProjet(Projet);
    ChangeMode(mProjet);
    if Projet.Load(S) then begin
      //Ok => permutation
      FProjet.Free;
      FProjet := Projet;
      FStepProgress('calculs');
      FBeginProgress2(FProjet.TotalCount*2, '', false, '', false, ImageLogo);
      try
        FProjet.LoadFinishedXML;
        FEndProgress;
        AdvOfficeTabSet_Reseaux.ActiveTabIndex := 0;
        Result := true;
      except
        FEndProgress;
        raise;
      end;
    end else begin
      //Ko => destruction
      Projet.Destroy;
      if (FProjet <> nil) then LinkProjet(FProjet);
      raise Exception.Create(vLastError);
    end;
    S.Destroy;
    if (FProjet <> nil) then FProjet.ToForm;
    FStepProgress;
  except
    on E:Exception do AfficheMessage('Erreur de lecture :<br>' + E.Message, MB_OK + MB_ICONERROR);
  end;
  FEndProgress;
  UnLock;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.AddRecent(Fichier : string);
var i : integer;
    M : TMenuItem;
begin
  while (FFichiersRecents.IndexOf(Fichier) > -1) do FFichiersRecents.Delete(FFichiersRecents.IndexOf(Fichier));
  FFichiersRecents.Insert(0, Fichier);
  if (FFichiersRecents.Count > 10) then FFichiersRecents.Delete(10);
  PopupMenuRecent.Items.Clear;
  for i:=0 to FFichiersRecents.Count-1 do begin
    M := TMenuItem.Create(PopupMenuRecent);
    M.Caption := FFichiersRecents[i];
    M.ImageIndex := 0;
    M.OnClick := RecentClick;
    PopupMenuRecent.Items.Add(M);
  end;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.OpenFile(Fichier : string);
begin
  if Load(Fichier) then begin
    FichierCourant := Fichier;
    AddRecent(Fichier);
  end;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.MouseLeaveCAD(Sender : TObject);
var EV : TEventMode;
begin
  if (FormCAD.Global_ObjetCourantH <> nil) then begin
    if FormCAD.Global_ObjetCourantH.InheritsFrom(TBase_Variables) then TBase_Variables(FormCAD.Global_ObjetCourantH).MouseUp(mbLeft, []);
    EV := FormCAD.EventMode;
    FormCAD.AbortAllActions;
    FormCAD.EventMode := EV;
  end;
  FormCAD.ViewportHorizontalMouseLeave(Sender);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.RecentClick(Sender : TObject);
begin
  if (FLockCount = 0) and (ChangeMode(mSaveCurrent) <> cmAbort) then OpenFile(TMenuItem(Sender).Caption);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.PanelToolbarResize(Sender: TObject);
begin
  AdvSmoothPanelToolbar.Left := (AdvSmoothPanelToolbar.Parent.ClientWidth - AdvSmoothPanelToolbar.Width) div 2;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.LOCAL_AdvPanelGroupRightMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  case TAdvPanel(Sender).Caption.Position of
    AdvPanel.cpTop : begin
      if (Y < TAdvPanel(Sender).Caption.Height-1) and (X > TAdvPanel(Sender).Width-14) then PushCursor(crHandPoint)
                                                                                       else PopCursor;
    end;

    AdvPanel.cpLeft : begin
      if (X < TAdvPanel(Sender).Caption.Height-1) and (Y < 24) then PushCursor(crHandPoint)
                                                               else PopCursor;
    end;
  end;
end;                     
//*************************************************************************************************************************************
procedure TFormCuisiNote.LOCAL_AdvPanelGroupRightMouseLeave(Sender: TObject);
begin
  PopCursor;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_CAD_ArboClick(Sender: TObject);
begin
  FormCAD.Action_Debug_GLSceneTree.Execute;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_CAD_SelectClick(Sender: TObject);
begin
  FormCAD.AbortAllActions;
  FormCAD.Action_Aides_Aucune.Execute;
  FormCAD.Action_Aides_Actives.Checked := false;
  FormCAD.Action_Modes_Move.Execute;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_CAD_RotateClick(Sender: TObject);
begin
  FormCAD.AbortAllActions;
  FormCAD.Action_Aides_Aucune.Execute;
  FormCAD.Action_Aides_Angles90.Checked := true;
  FormCAD.Action_Aides_Angles00.Checked := true;
  FormCAD.Action_Aides_Actives.Checked := true;
  FormCAD.Action_Modes_Rotate.Execute;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_CAD_PanClick(Sender: TObject);
begin
  FormCAD.AbortAllActions;
  FormCAD.Action_Modes_Pan.Execute;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_CAD_ZoomInClick(Sender: TObject);
begin
  FormCAD.Action_ZoomIn.Execute;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_CAD_ZoomOutClick(Sender: TObject);
begin
  FormCAD.Action_ZoomOut.Execute;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_CAD_ZoomAllClick(Sender: TObject);
begin
  FormCAD.Action_Proprietes_Centrer.Execute;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_CAD_ParamsClick(Sender: TObject);
var P : TPoint;
begin
  P := TControl(Sender).ClientToScreen(Point(0, TControl(Sender).Height));
  PopupMenuParamsCAD.Popup(P.X, P.Y);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_ProjetClick(Sender: TObject);
begin
  ChangeMode(mProjet, Sender);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_LocalClick(Sender: TObject);
begin
  ChangeMode(mLocal, Sender);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_VentilationClick(Sender: TObject);
begin
  ChangeMode(mReseaux, Sender);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_RecapitulatifClick(Sender: TObject);
begin
  ChangeMode(mPDF, Sender);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_ConsommationClick(Sender: TObject);
begin
  ChangeMode(mConso, Sender);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_SynoptiqueClick(Sender: TObject);
begin
//  ChangeMode(mSynopt, Sender);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_PieceClick(Sender: TObject);
begin
  ChangeMode(mArchi, Sender);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_CuissonClick(Sender: TObject);
begin
  ChangeMode(mCuisson, Sender);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_HottesClick(Sender: TObject);
begin
  ChangeMode(mHottes, Sender);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_DiffuseursClick(Sender: TObject);
begin
  ChangeMode(mDiffuseurs, Sender);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_LocalPrevClick(Sender: TObject);
begin
  ChangeMode(mLocalPrec, Sender);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_LocalSuivClick(Sender: TObject);
begin
  ChangeMode(mLocalSuiv, Sender);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.FormShortCut(var Msg: TWMKey; var Handled: Boolean);
begin
{BUG : emp�che de saisir des "Z"
  //Ctrl+Z => UNDO
  if (Msg.CharCode = Ord('Z')) and (HiWord(Msg.KeyData) and VK_CONTROL <> 0) then begin
    if ImageEx_Undo.Enabled then ImageEx_Undo.OnClick(nil);
    Handled := true;
  end;
}
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_ExitClick(Sender: TObject);
begin
  Close;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.FontDialogMursApply(Sender: TObject; Wnd: HWND);
begin
  FontDialogMursClose(nil);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.FontDialogMursClose(Sender: TObject);
begin
  FCuisiNoteRoot.RefreshWithChilds(false ,true, rtStructure);
  FormCAD.ViewportHorizontal.Invalidate;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageExCollapsClick(Sender: TObject);
begin
  LOCAL_AdvPanelGroupRight.Collaps := not LOCAL_AdvPanelGroupRight.Collaps;
  ImageExCollaps.Visible := not LOCAL_AdvPanelGroupRight.Collaps;
  ImageExUnCollaps.Visible := LOCAL_AdvPanelGroupRight.Collaps;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.Policedecotationdesmurs1Click(Sender: TObject);
begin
  FontDialogMurs.Execute;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.Policedecotationdesobjets1Click(Sender: TObject);
begin
  FontDialogObjets.Execute;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_CAD_23DClick(Sender: TObject);
begin
  if FormCAD.Action_Modes_3D.Checked then FormCAD.Action_Modes_2D_Z.Execute
                                     else FormCAD.Action_Modes_3D.Execute;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageEx_RESEAUX_AddClick(Sender: TObject);
begin
  PushCursor(crHourGlass);
  FormCuisiNote.Enabled := false;
  FProjet.Reseau_Add;
  FormCuisiNote.Enabled := True;
  PopCursor;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.AdvOfficeTabSet_ReseauxChange(Sender: TObject);
begin
  FProjet.Reseau_Activate(AdvOfficeTabSet_Reseaux.AdvOfficeTabs[AdvOfficeTabSet_Reseaux.ActiveTabIndex]);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.AdvOfficeTabSet_ReseauxTabClose(Sender: TObject; TabIndex: Integer; var Allow: Boolean);
begin
  FProjet.Reseau_Delete(AdvOfficeTabSet_Reseaux.AdvOfficeTabs[TabIndex]);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (ActiveControl = FormCAD.PanelResize) then FormCAD.OnKeyDown(Sender, Key, Shift);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (ActiveControl = FormCAD.PanelResize) then FormCAD.FormKeyUp(Sender, Key, Shift);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (ActiveControl = FormCAD.PanelResize) then FormCAD.FormKeyPress(Sender, Key);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.AdvOfficePager_ReseauResize(Sender: TObject);
var R, R2 : TRect;
begin
  AdvOfficePager_Reseau.TabSettings.StartMargin := 0;
  R := AdvOfficePager_Reseau.GetTabRect(0);
  R2 := AdvOfficePager_Reseau.GetTabRect(AdvOfficePager_Reseau.AdvPageCount-1);
  AdvOfficePager_Reseau.TabSettings.StartMargin := (AdvOfficePager_Reseau.Width - (R2.Right - R.Left)) div 2;

  R := AdvOfficePager_Reseau.GetTabRect(AdvOfficePage_Reseau_Ventilo.PageIndex);
  ComboBoxVentilo.Left := R.Left + ((R.Right-R.Left-ComboBoxVentilo.Width) div 2);
  R := AdvOfficePager_Reseau.GetTabRect(AdvOfficePage_Reseau_Syno.PageIndex);
  ComboBoxPages.Left := R.Left + ((R.Right-R.Left-ComboBoxPages.Width) div 2);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.AdvOfficePager_ReseauChange(Sender: TObject);
begin
  if (Application.MainForm <> nil) and (Application.MainForm.Visible) then AdvOfficePager_Reseau.ActivePage.SetFocus;
  ComboBoxVentilo.Invalidate;
  ComboBoxPages.Invalidate;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.LOCAL_AdvHorizontalPolyListEltsMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
  MousePos := LOCAL_AdvHorizontalPolyListElts.ScreenToClient(MousePos);
  if (MousePos.X > 0) and
     (MousePos.X < LOCAL_AdvHorizontalPolyListElts.Width) and
     (MousePos.Y > 0) and
     (MousePos.Y < LOCAL_AdvHorizontalPolyListElts.Height) then
  begin
    LOCAL_AdvHorizontalPolyListElts.HorzScrollPos := LOCAL_AdvHorizontalPolyListElts.HorzScrollPos - (WheelDelta div 2);
    LOCAL_AdvHorizontalPolyListElts.UpdateItemPositions;
  end;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.LOCAL_AdvHorizontalPolyListEltsMouseEnter(Sender: TObject);
begin
  LOCAL_AdvHorizontalPolyListElts.SetFocus;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.Grid_IlotsGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  HAlign := taCenter;
  VAlign := vtaCenter;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.Grid_IlotsCanClickCell(Sender: TObject; ARow, ACol: Integer; var Allow: Boolean);
begin
  Allow := (ARow > -1) and TAdvStringGrid(Sender).RowEnabled[ARow];
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ComboBoxVentiloClick(Sender: TObject);
var P : TPoint;
begin
  P := TAdvGlassButton(Sender).ClientToScreen(Point(0, TAdvGlassButton(Sender).Height));
  TAdvGlassButton(Sender).PopupMenu.Popup(P.X, P.Y);
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageFAClick(Sender: TObject);
begin
  XLS_ShowHide;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.ImageExAproposClick(Sender: TObject);
var FormAbout : TFormAbout;
begin
  Application.CreateForm(TFormAbout, FormAbout);
  FormAbout.HTMLabel1.HTMLText.Text := '<p align="center">' +
                                       '<b>' +
                                       '<font size="14">' + cLogiciel + '</font>' + '<br>' +
                                       'version ' + cVersion + '<br>' +
                                       '</b>' +
                                       'Logiciel de dimensionnement de hottes de cuisines' +
                                       '</p>';
  FormAbout.ShowModal;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.LOCAL_AdvHorizontalPolyListEltsItemReorder(Sender: TObject; AItem, ADropItem: TCustomItem; var Allow: Boolean);
begin
  Allow := false
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.LOCAL_AdvHorizontalPolyListEltsItemSelect(Sender: TObject; Item: TCustomItem; var Allow: Boolean);
begin
  Allow := false;
end;
//*************************************************************************************************************************************
//DCEF3 procedure TFormCuisiNote.Chromium1BeforeResourceLoad(Sender: TObject; const browser: ICefBrowser; const frame: ICefFrame; const request: ICefRequest; out Result: Boolean);
procedure TFormCuisiNote.Chromium1BeforeResourceLoad(Sender: TObject; const browser: ICefBrowser; const request: ICefRequest; var redirectUrl: ustring; var resourceStream: ICefStreamReader; const response: ICefResponse; loadFlags: Integer; out Result: Boolean);
begin
  Result := not StrIsStartingWith(request.Url, 'http://www.france-air.com/');
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.Chromium1BeforeMenu(Sender: TObject; const browser: ICefBrowser; const menuInfo: PCefMenuInfo; out Result: Boolean);
begin
  Result := true;
end;
//*************************************************************************************************************************************
procedure TFormCuisiNote.Projet_Locaux_AideClick(Sender: TObject);
begin
  Projet.OnAideLocaux;
end;
//*************************************************************************************************************************************

{
initialization
  if Eurekalog_Enabled then begin
    CurrentEurekaLogOptions.SaveLogFile := false;
    CurrentEurekaLogOptions.WebSendMode := wsmMantis;
    CurrentEurekaLogOptions.WebURL := 'http://www.bbs-slama.com/mantis';
    CurrentEurekaLogOptions.TrakerUserID := 'eurekalog';
    CurrentEurekaLogOptions.TrakerPassword := '?fgbbs63';
    CurrentEurekaLogOptions.TrakerCategory := 'Vilo';
    CurrentEurekaLogOptions.TrakerAssignTo := 'Vilo';
    CurrentEurekaLogOptions.TrakerProject := 'CuisiNote';
    CurrentEurekaLogOptions.CommonSendOptions := [sndShowSendDialog, sndSendEntireLog, sndSendScreenshot];
    CurrentEurekaLogOptions.ExceptionDialogType := edtMSClassic;
    CurrentEurekaLogOptions.ExceptionDialogOptions := [edoSendErrorReportChecked, edoAttachScreenshotChecked, edoShowInTopMostMode, edoShowSendErrorReportOption];
    CurrentEurekaLogOptions.CustomizedTexts[mtMSDialog_ErrorMsgCaption] := cLogiciel + ' ' + cVersion + ' a rencontr� un probl�me.';
    CurrentEurekaLogOptions.CustomizedTexts[mtMSDialog_PleaseCaption] := 'Pour nous aider � am�liorer ' + cLogiciel + ', vous pouvez nous faire parvenir ce rapport d''erreur en cliquant sur "Envoyer".';
    CurrentEurekaLogOptions.CustomizedTexts[mtMSDialog_DescriptionCaption] :=  #13#10 + 'Pour un suivi personnalis�, indiquez votre adresse mail :';
    CurrentEurekaLogOptions.CustomizedTexts[mtMSDialog_EmailCaption] := '';
    CurrentEurekaLogOptions.CustomizedTexts[mtMSDialog_SendButtonCaption] := 'Envoyer';
    CurrentEurekaLogOptions.CustomizedTexts[mtMSDialog_NoSendButtonCaption] := 'Ne pas envoyer';
  end;
}


end.

