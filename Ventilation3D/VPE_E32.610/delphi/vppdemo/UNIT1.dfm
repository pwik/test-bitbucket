�
 TFORM1 0p  TPF0TForm1Form1Left� Top� CaptionVirtual Print EngineClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Menu	MainMenu1OldCreateOrder	PositionpoScreenCenter
OnActivateFormActivateOnClose	FormCloseOnCreate
FormCreatePixelsPerInch`
TextHeight TLabelLabel1Left� Top� Width,HeightKCaptionVPEngineColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold ParentColor
ParentFont  TLabelLabel2Left:Top� WidthHeight	AlignmenttaRightJustifyCaptionVCLFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFont  	TVPEngine	ClickableLeft0TopXWidthYHeightIBorderStylebsNoneTabOrderVisibleOnDestroyWindowClickableDestroyWindowOnRequestPrintRequestPrintOnBeforeMail
BeforeMailOnAfterMail	AfterMailOnObjectClickedClickableObjectClickedCaptionClickable ReportExternalWindow	PageOrientationVORIENT_PORTRAIT
PageFormat	VPAPER_A4	PageWidth      @�
@
PageHeight      ��
@Rulers	RulersMeasurecmGridModeh @[�j ���  P�l&�� ���Z��P��;�Xt�Z��[ÐS�؃��   u3ɲ��F��ڞ���qZ�����  P�H&�� ��[Í@ ���   uGridVisibleEnableHelpRouting	StatusBar	ToolBar	tbAbouttbClose	tbGrid	tbHelp
tbNavigate	tbOpen	tbSave	tbPrint	EnablePrintSetupDialog	tbScale		PaperView	DocFileReadOnlyPageScroller	tbMail	StatusSegment	PageScrollerTracking	PreviewCtrlJumpTop  	TVPEngineRTFLeft0TopWidthHeightIBorderStylebsNoneTabOrder TabStop	VisibleOnDestroyWindowRTFDestroyWindowOnRequestPrintRequestPrintOnHelpRTFHelpOnBeforeMail
BeforeMailOnAfterMail	AfterMailCaptionRTFExternalWindowPageOrientationVORIENT_PORTRAIT
PageFormat	VPAPER_A4	PageWidth      @�
@
PageHeight      ��
@Rulers	RulersMeasurecmGridModeh @[�j ���  P�l&�� ���Z��P��;�Xt�Z��[ÐS�؃��   u3ɲ��F��ڞ���qZ�����  P�H&�� ��[Í@ ���   uGridVisibleEnableHelpRouting		StatusBar	ToolBar	tbAbout	tbClose	tbGrid	tbHelp	
tbNavigate	tbOpen	tbSave	tbPrint	EnablePrintSetupDialog	tbScale		PaperView	DocFileReadOnlyPageScroller	tbMail	StatusSegment	PageScrollerTracking	PreviewCtrlJumpTop  	TVPEnginePictureImportLeft�TopXWidthYHeightIBorderStylebsNoneTabOrderVisibleOnRequestPrintRequestPrintOnBeforeMail
BeforeMailOnAfterMail	AfterMailCaptionPicture ImportExternalWindow	PageOrientationVORIENT_PORTRAIT
PageFormat	VPAPER_A4	PageWidth      @�
@
PageHeight      ��
@Rulers	RulersMeasurecmGridModeh @[�j ���  P�l&�� ���Z��P��;�Xt�Z��[ÐS�؃��   u3ɲ��F��ڞ���qZ�����  P�H&�� ��[Í@ ���   uGridVisibleEnableHelpRouting	StatusBar	ToolBar	tbAbout	tbClose	tbGrid	tbHelp	
tbNavigate	tbOpen	tbSave	tbPrint	EnablePrintSetupDialog	tbScale		PaperView	DocFileReadOnlyPageScroller	tbMail	StatusSegment	PageScrollerTracking	PreviewCtrlJumpTop  	TVPEngineUDOLeft� TopXWidthYHeightABorderStylebsNoneTabOrderVisibleOnDestroyWindowUDODestroyWindowOnRequestPrintRequestPrintOnBeforeMail
BeforeMailOnAfterMail	AfterMail
OnUDOPaintUDOUDOPaintCaptionUDO DemoExternalWindow	PageOrientationVORIENT_PORTRAIT
PageFormat	VPAPER_A4	PageWidth      @�
@
PageHeight      ��
@Rulers	RulersMeasurecmGridModeh @[�j ���  P�l&�� ���Z��P��;�Xt�Z��[ÐS�؃��   u3ɲ��F��ڞ���qZ�����  P�H&�� ��[Í@ ���   uGridVisibleEnableHelpRouting	StatusBar	ToolBar	tbAbout	tbClose	tbGrid	tbHelp	
tbNavigate	tbOpen	tbSave	tbPrint	EnablePrintSetupDialog	tbScale		PaperView	DocFileReadOnlyPageScrollertbMail	StatusSegment	PageScrollerTracking	PreviewCtrlJumpTop  	TVPEngineScaleLeft TopXWidthYHeightABorderStylebsNoneTabOrderVisibleOnDestroyWindowScaleDestroyWindowOnRequestPrintRequestPrintOnBeforeMail
BeforeMailOnAfterMail	AfterMailCaptionScale-to-GrayExternalWindow	PageOrientationVORIENT_PORTRAIT
PageFormat	VPAPER_A4	PageWidth      @�
@
PageHeight      ��
@Rulers	RulersMeasurecmGridModeh @[�j ���  P�l&�� ���Z��P��;�Xt�Z��[ÐS�؃��   u3ɲ��F��ڞ���qZ�����  P�H&�� ��[Í@ ���   uGridVisibleEnableHelpRouting	StatusBar	ToolBar	tbAbout	tbClose	tbGrid	tbHelp	
tbNavigate	tbOpen	tbSave	tbPrint	EnablePrintSetupDialog	tbScale		PaperView	DocFileReadOnlyPageScroller	tbMail	StatusSegment	PageScrollerTracking	PreviewCtrlJumpTop  	TVPEnginePictureExportLefthTopXWidthYHeightABorderStylebsNoneTabOrderVisibleOnDestroyWindowPictureExportDestroyWindowOnRequestPrintRequestPrintOnBeforeMail
BeforeMailOnAfterMail	AfterMailOnObjectClickedPictureExportObjectClickedCaptionPicture ExportExternalWindow	PageOrientationVORIENT_PORTRAIT
PageFormat	VPAPER_A4	PageWidth      @�
@
PageHeight      ��
@Rulers	RulersMeasurecmGridModeh @[�j ���  P�l&�� ���Z��P��;�Xt�Z��[ÐS�؃��   u3ɲ��F��ڞ���qZ�����  P�H&�� ��[Í@ ���   uGridVisibleEnableHelpRouting	StatusBar	ToolBar	tbAbout	tbClose	tbGrid	tbHelp	
tbNavigate	tbOpen	tbSave	tbPrint	EnablePrintSetupDialog	tbScale		PaperView	DocFileReadOnlyPageScroller	tbMail	StatusSegment	PageScrollerTracking	PreviewCtrlJumpTop  	TVPEngineBar2DLeft@TopXWidthYHeightABorderStylebsNoneTabOrderVisibleCaption2D BarcodesExternalWindow	PageOrientationVORIENT_PORTRAIT
PageFormat	VPAPER_A4	PageWidth      @�
@
PageHeight      ��
@Rulers	RulersMeasurecmGridModeInForegroundGridVisibleEnableHelpRouting	StatusBar	ToolBar	tbAbout	tbClose	tbGridtbHelp	
tbNavigate	tbOpen	tbSave	tbPrint	EnablePrintSetupDialog	tbScale		PaperView	DocFileReadOnlyPageScroller	tbMail	StatusSegment	PageScrollerTracking	PreviewCtrlJumpTop  	TMainMenu	MainMenu1LeftTop 	TMenuItemFile1Caption&File 	TMenuItemExit1CaptionE&xitOnClick
Exit1Click   	TMenuItemDemos1Caption&Demos 	TMenuItemRTF1Caption&Rich Text Format (RTF)OnClick	RTF1Click  	TMenuItem
Clickable1Caption&Clickable Objects + ChartsOnClickClickable1Click  	TMenuItemN2DBarcodesCaption&2D-BarcodesOnClickN2DBarcodesClick  	TMenuItemUDODemo1Caption&User Defined Objects (UDO)OnClickUDODemo1Click  	TMenuItemScale1Caption&Scale-to-Gray TechnologyOnClickScale1Click  	TMenuItemPictureExport1Caption&Picture ExportOnClickPictureExport1Click     