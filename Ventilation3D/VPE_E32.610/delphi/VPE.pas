unit VPE;


interface

uses
  Math, Windows, Controls, ExtCtrls, VPE_VCL, Grids, Graphics, Classes, JPeg;

type

  TRectVPE = record
           Top,
           Bottom,
           Left,
           Right : double;
           end;
           
  TAlignment = (taLeftJustify, taRightJustify, taCenter, taFullJustify);
  TPostionHF = (tphfTopLeft, tphfTopCenter, tphfTopRight, tphfBottomLeft, tphfBottomCenter, tphfBottomRight, tphfAbsent);

    TCelluleImp = Class
      Constructor Create(_Texte: String; _Font: TFont; _Largeur: SmallInt = 0; _PenColor: TColor = clBlack; _BrushColor: TColor = clWhite;
                         _Style: TFontStyles = []; _Align: TAlignment = taCenter);
      Destructor  Destroy; Override;
      Function    GetImgScale(_ColWidth: Extended): Extended; { En Pixels merci }
      Function    GetImgScaleComplete(_ColWidth, _RowHeight: Extended): Extended; { En Pixels merci }
    Private
      FTexte : String;
      FReal  : Extended;
      FInt   : integer;
      CurrentTextHeight : Double;
      RealHeightAfterMerged : Double;
      Procedure SetTexte(Const _Value : String);
      function GetOldAlignment : TAlignment;
      procedure SetOldAlignment(_Value : TAlignment);
      function GetTextAndStyleVPE : String;
      procedure SetBrushVPE(Var _VPE : TVPEngine);
    Public
      IFont       : TFont;
      IPenColor   : TColor;
      IBrushColor : TColor;
      IHorAlign   : TAlignment;
      ILargeur    : SmallInt;
      IImage      : TBitmap;
      IImageMergeable : boolean;
      IImageAdapt : boolean;
      IImageAdaptNbLignes : Integer;
      IMerged     : Boolean;
      IHorMerged  : Boolean;
      ISeparateurBas : boolean;
      ISeparateurDroite : boolean;
      IAngle      : Extended;
      IImgDrawn   : Boolean;
      //todo
      IFormatRTF : Boolean;
      property ITexte : String read FTexte write SetTexte;
      property IFloat : Extended read FReal;
      property IInt   : Integer read FInt;
      property IAlign : TAlignment read GetOldAlignment write SetOldAlignment; // POG : N�cessaire pour compatibilit�
    End;

  TPageOrientation = (VORIENT_PORTRAIT = Ord(VPE_VCL.VORIENT_PORTRAIT),
                      VORIENT_LANDSCAPE = Ord(VPE_VCL.VORIENT_LANDSCAPE));
  THAlign = (hLEFT         = Ord(VPE_VCL.ALIGN_LEFT),
             hRIGHT        = Ord(VPE_VCL.ALIGN_RIGHT),
             hCENTER       = Ord(VPE_VCL.ALIGN_CENTER),
             hJUSTIFIED    = Ord(VPE_VCL.ALIGN_JUSTIFIED),
             hJUSTIFIED_AB = Ord(VPE_VCL.ALIGN_JUSTIFIED_AB),
             hLEFT_CUT     = Ord(VPE_VCL.ALIGN_LEFT_CUT));
  TPosRefX = (prLeftMargin,
              prRightMargin,
              prCenter,
              prLastLeft,
              prLastRight);
  TPosRefY = (prLastBottom,
              prLastBottomMarge,
              prLastTop,
              prTopMargin,
              prBottomMargin,
              prFree
             );
  TCellule = record
               Texte : string;
               RefG : TPosRefX;
               OffsetG : double;
               RefD : TPosRefX;
               OffsetD : double;
             end;
  TVPE_DessinCallback = procedure(Tag : integer; DC : HDC; Left, Right, Top, Bottom : integer) of object;
  TVPE_NewGridPageCallback = procedure of object;
  TVPE_HeaderCallback = procedure of object;
  TVPE_FooterCallback = procedure of object;

  TVPE = class
  private
    CurrentVPE : TVPEngine;
    FHeaderMargePos : Double;
    FFooterMargePos : Double;
    VPE_DessinCallback : TVPE_DessinCallback;
    VPE_NewGridPageCallback : TVPE_NewGridPageCallback;
    VPE_HeaderCallback : TVPE_HeaderCallback;
    VPE_FooterCallback : TVPE_FooterCallback;
    FSplitterStart : integer;
    Logo : Graphics.TBitmap;
    LogoHeight : Double;
    LogoPostion : TPostionHF;
    HeaderLeft : String;
    HeaderCenter : String;
    HeaderRight : String;
    FooterLeft : String;
    FooterCenter : String;
    FooterRight : String;
    FontHeaderLeft : TFont;
    FontHeaderCenter : TFont;
    FontHeaderRight : TFont;
    FontFooterLeft : TFont;
    FontFooterCenter : TFont;
    FontFooterRight : TFont;
    HeaderRenderHeights : array of Double;
    FooterRenderHeights : array of Double;
    function GetFontAndBrush(_Font : TFont; _Brush : TBrush) : String;
    procedure VPEUDOPaint(Sender : TObject);
    function PosRefX(PosRef : TPosRefX; Offset : double) : double;
    function PosRefY(PosRef : TPosRefY; Offset : double) : double;
    procedure GetProportionalImageDimensions(_ImageHeight, _ImageWidth : Integer; Var _RealLeft, _RealTop, _RealRight, _RealBottom : Double; _VertCenter : Boolean = True);
    procedure HeaderFooterNewPage;
    procedure HeaderFooterText(_Texte : String; _Position : TPostionHF; _TextHeight : Double);
    procedure PrintLogo;
  public
    Vignettes : TList;
    procedure OpenDoc(_VPE : TVPEngine; Orientation : TPageOrientation; _Marge : double; _NewGridPageCallback : TVPE_NewGridPageCallback = Nil; _HeaderCallback : TVPE_HeaderCallback = Nil; _FooterCallback : TVPE_FooterCallback = Nil; _CallbackDessin : TVPE_DessinCallback = Nil);
    procedure SetLogo(_Logo : Graphics.TBitmap; _Height : Double; _Postion : TPostionHF);
    procedure SetHeaderFooter(_Texte : String; _Font : TFont; _Position : TPostionHF);
    procedure PrintNbPages(_Position : TPostionHF; _Font : TFont);
    procedure GenereVignettes;
    procedure WriteDoc(Fichier : string);
    procedure CloseDoc;
    procedure NewPage(Orientation : TPageOrientation; _Marge : double);
//    procedure SetOrientation(Orientation : TPageOrientation);
    procedure Preview;
    procedure WriteRTF(const RTF : string;
                       Bordure : boolean = false;
                       HAlign : THAlign = hLEFT;
                       RefX : TPosRefX = prLeftMargin;
                       RefX2 : TPosRefX = prRightMargin;
                       RefY : TPosRefY = prLastBottomMarge;
                       RefY2 : TPosRefY = prFree;
                       OffsetX : double = 0;
                       OffsetX2 : double = 0;
                       OffsetY : double = 0;
                       OffsetY2 : double = 0);
    function Cellule(Texte : string = '';
                     RefG : TPosRefX = prLastRight;
                     OffsetG : double = 0;
                     RefD : TPosRefX = prLastRight;
                     OffsetD : double = NaN) : TCellule;
    procedure Title(_Text : String; _Font : TFont; _Brush : TBrush; _Bordures : Boolean = true);
    procedure GridTitle(_Text : String; _Font : TFont; _Brush : TBrush; _Bordures : Boolean = true);
    procedure Grid(_Grille: TStringGrid; _QuadrillageVertical: Boolean = True; _Quadrillagehoritontal: Boolean = True; _Nue: Boolean = False; _RepeatFirstLineOneNewPage : Boolean = true);
    Procedure SauteLigne(_NbLignes : Integer = 1);
    procedure GridFirst(Ligne : array of TCellule;
                        RefY : TPosRefY = prLastBottomMarge;
                        OffsetY : double = 0);
    procedure GridNext(Ligne : array of string);
    procedure Dessin(Tag : pointer;
                     RefX : TPosRefX = prLeftMargin;
                     RefX2 : TPosRefX = prRightMargin;
                     RefY : TPosRefY = prLastBottom;
                     RefY2 : TPosRefY = prFree;
                     OffsetX : double = 0;
                     OffsetX2 : double = 0;
                     OffsetY : double = 0;
                     OffsetY2 : double = 0);
    procedure LigneHorizontale(_PosY : Double); overload;
    procedure LigneHorizontale(RefY : TPosRefY = prLastBottom;
                               OffsetY : double = 0); overload;
    procedure DrawJPeg(_JPeg : TJPEGImage; _Left, _Top, _Right, _Bottom : Double; _IsProportional : Boolean = True; _VertCenter : Boolean = True; _WithBorder : Boolean = False);
    procedure DrawBitmap(_Bitmap : Graphics.TBitmap; _Left, _Top, _Right, _Bottom : Double; _IsProportional : Boolean = True; _VertCenter : Boolean = True; _WithBorder : Boolean = False);
    procedure DrawPictureFromStream(_MemStream : TMemoryStream; _Left, _Top, _Right, _Bottom : Double; _WithBorder : Boolean = False);
    function GetTextHeight(_Text : String; _Left, _Right : Double; _Font : TFont) : Double;
    function GetTextWidth(_Text : String; _Top, _Bottom : Double; _Font : TFont) : Double;
    function GetTopPagePos : Double;
    function GetTopMargePagePos : Double;
    function GetBottomPagePos : Double;
    function GetBottomMargePagePos : Double;
    function GetHeaderMargePagePos : Double;    
    function GetFooterMargePagePos : Double;
    function GetLeftPagePos : Double;
    function GetLeftMargePagePos : Double;
    function GetRightPagePos : Double;
    function GetRightMargePagePos : Double;
    function GetCurrentYPos : Double;
    function GetCurrentXPos : Double;
    procedure SetCurrentYPos(_Value : Double);
    procedure SetCurrentXPos(_Value : Double);
    function PageCount : integer;
    procedure SetCurrentPage(Num : integer);
    procedure StorePos;
    procedure RestorePos;
    procedure SplitterStart;
    function SplitterEnd(RefY : TPosRefY; OffsetY : double) : boolean;
    procedure SplitterRewind;
    function cm2pixels(_Value : Double) : Integer;
  end;

    function TranslateTextAndFont(_Texte : String; _Font : TFont; _Align : TAlignment; _Rotation : Double = 0) : String;

implementation

uses
  SysUtils, BbsgTec;

const
  cMargeInter = 0.3; //marge entre chaque �l�ments dans une page


function TranslateTextAndFont(_Texte : String; _Font : TFont; _Align : TAlignment; _Rotation : Double = 0) : String;
begin

Result := '';


        if _Font <> Nil then
                begin
                Result := '''' + _Font.Name + ''' ';
                Result := Result + 'S ' + IntToStr(_Font.Size) + ' ';
                Result := Result + 'C ' + ColorToString(_Font.Color) + ' ';
                Result := Result + 'Rot ' + FloatToStr(_Rotation) + ' ';

                        if fsBold in _Font.Style then
                                Result := Result + 'B '
                        else
                                Result := Result + 'BO ';
                        if fsItalic in _Font.Style then
                                Result := Result + 'I '
                        else
                                Result := Result + 'IO ';
                        if fsUnderline in _Font.Style then
                                Result := Result + 'U '
                        else
                                Result := Result + 'UO ';
                        if fsStrikeOut in _Font.Style then
                                Result := Result + 'ST '
                        else
                                Result := Result + 'STO ';

                        if _Align = taLeftJustify then
                                Result := Result + 'L '
                        else
                        if _Align = taRightJustify then
                                Result := Result + 'R '
                        else
                        if _Align = taCenter then
                                Result := Result + 'CE '
                        else
                        if _Align = taFullJustify then
                                Result := Result + 'J ';



                        if Result <> '' then
                                Result := '[' + Trim(Result) + ']';

                        Result := Result + _Texte;
                end;


end;
{ TCelluleImp }
constructor TCelluleImp.Create(_Texte: String; _Font: TFont; _Largeur: SmallInt = 0; _PenColor: TColor = clBlack; _BrushColor: TColor = clWhite;
                               _Style: TFontStyles = []; _Align: TAlignment = taCenter);
begin
  IFont       := _Font;
  ITexte      := _Texte;
  IPenColor   := _PenColor;
  IBrushColor := _BrushColor;
  IFont.Style := _Style;
  ILargeur    := _Largeur;
  IHorAlign   := _Align;
  IImage      := Nil;
  IImageMergeable := false;
  IMerged     := False;
  IHorMerged  := False;
  ISeparateurBas := True;
  ISeparateurDroite := True;
  IAngle      := 0;
  IImgDrawn   := False;
  IImageAdapt := false;
  IImageAdaptNbLignes := 1;
  IFormatRTF := False;
end;

Destructor TCelluleImp.Destroy;
Begin
  IImage := nil;
  IFont := nil;
  Inherited Destroy;
End;

Procedure TCelluleImp.SetTexte(Const _Value : String);
Var
  i : Integer;
Begin
  FTexte := _Value;
  Val(_Value, FInt, i);
  if i <> 0 then FInt := 0;
  if not TextToFloat(PChar(_Value), FReal, fvExtended) Then FReal := 0;
End;

function TCelluleImp.GetOldAlignment : TAlignment;
begin
end;

procedure TCelluleImp.SetOldAlignment(_Value : TAlignment);
begin
  IHorAlign := _Value;
end;

function TCelluleImp.GetImgScale(_ColWidth: Extended): Extended;
begin
  Result := 1;
  if IImage <> Nil Then Result := _ColWidth / IImage.Width;
end;

function TCelluleImp.GetImgScaleComplete(_ColWidth, _RowHeight: Extended): Extended;
begin
  result := GetImgScale(_ColWidth);
  result := Min(result, _RowHeight / IImage.Height)

end;
//*************************************************************************************************************************************
function TCelluleImp.GetTextAndStyleVPE : String;
begin

Result := TranslateTextAndFont(ITexte, IFont, IHorAlign, IAngle);

end;
//*************************************************************************************************************************************
procedure TCelluleImp.SetBrushVPE(Var _VPE : TVPEngine);
begin
        if _VPE = Nil then
                exit;


        if IBrushColor = clNone then
                _VPE.BkgMode := VBKG_TRANSPARENT
        else
                begin
                _VPE.BkgMode := VBKG_SOLID;
                _VPE.BkgColor := ColorToRGB(IBrushColor);
                end;

end;
//*************************************************************************************************************************************
function TVPE.GetFontAndBrush(_Font : TFont; _Brush : TBrush) : String;
begin

Result := '';

        if CurrentVPE = Nil then
                exit;

        if _Font <> Nil then
                begin
                //non utilis�
                {
                CurrentVPE.FontName := _Font.Name;
                CurrentVPE.FontSize := _Font.Size;
                }
                Result := '''' + _Font.Name + ''' ' + 'S ' + IntToStr(_Font.Size) + ' ';
                        if fsBold in _Font.Style then
                                Result := Result + 'B '
                        else
                                Result := Result + 'BO ';
                        if fsItalic in _Font.Style then
                                Result := Result + 'I '
                        else
                                Result := Result + 'IO ';
                        if fsUnderline in _Font.Style then
                                Result := Result + 'U '
                        else
                                Result := Result + 'UO ';
                        if fsStrikeOut in _Font.Style then
                                Result := Result + 'ST '
                        else
                                Result := Result + 'STO ';
                        if Result <> '' then
                                Result := '[' + Trim(Result) + ']';
                end;

        if _Brush <> Nil then
                begin
                CurrentVPE.Brush.Assign(_Brush);
                CurrentVPE.BkgMode := VBKG_SOLID;
                CurrentVPE.BkgColor := ColorToRGB(_Brush.Color);

                end;
end;
//*************************************************************************************************************************************
procedure TVPE.VPEUDOPaint(Sender : TObject);
begin

  if Assigned(VPE_DessinCallback) then VPE_DessinCallback(TVPEngine(Sender).UDOlParam,
                                                          TVPEngine(Sender).UDODC,
                                                          TVPEngine(Sender).nUDOLeft,
                                                          TVPEngine(Sender).nUDORight,
                                                          TVPEngine(Sender).nUDOTop,
                                                          TVPEngine(Sender).nUDOBottom);
                                                          
end;
//*************************************************************************************************************************************
function TVPE.PosRefX(PosRef : TPosRefX; Offset : double) : double;
begin
  case PosRef of
    prLeftMargin  : Result := Offset + CurrentVPE.nLeftMargin;
    prRightMargin : Result := Offset + CurrentVPE.nRightMargin;
    prCenter      : Result := Offset + (CurrentVPE.nRightMargin + CurrentVPE.nLeftMargin)/2;
    prLastLeft    : Result := Offset + CurrentVPE.nLeft;
    prLastRight   : Result := Offset + CurrentVPE.nRight;
    else Result := -1;
  end;
end;
//*************************************************************************************************************************************
function TVPE.PosRefY(PosRef : TPosRefY; Offset : double) : double;
begin
  case PosRef of
    prLastBottom      : Result := Offset + CurrentVPE.nBottom;
    prLastBottomMarge : Result := Offset + CurrentVPE.nBottom + cMargeInter;
    prTopMargin       : Result := Offset + CurrentVPE.nTopMargin;
    prBottomMargin    : Result := Offset + CurrentVPE.nBottomMargin;
    prLastTop         : Result := Offset + CurrentVPE.nTop;
    prFree            : Result := VFREE
    else Result := -1;
  end;
end;
//*************************************************************************************************************************************
procedure TVPE.GetProportionalImageDimensions(_ImageHeight, _ImageWidth : Integer; Var _RealLeft, _RealTop, _RealRight, _RealBottom : Double; _VertCenter : Boolean = True);
Var
        ImageRatio : Double;
        RealRatio : Double;
        ProportionalRatio : Double;
        ScaleW : Double;
        ScaleH : Double;        
        tempRealHeight : Double;
        tempRealWidth : Double;
        MargeH : Double;
        MargeV : Double;
        UseImageHeight : Double;
        UseImageWidth : Double;
Begin


tempRealHeight  := abs(_RealBottom      - _RealTop);
tempRealWidth   := abs(_RealRight       - _RealLeft);

ProportionalRatio := tempRealWidth / _ImageWidth;

UseImageWidth := ProportionalRatio * _ImageWidth;
UseImageHeight := ProportionalRatio * _ImageHeight;

ScaleW := tempRealWidth         / UseImageWidth;
ScaleH := tempRealHeight        / UseImageHeight;

        if (UseImageHeight * ScaleW > tempRealHeight) then
                begin
                tempRealWidth   := UseImageWidth * ScaleH;
                tempRealHeight   := UseImageHeight * ScaleH;
                end        
        else
                begin
                tempRealWidth   := UseImageWidth * ScaleW;
                tempRealHeight   := UseImageHeight * ScaleW;
                end;


  MargeH := abs((_RealRight - _RealLeft) - tempRealWidth) / 2;
  MargeV := abs((_RealBottom - _RealTop) - tempRealHeight) / 2;

  _RealLeft     := _RealLeft    + MargeH;
  _RealRight    := _RealRight    - MargeH;
        if _VertCenter then
                begin
                _RealTop      := _RealTop     + MargeV;
                _RealBottom   := _RealBottom  - MargeV;
                end
        else
                _RealBottom   := _RealTop     + tempRealHeight;
End;
//*************************************************************************************************************************************
procedure TVPE.HeaderFooterText(_Texte : String; _Position : TPostionHF; _TextHeight : Double);
Var
        _YPos : Double;
        _Retour : String;
        SavePosY : Double;
begin
        if trim(_Texte) = '' then
                exit;

        if _Position = tphfAbsent then
                exit;

  case _Position of
        tphfTopLeft, tphfTopCenter, tphfTopRight                : _YPos := CurrentVPE.nTopMargin;
        tphfBottomLeft, tphfBottomCenter, tphfBottomRight       : Begin
                                                                  _YPos := CurrentVPE.nBottomMargin - _TextHeight;
                                                                  SavePosY := CurrentVPE.nBottom;
                                                                  End;
        End;
  CurrentVPE.Write(CurrentVPE.nLeftMargin, _YPos, CurrentVPE.nRightMargin, _YPos +_TextHeight, _Texte);

  case _Position of
        tphfTopLeft, tphfTopCenter, tphfTopRight                : ;
        tphfBottomLeft, tphfBottomCenter, tphfBottomRight       : CurrentVPE.nBottom := SavePosY;
        End;


end;
//*************************************************************************************************************************************
procedure TVPE.HeaderFooterNewPage;
var
        RetourTextLeft : String;
        RetourTextCenter : String;
        RetourTextRight : String;
        RenderTextLeft : Double;
        RenderTextCenter : Double;
        RenderTextRight : Double;
        RenderTexts : Double;
        SavePosY : Double;
begin

SetLength(HeaderRenderHeights, Length(HeaderRenderHeights) + 1);
SetLength(FooterRenderHeights, Length(FooterRenderHeights) + 1);

PrintLogo;

        if Assigned(VPE_HeaderCallback) then
                VPE_HeaderCallback
        else
                Begin
                RetourTextLeft := TranslateTextAndFont(HeaderLeft, FontHeaderLeft, taLeftJustify);
                CurrentVPE.RenderWrite(CurrentVPE.nLeftMargin, 0, CurrentVPE.nRightMargin, VFREE, RetourTextLeft);
                RenderTextLeft := CurrentVPE.nRenderHeight;
                RetourTextCenter := TranslateTextAndFont(HeaderCenter, FontHeaderCenter, taCenter);
                CurrentVPE.RenderWrite(CurrentVPE.nLeftMargin, 0, CurrentVPE.nRightMargin, VFREE, RetourTextCenter);
                RenderTextCenter := CurrentVPE.nRenderHeight;
                RetourTextRight := TranslateTextAndFont(HeaderRight, FontHeaderRight, taRightJustify);
                CurrentVPE.RenderWrite(CurrentVPE.nLeftMargin, 0, CurrentVPE.nRightMargin, VFREE, RetourTextRight);
                RenderTextRight := CurrentVPE.nRenderHeight;
                RenderTexts := Max(RenderTextLeft, Max(RenderTextCenter, RenderTextLeft));
                        if LogoPostion in [tphfTopLeft, tphfTopCenter, tphfTopRight] then
                                RenderTexts := Max(RenderTexts, LogoHeight);
                HeaderRenderHeights[Length(HeaderRenderHeights) - 1] := RenderTexts;
                HeaderFooterText(RetourTextLeft, tphfTopLeft, RenderTexts);
                HeaderFooterText(RetourTextCenter, tphfTopCenter, RenderTexts);
                HeaderFooterText(RetourTextRight, tphfTopRight, RenderTexts);
                FHeaderMargePos := CurrentVPE.nTopMargin + RenderTexts + 0.1;
                LigneHorizontale(FHeaderMargePos);
                End;

        if Assigned(VPE_FooterCallback) then
                VPE_FooterCallback
        else
                Begin
                RetourTextLeft := TranslateTextAndFont(FooterLeft, FontFooterLeft, taLeftJustify);
                CurrentVPE.RenderWrite(CurrentVPE.nLeftMargin, 0, CurrentVPE.nRightMargin, VFREE, RetourTextLeft);
                RenderTextLeft := CurrentVPE.nRenderHeight;
                RetourTextCenter := TranslateTextAndFont(FooterCenter, FontFooterLeft, taCenter);
                CurrentVPE.RenderWrite(CurrentVPE.nLeftMargin, 0, CurrentVPE.nRightMargin, VFREE, RetourTextCenter);
                RenderTextCenter := CurrentVPE.nRenderHeight;
                RetourTextRight := TranslateTextAndFont(FooterRight, FontFooterLeft, taRightJustify);
                CurrentVPE.RenderWrite(CurrentVPE.nLeftMargin, 0, CurrentVPE.nRightMargin, VFREE, RetourTextRight);
                RenderTextRight := CurrentVPE.nRenderHeight;
                RenderTexts := Max(RenderTextLeft, Max(RenderTextCenter, RenderTextLeft));
                        if LogoPostion in [tphfBottomLeft, tphfBottomCenter, tphfBottomRight] then
                                RenderTexts := Max(RenderTexts, LogoHeight);
                FooterRenderHeights[Length(FooterRenderHeights) - 1] := RenderTexts;
                HeaderFooterText(RetourTextLeft, tphfBottomLeft, RenderTexts);
                HeaderFooterText(RetourTextCenter, tphfBottomCenter, RenderTexts);
                HeaderFooterText(RetourTextRight, tphfBottomRight, RenderTexts);
                SavePosY := CurrentVPE.nBottom;
                FFooterMargePos := CurrentVPE.nBottomMargin - RenderTexts - 0.1;
                LigneHorizontale(FFooterMargePos);
                CurrentVPE.nBottom := SavePosY;
                End;
end;
//*************************************************************************************************************************************
procedure TVPE.PrintLogo;
Var
  RectImp: TRectVPE;
  Ratio : Extended;
  SavePosY : Double;
  SavePosX : Double;
Const
  Centrage = true;
  Encadre = false;
Begin
  if (Logo = nil) Or (Logo.Height = 0) or (LogoHeight = 0) or (LogoPostion = tphfAbsent)Then exit;
  { Facteur de l'image }
//  Ratio := 1;

//  if (LogoHeight < Logo.Height) then
        Ratio := LogoHeight / Logo.Height;


  SavePosY := GetCurrentYPos;
  SavePosX := GetCurrentXPos;

  case LogoPostion of
        tphfTopLeft :
                Begin
                RectImp.Left   := CurrentVPE.nLeftMargin;
                RectImp.Top    := CurrentVPE.nTopMargin;
                End;
        tphfTopCenter :
                Begin
                RectImp.Left   := CurrentVPE.nLeftMargin + (CurrentVPE.nRightMargin - CurrentVPE.nLeftMargin - (ratio * Logo.Width) )/ 2;
                RectImp.Top    := CurrentVPE.nTopMargin;
                End;
        tphfTopRight :
                Begin
                RectImp.Left   := CurrentVPE.nRightMargin - (ratio * Logo.Width);
                RectImp.Top    := CurrentVPE.nTopMargin;
                End;
        tphfBottomLeft :
                Begin
                RectImp.Left   := CurrentVPE.nLeftMargin;
                RectImp.Top    := CurrentVPE.nBottomMargin - (ratio * Logo.Height);
                End;
        tphfBottomCenter :
                Begin
                RectImp.Left   := CurrentVPE.nLeftMargin + (CurrentVPE.nRightMargin - CurrentVPE.nLeftMargin - (ratio * Logo.Width) )/ 2;
                RectImp.Top    := CurrentVPE.nBottomMargin - (ratio * Logo.Height);
                End;
        tphfBottomRight :
                Begin
                RectImp.Left   := CurrentVPE.nRightMargin - (ratio * Logo.Width);
                RectImp.Top    := CurrentVPE.nBottomMargin - (ratio * Logo.Height);                
                End;
  End;

  RectImp.Right  := RectImp.Left + (ratio * Logo.Width);
  RectImp.Bottom := RectImp.Top + (ratio * Logo.Height);

  DrawBitmap(Logo, RectImp.Left, RectImp.Top, RectImp.Right, RectImp.Bottom);
  SetCurrentYPos(SavePosY);
  SetCurrentXPos(SavePosX);
End;
//*************************************************************************************************************************************
procedure TVPE.OpenDoc(_VPE : TVPEngine; Orientation : TPageOrientation; _Marge : double; _NewGridPageCallback : TVPE_NewGridPageCallback = Nil; _HeaderCallback : TVPE_HeaderCallback = Nil; _FooterCallback : TVPE_FooterCallback = Nil; _CallbackDessin : TVPE_DessinCallback = Nil);
begin
  CurrentVPE := _VPE;
//  CurrentVPE.Visible := false;
//  CurrentVPE.Parent := _Parent;
  VPE_DessinCallback := _CallbackDessin;
  VPE_NewGridPageCallback := _NewGridPageCallback;
  VPE_HeaderCallback := _HeaderCallback;
  VPE_FooterCallback := _FooterCallback;
  
  CurrentVPE.OnUDOPaint := VPEUDOPaint;
  CurrentVPE.ExternalWindow := False;

  CurrentVPE.OpenDoc;
  CurrentVPE.License('VPE-E2610-5510', 'VTD4-LVTD');

  CurrentVPE.Compression   := DOC_COMPRESS_FLATE;
  CurrentVPE.AutoBreakMode := AUTO_BREAK_NO_LIMITS;
  CurrentVPE.DocExportType := VPE_DOC_TYPE_PDF;
  CurrentVPE.Encryption    := DOC_ENCRYPT_STREAM;
  CurrentVPE.Protection    := PDF_ALLOW_PRINT + PDF_ALLOW_HIQ_PRINT + PDF_ALLOW_COPY;

  CurrentVPE.PictureKeepAspect := true;
  CurrentVPE.PictureBestFit    := false;
  CurrentVPE.PictureEmbedInDoc := true; //manifestement ne marche pas

  CurrentVPE.PageOrientation := VPE_VCL.TPageOrientation(Orientation);
  CurrentVPE.nLeftMargin := _Marge;
  CurrentVPE.nRightMargin := CurrentVPE.PageWidth - _Marge;
  CurrentVPE.nTopMargin := _Marge;
  CurrentVPE.nBottomMargin := CurrentVPE.PageHeight - _Marge;

  FHeaderMargePos := CurrentVPE.nTopMargin;
  FFooterMargePos := CurrentVPE.nBottomMargin;

  SetLength(HeaderRenderHeights, 0);
  SetLength(FooterRenderHeights, 0);

  Vignettes := TList.Create;
  
  HeaderFooterNewPage;

end;
//*************************************************************************************************************************************
procedure TVPE.SetLogo(_Logo : Graphics.TBitmap; _Height : Double; _Postion : TPostionHF);
Begin
Logo := _Logo;
LogoHeight := _Height;
LogoPostion := _Postion;
End;
//*************************************************************************************************************************************
procedure TVPE.SetHeaderFooter(_Texte : String; _Font : TFont; _Position : TPostionHF);
Begin
        if Trim(_Texte) = '' then
                exit;
        if _Font = Nil then
                exit;
        if _Position = tphfAbsent then
                exit;
        case _Position of
                tphfTopLeft :           begin
                                        HeaderLeft := _Texte;
                                        FontHeaderLeft := _Font;
                                        end;
                tphfTopCenter :         begin
                                        HeaderCenter := _Texte;
                                        FontHeaderCenter := _Font;
                                        end;
                tphfTopRight :          begin
                                        HeaderRight := _Texte;
                                        FontHeaderRight := _Font;
                                        end;
                tphfBottomLeft :        begin
                                        FooterLeft := _Texte;
                                        FontFooterLeft := _Font;
                                        end;
                tphfBottomCenter :      begin
                                        FooterCenter := _Texte;
                                        FontFooterCenter := _Font;
                                        end;
                tphfBottomRight :       begin
                                        FooterRight := _Texte;
                                        FontFooterRight := _Font;
                                        end;
        End;
End;
//*************************************************************************************************************************************
procedure TVPE.PrintNbPages(_Position : TPostionHF; _Font : TFont);
Var
        cpt : Integer;
        TextHeight : Double;
        TextAlignement : TAlignment;
Begin
        if _Position = TphfAbsent then
                exit;

        for cpt := 1 to CurrentVPE.PageCount do
                begin
                CurrentVPE.CurrentPage := cpt;
                        if _Position in [TphfTopLeft, TphfTopCenter, TphfTopRight] then
                                TextHeight := HeaderRenderHeights[cpt - 1]
                        else
                                TextHeight := FooterRenderHeights[cpt - 1];
                HeaderFooterText(TranslateTextAndFont(inttostr(cpt) + ' / ' + inttostr(CurrentVPE.PageCount), _Font, TextAlignement), _Position, TextHeight);
                end;
CurrentVPE.RefreshDoc;
End;
//*************************************************************************************************************************************
procedure TVPE.GenereVignettes;
        procedure Encadrer(BmpSource : Graphics.TBitmap);
        Var
                BmpDest : TBitmap;
        Const
                CadreWidth = 2;
        begin
                BmpDest := TBitmap.Create;
                try
                BmpDest.Width  := BmpSource.Width  + CadreWidth + CadreWidth;
                BmpDest.Height := BmpSource.Height + CadreWidth + CadreWidth;
                BmpDest.Canvas.Brush.Color := clBlack;
                BmpDest.Canvas.FillRect(BmpDest.Canvas.ClipRect);
                BmpDest.Canvas.Draw(CadreWidth, CadreWidth, BmpSource);
                BmpSource.Assign(BmpDest);
                finally
                BmpDest.Free;
                end;
        end;
Var
        cpt : Integer;
        tempPath : String;
Const
        //DPI = 32;
        DPI = 24;
Begin
{
CurrentVPE.PictureExportColorDepth := PICEXP_COLOR_256;
CurrentVPE.PictureExportDither := PICEXP_DITHER_256;
}
CurrentVPE.SetPictureDefaultDPI(DPI, DPI);

Vignettes.Clear;

        for cpt := 1 to CurrentVPE.PageCount do
                begin
                //on ne passe pas par le PictureExportPageStream car il retourne un stream vide (result false) sans raison apparente...
                tempPath := TempRep + 'vpe' + inttostr(cpt) + '.bmp';
                CurrentVPE.PictureExportPage(tempPath, cpt);
                Vignettes.Add(Graphics.TBitmap.Create);
                Graphics.TBitmap(Vignettes.Last).LoadFromFile(tempPath);
                Encadrer(Graphics.TBitmap(Vignettes.Last));
                if cpt = 1 then Graphics.TBitmap(Vignettes.Last).SaveToFile('d:\toto.bmp');
                DeleteFile(tempPath);
                end;
End;
//*************************************************************************************************************************************
procedure TVPE.WriteDoc(Fichier : string);
begin
  CurrentVPE.WriteDoc(Fichier);
end;
//*************************************************************************************************************************************
procedure TVPE.CloseDoc;
begin
{
  if (LogoVS <> nil) then begin
    LogoVS.Close;
    LogoVS.Destroy;
  end;
}
  CurrentVPE.CloseDoc;
  SetLength(HeaderRenderHeights, 0);
  SetLength(FooterRenderHeights, 0);
//  Vignettes.Destroy;  
  CurrentVPE.Destroy;
end;
//*************************************************************************************************************************************
procedure TVPE.Preview;
begin
CurrentVPE.Preview;
end;
//*************************************************************************************************************************************
procedure TVPE.NewPage(Orientation : TPageOrientation; _Marge : double);
begin
        //r�gis
//  if (CurrentVPE.LastInsertedObject <> nil) then CurrentVPE.PageBreak;
  CurrentVPE.PageBreak;
  CurrentVPE.FontName := 'arial';
  CurrentVPE.FontSize := 8;
  CurrentVPE.PageOrientation := VPE_VCL.TPageOrientation(Orientation);
  CurrentVPE.nLeftMargin := _Marge;
  CurrentVPE.nRightMargin := CurrentVPE.PageWidth - _Marge;
  CurrentVPE.nTopMargin := _Marge;
  CurrentVPE.nBottomMargin := CurrentVPE.PageHeight - _Marge;

  FHeaderMargePos := CurrentVPE.nTopMargin;
  FFooterMargePos := CurrentVPE.nBottomMargin;

  HeaderFooterNewPage;
end;
//*************************************************************************************************************************************
{
procedure TVPE.SetOrientation(Orientation : TPageOrientation);
begin
  CurrentVPE.PageOrientation := VPE_VCL.TPageOrientation(Orientation);
end;
}
//*************************************************************************************************************************************
procedure TVPE.WriteRTF(const RTF : string;
                        Bordure : boolean = false;
                        HAlign : THAlign = hLEFT;
                        RefX : TPosRefX = prLeftMargin;
                        RefX2 : TPosRefX = prRightMargin;
                        RefY : TPosRefY = prLastBottomMarge;
                        RefY2 : TPosRefY = prFree;
                        OffsetX : double = 0;
                        OffsetX2 : double = 0;
                        OffsetY : double = 0;
                        OffsetY2 : double = 0);
begin
  CurrentVPE.TextAlignment := Ord(HAlign);
  OffsetX  := PosRefX(RefX, OffsetX);
  OffsetX2 := PosRefX(RefX2, OffsetX2);
  OffsetY  := PosRefY(RefY, OffsetY);
  OffsetY2 := PosRefY(RefY2, OffsetY2);
  if Bordure then begin
    CurrentVPE.PenSize := 0.01;
    CurrentVPE.WriteBoxRTF(OffsetX, OffsetY, OffsetX2, OffsetY2, '{' + RTF + '}');
  end else CurrentVPE.WriteRTF(OffsetX, OffsetY, OffsetX2, OffsetY2, '{' + RTF + '}');
end;
//*************************************************************************************************************************************
function TVPE.Cellule(Texte : string = '';
                      RefG : TPosRefX = prLastRight;
                      OffsetG : double = 0;
                      RefD : TPosRefX = prLastRight;
                      OffsetD : double = NaN) : TCellule;
begin
  Result.Texte := Texte;
  Result.RefG := RefG;
  Result.OffsetG := OffsetG;
  Result.RefD := RefD;
  Result.OffsetD := OffsetD;
end;
//*************************************************************************************************************************************
procedure TVPE.Title(_Text : String; _Font : TFont; _Brush : TBrush; _Bordures : Boolean = true);
var
        OffsetX : double;
        OffsetX2 : double;
        OffsetY : double;
        OffsetY2 : double;
        FontStyle : String;
begin

OffsetX := 0;
OffsetX2 := 0;
OffsetY := 0;
OffsetY2 := 0;

CurrentVPE.PenSize := 0.01;

CurrentVPE.TextAlignment := Ord(hCENTER);
OffsetX  := PosRefX(prLeftMargin, OffsetX);
OffsetX2 := PosRefX(prRightMargin, OffsetX2);
OffsetY  := PosRefY(prLastBottomMarge, OffsetY);
OffsetY2 := PosRefY(prFree, OffsetY2);


PosRefY(prLastBottom, 0);

FontStyle := GetFontAndBrush(_Font, _Brush);

        if _Bordures then
                CurrentVPE.WriteBox(OffsetX, OffsetY, OffsetX2, OffsetY2, FontStyle + _Text)
        else
                CurrentVPE.Write(OffsetX, OffsetY, OffsetX2, OffsetY2, FontStyle + _Text);

SauteLigne;                
end;
//*************************************************************************************************************************************
procedure TVPE.GridTitle(_Text : String; _Font : TFont; _Brush : TBrush; _Bordures : Boolean = true);
var
        OffsetX : double;
        OffsetX2 : double;
        OffsetY : double;
        OffsetY2 : double;
        FontStyle : String;
begin

OffsetX := 0;
OffsetX2 := 0;
OffsetY := 0;
OffsetY2 := 0;

CurrentVPE.TextAlignment := Ord(hCENTER);
OffsetX  := PosRefX(prLeftMargin, OffsetX);
OffsetX2 := PosRefX(prRightMargin, OffsetX2);
OffsetY  := PosRefY(prLastBottomMarge, OffsetY);
OffsetY2 := PosRefY(prFree, OffsetY2);


PosRefY(prLastBottom, 0);

        if Not _Bordures then
                _Brush.Color := clWhite;

FontStyle := GetFontAndBrush(_Font, _Brush);
        if _Bordures then
                CurrentVPE.WriteBox(OffsetX, OffsetY, OffsetX2, OffsetY2, FontStyle + _Text)
        else
                CurrentVPE.Write(OffsetX, OffsetY, OffsetX2, OffsetY2, FontStyle + _Text);

end;
//*************************************************************************************************************************************
Procedure TVPE.SauteLigne(_NbLignes : Integer = 1);
Begin
CurrentVPE.nBottom := CurrentVPE.nBottom + (_NbLignes * cMargeInter);
End;
//*************************************************************************************************************************************
procedure TVPE.Grid(_Grille: TStringGrid; _QuadrillageVertical: Boolean = True; _Quadrillagehoritontal: Boolean = True; _Nue: Boolean = False; _RepeatFirstLineOneNewPage : Boolean = true);
var
        StoreCoordHeight : array of record
                               RowHeight : double;
                               RowHeightMerged : double;
                               IndexAfterMerged : Integer;
                          end;
                          
        //permet de savoir si on est sur une cellule merg�e par une autre
        function IsCurrentlyMergedByOther(_IndexCol : Integer; _IndexRow : Integer) : Boolean;
        begin
        Result := ((_IndexCol > 0) and (TCelluleImp(_Grille.Objects[_IndexCol - 1, _IndexRow]).IMerged));
        Result := Result or ((_IndexRow > 0) and (TCelluleImp(_Grille.Objects[_IndexCol, _IndexRow - 1]).IHorMerged));
        end;

        //permet de connaitre la cellule sur laquelle on va travailler. Dans le cas de plusieurs cellules merg�es, on prend la premi�re cellule avec du texte,
        //d'abord en parcourant sur la ligne, puis sur la colonne
        function GetActiveCellOnMerged(_IndexCol : Integer; _IndexRow : Integer; _IndexMergedOnLine : Integer; _IndexMergedOnColumn : Integer) : TCelluleImp;
        var
                cptCol : Integer;
                cptRow : Integer;
        Begin
        Result := TCelluleImp(_Grille.Objects[_IndexCol, _IndexRow]);
                for CptRow := _IndexRow to _IndexMergedOnColumn do
                        for CptCol := _IndexCol to _IndexMergedOnLine do
                                if (Trim(TCelluleImp(_Grille.Objects[cptCol, CptRow]).ITexte) <> '') or (TCelluleImp(_Grille.Objects[cptCol, CptRow]).IImage <> Nil) then
                                        begin
                                        Result := TCelluleImp(_Grille.Objects[cptCol, CptRow]);
                                        exit;
                                        end;
        End;

        //Permet de connaitre jusqu'au la premi�re cellule est merg�e sur la ligne
        function GetIndexMergedOnLine(_IndexCol : Integer; _IndexRow : Integer) : Integer;
        var
                cptCol : Integer;
        begin
        Result := _IndexCol;
                for cptCol := _IndexCol to _Grille.ColCount - 1 do
                        begin
                                if TCelluleImp(_Grille.Objects[cptCol, _IndexRow]).IMerged then
                                        inc(Result)
                                else
                                        break;
                        end;
        Result := Min(Result, _Grille.ColCount - 1);
        end;

        //Permet de connaitre jusqu'au la premi�re cellule est merg�e sur la colonne
        function GetIndexMergedOnColumn(_IndexCol : Integer; _IndexRow : Integer) : Integer;
        var
                cptRow : Integer;
        begin
        Result := _IndexRow;
                for cptRow := _IndexRow to _Grille.RowCount - 1 do
                        begin
                                if TCelluleImp(_Grille.Objects[_IndexCol, cptRow]).IHorMerged then
                                        inc(Result)
                                else
                                        break;
                        end;
        Result := Min(Result, _Grille.RowCount - 1);
        end;

        procedure InsertRows(_Index : Integer; _NbRow : Integer);
        var
                cptRow : Integer;
                cptCol : Integer;
        begin
        _Grille.RowCount := _Grille.RowCount + _NbRow;
        SetLength(StoreCoordHeight, Length(StoreCoordHeight) + _NbRow);
                for cptRow := _Grille.RowCount - 1 downto _Index do
                        begin
                                for cptCol := 0 to _Grille.ColCount - 1 do
                                        _Grille.Objects[cptCol, cptRow] := _Grille.Objects[cptCol, Max(0, cptRow - _NbRow)];
                        StoreCoordHeight[cptRow] := StoreCoordHeight[cptRow - _NbRow];
                        end;

                for cptRow := _Index to _Index + _NbRow - 1 do
                        for cptCol := 0 to _Grille.ColCount - 1 do
                                _Grille.Objects[cptCol, cptRow] := Nil;                
        end;

        procedure CopyRow(_IndexSource : Integer; _IndexDest : Integer);
        var
                cptCol : Integer;
        begin
                for cptCol := 0 to _Grille.ColCount - 1 do
                        _Grille.Objects[cptCol, _IndexDest] := _Grille.Objects[cptCol, _IndexSource];

        StoreCoordHeight[_IndexDest] := StoreCoordHeight[_IndexSource];
        end;

        function GetRowsHeight(_StartIndex : Integer; _StopIndex : Integer): double;
        var
                cptCol : Integer;
        begin
        Result := 0;
                for cptCol := _StartIndex to _StopIndex do
                        Result := Result + StoreCoordHeight[cptCol].RowHeight;
        end;

Var
        tempFont : TFont;

        ARow : Integer;
        ACol : Integer;
        OffsetX : double;
        OffsetX2 : double;
        OffsetY : double;
        OffsetY2 : double;
        OffsetY3 : double;        
//        OffsetLocalX : double;
        StoreCoordWidth : array of record
                               Left,
                               Right : double;
                          end;

//        StoreCurrentRowTextHeight : array of double;
        LargeurGrid : double;
        LargeurUtilisee : double;
        LargeurAutoCellule : double;
        ForceLargeurColAuto : Boolean;
        NbColZero : Integer;
        tempRowHeight : double;
        tempRowHeightMerged : double;
        tempIndexAfterMerged : Integer;        
        IndexMergedOnLine : Integer;
        IndexMergedOnColumn : Integer;
        ActiveCellOnMerged : TCelluleImp;
        TempPosY : double;
        TempPosBottomY : double;        
        NbLineEntete : Integer;
        cpt : Integer;
        IsNewPage : Boolean;
        IsNextRowNewPage : Boolean;
//        PictureStream : TVPEStream;     

Const
        MargeHTexte = 0.05;
begin

tempFont := TFont.Create;
        for ACol := 0 to _Grille.ColCount - 1 do
                for ARow := 0 to _Grille.RowCount - 1 do
                        if _Grille.Objects[ACol, ARow] = Nil then
                                _Grille.Objects[ACol, ARow] := TCelluleImp.Create('', tempFont);

OffsetX  := CurrentVPE.nLeftMargin;
OffsetX2 := CurrentVPE.nRightMargin;
LargeurGrid := OffsetX2 - OffsetX;


SetLength(StoreCoordWidth, _Grille.ColCount);
StoreCoordWidth[0].Left := OffsetX;
StoreCoordWidth[0].Right := StoreCoordWidth[0].Left;
StoreCoordWidth[_Grille.ColCount - 1].Right := OffsetX2;

SetLength(StoreCoordHeight, _Grille.RowCount);

NbColZero := 0;
LargeurUtilisee := 0;
        for ACol := 0 to _Grille.ColCount - 1 do
                begin
                        if TCelluleImp(_Grille.Objects[ACol, 0]).ILargeur > 0 then
                                LargeurUtilisee := LargeurUtilisee + TCelluleImp(_Grille.Objects[ACol, 0]).ILargeur / 100 * LargeurGrid
                        else
                                inc(NbColZero);
                end;

ForceLargeurColAuto := False;
        if LargeurUtilisee > LargeurGrid then
                begin
                ForceLargeurColAuto := True;
                LargeurAutoCellule := LargeurGrid / _Grille.ColCount;
                end
        else
        if NbColZero > 0 then
                LargeurAutoCellule := (LargeurGrid - LargeurUtilisee) / NbColZero;


        if _Grille.ColCount > 1 then
                for ACol := 0 to _Grille.ColCount - 1 do
                        begin
                        StoreCoordWidth[ACol].Left := StoreCoordWidth[Max(0, ACol - 1)].Right;
                                if ACol < _Grille.ColCount - 1 then
                                        begin
                                                if (TCelluleImp(_Grille.Objects[ACol, 0]).ILargeur > 0) and not(ForceLargeurColAuto) then
                                                        StoreCoordWidth[ACol].Right := StoreCoordWidth[ACol].Left + TCelluleImp(_Grille.Objects[ACol, 0]).ILargeur / 100 * LargeurGrid
                                                else
                                                        StoreCoordWidth[ACol].Right := StoreCoordWidth[ACol].Left + LargeurAutoCellule;
                                        end;
                        end;

CurrentVPE.PenSize := 0.01;
NbLineEntete := 1;

        for ARow := 0 to _Grille.RowCount - 1 do
                begin

                tempRowHeight := -1;
                tempRowHeightMerged := -1;
                tempIndexAfterMerged := - 1;
                        for ACol := 0 to _Grille.ColCount - 1 do
                                begin
                                        if IsCurrentlyMergedByOther(ACol, ARow) = false then
                                                begin
                                                IndexMergedOnLine := GetIndexMergedOnLine(ACol, ARow);
                                                IndexMergedOnColumn := GetIndexMergedOnColumn(ACol, ARow);
                                                        if ARow = 0 then
                                                                NbLineEntete := Max(NbLineEntete, IndexMergedOnColumn - ARow + 1);
                                                ActiveCellOnMerged := GetActiveCellOnMerged(ACol, ARow, IndexMergedOnLine, IndexMergedOnColumn);
{
                                                        if ActiveCellOnMerged.IImage <> Nil then
                                                                begin
                                                                PictureStream := CurrentVPE.CreateMemoryStream(0);
                                                                ActiveCellOnMerged.IImage.SaveToStream(TMemoryStream(PictureStream));
                                                                CurrentVPE.RenderPictureStream(PictureStream, ActiveCellOnMerged.IImage.width, ActiveCellOnMerged.IImage.Height, inttostr(ACol) + inttostr(ARow));
                                                                end
                                                        else
}
                                                                CurrentVPE.RenderWrite(StoreCoordWidth[ACol].Left + MargeHTexte , 0, StoreCoordWidth[IndexMergedOnLine].Right - MargeHTexte, VFREE, ActiveCellOnMerged.GetTextAndStyleVPE);

                                                ActiveCellOnMerged.CurrentTextHeight := CurrentVPE.nRenderHeight;
                                                        if TCelluleImp(_Grille.Objects[ACol, ARow]).IHorMerged = false then
                                                                tempRowHeight := Max(tempRowHeight, ActiveCellOnMerged.CurrentTextHeight);
                                                tempRowHeightMerged := Max(tempRowHeightMerged, ActiveCellOnMerged.CurrentTextHeight);
                                                tempIndexAfterMerged := Max(tempIndexAfterMerged, IndexMergedOnColumn + 1);
                                                end;
                                end;

                StoreCoordHeight[ARow].RowHeight := tempRowHeight;
                StoreCoordHeight[ARow].RowHeightMerged := tempRowHeightMerged;
                StoreCoordHeight[ARow].IndexAfterMerged := Min(_Grille.RowCount - 1, tempIndexAfterMerged);
                end;


ARow := 0;
// _Nue := true;
// _QuadrillageVertical := false;
// _Quadrillagehoritontal := false;
                Repeat

                        if CurrentVPE.nBottom + StoreCoordHeight[ARow].RowHeightMerged > FFooterMargePos then
                                begin
                                NewPage(TPageOrientation(CurrentVPE.PageOrientation), CurrentVPE.nLeftMargin);
                                if Assigned(VPE_NewGridPageCallback) then
                                        VPE_NewGridPageCallback;                                
                                //todo AutoNewPage;
                                IsNewPage := True;
                                        if _RepeatFirstLineOneNewPage then
                                                begin
                                                        if ARow > 0 then
                                                                begin
                                                                InsertRows(ARow, NbLineEntete);
                                                                        for cpt := 0 to NbLineEntete - 1 do
                                                                                begin
                                                                                CopyRow(cpt, ARow + cpt);
                                                                                end;
                                                                end;
                                                end;                                
                                end
                        else
                                begin
                                IsNewPage := False;
                                        if StoreCoordHeight[ARow].IndexAfterMerged <> ARow then
                                                if CurrentVPE.nBottom + StoreCoordHeight[ARow].RowHeightMerged +  StoreCoordHeight[StoreCoordHeight[ARow].IndexAfterMerged].RowHeightMerged > FFooterMargePos then
                                                        IsNextRowNewPage := True
                                                else
                                                        IsNextRowNewPage := False;
                                end;

                        for ACol := 0 to _Grille.ColCount - 1 do
                                begin
                                        if IsCurrentlyMergedByOther(ACol, ARow) = false then
                                                begin
                                                IndexMergedOnLine := GetIndexMergedOnLine(ACol, ARow);
                                                IndexMergedOnColumn := GetIndexMergedOnColumn(ACol, ARow);
                                                TempPosY := CurrentVPE.nBottom;
                                                TempPosBottomY := TempPosY + GetRowsHeight(ARow, IndexMergedOnColumn);
                                                ActiveCellOnMerged := GetActiveCellOnMerged(ACol, ARow, IndexMergedOnLine, IndexMergedOnColumn);
                                                ActiveCellOnMerged.SetBrushVPE(CurrentVPE);
                                                CurrentVPE.Write(StoreCoordWidth[ACol].Left, TempPosY, StoreCoordWidth[IndexMergedOnLine].Right, TempPosBottomY, '');
                                                //texte
                                                CurrentVPE.BkgMode := VBKG_TRANSPARENT;
                                                CurrentVPE.Write(StoreCoordWidth[ACol].Left + MargeHTexte, TempPosY + ((TempPosBottomY - TempPosY - ActiveCellOnMerged.CurrentTextHeight) / 2), StoreCoordWidth[IndexMergedOnLine].Right - MargeHTexte, TempPosBottomY, ActiveCellOnMerged.GetTextAndStyleVPE);
                                                //trait haut
                                                        if ((ARow > 0) and not(IsNewPage)) or Not(_Nue) or
                                                           (((ARow = 0) or (IsNewPage)) and Not(_Nue)) then
                                                                if _Quadrillagehoritontal or (((ARow = 0) or (IsNewPage)) and Not(_Nue)) then
                                                                CurrentVPE.Line(StoreCoordWidth[ACol].Left, TempPosY, StoreCoordWidth[IndexMergedOnLine].Right, TempPosY);
                                                //trait bas
                                                        if ((IndexMergedOnColumn < _Grille.RowCount - 1) and not(IsNextRowNewPage)) or
                                                           (((ARow = _Grille.RowCount - 1) or (IsNextRowNewPage)) and Not(_Nue)) then
                                                                begin
                                                                        if (ActiveCellOnMerged.ISeparateurBas) or (((ARow = _Grille.RowCount - 1) or (IsNextRowNewPage)) and Not(_Nue)) then
                                                                                if _Quadrillagehoritontal or (((ARow = _Grille.RowCount - 1) or (IsNextRowNewPage)) and Not(_Nue)) then
                                                                                        CurrentVPE.Line(StoreCoordWidth[ACol].Left, tempPosBottomY, StoreCoordWidth[IndexMergedOnLine].Right, tempPosBottomY);
                                                                end;
                                                //trait gauche
                                                        if (ACol > 0) or Not(_Nue) then
                                                                if (_QuadrillageVertical) or ((ACol = 0) and Not(_Nue)) then
                                                                        CurrentVPE.Line(StoreCoordWidth[ACol].Left, tempPosY, StoreCoordWidth[ACol].Left, tempPosBottomY);
                                                //trait droit
                                                        if (IndexMergedOnLine < _Grille.ColCount - 1) or Not(_Nue) then
                                                                begin
                                                                        if (ActiveCellOnMerged.ISeparateurDroite) or ((IndexMergedOnLine = _Grille.ColCount - 1) and Not(_Nue))  then
                                                                                if (_QuadrillageVertical) or ((IndexMergedOnLine = _Grille.ColCount - 1) and Not(_Nue)) then
                                                                                        CurrentVPE.Line(StoreCoordWidth[IndexMergedOnLine].Right, tempPosY, StoreCoordWidth[IndexMergedOnLine].Right, tempPosBottomY);
                                                                end;
                                                CurrentVPE.nBottom := TempPosY;
                                                end;

                                end;
                CurrentVPE.nBottom := TempPosY + StoreCoordHeight[ARow].RowHeight;
                Inc(ARow);
                Until ARow = _Grille.RowCount;

SetLength(StoreCoordWidth, 0);
SetLength(StoreCoordHeight, 0);
tempFont.Destroy;
end;
//*************************************************************************************************************************************
var Store : array[0..999] of record
                               Left,
                               Right : double;
                             end;
procedure TVPE.GridFirst(Ligne : array of TCellule;
                         RefY : TPosRefY = prLastBottomMarge;
                         OffsetY : double = 0);
var i : integer;
begin
  ZeroMemory(@Store, SizeOf(Store));
  for i:=Low(Ligne) to High(Ligne) do begin
    if IsNan(Ligne[i].OffsetD) then Ligne[i].OffsetD := CurrentVPE.nRight - CurrentVPE.nLeft;
    if (i = Low(Ligne)) then WriteRTF(Ligne[i].Texte, true, hCENTER, Ligne[i].RefG, Ligne[i].RefD, RefY, prFree, Ligne[i].OffsetG, Ligne[i].OffsetD, OffsetY, 0)
                        else WriteRTF(Ligne[i].Texte, true, hCENTER, Ligne[i].RefG, Ligne[i].RefD, prLastTop, prLastBottom, Ligne[i].OffsetG, Ligne[i].OffsetD, 0, 0);
    Store[i].Left  := CurrentVPE.nLeft;
    Store[i].Right := CurrentVPE.nRight;
  end;
end;
//*************************************************************************************************************************************
procedure TVPE.GridNext(Ligne : array of string);
var i : integer;
begin
  for i:=Low(Ligne) to High(Ligne) do begin
    CurrentVPE.nLeft  := Store[i].Left;
    CurrentVPE.nRight := Store[i].Right;
    if (i = Low(Ligne)) then WriteRTF(Ligne[i], true, hCENTER, prLastLeft, prLastRight, prLastBottom, prFree, 0, 0, 0, 0)
                        else WriteRTF(Ligne[i], true, hCENTER, prLastLeft, prLastRight, prLastTop, prLastBottom, 0, 0, 0, 0);
  end;
end;
//*************************************************************************************************************************************
procedure TVPE.Dessin(Tag : pointer;
                      RefX : TPosRefX = prLeftMargin;
                      RefX2 : TPosRefX = prRightMargin;
                      RefY : TPosRefY = prLastBottom;
                      RefY2 : TPosRefY = prFree;
                      OffsetX : double = 0;
                      OffsetX2 : double = 0;
                      OffsetY : double = 0;
                      OffsetY2 : double = 0);
begin
  OffsetX  := PosRefX(RefX, OffsetX);
  OffsetX2 := PosRefX(RefX2, OffsetX2);
  OffsetY  := PosRefY(RefY, OffsetY);
  OffsetY2 := PosRefY(RefY2, OffsetY2);
  CurrentVPE.PenSize := 0;
  CurrentVPE.CreateUDO(OffsetX, OffsetY, OffsetX2, OffsetY2, integer(Tag));
end;
//*************************************************************************************************************************************
procedure TVPE.LigneHorizontale(_PosY : Double);
begin
  CurrentVPE.PenSize := 0.01;
  CurrentVPE.Line(CurrentVPE.nLeftMargin, _PosY, CurrentVPE.nRightMargin, _PosY);
end;
//*************************************************************************************************************************************
procedure TVPE.LigneHorizontale(RefY : TPosRefY = prLastBottom; OffsetY : double = 0);
begin
  CurrentVPE.PenSize := 0.01;
  CurrentVPE.Line(CurrentVPE.nLeftMargin, PosRefY(RefY, OffsetY), CurrentVPE.nRightMargin, PosRefY(RefY, OffsetY));
end;
{ ************************************************************************************************************************************************** }
procedure TVPE.DrawJPeg(_JPeg : TJPEGImage; _Left, _Top, _Right, _Bottom : Double; _IsProportional : Boolean = True; _VertCenter : Boolean = True; _WithBorder : Boolean = False);
var
  MemStream: TMemoryStream;
Begin
        if _IsProportional then
                GetProportionalImageDimensions(_JPeg.Height, _JPeg.Width, _Left, _Top, _Right, _Bottom, _VertCenter);
  MemStream := TMemoryStream.Create;
  _JPeg.SaveToStream(MemStream);
  DrawPictureFromStream(MemStream, _Left, _Top, _Right, _Bottom, _WithBorder);
  MemStream.Destroy;
End;
{ ************************************************************************************************************************************************** }
procedure TVPE.DrawBitmap(_Bitmap : Graphics.TBitmap; _Left, _Top, _Right, _Bottom : Double; _IsProportional : Boolean = True; _VertCenter : Boolean = True; _WithBorder : Boolean = False);
var
  MemStream: TMemoryStream;
Begin
        if _IsProportional then
                GetProportionalImageDimensions(_Bitmap.Height, _Bitmap.Width, _Left, _Top, _Right, _Bottom, _VertCenter);
  MemStream := TMemoryStream.Create;
  _Bitmap.SaveToStream(MemStream);
  DrawPictureFromStream(MemStream, _Left, _Top, _Right, _Bottom, _WithBorder);
  MemStream.Destroy;
End;
{ ************************************************************************************************************************************************** }
procedure TVPE.DrawPictureFromStream(_MemStream : TMemoryStream; _Left, _Top, _Right, _Bottom : Double; _WithBorder : Boolean = False);
var
  PicStream: TVPEStream;
  bytes_read: Integer;
  buffer: TByteArray;
Begin

        if _WithBorder then
                CurrentVPE.PenSize := 0.01
        else
                CurrentVPE.PenSize := 0;

  PicStream := CurrentVPE.CreateMemoryStream(0);

  _MemStream.Position:=0;

  while (_MemStream.Position < _MemStream.size) do
        begin
        bytes_read := _MemStream.Read(buffer, SizeOf(buffer));
        PicStream.Write(buffer, bytes_read);
        end;
CurrentVPE.PictureStream(PicStream, _Left, _Top, _Right,  _Bottom, IntToStr(_MemStream.Size));
PicStream.Close;
End;
{ ************************************************************************************************************************************************** }
function TVPE.GetTextHeight(_Text : String; _Left, _Right : Double; _Font : TFont) : Double;
begin
GetFontAndBrush(_Font, Nil);
CurrentVPE.RenderWrite(_Left, 0, _Right, VFREE, _Text);
Result := CurrentVPE.nRenderHeight;
end;
{ ************************************************************************************************************************************************** }
function TVPE.GetTextWidth(_Text : String; _Top, _Bottom : Double; _Font : TFont) : Double;
begin
GetFontAndBrush(_Font, Nil);
CurrentVPE.RenderWrite(0, _Top, VFree, _Bottom, _Text);
Result := CurrentVPE.nRenderWidth;
end;
{ ************************************************************************************************************************************************** }
function TVPE.GetTopPagePos : Double;
begin
Result := 0;
end;
{ ************************************************************************************************************************************************** }
function TVPE.GetTopMargePagePos : Double;
begin
Result := CurrentVPE.nTopMargin;
end;
{ ************************************************************************************************************************************************** }
function TVPE.GetBottomPagePos : Double;
begin
Result := CurrentVPE.nHeight;
end;
{ ************************************************************************************************************************************************** }
function TVPE.GetBottomMargePagePos : Double;
begin
Result := CurrentVPE.nBottomMargin;
end;
{ ************************************************************************************************************************************************** }
function TVPE.GetHeaderMargePagePos : Double;
Begin
Result := FHeaderMargePos;
End;
{ ************************************************************************************************************************************************** }
function TVPE.GetFooterMargePagePos : Double;
Begin
Result := FFooterMargePos;
End;
{ ************************************************************************************************************************************************** }
function TVPE.GetLeftPagePos : Double;
begin
Result := 0;
end;
{ ************************************************************************************************************************************************** }
function TVPE.GetLeftMargePagePos : Double;
begin
Result := CurrentVPE.nLeftMargin;
end;
{ ************************************************************************************************************************************************** }
function TVPE.GetRightPagePos : Double;
begin
Result := CurrentVPE.nWidth;
end;
{ ************************************************************************************************************************************************** }
function TVPE.GetRightMargePagePos : Double;
begin
Result := CurrentVPE.nRightMargin;
end;
{ ************************************************************************************************************************************************** }
function TVPE.GetCurrentYPos : Double;
begin
Result := CurrentVPE.nBottom;
end;
{ ************************************************************************************************************************************************** }
function TVPE.GetCurrentXPos : Double;
begin
Result := CurrentVPE.nLeft;
end;
{ ************************************************************************************************************************************************** }
procedure TVPE.SetCurrentYPos(_value : Double);
begin
CurrentVPE.nBottom := _Value;
end;
{ ************************************************************************************************************************************************** }
procedure TVPE.SetCurrentXPos(_value : Double);
begin
CurrentVPE.nLeft := _Value;
end;
//*************************************************************************************************************************************
function TVPE.PageCount : integer;
begin
  Result := CurrentVPE.PageCount;
end;
//*************************************************************************************************************************************
procedure TVPE.SetCurrentPage(Num : integer);
begin
  CurrentVPE.CurrentPage := Num;
end;
//*************************************************************************************************************************************
procedure TVPE.StorePos;
begin
  CurrentVPE.StorePos;
end;
//*************************************************************************************************************************************
procedure TVPE.RestorePos;
begin
  CurrentVPE.RestorePos;
end;
//*************************************************************************************************************************************
procedure TVPE.SplitterStart;
begin
//r�gis
//  FSplitterStart := CurrentVPE.LastInsertedObject.ObjectHandle;
end;
//*************************************************************************************************************************************
const cNoInit = -1;
function TVPE.SplitterEnd(RefY : TPosRefY; OffsetY : double) : boolean;
begin
  Result := (FSplitterStart = cNoInit) or (CurrentVPE.nBottom <= PosRefY(RefY, OffsetY));
end;
//*************************************************************************************************************************************
procedure TVPE.SplitterRewind;
//r�gis
//var Obj : TVPEObject;
begin
//r�gis
(*
  if (FSplitterStart <> cNoInit) then begin
    Obj := CurrentVPE.FirstObject;
    while (Obj.ObjectHandle <> FSplitterStart) do Obj := Obj.NextObject;
    Obj := Obj.NextObject;
    while (Obj.ObjectHandle <> 0) do begin
      Obj.Printable := false;
      Obj:= Obj.NextObject;
{ marche po
      Obj2 := Obj.CreateCopy;
      Obj:= Obj.NextObject;
      CurrentVPE.DeleteObject(Obj2);
      Obj2.Destroy;
}
    end;
  end;
  FSplitterStart := cNoInit;
*)
end;
//*************************************************************************************************************************************
function TVPE.cm2pixels(_Value : Double) : Integer;
var
        ppcm: double;
Const
        DPI = 96;
        ConvertInchesToCm = 2.54;

begin
    ppcm := DPI / ConvertInchesToCm;
    Result := Trunc(ppcm * _Value);

end;
//*************************************************************************************************************************************


end.
