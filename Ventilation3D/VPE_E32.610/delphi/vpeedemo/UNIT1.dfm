�
 TFORM1 0�  TPF0TForm1Form1Left� Top� 
AutoScrollCaptionVirtual Print EngineClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Menu	MainMenu1OldCreateOrder	PositionpoScreenCenterOnClose	FormCloseOnCreate
FormCreatePixelsPerInch`
TextHeight TLabelLabel1Left� Top� Width,HeightKCaptionVPEngineColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold ParentColor
ParentFont  TLabelLabel2Left:Top� WidthHeight	AlignmenttaRightJustifyCaptionVCLFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFont  	TVPEngineEnhancedLeft0TopXWidthYHeightABorderStylebsNoneTabOrder TabStop	VisibleOnDestroyWindowEnhancedDestroyWindowCaptionPrecision + CapabilitiesExternalWindow	PageOrientationVORIENT_PORTRAIT
PageFormat	VPAPER_A4	PageWidth       �@
PageHeight �������@Rulers	RulersMeasurecmGridMode�M����GridVisibleEnableHelpRouting		StatusBar	ToolBar	tbAbout	tbClose	tbGrid	tbHelp	
tbNavigate	tbOpen	tbSave	tbPrint	EnablePrintSetupDialog	tbScale		PaperView	DocFileReadOnlyPageScroller	tbMail	StatusSegment	PageScrollerTracking	PreviewCtrlJumpTop  	TVPEngine	StructureLeft� TopXWidthYHeightABorderStylebsNoneTabOrderVisibleOnDestroyWindowStructureDestroyWindowCaption	StructureExternalWindow	PageOrientationVORIENT_PORTRAIT
PageFormat	VPAPER_A4	PageWidth       �@
PageHeight �������@Rulers	RulersMeasurecmGridMode�M����GridVisibleEnableHelpRouting	StatusBar	ToolBar	tbAbout	tbClose	tbGrid	tbHelp	
tbNavigate	tbOpen	tbSave	tbPrint	EnablePrintSetupDialog	tbScale		PaperView	DocFileReadOnlyPageScroller	tbMail	StatusSegment	PageScrollerTracking	PreviewCtrlJumpTop  	TVPEngineUsageLeft0TopWidthHeightABorderStylebsNoneTabOrderVisibleOnDestroyWindowUsageDestroyWindowCaptionUsageExternalWindow	PageOrientationVORIENT_PORTRAIT
PageFormat	VPAPER_A4	PageWidth       �@
PageHeight �������@Rulers	RulersMeasurecmGridMode�M����GridVisibleEnableHelpRouting	StatusBar	ToolBar	tbAbouttbClose	tbGrid	tbHelp
tbNavigate	tbOpen	tbSave	tbPrint	EnablePrintSetupDialog	tbScale		PaperView	DocFileReadOnlyPageScroller	tbMail	StatusSegment	PageScrollerTracking	PreviewCtrlJumpTop  	TMainMenu	MainMenu1LeftTop 	TMenuItemFile1Caption&File 	TMenuItemExit1CaptionE&xitOnClick
Exit1Click   	TMenuItemDemos1Caption&Demos 	TMenuItemTemplateUsageCaption&Template UsageOnClickTemplateUsageClick  	TMenuItemEnhancedTemplateProcessing1Caption&Enhanced Template ProcessingOnClick EnhancedTemplateProcessing1Click  	TMenuItemShowTemplateStructure1Caption&Show Template StructureOnClickShowTemplateStructure1Click    TOpenDialog
OpenDialog
DefaultExtTPLFilter VPE Template Files (*.tpl)|*.TPLLeft�   