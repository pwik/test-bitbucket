�
 TFORM1 0�  TPF0TForm1Form1Left�Top� CaptionVirtual Print EngineClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Menu	MainMenu1OldCreateOrder	PositionpoScreenCenter
OnActivateFormActivateOnClose	FormCloseOnCreate
FormCreatePixelsPerInch`
TextHeight TLabelLabel1Left� Top� Width,HeightKCaptionVPEngineColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold ParentColor
ParentFont  TLabelLabel2Left:Top� WidthHeight	AlignmenttaRightJustifyCaptionVCLFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFont  	TVPEngineCapLeft0TopXWidthYHeightABorderStylebsNoneTabOrder TabStop	VisibleOnDestroyWindowCapDestroyWindowOnRequestPrintRequestPrintOnHelpCapHelpOnBeforeMail
BeforeMailOnAfterMail	AfterMailCaptionPrecision + CapabilitiesExternalWindowPageOrientationVORIENT_PORTRAIT
PageFormat	VPAPER_A4	PageWidth       �@
PageHeight �������@Rulers	RulersMeasurecmGridModexk����R���  P��"�� ��Í@ S�؃��   u3ɲ��F�誯���Ak�����  P��&�� ��[Í@ U��QS�U��؋E��rk��3�Uhǥ�d�0d� ���   uGridVisibleEnableHelpRouting		StatusBar	ToolBar	tbAbout	tbClose	tbGrid	tbHelp	
tbNavigate	tbOpen	tbSave	tbPrint	EnablePrintSetupDialog	tbScale		PaperView	DocFileReadOnlyPageScroller	tbMail	StatusSegment	PageScrollerTracking	PreviewCtrlJumpTop  	TVPEngineReportLeft� TopXWidthYHeightABorderStylebsNoneTabOrderVisibleOnDestroyWindowReportDestroyWindowOnRequestPrintRequestPrintOnBeforeMail
BeforeMailOnAfterMail	AfterMailCaptionReportExternalWindow	PageOrientationVORIENT_PORTRAIT
PageFormat	VPAPER_A4	PageWidth       �@
PageHeight �������@Rulers	RulersMeasurecmGridModexk����R���  P��"�� ��Í@ S�؃��   u3ɲ��F�誯���Ak�����  P��&�� ��[Í@ U��QS�U��؋E��rk��3�Uhǥ�d�0d� ���   uGridVisibleEnableHelpRouting	StatusBar	ToolBar	tbAbout	tbClose	tbGrid	tbHelp	
tbNavigate	tbOpen	tbSave	tbPrint	EnablePrintSetupDialog	tbScale		PaperView	DocFileReadOnlyPageScroller	tbMail	StatusSegment	PageScrollerTracking	PreviewCtrlJumpTop  	TVPEngineClrLeft TopXWidthYHeightABorderStylebsNoneTabOrderVisibleOnDestroyWindowClrDestroyWindowOnRequestPrintRequestPrintOnBeforeMail
BeforeMailOnAfterMail	AfterMailCaption
Color TestExternalWindow	PageOrientationVORIENT_PORTRAIT
PageFormat	VPAPER_A4	PageWidth       �@
PageHeight �������@RulersRulersMeasurecmGridModexk����R���  P��"�� ��Í@ S�؃��   u3ɲ��F�誯���Ak�����  P��&�� ��[Í@ U��QS�U��؋E��rk��3�Uhǥ�d�0d� ���   uGridVisibleEnableHelpRouting	StatusBarToolBar	tbAbouttbClosetbGridtbHelp
tbNavigatetbOpen	tbSave	tbPrint	EnablePrintSetupDialog	tbScale	PaperViewDocFileReadOnlyPageScroller	tbMail	StatusSegment	PageScrollerTracking	PreviewCtrlJumpTop  	TVPEngineSpeedLefthTopXWidthYHeightABorderStylebsNoneTabOrderVisibleOnDestroyWindowSpeedDestroyWindowOnRequestPrintRequestPrintOnBeforeMail
BeforeMailOnAfterMail	AfterMailCaptionSpeed + TablesExternalWindow	PageOrientationVORIENT_PORTRAIT
PageFormat	VPAPER_A4	PageWidth       �@
PageHeight �������@Rulers	RulersMeasurecmGridModexk����R���  P��"�� ��Í@ S�؃��   u3ɲ��F�誯���Ak�����  P��&�� ��[Í@ U��QS�U��؋E��rk��3�Uhǥ�d�0d� ���   uGridVisibleEnableHelpRouting	StatusBar	ToolBar	tbAbout	tbClose	tbGrid	tbHelp	
tbNavigate	tbOpen	tbSave	tbPrint	EnablePrintSetupDialog	tbScale		PaperView	DocFileReadOnlyPageScroller	tbMail	StatusSegment	PageScrollerTracking	PreviewCtrlJumpTop  	TVPEngineAutoLeft�TopXWidthYHeightABorderStylebsNoneTabOrderVisibleOnDestroyWindowAutoDestroyWindowOnRequestPrintRequestPrintOnBeforeMail
BeforeMailOnAfterMail	AfterMailCaptionAuto RenderingExternalWindow	PageOrientationVORIENT_PORTRAIT
PageFormat	VPAPER_A4	PageWidth       �@
PageHeight �������@Rulers	RulersMeasurecmGridModexk����R���  P��"�� ��Í@ S�؃��   u3ɲ��F�誯���Ak�����  P��&�� ��[Í@ U��QS�U��؋E��rk��3�Uhǥ�d�0d� ���   uGridVisibleEnableHelpRouting	StatusBar	ToolBar	tbAbout	tbClose	tbGrid	tbHelp	
tbNavigate	tbOpen	tbSave	tbPrint	EnablePrintSetupDialog	tbScale		PaperView	DocFileReadOnlyPageScroller	tbMail	StatusSegment	PageScrollerTracking	PreviewCtrlJumpTop  	TVPEngineWelcomeLeft0TopWidthHeightABorderStylebsNoneTabOrderVisibleOnDestroyWindowWelcomeDestroyWindowOnRequestPrintRequestPrintOnBeforeMail
BeforeMailOnAfterMail	AfterMailCaptionWelcomeExternalWindow	PageOrientationVORIENT_PORTRAIT
PageFormat	VPAPER_A4	PageWidth       �@
PageHeight �������@Rulers	RulersMeasurecmGridModexk����R���  P��"�� ��Í@ S�؃��   u3ɲ��F�誯���Ak�����  P��&�� ��[Í@ U��QS�U��؋E��rk��3�Uhǥ�d�0d� ���   uGridVisibleEnableHelpRouting	StatusBar	ToolBar	tbAbouttbClose	tbGrid	tbHelp
tbNavigate	tbOpen	tbSave	tbPrint	EnablePrintSetupDialog	tbScale		PaperView	DocFileReadOnlyPageScroller	tbMail	StatusSegment	PageScrollerTracking	PreviewCtrlJumpTop  	TMainMenu	MainMenu1LeftTop 	TMenuItemFile1Caption&File 	TMenuItemGenerateData1Caption&Generate ReportOnClickGenerateData1Click  	TMenuItemDeleteReport1Caption&Delete ReportOnClickDeleteReport1Click  	TMenuItemN1Caption-  	TMenuItemExit1CaptionE&xitOnClick
Exit1Click   	TMenuItemDemos1Caption&Demos 	TMenuItemWelcome1Caption&WelcomeOnClickWelcome1Click  	TMenuItemN2Caption-  	TMenuItemCapabilitiesPrecision1Caption&Precision + CapabilitiesOnClickCapabilitiesPrecision1Click  	TMenuItemPrintinBackground1CaptionPrint in &BackgroundOnClickPrintinBackground1Click  	TMenuItemN3Caption-  	TMenuItemSpeedTables1Caption&Speed + TablesOnClickSpeedTables1Click  	TMenuItemN4Caption-  	TMenuItemColors1Caption&ColorsOnClickColors1Click  	TMenuItemCloseColors1CaptionClose &ColorsOnClickCloseColors1Click  	TMenuItemN5Caption-  	TMenuItemReport1Caption&ReportOnClickReport1Click  	TMenuItemeMailBkgCaption- " - &e-Mail in BackgroundOnClickeMailBkgClick  	TMenuItemeMailBkgWODlgCaption*- " - e-Mail in Background without &DialogOnClickeMailBkgWODlgClick  	TMenuItemN6Caption-  	TMenuItemAutoRender1Caption&Auto RenderingOnClickAutoRender1Click     