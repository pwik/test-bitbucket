unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, VPE_VCL, StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Doc: TVPEngine;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  stream: TVPEStream;
  pdfFile: TFileStream;
  bytes_read: Integer;
  buffer: array [0..4095] of byte;
begin
  Doc.OpenDoc();
  Doc.Print(1, 1, 'Hello World!');

  // Create a memory stream
  stream := Doc.CreateMemoryStream(0);

  // Write the VPE document as PDF file to the memory stream
  Doc.DocExportType := VPE_DOC_TYPE_PDF;
  Doc.WriteDocStream(stream);

  // Export the memory stream to an external file.
  // This is only for demonstration purposes, normally you would write the memory stream now
  // as BLOB to a database or do something like that.
  pdfFile := TFileStream.Create('test.pdf', fmCreate);

  // Seek to the beginning of the memory stream
  stream.Seek(0);

  // As long as there is data in the stream, copy it
  while not stream.IsEof do
  begin
    // Read data from the VPE stream into a memory buffer
    bytes_read := stream.Read(buffer, 4096);

    // Write the memory buffer to the external file
    pdfFile.Write(buffer, bytes_read);
  end;

  pdfFile.Destroy;
  stream.Close;
  Application.MessageBox('PDF file "test.pdf" created in working directory', 'Note:');
end;

end.
