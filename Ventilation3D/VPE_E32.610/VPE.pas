unit VPE;


interface

uses
  Math, Windows, Controls, ExtCtrls, VPE_VCL;

type
  TPageOrientation = (VORIENT_PORTRAIT = Ord(VPE_VCL.VORIENT_PORTRAIT),
                      VORIENT_LANDSCAPE = Ord(VPE_VCL.VORIENT_LANDSCAPE));
  THAlign = (hLEFT         = Ord(VPE_VCL.ALIGN_LEFT),
             hRIGHT        = Ord(VPE_VCL.ALIGN_RIGHT),
             hCENTER       = Ord(VPE_VCL.ALIGN_CENTER),
             hJUSTIFIED    = Ord(VPE_VCL.ALIGN_JUSTIFIED),
             hJUSTIFIED_AB = Ord(VPE_VCL.ALIGN_JUSTIFIED_AB),
             hLEFT_CUT     = Ord(VPE_VCL.ALIGN_LEFT_CUT));
  TPosRefX = (prLeftMargin,
              prRightMargin,
              prCenter,
              prLastLeft,
              prLastRight);
  TPosRefY = (prLastBottom,
              prLastBottomMarge,
              prLastTop,
              prTopMargin,
              prBottomMargin,
              prFree
             );
  TCellule = record
               Texte : string;
               RefG : TPosRefX;
               OffsetG : double;
               RefD : TPosRefX;
               OffsetD : double;
             end;
  TVPE_DessinCallback = procedure(Tag : integer; DC : HDC; Left, Right, Top, Bottom : integer) of object;

  TVPE = class
  private
    CurrentVPE : TVPEngine;
    VPE_DessinCallback : TVPE_DessinCallback;
    FSplitterStart : integer;
//    LogoVS : TVPEStream;
    procedure VPEUDOPaint(Sender : TObject);
    function PosRefX(PosRef : TPosRefX; Offset : double) : double;
    function PosRefY(PosRef : TPosRefY; Offset : double) : double;
  public
    procedure OpenDoc(Parent : TWinControl; CallbackDessin : TVPE_DessinCallback);
    procedure WriteDoc(Fichier : string);
    procedure CloseDoc;
    procedure NewPage(Orientation : TPageOrientation; Marge : double);
    procedure WriteRTF(const RTF : string;
                       Bordure : boolean = false;
                       HAlign : THAlign = hLEFT;
                       RefX : TPosRefX = prLeftMargin;
                       RefX2 : TPosRefX = prRightMargin;
                       RefY : TPosRefY = prLastBottomMarge;
                       RefY2 : TPosRefY = prFree;
                       OffsetX : double = 0;
                       OffsetX2 : double = 0;
                       OffsetY : double = 0;
                       OffsetY2 : double = 0);
    function Cellule(Texte : string = '';
                     RefG : TPosRefX = prLastRight;
                     OffsetG : double = 0;
                     RefD : TPosRefX = prLastRight;
                     OffsetD : double = NaN) : TCellule;
    procedure GridFirst(Ligne : array of TCellule;
                        RefY : TPosRefY = prLastBottomMarge;
                        OffsetY : double = 0);
    procedure GridNext(Ligne : array of string);
    procedure Dessin(Tag : pointer;
                     RefX : TPosRefX = prLeftMargin;
                     RefX2 : TPosRefX = prRightMargin;
                     RefY : TPosRefY = prLastBottom;
                     RefY2 : TPosRefY = prFree;
                     OffsetX : double = 0;
                     OffsetX2 : double = 0;
                     OffsetY : double = 0;
                     OffsetY2 : double = 0);
    procedure LigneHorizontale(RefY : TPosRefY = prLastBottom;
                               OffsetY : double = 0);
    function PageCount : integer;
    procedure SetCurrentPage(Num : integer);
    procedure StorePos;
    procedure RestorePos;
    procedure SplitterStart;
    function SplitterEnd(RefY : TPosRefY; OffsetY : double) : boolean;
    procedure SplitterRewind;
  end;

implementation

uses
  Classes, SysUtils;

const
  cMargeInter = 0.3; //marge entre chaque �l�ments dans une page

//*************************************************************************************************************************************
procedure TVPE.VPEUDOPaint(Sender : TObject);
begin
  if Assigned(VPE_DessinCallback) then VPE_DessinCallback(TVPEngine(Sender).UDOlParam,
                                                          TVPEngine(Sender).UDODC,
                                                          TVPEngine(Sender).nUDOLeft,
                                                          TVPEngine(Sender).nUDORight,
                                                          TVPEngine(Sender).nUDOTop,
                                                          TVPEngine(Sender).nUDOBottom);
end;
//*************************************************************************************************************************************
function TVPE.PosRefX(PosRef : TPosRefX; Offset : double) : double;
begin
  case PosRef of
    prLeftMargin  : Result := Offset + CurrentVPE.nLeftMargin;
    prRightMargin : Result := Offset + CurrentVPE.nRightMargin;
    prCenter      : Result := Offset + (CurrentVPE.nRightMargin + CurrentVPE.nLeftMargin)/2;
    prLastLeft    : Result := Offset + CurrentVPE.nLeft;
    prLastRight   : Result := Offset + CurrentVPE.nRight;
    else Result := -1;
  end;
end;
//*************************************************************************************************************************************
function TVPE.PosRefY(PosRef : TPosRefY; Offset : double) : double;
begin
  case PosRef of
    prLastBottom      : Result := Offset + CurrentVPE.nBottom;
    prLastBottomMarge : Result := Offset + CurrentVPE.nBottom + cMargeInter;
    prTopMargin       : Result := Offset + CurrentVPE.nTopMargin;
    prBottomMargin    : Result := Offset + CurrentVPE.nBottomMargin;
    prLastTop         : Result := Offset + CurrentVPE.nTop;
    prFree            : Result := VFREE
    else Result := -1;
  end;
end;
//*************************************************************************************************************************************
procedure TVPE.OpenDoc(Parent : TWinControl; CallbackDessin : TVPE_DessinCallback);
begin
  CurrentVPE := TVPEngine.Create(nil);
  CurrentVPE.Visible := false;
  CurrentVPE.Parent := Parent;
  VPE_DessinCallback := CallbackDessin;
  CurrentVPE.OnUDOPaint := VPEUDOPaint;

  CurrentVPE.OpenDoc;
  CurrentVPE.License('VPE-E2610-5510', 'VTD4-LVTD');

  CurrentVPE.Compression   := DOC_COMPRESS_FLATE;
  CurrentVPE.AutoBreakMode := AUTO_BREAK_NO_LIMITS;
  CurrentVPE.DocExportType := VPE_DOC_TYPE_PDF;
  CurrentVPE.Encryption    := DOC_ENCRYPT_STREAM;
  CurrentVPE.Protection    := PDF_ALLOW_PRINT + PDF_ALLOW_HIQ_PRINT + PDF_ALLOW_COPY;

  CurrentVPE.PictureKeepAspect := true;
  CurrentVPE.PictureBestFit    := false;
  CurrentVPE.PictureEmbedInDoc := true; //manifestement ne marche pas
end;
//*************************************************************************************************************************************
procedure TVPE.WriteDoc(Fichier : string);
begin
  CurrentVPE.WriteDoc(Fichier);
end;
//*************************************************************************************************************************************
procedure TVPE.CloseDoc;
begin
{
  if (LogoVS <> nil) then begin
    LogoVS.Close;
    LogoVS.Destroy;
  end;
}
  CurrentVPE.CloseDoc;
  CurrentVPE.Destroy;
end;
//*************************************************************************************************************************************
procedure TVPE.NewPage(Orientation : TPageOrientation; Marge : double);
begin
  if (CurrentVPE.LastInsertedObject <> nil) then CurrentVPE.PageBreak;
  CurrentVPE.FontName := 'arial';
  CurrentVPE.FontSize := 8;
  CurrentVPE.PageOrientation := VPE_VCL.TPageOrientation(Orientation);
  CurrentVPE.nLeftMargin := Marge;
  CurrentVPE.nRightMargin := CurrentVPE.PageWidth - Marge;
  CurrentVPE.nTopMargin := Marge;
  CurrentVPE.nBottomMargin := CurrentVPE.PageHeight - Marge;
end;
//*************************************************************************************************************************************
procedure TVPE.WriteRTF(const RTF : string;
                        Bordure : boolean = false;
                        HAlign : THAlign = hLEFT;
                        RefX : TPosRefX = prLeftMargin;
                        RefX2 : TPosRefX = prRightMargin;
                        RefY : TPosRefY = prLastBottomMarge;
                        RefY2 : TPosRefY = prFree;
                        OffsetX : double = 0;
                        OffsetX2 : double = 0;
                        OffsetY : double = 0;
                        OffsetY2 : double = 0);
begin
  CurrentVPE.TextAlignment := Ord(HAlign);
  OffsetX  := PosRefX(RefX, OffsetX);
  OffsetX2 := PosRefX(RefX2, OffsetX2);
  OffsetY  := PosRefY(RefY, OffsetY);
  OffsetY2 := PosRefY(RefY2, OffsetY2);
  if Bordure then begin
    CurrentVPE.PenSize := 0.01;
    CurrentVPE.WriteBoxRTF(OffsetX, OffsetY, OffsetX2, OffsetY2, '{' + RTF + '}');
  end else CurrentVPE.WriteRTF(OffsetX, OffsetY, OffsetX2, OffsetY2, '{' + RTF + '}');
end;
//*************************************************************************************************************************************
function TVPE.Cellule(Texte : string = '';
                      RefG : TPosRefX = prLastRight;
                      OffsetG : double = 0;
                      RefD : TPosRefX = prLastRight;
                      OffsetD : double = NaN) : TCellule;
begin
  Result.Texte := Texte;
  Result.RefG := RefG;
  Result.OffsetG := OffsetG;
  Result.RefD := RefD;
  Result.OffsetD := OffsetD;
end;
//*************************************************************************************************************************************
var Store : array[0..999] of record
                               Left,
                               Right : double;
                             end;
procedure TVPE.GridFirst(Ligne : array of TCellule;
                         RefY : TPosRefY = prLastBottomMarge;
                         OffsetY : double = 0);
var i : integer;
begin
  ZeroMemory(@Store, SizeOf(Store));
  for i:=Low(Ligne) to High(Ligne) do begin
    if IsNan(Ligne[i].OffsetD) then Ligne[i].OffsetD := CurrentVPE.nRight - CurrentVPE.nLeft;
    if (i = Low(Ligne)) then WriteRTF(Ligne[i].Texte, true, hCENTER, Ligne[i].RefG, Ligne[i].RefD, RefY, prFree, Ligne[i].OffsetG, Ligne[i].OffsetD, OffsetY, 0)
                        else WriteRTF(Ligne[i].Texte, true, hCENTER, Ligne[i].RefG, Ligne[i].RefD, prLastTop, prLastBottom, Ligne[i].OffsetG, Ligne[i].OffsetD, 0, 0);
    Store[i].Left  := CurrentVPE.nLeft;
    Store[i].Right := CurrentVPE.nRight;
  end;
end;
//*************************************************************************************************************************************
procedure TVPE.GridNext(Ligne : array of string);
var i : integer;
begin
  for i:=Low(Ligne) to High(Ligne) do begin
    CurrentVPE.nLeft  := Store[i].Left;
    CurrentVPE.nRight := Store[i].Right;
    if (i = Low(Ligne)) then WriteRTF(Ligne[i], true, hCENTER, prLastLeft, prLastRight, prLastBottom, prFree, 0, 0, 0, 0)
                        else WriteRTF(Ligne[i], true, hCENTER, prLastLeft, prLastRight, prLastTop, prLastBottom, 0, 0, 0, 0);
  end;
end;
//*************************************************************************************************************************************
procedure TVPE.Dessin(Tag : pointer;
                      RefX : TPosRefX = prLeftMargin;
                      RefX2 : TPosRefX = prRightMargin;
                      RefY : TPosRefY = prLastBottom;
                      RefY2 : TPosRefY = prFree;
                      OffsetX : double = 0;
                      OffsetX2 : double = 0;
                      OffsetY : double = 0;
                      OffsetY2 : double = 0);
begin
  OffsetX  := PosRefX(RefX, OffsetX);
  OffsetX2 := PosRefX(RefX2, OffsetX2);
  OffsetY  := PosRefY(RefY, OffsetY);
  OffsetY2 := PosRefY(RefY2, OffsetY2);
  CurrentVPE.PenSize := 0;
  CurrentVPE.CreateUDO(OffsetX, OffsetY, OffsetX2, OffsetY2, integer(Tag));
end;
//*************************************************************************************************************************************
procedure TVPE.LigneHorizontale(RefY : TPosRefY = prLastBottom; OffsetY : double = 0);
begin
  CurrentVPE.PenSize := 0.01;
  CurrentVPE.Line(CurrentVPE.nLeftMargin, PosRefY(RefY, OffsetY), CurrentVPE.nRightMargin, PosRefY(RefY, OffsetY));
end;
//*************************************************************************************************************************************
function TVPE.PageCount : integer;
begin
  Result := CurrentVPE.PageCount;
end;
//*************************************************************************************************************************************
procedure TVPE.SetCurrentPage(Num : integer);
begin
  CurrentVPE.CurrentPage := Num;
end;
//*************************************************************************************************************************************
procedure TVPE.StorePos;
begin
  CurrentVPE.StorePos;
end;
//*************************************************************************************************************************************
procedure TVPE.RestorePos;
begin
  CurrentVPE.RestorePos;
end;
//*************************************************************************************************************************************
procedure TVPE.SplitterStart;
begin
  FSplitterStart := CurrentVPE.LastInsertedObject.ObjectHandle;
end;
//*************************************************************************************************************************************
const cNoInit = -1;
function TVPE.SplitterEnd(RefY : TPosRefY; OffsetY : double) : boolean;
begin
  Result := (FSplitterStart = cNoInit) or (CurrentVPE.nBottom <= PosRefY(RefY, OffsetY));
end;
//*************************************************************************************************************************************
procedure TVPE.SplitterRewind;
var Obj : TVPEObject;
begin
  if (FSplitterStart <> cNoInit) then begin
    Obj := CurrentVPE.FirstObject;
    while (Obj.ObjectHandle <> FSplitterStart) do Obj := Obj.NextObject;
    Obj := Obj.NextObject;
    while (Obj.ObjectHandle <> 0) do begin
      Obj.Printable := false;
      Obj:= Obj.NextObject;
{ marche po
      Obj2 := Obj.CreateCopy;
      Obj:= Obj.NextObject;
      CurrentVPE.DeleteObject(Obj2);
      Obj2.Destroy;
}
    end;
  end;
  FSplitterStart := cNoInit;
end;
//*************************************************************************************************************************************


end.
