Unit Ventil_Impressions;
             
{$Include '..\Ventil_Directives.pas'}

Interface

Uses
  Classes, Graphics, SysUtils, Math, Forms, Dialogs, Types,
  Ventil_EdibatecProduits, Ventil_ChiffrageTypes, Ventil_Troncon, Ventil_Bouche, Ventil_Registre, Jpeg, CAd_Acoustique,

  GmObjects,
  {$IfDef PRINTRTF}
  BBSRTF,
  {$EndIf}
  Ventil_Logement,
  Grids;

Type
  TPrintBouche = Record
    Bouche      : TVentil_Bouche;
    SurColonnes : Array Of String;
    Nombre      : Integer;
  End;
  TListePrintBouche = Array Of TPrintBouche;

Function TypeElementTerminal(_Troncon: TVentil_Troncon): String;
Function CouleurElementTerminal(_Troncon: TVentil_Troncon): TColor;


{ Impressions des bouches et entr�es d'air pour chaque logement du b�timent }
Procedure Impression_Logements(_ImpressionEnCours : Boolean; _Lanceur: TForm);
{ Fonctionnement du r�seau � d�bit maxi ou mini }
Procedure Impression_FonctionnnementSelonDebit(_ImpressionEnCours : Boolean; _ADebitMaxi: Boolean; _Lanceur: TForm);
{ Pressions limites pour bon fontionnement des bouches }
Procedure Impression_PressionsLimitesPourBouches(_ImpressionEnCours : Boolean; _Lanceur: TForm);
{ Pertes de charge entre le caisson et les bouches }
Procedure Impression_PdcCaissonBouches(_ImpressionEnCours : Boolean; _Lanceur: TForm);
{ Sch�mas des colonnes }
Procedure Impression_SchemasColonnes(_ImpressionEnCours : Boolean; _Lanceur: TForm);
Procedure SetDataImp_Schemas;
Procedure Impression_SchemasRecapColonnes(_ImpressionEnCours: Boolean; _Lanceur: TForm);
{ Sch�mas des �tages }
procedure FinalizeImpression;
Procedure Impression_SchemasEtages(_ImpressionEnCours: Boolean; _Lanceur: TForm);
{ Chiffrage s�lectionn� }
Procedure Impression_Quantitatif(_Chiffrage: TChiffrage; _ImpressionEnCours: Boolean; _Lanceur: TForm; _IsChiffrage : boolean = true);
{ Impression des caract�ristiques g�n�rales de l'installation }
Procedure Impression_DonneesGenerales(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Procedure SetDataImp_DonneesGenerale;
{ Impression des caract�ristiques des avis techniques utilis�s dans l'installation }
Procedure Impression_DonneesAvisTechniques(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Procedure SetDataImp_DonneesAvisTechniques;
{ Impression des caract�ristiques des entr�es d'air utilis�s dans l'installation }
Procedure Impression_DonneesEntreesAir(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Procedure Impression_DonneesEntreesAirVIM(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Procedure Impression_DonneesEntreesAirANJOS(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Procedure SetDataImp_DonneesEntreesAir;
{ Impression acoustique des bouches cuisine }
Procedure Impression_Acoustique(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Procedure Impression_AcoustiqueANJOS(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Procedure Impression_AcoustiqueVIM(_ImpressionEnCours: Boolean; _Lanceur: TForm);
{ Impression acoustique fa�ade }
Procedure Impression_AcoustFacade(_ImpressionEnCours: Boolean; _Lanceur: TForm);
{ Impression Calcul retour }
Procedure SetDataImp_CalculRetour;
Procedure Impression_CalculRetour(_ImpressionEnCours: Boolean; _Lanceur: TForm);
{ Impression de la courbe th�orique de fonctionnement du r�seau }
Procedure Impression_CourbeFonctionnement(_ImpressionEnCours: Boolean; _Lanceur: TForm);
{ Impression de la fiche du caisson }
Procedure SetDataImp_FicheCaissonOld;
Procedure SetDataImp_FicheCaisson;
{ Impression des courbes du caisson }
Procedure SetDataImp_CourbesCaisson;

{ Impression de la fiche du caisson }
Procedure Impression_FicheCaissonOld(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Procedure Impression_FicheCaisson(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Procedure Impression_CourbesCaisson;

{ Impression de l'acoustique }
Procedure SetDataImp_Acoustique;
{ Impression de la page de garde avec les infos de l'�tude }
Procedure Impression_PageDeGarde(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Function GetPiedDePage : String;

Procedure ImpressionHautDePageAnjos(NbLignesSautees : Integer = 1);
Procedure ImpressionBasDePageAnjos(NbLignesSautees : Integer = 1);

{ G�n�ration des fichiers d'impression � exporter sur demande }
Procedure SetDataImp;


Implementation
Uses
  Ventil_Utils, CADAero_Echange, Ventil_Edibatec,
  BBS_Progress,
  GmGridPrint, GmTypes, GmFuncs,
  Ventil_Const, Ventil_TeSouche, Ventil_Accident, Ventil_Caisson, Ventil_Collecteur, Ventil_Bifurcation, Ventil_Declarations, Ventil_Etude,
  Ventil_SystemeVMC, Ventil_SortieToiture, Ventil_FonctionnementReseau,
  Ventil_ApercuAvantImpressions, Ventil_Batiment, Ventil_Types,
  Ventil_Commun,
  Ventil_AcoustiqueDn;

{ ************************************************************************************************************************************************* }
                            { Impressions des bouches et entr�es d'air pour chaque logement du b�timent }
{ ************************************************************************************************************************************************* }
Function RecapBoucheDuLogement(_Logement : TVentil_Logement; Var _ListeBouches: TListePrintBouche): Integer;
Var
  m_NoComposant  : Integer;
  m_Bouche       : TVentil_Bouche;
  m_NbCuisines   : Integer;
  m_BoucheUtile  : Boolean;
  m_NoBouche     : Integer;
Begin
  m_NbCuisines := 0;
  SetLength(_ListeBouches, 0);
  For m_NoComposant := 0 To Etude.Batiment.Composants.Count - 1 Do If Etude.Batiment.Composants[m_NoComposant].InheritsFrom(TVentil_Bouche) Then Begin
    m_Bouche := TVentil_Bouche(Etude.Batiment.Composants[m_NoComposant]);
    If m_Bouche.Logement = _Logement Then Begin
      m_BoucheUtile := False;
      m_NoBouche    := Low(_ListeBouches);
      While (m_NoBouche <= High(_ListeBouches)) And (Not m_BoucheUtile) Do Begin
        If (_ListeBouches[m_NoBouche].Bouche = m_Bouche) And IsNotNil(m_Bouche.Collecteur) Then Begin
          SetLength(_ListeBouches[m_NoBouche].SurColonnes, Length(_ListeBouches[m_NoBouche].SurColonnes) + 1);                                               
          _ListeBouches[m_NoBouche].SurColonnes[High(_ListeBouches[m_NoBouche].SurColonnes)] := m_Bouche.Collecteur.Parentcalcul.Reference;
          m_BoucheUtile := True;
        End;
        Inc(m_NoBouche);
      End;
      If Not m_BoucheUtile Then Begin
        SetLength(_ListeBouches, Length(_ListeBouches) + 1);
        _ListeBouches[High(_ListeBouches)].Bouche := m_Bouche;
        _ListeBouches[High(_ListeBouches)].Nombre := 1;
        If IsNotNil(m_Bouche.Collecteur) Then Begin
          SetLength(_ListeBouches[m_NoBouche].SurColonnes, Length(_ListeBouches[m_NoBouche].SurColonnes) + 1);
          _ListeBouches[m_NoBouche].SurColonnes[High(_ListeBouches[m_NoBouche].SurColonnes)] := m_Bouche.Collecteur.Parentcalcul.Reference;
        End;
      End;
      If m_Bouche.TypePiece = c_cuisine Then Inc(m_NbCuisines);
    End;
  End;
  If m_NbCuisines = 0 Then m_NbCuisines := 1;
  Result := m_NbCuisines;
End;
{ ************************************************************************************************************************************************* }
Procedure Imprime_EntreesSejourLogement(_Logement: TVentil_Logement; _NbCuisines: Integer; Var _Grid: TStringGrid);
Var
  m_Row       : Integer;
  m_NoCuisine : Integer;
  m_NoEa      : Integer;
Begin
  If IsNotNil(_Logement.Systeme) Then For m_NoCuisine := 1 To _NbCuisines Do For m_NoEa := 0 To 1 Do
        If _Logement.Systeme.EntreeAir[_Logement.TypeLogement, 1][m_NoEA].Nombre > 0 Then Begin
    _Grid.RowCount := _Grid.RowCount + 1;
    m_Row := _Grid.RowCount - 1;
    _Grid.Objects[0, m_Row] := TCelluleImp.Create('S�jour',
                                                  GetFontInfos, 0, clBlack, clWhite, [fsBold], taLeftJustify);
   _Grid.Objects[1, m_Row] := TCelluleImp.Create('Entr�e',
                                                  GetFontInfos, 0, clBlack, clWhite, [fsBold], taCenter);
   if _Logement.Systeme.AlizeIII and _Logement.Systeme.HygroB and (_Logement.TypeLogement = c_TypeT3) and (_Logement.T3Optimise = c_Oui) then
   Begin
    _Grid.Objects[2, m_Row] := TCelluleImp.Create(_Logement.Systeme.EASejourT3Opt.Libelle2,
                                                  GetFontInfos, 0, clBlack, clWhite, [fsBold], taCenter);
    _Grid.Objects[3, m_Row] := TCelluleImp.Create(IntToStr(_Logement.Systeme.EASejourT3Opt.Nombre) + ' module(s)',
                                                  GetFontInfos, 0, clBlack, clWhite, [fsBold], taCenter);
   End
   Else
   Begin
    _Grid.Objects[2, m_Row] := TCelluleImp.Create(_Logement.Systeme.EntreeAir[_Logement.TypeLogement, 1][m_NoEA].Libelle2,
                                                  GetFontInfos, 0, clBlack, clWhite, [fsBold], taCenter);
    _Grid.Objects[3, m_Row] := TCelluleImp.Create(IntToStr(_Logement.Systeme.EntreeAir[_Logement.TypeLogement, 1][m_NoEA].Nombre) + ' module(s)',
                                                  GetFontInfos, 0, clBlack, clWhite, [fsBold], taCenter);
   End;
  End;

End;
{ ************************************************************************************************************************************************* }
Procedure Imprime_EntreesChambresLogement(_Logement: TVentil_Logement; _NbCuisines: Integer; Var _Grid: TStringGrid);
Var
  m_Row       : Integer;
  m_NoCuisine : Integer;
  m_NoChambre : Integer;
Begin
  If IsNotNil(_Logement.Systeme) Then For m_NoCuisine := 1 To _NbCuisines Do For m_NoChambre := 2 To _Logement.TypeLogement Do Begin
    _Grid.RowCount := _Grid.RowCount + 1;
    m_Row := _Grid.RowCount - 1;
    _Grid.Objects[0, m_Row] := TCelluleImp.Create('Chambre n�' + IntToStr(m_NoChambre),
                                                  GetFontInfos, 0, clBlack, clWhite, [fsBold], taLeftJustify);
    _Grid.Objects[1, m_Row] := TCelluleImp.Create('Entr�e',
                                                  GetFontInfos, 0, clBlack, clWhite, [fsBold], taCenter);
    _Grid.Objects[2, m_Row] := TCelluleImp.Create(_Logement.Systeme.EntreeAir[_Logement.TypeLogement, 2][0].Libelle2,
                                                  GetFontInfos, 0, clBlack, clWhite, [fsBold], taCenter);
    _Grid.Objects[3, m_Row] := TCelluleImp.Create(IntToStr(_Logement.Systeme.EntreeAir[_Logement.TypeLogement, 2][0].Nombre) + ' module(s)',
                                                  GetFontInfos, 0, clBlack, clWhite, [fsBold], taCenter);
  End;

End;
{ ************************************************************************************************************************************************* }
Procedure Imprime_BouchesLogement(_Logement: TVentil_Logement; _ListeBouches: TListePrintBouche; Var _Grid: TStringGrid);
Var
  m_NoBouche : Integer;
  m_Row      : Integer;
  m_Bouche   : TVentil_Bouche;
  m_Occurence: Integer;
  m_Str      : String;
Begin
  For m_NoBouche := Low(_ListeBouches) To High(_ListeBouches) Do
    For m_Occurence := Low(_ListeBouches[m_NoBouche].SurColonnes) To High(_ListeBouches[m_NoBouche].SurColonnes) Do Begin
      _Grid.RowCount := _Grid.RowCount + 1;
      m_Row := _Grid.RowCount - 1;
      m_Bouche := _ListeBouches[m_NoBouche].Bouche;
      If m_Occurence = 0 Then Begin
        _Grid.Objects[0, m_Row] := TCelluleImp.Create(c_NomsPiecesBouche[m_Bouche.TypePiece], GetFontInfos, 0, clBlack, clWhite, [fsBold], taLeftJustify);
        _Grid.Objects[1, m_Row] := TCelluleImp.Create('Bouche', GetFontInfos, 0, clBlack, clWhite, [fsBold], taCenter);
      End Else Begin
        _Grid.Objects[0, m_Row] := TCelluleImp.Create(' ', GetFontInfos, 0, clBlack, clWhite, [fsBold], taLeftJustify);
        _Grid.Objects[1, m_Row] := TCelluleImp.Create(' ', GetFontInfos, 0, clBlack, clWhite, [fsBold], taCenter);
      End;
      If m_Bouche.TypePiece < c_ChoixBouche Then
        Begin
                If (m_Bouche.TypePiece = c_Cuisine) and (m_Bouche.Logement.TypeLogement = c_TypeT3) And (m_Bouche.Logement.Systeme.Hygro) And (m_Bouche.Logement.Systeme.Gaz = false) And (m_Bouche.Logement.Systeme.AlizeIII) And (m_Bouche.Logement.T3Optimise = c_Oui) Then
                        m_Str := m_Bouche.Logement.Systeme.BoucheCuisT3Opt.Libelle
                else
                        m_Str := m_Bouche.Systeme.Bouche[_Logement.TypeLogement,m_Bouche.TypePiece].Libelle;
        End
      Else
        m_Str := m_Bouche.Reference;
      _Grid.Objects[2, m_Row] := TCelluleImp.Create(m_Str, GetFontInfos, 0, clBlack, clWhite, [fsBold], taCenter);
      m_Str := 'Colonne: ' + _ListeBouches[m_NoBouche].SurColonnes[m_Occurence];
      _Grid.Objects[3, m_Row] := TCelluleImp.Create(m_Str, GetFontInfos, 0, clBlack, clWhite, [fsBold], taCenter);
    End;
End;
{ ************************************************************************************************************************************************* }

Procedure Impression_Logement(_Logement : TVentil_Logement; Var _Grid: TStringGrid);
Var
  m_Row          : Integer;
  m_ListeBouches : TListePrintBouche;
  m_NbCuisines   : Integer;
  m_Col          : Integer;
Begin
_Grid.RowCount := _Grid.RowCount + 1;
m_Row := _Grid.RowCount - 1;
  _Grid.Objects[0, m_Row] := TCelluleImp.Create(_Logement.Reference, GetFontInfos, 0, clBlack, $00FF8000, [fsBold], taCenter);
  For m_Col := 1 To _Grid.ColCount - 1 Do
    _Grid.Objects[m_Col, m_Row] := TCelluleImp.Create(' ', GetFontInfos, 0, clBlack, $00FF8000, [fsBold], taCenter);
  // Bouches pointant sur le logement
  m_NbCuisines := RecapBoucheDuLogement(_Logement, m_ListeBouches);
  // Affichage entr�es d'air 
  If _Logement.TypeLogement <> c_TypeAutre Then Begin
    Imprime_EntreesSejourLogement(_Logement, m_NbCuisines, _Grid);
    Imprime_EntreesChambresLogement(_Logement, m_NbCuisines, _Grid);
  End;
  // Affichage bouches
  Imprime_BouchesLogement(_Logement, m_ListeBouches, _Grid);
End;
{ ************************************************************************************************************************************************* }
Procedure Impression_Logements(_ImpressionEnCours : Boolean; _Lanceur: TForm);
Const
  c_TitresColonnes : Array[0..3] Of String = ('Logements - Pi�ces', 'Nature', 'R�f�rence', 'Position - Nombre');
  _Title = 'Impression des logements';
Var
  m_Grid : TStringGrid;
  m_Col  : Integer;
  m_Log  : Integer;
Begin

  ApercuImp_Debut(_Lanceur, _Title, Etude.Batiment.Reference, GetPiedDePage, Not _ImpressionEnCours);

  {$IfDef ANJOS_OPTIMA3D}
  ImpressionHautDePageAnjos;
  {$EndIf}

  m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 4; m_Grid.RowCount := 1;
  For m_Col := Low(c_TitresColonnes) To High(c_TitresColonnes) Do
    m_Grid.Objects[m_Col, 0] := TCelluleImp.Create(c_TitresColonnes[m_Col], GetFontInfos, 100, clBlack, ClSkyBlue, GetFontInfos.Style, tacenter);
  For m_Log := 0 To Etude.Batiment.Logements.Count - 1 Do Impression_Logement(Etude.Batiment.Logements[m_Log], m_Grid);
  ApercuImp_AjouteGrille(m_Grid);

  {$IfDef ANJOS_OPTIMA3D}
  ImpressionBasDePageAnjos;
  {$EndIf}


  ApercuImp_Show;
End;

{ ************************************************************************************************************************************************* }


{ ************************************************************************************************************************************************* }
                                              { Pertes de charge entre le caisson et les bouches }
{ ************************************************************************************************************************************************* }

Procedure ImprimeTroncon_PdcCaissonBouche(_Troncon: TVentil_Troncon; Var _Grid: TStringGrid; _AvalMini, _AvalMaxi: Real);
Var
  m_Row    : Integer;
  m_Col    : Integer;
  m_NoFils : Integer;
  m_Mini   : Real;
  m_Maxi   : Real;
  m_Sys    : TVentil_Systeme;
  m_Lgt    : TVentil_Logement;
  m_Appart : Integer;
  m_Val    : String;
Begin
  m_Mini := _Troncon.PdcTotaleTotaleMini - _Troncon.PdcTotalAvalMini;
  m_Maxi := _Troncon.PdcTotaleTotaleMaxi - _Troncon.PdcTotalAvalMaxi;
  If _Troncon.InheritsFrom(TVentil_TeSouche) Or _Troncon.InheritsFrom(TVentil_Collecteur) Or _Troncon.InheritsFrom(TVentil_Bouche) Then Begin
    _Grid.RowCount := _Grid.RowCount + 1; m_Row := _Grid.RowCount - 1;
    _Grid.Objects[0, m_Row] := TCelluleImp.Create(_Troncon.Reference, GetFontInfos, 0, clBlack, CouleurElementTerminal(_Troncon), [fsBold], taCenter);
    _Grid.Objects[1, m_Row] := TCelluleImp.Create(TypeElementTerminal(_Troncon), GetFontInfos, 0, clBlack, CouleurElementTerminal(_Troncon), [], taCenter);
    For m_Col := 2 To _Grid.ColCount - 1 Do _Grid.Objects[m_Col, m_Row] := TCelluleImp.Create(' ', GetFontInfos, 0, clBlack, CouleurElementTerminal(_Troncon), [], taCenter);

    if _Troncon.InheritsFrom(TVentil_Collecteur) Then Begin
      m_Mini := _AvalMini;
      m_Maxi := _AvalMaxi;
    End Else If _Troncon.InheritsFrom(TVentil_TeSouche) Then Begin
//      m_Mini := _AvalMini;
//      m_Maxi := _AvalMaxi;
    End Else If _Troncon.InheritsFrom(TVentil_Bouche) Then Begin
      m_Mini := _AvalMini;
      m_Maxi := _AvalMaxi;
      { c'est sur les bouches que �a se passe }
      m_Sys := TVentil_Bouche(_Troncon).Systeme;
      If IsNotNil(m_Sys) Then Begin
        m_Lgt := TVentil_Bouche(_Troncon).Logement;
        m_Appart := Min(m_Lgt.TypeLogement, c_TypeT7);
        For m_Col := 2 To 6 Do Begin
          Case m_Col Of
            2: If TVentil_Bouche(_Troncon).TypePiece < c_ChoixBouche Then
                        Begin
                                If (TVentil_Bouche(_Troncon).TypePiece = c_Cuisine) and (TVentil_Bouche(_Troncon).Logement.TypeLogement = c_TypeT3) And (TVentil_Bouche(_Troncon).Logement.Systeme.Hygro) And (TVentil_Bouche(_Troncon).Logement.Systeme.Gaz = false) And (TVentil_Bouche(_Troncon).Logement.Systeme.AlizeIII) And (TVentil_Bouche(_Troncon).Logement.T3Optimise = c_Oui) Then
                                        m_Val := TVentil_Bouche(_Troncon).Logement.Systeme.BoucheCuisT3Opt.Libelle
                                else
                                        m_Val := m_Sys.Bouche[m_Appart, TVentil_Bouche(_Troncon).TypePiece].Libelle;
                        End
               else
                        m_Val := ' ';
            3: m_Val := IntToStr(TVentil_Bouche(_Troncon).DebitMini);
            4: m_Val := IntToStr(TVentil_Bouche(_Troncon).DebitMaxi);
            5: m_Val := Float2Str(TVentil_Bouche(_Troncon).PdcTotaleTotaleMini + _AvalMini, 1);
            6: m_Val := Float2Str(TVentil_Bouche(_Troncon).PdcTotaleTotaleMaxi + _AvalMaxi, 1);
            Else m_Val := ' ';
          End;
          _Grid.Objects[m_Col, m_Row] := TCelluleImp.Create(m_Val, GetFontInfos, 0, clBlack, CouleurElementTerminal(_Troncon), [], taCenter);
        End;
      End;

    End
  End;

  If _Troncon.FilsCalcul.Count > 0 Then For m_NoFils := 0 To _Troncon.FilsCalcul.Count - 1 Do
    ImprimeTroncon_PdcCaissonBouche(_Troncon.FilsCalcul[m_NoFils], _Grid,  m_Mini, m_Maxi);
End;
{ ************************************************************************************************************************************************* }
Procedure Impression_PdcCaissonBouches(_ImpressionEnCours : Boolean; _Lanceur: TForm);
Const
{$IfDef ANJOS_OPTIMA3D}
  c_TitresColonnes : Array[0..6] Of String = ('Tron�on', 'Elt Terminal', 'R�f�rence', 'D�b. min (m�/h) avec fuite', 'D�b. max (m�/h) avec fuite',
                                              'Pdc min (Pa)', 'Pdc max (Pa)');
{$Else}
  c_TitresColonnes : Array[0..6] Of String = ('Tron�on', 'Elt Terminal', 'R�f�rence', 'D�b. min (m�/h)', 'D�b. max (m�/h)',
                                              'Pdc min (Pa)', 'Pdc max (Pa)');
{$EndIf}
  _Title = 'Pertes de charge entre le caisson et les bouches';
Var
  m_Grid : TStringGrid;
  m_Col  : Integer;
  m_NoRes: Integer;
Begin
  ApercuImp_Debut(_Lanceur, _Title, Etude.Batiment.Reference, GetPiedDePage, Not _ImpressionEnCours);

  {$IfDef ANJOS_OPTIMA3D}
  ImpressionHautDePageAnjos;
  {$EndIf}  

 For m_NoRes := 0 To Etude.Batiment.Reseaux.Count - 1 Do Begin
    m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 7; m_Grid.RowCount := 1;
    For m_Col := Low(c_TitresColonnes) To High(c_TitresColonnes) Do
      m_Grid.Objects[m_Col, 0] := TCelluleImp.Create(c_TitresColonnes[m_Col], GetFontInfos, 100, clBlack, ClSkyBlue, GetFontInfos.Style, tacenter);
    ImprimeTroncon_PdcCaissonBouche(Etude.Batiment.Reseaux[m_NoRes], m_Grid, 0, 0);
    ApercuImp_AjouteGrille(m_Grid, True, 'R�seau: ' + Etude.Batiment.Reseaux[m_NoRes].Reference);
    If m_NoRes < Etude.Batiment.Reseaux.Count - 1 Then ApercuImp_SauteLigne(2);
  End;

  {$IfDef ANJOS_OPTIMA3D}
  ImpressionBasDePageAnjos;
  {$EndIf}  

  ApercuImp_Show;

End;
{ ************************************************************************************************************************************************* }

{ ************************************************************************************************************************************************* }
                                              { Pressions limites pour bon fontionnement des bouches }
{ ************************************************************************************************************************************************* }

Function CouleurElementTerminal(_Troncon: TVentil_Troncon): TColor;
Begin
  If _Troncon.InheritsFrom(TVentil_TeSouche)    Then Result := $00FF8000   Else
  If _Troncon.InheritsFrom(TVentil_Collecteur)  Then Result := $00E1E1E1 Else
  Result := clWhite;
End;
{ ************************************************************************************************************************************************* }
Procedure ImprimeTroncon_PressionsLimites(_Troncon: TVentil_Troncon; Var _Grid: TStringGrid; _AvalMini, _AvalMaxi: Real);
Var
  m_Row    : Integer;
  m_Col    : Integer;
  m_NoFils : Integer;
  m_Mini   : Real;
  m_Maxi   : Real;
  m_Sys    : TVentil_Systeme;
  m_Lgt    : TVentil_Logement;
  m_Appart : Integer;
  m_Val    : String;
Begin
  m_Mini := _Troncon.PdcTotaleTotaleMini - _Troncon.PdcTotalAvalMini;
  m_Maxi := _Troncon.PdcTotaleTotaleMaxi - _Troncon.PdcTotalAvalMaxi;
  If _Troncon.InheritsFrom(TVentil_TeSouche) Or _Troncon.InheritsFrom(TVentil_Collecteur) Or _Troncon.InheritsFrom(TVentil_Bouche) Then Begin
    _Grid.RowCount := _Grid.RowCount + 1; m_Row := _Grid.RowCount - 1;
    _Grid.Objects[0, m_Row] := TCelluleImp.Create(_Troncon.Reference, GetFontInfos, 0, clBlack, CouleurElementTerminal(_Troncon), [fsBold], taCenter);
    _Grid.Objects[1, m_Row] := TCelluleImp.Create(TypeElementTerminal(_Troncon), GetFontInfos, 0, clBlack, CouleurElementTerminal(_Troncon), [], taCenter);
    For m_Col := 2 To _Grid.ColCount - 1 Do _Grid.Objects[m_Col, m_Row] := TCelluleImp.Create(' ', GetFontInfos, 0, clBlack, CouleurElementTerminal(_Troncon), [], taCenter);

    if _Troncon.InheritsFrom(TVentil_Collecteur) Then Begin
      m_Mini := _AvalMini;
      m_Maxi := _AvalMaxi;
    End Else If _Troncon.InheritsFrom(TVentil_TeSouche) Then Begin
//      m_Mini := _AvalMini;
//      m_Maxi := _AvalMaxi;
    End Else If _Troncon.InheritsFrom(TVentil_Bouche) Then Begin
      m_Mini := _AvalMini;
      m_Maxi := _AvalMaxi;
      { c'est sur les bouches que �a se passe }
      m_Sys := TVentil_Bouche(_Troncon).Systeme;
      If IsNotNil(m_Sys) Then Begin
        m_Lgt := TVentil_Bouche(_Troncon).Logement;
        m_Appart := Min(m_Lgt.TypeLogement, c_TypeT7);
        For m_Col := 2 To 6 Do Begin
          Case m_Col Of
            2: If TVentil_Bouche(_Troncon).TypePiece < c_ChoixBouche Then
                        Begin
                                If (TVentil_Bouche(_Troncon).TypePiece = c_Cuisine) and (TVentil_Bouche(_Troncon).Logement.TypeLogement = c_TypeT3) And (TVentil_Bouche(_Troncon).Logement.Systeme.Hygro) And (TVentil_Bouche(_Troncon).Logement.Systeme.Gaz = false) And (TVentil_Bouche(_Troncon).Logement.Systeme.AlizeIII) And (TVentil_Bouche(_Troncon).Logement.T3Optimise = c_Oui) Then
                                        m_Val := TVentil_Bouche(_Troncon).Logement.Systeme.BoucheCuisT3Opt.Libelle
                                else
                                        m_Val := m_Sys.Bouche[m_Appart, TVentil_Bouche(_Troncon).TypePiece].Libelle;
                        End
               Else
                        m_Val := ' ';
            3: m_Val := IntToStr(TVentil_Bouche(_Troncon).DebitMini);
            4: m_Val := IntToStr(TVentil_Bouche(_Troncon).DebitMaxi);
            5: m_Val := Float2Str(TVentil_Bouche(_Troncon).PdcVentilMini + _AvalMini, 1);
            6: m_Val := Float2Str(TVentil_Bouche(_Troncon).PdcVentilMaxi + _AvalMaxi, 1);
            Else m_Val := ' ';
          End;
          _Grid.Objects[m_Col, m_Row] := TCelluleImp.Create(m_Val, GetFontInfos, 0, clBlack, CouleurElementTerminal(_Troncon), [], taCenter);
        End;
      End;

    End
  End;

  If _Troncon.FilsCalcul.Count > 0 Then For m_NoFils := 0 To _Troncon.FilsCalcul.Count - 1 Do
    ImprimeTroncon_PressionsLimites(_Troncon.FilsCalcul[m_NoFils], _Grid,  m_Mini, m_Maxi);
End;
{ ************************************************************************************************************************************************* }
Procedure Impression_PressionsLimitesPourBouches(_ImpressionEnCours : Boolean; _Lanceur: TForm);
Const
{$IfDef ANJOS_OPTIMA3D}
  c_TitresColonnes : Array[0..6] Of String = ('Tron�on', 'Elt Terminal', 'R�f�rence', 'D�b. min (m�/h) avec fuite', 'D�b. max (m�/h) avec fuite',
                                              'D�b. min Hm (Pa)', 'D�b. max Hm (Pa)');
{$Else}
  c_TitresColonnes : Array[0..6] Of String = ('Tron�on', 'Elt Terminal', 'R�f�rence', 'D�b. min (m�/h)', 'D�b. max (m�/h)',
                                              'D�b. min Hm (Pa)', 'D�b. max Hm (Pa)');
{$EndIf}
  _Title = 'Pressions limites pour un bon fonctionnement aux bouches';
Var
  m_Grid : TStringGrid;
  m_Col  : Integer;
  m_NoRes: Integer;
Begin
  ApercuImp_Debut(_Lanceur, _Title, Etude.Batiment.Reference, GetPiedDePage, Not _ImpressionEnCours);

  {$IfDef ANJOS_OPTIMA3D}
  ImpressionHautDePageAnjos;
  {$EndIf}  

 For m_NoRes := 0 To Etude.Batiment.Reseaux.Count - 1 Do Begin
    m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 7; m_Grid.RowCount := 1;
    For m_Col := Low(c_TitresColonnes) To High(c_TitresColonnes) Do
      m_Grid.Objects[m_Col, 0] := TCelluleImp.Create(c_TitresColonnes[m_Col], GetFontInfos, 100, clBlack, ClSkyBlue, GetFontInfos.Style, tacenter);
    ImprimeTroncon_PressionsLimites(Etude.Batiment.Reseaux[m_NoRes], m_Grid, 0, 0);
    ApercuImp_AjouteGrille(m_Grid, True, 'R�seau: ' + Etude.Batiment.Reseaux[m_NoRes].Reference);
    If m_NoRes < Etude.Batiment.Reseaux.Count - 1 Then ApercuImp_SauteLigne(2);
  End;

  {$IfDef ANJOS_OPTIMA3D}
  ImpressionBasDePageAnjos;
  {$EndIf}

  ApercuImp_Show;
End;
{ ************************************************************************************************************************************************* }


{ ************************************************************************************************************************************************* }
                                                 { Fonctionnement du r�seau � d�bit maxi ou mini }
{ ************************************************************************************************************************************************* }

{ ************************************************************************************************************************************************* }
Function TypeElementTerminal(_Troncon: TVentil_Troncon): String;
Begin
  Result := '';
  If _Troncon.InheritsFrom(TVentil_TeSouche)    Then Result := 'T� souche'   Else
  If _Troncon.InheritsFrom(TVentil_Bifurcation) Then Result := 'Bifurcation' Else
  If _Troncon.InheritsFrom(TVentil_Collecteur)  Then Result := 'Collecteur'  Else
  If _Troncon.InheritsFrom(TVentil_SortieToit)  Then Result := 'Sortie toit' Else
  If _Troncon.InheritsFrom(TVentil_Bouche)      Then Result := 'Bouche';
End;
{ ************************************************************************************************************************************************* }
Procedure ImprimeTronconDebit(_Troncon: TVentil_Troncon; Var _Grid: TStringGrid; _Terrasse: Boolean; _Max: Boolean);
Var
  m_Row : Integer;
  m_Tr  : Integer;
  m_Col : Integer;
Begin
  If Not _Troncon.InheritsFrom(TVentil_Accident) And
    ((_Troncon.InheritsFrom(TVentil_TeSouche)) Or (_Troncon.SurTerrasse And _Terrasse) Or (Not _Troncon.SurTerrasse And Not _Terrasse)) Then Begin
    _Grid.RowCount := _Grid.RowCount + 1; m_Row := _Grid.RowCount - 1;
    _Grid.Objects[0, m_Row] := TCelluleImp.Create(_Troncon.Reference, GetFontInfos, 0, clBlack, clWhite, [], taCenter);

    If Not _Troncon.InheritsFrom(TVentil_Caisson) And Not (Not _Terrasse And _Troncon.InheritsFrom(TVentil_TeSouche))  Then Begin
      _Grid.Objects[1, m_Row] := TCelluleImp.Create(TypeElementTerminal(_Troncon), GetFontInfos, 0, clBlack, clWhite, [], taCenter);
      If _Troncon.TronconGaz Then _Grid.Objects[3, m_Row] := TCelluleImp.Create(IntToStr(_Troncon.Diametre) + 'A' , GetFontInfos, 0, clBlack, clWhite, [], taCenter)
                             Else _Grid.Objects[3, m_Row] := TCelluleImp.Create(IntToStr(_Troncon.Diametre) + 'G' , GetFontInfos, 0, clBlack, clWhite, [], taCenter);
      _Grid.Objects[4, m_Row] := TCelluleImp.Create(Float2Str(_Troncon.LongGaine, 2), GetFontInfos, 0, clBlack, clWhite, [], taCenter);
      _Grid.Objects[5, m_Row] := TCelluleImp.Create(Float2Str(_Troncon.Vitesse, 2), GetFontInfos, 0, clBlack, clWhite, [], taCenter);
      If _Max Then Begin
      _Grid.Objects[2, m_Row] := TCelluleImp.Create(IntToStr(_Troncon.DebitMaxi), GetFontInfos, 0, clBlack, clWhite, [], taCenter);
        _Grid.Objects[6, m_Row] := TCelluleImp.Create(Float2Str(_Troncon.PdcReguliereMaxi, 2), GetFontInfos, 0, clBlack, clWhite, [], taCenter);
        _Grid.Objects[7, m_Row] := TCelluleImp.Create(Float2Str(_Troncon.PdcSinguliereMaxi, 2), GetFontInfos, 0, clBlack, clWhite, [], taCenter);
        _Grid.Objects[8, m_Row] := TCelluleImp.Create(Float2Str(_Troncon.PdcTotaleMaxi, 2), GetFontInfos, 0, clBlack, clWhite, [], taCenter);
        _Grid.Objects[9, m_Row] := TCelluleImp.Create(Float2Str(_Troncon.PdcTotaleTotaleMaxi, 2), GetFontInfos, 0, clBlack, clWhite, [], taCenter);
      End Else Begin
        _Grid.Objects[2, m_Row] := TCelluleImp.Create(IntToStr(_Troncon.DebitMini), GetFontInfos, 0, clBlack, clWhite, [], taCenter);
        _Grid.Objects[6, m_Row] := TCelluleImp.Create(Float2Str(_Troncon.PdcReguliereMini, 2), GetFontInfos, 0, clBlack, clWhite, [], taCenter);
        _Grid.Objects[7, m_Row] := TCelluleImp.Create(Float2Str(_Troncon.PdcSinguliereMini, 2), GetFontInfos, 0, clBlack, clWhite, [], taCenter);
        _Grid.Objects[8, m_Row] := TCelluleImp.Create(Float2Str(_Troncon.PdcTotaleMini, 2), GetFontInfos, 0, clBlack, clWhite, [], taCenter);
        _Grid.Objects[9, m_Row] := TCelluleImp.Create(Float2Str(_Troncon.PdcTotaleTotaleMini, 2), GetFontInfos, 0, clBlack, clWhite, [], taCenter);
      End;
      // _Grid.Objects[10, m_Row] := TCelluleImp.Create(Float2Str(_Troncon.TotalDzeta, 2), GetFontInfos, 0, clBlack, clWhite, [], taCenter);
    End;
    If Not _Terrasse And _Troncon.InheritsFrom(TVentil_TeSouche) Then Begin
      TCelluleImp(_Grid.Objects[0, m_Row]).IBrushColor := $00FF8000;
      For m_Col := 1 To _Grid.ColCount - 1 Do _Grid.Objects[m_Col, m_Row] := TCelluleImp.Create(' ', GetFontInfos, 0, clBlack, $00FF8000, [], taCenter); 
    End;
  End;

  For m_Tr := 0 To _Troncon.FilsCalcul.Count - 1 Do
    If Not (_Terrasse And _Troncon.InheritsFrom(TVentil_TeSouche)) Then  ImprimeTronconDebit(_Troncon.FilsCalcul[m_Tr], _Grid, _Terrasse, _Max);
End;
{ ************************************************************************************************************************************************* }
Procedure Impression_FonctionnnementSelonDebit(_ImpressionEnCours : Boolean; _ADebitMaxi: Boolean; _Lanceur: TForm);
Const
{$IfDef ANJOS_OPTIMA3D}
  c_TitresColonnes : Array[0..9] Of String = ('Tron�on', 'Elt Terminal', 'D�b. (m�/h) avec fuite', 'Diam. (mm)', 'Long. (m)', 'Vitesse (m/s)', 'PdCReg (Pa)',
                                               'PdcSin (Pa)', 'Pdc Tron�on (Pa)', 'Pdc Cumul�e (Pa)');
{$Else}
  c_TitresColonnes : Array[0..9] Of String = ('Tron�on', 'Elt Terminal', 'D�b. (m�/h)', 'Diam. (mm)', 'Long. (m)', 'Vitesse (m/s)', 'PdCReg (Pa)',
                                               'PdcSin (Pa)', 'Pdc Tron�on (Pa)', 'Pdc Cumul�e (Pa)');
{$EndIf}
Var
  m_Grid : TStringGrid;
  m_Col  : Integer;
  m_NoRes: Integer;
   _Title : String;
Begin

  If _ADebitMaxi Then _Title := 'Caract�ristiques du r�seau � d�bit maxi'
                 Else _Title := 'Caract�ristiques du r�seau � d�bit mini';

   ApercuImp_Debut(_Lanceur, _Title, Etude.Batiment.Reference, GetPiedDePage, Not _ImpressionEnCours);

  {$IfDef ANJOS_OPTIMA3D}
  ImpressionHautDePageAnjos;
  {$EndIf}                 

 For m_NoRes := 0 To Etude.Batiment.Reseaux.Count - 1 Do Begin
   // R�seau terrasse
    m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 10; m_Grid.RowCount := 1;
    For m_Col := Low(c_TitresColonnes) To High(c_TitresColonnes) Do
      m_Grid.Objects[m_Col, 0] := TCelluleImp.Create(c_TitresColonnes[m_Col], GetFontInfos, 100, clBlack, ClSkyBlue, GetFontInfos.Style, tacenter);
    ImprimeTronconDebit(Etude.Batiment.Reseaux[m_NoRes], m_Grid, True, _ADebitMaxi);
    ApercuImp_AjouteGrille(m_Grid, True, 'R�seau terrasse: ' + Etude.Batiment.Reseaux[m_NoRes].Reference);
   // Colonnes
    m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 10; m_Grid.RowCount := 1;
    For m_Col := Low(c_TitresColonnes) To High(c_TitresColonnes) Do
      m_Grid.Objects[m_Col, 0] := TCelluleImp.Create(c_TitresColonnes[m_Col], GetFontInfos, 100, clBlack, ClSkyBlue, GetFontInfos.Style, tacenter);
    ImprimeTronconDebit(Etude.Batiment.Reseaux[m_NoRes], m_Grid, False, _ADebitMaxi);
    ApercuImp_AjouteGrille(m_Grid, True, 'Colonnes: '  + Etude.Batiment.Reseaux[m_NoRes].Reference);
    If m_NoRes < Etude.Batiment.Reseaux.Count - 1 Then ApercuImp_SauteLigne(2);
  End;


  {$IfDef ANJOS_OPTIMA3D}
  ImpressionBasDePageAnjos;
  {$EndIf}


  ApercuImp_Show;

End;
{ ************************************************************************************************************************************************* }


{ ************************************************************************************************************************************************* }
                                                 { Sch�mas des colonnes }
{ ************************************************************************************************************************************************* }

{ ************************************************************************************************************************************************* }
Procedure SetDataImp_Schemas;
Var
  m_NoImg : Integer;
  m_NoRes : Integer;
  m_List  : TList;
  MyJPeg  : TJPEGImage;
Begin
{
//exit;
  m_List := Aero_Vilo2Fredo_PrintReseauTerrassToJPGs;
  If m_List <> nil Then For m_NoRes := m_List.Count - 1 DownTo 0 Do Begin
    TJPEGImage(m_List[m_NoRes]).Destroy;
  End;
  FreeAndNil(m_List);
exit;
}
  { Colonnes }
  { Destruction �ventuelle des images d�j� pr�sentes }

  If Etude.DataImp_SchemasColonnes <> Nil Then Begin
    For m_NoImg := Etude.DataImp_SchemasColonnes.Count - 1 DownTo 0 Do
        Begin
                if Etude.DataImp_SchemasColonnes[m_NoImg] <> nil then
                        Begin
                        TJpegImage(Etude.DataImp_SchemasColonnes[m_NoImg]).Destroy;
                        Etude.DataImp_SchemasColonnes[m_NoImg] := nil;
                        End;
        End;
    FreeAndNil(Etude.DataImp_SchemasColonnes);
  End;
  Etude.DataImp_SchemasColonnes := TList.Create;
  For m_NoImg := 0 To Etude.Batiment.Composants.Count - 1 Do If (Etude.Batiment.Composants[m_NoImg].InheritsFrom(TVentil_TeSouche)) And (Etude.Batiment.Composants[m_NoImg].LienCad <> nil) Then Begin
    m_List := Aero_Vilo2Fredo_PrintColonneToJPGs(Etude.Batiment.Composants[m_NoImg].LienCad As IAero_TSoucheToiture);
    If m_List <> nil Then For m_NoRes := m_List.Count - 1 DownTo 0 Do Begin

      MyJPeg := TJPEGImage(m_List[m_NoRes]);
      MyJPeg.SaveToFile(TempInternalDir + 'col.jpg');
      FreeAndNil(MyJPeg);
      MyJPeg := TJPEGImage.Create;
      MyJPeg.LoadFromFile(TempInternalDir + 'col.jpg');
      Ventil_DeleteFile(TempInternalDir + 'col.jpg');
      Etude.DataImp_SchemasColonnes.Add(MyJPeg);

//      Etude.DataImp_SchemasColonnes.Add(TJPEGImage(m_List[m_NoRes]));
      m_List[m_NoRes] := nil;
      m_List.Remove(TJPEGImage(m_List[m_NoRes]));
    End;
    FreeAndNil(m_List);
  End;


  { Etages }
  { Destruction �ventuelle des images d�j� pr�sentes }

  If Etude.DataImp_SchemasEtages <> Nil Then Begin
    For m_NoImg := Etude.DataImp_SchemasEtages.Count - 1 DownTo 0 Do
        Begin
                if Etude.DataImp_SchemasEtages[m_NoImg] <> nil then
                        begin
                        TJpegImage(Etude.DataImp_SchemasEtages[m_NoImg]).Destroy;
                        Etude.DataImp_SchemasEtages[m_NoImg] := nil;
                        end;
        End;
    FreeAndNil(Etude.DataImp_SchemasEtages);
  End;
  Etude.DataImp_SchemasEtages := TList.Create;
  //m_List := Aero_Vilo2Fredo_PrintEtageToJPGs(Aero_Vilo2Fredo_GetEtageMax);
  m_List := Aero_Vilo2Fredo_PrintReseauTerrasseToJPGs;
  If m_List <> nil Then For m_NoRes := m_List.Count - 1 DownTo 0 Do Begin

      MyJPeg := TJPEGImage(m_List[m_NoRes]);
      MyJPeg.SaveToFile(TempInternalDir + 'ter.jpg');
      FreeAndNil(MyJPeg);
      MyJPeg := TJPEGImage.Create;
      MyJPeg.LoadFromFile(TempInternalDir + 'ter.jpg');
      Ventil_DeleteFile(TempInternalDir + 'ter.jpg');
      Etude.DataImp_SchemasEtages.Add(MyJPeg);

//    Etude.DataImp_SchemasEtages.Add(TJPEGImage(m_List[m_NoRes]));
    m_List[m_NoRes] := nil;
    m_List.Remove(TJPEGImage(m_List[m_NoRes]));
  End;
  FreeAndNil(m_List);

End;
{ ************************************************************************************************************************************************* }
Procedure Impression_SchemasColonnes(_ImpressionEnCours : Boolean; _Lanceur: TForm);
        function CompareNames(Item1, Item2: Pointer): Integer;
        begin
          Result := CompareText(TVentil_TeSouche(Item1).Reference, TVentil_TeSouche(Item2).Reference);
        end;

Var
  m_No   : Integer;
  m_New  : Boolean;
  m_Rect : TGmrect;
  ListColonnes : TList;
  MyJPeg     : TJPEGImage;
  MyListJPeg : TList;
  {$IfDef ANJOS_OPTIMA3D}
  m_Grid : TStringGrid;
  {$EndIf}
  YBottom : Extended;
  Stl     : TBrushStyle;
  PenColor : TColor;
  _Title : String;

  nbdecoup : Integer;
  i : integer;
  myscale : Extended;

  j : integer;
  CompensationTexteLibre : Integer;
Begin
  Etude.NouvellePageImpInutile := True;
  m_New := Not _ImpressionEnCours;



  ListColonnes := TList.Create;

  For m_No := 0 To Etude.Batiment.Composants.Count - 1 Do If (Etude.Batiment.Composants[m_No].InheritsFrom(TVentil_TeSouche)) And (Etude.Batiment.Composants[m_No].LienCad <> nil) Then
        Begin
        ListColonnes.Add(Etude.Batiment.Composants[m_No]);
        End;

        If ListColonnes.Count = 0 then exit;

  //tri par r�f�rence
  ListColonnes.Sort(@CompareNames);

FBeginProgress(Aero_Vilo2Fredo_GetNombreColonnes, 'Sch�mas des colonnes');
//  {$EndIf}

  For m_No := 0 To ListColonnes.Count - 1 do
  Begin
    MyListJPeg := TList.Create;
    with Aero_Vilo2Fredo_PrintColonneToJPGs(TVentil_TeSouche(ListColonnes[m_No]).LienCad As IAero_TSoucheToiture) do begin
    nbdecoup := Count;
        for i := 0 to nbdecoup - 1 do
                MyListJPeg.Add(TJPEGImage(Items[i]));
      Destroy;
    end;

  for i := 0 to nbdecoup - 1 do
  begin

          if Not(m_New) and ((m_No > 0) or (i >0)) then

  _Title := 'Sch�mas des colonnes: ' + Etude.Batiment.Reference + ' - ' + TVentil_TeSouche(ListColonnes[m_No]).Reference;
  ApercuImp_Debut(_Lanceur, _Title, Etude.Batiment.Reference, GetPiedDePage, m_New);//, nbdecoup = 1);
  CompensationTexteLibre := 0;
  {$IfDef ANJOS_OPTIMA3D}
  ImpressionHautDePageAnjos(0);
        if TVentil_TeSouche(ListColonnes[m_No]).TexteLibre.Count > 0 then
                Begin
                m_Grid := TStringGrid.Create(nil);
                CompensationTexteLibre := (m_Grid.DefaultRowHeight + 3);
                End;
  {$EndIf}

    m_Rect.Left   := Dlg_ApercuImpressions.Apercu.Margins.Left.AsMillimeters;
    m_Rect.Top    := Dlg_ApercuImpressions.CurrentYPosOnPage + 5;
    m_Rect.Right  := m_Rect.Left + Dlg_ApercuImpressions.Apercu.PageWidth[gmMillimeters] -
                     Dlg_ApercuImpressions.Apercu.Margins.Right.AsMillimeters - Dlg_ApercuImpressions.Apercu.Margins.Left.AsMillimeters;
    m_Rect.Bottom := Dlg_ApercuImpressions.Apercu.PageHeight[gmMillimeters] -
                     Dlg_ApercuImpressions.Apercu.Footer.Height[GmMillimeters] -
                     Dlg_ApercuImpressions.Apercu.Margins.Bottom.AsMillimeters{$IfDef ANJOS_OPTIMA3D} - (CompensationTexteLibre *TVentil_TeSouche(ListColonnes[m_No]).TexteLibre.Count){$EndIf};

    YBottom       := m_Rect.Bottom;
    m_Rect.Left   := ConvertValue(m_Rect.Left, GmMillimeters, GmPixels);
    m_Rect.Right  := ConvertValue(m_Rect.Right, GmMillimeters, GmPixels);
    m_Rect.Bottom := ConvertValue(m_Rect.Bottom, GmMillimeters, GmPixels);
    m_Rect.Top    := ConvertValue(m_Rect.Top, GmMillimeters, GmPixels);

      MyJPeg := TJPEGImage(MyListJPeg[i]);

    MyJPeg.SaveToFile(TempInternalDir + 'col.jpg');
//    MyJPeg.SaveToFile('c:/toto/col.jpg');
    FreeAndNil(MyJPeg);
    MyJPeg := TJPEGImage.Create;
    MyJPeg.LoadFromFile(TempInternalDir + 'col.jpg');
//    MyJPeg.LoadFromFile('c:/toto/col.jpg');
    Ventil_DeleteFile(TempInternalDir + 'col.jpg');
//    Ventil_DeleteFile('c:/toto/col.jpg');

myscale := (abs(m_Rect.Right - m_Rect.Left)) / MyJPeg.Width;
myscale := Min(myscale,  (abs(m_Rect.Top - m_Rect.Bottom) / nbdecoup) / MyJPeg.Height);

// Dlg_ApercuImpressions.Apercu.Canvas.StretchDraw(m_Rect.Left, m_Rect.Top, m_Rect.Right, m_Rect.Bottom, MyJPeg, GmPixels);

 Dlg_ApercuImpressions.Apercu.Canvas.Draw(m_Rect.Left, m_Rect.Top, MyJPeg, myscale, GmPixels);

  FreeAndNil(MyJPeg);


  PenColor := Dlg_ApercuImpressions.Apercu.Canvas.Pen.Color;
  Stl := Dlg_ApercuImpressions.Apercu.Canvas.Brush.Style;
  Dlg_ApercuImpressions.Apercu.Canvas.Brush.Style := bsClear;
  Dlg_ApercuImpressions.Apercu.Canvas.Pen.Color := clBlack;
  Dlg_ApercuImpressions.Apercu.Canvas.Rectangle(m_Rect.Left, m_Rect.Top, m_Rect.Right, m_Rect.Bottom, GmPixels);
  Dlg_ApercuImpressions.Apercu.Canvas.Brush.Style := Stl;
  Dlg_ApercuImpressions.Apercu.Canvas.Pen.Color := PenColor;

  {$IfDef PRINTRTF}
    ApercuImp_AjouteBitmapRtfForCurrentPage(false, 5);
  {$EndIf}

  Dlg_ApercuImpressions.CurrentYPosOnPage := YBottom;

  {$IfDef ANJOS_OPTIMA3D}

  if TVentil_TeSouche(ListColonnes[m_No]).TexteLibre.Count > 0 then
        Begin
//        ApercuImp_SauteLigne(1);
        m_Grid.ColCount := 1; m_Grid.RowCount := TVentil_TeSouche(ListColonnes[m_No]).TexteLibre.Count;
                        for j := 0 to TVentil_TeSouche(ListColonnes[m_No]).TexteLibre.Count - 1 do
                                m_Grid.Objects[0, j] := TCelluleImp.Create(TVentil_TeSouche(ListColonnes[m_No]).TexteLibre[j], GetFontInfos, 0, ClRed, clWhite, [fsItalic], taLeftJustify);

        ApercuImp_AjouteGrille(m_Grid, False, '', True, False, False, False);
        End;

  ImpressionBasDePageAnjos(0);

  {$EndIf}

  FStepProgress;

    m_New := False;

  end;
  FreeAndNil(MyListJPeg);
  End;

  FreeAndNil(ListColonnes);
  FEndProgress;

  If Not _ImpressionEnCours Then ApercuImp_Show;


End;
{ ************************************************************************************************************************************************* }

Procedure Impression_SchemasRecapColonnes(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Const
  _Title = 'R�capitulatif des colonnes';
Var
  m_New  : Boolean;
  m_Rect : TGmrect;
  YBottom : Extended;
Begin

  Etude.NouvellePageImpInutile := True;
  m_New := Not _ImpressionEnCours;

  ApercuImp_Debut(_Lanceur, _Title, Etude.Batiment.Reference, GetPiedDePage, m_New, false);
  {$IfDef ANJOS_OPTIMA3D}
  ImpressionHautDePageAnjos(0);
  {$EndIf}
  
//  FBeginProgress(Aero_Vilo2Fredo_GetEtageMax - Aero_Vilo2Fredo_GetEtageMin + 1, 'Sch�mas des �tages');

  m_Rect.Left   := Dlg_ApercuImpressions.Apercu.Margins.Left.AsMillimeters;
  m_Rect.Top    := Dlg_ApercuImpressions.CurrentYPosOnPage + 5;
  m_Rect.Right  := m_Rect.Left + Dlg_ApercuImpressions.Apercu.PageWidth[gmMillimeters] -
                   Dlg_ApercuImpressions.Apercu.Margins.Right.AsMillimeters - Dlg_ApercuImpressions.Apercu.Margins.Left.AsMillimeters;
  m_Rect.Bottom := Dlg_ApercuImpressions.Apercu.PageHeight[gmMillimeters] -
                   Dlg_ApercuImpressions.Apercu.Footer.Height[GmMillimeters] -
                   Dlg_ApercuImpressions.Apercu.Margins.Bottom.AsMillimeters - Dlg_ApercuImpressions.Apercu.Header.Height[GmMillimeters]{$IfDef ANJOS_OPTIMA3D} - 15{$EndIf};

  YBottom       := m_Rect.Bottom;
  m_Rect.Left   := ConvertValue(m_Rect.Left, GmMillimeters, GmPixels);
  m_Rect.Right  := ConvertValue(m_Rect.Right, GmMillimeters, GmPixels);
  m_Rect.Bottom := ConvertValue(m_Rect.Bottom, GmMillimeters, GmPixels);
  m_Rect.Top    := ConvertValue(m_Rect.Top, GmMillimeters, GmPixels);

  Aero_Vilo2Fredo_PrintColonnes(m_Rect);
  {$IfDef PRINTRTF}
  ApercuImp_AjouteBitmapRtfForCurrentPage(false, 5);
  {$EndIf}
//  FEndProgress;

  Dlg_ApercuImpressions.CurrentYPosOnPage := YBottom;

  {$IfDef ANJOS_OPTIMA3D}
  ImpressionBasDePageAnjos(0);
  {$EndIf}

  If Not _ImpressionEnCours Then ApercuImp_Show;
End;

Procedure FinalizeImpression;
begin
end;
{ ************************************************************************************************************************************************* }
                                                 { Sch�mas des colonnes }
{ ************************************************************************************************************************************************* }
Procedure Impression_SchemasEtages(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Const
  _Title = 'Sch�mas horizontal';
Var
  m_New  : Boolean;
  m_Rect : TGmrect;
  YBottom : Extended;
  {$IfDef ANJOS_OPTIMA3D}
  m_Grid : TStringGrid;
  {$EndIf}
  ListeTerrasse : TList;
  m_NoRes : Integer;
  MyJPeg : TJPEGImage;
  MyScale : Extended;
  DecalGauche : Integer;

  i : integer;
  CompensationTexteLibre : Integer;
Begin


if Etude.Batiment.Reseaux.GetCaisson = nil then exit;

ListeTerrasse := Aero_Vilo2Fredo_PrintReseauTerrasseToJPGs;

If ListeTerrasse = nil then exit;

//  Etude.NouvellePageImpInutile := True;

  m_New := Not _ImpressionEnCours;

For m_NoRes := 0 To ListeTerrasse.Count - 1 Do Begin
MyJPeg := TJPEGImage(ListeTerrasse[m_NoRes]);

    MyJPeg.SaveToFile(TempInternalDir + 'ter.jpg');
    FreeAndNil(MyJPeg);
    MyJPeg := TJPEGImage.Create;
    MyJPeg.LoadFromFile(TempInternalDir + 'ter.jpg');
    Ventil_DeleteFile(TempInternalDir + 'ter.jpg');


          if Not(m_New) and (m_NoRes > 0) then
                Dlg_ApercuImpressions.Apercu.NewPage;

  ApercuImp_Debut(_Lanceur, _Title, Etude.Batiment.Reference, GetPiedDePage, m_New{$IfDef ANJOS_OPTIMA3D}, TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).OrientationImpression = c_OrientationImpressionPortrait {$EndIf});
  m_New := False;
  CompensationTexteLibre := 0;
  {$IfDef ANJOS_OPTIMA3D}
  ImpressionHautDePageAnjos(0);
        if TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).TexteLibre.Count > 0 then
                Begin
                m_Grid := TStringGrid.Create(nil);
                CompensationTexteLibre := (m_Grid.DefaultRowHeight + 3);
                End;
  {$EndIf}
    
//  FBeginProgress(Aero_Vilo2Fredo_GetEtageMax - Aero_Vilo2Fredo_GetEtageMin + 1, 'Sch�mas des �tages');
  m_Rect.Left   := Dlg_ApercuImpressions.Apercu.Margins.Left.AsMillimeters;
  m_Rect.Top    := Dlg_ApercuImpressions.CurrentYPosOnPage + 5;
  m_Rect.Right  := m_Rect.Left + Dlg_ApercuImpressions.Apercu.PageWidth[gmMillimeters] -
                   Dlg_ApercuImpressions.Apercu.Margins.Right.AsMillimeters - Dlg_ApercuImpressions.Apercu.Margins.Left.AsMillimeters;
  m_Rect.Bottom := Dlg_ApercuImpressions.Apercu.PageHeight[gmMillimeters] -
                   Dlg_ApercuImpressions.Apercu.Footer.Height[GmMillimeters] -
                   Dlg_ApercuImpressions.Apercu.Margins.Bottom.AsMillimeters {$IfDef ANJOS_OPTIMA3D} - (CompensationTexteLibre * TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).TexteLibre.Count){$EndIf};

  YBottom       := m_Rect.Bottom;
  m_Rect.Left   := ConvertValue(m_Rect.Left, GmMillimeters, GmPixels);
  m_Rect.Right  := ConvertValue(m_Rect.Right, GmMillimeters, GmPixels);
  m_Rect.Bottom := ConvertValue(m_Rect.Bottom, GmMillimeters, GmPixels);
  m_Rect.Top    := ConvertValue(m_Rect.Top, GmMillimeters, GmPixels);


  MyScale := Abs(m_Rect.Top - m_Rect.Bottom) / MyJPeg.Height;
          if Abs(m_Rect.Left - m_Rect.Right) < MyScale * MyJPeg.Width then
                    MyScale := Abs(m_Rect.Left - m_Rect.Right) / MyJPeg.Width;


DecalGauche := Round( (m_Rect.Left + m_Rect.Right - MyScale * MyJPeg.Width) / 2);
 Dlg_ApercuImpressions.Apercu.Canvas.Draw(DecalGauche, m_Rect.Top, MyJPeg, MyScale, GmPixels);
  FreeAndNil(MyJPeg);
  {$IfDef PRINTRTF}
  ApercuImp_AjouteBitmapRtfForCurrentPage(false, 5);
  {$EndIf}
//  FEndProgress;

  Dlg_ApercuImpressions.CurrentYPosOnPage := YBottom;
  {$IfDef ANJOS_OPTIMA3D}

        if TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).TexteLibre.Count > 0 then
                Begin
//                ApercuImp_SauteLigne(1);
                m_Grid.ColCount := 1; m_Grid.RowCount := TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).TexteLibre.Count;
                        for i := 0 to TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).TexteLibre.Count - 1 do
                                m_Grid.Objects[0, i] := TCelluleImp.Create(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).TexteLibre[i], GetFontInfos, 0, ClRed, clWhite, [fsItalic], taLeftJustify);
                ApercuImp_AjouteGrille(m_Grid, False, '', True, False, False, False);
                End;

  ImpressionBasDePageAnjos(0);
  {$EndIf}
end;

  FreeAndNil(ListeTerrasse);

  If Not _ImpressionEnCours Then ApercuImp_Show;
End;

{ ************************************************************************************************************************************************* }
                                                 { Quantitatif s�lectionn� }
{ ************************************************************************************************************************************************* }

Procedure Impression_Quantitatif(_Chiffrage: TChiffrage; _ImpressionEnCours: Boolean; _Lanceur: TForm; _IsChiffrage : boolean = true);
Const
        _Title = 'Mat�riel s�lectionn�';
Var
  m_Grid : TStringGrid;
  m_NoCol: Integer;
  TempLargeur : Integer;
Begin
  ApercuImp_Debut(_Lanceur, _Title, Etude.Batiment.Reference, GetPiedDePage, Not _ImpressionEnCours);
  m_Grid := TStringGrid.Create(nil);
  m_Grid.ColCount := GetColCountPrintChiffrage;
  m_Grid.RowCount := 2;

  For m_NoCol := Low(Chiffrage_ColHeaders) To High(Chiffrage_ColHeaders) Do
        if Chiffrage_ColPrint[m_NoCol] then
                Begin
                        if m_NoCol = Chiffrage_ColDesignation then
                                TempLargeur := 50
                        else
                                TempLargeur := round(50 / (m_Grid.ColCount - 1));
                m_Grid.Objects[GetRealColPrintChiffrage(m_NoCol), 0] := TCelluleImp.Create(Chiffrage_ColHeaders[m_NoCol], GetFontInfos, TempLargeur, ClBlack, $00E1E1E1, [fsBold], taCenter);
                End;
  _Chiffrage.Imprime(m_Grid, _IsChiffrage);
  ApercuImp_AjouteGrille(m_Grid, True, '');
  ApercuImp_Show;
End;
{ ************************************************************************************************************************************************* }
Procedure SetDataImp_DonneesGenerale;
Var
  m_Txt     : String;
  m_Txt1    : String;
  m_Cat     : String;
  m_Caisson : TEdibatec_Caisson;
  m_Ventil  : TVentil_Caisson;
Begin
  FreeAndNil(Etude.DataImp_DonneesGenerale);
  Etude.DataImp_DonneesGenerale := TStringList.Create;
  { Titres }
  Etude.DataImp_DonneesGenerale.Add(cst_Titre1 + 'Caract�ristiques g�n�rales');
  Etude.DataImp_DonneesGenerale.Add(cst_Titre2 + 'Installation de ventilation m�canique contr�l�e');
  { Bouches utilis�es }
  m_Txt := 'Bouches';
  If Etude.Batiment.Reseaux.GetCaisson = nil Then Exit;
  If Etude.Batiment.Reseaux.LogementsA  Then m_Txt := m_Txt + ' autor�glables';
  If Etude.Batiment.Reseaux.LogementsH  Then m_Txt := m_Txt + ', hygror�glables';
  If (Etude.Batiment.Reseaux.LogementsGH) Or (Etude.Batiment.Reseaux.LogementsG) Then m_Txt := m_Txt + ', gaz';
  Etude.DataImp_DonneesGenerale.Add(cst_TxtNorm + m_Txt);
  { Mati�re des joints et accessoires }
    { Pour le reseau horizontal }
    m_Txt := '';
    If Etude.ToutGazHorizontal Then m_Txt1 := 'R�seau horizontal: conduits et accessoires en aluminium'
    Else If Etude.WithTronconsGazHorizontal Then Begin
      If Etude.Batiment.MatiereAccessoires = c_AccessoiresVeloduct Then
        begin
        m_Txt1 := 'autres conduits et accessoires en acier galvanis� �quip�s de joints ';
                if Etude.Batiment.IsFabricantVIM then
                         m_Txt1 := m_Txt1 + 'VELODUCT'
                else
                         m_Txt1 := m_Txt1 + 'Accessoires � joints';
        end
      Else
      If Etude.Batiment.MatiereAccessoires = c_AccessoiresGalva Then
        begin
        m_Txt1 := 'autres conduits et accessoires en acier galvanis� sans joints ';
                if Etude.Batiment.IsFabricantVIM then
                         m_Txt1 := m_Txt1 + 'VELODUCT'
                else
                         m_Txt1 := m_Txt1 + 'Accessoires � joints';
        end
      Else
      m_Txt1 := 'autres conduits et accessoires en mati�re autre';
        m_Txt := 'R�seau horizontal: conduits et accessoires raccord�s � une bouche gaz en aluminium';

    End Else Begin
      If Etude.Batiment.MatiereAccessoires = c_AccessoiresVeloduct Then
        begin
        m_Txt1 := 'R�seau horizontal: conduits et accessoires en acier galvanis� �quip�s de joints ';
                if Etude.Batiment.IsFabricantVIM then
                         m_Txt1 := m_Txt1 + 'VELODUCT'
                else
                         m_Txt1 := m_Txt1 + 'Accessoires � joints';
        end
      Else
      If Etude.Batiment.MatiereAccessoires = c_AccessoiresGalva Then
        begin
        m_Txt1 := 'R�seau horizontal: conduits et accessoires en acier galvanis� sans joints ';
                if Etude.Batiment.IsFabricantVIM then
                         m_Txt1 := m_Txt1 + 'VELODUCT'
                else
                         m_Txt1 := m_Txt1 + 'Accessoires � joints';
        end
      else
      m_Txt1 := 'R�seau horizontal: conduits et accessoires en mati�re autre';
   End;
   If m_Txt <> '' Then Etude.DataImp_DonneesGenerale.Add(cst_TxtNorm + m_Txt);
   Etude.DataImp_DonneesGenerale.Add(cst_TxtNorm + m_Txt1);
    { Pour le reseau vertical }
    m_Txt := '';
    if Etude.Batiment.Reseaux.HasPartieVerticale then
   Begin
    If Etude.ToutGazvertical Then m_Txt1 := 'R�seau vertical: conduits et accessoires en aluminium'
    Else If Etude.WithTronconsGazvertical Then Begin
      If Etude.Batiment.MatiereAccessoires = c_AccessoiresVeloduct Then
        begin
        m_Txt1 := 'autres conduits et accessoires en acier galvanis� �quip�s de joints ';
                if Etude.Batiment.IsFabricantVIM then
                        m_Txt1 := m_Txt1 + 'VELODUCT'
                else
                        m_Txt1 := m_Txt1 + 'Accessoires � joints';
        end
      Else
      If Etude.Batiment.MatiereAccessoires = c_AccessoiresGalva Then
        begin
        m_Txt1 := 'autres conduits et accessoires en acier galvanis� sans joints ';
                if Etude.Batiment.IsFabricantVIM then
                         m_Txt1 := m_Txt1 + 'VELODUCT'
                else
                         m_Txt1 := m_Txt1 + 'Accessoires � joints';
        end
      else
      m_Txt1 := 'autres conduits et accessoires en acier mati�re autre';

        m_Txt := 'R�seau vertical: conduits et accessoires raccord�s � une bouche gaz en aluminium';

    End Else Begin
      If Etude.Batiment.MatiereAccessoires = c_AccessoiresVeloduct Then
        begin
        m_Txt1 := 'R�seau vertical: conduits et accessoires en acier galvanis� �quip�s de joints ';
                if Etude.Batiment.IsFabricantVIM then
                         m_Txt1 := m_Txt1 + 'VELODUCT'
                else
                         m_Txt1 := m_Txt1 + 'Accessoires � joints';
        end
      Else
      If Etude.Batiment.MatiereAccessoires = c_AccessoiresGalva Then
        begin
        m_Txt1 := 'R�seau vertical: conduits et accessoires en acier galvanis� sans joints ';
                if Etude.Batiment.IsFabricantVIM then
                         m_Txt1 := m_Txt1 + 'VELODUCT'
                else
                         m_Txt1 := m_Txt1 + 'Accessoires � joints';
        end
      Else
      m_Txt1 := 'R�seau vertical: conduits et accessoires en mati�re autre';
    End;
   If m_Txt <> '' Then Etude.DataImp_DonneesGenerale.Add(cst_TxtNorm + m_Txt);
   Etude.DataImp_DonneesGenerale.Add(cst_TxtNorm + m_Txt1);
   End;
  { Vitesses maxi }
    m_Txt := ' ';
    if (Etude.Batiment.IsFabricantVIM and Etude.Batiment.Reseaux.HasPartieVerticale) or not(Etude.Batiment.IsFabricantVIM) then
        Begin
        m_Txt := 'Vitesse verticale maxi: ' + Float2Str(Etude.Batiment.VitesseVerticale, 2) + ' m/s';
        Etude.DataImp_DonneesGenerale.Add(cst_TxtNorm + m_Txt);
        End;
  m_Txt := 'Vitesse horizontale maxi: ' + Float2Str(Etude.Batiment.VitesseHorizontale, 2) + ' m/s';
  Etude.DataImp_DonneesGenerale.Add(cst_TxtNorm + m_Txt);
  { Ventilateur }
  m_Ventil  := TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson);
  m_Caisson := m_Ventil.ProduitAssocie;
  If m_Caisson <> nil Then Begin
    m_Txt := 'Ventilateur s�lectionn�: ' + m_Caisson.Reference +  '   r�glage: ' + m_Caisson.Position;
            If (m_Caisson.Entrainement <> c_Edibatec_Caisson_Entrainement_Direct) and (m_Caisson.TypeSelection <> c_CaissonSelect_DebitCst) then
                m_Txt := m_Txt + ' tr/min';
    Etude.DataImp_DonneesGenerale.Add(cst_TxtNorm + m_Txt);
    m_txt := 'D�bit maxi: ' + IntToStr(m_Ventil.DebitMaxi) + ' m�/h - DP: ' + Float2Str(m_Caisson.DpCalcQMax, 0) + ' Pa';
    Etude.DataImp_DonneesGenerale.Add(cst_TxtNorm + m_Txt);
    If Etude.SaveOptions_SelectionCaisson.CheckBox_DTU_Checked  Then
        begin
        m_txt := 'Consommation RT 2005: ' + Float2Str(m_Caisson.CoeffRT, 2) + ' W/m�/h';
        Etude.DataImp_DonneesGenerale.Add(cst_TxtGras + m_Txt);
        end;
        if Etude.SaveOptions_SelectionCaisson.CheckBox_DTU_Checked then
                Begin
                m_txt := 'M�thode de calcul conforme au DTU 68.1';
                Etude.DataImp_DonneesGenerale.Add(cst_TxtNorm + m_Txt);                
                End;
  End Else Etude.DataImp_DonneesGenerale.Add(cst_TxtNorm + 'Pas de ventilateur s�lectionn�'); 

  { Classement au feu }
  If (m_Caisson <> nil) and (Etude.Batiment.MethodeCalcul = c_CalculDTU) Then Begin
    Etude.DataImp_DonneesGenerale.Add(cst_Titre2 + 'Protection incendie - non propagation du feu');
    Etude.DataImp_DonneesGenerale.Add(cst_TxtGras + m_Caisson.Reference);
    Etude.DataImp_DonneesGenerale.Add(cst_TxtNorm + 'Cat�gorie feu: ' + m_Caisson.ClassementFeu);
    If Etude.Batiment.ClapetsSurBouches = c_Oui Then m_txt := 'Pr�sence de clapets coupe-feu � chaque bouche'
                                                Else m_txt := 'Pas de clapets coupe-feu � chaque bouche';
    Etude.DataImp_DonneesGenerale.Add(cst_TxtNorm + m_Txt);
    Etude.DataImp_DonneesGenerale.Add(cst_TxtNorm + 'D�bit par bouche disparue diam�tre 125: 420 m�/h');
    If m_Ventil.TauxDilution <= 1 Then m_Cat := '4'
    Else If m_Ventil.TauxDilution <= 1.6 Then m_Cat := '3'
    Else If m_Ventil.TauxDilution <= 3.5 Then m_Cat := '2'
    Else m_Cat := '1';
    m_Txt := 'Taux de dilution de ' + Float2Str(m_Ventil.TauxDilution, 2) + ' n�cessitant un caisson de cat�gorie ' + m_Cat;
    Etude.DataImp_DonneesGenerale.Add(cst_TxtNorm + m_Txt);
    m_Txt := 'Au sens de l''arr�t� du 31/10/1986 relatif � la protection incendie dans les immeubles d''habitation.';
    Etude.DataImp_DonneesGenerale.Add(cst_TxtNorm + m_Txt);
  End;
End;
{ ************************************************************************************************************************************************* }
Procedure Impression_DonneesGenerales(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Const
        _Title = 'Caract�ristiques g�n�rales';
Var
  m_Grid    : TStringGrid;
  m_Txt     : String;
  m_Txt1    : String;
  m_Cat     : String;
  m_Caisson : TEdibatec_Caisson;
  m_Ventil  : TVentil_Caisson;
Begin
  ApercuImp_Debut(_Lanceur, _Title, Etude.Batiment.Reference, GetPiedDePage, Not _ImpressionEnCours);
  m_Grid := TStringGrid.Create(nil);
  m_Grid.ColCount := 2;
        if Etude.Batiment.IsFabricantVIM then
          m_Grid.RowCount := 12
        else
          m_Grid.RowCount := 11;
  { Bouches utilis�es }
  m_Txt := 'Bouches';
  If Etude.Batiment.Reseaux.LogementsA  Then m_Txt := m_Txt + ' autor�glables';
  If Etude.Batiment.Reseaux.LogementsH  Then m_Txt := m_Txt + ', hygror�glables';
  If (Etude.Batiment.Reseaux.LogementsGH) Or (Etude.Batiment.Reseaux.LogementsG) Then m_Txt := m_Txt + ', gaz';
  m_Grid.Objects[0, 0] := TCelluleImp.Create(m_Txt, GetFontInfos, 99, ClBlack, clWhite, [], taLeftJustify);
  { Mati�re des joints et accessoires }
    { Pour le reseau horizontal }
    m_Txt := '';
    If Etude.ToutGazHorizontal Then m_Txt1 := 'R�seau horizontal: conduits et accessoires en aluminium'
    Else If Etude.WithTronconsGazHorizontal Then Begin
      If Etude.Batiment.MatiereAccessoires = c_AccessoiresVeloduct Then
        begin
        m_Txt1 := 'autres conduits et accessoires en acier galvanis� �quip�s de joints ';
                if Etude.Batiment.IsFabricantVIM then
                          m_Txt1 :=  m_Txt1 + 'VELODUCT'
                else
                          m_Txt1 :=  m_Txt1 + 'Accessoires � joints';
        end
      Else
      If Etude.Batiment.MatiereAccessoires = c_AccessoiresGalva Then
        begin
        m_Txt1 := 'autres conduits et accessoires en acier galvanis� sans joints ';
                if Etude.Batiment.IsFabricantVIM then
                          m_Txt1 :=  m_Txt1 + 'VELODUCT'
                else
                          m_Txt1 :=  m_Txt1 + 'Accessoires � joints';        
        end
      Else
      m_Txt1 := 'autres conduits et accessoires en mati�re autre';
        m_Txt := 'R�seau horizontal: conduits et accessoires raccord�s � une bouche gaz en aluminium';

    End Else Begin
      If Etude.Batiment.MatiereAccessoires = c_AccessoiresVeloduct Then
        begin
        m_Txt1 := 'R�seau horizontal: conduits et accessoires en acier galvanis� �quip�s de joints ';
                if Etude.Batiment.IsFabricantVIM then
                          m_Txt1 :=  m_Txt1 + 'VELODUCT'
                else
                          m_Txt1 :=  m_Txt1 + 'Accessoires � joints';
        end
      Else
      if Etude.Batiment.MatiereAccessoires = c_AccessoiresGalva Then
        begin
        m_Txt1 := 'R�seau horizontal: conduits et accessoires en acier galvanis� sans joints ';
                if Etude.Batiment.IsFabricantVIM then
                          m_Txt1 :=  m_Txt1 + 'VELODUCT'
                else
                          m_Txt1 :=  m_Txt1 + 'Accessoires � joints';        
        end
      Else
      m_Txt1 := 'R�seau horizontal: conduits et accessoires en mati�re autre';
   End;
    m_Grid.Objects[0, 1] := TCelluleImp.Create(m_Txt, GetFontInfos, 99, ClBlack, clWhite, [], taLeftJustify);
    m_Grid.Objects[0, 2] := TCelluleImp.Create(m_Txt1, GetFontInfos, 99, ClBlack, clWhite, [], taLeftJustify);
    { Pour le reseau vertical }
    m_Txt := '';
    if (Etude.Batiment.IsFabricantVIM and Etude.Batiment.Reseaux.HasPartieVerticale) or not(Etude.Batiment.IsFabricantVIM) then
  Begin
    If Etude.ToutGazvertical Then m_Txt1 := 'R�seau vertical: conduits et accessoires en aluminium'
    Else If Etude.WithTronconsGazvertical Then Begin
      If Etude.Batiment.MatiereAccessoires = c_AccessoiresVeloduct Then
        begin
        m_Txt1 := 'autres conduits et accessoires en acier galvanis� �quip�s de joints ';
                if Etude.Batiment.IsFabricantVIM then
                          m_Txt1 :=  m_Txt1 + 'VELODUCT'
                else
                          m_Txt1 :=  m_Txt1 + 'Accessoires � joints';
        end
      Else
      If Etude.Batiment.MatiereAccessoires = c_AccessoiresGalva Then
        begin
        m_Txt1 := 'autres conduits et accessoires en acier galvanis� sans joints ';
                if Etude.Batiment.IsFabricantVIM then
                          m_Txt1 :=  m_Txt1 + 'VELODUCT'
                else
                          m_Txt1 :=  m_Txt1 + 'Accessoires � joints';         
        end
      Else
      m_Txt1 := 'autres conduits et accessoires en mati�re autre';
        m_Txt := 'R�seau vertical: conduits et accessoires raccord�s � une bouche gaz en aluminium';

    End Else Begin
      If Etude.Batiment.MatiereAccessoires = c_AccessoiresVeloduct Then
        begin
        m_Txt1 := 'R�seau vertical: conduits et accessoires en acier galvanis� �quip�s de joints ';
                if Etude.Batiment.IsFabricantVIM then
                          m_Txt1 :=  m_Txt1 + 'VELODUCT'
                else
                          m_Txt1 :=  m_Txt1 + 'Accessoires � joints';
        end
      Else
      If Etude.Batiment.MatiereAccessoires = c_AccessoiresGalva Then
        begin
        m_Txt1 := 'R�seau vertical: conduits et accessoires en acier galvanis� sans joints ';
                if Etude.Batiment.IsFabricantVIM then
                          m_Txt1 :=  m_Txt1 + 'VELODUCT'
                else
                          m_Txt1 :=  m_Txt1 + 'Accessoires � joints';         
        end
      Else
      m_Txt1 := 'R�seau vertical: conduits et accessoires en mati�re autre';
   End;
    m_Grid.Objects[0, 3] := TCelluleImp.Create(m_Txt, GetFontInfos, 99, ClBlack, clWhite, [], taLeftJustify);
    m_Grid.Objects[0, 4] := TCelluleImp.Create(m_Txt1, GetFontInfos, 99, ClBlack, clWhite, [], taLeftJustify);
  End;    

  { Vitesses maxi }
    m_Txt := ' ';
    if (Etude.Batiment.IsFabricantVIM and Etude.Batiment.Reseaux.HasPartieVerticale) or not(Etude.Batiment.IsFabricantVIM) then
        m_Txt := 'Vitesse verticale maxi: ' + Float2Str(Etude.Batiment.VitesseVerticale, {$IfDef VIM_OPTAIR}2{$Else}0{$EndIf}) + ' m/s';
  m_Grid.Objects[0, 5] := TCelluleImp.Create(m_Txt, GetFontInfos, 99, ClBlack, clWhite, [], taLeftJustify);
  m_Txt := 'Vitesse horizontale maxi: ' + Float2Str(Etude.Batiment.VitesseHorizontale, {$IfDef VIM_OPTAIR}2{$Else}0{$EndIf}) + ' m/s';
  m_Grid.Objects[0, 6] := TCelluleImp.Create(m_Txt, GetFontInfos, 99, ClBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[0, 7] := TCelluleImp.Create('   ', GetFontInfos, 99, ClBlack, clWhite, [], taLeftJustify); // On saute une ligne :)
  { Ventilateur }
  m_Ventil  := TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson);
  m_Caisson := m_Ventil.ProduitAssocie;
  If m_Caisson <> nil Then Begin
    m_Txt := 'Ventilateur s�lectionn�: ' + m_Caisson.Reference +  '   r�glage: ' + m_Caisson.Position;
            If (m_Caisson.Entrainement <> c_Edibatec_Caisson_Entrainement_Direct) and (m_Caisson.TypeSelection <> c_CaissonSelect_DebitCst) then
                m_Txt := m_Txt + ' tr/min';
    m_Grid.Objects[0, 8] := TCelluleImp.Create(m_Txt, GetFontInfos, 99, ClBlack, clWhite, [], taLeftJustify);
    m_txt := 'D�bit maxi: ' + IntToStr(m_Ventil.DebitMaxi) + ' m�/h - DP: ' + Float2Str(m_Caisson.DpCalcQMax, 0) + ' Pa';
    m_Grid.Objects[0, 9] := TCelluleImp.Create(m_Txt, GetFontInfos, 99, ClBlack, clWhite, [], taLeftJustify);
    If  not(Etude.Batiment.IsFabricantVIM ) or Etude.SaveOptions_SelectionCaisson.CheckBox_DTU_Checked  Then
            m_txt := 'Consommation RT 2005: ' + Float2Str(m_Caisson.CoeffRT, 2) + ' W/m�/h'
    else
            m_txt := '';
    m_Grid.Objects[0, 10] := TCelluleImp.Create(m_Txt, GetFontInfos, 99, ClBlack, clWhite, [fsBold], taLeftJustify);
    if Etude.Batiment.IsFabricantVIM then
        begin
        if Etude.SaveOptions_SelectionCaisson.CheckBox_DTU_Checked then
                m_txt := 'M�thode de calcul conforme au DTU 68.1'
        else
                m_txt := ' ';
        m_Grid.Objects[0, 11] := TCelluleImp.Create(m_Txt, GetFontInfos, 99, ClBlack, clWhite, [fsItalic], taLeftJustify);
        end;
  End Else m_Grid.Objects[0, 8] := TCelluleImp.Create('Pas de ventilateur s�lectionn�', GetFontInfos, 99, ClBlack, clWhite, [], taLeftJustify);
  ApercuImp_AjouteGrille(m_Grid, False, 'Installation de ventilation m�canique contr�l�e');
  { Classement au feu }
  If (m_Caisson <> nil) and (Etude.Batiment.MethodeCalcul = c_CalculDTU) Then Begin
    m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 2; m_Grid.RowCount := 8;
    m_Grid.Objects[0, 0] := TCelluleImp.Create(m_Caisson.Reference, GetFontInfos, 99, ClBlack, clWhite, [fsBold], taLeftJustify);
    m_Grid.Objects[0, 1] := TCelluleImp.Create('Cat�gorie feu: ' + m_Caisson.ClassementFeu, GetFontInfos, 99, ClBlack, clWhite, [], taLeftJustify);
    If Etude.Batiment.ClapetsSurBouches = c_Oui Then m_txt := 'Pr�sence de clapets coupe-feu � chaque bouche'
                                                Else m_txt := 'Pas de clapets coupe-feu � chaque bouche';
    m_Grid.Objects[0, 2] := TCelluleImp.Create(m_Txt, GetFontInfos, 99, ClBlack, clWhite, [], taLeftJustify);
    m_Grid.Objects[0, 3] := TCelluleImp.Create('D�bit par bouche disparue diam�tre 125: 420 m�/h', GetFontInfos, 99, ClBlack, clWhite, [], taLeftJustify);
    If m_Ventil.TauxDilution <= 1 Then m_Cat := '4'
    Else If m_Ventil.TauxDilution <= 1.6 Then m_Cat := '3'
    Else If m_Ventil.TauxDilution <= 3.5 Then m_Cat := '2'
    Else m_Cat := '1';
    m_Txt := 'Taux de dilution de ' + Float2Str(m_Ventil.TauxDilution, 2) + ' n�cessitant un caisson de cat�gorie ' + m_Cat;
    m_Grid.Objects[0, 4] := TCelluleImp.Create(m_Txt, GetFontInfos, 99, ClBlack, clWhite, [], taLeftJustify);
    m_Txt := 'Au sens de l''arr�t� du 31/10/1986 relatif � la protection incendie dans les immeubles d''habitation.';
    m_Grid.Objects[0, 5] := TCelluleImp.Create(m_Txt, GetFontInfos, 99, ClBlack, clWhite, [], taLeftJustify);
    TCelluleImp(m_Grid.Objects[0, 5]).IMerged := True;
//    m_Txt := 'P.V. du CTICM N� 05-H-0148';
//    m_Grid.Objects[0, 6] := TCelluleImp.Create(m_Txt, GetFontInfos, 99, ClBlack, clWhite, [], taLeftJustify);
    ApercuImp_AjouteGrille(m_Grid, False, 'Protection incendie - non propagation du feu');
  End;
  { Si il y a de l'hygro on affiche les informations sur les avis techniques
  If (Etude.Batiment.Reseaux.LogementsH) Or (Etude.Batiment.Reseaux.LogementsGH) Then Begin
    ApercuImp_NouvellePage;
    Impression_DonneesAvisTechniques(True);
  End;}
  { D�termination des entr�es d'air
  ApercuImp_NouvellePage;
  Impression_DonneesEntreesAir(True); }
  { Resultats acoustiques }
  {ApercuImp_NouvellePage(False);
  Impression_Acoustique(True);}
  { Calcul retour
  If m_Caisson <> nil Then Begin
    ApercuImp_NouvellePage;
    Impression_CalculRetour(True);
  End;            }
  { Sch�mas
  ApercuImp_NouvellePage;
  Impression_SchemasEtages(True);
  Impression_SchemasColonnes(True);
  ApercuImp_Show;}
End;
{ ************************************************************************************************************************************************* }
Procedure SetDataImp_DonneesAvisTechniques;
Var
  m_Col,
  m_Row  : Integer;
  m_Txt  : String;
Begin
  FreeAndNil(Etude.DataImp_AvisTechniques);

  If Not(Etude.Batiment.Reseaux.LogementsHA) and Not(Etude.Batiment.Reseaux.LogementsHB) then exit;

  Etude.DataImp_AvisTechniques := TStringList.Create;
  Etude.DataImp_AvisTechniques.Add(cst_Titre1 + 'Avis techniques pr�sents dans l''�tude');




//***********************

if Etude.Batiment.Reseaux.AlizeIII_Present then
begin
  { Aliz�e III HygroA }
  If Etude.Batiment.Reseaux.LogementsHA Then Begin
    Etude.DataImp_AvisTechniques.Add(cst_Titre2 + '14/07-1194*V10 - Syst�mes de ventilation m�canique hygror�glable VIM - Alize III Hygro A');
    For m_Row := 1 To 10 Do Begin
      m_Txt := 'L6' + cst_AlizeIIIHygroA[m_Row, 0];
      For m_Col := 1 To 5 Do m_Txt := m_Txt + cst_ImpSep + cst_AlizeIIIHygroA[m_Row, m_Col];
      Etude.DataImp_AvisTechniques.Add(m_Txt);
    End;
  End;
  { Aliz�e III HygroB }
  If Etude.Batiment.Reseaux.LogementsHB Then Begin
    Etude.DataImp_AvisTechniques.Add(cst_Titre2 + '14/07-1194*V10 - Syst�mes de ventilation m�canique hygror�glable VIM - Alize III Hygro B');
    For m_Row := 1 To 10 Do Begin
      m_Txt := 'L6' + cst_AlizeIIIHygroB[m_Row, 0];
      For m_Col := 1 To 5 Do m_Txt := m_Txt + cst_ImpSep + cst_AlizeIIIHygroB[m_Row, m_Col];
      Etude.DataImp_AvisTechniques.Add(m_Txt);
    End;
  End;
  { Aliz�e III HygroGaz }
  If Etude.Batiment.Reseaux.LogementsGH Then Begin
    Etude.DataImp_AvisTechniques.Add(cst_Titre2 + '14/07-1194*V10 - Syst�mes de ventilation m�canique hygror�glable VIM - Alize III Hygro Gaz');
    For m_Row := 1 To 9 Do Begin
      m_Txt := 'L6' + cst_AlizeIIIHygroGaz[m_Row, 0];
      For m_Col := 1 To 5 Do m_Txt := m_Txt + cst_ImpSep + cst_AlizeIIIHygroGaz[m_Row, m_Col];
      Etude.DataImp_AvisTechniques.Add(m_Txt);
    End;
  End;
end
else
begin
  { HygroA }
  If Etude.Batiment.Reseaux.LogementsHA Then Begin
    Etude.DataImp_AvisTechniques.Add(cst_Titre2 + 'Syst�me SIROC HYGRO 14/01-629');
    For m_Row := 1 To 7 Do Begin
      m_Txt := 'L5' + cst_HygroA[m_Row, 0];
      For m_Col := 1 To 4 Do m_Txt := m_Txt + cst_ImpSep + cst_HygroA[m_Row, m_Col];
      Etude.DataImp_AvisTechniques.Add(m_Txt);
    End;
  End;
  { HygroB }
  If Etude.Batiment.Reseaux.LogementsHB Then Begin
    Etude.DataImp_AvisTechniques.Add(cst_Titre2 + 'Syst�me de ventilation hygror�glable HYGRO''AIR 14/01-700');
    For m_Row := 1 To 26 Do Begin
      m_Txt := 'L6' + cst_HygroB[m_Row, 0];
      For m_Col := 1 To 5 Do m_Txt := m_Txt + cst_ImpSep + cst_HygroB[m_Row, m_Col];
      Etude.DataImp_AvisTechniques.Add(m_Txt);
    End;
  End;
end;

End;
{ ************************************************************************************************************************************************* }
Procedure Impression_DonneesAvisTechniques(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Const
        _Title = 'Avis techniques pr�sents dans l''�tude';
Var
  m_Grid : TStringGrid;
  m_Col,
  m_Row  : Integer;
  m_Color: TColor;
Begin
  ApercuImp_Debut(_Lanceur, _Title, Etude.Batiment.Reference, GetPiedDePage, Not _ImpressionEnCours);


if Etude.Batiment.Reseaux.AlizeIII_Present then
begin
  { Aliz�e III HygroA }
  If Etude.Batiment.Reseaux.LogementsHA Then Begin
    m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 6; m_Grid.RowCount := 11;
    For m_Row := 0 To 10 Do For m_Col := 0 To 5 Do Begin
      If m_Row = 0 Then m_Color := clSkyBlue
                   Else m_Color := clWhite;
      m_Grid.Objects[m_Col, m_Row] := TCelluleImp.Create(cst_AlizeIIIHygroA[m_Row, m_Col], GetFontInfos, 0, clBlack, m_Color);
    End;
  ApercuImp_AjouteGrille(m_Grid, True, '14/07-1194*V10 - Syst�mes de ventilation m�canique hygror�glable VIM - Alize III Hygro A', False, False, False, True);
  End;
  { Aliz�e III HygroB }
  If Etude.Batiment.Reseaux.LogementsHB Then Begin
    m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 5; m_Grid.RowCount := 11;
    For m_Row := 0 To 10 Do For m_Col := 0 To 5 Do Begin
      If m_Row = 0 Then m_Color := clSkyBlue
                   Else m_Color := clWhite;
      m_Grid.Objects[m_Col, m_Row] := TCelluleImp.Create(cst_AlizeIIIHygroB[m_Row, m_Col], GetFontInfos, 0, clBlack, m_Color);
    End;
  ApercuImp_AjouteGrille(m_Grid, True, '14/07-1194*V10 - Syst�mes de ventilation m�canique hygror�glable VIM - Alize III Hygro B', False, False, False, True);
  End;
  { Aliz�e III HygroGaz }
  If Etude.Batiment.Reseaux.LogementsGH Then Begin
    m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 5; m_Grid.RowCount := 10;
    For m_Row := 0 To 9 Do For m_Col := 0 To 5 Do Begin
      If m_Row = 0 Then m_Color := clSkyBlue
                   Else m_Color := clWhite;
      m_Grid.Objects[m_Col, m_Row] := TCelluleImp.Create(cst_AlizeIIIHygroGaz[m_Row, m_Col], GetFontInfos, 0, clBlack, m_Color);
    End;
  ApercuImp_AjouteGrille(m_Grid, True, '14/07-1194*V10 - Syst�mes de ventilation m�canique hygror�glable VIM - Alize III Hygro Gaz', False, False, False, True);
  End;
end
else
begin
  { HygroA }
  If Etude.Batiment.Reseaux.LogementsHA Then Begin
    m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 5; m_Grid.RowCount := 8;
    For m_Row := 0 To 7 Do For m_Col := 0 To 4 Do Begin
      If m_Row = 0 Then m_Color := clSkyBlue
                   Else m_Color := clWhite;
      m_Grid.Objects[m_Col, m_Row] := TCelluleImp.Create(cst_HygroA[m_Row, m_Col], GetFontInfos, 0, clBlack, m_Color);
    End;
    ApercuImp_AjouteGrille(m_Grid, True, 'Syst�me SIROC HYGRO 14/01-629', False, False, False, True);
  End;
  { HygroB }
  If Etude.Batiment.Reseaux.LogementsHB Then Begin
    m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 6; m_Grid.RowCount := 27;
    For m_Row := 0 To 26 Do For m_Col := 0 To 5 Do Begin
      If m_Row = 0 Then m_Color := clSkyBlue
                   Else m_Color := clWhite;
      m_Grid.Objects[m_Col, m_Row] := TCelluleImp.Create(cst_HygroB[m_Row, m_Col], GetFontInfos, 0, clBlack, m_Color);
    End;
    ApercuImp_AjouteGrille(m_Grid, True, 'Syst�me de ventilation hygror�glable HYGRO''AIR 14/01-700', False, False, False, True);
  End;
end;
//  If Not _ImpressionEnCours Then ApercuImp_Show;
End;
{ ************************************************************************************************************************************************* }
Procedure SetDataImp_DonneesEntreesAir;
Const
  c_ColHeaders : array[0..8] Of String = (' ', ' ', ' ', 'D�bits normalis�s', '', ' ', 'Nombre de modules s�lectionn�s', '', ' ');
  c_ColHeaders2: array[0..8] Of String = ('Nom logement', 'Type logement', 'Syst�me', 'S�jour', 'Chambre(s)', '22 m�/h', '30 m�/h', '45 m�/h', '6/45 m�/h');
Var
  m_Txt             : String;
  m_NbEA            : Array[1..4] Of Integer;
//  m_NoCol           : Integer;
  m_NoLogement      : Integer;
  m_Logement        : TVentil_Logement;
  m_NoEA            : Integer;
  m_NoPiece         : Integer;
  MyDebit           : Integer;
  _DebitNormSejour  : Integer;
  _DebitNormSejour2 : Integer;
  _DebitNormChambre : Integer;
  _NbNormSejour     : Integer;
  _NbNormChambre    : Integer;
  _LibNormSejour    : String;
  _LibNormChambre   : String;
  _NbEntree         : Integer;
  m_InfosEA         : TVentil_EntreesAirLogement;
Begin

if (Etude.Batiment.TypeBat = c_BatimentTertiaire) then exit;

if Etude.Batiment.IsFabricantVIM then
begin

_NbNormSejour := 0;
_NbNormChambre := 0;

  FreeAndNil(Etude.DataImp_DonneesEntreesAir);

  if (Etude.Batiment.ChiffrageDN = C_NON) then exit;

  Etude.DataImp_DonneesEntreesAir := TStringList.Create;
  Etude.DataImp_DonneesEntreesAir.Add(cst_Titre1 + 'D�termination des entr�es d''air');

if (Etude.Batiment.ChiffrageDN = C_OUI) then
  For m_NoLogement := 0 To Etude.Batiment.Logements.Count - 1 Do
  Begin
  m_NbEA[1] := 0;
  m_NbEA[2] := 0;
  m_NbEA[4] := 0;
  m_NbEA[3] := 0;

  _DebitNormSejour  := -1;
  _DebitNormSejour2 := -1;
  _DebitNormChambre := -1;

    m_Logement := Etude.Batiment.Logements[m_NoLogement];

    If ((m_Logement.CalculDn = c_Oui) or (Etude.Batiment.CalculDN = c_Oui))  And (m_Logement.EntreesDAirDnChoisies) Then Begin
      m_InfosEA := m_Logement.Ventil_EntreesAirLogement[m_Logement.TypeLogement];
      For m_NoPiece := 1 To m_InfosEA.Count Do
        if m_InfosEA[m_NoPiece - 1].EntreeChoisie.Reference <> '' then
                inc(m_NbEA[TVentil_DonneesEA(m_InfosEA[m_NoPiece - 1]).Debit]);
    
    End;
For m_NoPiece := 1 To c_NbPiecesSeches Do
    Begin

        If m_NoPiece = 1 Then
                _NbEntree := 1
        else
                _NbEntree := m_Logement.TypeLogement - 1;

      If m_Logement.Systeme.AutoGaz And (m_Logement.ChaudiereCuisine = c_NON) And (m_Logement.CellierVentile = c_Non) Then
        Begin
        MyDebit := m_Logement.Systeme.EntreeAirSiCellier[m_Logement.TypeLogement, m_NoPiece][0].Debit;
                If m_NoPiece = 1 Then
                        Begin
                                if (_DebitNormSejour = -1) or (_DebitNormSejour = MyDebit) then
                                        Begin
                                        _DebitNormSejour := MyDebit;
                                        _NbNormSejour    := m_Logement.Systeme.EntreeAirSiCellier[m_Logement.TypeLogement, m_NoPiece][0].Nombre;
                                        End
                                else
                                        _DebitNormSejour2 := MyDebit;
                        end
                else
                        Begin
                        _DebitNormChambre := MyDebit;
                        _NbNormChambre    := m_Logement.Systeme.EntreeAirSiCellier[m_Logement.TypeLogement, m_NoPiece][0].Nombre;
                        End;
                If not(((m_Logement.CalculDn = c_Oui) or (Etude.Batiment.CalculDN = c_Oui))  And (m_Logement.EntreesDAirDnChoisies)) Then
                case MyDebit of    //
                        22: m_NbEA[1] := m_NbEA[1] +  m_Logement.Systeme.EntreeAirSiCellier[m_Logement.TypeLogement, m_NoPiece][0].Nombre * _NbEntree;
                        30: m_NbEA[2] := m_NbEA[2] +  m_Logement.Systeme.EntreeAirSiCellier[m_Logement.TypeLogement, m_NoPiece][0].Nombre * _NbEntree;
                        40: m_NbEA[4] := m_NbEA[4] +  m_Logement.Systeme.EntreeAirSiCellier[m_Logement.TypeLogement, m_NoPiece][0].Nombre * _NbEntree;
                        45: m_NbEA[3] := m_NbEA[3] +  m_Logement.Systeme.EntreeAirSiCellier[m_Logement.TypeLogement, m_NoPiece][0].Nombre * _NbEntree;
                end;    // case
        End
      Else
        Begin                                           
                For m_NoEA := Low(m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, m_NoPiece]) To High(m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, m_NoPiece]) Do
                        If m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, m_NoPiece][m_NoEA].Nombre > 0 Then
                                Begin
                                        if (m_NoPiece = 1) And (m_Logement.Systeme.AlizeIII) And (m_Logement.TypeLogement = c_TypeT3) and (m_Logement.T3Optimise = c_Oui) then
                                                Begin
                                                MyDebit := m_Logement.Systeme.EASejourT3Opt.Debit;
                                                        if (_DebitNormSejour = -1) or (_DebitNormSejour = MyDebit) then
                                                                Begin
                                                                _DebitNormSejour := MyDebit;
                                                                _NbNormSejour    := m_Logement.Systeme.EASejourT3Opt.Nombre;
                                                                End
                                                        Else
                                                                _DebitNormSejour2 := MyDebit;
                                                End
                                        Else
                                                Begin
                                                MyDebit := m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, m_NoPiece][m_NoEA].Debit;
                                                        If m_NoPiece = 1 Then
                                                                Begin
                                                                        if (_DebitNormSejour = -1) or (_DebitNormSejour = MyDebit) then
                                                                                Begin
                                                                                _DebitNormSejour := MyDebit;
                                                                                _NbNormSejour    := m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, m_NoPiece][m_NoEA].Nombre;
                                                                                End
                                                                        Else
                                                                                _DebitNormSejour2 := MyDebit;
                                                                End
                                                        else
                                                                Begin
                                                                _DebitNormChambre := MyDebit;
                                                                _NbNormChambre    := m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, m_NoPiece][m_NoEA].Nombre;                                                                
                                                                End;
                                                End;

                                        If not(((m_Logement.CalculDn = c_Oui) or (Etude.Batiment.CalculDN = c_Oui))  And (m_Logement.EntreesDAirDnChoisies)) Then
                                        case MyDebit of    //
                                                22: m_NbEA[1] := m_NbEA[1] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, m_NoPiece][m_NoEA].Nombre * _NbEntree;
                                                30: m_NbEA[2] := m_NbEA[2] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, m_NoPiece][m_NoEA].Nombre * _NbEntree;
                                                40: m_NbEA[4] := m_NbEA[4] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, m_NoPiece][m_NoEA].Nombre * _NbEntree;
                                                45: m_NbEA[3] := m_NbEA[3] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, m_NoPiece][m_NoEA].Nombre * _NbEntree;
                                        end;    // case
                                End;
        End;
    End;


if _DebitNormSejour > -1 then
        case _DebitNormSejour of
                22: _LibNormSejour := c_DebitEA[c_DebitEA22];
                30: _LibNormSejour := c_DebitEA[c_DebitEA30];
                40: _LibNormSejour := c_DebitEA[c_DebitEA645];
                45: _LibNormSejour := c_DebitEA[c_DebitEA45];
        end;    // case

if _DebitNormChambre > -1 then
        case _DebitNormChambre of
                22: _LibNormChambre := c_DebitEA[c_DebitEA22];
                30: _LibNormChambre := c_DebitEA[c_DebitEA30];
                40: _LibNormChambre := c_DebitEA[c_DebitEA645];
                45: _LibNormChambre := c_DebitEA[c_DebitEA45];
        end;    // case

if (_NbNormSejour > 1) and (_DebitNormSejour > -1) then
        _LibNormSejour := IntToStr(_NbNormSejour) + 'X ' + _LibNormSejour;
if (_NbNormChambre > 1) and (_DebitNormChambre > -1) then
        _LibNormChambre := IntToStr(_NbNormChambre) + 'X ' + _LibNormChambre;

if _DebitNormSejour2 > -1 then
        case _DebitNormSejour2 of
                22: _LibNormSejour := _LibNormSejour + ' + ' + c_DebitEA[c_DebitEA22];
                30: _LibNormSejour := _LibNormSejour + ' + ' + c_DebitEA[c_DebitEA30];
                40: _LibNormSejour := _LibNormSejour + ' + ' + c_DebitEA[c_DebitEA645];
                45: _LibNormSejour := _LibNormSejour + ' + ' + c_DebitEA[c_DebitEA45];
        end;    // case


    m_Txt := 'L9' + m_Logement.Reference;
    m_Txt := m_Txt + cst_ImpSep + 'T' + IntToSTr(m_Logement.TypeLogement);
    m_Txt := m_Txt + cst_ImpSep + m_Logement.Systeme.Reference;
    m_Txt := m_Txt + cst_ImpSep + _LibNormSejour;
    m_Txt := m_Txt + cst_ImpSep + _LibNormChambre;
    m_Txt := m_Txt + cst_ImpSep + IntToStr(m_NbEA[1]);
    m_Txt := m_Txt + cst_ImpSep + IntToStr(m_NbEA[2]);
    m_Txt := m_Txt + cst_ImpSep + IntToStr(m_NbEA[3]);
    m_Txt := m_Txt + cst_ImpSep + IntToStr(m_NbEA[4]);
    m_Txt := m_Txt + cst_ImpSep;
    Etude.DataImp_DonneesEntreesAir.Add(m_Txt);

  End;
End;
End;
{ ************************************************************************************************************************************************* }
Procedure Impression_DonneesEntreesAir(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Begin
        if Etude.Batiment.IsFabricantVIM then
                Impression_DonneesEntreesAirVIM(_ImpressionEnCours, _Lanceur)
        else
                Impression_DonneesEntreesAirANJOS(_ImpressionEnCours, _Lanceur);                                
End;
{ ************************************************************************************************************************************************* }
Procedure Impression_DonneesEntreesAirVIM(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Const
  _Title = 'D�termination des entr�es d''air';
  c_ColHeaders : array[0..8] Of String = (' ', ' ', ' ', 'D�bits normalis�s', '', ' ', 'Nombre de modules s�lectionn�s', '', ' ');
  c_ColHeaders2: array[0..8] Of String = ('Nom logement', 'Type logement', 'Syst�me', 'S�jour', 'Chambre(s)', '22 m�/h', '30 m�/h', '45 m�/h', '6/45 m�/h');
Var
  m_Grid            : TStringGrid;
  m_NbEA            : Array[1..4] Of Integer;
  m_NoCol           : Integer;
  m_NoLogement      : Integer;
  m_Logement        : TVentil_Logement;
  m_NoEA            : Integer;
  m_NoPiece         : Integer;
  MyDebit           : Integer;
  _DebitNormSejour  : Integer;
  _DebitNormSejour2 : Integer;
  _DebitNormChambre : Integer;
  _NbNormSejour     : Integer;
  _NbNormChambre    : Integer;
  _LibNormSejour    : String;
  _LibNormChambre   : String;
  _NbEntree         : Integer;
  m_InfosEA         : TVentil_EntreesAirLogement;  
Begin

if (Etude.Batiment.TypeBat = c_BatimentTertiaire) then exit;

_NbNormSejour := 0;
_NbNormChambre := 0;

  ApercuImp_Debut(_Lanceur, _Title, Etude.Batiment.Reference, GetPiedDePage, Not _ImpressionEnCours);
  m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 9; m_Grid.RowCount := 3;
  For m_NoCol := Low(c_ColHeaders) To High(c_ColHeaders) Do m_Grid.Objects[m_NoCol, 0] := TCelluleImp.Create(c_ColHeaders[m_NoCol], GetFontInfos, 0, ClBlack, ClSkyBlue, [], taCenter);

  TCelluleImp(m_Grid.Objects[3, 0]).IMerged := True;
  TCelluleImp(m_Grid.Objects[5, 0]).IMerged := True;
  TCelluleImp(m_Grid.Objects[6, 0]).IMerged := True;
  TCelluleImp(m_Grid.Objects[7, 0]).IMerged := True;

  For m_NoCol := Low(c_ColHeaders) To High(c_ColHeaders) Do m_Grid.Objects[m_NoCol, 1] := TCelluleImp.Create(c_ColHeaders2[m_NoCol], GetFontInfos, 0, ClBlack, ClSkyBlue, [], taCenter);
  TCelluleImp(m_Grid.Objects[0, 0]).IHorMerged := True;
  TCelluleImp(m_Grid.Objects[1, 0]).IHorMerged := True;
  TCelluleImp(m_Grid.Objects[2, 0]).IHorMerged := True;
  ApercuImp_AjouteGrille(m_Grid, True, '', False, False, False, True);


  m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 9; m_Grid.RowCount := Etude.Batiment.Logements.Count;

if (Etude.Batiment.ChiffrageDN = C_OUI) then
  For m_NoLogement := 0 To Etude.Batiment.Logements.Count - 1 Do
  Begin
  m_NbEA[1] := 0;
  m_NbEA[2] := 0;
  m_NbEA[4] := 0;
  m_NbEA[3] := 0;

  _DebitNormSejour  := -1;
  _DebitNormSejour2 := -1;
  _DebitNormChambre := -1;

    m_Logement := Etude.Batiment.Logements[m_NoLogement];


    If ((m_Logement.CalculDn = c_Oui) or (Etude.Batiment.CalculDN = c_Oui))  And (m_Logement.EntreesDAirDnChoisies) Then Begin

      m_InfosEA := m_Logement.Ventil_EntreesAirLogement[m_Logement.TypeLogement];
      For m_NoPiece := 1 To m_InfosEA.Count Do
        if m_InfosEA[m_NoPiece - 1].EntreeChoisie.Reference <> '' then
                inc(m_NbEA[TVentil_DonneesEA(m_InfosEA[m_NoPiece - 1]).Debit]);
    
    End;
For m_NoPiece := 1 To c_NbPiecesSeches Do
    Begin

        If m_NoPiece = 1 Then
                _NbEntree := 1
        else
                _NbEntree := m_Logement.TypeLogement - 1;

      If m_Logement.Systeme.AutoGaz And (m_Logement.ChaudiereCuisine = c_NON) And (m_Logement.CellierVentile = c_Non) Then
        Begin
        MyDebit := m_Logement.Systeme.EntreeAirSiCellier[m_Logement.TypeLogement, m_NoPiece][0].Debit;
                If m_NoPiece = 1 Then
                        Begin
                                if (_DebitNormSejour = -1) or (_DebitNormSejour = MyDebit) then
                                        Begin
                                        _DebitNormSejour := MyDebit;
                                        _NbNormSejour    := m_Logement.Systeme.EntreeAirSiCellier[m_Logement.TypeLogement, m_NoPiece][0].Nombre;
                                        End
                                else
                                        _DebitNormSejour2 := MyDebit;
                        end
                else
                        Begin
                        _DebitNormChambre := MyDebit;
                        _NbNormChambre    := m_Logement.Systeme.EntreeAirSiCellier[m_Logement.TypeLogement, m_NoPiece][0].Nombre;
                        End;
                If not(((m_Logement.CalculDn = c_Oui) or (Etude.Batiment.CalculDN = c_Oui))  And (m_Logement.EntreesDAirDnChoisies)) Then
                case MyDebit of    //
                        22: m_NbEA[1] := m_NbEA[1] +  m_Logement.Systeme.EntreeAirSiCellier[m_Logement.TypeLogement, m_NoPiece][0].Nombre * _NbEntree;
                        30: m_NbEA[2] := m_NbEA[2] +  m_Logement.Systeme.EntreeAirSiCellier[m_Logement.TypeLogement, m_NoPiece][0].Nombre * _NbEntree;
                        40: m_NbEA[4] := m_NbEA[4] +  m_Logement.Systeme.EntreeAirSiCellier[m_Logement.TypeLogement, m_NoPiece][0].Nombre * _NbEntree;
                        45: m_NbEA[3] := m_NbEA[3] +  m_Logement.Systeme.EntreeAirSiCellier[m_Logement.TypeLogement, m_NoPiece][0].Nombre * _NbEntree;
                end;    // case
        End
      Else
        Begin                                           
                For m_NoEA := Low(m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, m_NoPiece]) To High(m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, m_NoPiece]) Do
                        If m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, m_NoPiece][m_NoEA].Nombre > 0 Then
                                Begin
                                        if (m_NoPiece = 1) And (m_Logement.Systeme.AlizeIII) And (m_Logement.TypeLogement = c_TypeT3) and (m_Logement.T3Optimise = c_Oui) then
                                                Begin
                                                MyDebit := m_Logement.Systeme.EASejourT3Opt.Debit;
                                                        if (_DebitNormSejour = -1) or (_DebitNormSejour = MyDebit) then
                                                                Begin
                                                                _DebitNormSejour := MyDebit;
                                                                _NbNormSejour    := m_Logement.Systeme.EASejourT3Opt.Nombre;
                                                                End
                                                        Else
                                                                _DebitNormSejour2 := MyDebit;
                                                End
                                        Else
                                                Begin
                                                MyDebit := m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, m_NoPiece][m_NoEA].Debit;
                                                        If m_NoPiece = 1 Then
                                                                Begin
                                                                        if (_DebitNormSejour = -1) or (_DebitNormSejour = MyDebit) then
                                                                                Begin
                                                                                _DebitNormSejour := MyDebit;
                                                                                _NbNormSejour    := m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, m_NoPiece][m_NoEA].Nombre;
                                                                                End
                                                                        Else
                                                                                _DebitNormSejour2 := MyDebit;
                                                                End
                                                        else
                                                                Begin
                                                                _DebitNormChambre := MyDebit;
                                                                _NbNormChambre    := m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, m_NoPiece][m_NoEA].Nombre;                                                                
                                                                End;
                                                End;

                                        If not(((m_Logement.CalculDn = c_Oui) or (Etude.Batiment.CalculDN = c_Oui))  And (m_Logement.EntreesDAirDnChoisies)) Then
                                        case MyDebit of    //
                                                22: m_NbEA[1] := m_NbEA[1] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, m_NoPiece][m_NoEA].Nombre * _NbEntree;
                                                30: m_NbEA[2] := m_NbEA[2] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, m_NoPiece][m_NoEA].Nombre * _NbEntree;
                                                40: m_NbEA[4] := m_NbEA[4] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, m_NoPiece][m_NoEA].Nombre * _NbEntree;
                                                45: m_NbEA[3] := m_NbEA[3] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, m_NoPiece][m_NoEA].Nombre * _NbEntree;
                                        end;    // case
                                End;
        End;
    End;


if _DebitNormSejour > -1 then
        case _DebitNormSejour of
                22: _LibNormSejour := c_DebitEA[c_DebitEA22];
                30: _LibNormSejour := c_DebitEA[c_DebitEA30];
                40: _LibNormSejour := c_DebitEA[c_DebitEA645];
                45: _LibNormSejour := c_DebitEA[c_DebitEA45];
        end;    // case


if _DebitNormChambre > -1 then
        case _DebitNormChambre of
                22: _LibNormChambre := c_DebitEA[c_DebitEA22];
                30: _LibNormChambre := c_DebitEA[c_DebitEA30];
                40: _LibNormChambre := c_DebitEA[c_DebitEA645];
                45: _LibNormChambre := c_DebitEA[c_DebitEA45];
        end;    // case

if (_NbNormSejour > 1) and (_DebitNormSejour > -1) then
        _LibNormSejour := IntToStr(_NbNormSejour) + 'X ' + _LibNormSejour;
if (_NbNormChambre > 1) and (_DebitNormChambre > -1) then
        _LibNormChambre := IntToStr(_NbNormChambre) + 'X ' + _LibNormChambre;

if _DebitNormSejour2 > -1 then
        case _DebitNormSejour2 of
                22: _LibNormSejour := _LibNormSejour + ' + ' + c_DebitEA[c_DebitEA22];
                30: _LibNormSejour := _LibNormSejour + ' + ' + c_DebitEA[c_DebitEA30];
                40: _LibNormSejour := _LibNormSejour + ' + ' + c_DebitEA[c_DebitEA645];
                45: _LibNormSejour := _LibNormSejour + ' + ' + c_DebitEA[c_DebitEA45];
        end;    // case

    m_Grid.Objects[0, m_NoLogement] := TCelluleImp.Create(m_Logement.Reference, GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    m_Grid.Objects[1, m_NoLogement] := TCelluleImp.Create('T' + IntToSTr(m_Logement.TypeLogement), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    m_Grid.Objects[2, m_NoLogement] := TCelluleImp.Create(m_Logement.Systeme.Reference, GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    m_Grid.Objects[3, m_NoLogement] := TCelluleImp.Create(_LibNormSejour, GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    m_Grid.Objects[4, m_NoLogement] := TCelluleImp.Create(_LibNormChambre, GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    m_Grid.Objects[5, m_NoLogement] := TCelluleImp.Create(IntToStr(m_NbEA[1]), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    m_Grid.Objects[6, m_NoLogement] := TCelluleImp.Create(IntToStr(m_NbEA[2]), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    m_Grid.Objects[7, m_NoLogement] := TCelluleImp.Create(IntToStr(m_NbEA[3]), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    m_Grid.Objects[8, m_NoLogement] := TCelluleImp.Create(IntToStr(m_NbEA[4]), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
  End;

  ApercuImp_AjouteGrille(m_Grid, True, '', False, False, False, True);


End;
{ ************************************************************************************************************************************************* }
Procedure Impression_DonneesEntreesAirANJOS(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Const
  _Title = 'D�termination des entr�es d''air';
  c_ColHeaders : array[0..7] Of String = (' ', ' ', 'Nombre de pi�ces humides', '', ' ', 'Nombre d''entr�es d''air', '', ' ');
  c_ColHeaders2: array[0..7] Of String = ('Type log.', 'Nb log.', 'SdB', 'WC', '22 m�/h', '30 m�/h', '7/40 m�/h', '45 m�/h');
  c_RowHeaders : array[0..3] Of String = ('22 m�/h', '30 m�/h', '7/40 m�/h', '45 m�/h');
Var
  m_Grid          : TStringGrid;
  m_Txt           : String;
  m_Code          : String;
  m_NoCol         : Integer;
  m_NbAuto        : Array[1..7] Of Integer;
  m_NbAutoHumides : Array[1..7, 1..2] Of Integer;
  m_NbAutoEA      : Array[1..7, 1..4] Of Integer;
  m_NbHygro       : Array[1..7] Of Integer;
  m_NbHygroHumides: Array[1..7, 1..2] Of Integer;
  m_NbHygroEA     : Array[1..7, 1..4] Of Integer;
  m_Totaux        : Array[1..4] Of Integer;
  m_TotauxGen     : Array[1..4] Of Integer;
  m_NoLogement: Integer;
  m_Logement  : TVentil_Logement;
  m_NoType: Integer;
  m_No: Integer;
  m_NoEA: Integer;
Begin
  ApercuImp_Debut(_Lanceur, _Title, Etude.Batiment.Reference, GetPiedDePage, Not _ImpressionEnCours);
  m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 8; m_Grid.RowCount := 3;
  For m_NoCol := Low(c_ColHeaders) To High(c_ColHeaders) Do m_Grid.Objects[m_NoCol, 0] := TCelluleImp.Create(c_ColHeaders[m_NoCol], GetFontInfos, 0, ClBlack, ClSkyBlue, [], taCenter);

  TCelluleImp(m_Grid.Objects[2, 0]).IMerged := True;
  TCelluleImp(m_Grid.Objects[5, 0]).IMerged := True;
  TCelluleImp(m_Grid.Objects[6, 0]).IMerged := True;
  TCelluleImp(m_Grid.Objects[4, 0]).IMerged := True;

  For m_NoCol := Low(c_ColHeaders) To High(c_ColHeaders) Do m_Grid.Objects[m_NoCol, 1] := TCelluleImp.Create(c_ColHeaders2[m_NoCol], GetFontInfos, 0, ClBlack, ClSkyBlue, [], taCenter);
  TCelluleImp(m_Grid.Objects[0, 0]).IHorMerged := True;
  TCelluleImp(m_Grid.Objects[1, 0]).IHorMerged := True;
  ApercuImp_AjouteGrille(m_Grid, True, '', False, False, False, True);


  For m_NoType := 1 To 7 Do Begin
    m_NbHygro[m_NoType] := 0;
    m_NbAuto[m_NoType] := 0;
    For m_No := 1 To 2 Do Begin
      m_NbAutoHumides[m_NoType, m_No] := 0;
      m_NbHygroHumides[m_NoType, m_No] := 0;
    End;
    For m_No := 1 To 4 Do Begin
      m_NbAutoEA[m_NoType, m_No] := 0;
      m_NbHygroEA[m_NoType, m_No] := 0;
    End;
  End;
  For m_No := 1 To 4 Do m_TotauxGen[m_No] := 0;

  For m_NoLogement := 0 To Etude.Batiment.Logements.Count - 1 Do Begin
    m_Logement := Etude.Batiment.Logements[m_NoLogement];
    If m_Logement.Systeme <> nil Then Begin
      If m_Logement.Systeme.Hygro Then Begin
        If m_Logement.TypeLogement < 8 Then Begin
          m_NbHygro[m_Logement.TypeLogement] := m_NbHygro[m_Logement.TypeLogement] + m_Logement.nb_Cuisine;
          m_NbHygroHumides[m_Logement.TypeLogement, 1] := m_NbHygroHumides[m_Logement.TypeLogement, 1] + m_Logement.nb_Sdb;
          m_NbHygroHumides[m_Logement.TypeLogement, 2] := m_NbHygroHumides[m_Logement.TypeLogement, 2] + m_Logement.nb_WC;
          if m_Logement.Systeme.AlizeIII and m_Logement.Systeme.HygroB and (m_Logement.TypeLogement = c_TypeT3) and (m_Logement.T3Optimise = c_Oui) then
          Begin
          For m_NoEA := 0 To 1 Do Case m_Logement.Systeme.EASejourT3Opt.Debit Of
            22: m_NbHygroEA[m_Logement.TypeLogement, 1] := m_NbHygroEA[m_Logement.TypeLogement, 1] + m_Logement.Systeme.EASejourT3Opt.Nombre * m_Logement.nb_Cuisine;
            30: m_NbHygroEA[m_Logement.TypeLogement, 2] := m_NbHygroEA[m_Logement.TypeLogement, 2] + m_Logement.Systeme.EASejourT3Opt.Nombre * m_Logement.nb_Cuisine;
            40: m_NbHygroEA[m_Logement.TypeLogement, 3] := m_NbHygroEA[m_Logement.TypeLogement, 3] + m_Logement.Systeme.EASejourT3Opt.Nombre * m_Logement.nb_Cuisine;
//            45: m_NbHygroEA[m_Logement.TypeLogement, 4] := m_NbHygroEA[m_Logement.TypeLogement, 4] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, 1].Nombre;
            45: m_NbHygroEA[m_Logement.TypeLogement, 4] := m_NbHygroEA[m_Logement.TypeLogement, 4] + m_Logement.Systeme.EASejourT3Opt.Nombre * m_Logement.nb_Cuisine;
          End;
          End
          Else
          Begin
          For m_NoEA := 0 To 1 Do Case m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, 1][m_NoEA].Debit Of
            22: m_NbHygroEA[m_Logement.TypeLogement, 1] := m_NbHygroEA[m_Logement.TypeLogement, 1] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, c_Sejour][m_NoEA].Nombre * m_Logement.nb_Cuisine;
            30: m_NbHygroEA[m_Logement.TypeLogement, 2] := m_NbHygroEA[m_Logement.TypeLogement, 2] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, c_Sejour][m_NoEA].Nombre * m_Logement.nb_Cuisine;
            40: m_NbHygroEA[m_Logement.TypeLogement, 3] := m_NbHygroEA[m_Logement.TypeLogement, 3] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, c_Sejour][m_NoEA].Nombre * m_Logement.nb_Cuisine;
//            45: m_NbHygroEA[m_Logement.TypeLogement, 4] := m_NbHygroEA[m_Logement.TypeLogement, 4] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, 1].Nombre;
            45: m_NbHygroEA[m_Logement.TypeLogement, 4] := m_NbHygroEA[m_Logement.TypeLogement, 4] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, c_Sejour][m_NoEA].Nombre * m_Logement.nb_Cuisine;
          End;
          End;
          Case m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, 2][0].Debit Of
            22: m_NbHygroEA[m_Logement.TypeLogement, 1] := m_NbHygroEA[m_Logement.TypeLogement, 1] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, c_Chambre][0].Nombre * (m_Logement.TypeLogement - 1) * m_Logement.nb_Cuisine;
            30: m_NbHygroEA[m_Logement.TypeLogement, 2] := m_NbHygroEA[m_Logement.TypeLogement, 2] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, c_Chambre][0].Nombre * (m_Logement.TypeLogement - 1) * m_Logement.nb_Cuisine;
            40: m_NbHygroEA[m_Logement.TypeLogement, 3] := m_NbHygroEA[m_Logement.TypeLogement, 3] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, c_Chambre][0].Nombre * (m_Logement.TypeLogement - 1) * m_Logement.nb_Cuisine;
//            45: m_NbHygroEA[m_Logement.TypeLogement, 4] := m_NbHygroEA[m_Logement.TypeLogement, 4] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, 2].Nombre;
            45: m_NbHygroEA[m_Logement.TypeLogement, 4] := m_NbHygroEA[m_Logement.TypeLogement, 4] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, c_Chambre][0].Nombre * (m_Logement.TypeLogement - 1) * m_Logement.nb_Cuisine;
          End;
        End;
      End Else Begin
        If m_Logement.TypeLogement < 8 Then Begin
          m_NbAuto[m_Logement.TypeLogement] := m_NbAuto[m_Logement.TypeLogement] + m_Logement.nb_Cuisine;
          m_NbAutoHumides[m_Logement.TypeLogement, 1] := m_NbAutoHumides[m_Logement.TypeLogement, 1] + m_Logement.nb_Sdb;
          m_NbAutoHumides[m_Logement.TypeLogement, 2] := m_NbAutoHumides[m_Logement.TypeLogement, 2] + m_Logement.nb_WC;
          if m_Logement.Systeme.AlizeIII and m_Logement.Systeme.HygroB and (m_Logement.TypeLogement = c_TypeT3) and (m_Logement.T3Optimise = c_Oui) then
          Begin
          For m_NoEA := 0 To 1 Do Case m_Logement.Systeme.EASejourT3Opt.Debit Of
            22: m_NbAutoEA[m_Logement.TypeLogement, 1] := m_NbAutoEA[m_Logement.TypeLogement, 1] + m_Logement.Systeme.EASejourT3Opt.Nombre * m_Logement.nb_Cuisine;
            30: m_NbAutoEA[m_Logement.TypeLogement, 2] := m_NbAutoEA[m_Logement.TypeLogement, 2] + m_Logement.Systeme.EASejourT3Opt.Nombre * m_Logement.nb_Cuisine;
            40: m_NbAutoEA[m_Logement.TypeLogement, 3] := m_NbAutoEA[m_Logement.TypeLogement, 3] + m_Logement.Systeme.EASejourT3Opt.Nombre * m_Logement.nb_Cuisine;
            45: m_NbAutoEA[m_Logement.TypeLogement, 4] := m_NbAutoEA[m_Logement.TypeLogement, 4] + m_Logement.Systeme.EASejourT3Opt.Nombre * m_Logement.nb_Cuisine;
          End;
          End
          Else
          Begin
          For m_NoEA := 0 To 1 Do Case m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, 1][m_NoEA].Debit Of
            22: m_NbAutoEA[m_Logement.TypeLogement, 1] := m_NbAutoEA[m_Logement.TypeLogement, 1] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, c_Sejour][m_NoEA].Nombre * m_Logement.nb_Cuisine;
            30: m_NbAutoEA[m_Logement.TypeLogement, 2] := m_NbAutoEA[m_Logement.TypeLogement, 2] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, c_Sejour][m_NoEA].Nombre * m_Logement.nb_Cuisine;
            40: m_NbAutoEA[m_Logement.TypeLogement, 3] := m_NbAutoEA[m_Logement.TypeLogement, 3] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, c_Sejour][m_NoEA].Nombre * m_Logement.nb_Cuisine;
            45: m_NbAutoEA[m_Logement.TypeLogement, 4] := m_NbAutoEA[m_Logement.TypeLogement, 4] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, c_Sejour][m_NoEA].Nombre * m_Logement.nb_Cuisine;
          End;
          End;
          Case m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, 2][0].Debit Of
            22: m_NbAutoEA[m_Logement.TypeLogement, 1] := m_NbAutoEA[m_Logement.TypeLogement, 1] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, c_Chambre][0].Nombre * (m_Logement.TypeLogement - 1) * m_Logement.nb_Cuisine;
            30: m_NbAutoEA[m_Logement.TypeLogement, 2] := m_NbAutoEA[m_Logement.TypeLogement, 2] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, c_Chambre][0].Nombre * (m_Logement.TypeLogement - 1) * m_Logement.nb_Cuisine;
            40: m_NbAutoEA[m_Logement.TypeLogement, 3] := m_NbAutoEA[m_Logement.TypeLogement, 3] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, c_Chambre][0].Nombre * (m_Logement.TypeLogement - 1) * m_Logement.nb_Cuisine;
            45: m_NbAutoEA[m_Logement.TypeLogement, 4] := m_NbAutoEA[m_Logement.TypeLogement, 4] + m_Logement.Systeme.EntreeAir[m_Logement.TypeLogement, c_Chambre][0].Nombre * (m_Logement.TypeLogement - 1) * m_Logement.nb_Cuisine;
          End;
        End;
      End;
    End;
  End;

  m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 8; m_Grid.RowCount := 1;
  For m_No := 1 To 4 Do m_Totaux[m_No] := 0;
  For m_NoType := 1 To 7 Do If m_NbAuto[m_NoType] > 0 Then Begin
    m_Grid.RowCount := m_Grid.RowCount + 1;
    m_Grid.Objects[0, m_Grid.RowCount - 1] := TCelluleImp.Create('T' + IntToSTr(m_NoType), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    m_Grid.Objects[1, m_Grid.RowCount - 1] := TCelluleImp.Create(IntToStr(m_NbAuto[m_NoType]), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    m_Grid.Objects[2, m_Grid.RowCount - 1] := TCelluleImp.Create(IntToStr(m_NbAutoHumides[m_NoType, 1]), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    m_Grid.Objects[3, m_Grid.RowCount - 1] := TCelluleImp.Create(IntToStr(m_NbAutoHumides[m_NoType, 2]), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);

    m_Grid.Objects[4, m_Grid.RowCount - 1] := TCelluleImp.Create(IntToStr(m_NbAutoEA[m_NoType, 1]), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    m_Grid.Objects[5, m_Grid.RowCount - 1] := TCelluleImp.Create(IntToStr(m_NbAutoEA[m_NoType, 2]), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    m_Grid.Objects[6, m_Grid.RowCount - 1] := TCelluleImp.Create(IntToStr(m_NbAutoEA[m_NoType, 3]), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    m_Grid.Objects[7, m_Grid.RowCount - 1] := TCelluleImp.Create(IntToStr(m_NbAutoEA[m_NoType, 4]), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    For m_No := 1 To 4 Do Begin
      m_Totaux[m_No] := m_Totaux[m_No] + m_NbAutoEA[m_NoType, m_No];
      m_TotauxGen[m_No] := m_TotauxGen[m_No] + m_NbAutoEA[m_NoType, m_No];
    End;
  End;
  m_Grid.RowCount := m_Grid.RowCount + 1;
  m_Grid.Objects[0, m_Grid.RowCount - 1] := TCelluleImp.Create('Total logements autor�glables', GetFontInfos, 0, ClBlack, clCream, [], taLeftJustify);
  For m_No := 0 To 2 Do Begin
    If m_no > 0 Then m_Grid.Objects[m_No, m_Grid.RowCount - 1] := TCelluleImp.Create('', GetFontInfos, 0, ClBlack, clCream, [], taLeftJustify);
    TCelluleImp(m_Grid.Objects[m_No, m_Grid.RowCount - 1]).ISeparateurDroite := False;
  End;
  TCelluleImp(m_Grid.Objects[0, m_Grid.RowCount - 1]).IMerged := True;
  TCelluleImp(m_Grid.Objects[1, m_Grid.RowCount - 1]).IMerged := True;
  For m_No := 1 To 4 Do m_Grid.Objects[m_No + 3, m_Grid.RowCount - 1] := TCelluleImp.Create(IntToStr(m_Totaux[m_No]), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
  If m_Grid.RowCount > 1 Then ApercuImp_AjouteGrille(m_Grid, True, 'Autor�glable', False, False, False, True);
  m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 8; m_Grid.RowCount := 1;
  For m_No := 1 To 4 Do m_Totaux[m_No] := 0;
  For m_NoType := 1 To 7 Do If m_NbHygro[m_NoType] > 0 Then Begin
    m_Grid.RowCount := m_Grid.RowCount + 1;
    m_Grid.Objects[0, m_Grid.RowCount - 1] := TCelluleImp.Create('T' + IntToSTr(m_NoType), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    m_Grid.Objects[1, m_Grid.RowCount - 1] := TCelluleImp.Create(IntToStr(m_NbHygro[m_NoType]), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    m_Grid.Objects[2, m_Grid.RowCount - 1] := TCelluleImp.Create(IntToStr(m_NbHygroHumides[m_NoType, 1]), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    m_Grid.Objects[3, m_Grid.RowCount - 1] := TCelluleImp.Create(IntToStr(m_NbHygroHumides[m_NoType, 2]), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);

    m_Grid.Objects[4, m_Grid.RowCount - 1] := TCelluleImp.Create(IntToStr(m_NbHygroEA[m_NoType, 1]), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    m_Grid.Objects[5, m_Grid.RowCount - 1] := TCelluleImp.Create(IntToStr(m_NbHygroEA[m_NoType, 2]), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    m_Grid.Objects[6, m_Grid.RowCount - 1] := TCelluleImp.Create(IntToStr(m_NbHygroEA[m_NoType, 3]), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    m_Grid.Objects[7, m_Grid.RowCount - 1] := TCelluleImp.Create(IntToStr(m_NbHygroEA[m_NoType, 4]), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    For m_No := 1 To 4 Do Begin
      m_Totaux[m_No] := m_Totaux[m_No] + m_NbHygroEA[m_NoType, m_No];
      m_TotauxGen[m_No] := m_TotauxGen[m_No] + m_NbHygroEA[m_NoType, m_No];
    End;
  End;
  m_Grid.RowCount := m_Grid.RowCount + 1;
  m_Grid.Objects[0, m_Grid.RowCount - 1] := TCelluleImp.Create('Total logements Hygror�glables', GetFontInfos, 0, ClBlack, clCream, [], taLeftJustify);
  For m_No := 0 To 2 Do Begin
    If m_no > 0 Then m_Grid.Objects[m_No, m_Grid.RowCount - 1] := TCelluleImp.Create('', GetFontInfos, 0, ClBlack, clCream, [], taLeftJustify);
    TCelluleImp(m_Grid.Objects[m_No, m_Grid.RowCount - 1]).ISeparateurDroite := False;
  End;
  TCelluleImp(m_Grid.Objects[0, m_Grid.RowCount - 1]).IMerged := True;
  TCelluleImp(m_Grid.Objects[1, m_Grid.RowCount - 1]).IMerged := True;
  For m_No := 1 To 4 Do m_Grid.Objects[m_No + 3, m_Grid.RowCount - 1] := TCelluleImp.Create(IntToStr(m_Totaux[m_No]), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
  If m_Grid.RowCount > 1 Then ApercuImp_AjouteGrille(m_Grid, True, 'Hygror�glable', False, False, False, True);
  { Totaux g�n�raux }
  m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 8; m_Grid.RowCount := 1;
  m_Grid.Objects[0, m_Grid.RowCount - 1] := TCelluleImp.Create('Total g�n�ral', GetFontInfos, 0, ClBlack, clSkyBlue, [], taLeftJustify);
  For m_No := 0 To 2 Do Begin
    If m_no > 0 Then m_Grid.Objects[m_No, m_Grid.RowCount - 1] := TCelluleImp.Create(' ', GetFontInfos, 0, ClBlack, clSkyBlue, [], taLeftJustify);
    TCelluleImp(m_Grid.Objects[m_No, m_Grid.RowCount - 1]).ISeparateurDroite := False;
  End;
  m_Grid.Objects[3, m_Grid.RowCount - 1] := TCelluleImp.Create(' ', GetFontInfos, 0, ClBlack, clSkyBlue, [], taCenter);
  For m_No := 1 To 4 Do m_Grid.Objects[m_No + 3, m_Grid.RowCount - 1] := TCelluleImp.Create(IntToStr(m_TotauxGen[m_No]), GetFontInfos, 0, ClBlack, clSkyBlue, [], taCenter);
  ApercuImp_AjouteGrille(m_Grid, True, '', False, False, False, True);
  { Modules correspondants }
  ApercuImp_SauteLigne(1);
  m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 3; m_Grid.RowCount := 4;
  For m_No := 0 To 3 Do Begin
    m_Grid.Objects[0, m_No] := TCelluleImp.Create(c_RowHeaders[m_No] , GetFontInfos, 20, ClBlack, clWhite, [], taCenter);
    If m_TotauxGen[m_No + 1] > 0 Then Begin
      m_txt  := '';
      m_Code := '';
      Case m_No Of
        0: Begin
          m_Txt := 'ISOLA 2 41 22 + RA + CE2A Entd''air auto + RAcous + Capuchon';
          m_Code := '300902';
        End;
        1: Begin
          m_Txt := 'ISOLA 2 41 30 + RA + CE2A Entd''air auto + RAcous + Capuchon';
          m_Code := '300903';
        End;
        2: Begin
          m_Txt := 'ISOLA HY 7 40 + RA + CEA Ent d''air hygro + RAcous + Capuchon';
          m_Code := '300911';
        End;
        3: Begin
          m_Txt := 'ISOLA 2 39 45 + RA + CE2A Entd''air auto + RAcous + Capuchon'; //
          m_Code := '300595';
        End;
      End;
      m_Grid.Objects[1, m_No] := TCelluleImp.Create(m_Code, GetFontInfos, 10, ClBlack, clWhite, [], taCenter);
      m_Grid.Objects[2, m_No] := TCelluleImp.Create(m_Txt, GetFontInfos, 80, ClBlack, clWhite, [], taLeftJustify);
    End;
  End;
  ApercuImp_AjouteGrille(m_Grid, True, 'Modules employ�s', False, False, False, True);
//  If Not _ImpressionEnCours Then ApercuImp_Show;

End;
{ ************************************************************************************************************************************************* }
Procedure NewCurLine(var LineNum: SmallInt; var G: TStringGrid);
Begin
  Inc(LineNum);
  G.RowCount := G.RowCount + 1;
End;
{ ************************************************************************************************************************************************* }
Procedure GetBoucheCuisineDefav(_Objet : TVentil_Troncon; Var BoucheDefav : TVentil_Bouche; Var HasCuisineVol0 : Boolean);
Var
        m_NoFils     : Integer;
        LpGBoucheFav : Single;
        LpG_Objet    : Single;
        VolumePiece  : Single;
Begin
if Etude.Batiment.IsFabricantVIM then
begin
        IF HasCuisineVol0 then
                exit;
                
        If _Objet.InheritsFrom(TVentil_Bouche) and (TVentil_Bouche(_Objet).TypePiece = c_Cuisine) then
                Begin

                        If IsNotNil(TVentil_Bouche(_Objet).Logement) then
                                Begin
                                VolumePiece := TVentil_Bouche(_Objet).Logement.InfosPieces[TVentil_Bouche(_Objet).TypePiece, 1] * TVentil_Bouche(_Objet).Logement.InfosPieces[TVentil_Bouche(_Objet).TypePiece, 2];
                                        If VolumePiece = 0 then
                                                Begin
                                                BoucheDefav    := Nil;
                                                HasCuisineVol0 := True;
                                                exit;
                                                End;
                                End
                        else
                                Begin
                                BoucheDefav    := Nil;
                                HasCuisineVol0 := True;
                                exit;
                                End;

                        If BoucheDefav = nil then
                                BoucheDefav := TVentil_Bouche(_Objet)
                        Else
                                Begin
                                LpG_Objet    := TVentil_Bouche(_Objet).GetLPGlobal;
                                LpGBoucheFav := BoucheDefav.GetLPGlobal;
                                        If LpG_Objet > LpGBoucheFav then
                                                BoucheDefav := TVentil_Bouche(_Objet)
                                        Else
                                                Begin
                                                End;
                                End;
                End;


        IF HasCuisineVol0 then
                exit
        else
        For m_NoFils := 0 To _Objet.FilsCalcul.Count - 1 Do
                GetBoucheCuisineDefav(_Objet.FilsCalcul[m_NoFils], BoucheDefav, HasCuisineVol0);
end;
End;
{ ************************************************************************************************************************************************* }
Procedure Impression_ObjetCalcul(Const _Objet: TVentil_Troncon; Var _Grid: TStringGrid; _Horizontal: Boolean;Var _Debute: Boolean;Var _DpQMax, _DpQMin: Real);
Var
  m_2Print: Boolean;
  m_NoFils: Integer;
  m_DeltaQMax : Real;
  m_DeltaQMin : Real;
  TempReference : String;
  AfficheMin   : Real;
  AfficheMax   : Real;
  TempReglage  : Real;
  MyBoucheDefav : TVentil_Bouche;
  MyHasCuisineVol0 : boolean;
  add1 : Integer;
Begin

        if (Etude.Batiment.IsFabricantVIM and not(_Objet.InheritsFrom(TVentil_Caisson)) and not(_Objet.InheritsFrom(TVentil_Accident))) or not(Etude.Batiment.IsFabricantVIM) then
                Begin
                _DPQMax := _DPQMax - abs(_Objet.PdcTotaleMaxi);
                _DPQMin := _DPQMin - abs(_Objet.PdcTotaleMini);
                End;

        if Etude.Batiment.IsFabricantVIM and _Objet.InheritsFrom(TVentil_Registre) then
                Begin
{
                _DPQMax := _DPQMax - 150;
                _DPQMin := _DPQMin - 150 *_Objet.DebitMini / power(_Objet.DebitMaxi, 2);
}                
                End;


if Etude.Batiment.IsFabricantVIM then
begin
//on retire 50 Pa pour les t� souches insonoris�s
        if _Objet.InheritsFrom(TVentil_TeSouche) and TVentil_TeSouche(_Objet).IAero_Fredo_TSouche_GetInsonorise then
                begin
//                _DPQMax := _DPQMax - 50;
//                _DPQMin := _DPQMin - 50;
                end;
End;


  m_DeltaQMax := _DpQMax;
  m_DeltaQMin := _DpQMin;

   if not(_Objet.InheritsFrom(TVentil_Bouche)) then
        Begin
        AfficheMax := _DpQMax;
        AfficheMin := _DpQMin;
        End

   Else

        Begin
        AfficheMax := _DpQMax;
        AfficheMin := _DpQMin;
        End;



if Etude.Batiment.IsFabricantVIM then
        Add1 := 1
else
        Add1 := 0;

  _Debute := (_Objet Is TVentil_TeSouche) And Not _Horizontal;
  If Not _Horizontal And (_Objet Is TVentil_TeSouche) Then Begin
    _Grid.RowCount := _Grid.RowCount + 1;
    _Grid.Objects[0, _Grid.RowCount - 1] := TCelluleImp.Create('', GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    For m_NoFils := 1 To 7 + Add1 Do Begin
      _Grid.Objects[m_NoFils, _Grid.RowCount - 1] := TCelluleImp.Create('', GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
      TCelluleImp(_Grid.Objects[m_NoFils - 1, _Grid.RowCount - 1]).IMerged := True;
    End;
    TCelluleImp(_Grid.Objects[3, _Grid.RowCount - 1]).ITexte := 'Colonne: ' + _Objet.Reference;
  End;
  If _Horizontal Then m_2Print := (_Objet Is TVentil_Caisson) Or (_Objet Is TVentil_Bifurcation) Or (_Objet Is TVentil_TeSouche) Or (_Objet Is TVentil_Registre)
  Else m_2Print := _Debute And (_Objet Is TVentil_Bifurcation) Or (_Objet Is TVentil_TeSouche) Or (_Objet Is TVentil_Collecteur) Or (_Objet Is TVentil_Bouche);
  If m_2Print Then Begin
    _Grid.RowCount := _Grid.RowCount + 1;

if Etude.Batiment.IsFabricantVIM then
Begin
    TempReference := '';
        if _Objet.InheritsFrom(TVentil_Caisson) and (TVentil_Caisson(_Objet).ProduitAssocie <> nil) then
                TempReference := TVentil_Caisson(_Objet).ProduitAssocie.Reference
        else
        If _Objet.InheritsFrom(TVentil_Bouche) and (TVentil_Bouche(_Objet).TypePiece < c_TypeT7) Then
                Begin

                        if TVentil_Bouche(_Objet).Logement = nil then
                                TempReference := TVentil_Bouche(_Objet).Reference
                        Else
                        If (TVentil_Bouche(_Objet).TypePiece = c_Cuisine) and (TVentil_Bouche(_Objet).Logement.TypeLogement = c_TypeT3) And (TVentil_Bouche(_Objet).Logement.Systeme.Hygro) And (TVentil_Bouche(_Objet).Logement.Systeme.Gaz = false) And (TVentil_Bouche(_Objet).Logement.Systeme.AlizeIII) And (TVentil_Bouche(_Objet).Logement.T3Optimise = c_Oui) Then
                                TempReference := TVentil_Bouche(_Objet).Logement.Systeme.BoucheCuisT3Opt.Libelle
                        else
                                TempReference := TVentil_Bouche(_Objet).Logement.Systeme.Bouche[TVentil_Bouche(_Objet).Logement.TypeLogement, TVentil_Bouche(_Objet).TypePiece].Libelle;
                End
        else
        if _Objet.InheritsFrom(TVentil_Bouche) and (TVentil_Bouche(_Objet).TypePiece = c_ChoixBouche) And (TVentil_Bouche(_Objet).BoucheSelection <> nil)  then
                TempReference := TVentil_Bouche(_Objet).BoucheSelection.Reference
        else
        if _Objet.InheritsFrom(TVentil_Collecteur) and (TVentil_Collecteur(_Objet).Logement <> nil) then
                TempReference := _Objet.Reference + ' (' + TVentil_Collecteur(_Objet).Logement.Reference + ')'
        else
                TempReference := _Objet.Reference;
End
Else
begin
                TempReference := _Objet.Reference;
end;

if Etude.Batiment.IsFabricantVIM then
begin
        If (_Objet.InheritsFrom(TVentil_Bouche)) and (_Objet.LienCad <> nil) and (TVentil_Bouche(_Objet).Logement <> nil) and (TVentil_Bouche(_Objet).TypePiece = c_Cuisine) then
                Begin
                MyBoucheDefav := Nil;
                MyHasCuisineVol0 := False;
                GetBoucheCuisineDefav(Etude.Batiment.Reseaux.GetCaisson, MyBoucheDefav, MyHasCuisineVol0);
                        If MyHasCuisineVol0 then
                                _Grid.Objects[6, _Grid.RowCount - 1] := TCelluleImp.Create(' ', GetFontInfos, 0, ClBlack, clWhite, [], taCenter)
                        else
                                _Grid.Objects[6, _Grid.RowCount - 1] := TCelluleImp.Create(Float2Str(TVentil_Bouche(_Objet).GetLPGlobal, 0), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
                End;
End;

    _Grid.Objects[0, _Grid.RowCount - 1] := TCelluleImp.Create(TempReference, GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
        if Etude.Batiment.IsFabricantVIM and not(Etude.SaveOptions_SelectionCaisson.CheckBox_DTU_Checked) then
            Begin
            _Grid.Objects[1, _Grid.RowCount - 1] := TCelluleImp.Create(' ', GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
            _Grid.Objects[2, _Grid.RowCount - 1] := TCelluleImp.Create(' ', GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
            End
        else
            Begin
            _Grid.Objects[1, _Grid.RowCount - 1] := TCelluleImp.Create(IntToStr(_Objet.DebitMini), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
            _Grid.Objects[2, _Grid.RowCount - 1] := TCelluleImp.Create(Float2Str(AfficheMin, 0), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
            End;
    _Grid.Objects[3, _Grid.RowCount - 1] := TCelluleImp.Create(IntToStr(_Objet.DebitMaxi), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    _Grid.Objects[4, _Grid.RowCount - 1] := TCelluleImp.Create(Float2Str(AfficheMax, 0), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
    _Grid.Objects[5, _Grid.RowCount - 1] := TCelluleImp.Create('', GetFontInfos, 0, ClBlack, clWhite, [], taCenter);

        if _Objet.InheritsFrom(TVentil_Caisson) and (TVentil_Caisson(_Objet).ProduitAssocie <> nil) then
                Begin
                        If TVentil_Caisson(_Objet).ProduitAssocie.Entrainement = c_Edibatec_Caisson_Entrainement_Direct Then
                                _Grid.Objects[6 + Add1, _Grid.RowCount - 1] := TCelluleImp.Create(TVentil_Caisson(_Objet).ProduitAssocie.Position, GetFontInfos, 0, ClBlack, clWhite, [], taCenter)
                        Else
                        if TVentil_Caisson(_Objet).ProduitAssocie.TypeSelection = c_CaissonSelect_DebitCst then
                                _Grid.Objects[6 + Add1, _Grid.RowCount - 1] := TCelluleImp.Create(TVentil_Caisson(_Objet).ProduitAssocie.Position, GetFontInfos, 0, ClBlack, clWhite, [], taCenter)
                        Else
                                _Grid.Objects[6 + Add1, _Grid.RowCount - 1] := TCelluleImp.Create(TVentil_Caisson(_Objet).ProduitAssocie.Position + ' tr/min', GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
                End
        else
if Etude.Batiment.IsFabricantVIM then
begin
                if _Objet.InheritsFrom(TVentil_Bouche) and (TVentil_Bouche(_Objet).TypePiece = c_ChoixBouche) and (TVentil_Bouche(_Objet).BoucheSelection <> nil) and (TVentil_Bouche(_Objet).isDebitReglable ) then
                        Begin
                        TempReglage := TVentil_Bouche(_Objet).GetReglage(AfficheMax);
                                If TempReglage > -100 then
                                        _Grid.Objects[7, _Grid.RowCount - 1] := TCelluleImp.Create(Float2Str(TempReglage, 1), GetFontInfos, 0, ClBlack, clWhite, [], taCenter)
                                Else
                                        _Grid.Objects[7, _Grid.RowCount - 1] := TCelluleImp.Create('', GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
                        End
                Else
                if _Objet.InheritsFrom(TVentil_Registre) and (TVentil_Registre(_Objet).RegistreSelection <> nil) then
                        Begin
                        TempReglage := TVentil_Registre(_Objet).GetReglage(AfficheMax);
                                If TempReglage > -100 then
                                        _Grid.Objects[7, _Grid.RowCount - 1] := TCelluleImp.Create(Float2Str(TempReglage, 1), GetFontInfos, 0, ClBlack, clWhite, [], taCenter)
                                Else
                                        _Grid.Objects[7, _Grid.RowCount - 1] := TCelluleImp.Create('', GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
                        End
                Else
                        _Grid.Objects[7, _Grid.RowCount - 1] := TCelluleImp.Create('', GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
end
else
begin
                        _Grid.Objects[6, _Grid.RowCount - 1] := TCelluleImp.Create('', GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
end;

    _Grid.Objects[7 + Add1, _Grid.RowCount - 1] := TCelluleImp.Create(Float2Str(_Objet.Vitesse, 1), GetFontInfos, 0, ClBlack, clWhite, [], taCenter);
  End;
  If _Horizontal And (_Objet Is TVentil_TeSouche) Then Exit
  Else Begin
    For m_NoFils := 0 To _Objet.FilsCalcul.Count - 1 Do
        Begin
        Impression_ObjetCalcul(_Objet.FilsCalcul[m_NoFils], _Grid, _Horizontal, _Debute, _DpQMax, _DpQMin);
                If (_Objet Is TVentil_Bifurcation) Or (_Objet Is TVentil_Collecteur) Or (_Objet Is TVentil_Caisson) Then Begin
                        _DpQMin := m_DeltaQMin;
                        _DpQMax := m_DeltaQMax;
                End;
        End;
  End;
End;
{ ************************************************************************************************************************************************* }
Procedure SetDataImp_ObjetCalcul(Const _Objet: TVentil_Troncon;Const _Horizontal: Boolean; Var _Debute: Boolean;Var _DpQMax, _DpQMin: Real);
Var
  m_2Print: Boolean;
  m_NoFils: Integer;
  m_DeltaQMax : Real;
  m_DeltaQMin : Real;
  TempReference : String;
  AfficheMin   : Real;
  AfficheMax   : Real;
  m_Txt       : String;
  TempReglage : Real;
  Col0 : String;
  Col1 : String;
  Col2 : String;
  Col3 : String;
  Col4 : String;
  Col5 : String;
  Col6 : String;
  Col7 : String;
  Col8 : String;
  MyBoucheDefav : TVentil_Bouche;
  MyHasCuisineVol0 : Boolean;
Begin

//  m_DeltaQMax := _DpQMax;
//  m_DeltaQMin := _DpQMin;
        if not(_Objet.InheritsFrom(TVentil_Caisson)) and not(_Objet.InheritsFrom(TVentil_Accident)) then
                Begin
                _DPQMax := _DPQMax - abs(_Objet.PdcTotaleMaxi);
                _DPQMin := _DPQMin - abs(_Objet.PdcTotaleMini);
                End;


if Etude.Batiment.IsFabricantVIM then
begin
//on retire 50 Pa pour les t� souches insonoris�s
        if _Objet.InheritsFrom(TVentil_TeSouche) and TVentil_TeSouche(_Objet).IAero_Fredo_TSouche_GetInsonorise then
                begin
//                _DPQMax := _DPQMax - 50;
//                _DPQMin := _DPQMin - 50;
                end;
End;

  m_DeltaQMax := _DpQMax;
  m_DeltaQMin := _DpQMin;

   if not(_Objet.InheritsFrom(TVentil_Bouche)) then
        Begin
        AfficheMax := _DpQMax;
        AfficheMin := _DpQMin;
        End

   Else

        Begin
        AfficheMax := _DpQMax;
        AfficheMin := _DpQMin;
        End;


  _Debute := (_Objet Is TVentil_TeSouche) And Not _Horizontal;

  If Not _Horizontal And (_Objet Is TVentil_TeSouche) Then
    Etude.DataImp_CalculRetour.Add(cst_TxtGras + 'Colonne: ' + _Objet.Reference);
    
  If _Horizontal Then m_2Print := (_Objet Is TVentil_Caisson) Or (_Objet Is TVentil_Bifurcation) Or (_Objet Is TVentil_TeSouche)
  Else m_2Print := _Debute And (_Objet Is TVentil_Bifurcation) Or (_Objet Is TVentil_TeSouche) Or (_Objet Is TVentil_Collecteur) Or (_Objet Is TVentil_Bouche);
  If m_2Print Then Begin

    TempReference := '';
        if _Objet.InheritsFrom(TVentil_Caisson) and (TVentil_Caisson(_Objet).ProduitAssocie <> nil) then
                TempReference := TVentil_Caisson(_Objet).ProduitAssocie.Reference
        else
        If _Objet.InheritsFrom(TVentil_Bouche) and (TVentil_Bouche(_Objet).TypePiece < c_TypeT7) Then
                Begin

                        if TVentil_Bouche(_Objet).Logement = nil then
                                TempReference := TVentil_Bouche(_Objet).Reference
                        Else
                        If (TVentil_Bouche(_Objet).TypePiece = c_Cuisine) and (TVentil_Bouche(_Objet).Logement.TypeLogement = c_TypeT3) And (TVentil_Bouche(_Objet).Logement.Systeme.Hygro) And (TVentil_Bouche(_Objet).Logement.Systeme.Gaz = false) And (TVentil_Bouche(_Objet).Logement.Systeme.AlizeIII) And (TVentil_Bouche(_Objet).Logement.T3Optimise = c_Oui) Then
                                TempReference := TVentil_Bouche(_Objet).Logement.Systeme.BoucheCuisT3Opt.Libelle
                        else
                                TempReference := TVentil_Bouche(_Objet).Logement.Systeme.Bouche[TVentil_Bouche(_Objet).Logement.TypeLogement, TVentil_Bouche(_Objet).TypePiece].Libelle;
                End
        else
        if _Objet.InheritsFrom(TVentil_Bouche) and (TVentil_Bouche(_Objet).TypePiece = c_ChoixBouche) and (TVentil_Bouche(_Objet).BoucheSelection <> nil) then
                TempReference := TVentil_Bouche(_Objet).BoucheSelection.Reference
        else
        if _Objet.InheritsFrom(TVentil_Collecteur) and (TVentil_Collecteur(_Objet).Logement <> nil) then
                TempReference := _Objet.Reference + ' (' + TVentil_Collecteur(_Objet).Logement.Reference + ')'
        else
                TempReference := _Objet.Reference;


if Etude.Batiment.IsFabricantVIM then
begin
        If (_Objet.InheritsFrom(TVentil_Bouche)) and (_Objet.LienCad <> nil) and (TVentil_Bouche(_Objet).Logement <> nil) {and (TVentil_Bouche(_Objet).Logement.EntreesDAirDnChoisies)} and (TVentil_Bouche(_Objet).TypePiece = c_Cuisine) then
                Begin
                MyBoucheDefav := Nil;
                MyHasCuisineVol0 := False;
                GetBoucheCuisineDefav(Etude.Batiment.Reseaux.GetCaisson, MyBoucheDefav, MyHasCuisineVol0);
                        If MyHasCuisineVol0 then
                                Col6 := ''
                        else
                                Col6 := Float2Str(TVentil_Bouche(_Objet).GetLPGlobal, 0);
                End;
end;


    Col0 := TempReference;
        if not(Etude.SaveOptions_SelectionCaisson.CheckBox_DTU_Checked) then
            Begin
            Col1 := '';
            Col2 := '';
            End
        else
            Begin
            Col1 := IntToStr(_Objet.DebitMini);
            Col2 := Float2Str(AfficheMin, 0);
            End;
    Col3 := IntToStr(_Objet.DebitMaxi);
    Col4 := Float2Str(AfficheMax, 0);
    Col5 := '';

        if _Objet.InheritsFrom(TVentil_Caisson) and (TVentil_Caisson(_Objet).ProduitAssocie <> nil) then
                Begin
                        If TVentil_Caisson(_Objet).ProduitAssocie.Entrainement = c_Edibatec_Caisson_Entrainement_Direct Then
                                Col7 := TVentil_Caisson(_Objet).ProduitAssocie.Position
                        Else
                        if TVentil_Caisson(_Objet).ProduitAssocie.TypeSelection = c_CaissonSelect_DebitCst then
                                Col7 := TVentil_Caisson(_Objet).ProduitAssocie.Position
                        Else
                                Col7 := TVentil_Caisson(_Objet).ProduitAssocie.Position + ' tr/min';
                End
        else

if Etude.Batiment.IsFabricantVIM then
begin
                if _Objet.InheritsFrom(TVentil_Bouche) and (TVentil_Bouche(_Objet).BoucheSelection <> nil) and (TVentil_Bouche(_Objet).isDebitReglable ) then
                        Begin
                        TempReglage := TVentil_Bouche(_Objet).GetReglage(AfficheMax);
                                If TempReglage > -100 then
                                        Col7 := Float2Str(TVentil_Bouche(_Objet).GetReglage(AfficheMax), 1)
                                Else
                                        Col7 := '';
                        End
                Else
                if _Objet.InheritsFrom(TVentil_Registre) and (TVentil_Registre(_Objet).RegistreSelection <> nil) then
                        Begin
                        TempReglage := TVentil_Registre(_Objet).GetReglage(AfficheMax);
                                If TempReglage > -100 then
                                        Col7 := Float2Str(TVentil_Registre(_Objet).GetReglage(AfficheMax), 1)
                                Else
                                        Col7 := '';
                        End
                Else
                        Col7 := '';
end
else
begin
                        Col7 := '';
end;

    Col8 := Float2Str(_Objet.Vitesse, 1);

    m_Txt := 'L9' + Col0 + cst_ImpSep;
    m_Txt := m_Txt + Col1 + cst_ImpSep;
    m_Txt := m_Txt + Col2 + cst_ImpSep;
    m_Txt := m_Txt + Col3 + cst_ImpSep;
    m_Txt := m_Txt + Col4 + cst_ImpSep;
    m_Txt := m_Txt + Col5 + cst_ImpSep;
    m_Txt := m_Txt + Col6 + cst_ImpSep;
    m_Txt := m_Txt + Col7 + cst_ImpSep;
    m_Txt := m_Txt + Col8 + cst_ImpSep;
    Etude.DataImp_CalculRetour.Add(m_Txt);

  End;


  If _Horizontal And (_Objet Is TVentil_TeSouche) Then Exit
  Else Begin
    For m_NoFils := 0 To _Objet.FilsCalcul.Count - 1 Do
        Begin
        SetDataImp_ObjetCalcul(_Objet.FilsCalcul[m_NoFils], _Horizontal, _Debute, _DpQMax, _DpQMin);
                If (_Objet Is TVentil_Bifurcation) Or (_Objet Is TVentil_Collecteur) Or (_Objet Is TVentil_Caisson) Then Begin
                        _DpQMin := m_DeltaQMin;
                        _DpQMax := m_DeltaQMax;
                End;
        End;
  End;
End;
{ ************************************************************************************************************************************************* }
Procedure SetDataImp_CalculRetour;
Var
  m_Debute: Boolean;
  m_Ventil: TEdibatec_Caisson;
  m_DPQMax,
  m_DPQMin : Real;
  tempTitre : String;
Begin
  m_Ventil:= TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie;
  FreeAndNil(Etude.DataImp_CalculRetour);

  If m_Ventil = nil Then Exit;

        If Etude.Batiment.TypeBat = c_BatimentCollectif Then
                tempTitre := 'COLLECTIF'
        Else
                tempTitre := 'TERTIAIRE';

  Etude.DataImp_CalculRetour := TStringList.Create;
  Etude.DataImp_CalculRetour.Add(cst_Titre1 + 'R�sultats du calcul');

  { R�seau vertical }
  m_DpQMax := m_Ventil.DpCalcQMax;
  m_DpQMin := m_Ventil.DpCalcQMin;
  m_Debute := False;
  Etude.DataImp_CalculRetour.Add(cst_Titre2 + 'RESEAU VERTICAL VMC HABITAT ' + tempTitre);
  SetDataImp_ObjetCalcul(Etude.Batiment.Reseaux.GetCaisson, False, m_Debute, m_DPQMax, m_DPQMin);
  { R�seau horizontal }
  m_DpQMax := m_Ventil.DpCalcQMax;
  m_DpQMin := m_Ventil.DpCalcQMin;
  m_Debute := False;
  Etude.DataImp_CalculRetour.Add(cst_Titre2 + 'RESEAU HORIZONTAL VMC HABITAT ' + tempTitre);
  SetDataImp_ObjetCalcul(Etude.Batiment.Reseaux.GetCaisson, True, m_Debute, m_DPQMax, m_DPQMin);
End;
{ ************************************************************************************************************************************************* }
Procedure Impression_CalculRetour(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Const
{$IfDef VIM_OPTAIR}
  c_ColHeaders : Array[0..8] Of String = ('Element', 'D�bit mini (m�/h)', 'Dp � d�bit mini (Pa)', 'D�bit maxi (m�/h)', 'DP � d�bit maxi (Pa)', 'Press equil. (Pa)', 'Lp global', 'R�glage', 'Vitesse maxi (m/s)');
{$Else}
  c_ColHeaders : Array[0..7] Of String = ('Element', 'D�bit mini (m�/h)', 'Dp � d�bit mini (Pa)', 'D�bit maxi (m�/h)', 'DP � d�bit maxi (Pa)', 'Press equil. (Pa)', 'R�glage', 'Vitesse maxi (m/s)');
{$EndIf}
  LargeurColonne0 = 25;
  _Title = 'R�sultats du calcul';
Var
  m_Grid  : TStringGrid;
  m_NoCol : Integer;
  m_Debute: Boolean;
  m_Ventil: TEdibatec_Caisson;
  m_DPQMax,
  m_DPQMin : Real;
  TempLargeur : integer;
  tempTitre : String;
Begin
  m_Ventil:= TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie;
  ApercuImp_Debut(_Lanceur, _Title, Etude.Batiment.Reference, GetPiedDePage, Not _ImpressionEnCours);
  m_DpQMax := m_Ventil.DpCalcQMax;
  m_DpQMin := m_Ventil.DpCalcQMin;
//  m_DpQMin := TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).DPVentilateur.MinAQmin + Etude.DeltaCalcule;

  { R�seau vertical }
  m_Debute := False;
  m_Grid :=TStringGrid.Create(nil);
{$IfDef VIM_OPTAIR}
  m_Grid.ColCount := 9;
{$Else}
  m_Grid.ColCount := 8;
{$EndIf}
  m_Grid.RowCount := 1;

        If Etude.Batiment.TypeBat = c_BatimentCollectif Then
                tempTitre := 'COLLECTIF'
        Else
                tempTitre := 'TERTIAIRE';


  For m_NoCol := Low(c_ColHeaders) To High(c_ColHeaders) Do
        Begin
                {$IfDef VIM_OPTAIR}
                if m_NoCol = 0 then
                        TempLargeur := LargeurColonne0
                else
                {$EndIf}
                        TempLargeur := 0;
        m_Grid.Objects[m_NoCol, 0] := TCelluleImp.Create(c_ColHeaders[m_NoCol], GetFontInfos, TempLargeur, ClBlack, ClSkyBlue, [], taCenter);
        End;
  Impression_ObjetCalcul(Etude.Batiment.Reseaux.GetCaisson, m_Grid, False, m_Debute, m_DPQMax, m_DPQMin);

  ApercuImp_AjouteGrille(m_Grid, True, 'RESEAU VERTICAL VMC HABITAT ' + tempTitre, False, False, False, True);
  { R�seau horizontal }
  m_DpQMax := m_Ventil.DpCalcQMax;
  m_DpQMin := m_Ventil.DpCalcQMin;
//  m_DpQMin := TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).DPVentilateur.MinAQmin + Etude.DeltaCalcule;

  m_Debute := False;
  m_Grid :=TStringGrid.Create(nil);
{$IfDef VIM_OPTAIR}
  m_Grid.ColCount := 9;
{$Else}
  m_Grid.ColCount := 8;
{$EndIf}
  m_Grid.RowCount := 1;
  For m_NoCol := Low(c_ColHeaders) To High(c_ColHeaders) Do
        Begin
                {$IfDef VIM_OPTAIR}
                if m_NoCol = 0 then
                        TempLargeur := LargeurColonne0
                else
                {$EndIf}                
                        TempLargeur := 0;
        m_Grid.Objects[m_NoCol, 0] := TCelluleImp.Create(c_ColHeaders[m_NoCol], GetFontInfos, TempLargeur, ClBlack, ClSkyBlue, [], taCenter);
        End;
  Impression_ObjetCalcul(Etude.Batiment.Reseaux.GetCaisson, m_Grid, True, m_Debute, m_DPQMax, m_DPQMin);


  ApercuImp_AjouteGrille(m_Grid, True, 'RESEAU HORIZONTAL VMC HABITAT ' + tempTitre, False, False, False, True);
End;
{ ************************************************************************************************************************************************* }
Procedure Imprime_AcoustBoucheCouranteVIM(Const _Objet: TVentil_Bouche; _ImpressionEnCours: Boolean; _Lanceur: TForm);
Const
  _Title = 'Calculs acoustiques';
  ColHeaders : Array[1..7] Of string = ('63 Hz', '125 Hz', '250 Hz', '500 Hz', '1 kHz', '2 kHz', '4 kHz') ;
Var

  m_Frequence  : Integer;
  VolumePiece  : Single;
  LpG          : Single;

  ImpTxtVolume : String;
  ImpTxtNRA    : String;
  ArrayL1      : Array[1..7] of Single;
  ArrayL2      : Array[1..7] of Single;
  ArrayL3      : Array[1..7] of Single;
  ArrayL4      : Array[1..7] of Single;
  ArrayL5      : Array[1..7] of Single;
  ArrayL6      : Array[1..7] of Single;
  ArrayL7      : Array[1..7] of Single;
  ArrayL8      : Array[1..7] of Single;
  ArrayL9      : Array[1..7] of Single;
  ArrayL10     : Array[1..7] of Single;


  m_Grid: TStringGrid;
  CurLine    : Integer;
  i          : Integer;
  NoElement  : SmallInt;
  m_Message   : String;
Begin
  ApercuImp_Debut(_Lanceur, _Title, Etude.Batiment.Reference, GetPiedDePage, Not _ImpressionEnCours, False);

  VolumePiece := 0;

        if (_Objet <> nil) and (_Objet.TypePiece = c_cuisine) then
                Begin

                        If IsNotNil(_Objet.Logement) then
                                VolumePiece := TVentil_Bouche(_Objet).Logement.InfosPieces[_Objet.TypePiece, 1] * TVentil_Bouche(_Objet).Logement.InfosPieces[_Objet.TypePiece, 2];

                        For m_Frequence := Low((_Objet.LienCad As IAero_Bouche).IAero_BoucheBase_GetLp) + 1 to High((_Objet.LienCad As IAero_Bouche).IAero_BoucheBase_GetLp) - 1 do
                                Begin
                                ArrayL1[m_Frequence]  := TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).IAero_Fredo_Caisson_GetSpectreAcoustique[m_Frequence];
                                ArrayL2[m_Frequence]  := TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).IAero_Fredo_Caisson_GetSpectreAcoustique[m_Frequence] - (_Objet.LienCad As IAero_Bouche).IAero_BoucheBase_GetLwAmont[m_Frequence];
                                ArrayL3[m_Frequence]  := (_Objet.LienCad As IAero_Bouche).IAero_BoucheBase_GetLwAmont[m_Frequence] - (_Objet.LienCad As IAero_Bouche).IAero_BoucheBase_GetLw[m_Frequence];
                                ArrayL4[m_Frequence]  := 10 * Log10(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.DiametreAspiration / _Objet.Diametre);
                                ArrayL5[m_Frequence]  := ArrayL1[m_Frequence] - (ArrayL2[m_Frequence] + ArrayL3[m_Frequence] + ArrayL4[m_Frequence]);
                                ArrayL6[m_Frequence]  := (_Objet.LienCad As IAero_Bouche).IAero_BoucheBase_GetLw[m_Frequence];
                                ArrayL7[m_Frequence]  := SommeLog10([ArrayL5[m_Frequence], ArrayL6[m_Frequence]], 0, 1);
                                ArrayL8[m_Frequence]  := -10 * Log10(VolumePiece / 12.5);
                                ArrayL9[m_Frequence]  := ct_PonderationA[m_Frequence - 1];
                                ArrayL10[m_Frequence] := ArrayL7[m_Frequence] + ArrayL8[m_Frequence] + ArrayL9[m_Frequence];
                                End;


                m_Grid := TStringGrid.Create(nil);
                m_Grid.ColCount := 1;
                m_Grid.RowCount := 1;
                m_Grid.Objects[0, 0] := TCelluleImp.Create('Calculs effectu�s selon les indications du cahier technique N� 1876 du C.S.T.B.', GetFontInfos, 99, ClBlack, clWhite, [fsbold], taCenter);
                ApercuImp_AjouteGrille(m_Grid, False, '', True, False, False, False);
                ApercuImp_SauteLigne(1);

                m_Grid := TStringGrid.Create(nil);
                // on ne prend pas les fr�quences en limite... d'o� le -2
                m_Grid.ColCount := High((_Objet.LienCad As IAero_Bouche).IAero_BoucheBase_GetLp) - 2 + 1;
                m_Grid.RowCount := 11;
                m_Grid.Objects[0, 0] := TCelluleImp.Create('', GetFontInfos, 100, clBlack, ClSkyBlue, GetFontInfos.Style, tacenter);
                m_Grid.Objects[0, 1] := TCelluleImp.Create('Lw rayonn� en conduit', GetFontInfos, 100, clBlack, ClSkyBlue, GetFontInfos.Style, tacenter);
                m_Grid.Objects[0, 2] := TCelluleImp.Create('Att�nuations du r�seau', GetFontInfos, 100, clBlack, ClSkyBlue, GetFontInfos.Style, tacenter);
                m_Grid.Objects[0, 3] := TCelluleImp.Create('Att. Bouche / R�seau', GetFontInfos, 100, clBlack, ClSkyBlue, GetFontInfos.Style, tacenter);
                m_Grid.Objects[0, 4] := TCelluleImp.Create('10 Log(S1/S2)', GetFontInfos, 100, clBlack, ClSkyBlue, GetFontInfos.Style, tacenter);
                m_Grid.Objects[0, 5] := TCelluleImp.Create('Lw du r�seau = 1 -(2+3+4)', GetFontInfos, 100, clBlack, ClSkyBlue, GetFontInfos.Style, tacenter);
                m_Grid.Objects[0, 6] := TCelluleImp.Create('Lw de la bouche', GetFontInfos, 100, clBlack, ClSkyBlue, GetFontInfos.Style, tacenter);
                m_Grid.Objects[0, 7] := TCelluleImp.Create('Lw TOTAL = SLog(5;6)', GetFontInfos, 100, clBlack, ClSkyBlue, GetFontInfos.Style, tacenter);
                m_Grid.Objects[0, 8] := TCelluleImp.Create('10 Log(V/12.5)', GetFontInfos, 100, clBlack, ClSkyBlue, GetFontInfos.Style, tacenter);
                m_Grid.Objects[0, 9] := TCelluleImp.Create('Correctif --> Db(A)', GetFontInfos, 100, clBlack, ClSkyBlue, GetFontInfos.Style, tacenter);
                m_Grid.Objects[0, 10] := TCelluleImp.Create('Lp Global en dB(A)', GetFontInfos, 100, clBlack, ClSkyBlue, GetFontInfos.Style, tacenter);


                        For m_Frequence := Low((_Objet.LienCad As IAero_Bouche).IAero_BoucheBase_GetLp) + 1 to High((_Objet.LienCad As IAero_Bouche).IAero_BoucheBase_GetLp) - 1 do
                                Begin
                                // on ne prend pas les fr�quences en limite... d'o� le -1
                                m_Grid.Objects[m_Frequence - 1, 0] := TCelluleImp.Create(ColHeaders[m_Frequence], GetFontInfos, 100, clBlack, ClSkyBlue, GetFontInfos.Style, tacenter);
                                m_Grid.Objects[m_Frequence - 1, 1] := TCelluleImp.Create(Float2Str(ArrayL1[m_Frequence] , 0), GetFontInfos, 99, ClBlack, clWhite, [], taCenter);
                                m_Grid.Objects[m_Frequence - 1, 2] := TCelluleImp.Create(Float2Str(ArrayL2[m_Frequence] , 0), GetFontInfos, 99, ClBlack, clWhite, [], taCenter);
                                m_Grid.Objects[m_Frequence - 1, 3] := TCelluleImp.Create(Float2Str(ArrayL3[m_Frequence] , 0), GetFontInfos, 99, ClBlack, clWhite, [], taCenter);
                                m_Grid.Objects[m_Frequence - 1, 4] := TCelluleImp.Create(Float2Str(ArrayL4[m_Frequence] , 0), GetFontInfos, 99, ClBlack, clWhite, [], taCenter);
                                m_Grid.Objects[m_Frequence - 1, 5] := TCelluleImp.Create(Float2Str(ArrayL5[m_Frequence] , 0), GetFontInfos, 99, ClBlack, clWhite, [], taCenter);
                                m_Grid.Objects[m_Frequence - 1, 6] := TCelluleImp.Create(Float2Str(ArrayL6[m_Frequence] , 0), GetFontInfos, 99, ClBlack, clWhite, [], taCenter);
                                m_Grid.Objects[m_Frequence - 1, 7] := TCelluleImp.Create(Float2Str(ArrayL7[m_Frequence] , 0), GetFontInfos, 99, ClBlack, clWhite, [], taCenter);
                                m_Grid.Objects[m_Frequence - 1, 8] := TCelluleImp.Create(Float2Str(ArrayL8[m_Frequence] , 0), GetFontInfos, 99, ClBlack, clWhite, [], taCenter);
                                m_Grid.Objects[m_Frequence - 1, 9] := TCelluleImp.Create(Float2Str(ArrayL9[m_Frequence] , 0), GetFontInfos, 99, ClBlack, clWhite, [], taCenter);
                                m_Grid.Objects[m_Frequence - 1, 10] := TCelluleImp.Create(Float2Str(ArrayL10[m_Frequence] , 0), GetFontInfos, 99, ClBlack, clWhite, [], taCenter);
                             End;

                ApercuImp_AjouteGrille(m_Grid, True, '', False, False, False, True);

                m_Grid := TStringGrid.Create(nil);
                m_Grid.ColCount := 1;
                m_Grid.RowCount := 1;

                LpG := SommeLog10(ArrayL10, Low(ArrayL10) + 1, High(ArrayL10) - 1);

                m_Grid.Objects[0, 0] := TCelluleImp.Create('Lp Global en dB(A) : ' + Float2Str(LpG, 0) + ' dB(A)', GetFontInfos, 99, ClBlack, clWhite, [fsbold], taCenter);
                ApercuImp_AjouteGrille(m_Grid, True, '', False, False, False, True);
                ApercuImp_SauteLigne(1);

                                If IsNotNil(_Objet.Logement) then
                                        Begin

                                        m_Grid := TStringGrid.Create(nil);
                                        m_Grid.ColCount := 1;
                                        m_Grid.RowCount := 1;

                                        ImpTxtVolume := 'Volume de la cuisine ';
                                                If _Objet.Logement.CuisineFermee = C_Oui then
                                                        ImpTxtVolume := ImpTxtVolume + 'ferm�e : '
                                                Else
                                                        ImpTxtVolume := ImpTxtVolume + 'ouverte : ';

                                        ImpTxtVolume := ImpTxtVolume + Float2Str(VolumePiece, 0) + ' m3.';

                                        m_Grid.Objects[0, 0] := TCelluleImp.Create(ImpTxtVolume, GetFontInfos, 99, ClBlack, clWhite, [fsbold], taCenter);

                                        ApercuImp_AjouteGrille(m_Grid, False, '', True, False, False, False);
                                        ApercuImp_SauteLigne(1);
                                        End
                                Else
                                        ImpTxtNRA := '';
                                        If IsNotNil(_Objet.Logement) then
                                                Begin
                                                        If (_Objet.Logement.CuisineFermee = C_Oui) And (LpG <= 35) then
                                                                ImpTxtNRA := 'Cette installation REPOND aux exigences de la N.R.A.'
                                                        Else
                                                        If (_Objet.Logement.CuisineFermee = C_Non) And (LpG <= 30) then
                                                                ImpTxtNRA := 'Cette installation REPOND aux exigences de la N.R.A.'
                                                        Else
                                                                ImpTxtNRA := 'Cette installation NE REPOND PAS aux exigences de la N.R.A.';

                                                End
                                        Else
                                                ImpTxtNRA := cst_TxtGras + 'Cette installation NE REPOND PAS aux exigences de la N.R.A.';

                                m_Grid := TStringGrid.Create(nil);
                                m_Grid.ColCount := 1;
                                m_Grid.RowCount := 1;
                                m_Grid.Objects[0, 0] := TCelluleImp.Create(ImpTxtNRA, GetFontInfos, 99, ClBlack, clWhite, [fsbold], taCenter);
                                ApercuImp_AjouteGrille(m_Grid, True, '', False, False, False, True);
                                ApercuImp_SauteLigne(1);

                                m_Grid := TStringGrid.Create(nil);
                                m_Grid.ColCount := 1;
                                m_Grid.RowCount := 1;
                                m_Grid.Objects[0, 0] := TCelluleImp.Create('Le Lw rayonn� en conduit par le ventilateur a �t� estim� suivant les indications du cahier technique N� 1876 du C.S.T.B.', GetFontInfos, 99, ClBlack, clWhite, [fsbold], taCenter);
                                ApercuImp_AjouteGrille(m_Grid, False, '', True, False, False, False);
                End;


  If Not _ImpressionEnCours Then ApercuImp_Show;



End;
{ ************************************************************************************************************************************************* }
Procedure Imprime_AcoustBoucheCouranteANJOS(Const _Bouche: TVentil_Bouche; Var _Ligne: Integer; Var _Grid: TStringGrid);
Var
  m_Log : TVentil_Logement;
  m_Col : TVentil_Collecteur;
  m_TeS : TVentil_Troncon;
  m_Text: String;
  m_Sys : TVentil_Systeme;
  m_No  : Integer;
  m_Lw  : Integer;
  m_Zero: Boolean;
  m_Tab : TTabAcoustique;
  m_Supp: Integer;
  m_Ind : Integer;
  m_LpG : Real;
  m_LwG : Real;
  m_ESA : Integer;
  m_DecalReverb : integer;
Begin
  _Ligne := _Ligne + 1; _Grid.RowCount := _Grid.RowCount + 1;
  m_Log := _Bouche.Logement; If m_Log = nil Then Exit;
  _Grid.Objects[0, _Ligne] := TCelluleImp.Create(m_Log.Reference, GetFontInfos);
  m_Col := _Bouche.Collecteur; If m_Col = nil Then Exit;
  m_TeS := m_Col.ParentCalcul; While (m_TeS <> nil) And (Not m_TeS.InheritsFrom(TVentil_TeSouche)) Do m_TeS := m_TeS.ParentCalcul;
  If m_TeS = nil Then Exit;
  m_Text := 'Col: ' + m_TeS.Reference + #13 + 'Local: ' + _Bouche.Reference;
        if (_Bouche.TypePiece < c_ChoixBouche) then
                Begin
                m_Text := m_Text + #13 + c_NomsPiecesBouche[_Bouche.TypePiece];
                        if _Bouche.Logement <> nil then

                                m_Text := m_Text + #13 + 'Etage: ' + IntToStr(_Bouche.LienCad.IAero_Vilo_Base_Etage);
                End;
  _Grid.Objects[1, _Ligne]   := TCelluleImp.Create(m_Text, GetFontInfos, 0, clBlack, clWhite, [], taLeftJustify);
  m_Sys := _Bouche.Systeme;
  If m_Sys <> nil Then Begin
    If _Bouche.TypePiece = c_Cellier Then _Grid.Objects[2, _Ligne] := TCelluleImp.Create(m_Sys.BoucheCellier[m_Log.TypeLogement].Libelle, GetFontInfos)
    Else Begin
      If _Bouche.TypePiece <= c_WCMultiple Then
        Begin
                If (_Bouche.TypePiece = c_Cuisine) and (_Bouche.Logement.TypeLogement = c_TypeT3) And (_Bouche.Logement.Systeme.Hygro) And (_Bouche.Logement.Systeme.Gaz = false) And (_Bouche.Logement.Systeme.AlizeIII) And (_Bouche.Logement.T3Optimise = c_Oui) Then
                        _Grid.Objects[2, _Ligne] := TCelluleImp.Create(_Bouche.Logement.Systeme.BoucheCuisT3Opt.Libelle, GetFontInfos)
                else
                        _Grid.Objects[2, _Ligne] := TCelluleImp.Create(m_Sys.Bouche[m_Log.TypeLogement, _Bouche.TypePiece].Libelle, GetFontInfos);
        End
      Else Begin
        If _Bouche.Reference <> '' Then _Grid.Objects[2, _Ligne] := TCelluleImp.Create(_Bouche.Reference, GetFontInfos)
                                   Else _Grid.Objects[2, _Ligne] := TCelluleImp.Create(' *** ', GetFontInfos);
      End;
    End;
  End Else _Grid.Objects[2, _Ligne] := TCelluleImp.Create(_Bouche.Reference, GetFontInfos);

  If Not _Bouche.TypePiece In [c_Cuisine..c_WCMultiple] Then
    For m_No := 3 To _Grid.ColCount - 1 Do _Grid.Objects[m_No , _Ligne] := TCelluleImp.Create('non calcul�', GetFontInfos, 0, clBlack, $00E1E1E1)
  Else Begin
    m_DecalReverb := 1;
    _Grid.Objects[3, _Ligne] := TCelluleImp.Create(C_Caisson_TypeCaisson[TVentil_Caisson(_Bouche.Caisson).TypeCaisson], GetFontInfos);
    { On remplit les donn�es acoust calcul�es }
    _Grid.Objects[3 + m_DecalReverb, _Ligne] := TCelluleImp.Create(Float2Str(m_Log.Tr_Cuisine, 1), GetFontInfos);

    If _Bouche.LienCad <> nil Then Begin
      m_Tab := (_Bouche.LienCad As IAero_Bouche).IAero_BoucheBase_GetLw;
      m_LwG := (_Bouche.LienCad As IAero_Bouche).IAero_BoucheBase_GetLwG;
      m_LpG := (_Bouche.LienCad As IAero_Bouche).IAero_BoucheBase_GetLpG;
      m_Zero := True;
      For m_Lw := Low(m_Tab) To High(m_Tab) Do If m_Tab[m_Lw] <> 0 Then Begin
        m_Zero := False;
        Break;
      End;
      If _Bouche.TypePiece = c_Cuisine Then m_Supp := 30
                                       Else m_Supp := 35;
      If Not m_Zero Then Begin
        If m_LpG > 25 Then _Grid.Objects[_Grid.ColCount - 5, _Ligne] := TCelluleImp.Create(Float2Str(m_LpG, 0), GetFontInfos)
                      Else _Grid.Objects[_Grid.ColCount - 5, _Ligne] := TCelluleImp.Create('< 25', GetFontInfos);
      End Else For m_Ind := 5 To 6 Do _Grid.Objects[m_ind, _Ligne]  := TCelluleImp.Create(' *** ', GetFontInfos, 0, clBlack, $00E1E1E1);
      If m_LwG <> 0 Then _Grid.Objects[_Grid.ColCount - 6, _Ligne]  := TCelluleImp.Create(Float2Str(m_LwG, 0), GetFontInfos)
                             Else _Grid.Objects[_Grid.ColCount - 6, _Ligne]  := TCelluleImp.Create(' *** ', GetFontInfos, 0, clBlack, $00E1E1E1);
      If m_LpG <= m_Supp Then _Grid.Objects[_Grid.ColCount - 4, _Ligne] := TCelluleImp.Create('oui', GetFontInfos)
                         Else _Grid.Objects[_Grid.ColCount - 4, _Ligne] := TCelluleImp.Create('non', GetFontInfos);
      { Dne }
      If (m_Sys <> nil) And (m_Log.TypeLogement <= c_TypeT7) And (_Bouche.TypePiece < c_Cellier) Then
        _Grid.Objects[_Grid.ColCount - 2, _Ligne] := TCelluleImp.Create(IntToStr(m_Sys.Bouche[Min(m_Log.TypeLogement, c_TypeT7), _Bouche.TypePiece]._Dnew), GetFontInfos);
{ Valeurs ESA }
      _Bouche.CalculESA;
      m_ESA := _Bouche.ESA;
      If (_Bouche.TypePiece = c_WC) Or (m_ESA = -1) Then Begin
        _Grid.Objects[_Grid.ColCount - 3, _Ligne] := TCelluleImp.Create('pas d''exigence', GetFontInfos, 0, clBlack, $00E1E1E1);
        _Grid.Objects[_Grid.ColCount - 2, _Ligne] := TCelluleImp.Create('pas d''exigence', GetFontInfos, 0, clBlack, $00E1E1E1);
        _Grid.Objects[_Grid.ColCount - 1, _Ligne] := TCelluleImp.Create('pas d''exigence', GetFontInfos, 0, clBlack, $00E1E1E1);
      End Else Begin
        _Grid.Objects[_Grid.ColCount - 3, _Ligne] := TCelluleImp.Create(c_TABESA[m_Esa], GetFontInfos);
        _Grid.Objects[_Grid.ColCount - 2, _Ligne] := TCelluleImp.Create(IntToSTr(Dnew[m_Esa]), GetFontInfos);
        _Grid.Objects[_Grid.ColCount - 1, _Ligne] := TCelluleImp.Create(IntToSTr(DnewNC[m_Esa]), GetFontInfos);
      End;
    End;
  End;
End;
{ ************************************************************************************************************************************************* }
Procedure Impression_AcoustiqueVIM(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Var
  m_Ventil         : TEdibatec_Caisson;
  MyBoucheDefav    : TVentil_Bouche;
  MyHasCuisineVol0 : Boolean;
Begin

  m_Ventil:= TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie;
  If m_Ventil = nil Then Exit;

  MyBoucheDefav := nil;
  MyHasCuisineVol0 := False;
  GetBoucheCuisineDefav(Etude.Batiment.Reseaux.GetCaisson, MyBoucheDefav, MyHasCuisineVol0);

        if (MyBoucheDefav = nil) or (MyHasCuisineVol0) then
                exit;  

  Imprime_AcoustBoucheCouranteVIM(MyBoucheDefav, _ImpressionEnCours, _Lanceur);
End;
{ ************************************************************************************************************************************************** }
Procedure Impression_AcoustiqueANJOS(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Const
  _Title = 'R�sultats acoustiques des bouches';
  TitresColonnesL1 : Array[0..10] Of String = ('', 'Bouche', '', '  ', ' ',
                                              ' ', ' ', ' ', ' ', 'Isolement acoustique bouche' + #13 + 'Dn,e,w + C (dB) � obtenir', '');
  TitresColonnesL2 : Array[0..10] Of String = ('Logement', 'Position', 'Type', 'Ventilateur', 'Tps de r�verb�ration (s)', 'Lw calcul�* (dB(A))',
                                               'Lp calcul�* (dB(A))', 'Conformit� Lp/NRA**', 'Classe ESA requise pour Dn', 'Si certifi�e', 'Si non certifi�e');
Var
  m_Grid: TStringGrid;
  CurLine    : Integer;
  i          : Integer;
  NoElement  : SmallInt;
  m_Message   : String;
Begin
  ApercuImp_Debut(_Lanceur, _Title, Etude.Batiment.Reference, GetPiedDePage, Not _ImpressionEnCours, False);

  ImpressionHautDePageAnjos;


  m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := Length(TitresColonnesL1); m_Grid.RowCount := 3;
  For i := Low(TitresColonnesL1) To High(TitresColonnesL1) Do m_Grid.Objects[i, 0] := TCelluleImp.Create(TitresColonnesL1[i], GetFontInfos, 0, clBlack, $00E1E1E1);
  For i := Low(TitresColonnesL2) To High(TitresColonnesL2) Do m_Grid.Objects[i, 1] := TCelluleImp.Create(TitresColonnesL2[i], GetFontInfos, 0, clBlack, $00E1E1E1);
  TCelluleImp(m_Grid.Objects[1 , 0]).IMerged := True;
  TCelluleImp(m_Grid.Objects[m_Grid.Colcount - 2, 0]).IMerged := True;
  TCelluleImp(m_Grid.Objects[0, 0]).IHorMerged := True;
  For i := 3 To m_Grid.Colcount - 3 Do TCelluleImp(m_Grid.Objects[i, 0]).IHorMerged := True;
  CurLine := 1;
  For NoElement := 0 To Etude.Batiment.Composants.Count - 1 Do If Etude.Batiment.Composants[NoElement].InheritsFrom(TVentil_Bouche) Then
    Imprime_AcoustBoucheCouranteANJOS(TVentil_Bouche(Etude.Batiment.Composants[NoElement]), CurLine, m_Grid);
  ApercuImp_AjouteGrille(m_Grid, True, '', False, False, False, True);

  { Message final }
  ApercuImp_SauteLigne(1);
  m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 1; m_Grid.RowCount := 1;
  m_Message := '*Les Lw et Lp donn�s sont maximum. Ils sont calcul�s pour une d�pression � la bouche de 160 Pa (ou 140 Pa pour les bouches gaz). Les valeurs peuvent �tre inf�rieures pour une d�pression inf�rieure.';
  m_Message := m_Message + #10 + #13 +
              '** Dans le cas de WC et salles de bain, les valeurs de Lp sont donn�es � titre indicatif, elles sont compar�es � l''exigence de 35 dB(A) de la NRA';
  m_Grid.Objects[0, 0] := TCelluleImp.Create(m_Message, GetFontInfos, 0, ClRed, clWhite, [fsItalic], taLeftJustify);
  m_Grid.RowHeights[0] := 40;
  ApercuImp_AjouteGrille(m_Grid, False, '', True, False, False, False);

  ImpressionBasDePageAnjos;

  If Not _ImpressionEnCours Then ApercuImp_Show;
End;
{ ************************************************************************************************************************************************** }
Procedure Impression_Acoustique(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Begin
        if Etude.Batiment.IsFabricantVIM then
                Impression_AcoustiqueVIM(_ImpressionEnCours, _Lanceur)
        else
                Impression_AcoustiqueANJOS(_ImpressionEnCours, _Lanceur);
End;
{ ************************************************************************************************************************************************** }
Procedure Impression_AcoustFacade_Logement(Log: TVentil_Logement; Var LigneDep: SmallInt; Grid: TStringGrid);
Var
  NoPiece   : SmallInt;
  NoLgt     : SmallInt;
  NumChambre: SmallInt;
//  Infos     : TInfos_EntreesFacade;
  Infos     : TVentil_DonneesEA;
  EASejour  : SmallInt;
  EAChambre : SmallInt;
  NoSejour  : SmallInt;
  NoChambre : SmallInt;
  Sys       : TVentil_Systeme;
  _Col      : integer;
  _BrushColor : integer;
  ConditionImpression : Boolean;
Begin

if Etude.Batiment.IsFabricantVIM then
        ConditionImpression := ((Log.CalculDn = c_Oui) or (Etude.Batiment.CalculDN = c_Oui))  and (Log.EntreesDAirDnChoisies)
else
        ConditionImpression := ((Log.CalculDn = c_Oui))  and (Log.EntreesDAirDnChoisies);

  If ConditionImpression Then
  Begin
    NewCurLine(LigneDep, Grid);
    Grid.Objects[0, LigneDep] := TCelluleImp.Create(Log.Reference, GetFontInfos, 0, clBlack, $00FF8000);
        for _Col := 1 to Grid.ColCount - 1 do
                Grid.Objects[_Col, LigneDep] := TCelluleImp.Create(' ', GetFontInfos, 0, clBlack, $00FF8000);
    NumChambre := 1;
    NoLgt      := Min(C_TypeT6, Log.TypeLogement);
    Sys        := Log.Systeme;
        if Sys.AlizeIII and Sys.HygroB and (Log.TypeLogement = c_TypeT3) and (Log.T3Optimise = c_Oui) then
                EASejour   := Sys.EASejourT3Opt.Nombre
        Else
                EASejour   := Sys.EntreeAir[NoLgt, 1][0].Nombre;
    EAChambre  := Sys.EntreeAir[NoLgt, 2][0].Nombre;

    NewCurLine(LigneDep, Grid);
    NoSejour  := 1;
    NoChambre := 1;
    For NoPiece := 0 To Log.Ventil_EntreesAirLogement[NoLgt].Count - 1 Do
    Begin
      Infos := TVentil_DonneesEA(Log.Ventil_EntreesAirLogement[NoLgt][NoPiece]);
      _BrushColor := clWhite;
      Case Infos.Piece Of
        1:
        Begin
          If (NoSejour = 1) Then
          Begin
          _BrushColor := ClSkyBlue;
            Grid.Objects[0, LigneDep] := TCelluleImp.Create('S�jour', GetFontInfos, 0, clBlack, _BrushColor);
            Grid.Objects[1, LigneDep] := TCelluleImp.Create(Float2Str(Infos.SurfacePiece, 0), GetFontInfos, 0, clBlack, _BrushColor);
            Grid.Objects[2, LigneDep] := TCelluleImp.Create(Float2Str(Infos.HauteurPiece, 2), GetFontInfos, 0, clBlack, _BrushColor);
                for _Col := 3 to Grid.ColCount - 1 do
                        Grid.Objects[_Col, LigneDep] := TCelluleImp.Create(' ', GetFontInfos, 0, clBlack, _BrushColor);
            If EASejour > 1 Then
            Begin
              NewCurLine(LigneDep, Grid);
              _BrushColor := clWhite;
              Grid.Objects[0, LigneDep] := TCelluleImp.Create('Entr�e n� ' + IntToStr(NoSejour), GetFontInfos, 0, clBlack, _BrushColor);
            End;
          End Else
          Begin
            _BrushColor := clWhite;
            Grid.Objects[0, LigneDep] := TCelluleImp.Create('Entr�e n� ' + IntToStr(NoSejour), GetFontInfos, 0, clBlack, _BrushColor);
          End;
        End;
        2:
        Begin
          If (NoChambre = 1) Then
          Begin
            _BrushColor := ClSkyBlue;
            Grid.Objects[0, LigneDep] := TCelluleImp.Create('Chambre  n� ' + IntToStr(NumChambre), GetFontInfos, 0, clBlack, _BrushColor);
            Inc(NumChambre);
            Grid.Objects[1, LigneDep] := TCelluleImp.Create(Float2Str(Infos.SurfacePiece, 0), GetFontInfos, 0, clBlack, _BrushColor);
            Grid.Objects[2, LigneDep] := TCelluleImp.Create(Float2Str(Infos.HauteurPiece, 2), GetFontInfos, 0, clBlack, _BrushColor);
            If EaChambre > 1 Then
            Begin
              NewCurLine(LigneDep, Grid);
              _BrushColor := clWhite;
              Grid.Objects[0, LigneDep] := TCelluleImp.Create('Entr�e n� ' + IntToStr(NoChambre), GetFontInfos, 0, clBlack, _BrushColor);
            End;
          End Else
          Begin
            _BrushColor := clWhite;
            Grid.Objects[0, LigneDep] := TCelluleImp.Create('Entr�e n� ' + IntToStr(NoChambre), GetFontInfos, 0, clBlack, _BrushColor);
          End;
          If NoChambre = EAChambre Then NoChambre := 1
                                   Else Inc(NoChambre);
        End;
      End;
      Grid.Objects[3, LigneDep] := TCelluleImp.Create(Float2Str(Infos.Cl_Facade, 0), GetFontInfos, 0, clBlack, _BrushColor);
      Grid.Objects[4, LigneDep] := TCelluleImp.Create(Float2Str(Infos.Isol_facade, 0), GetFontInfos, 0, clBlack, _BrushColor);
      Grid.Objects[5, LigneDep] := TCelluleImp.Create(IntToStr(Calc_Dnew(Infos.Cl_Facade, Infos.Isol_facade, Infos.HauteurPiece, Infos.SurfacePiece, Infos.NbDansPiece)), GetFontInfos, 0, clBlack, _BrushColor);
      Case Infos.EntreeChoisie.Pose Of
        CPoseMenuiserie: Grid.Objects[6, LigneDep] := TCelluleImp.Create('Menuiserie', GetFontInfos, 0, clBlack, _BrushColor);
        CPoseCoffre    : Grid.Objects[6, LigneDep] := TCelluleImp.Create('Coffre de volet roulant', GetFontInfos, 0, clBlack, _BrushColor);
        CPoseMaconnerie: Grid.Objects[6, LigneDep] := TCelluleImp.Create('Ma�onnerie', GetFontInfos, 0, clBlack, _BrushColor);
      End;
      Grid.Objects[7, LigneDep] := TCelluleImp.Create(Infos.EntreeChoisie.Reference, GetFontInfos, 0, clBlack, _BrushColor);
      If NoSejour = EASejour Then NoSejour := 1
                             Else Inc(NoSejour);
      NewCurLine(LigneDep, Grid);
    End;
  End;

End;
{ ************************************************************************************************************************************************** }
Procedure Impression_AcoustFacade(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Const
  TitresColonnes : Array[0..7] Of String = ('-', 'Surface (m�)', 'Hauteur (m)', 'Classement fa�ade dB(A)', 'Isolement sans entr�es d''air dB(A)',
                                            'Dn,e (dB)', 'Pose', 'Entr�e d''air');
  _Title = 'R�sultats acoustiques fa�ade';
Var
  m_Grid: TStringGrid;
  CurLine    : SmallInt;
  i          : SmallInt;
  NoLogement : SmallInt;
Begin
  ApercuImp_Debut(_Lanceur, _Title, Etude.Batiment.Reference, GetPiedDePage, Not _ImpressionEnCours, True);

  {$IfDef ANJOS_OPTIMA3D}
  ImpressionHautDePageAnjos;
  {$EndIf}

  m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 8; m_Grid.RowCount := 50;

  For i := Low(TitresColonnes) To High(TitresColonnes) Do m_Grid.Objects[i, 0] := TCelluleImp.Create(TitresColonnes[i], GetFontInfos, 0, clBlack, $00E1E1E1, [fsBold]);

  CurLine := 0;
  For NoLogement := 0 To Etude.Batiment.Logements.Count - 1 Do Impression_AcoustFacade_Logement(Etude.Batiment.Logements[NoLogement], CurLine, m_Grid);

  ApercuImp_AjouteGrille(m_Grid, True, '', False, False, False, False);

  {$IfDef ANJOS_OPTIMA3D}
  ImpressionBasDePageAnjos;
  {$EndIf}

  If Not _ImpressionEnCours Then ApercuImp_Show;
End;
{ ************************************************************************************************************************************************* }
Procedure ImpressionCartoucheCourbeFctVIM;
Var
  m_Grid   : TStringGrid;
  m_Caisson: TVentil_Caisson;
Begin
  m_Caisson := TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson);
  if m_Caisson = nil then exit;
  m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 4; m_Grid.RowCount := 3;
  m_Grid.Objects[0, 0] := TCelluleImp.Create('R�seau', GetFontInfos, 0, ClBlack, $00E1E1E1, [fsBold], taLeftJustify);
  m_Grid.Objects[0, 0] := TCelluleImp.Create('R�f�rence', GetFontInfos, 0, ClBlack, $00E1E1E1, [fsBold], taLeftJustify);
  m_Grid.Objects[1, 0] := TCelluleImp.Create(m_Caisson.Reference, GetFontInfos);
  m_Grid.Objects[2, 0] := TCelluleImp.Create('Syst�me', GetFontInfos, 0, ClBlack, $00E1E1E1, [fsBold], taLeftJustify);

  If Etude.Batiment.SystemeCollectif <> Nil Then Begin
        m_Grid.Objects[3, 0] := TCelluleImp.Create(Etude.Batiment.SystemeCollectif.Reference, GetFontInfos);
  End Else m_Grid.Objects[3, 0] := TCelluleImp.Create('*****', GetFontInfos);
  m_Grid.Objects[0, 1] := TCelluleImp.Create('PdC mini', GetFontInfos, 0, ClBlack, $00E1E1E1, [fsBold], taLeftJustify);

  m_Grid.Objects[1, 1] := TCelluleImp.Create(Float2Str(m_Caisson.Reseau_PdCTotaleMinimum, 1) + ' Pa', GetFontInfos);
  m_Grid.Objects[2, 1] := TCelluleImp.Create('PdC maxi', GetFontInfos, 0, ClBlack, $00E1E1E1, [fsBold], taLeftJustify);
  m_Grid.Objects[3, 1] := TCelluleImp.Create(Float2Str(m_Caisson.Reseau_PdCTotaleMaximum, 1) + ' Pa', GetFontInfos);
  m_Grid.Objects[0, 2] := TCelluleImp.Create('Fonctionnement pour d�bit mini', GetFontInfos, 0, ClBlack, $00E1E1E1, [fsBold], taLeftJustify);


  m_Grid.Objects[1, 2] := TCelluleImp.Create(Float2Str(m_Caisson.DPVentilateur.MaxAQmin, 1) + ' Pa - ' +
                                              IntToStr(m_Caisson.DebitMini) + ' m3/h', GetFontInfos);

  m_Grid.Objects[2, 2] := TCelluleImp.Create('Fonctionnement pour d�bit maxi', GetFontInfos, 0, ClBlack, $00E1E1E1, [fsBold], taLeftJustify);

  m_Grid.Objects[3, 2] := TCelluleImp.Create(Float2Str(m_Caisson.DPVentilateur.MinAQmax, 1) + ' Pa - ' +
                                              Float2Str( m_Caisson.DebitMaxi, 0) + ' m3/h', GetFontInfos);

  ApercuImp_AjouteGrille(m_Grid, True, '', False, False, False, True);
End;
{ ************************************************************************************************************************************************* }
Procedure ImpressionCartoucheCourbeFctANJOS;
Var
  m_Grid   : TStringGrid;
  m_Caisson: TVentil_Caisson;
  temptext : String;
Begin
  m_Caisson := TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson);
  if m_Caisson = nil then exit;
  m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 3; m_Grid.RowCount := 3;
  {$IfDef CALADAIR_VMC}
        if m_Caisson.ProduitAssocie <> Nil then
          m_Grid.Objects[0, 0] := TCelluleImp.Create('R�f�rence : ' + m_Caisson.ProduitAssocie.Reference, GetFontInfos, 0, ClBlack, $00E1E1E1, [], taLeftJustify)        
        else
  {$EndIf}
  m_Grid.Objects[0, 0] := TCelluleImp.Create('R�f�rence : ' + m_Caisson.Reference, GetFontInfos, 0, ClBlack, $00E1E1E1, [], taLeftJustify);
  If Etude.Batiment.SystemeCollectif <> Nil Then Begin
        temptext := Etude.Batiment.SystemeCollectif.Reference;
  End Else temptext := '*****';
  m_Grid.Objects[0, 1] := TCelluleImp.Create('Syst�me : ' + temptext, GetFontInfos, 0, ClBlack, $00E1E1E1, [], taLeftJustify);


  m_Grid.Objects[0, 2] := TCelluleImp.Create('PdC mini : ' + Float2Str(m_Caisson.Reseau_PdCVentilMinimum - 160, 1) + ' Pa', GetFontInfos, 0, ClBlack, $00E1E1E1, [], taLeftJustify);
  m_Grid.Objects[0, 3] := TCelluleImp.Create('PdC maxi : ' + Float2Str(m_Caisson.Reseau_PdCTotaleMaximum, 1) + ' Pa', GetFontInfos, 0, ClBlack, $00E1E1E1, [], taLeftJustify);
  m_Grid.Objects[1, 0] := TCelluleImp.Create('Fonctionnement pour d�bit mini : ' + Float2Str(m_Caisson.Reseau_PdCVentilMinimum, 1) + ' Pa - ' +
                                              IntToStr(m_Caisson.Reseau_DebitMinimum) + ' m3/h', GetFontInfos, 0, ClBlack, clNone, [], taLeftJustify);

  m_Grid.Objects[1, 1] := TCelluleImp.Create('Fonctionnement pour d�bit maxi : ' + Float2Str(m_Caisson.Reseau_PdCVentilMaximum, 1) + ' Pa - ' +
                                              Float2Str(m_Caisson.Reseau_DebitMaximum, 0) + ' m3/h', GetFontInfos, 0, ClBlack, clNone, [], taLeftJustify);



  If Etude.Batiment.MethodeCalcul = c_CalculDTU Then
    temptext := 'M�thode de calcul conforme au DTU 68.1'
  Else
    temptext := 'Vitesse horizontale maxi : ' + Float2Str(Etude.Batiment.VitesseHorizontale, 0) + 'm/s' + #13#10 + 'Vitesse verticale maxi : ' + Float2Str(Etude.Batiment.VitesseVerticale, 0) + 'm/s';
  m_Grid.Objects[1, 2] := TCelluleImp.Create(temptext, GetFontInfos, 0, ClBlack, clNone, [], taLeftJustify);

  m_Grid.Objects[2, 0] := TCelluleImp.Create('Masse volumique de l''air: 1,2 kg/m3', GetFontInfos, 0, ClBlack, $00E1E1E1, [], taLeftJustify);
  m_Grid.Objects[2, 1] := TCelluleImp.Create('Taux de fuite du r�seau: ' + IntToStr(Etude.Batiment.DebitFuite) + ' %', GetFontInfos, 0, ClBlack, $00E1E1E1, [], taLeftJustify);
  m_Grid.Objects[2, 2] := TCelluleImp.Create('Pertes de charge des entr�es d''air: 20 Pa', GetFontInfos, 0, ClBlack, $00E1E1E1, [], taLeftJustify);


  ApercuImp_AjouteGrille(m_Grid, True, '', False, False, False, True);
End;
{ ************************************************************************************************************************************************* }
Procedure ImpressionCartoucheCourbeFct;
Begin
        if Etude.Batiment.IsFabricantVIM then
                ImpressionCartoucheCourbeFctVIM
        else
                ImpressionCartoucheCourbeFctANJOS;
End;
{ ************************************************************************************************************************************************* }
Function DrawCourbeBezier(_Stl: TStringList; _Rect: TGmRect) : TBitmap;
Var
  NoBezier  : SmallInt;
  CurBezier : TStringList;
  DepX      : Integer;
  DepY      : Integer;

  Haut   : Real;
  Gauche : Real;
  Bas    : Real;
  Droite : Real;

Begin
  Result := TBitmap.Create;

  Droite := _Rect.Right;
  Gauche := _Rect.Left;
  Haut := _Rect.Top;
  Bas := _Rect.Bottom;

  Result.Width  := Round(Droite - Gauche);
  Result.Height := Round(Bas - Haut);
  DepX := 50;
  Depy := Round(Result.Height) - 20;
  For NoBezier := 0 To _Stl.Count - 1 Do
  Begin
    CurBezier := StrToStringList(_Stl[noBezier], ';');
    Case StrToInt(CurBezier[0]) Of
      c_Courbe_Line   : Begin
        Result.Canvas.Pen.Color := StringToColor((CurBezier[3]));
        {$IfDef VIM_OPTAIR}
        Result.Canvas.Pen.Width := StringToColor(CurBezier[4]);
        {$EndIf}
        Result.Canvas.LineTo (Depx + StrToInt(CurBezier[1]), DepY - StrToInt(CurBezier[2]));
      End;
      c_Courbe_Move   : Result.Canvas.MoveTo (Depx + StrToInt(CurBezier[1]), DepY - StrToInt(CurBezier[2]));
      c_Courbe_Text   : Begin
        Result.Canvas.Pen.Color := StringToColor(CurBezier[4]);
        {$IfDef VIM_OPTAIR}
        Result.Canvas.Font.Size := StringToColor(CurBezier[5]);
        Result.Canvas.Font.Style := [fsBold];
        {$EndIf}
        Result.Canvas.TextOut(Depx + StrToInt(CurBezier[1]) - 30 , DepY - StrToInt(CurBezier[2]) + 10, CurBezier[3]);
      End;
      c_Courbe_Ellips : Begin
        Result.Canvas.Pen.Color := StringToColor(CurBezier[5]);
        {$IfDef VIM_OPTAIR}
        Result.Canvas.Pen.Width := StringToColor(CurBezier[6]);
        {$EndIf}        
        Result.Canvas.Ellipse(Depx + StrToInt(CurBezier[1]), DepY - StrToInt(CurBezier[2]), depX + StrToInt(CurBezier[3]), DepY - StrToInt(CurBezier[4]));
      End;
    End;
    FreeAndNil(CurBezier);
  End;
End;
{ ************************************************************************************************************************************************* }
Procedure ImpressionCourbeBezier(_Stl: TStringList; _Rect: TGmRect);
Var
  Bmp : TBitmap;

  Haut   : Real;
  Gauche : Real;
  Bas    : Real;
  Droite : Real;
Begin

  Droite := _Rect.Right;
  Gauche := _Rect.Left;
  Haut := _Rect.Top;
  Bas := _Rect.Bottom;

Bmp := DrawCourbeBezier(_Stl, _Rect);
Dlg_ApercuImpressions.Apercu.Canvas.StretchDraw(Gauche, Haut, Droite, Bas, Bmp, GmPixels);

End;
{ ************************************************************************************************************************************************* }
Procedure ImpressionDessinCourbe;
Var
  RectImp  : TGmRect;
  Rect     : TRect;
  Stl_Impr : TStringList;
  PenColor : TColor;
  XMax     : Real;
  YMax     : Real;
  Stl      : TBrushStyle;
  YBottom  : Extended;
Begin

  PenColor := Dlg_ApercuImpressions.Apercu.Canvas.Pen.Color;
  RectImp.Left   := Dlg_ApercuImpressions.Apercu.Margins.Left.AsMillimeters;
  RectImp.Top    := Dlg_ApercuImpressions.CurrentYPosOnPage + 20;
  RectImp.Right  := RectImp.Left + Dlg_ApercuImpressions.Apercu.PageWidth[gmMillimeters] -
                    Dlg_ApercuImpressions.Apercu.Margins.Right.AsMillimeters -
                    Dlg_ApercuImpressions.Apercu.Margins.Left.AsMillimeters;
  RectImp.Bottom := Dlg_ApercuImpressions.Apercu.PageHeight[gmMillimeters] -
                    Dlg_ApercuImpressions.Apercu.Footer.Height[GmMillimeters] -
                    Dlg_ApercuImpressions.Apercu.Margins.Bottom.AsMillimeters -
                    Dlg_ApercuImpressions.Apercu.Header.Height[GmMillimeters] - 100;
  YBottom         := RectImp.Bottom;

  RectImp.Left    := ConvertValue(RectImp.Left, GmMillimeters, GmPixels);
  RectImp.Right   := ConvertValue(RectImp.Right, GmMillimeters, GmPixels);
  RectImp.Bottom  := ConvertValue(RectImp.Bottom, GmMillimeters, GmPixels);
  RectImp.Top     := ConvertValue(RectImp.Top, GmMillimeters, GmPixels);
  Rect.Left       := Round(RectImp.Left);
  Rect.Right      := Round(RectImp.Right);
  Rect.Top        := Round(RectImp.Top);
  Rect.Bottom     := Round(RectImp.Bottom);
  XMax := 0;
  YMax := 0;
  Stl_Impr := GenerationPoints(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson), Rect, XMax, YMax);
  ImpressionCourbeBezier(Stl_Impr, RectImp);
  FreeAndNil(Stl_Impr);

  Stl := Dlg_ApercuImpressions.Apercu.Canvas.Brush.Style;
  Dlg_ApercuImpressions.Apercu.Canvas.Brush.Style := bsClear;
  Dlg_ApercuImpressions.Apercu.Canvas.Pen.Color := clBlack;
  Dlg_ApercuImpressions.Apercu.Canvas.Rectangle(RectImp.Left, RectImp.Top, RectImp.Right, RectImp.Bottom, gmPixels);
  Dlg_ApercuImpressions.Apercu.Canvas.Brush.Style := Stl;
  Dlg_ApercuImpressions.Apercu.Canvas.Pen.Color := PenColor;




//  Dlg_ApercuImpressions.CurrentYPosOnPage := Dlg_ApercuImpressions.CurrentYPosOnPage + 80;
  Dlg_ApercuImpressions.CurrentYPosOnPage := YBottom;
End;
{ ************************************************************************************************************************************************* }
Procedure ImpressionBasCourbe(NbLignesSautees : Integer = 1);
Var
  m_Grid   : TStringGrid;
Begin
  ApercuImp_SauteLigne(NbLignesSautees);
  m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 1; m_Grid.RowCount := 1;

  If Etude.Batiment.MethodeCalcul = c_CalculDTU Then
    m_Grid.Objects[0, 0] := TCelluleImp.Create('M�thode de calcul conforme au DTU 68.1', GetFontInfos, 0, clRed, clWhite, [fsItalic], taLeftJustify)
  Else
    m_Grid.Objects[0, 0] := TCelluleImp.Create('Vitesses limites de ' + Float2Str(Etude.Batiment.VitesseHorizontale, 0) + ' m/s pour le r�seau horizontal et de ' +
                                                Float2Str(Etude.Batiment.VitesseVerticale, 0) + ' m/s pour le r�seau vertical',
                                                GetFontInfos, 0, clRed, clWhite, [fsItalic], taLeftJustify);
  ApercuImp_AjouteGrille(m_Grid, True, '', False, False, False, True);

  m_Grid := TStringGrid.Create(nil); m_Grid.ColCount := 3; m_Grid.RowCount := 1;

  m_Grid.Objects[0, 0] := TCelluleImp.Create('Masse volumique de l''air: 1,2 kg/m3', GetFontInfos);
  m_Grid.Objects[1, 0] := TCelluleImp.Create('Taux de fuite du r�seau: ' + IntToStr(Etude.Batiment.DebitFuite) + ' %', GetFontInfos);
  m_Grid.Objects[2, 0] := TCelluleImp.Create('Pertes de charge des entr�es d''air: 20 Pa', GetFontInfos);

  ApercuImp_AjouteGrille(m_Grid, True, '');
End;
{ ************************************************************************************************************************************************* }
Procedure ImpressionBasDePageAnjos(NbLignesSautees : Integer = 1);
Begin
//ImpressionBasCourbe(NbLignesSautees);
End;
{ ************************************************************************************************************************************************* }
Procedure ImpressionHautDePageAnjos(NbLignesSautees : Integer = 1);
Begin
ImpressionCartoucheCourbeFct;
ApercuImp_SauteLigne(NbLignesSautees);
End;
{ ************************************************************************************************************************************************* }
Procedure Impression_CourbeFonctionnement(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Begin
  ApercuImp_Debut(_Lanceur, 'COURBE DE FONCTIONNEMENT THEORIQUE DU VENTILATEUR: ' + Etude.Batiment.Reference,
                  Etude.Batiment.Reference, GetPiedDePage, Not _ImpressionEnCours);
  ImpressionCartoucheCourbeFct;
  ImpressionDessinCourbe;
  ImpressionBasCourbe;
  If Not _ImpressionEnCours Then ApercuImp_Show;
End;
{ ************************************************************************************************************************************************* }
Procedure Impression_FicheCaissonOld(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Const
  _Title = 'Fiche technique du ventilateur';
Var
  m_Ventil: TEdibatec_Caisson;
//  m_DPQMax,
//  m_DPQMin : Real;
  m_NoImg  : Integer;
  VentilSchema : TJPegImage;
  VentilPhoto  : TJPegImage;

  RectImpSchema : TRect;
  RectImpPhoto : TRect;  

  m_Grid : TStringGrid;

  ListMAS : TList;
  ListELE : TList;
  ListDIM : TList;

  cptrow : integer;

  cpt_Elt : Integer;
  MyDonneImp : TDonneeImp;

Begin


  m_Ventil:= TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie;

  If m_Ventil = nil Then Exit;

  ApercuImp_Debut(_Lanceur, _Title, Etude.Batiment.Reference, GetPiedDePage, Not _ImpressionEnCours);


  ApercuImp_Titre(m_Ventil.Reference + #13 + m_Ventil.Desc_Ventil);


//-----------r�cup�ration des informations d'impression----------------
  ListMAS := TList.Create;
  ListELE := TList.Create;
  ListDIM := TList.Create;  
  DataImpCaractCaisson.GetImpressionElementsFromTypeVentilOld(ListMAS, ListELE, ListDIM, VentilPhoto, VentilSchema, m_Ventil.Type_Ventil);

//-----------ajout des informations manquantes----------------
        if Etude.SaveOptions_SelectionCaisson.CheckBox_DTU_Checked then
                Begin
                MyDonneImp := TDonneeImp.Create;
                MyDonneImp.Libelle := 'Coefficient RT';
                MyDonneImp.Valeur := Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.CoeffRT, 2);
                ListMAS.Insert(0, MyDonneImp);

                MyDonneImp := TDonneeImp.Create;
                MyDonneImp.Libelle := 'Puissance moyenne "ThC" (W)';
                MyDonneImp.Valeur := Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.PMoy, 2);
                ListMAS.Insert(0, MyDonneImp);

                MyDonneImp := TDonneeImp.Create;
                MyDonneImp.Libelle := 'D�bit moyen (m3/h)';
                MyDonneImp.Valeur := Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.QMoy, 2);
                ListMAS.Insert(0, MyDonneImp);

                End
        else
                Begin
                MyDonneImp := TDonneeImp.Create;
                MyDonneImp.Libelle := 'Puissance maxi (m3/h)';
                MyDonneImp.Valeur := Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.PuissCalcQMax, 2);
                ListMAS.Insert(0, MyDonneImp);
                End;

  MyDonneImp := TDonneeImp.Create;
  MyDonneImp.Libelle := 'D�bit maxi (m3/h)';
  MyDonneImp.Valeur := IntToStr(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).Reseau_DebitMaximum);
  ListMAS.Insert(0, MyDonneImp);

        if Etude.SaveOptions_SelectionCaisson.CheckBox_DTU_Checked then
                Begin
                MyDonneImp := TDonneeImp.Create;
                MyDonneImp.Libelle := 'D�bit mini (m3/h)';
                MyDonneImp.Valeur := IntToStr(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).Reseau_DebitMinimum);
                ListMAS.Insert(0, MyDonneImp);
                End;


//-----------partie haute----------------
  m_Grid := TStringGrid.Create(nil);

  m_Grid.ColCount := 7;
  m_Grid.RowCount :=8;

  for cpt_Elt := 0 to ListMAS.Count - 1 do
        Begin
        m_Grid.Objects[cpt_Elt, 0] := TCelluleImp.Create(TDonneeImp(ListMAS[cpt_Elt]).Libelle, GetFontInfos, 0, clBlack, clSkyBlue);
        m_Grid.Objects[cpt_Elt, 1] := TCelluleImp.Create(TDonneeImp(ListMAS[cpt_Elt]).Valeur, GetFontInfos, 0, clBlack, clWhite);
        End;


  for cpt_Elt := 0 to ListELE.Count - 1 do
        Begin
        m_Grid.Objects[cpt_Elt, 4] := TCelluleImp.Create(TDonneeImp(ListELE[cpt_Elt]).Libelle, GetFontInfos, 0, clBlack, clSkyBlue);
        m_Grid.Objects[cpt_Elt, 5] := TCelluleImp.Create(TDonneeImp(ListELE[cpt_Elt]).Valeur, GetFontInfos, 0, clBlack, clWhite);
        End;


  if ListMAS.Count < 6 then
  for cpt_Elt := ListMAS.Count to 5 do
        Begin
        m_Grid.Objects[cpt_Elt, 0] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
        m_Grid.Objects[cpt_Elt, 1] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
        TCelluleImp(m_Grid.Objects[cpt_Elt, 0]).ISeparateurBas := false;
        TCelluleImp(m_Grid.Objects[cpt_Elt, 1]).ISeparateurBas := false;        
                if cpt_Elt < 5 Then
                        Begin
                        TCelluleImp(m_Grid.Objects[cpt_Elt, 0]).ISeparateurDroite := false;
                        TCelluleImp(m_Grid.Objects[cpt_Elt, 1]).ISeparateurDroite := false;
                        End;
        End;

  if ListELE.Count < 6 then
  for cpt_Elt := ListELE.Count to 5 do
        Begin
        m_Grid.Objects[cpt_Elt, 4] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
        m_Grid.Objects[cpt_Elt, 5] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
        TCelluleImp(m_Grid.Objects[cpt_Elt, 4]).ISeparateurBas := false;
        TCelluleImp(m_Grid.Objects[cpt_Elt, 5]).ISeparateurBas := false;
                if cpt_Elt < 5 Then
                        Begin
                        TCelluleImp(m_Grid.Objects[cpt_Elt, 4]).ISeparateurDroite := false;
                        TCelluleImp(m_Grid.Objects[cpt_Elt, 5]).ISeparateurDroite := false;
                        End;
        End;

  for cpt_Elt := 0 to 5 do
        Begin
        m_Grid.Objects[cpt_Elt, 2] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
        m_Grid.Objects[cpt_Elt, 3] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
        m_Grid.Objects[cpt_Elt, 6] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
        m_Grid.Objects[cpt_Elt, 7] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
        TCelluleImp(m_Grid.Objects[cpt_Elt, 2]).ISeparateurBas := false;
        TCelluleImp(m_Grid.Objects[cpt_Elt, 6]).ISeparateurBas := false;
                if TCelluleImp(m_Grid.Objects[cpt_Elt, 4]).ITexte = '' then
                        TCelluleImp(m_Grid.Objects[cpt_Elt, 3]).ISeparateurBas := false;
                if cpt_Elt < 5 Then
                        Begin
                        TCelluleImp(m_Grid.Objects[cpt_Elt, 2]).ISeparateurDroite := false;
                        TCelluleImp(m_Grid.Objects[cpt_Elt, 3]).ISeparateurDroite := false;
                        End;
        TCelluleImp(m_Grid.Objects[cpt_Elt, 6]).ISeparateurDroite := false;
        TCelluleImp(m_Grid.Objects[cpt_Elt, 7]).ISeparateurDroite := false;
        End;


        if (VentilPhoto <> nil) then
                begin
                RectImpPhoto.Left   := 0;
                RectImpPhoto.Top    := 0;
                RectImpPhoto.Right  := VentilPhoto.Width;
                RectImpPhoto.Bottom := VentilPhoto.Height;

                m_Grid.Objects[6, 0] := TCelluleImp.Create(m_Ventil.Reference, GetFontInfos, 0, clBlack, clSkyBlue);
                m_Grid.Objects[6, 1] := TCelluleImp.Create('', GetFontInfos, 100, clBlack, clWhite);                
                TCelluleImp(m_Grid.Objects[6, 1]).IImage := TBitmap.Create;

                TCelluleImp(m_Grid.Objects[6, 1]).IImage.Width := RectImpPhoto.Right;
                TCelluleImp(m_Grid.Objects[6, 1]).IImage.Height := RectImpPhoto.Bottom;

                TCelluleImp(m_Grid.Objects[6, 1]).IImage.Canvas.StretchDraw(RectImpPhoto, VentilPhoto);

                TCelluleImp(m_Grid.Objects[6, 1]).IImageAdapt := true;
                TCelluleImp(m_Grid.Objects[6, 1]).IImageMergeable := false;
                TCelluleImp(m_Grid.Objects[6, 1]).IMerged := false;
                TCelluleImp(m_Grid.Objects[6, 1]).ISeparateurBas := false;
                TCelluleImp(m_Grid.Objects[6, 1]).IImageAdaptNbLignes := 7;

                        for cptrow := 2 to m_Grid.RowCount - 1 do
                                begin
                                m_Grid.Objects[6, cptrow] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                                TCelluleImp(m_Grid.Objects[6, cptrow]).ISeparateurBas := false;
                                end;
                TCelluleImp(m_Grid.Objects[6, 5]).ISeparateurBas := True;



                VentilPhoto.Destroy;


                end;

        for cpt_Elt := 0 to 5 do
                Begin
                        if m_Grid.Objects[cpt_Elt, 0] = nil then
                                m_Grid.Objects[cpt_Elt, 0] := TCelluleImp.Create('', GetFontInfos, 100, clBlack, clWhite);
                TCelluleImp(m_Grid.Objects[cpt_Elt, 0]).ILargeur := 15;
                End;

TCelluleImp(m_Grid.Objects[6, 0]).ILargeur := 25;

ApercuImp_AjouteGrille_GestionImage(m_Grid, true, '', false, false, False, true);


//-----------partie dimensions----------------

  m_Grid := TStringGrid.Create(nil);

  m_Grid.ColCount := 5;
  m_Grid.RowCount :=20;


//        if ListDIM.Count > 0 then
                Begin
                        for cpt_Elt := 0 to Min(3, ListDIM.Count - 1) do
                                Begin
                                m_Grid.Objects[cpt_Elt, 0] := TCelluleImp.Create(TDonneeImp(ListDIM[cpt_Elt]).Libelle, GetFontInfos, 0, clBlack, clSkyBlue);
                                m_Grid.Objects[cpt_Elt, 1] := TCelluleImp.Create(TDonneeImp(ListDIM[cpt_Elt]).Valeur, GetFontInfos, 0, clBlack, clWhite);
                                End;

                        for cpt_Elt := Max(0, ListDIM.Count) to 3 do
                                Begin
                                m_Grid.Objects[cpt_Elt, 0] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                                m_Grid.Objects[cpt_Elt, 1] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 0]).ISeparateurBas := false;
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 1]).ISeparateurBas := false;
                                        if cpt_Elt < 3 then
                                                Begin
                                                TCelluleImp(m_Grid.Objects[cpt_Elt, 0]).ISeparateurDroite := false;
                                                TCelluleImp(m_Grid.Objects[cpt_Elt, 1]).ISeparateurDroite := false;
                                                End;
                                End;
                End;

//        if ListDIM.Count > 3 then
                Begin
                        for cpt_Elt := 4 to Min(7, ListDIM.Count - 1) do
                                Begin
                                m_Grid.Objects[cpt_Elt - 4, 4] := TCelluleImp.Create(TDonneeImp(ListDIM[cpt_Elt]).Libelle, GetFontInfos, 0, clBlack, clSkyBlue);
                                m_Grid.Objects[cpt_Elt - 4, 5] := TCelluleImp.Create(TDonneeImp(ListDIM[cpt_Elt]).Valeur, GetFontInfos, 0, clBlack, clWhite);
                                End;

                        for cpt_Elt := Max(4, ListDIM.Count) to 7 do
                                Begin
                                m_Grid.Objects[cpt_Elt - 4, 4] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                                m_Grid.Objects[cpt_Elt - 4, 5] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
                                TCelluleImp(m_Grid.Objects[cpt_Elt - 4, 4]).ISeparateurBas := false;
                                TCelluleImp(m_Grid.Objects[cpt_Elt - 4, 5]).ISeparateurBas := false;
                                        if cpt_Elt < 7 then
                                                Begin
                                                TCelluleImp(m_Grid.Objects[cpt_Elt - 4, 4]).ISeparateurDroite := false;
                                                TCelluleImp(m_Grid.Objects[cpt_Elt - 4, 5]).ISeparateurDroite := false;
                                                End;
                                End;
                End;

//        if ListDIM.Count > 7 then
                Begin
                        for cpt_Elt := 8 to Min(11, ListDIM.Count - 1) do
                                Begin
                                m_Grid.Objects[cpt_Elt - 8, 8] := TCelluleImp.Create(TDonneeImp(ListDIM[cpt_Elt]).Libelle, GetFontInfos, 0, clBlack, clSkyBlue);
                                m_Grid.Objects[cpt_Elt - 8, 9] := TCelluleImp.Create(TDonneeImp(ListDIM[cpt_Elt]).Valeur, GetFontInfos, 0, clBlack, clWhite);
                                End;

                        for cpt_Elt := Max(8, ListDIM.Count) to 11 do
                                Begin
                                m_Grid.Objects[cpt_Elt - 8, 8] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                                m_Grid.Objects[cpt_Elt - 8, 9] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
                                TCelluleImp(m_Grid.Objects[cpt_Elt - 8, 8]).ISeparateurBas := false;
                                TCelluleImp(m_Grid.Objects[cpt_Elt - 8, 9]).ISeparateurBas := false;
                                        if cpt_Elt < 11 then
                                                Begin
                                                TCelluleImp(m_Grid.Objects[cpt_Elt - 8, 8]).ISeparateurDroite := false;
                                                TCelluleImp(m_Grid.Objects[cpt_Elt - 8, 9]).ISeparateurDroite := false;
                                                End;
                                End;
                End;

//        if ListDIM.Count > 11 then
                Begin
                        for cpt_Elt := 12 to Min(15, ListDIM.Count - 1) do
                                Begin
                                m_Grid.Objects[cpt_Elt - 12, 12] := TCelluleImp.Create(TDonneeImp(ListDIM[cpt_Elt]).Libelle, GetFontInfos, 0, clBlack, clSkyBlue);
                                m_Grid.Objects[cpt_Elt - 12, 13] := TCelluleImp.Create(TDonneeImp(ListDIM[cpt_Elt]).Valeur, GetFontInfos, 0, clBlack, clWhite);
                                End;

                        for cpt_Elt := Max(12, ListDIM.Count) to 15 do
                                Begin
                                m_Grid.Objects[cpt_Elt - 12, 12] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                                m_Grid.Objects[cpt_Elt - 12, 13] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
                                TCelluleImp(m_Grid.Objects[cpt_Elt - 12, 12]).ISeparateurBas := false;
                                TCelluleImp(m_Grid.Objects[cpt_Elt - 12, 13]).ISeparateurBas := false;
                                        if cpt_Elt < 15 then
                                                Begin
                                                TCelluleImp(m_Grid.Objects[cpt_Elt - 12, 12]).ISeparateurDroite := false;
                                                TCelluleImp(m_Grid.Objects[cpt_Elt - 12, 13]).ISeparateurDroite := false;
                                                End;
                                End;
                End;

//        if ListDIM.Count > 15 then
                Begin
                        for cpt_Elt := 16 to Min(19, ListDIM.Count - 1) do
                                Begin
                                m_Grid.Objects[cpt_Elt - 16, 16] := TCelluleImp.Create(TDonneeImp(ListDIM[cpt_Elt]).Libelle, GetFontInfos, 0, clBlack, clSkyBlue);
                                m_Grid.Objects[cpt_Elt - 16, 17] := TCelluleImp.Create(TDonneeImp(ListDIM[cpt_Elt]).Valeur, GetFontInfos, 0, clBlack, clWhite);
                                End;

                        for cpt_Elt := Max(16, ListDIM.Count) to 19 do
                                Begin
                                m_Grid.Objects[cpt_Elt - 16, 16] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                                m_Grid.Objects[cpt_Elt - 16, 17] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
                                TCelluleImp(m_Grid.Objects[cpt_Elt - 16, 16]).ISeparateurBas := false;
                                TCelluleImp(m_Grid.Objects[cpt_Elt - 16, 17]).ISeparateurBas := false;
                                        if cpt_Elt < 19 then
                                                Begin
                                                TCelluleImp(m_Grid.Objects[cpt_Elt - 16, 16]).ISeparateurDroite := false;
                                                TCelluleImp(m_Grid.Objects[cpt_Elt - 16, 17]).ISeparateurDroite := false;
                                                End;
                                End;
                End;



        for cpt_Elt := 0 to 3 do
                Begin
                m_Grid.Objects[cpt_Elt, 2] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                m_Grid.Objects[cpt_Elt, 3] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
                m_Grid.Objects[cpt_Elt, 6] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                m_Grid.Objects[cpt_Elt, 7] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
                m_Grid.Objects[cpt_Elt, 10] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                m_Grid.Objects[cpt_Elt, 11] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
                m_Grid.Objects[cpt_Elt, 14] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                m_Grid.Objects[cpt_Elt, 15] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
                m_Grid.Objects[cpt_Elt, 18] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                m_Grid.Objects[cpt_Elt, 19] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);


                        if cpt_Elt < 3 then
                                Begin
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 2]).ISeparateurDroite := False;
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 3]).ISeparateurDroite := False;
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 6]).ISeparateurDroite := False;
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 7]).ISeparateurDroite := False;
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 10]).ISeparateurDroite := False;
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 11]).ISeparateurDroite := False;
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 14]).ISeparateurDroite := False;
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 15]).ISeparateurDroite := False;
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 18]).ISeparateurDroite := False;
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 19]).ISeparateurDroite := False;
                                End
                        else
                                Begin
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 18]).ISeparateurDroite := False;
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 19]).ISeparateurDroite := False;
                                End;

                TCelluleImp(m_Grid.Objects[cpt_Elt, 2]).ISeparateurBas := False;
                TCelluleImp(m_Grid.Objects[cpt_Elt, 6]).ISeparateurBas := False;
                TCelluleImp(m_Grid.Objects[cpt_Elt, 10]).ISeparateurBas := False;
                TCelluleImp(m_Grid.Objects[cpt_Elt, 14]).ISeparateurBas := False;
                TCelluleImp(m_Grid.Objects[cpt_Elt, 18]).ISeparateurBas := False;

                        if TCelluleImp(m_Grid.Objects[cpt_Elt, 4]).ITexte = '' then
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 3]).ISeparateurBas := False;

                        if TCelluleImp(m_Grid.Objects[cpt_Elt, 8]).ITexte = '' then
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 7]).ISeparateurBas := False;

                        if TCelluleImp(m_Grid.Objects[cpt_Elt, 12]).ITexte = '' then
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 11]).ISeparateurBas := False;

                        if TCelluleImp(m_Grid.Objects[cpt_Elt, 16]).ITexte = '' then
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 15]).ISeparateurBas := False;

                End;


        if (VentilPhoto <> nil) then
                begin

                RectImpSchema.Left   := 0;
                RectImpSchema.Top    := 0;
                RectImpSchema.Right  := VentilSchema.Width;
                RectImpSchema.Bottom := VentilSchema.Height;

                m_Grid.Objects[4, 0] := TCelluleImp.Create('Dimensions (mm)', GetFontInfos, 0, clBlack, clSkyBlue);
                m_Grid.Objects[4, 1] := TCelluleImp.Create('', GetFontInfos, 100, clBlack, clWhite);
                TCelluleImp(m_Grid.Objects[4, 1]).IImage := TBitmap.Create;

                TCelluleImp(m_Grid.Objects[4, 1]).IImage.Width := RectImpSchema.Right;
                TCelluleImp(m_Grid.Objects[4, 1]).IImage.Height := RectImpSchema.Bottom;

                TCelluleImp(m_Grid.Objects[4, 1]).IImage.Canvas.StretchDraw(RectImpSchema, VentilSchema);

                TCelluleImp(m_Grid.Objects[4, 1]).IImageAdapt := true;
                TCelluleImp(m_Grid.Objects[4, 1]).IImageMergeable := false;
                TCelluleImp(m_Grid.Objects[4, 1]).IMerged := false;
                TCelluleImp(m_Grid.Objects[4, 1]).ISeparateurBas := false;
                TCelluleImp(m_Grid.Objects[4, 1]).IImageAdaptNbLignes := 50;

                        for cptrow := 2 to m_Grid.RowCount - 1 do
                                begin
                                m_Grid.Objects[4, cptrow] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                                TCelluleImp(m_Grid.Objects[4, cptrow]).ISeparateurBas := false;
                                end;

                TCelluleImp(m_Grid.Objects[4, 17]).ISeparateurBas := True;                                

                VentilSchema.Destroy;
                End;


        for cpt_Elt := 0 to 3 do
                Begin
                        if m_Grid.Objects[cpt_Elt, 0] = nil then
                                m_Grid.Objects[cpt_Elt, 0] := TCelluleImp.Create('', GetFontInfos, 100, clBlack, clWhite);
                TCelluleImp(m_Grid.Objects[cpt_Elt, 0]).ILargeur := 10;
                End;
                
TCelluleImp(m_Grid.Objects[4, 0]).ILargeur := 60;

ApercuImp_AjouteGrille_GestionImage(m_Grid, true, '', false, false, False, true);


//-----------destruction des �l�ments----------------
  for cpt_Elt := ListMAS.Count - 1 DownTo 0 do
        TDonneeImp(ListMAS[cpt_Elt]).Destroy;

  for cpt_Elt := ListELE.Count - 1 DownTo 0 do
        TDonneeImp(ListELE[cpt_Elt]).Destroy;

  for cpt_Elt := ListDIM.Count - 1 DownTo 0 do
        TDonneeImp(ListDIM[cpt_Elt]).Destroy;

  ListMAS.Destroy;
  ListELE.Destroy;
  ListDIM.Destroy;


//-----------partie courbes----------------
  Impression_CourbesCaisson;

End;
{ ************************************************************************************************************************************************* }
Procedure Impression_FicheCaisson(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Const
  _Title = 'Fiche technique du ventilateur';
Var
  m_Ventil: TEdibatec_Caisson;
//  m_DPQMax,
//  m_DPQMin : Real;
  m_NoImg  : Integer;
  VentilSchema : TJPegImage;
  VentilPhoto  : TJPegImage;

  RectImpSchema : TRect;
  RectImpPhoto : TRect;  

  m_Grid : TStringGrid;

  ListCALC : TList;
  ListELE  : TList;
  ListDIM  : TList;

  cptrow : integer;

  cpt_Elt : Integer;

  MyDonneImp : TDonneeImp;

Begin

  m_Ventil:= TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie;

  If m_Ventil = nil Then Exit;

  ApercuImp_Debut(_Lanceur, _Title, Etude.Batiment.Reference, GetPiedDePage, Not _ImpressionEnCours);


  ApercuImp_Titre(m_Ventil.Reference + #13 + m_Ventil.Desc_Ventil);


//-----------r�cup�ration des informations d'impression----------------
  ListCALC := TList.Create;
  ListELE := TList.Create;
  ListDIM := TList.Create;  
  DataImpCaractCaisson.GetImpressionElementsFromTypeVentil(ListELE, ListDIM, VentilPhoto, VentilSchema, m_Ventil.Type_Ventil);

//-----------ajout des informations manquantes----------------



MyDonneImp := TDonneeImp.Create;
MyDonneImp.Libelle := '';
MyDonneImp.Valeur := 'Maxi';
MyDonneImp.FixedValuesCell := True;
ListCALC.Add(MyDonneImp);

MyDonneImp := TDonneeImp.Create;
MyDonneImp.Libelle := 'D�bits "DTU" (m3/h)';
MyDonneImp.Valeur := IntToStr(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).Reseau_DebitMaximum);
ListCALC.Add(MyDonneImp);

MyDonneImp := TDonneeImp.Create;
MyDonneImp.Libelle := 'DP (Pa)';
MyDonneImp.Valeur := Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.DpCalcQMax, 0);
ListCALC.Add(MyDonneImp);

        if Etude.SaveOptions_SelectionCaisson.CheckBox_DTU_Checked then
                Begin
                        for cpt_Elt := 0 to ListCALC.Count - 1 Do
                                Begin
                                TDonneeImp(ListCALC[cpt_Elt]).Valeur2 := TDonneeImp(ListCALC[cpt_Elt]).Valeur;
                                TDonneeImp(ListCALC[cpt_Elt]).Valeur  := '';
                                End;

                TDonneeImp(ListCALC[0]).Valeur := 'Mini';
                TDonneeImp(ListCALC[1]).Valeur := IntToStr(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).Reseau_DebitMinimum);
                TDonneeImp(ListCALC[2]).Valeur := Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.DpCalcQMin, 0);

                MyDonneImp := TDonneeImp.Create;
                MyDonneImp.Libelle := 'D�bits "RT" (m3/h)';
                MyDonneImp.Valeur := Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.QMinRT, 0);
                MyDonneImp.Valeur2 := Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.QMaxRT, 0);
                ListCALC.Add(MyDonneImp);

                MyDonneImp := TDonneeImp.Create;
                MyDonneImp.Libelle := 'Puissances "RT" (W)';
                MyDonneImp.Valeur := Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.PMinRT, 0);
                MyDonneImp.Valeur2 := Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.PMaxRT, 0);
                MyDonneImp.SeparationDroiteEpaisse := True;
                ListCALC.Add(MyDonneImp);

                MyDonneImp := TDonneeImp.Create;
                MyDonneImp.Libelle := 'D�bit et Puisance Th-C';
                MyDonneImp.Valeur := Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.QMoy, 0) + ' m3/h';
                MyDonneImp.Valeur2 := Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.PMoy, 0) + ' W';
                MyDonneImp.WithoutHorizontalLine := True;
                ListCALC.Add(MyDonneImp);

                MyDonneImp := TDonneeImp.Create;
                MyDonneImp.Libelle := 'Conso. RT 2005 (W/m3/h)';
                MyDonneImp.Valeur := Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.CoeffRT, 2);
                MyDonneImp.MergedValuesCell := True;
                ListCALC.Add(MyDonneImp);

                End
        else
                Begin
                MyDonneImp := TDonneeImp.Create;
                MyDonneImp.Libelle := 'Puissance (m3/h)';
                MyDonneImp.Valeur := Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.PMaxRT, 2);
                ListCALC.Add(MyDonneImp);

                ListCALC.Delete(0);
                End;


//-----------partie haute----------------
  m_Grid := TStringGrid.Create(nil);

  m_Grid.ColCount := 8;
  m_Grid.RowCount :=9;
//  m_Grid.RowCount :=7;

  for cpt_Elt := 0 to ListCALC.Count - 1 do
        Begin
        m_Grid.Objects[cpt_Elt, 0] := TCelluleImp.Create(TDonneeImp(ListCALC[cpt_Elt]).Libelle, GetFontInfos, 0, clBlack, clSkyBlue);
                if TDonneeImp(ListCALC[cpt_Elt]).FixedValuesCell then
                        m_Grid.Objects[cpt_Elt, 1] := TCelluleImp.Create(TDonneeImp(ListCALC[cpt_Elt]).Valeur, GetFontInfos, 0, clBlack, clSkyBlue)
                else
                        m_Grid.Objects[cpt_Elt, 1] := TCelluleImp.Create(TDonneeImp(ListCALC[cpt_Elt]).Valeur, GetFontInfos, 0, clBlack, clWhite);
                if Etude.SaveOptions_SelectionCaisson.CheckBox_DTU_Checked then
                        Begin
                                if TDonneeImp(ListCALC[cpt_Elt]).FixedValuesCell then
                                        m_Grid.Objects[cpt_Elt, 2] := TCelluleImp.Create(TDonneeImp(ListCALC[cpt_Elt]).Valeur2, GetFontInfos, 0, clBlack, clSkyBlue)
                                else
                                        m_Grid.Objects[cpt_Elt, 2] := TCelluleImp.Create(TDonneeImp(ListCALC[cpt_Elt]).Valeur2, GetFontInfos, 0, clBlack, clWhite);
                        End
                else
                        Begin
                        m_Grid.Objects[cpt_Elt, 2] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
                        TCelluleImp(m_Grid.Objects[cpt_Elt, 2]).ISeparateurBas := false;
                                if cpt_Elt < 6 Then
                                        TCelluleImp(m_Grid.Objects[cpt_Elt, 2]).ISeparateurDroite := false;
                        End;

        TCelluleImp(m_Grid.Objects[cpt_Elt, 1]).IHorMerged := TDonneeImp(ListCALC[cpt_Elt]).MergedValuesCell;
        TCelluleImp(m_Grid.Objects[cpt_Elt, 1]).ISeparateurBas := Not(TDonneeImp(ListCALC[cpt_Elt]).WithoutHorizontalLine);
        End;


  for cpt_Elt := 0 to ListELE.Count - 1 do
        Begin
        m_Grid.Objects[cpt_Elt, 5] := TCelluleImp.Create(TDonneeImp(ListELE[cpt_Elt]).Libelle, GetFontInfos, 0, clBlack, clSkyBlue);
        m_Grid.Objects[cpt_Elt, 6] := TCelluleImp.Create(TDonneeImp(ListELE[cpt_Elt]).Valeur, GetFontInfos, 0, clBlack, clWhite);
        End;


  if ListCALC.Count < 7 then
  for cpt_Elt := ListCALC.Count to 6 do
        Begin
        m_Grid.Objects[cpt_Elt, 0] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
        m_Grid.Objects[cpt_Elt, 1] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
        m_Grid.Objects[cpt_Elt, 2] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
        TCelluleImp(m_Grid.Objects[cpt_Elt, 0]).ISeparateurBas := false;
        TCelluleImp(m_Grid.Objects[cpt_Elt, 1]).ISeparateurBas := false;
        TCelluleImp(m_Grid.Objects[cpt_Elt, 2]).ISeparateurBas := false;                
                if cpt_Elt < 6 Then
                        Begin
                        TCelluleImp(m_Grid.Objects[cpt_Elt, 0]).ISeparateurDroite := false;
                        TCelluleImp(m_Grid.Objects[cpt_Elt, 1]).ISeparateurDroite := false;
                        TCelluleImp(m_Grid.Objects[cpt_Elt, 2]).ISeparateurDroite := false;                        
                        End;
        End;

  if ListELE.Count < 7 then
  for cpt_Elt := ListELE.Count to 6 do
        Begin
        m_Grid.Objects[cpt_Elt, 5] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
        m_Grid.Objects[cpt_Elt, 6] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
        TCelluleImp(m_Grid.Objects[cpt_Elt, 5]).ISeparateurBas := false;
        TCelluleImp(m_Grid.Objects[cpt_Elt, 6]).ISeparateurBas := false;
                if cpt_Elt < 6 Then
                        Begin
                        TCelluleImp(m_Grid.Objects[cpt_Elt, 5]).ISeparateurDroite := false;
                        TCelluleImp(m_Grid.Objects[cpt_Elt, 6]).ISeparateurDroite := false;
                        End;
        End;

  for cpt_Elt := 0 to 6 do
        Begin
        m_Grid.Objects[cpt_Elt, 3] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
        m_Grid.Objects[cpt_Elt, 4] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
        m_Grid.Objects[cpt_Elt, 7] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
        m_Grid.Objects[cpt_Elt, 8] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
        TCelluleImp(m_Grid.Objects[cpt_Elt, 3]).ISeparateurBas := false;
        TCelluleImp(m_Grid.Objects[cpt_Elt, 7]).ISeparateurBas := false;
                if TCelluleImp(m_Grid.Objects[cpt_Elt, 5]).ITexte = '' then
                        TCelluleImp(m_Grid.Objects[cpt_Elt, 4]).ISeparateurBas := false;
                if cpt_Elt < 6 Then
                        Begin
                        TCelluleImp(m_Grid.Objects[cpt_Elt, 3]).ISeparateurDroite := false;
                        TCelluleImp(m_Grid.Objects[cpt_Elt, 4]).ISeparateurDroite := false;
                        End;
        TCelluleImp(m_Grid.Objects[cpt_Elt, 7]).ISeparateurDroite := false;
        TCelluleImp(m_Grid.Objects[cpt_Elt, 8]).ISeparateurDroite := false;
        End;
        

        if (VentilPhoto <> nil) then
                begin
                RectImpPhoto.Left   := 0;
                RectImpPhoto.Top    := 0;
                RectImpPhoto.Right  := VentilPhoto.Width;
                RectImpPhoto.Bottom := VentilPhoto.Height;

                m_Grid.Objects[7, 0] := TCelluleImp.Create(m_Ventil.Reference, GetFontInfos, 0, clBlack, clSkyBlue);
                m_Grid.Objects[7, 1] := TCelluleImp.Create('', GetFontInfos, 100, clBlack, clWhite);
                TCelluleImp(m_Grid.Objects[7, 1]).IImage := TBitmap.Create;

                TCelluleImp(m_Grid.Objects[7, 1]).IImage.Width := RectImpPhoto.Right;
                TCelluleImp(m_Grid.Objects[7, 1]).IImage.Height := RectImpPhoto.Bottom;

                TCelluleImp(m_Grid.Objects[7, 1]).IImage.Canvas.StretchDraw(RectImpPhoto, VentilPhoto);

                TCelluleImp(m_Grid.Objects[7, 1]).IImageAdapt := true;
                TCelluleImp(m_Grid.Objects[7, 1]).IImageMergeable := false;
                TCelluleImp(m_Grid.Objects[7, 1]).IMerged := false;
                TCelluleImp(m_Grid.Objects[7, 1]).ISeparateurBas := false;
                TCelluleImp(m_Grid.Objects[7, 1]).IImageAdaptNbLignes := 7;

                        for cptrow := 2 to m_Grid.RowCount - 1 do
                                begin
                                m_Grid.Objects[7, cptrow] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                                TCelluleImp(m_Grid.Objects[7, cptrow]).ISeparateurBas := false;
                                end;
                TCelluleImp(m_Grid.Objects[7, 6]).ISeparateurBas := True;



                VentilPhoto.Destroy;


                end;

        for cpt_Elt := 0 to 6 do
                Begin
                        if m_Grid.Objects[cpt_Elt, 0] = nil then
                                m_Grid.Objects[cpt_Elt, 0] := TCelluleImp.Create('', GetFontInfos, 100, clBlack, clWhite);
                TCelluleImp(m_Grid.Objects[cpt_Elt, 0]).ILargeur := 10;
                End;

TCelluleImp(m_Grid.Objects[7, 0]).ILargeur := 25;

ApercuImp_AjouteGrille_GestionImage(m_Grid, true, '', false, false, False, true);

{
//-----------partie texte estimation----------------
  m_Grid := TStringGrid.Create(nil);

  m_Grid.ColCount := 1;

  m_Grid.RowCount := 2;
  m_Grid.Objects[0, 0] := TCelluleImp.Create('Estimation devant �tre valid�e ult�rieurement par une �tude d�taill�e de l''installation.', GetFontInfos, 0, clBlack, clWhite);
  m_Grid.Objects[0, 1] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
  TCelluleImp(m_Grid.Objects[0, 0]).ISeparateurBas := False;
  TCelluleImp(m_Grid.Objects[0, 0]).IAlign := taLeftJustify;
  ApercuImp_AjouteGrille(m_Grid, True, '', False, False, False, True);
}
//-----------partie dimensions----------------

  m_Grid := TStringGrid.Create(nil);

  m_Grid.ColCount := 5;
  m_Grid.RowCount :=18;


//        if ListDIM.Count > 0 then
                Begin
                        for cpt_Elt := 0 to Min(3, ListDIM.Count - 1) do
                                Begin
                                m_Grid.Objects[cpt_Elt, 0] := TCelluleImp.Create(TDonneeImp(ListDIM[cpt_Elt]).Libelle, GetFontInfos, 0, clBlack, clSkyBlue);
                                m_Grid.Objects[cpt_Elt, 1] := TCelluleImp.Create(TDonneeImp(ListDIM[cpt_Elt]).Valeur, GetFontInfos, 0, clBlack, clWhite);
                                End;

                        for cpt_Elt := Max(0, ListDIM.Count) to 3 do
                                Begin
                                m_Grid.Objects[cpt_Elt, 0] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                                m_Grid.Objects[cpt_Elt, 1] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 0]).ISeparateurBas := false;
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 1]).ISeparateurBas := false;
                                        if cpt_Elt < 3 then
                                                Begin
                                                TCelluleImp(m_Grid.Objects[cpt_Elt, 0]).ISeparateurDroite := false;
                                                TCelluleImp(m_Grid.Objects[cpt_Elt, 1]).ISeparateurDroite := false;
                                                End;
                                End;
                End;

//        if ListDIM.Count > 3 then
                Begin
                        for cpt_Elt := 4 to Min(7, ListDIM.Count - 1) do
                                Begin
                                m_Grid.Objects[cpt_Elt - 4, 4] := TCelluleImp.Create(TDonneeImp(ListDIM[cpt_Elt]).Libelle, GetFontInfos, 0, clBlack, clSkyBlue);
                                m_Grid.Objects[cpt_Elt - 4, 5] := TCelluleImp.Create(TDonneeImp(ListDIM[cpt_Elt]).Valeur, GetFontInfos, 0, clBlack, clWhite);
                                End;

                        for cpt_Elt := Max(4, ListDIM.Count) to 7 do
                                Begin
                                m_Grid.Objects[cpt_Elt - 4, 4] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                                m_Grid.Objects[cpt_Elt - 4, 5] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
                                TCelluleImp(m_Grid.Objects[cpt_Elt - 4, 4]).ISeparateurBas := false;
                                TCelluleImp(m_Grid.Objects[cpt_Elt - 4, 5]).ISeparateurBas := false;
                                        if cpt_Elt < 7 then
                                                Begin
                                                TCelluleImp(m_Grid.Objects[cpt_Elt - 4, 4]).ISeparateurDroite := false;
                                                TCelluleImp(m_Grid.Objects[cpt_Elt - 4, 5]).ISeparateurDroite := false;
                                                End;
                                End;
                End;

//        if ListDIM.Count > 7 then
                Begin
                        for cpt_Elt := 8 to Min(11, ListDIM.Count - 1) do
                                Begin
                                m_Grid.Objects[cpt_Elt - 8, 8] := TCelluleImp.Create(TDonneeImp(ListDIM[cpt_Elt]).Libelle, GetFontInfos, 0, clBlack, clSkyBlue);
                                m_Grid.Objects[cpt_Elt - 8, 9] := TCelluleImp.Create(TDonneeImp(ListDIM[cpt_Elt]).Valeur, GetFontInfos, 0, clBlack, clWhite);
                                End;

                        for cpt_Elt := Max(8, ListDIM.Count) to 11 do
                                Begin
                                m_Grid.Objects[cpt_Elt - 8, 8] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                                m_Grid.Objects[cpt_Elt - 8, 9] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
                                TCelluleImp(m_Grid.Objects[cpt_Elt - 8, 8]).ISeparateurBas := false;
                                TCelluleImp(m_Grid.Objects[cpt_Elt - 8, 9]).ISeparateurBas := false;
                                        if cpt_Elt < 11 then
                                                Begin
                                                TCelluleImp(m_Grid.Objects[cpt_Elt - 8, 8]).ISeparateurDroite := false;
                                                TCelluleImp(m_Grid.Objects[cpt_Elt - 8, 9]).ISeparateurDroite := false;
                                                End;
                                End;
                End;

//        if ListDIM.Count > 11 then
                Begin
                        for cpt_Elt := 12 to Min(15, ListDIM.Count - 1) do
                                Begin
                                m_Grid.Objects[cpt_Elt - 12, 12] := TCelluleImp.Create(TDonneeImp(ListDIM[cpt_Elt]).Libelle, GetFontInfos, 0, clBlack, clSkyBlue);
                                m_Grid.Objects[cpt_Elt - 12, 13] := TCelluleImp.Create(TDonneeImp(ListDIM[cpt_Elt]).Valeur, GetFontInfos, 0, clBlack, clWhite);
                                End;

                        for cpt_Elt := Max(12, ListDIM.Count) to 15 do
                                Begin
                                m_Grid.Objects[cpt_Elt - 12, 12] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                                m_Grid.Objects[cpt_Elt - 12, 13] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
                                TCelluleImp(m_Grid.Objects[cpt_Elt - 12, 12]).ISeparateurBas := false;
                                TCelluleImp(m_Grid.Objects[cpt_Elt - 12, 13]).ISeparateurBas := false;
                                        if cpt_Elt < 15 then
                                                Begin
                                                TCelluleImp(m_Grid.Objects[cpt_Elt - 12, 12]).ISeparateurDroite := false;
                                                TCelluleImp(m_Grid.Objects[cpt_Elt - 12, 13]).ISeparateurDroite := false;
                                                End;
                                End;
                End;

//        if ListDIM.Count > 15 then
                Begin
                        for cpt_Elt := 16 to Min(19, ListDIM.Count - 1) do
                                Begin
                                m_Grid.Objects[cpt_Elt - 16, 16] := TCelluleImp.Create(TDonneeImp(ListDIM[cpt_Elt]).Libelle, GetFontInfos, 0, clBlack, clSkyBlue);
                                m_Grid.Objects[cpt_Elt - 16, 17] := TCelluleImp.Create(TDonneeImp(ListDIM[cpt_Elt]).Valeur, GetFontInfos, 0, clBlack, clWhite);
                                End;

                        for cpt_Elt := Max(16, ListDIM.Count) to 19 do
                                Begin
                                m_Grid.Objects[cpt_Elt - 16, 16] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                                m_Grid.Objects[cpt_Elt - 16, 17] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
                                TCelluleImp(m_Grid.Objects[cpt_Elt - 16, 16]).ISeparateurBas := false;
                                TCelluleImp(m_Grid.Objects[cpt_Elt - 16, 17]).ISeparateurBas := false;
                                        if cpt_Elt < 19 then
                                                Begin
                                                TCelluleImp(m_Grid.Objects[cpt_Elt - 16, 16]).ISeparateurDroite := false;
                                                TCelluleImp(m_Grid.Objects[cpt_Elt - 16, 17]).ISeparateurDroite := false;
                                                End;
                                End;
                End;



        for cpt_Elt := 0 to 3 do
                Begin
                m_Grid.Objects[cpt_Elt, 2] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                m_Grid.Objects[cpt_Elt, 3] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
                m_Grid.Objects[cpt_Elt, 6] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                m_Grid.Objects[cpt_Elt, 7] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
                m_Grid.Objects[cpt_Elt, 10] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                m_Grid.Objects[cpt_Elt, 11] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
                m_Grid.Objects[cpt_Elt, 14] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                m_Grid.Objects[cpt_Elt, 15] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
//                m_Grid.Objects[cpt_Elt, 18] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
//                m_Grid.Objects[cpt_Elt, 19] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);


                        if cpt_Elt < 3 then
                                Begin
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 2]).ISeparateurDroite := False;
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 3]).ISeparateurDroite := False;
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 6]).ISeparateurDroite := False;
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 7]).ISeparateurDroite := False;
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 10]).ISeparateurDroite := False;
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 11]).ISeparateurDroite := False;
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 14]).ISeparateurDroite := False;
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 15]).ISeparateurDroite := False;
//                                TCelluleImp(m_Grid.Objects[cpt_Elt, 18]).ISeparateurDroite := False;
//                                TCelluleImp(m_Grid.Objects[cpt_Elt, 19]).ISeparateurDroite := False;
                                End
                        else
                                Begin
//                                TCelluleImp(m_Grid.Objects[cpt_Elt, 18]).ISeparateurDroite := False;
//                                TCelluleImp(m_Grid.Objects[cpt_Elt, 19]).ISeparateurDroite := False;
                                End;

                TCelluleImp(m_Grid.Objects[cpt_Elt, 2]).ISeparateurBas := False;
                TCelluleImp(m_Grid.Objects[cpt_Elt, 6]).ISeparateurBas := False;
                TCelluleImp(m_Grid.Objects[cpt_Elt, 10]).ISeparateurBas := False;
                TCelluleImp(m_Grid.Objects[cpt_Elt, 14]).ISeparateurBas := False;
//                TCelluleImp(m_Grid.Objects[cpt_Elt, 18]).ISeparateurBas := False;

                        if TCelluleImp(m_Grid.Objects[cpt_Elt, 4]).ITexte = '' then
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 3]).ISeparateurBas := False;

                        if TCelluleImp(m_Grid.Objects[cpt_Elt, 8]).ITexte = '' then
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 7]).ISeparateurBas := False;

                        if TCelluleImp(m_Grid.Objects[cpt_Elt, 12]).ITexte = '' then
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 11]).ISeparateurBas := False;

                        if TCelluleImp(m_Grid.Objects[cpt_Elt, 16]).ITexte = '' then
                                TCelluleImp(m_Grid.Objects[cpt_Elt, 15]).ISeparateurBas := False;

                End;


        if (VentilPhoto <> nil) then
                begin

                RectImpSchema.Left   := 0;
                RectImpSchema.Top    := 0;
                RectImpSchema.Right  := VentilSchema.Width;
                RectImpSchema.Bottom := VentilSchema.Height;

                m_Grid.Objects[4, 0] := TCelluleImp.Create('Dimensions (mm)', GetFontInfos, 0, clBlack, clSkyBlue);
                m_Grid.Objects[4, 1] := TCelluleImp.Create('', GetFontInfos, 100, clBlack, clWhite);
                TCelluleImp(m_Grid.Objects[4, 1]).IImage := TBitmap.Create;

                TCelluleImp(m_Grid.Objects[4, 1]).IImage.Width := RectImpSchema.Right;
                TCelluleImp(m_Grid.Objects[4, 1]).IImage.Height := RectImpSchema.Bottom;

                TCelluleImp(m_Grid.Objects[4, 1]).IImage.Canvas.StretchDraw(RectImpSchema, VentilSchema);

                TCelluleImp(m_Grid.Objects[4, 1]).IImageAdapt := true;
                TCelluleImp(m_Grid.Objects[4, 1]).IImageMergeable := false;
                TCelluleImp(m_Grid.Objects[4, 1]).IMerged := false;
                TCelluleImp(m_Grid.Objects[4, 1]).ISeparateurBas := false;
                TCelluleImp(m_Grid.Objects[4, 1]).IImageAdaptNbLignes := 50;

                        for cptrow := 2 to m_Grid.RowCount - 1 do
                                begin
                                m_Grid.Objects[4, cptrow] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clSkyBlue);
                                TCelluleImp(m_Grid.Objects[4, cptrow]).ISeparateurBas := false;
                                end;



                VentilSchema.Destroy;
                End;


        for cpt_Elt := 0 to 3 do
                Begin
                        if m_Grid.Objects[cpt_Elt, 0] = nil then
                                m_Grid.Objects[cpt_Elt, 0] := TCelluleImp.Create('', GetFontInfos, 100, clBlack, clWhite);
                TCelluleImp(m_Grid.Objects[cpt_Elt, 0]).ILargeur := 10;
                End;
                
TCelluleImp(m_Grid.Objects[4, 0]).ILargeur := 60;

ApercuImp_AjouteGrille_GestionImage(m_Grid, true, '', false, false, False, true);

//-----------partie R�glage ----------------
  m_Grid := TStringGrid.Create(nil);

  m_Grid.ColCount := 1;
  m_Grid.RowCount := 2;
  m_Grid.Objects[0, 0] := TCelluleImp.Create('r�glage : ' + TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.Position, GetFontInfos, 0, clBlack, clWhite);

        If (TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.Entrainement <> c_Edibatec_Caisson_Entrainement_Direct) and (TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.TypeSelection <> c_CaissonSelect_DebitCst) then
                TCelluleImp(m_Grid.Objects[0, 0]).ITexte := TCelluleImp(m_Grid.Objects[0, 0]).ITexte + ' tr/min';

        if TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.Position = '' then
                TCelluleImp(m_Grid.Objects[0, 0]).ITexte := '';

  m_Grid.Objects[0, 1] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);

  TCelluleImp(m_Grid.Objects[0, 0]).ISeparateurBas := False;
  TCelluleImp(m_Grid.Objects[0, 0]).IAlign := tacenter;

  ApercuImp_AjouteGrille(m_Grid, True, '', False, False, False, True);


//-----------destruction des �l�ments----------------
  for cpt_Elt := ListCALC.Count - 1 DownTo 0 do
        TDonneeImp(ListCALC[cpt_Elt]).Destroy;

  for cpt_Elt := ListELE.Count - 1 DownTo 0 do
        TDonneeImp(ListELE[cpt_Elt]).Destroy;

  for cpt_Elt := ListDIM.Count - 1 DownTo 0 do
        TDonneeImp(ListDIM[cpt_Elt]).Destroy;

  ListCALC.Destroy;
  ListELE.Destroy;
  ListDIM.Destroy;


//-----------partie courbes----------------
  Impression_CourbesCaisson;

End;
{ ************************************************************************************************************************************************* }
Procedure Impression_CourbesCaisson;
Var
  m_NoImg : Integer;
  RectImp  : TGmRect;
  Rect     : TRect;
  Stl_Impr : TStringList;
  XMax     : Real;
  YMax     : Real;
  m_Grid : TStringGrid;
  cptrow : integer;

Begin

  m_Grid := TStringGrid.Create(nil);
  m_Grid.ColCount := 2;
  m_Grid.RowCount := 28;

  m_Grid.Objects[0, 0] := TCelluleImp.Create('D�bit (m3/h) / Pression (Pa)', GetFontInfos, 0, clBlack, clSkyBlue);
  m_Grid.Objects[1, 0] := TCelluleImp.Create('D�bit (m3/h) / Puissance absorb�e (W)', GetFontInfos, 0, clBlack, clSkyBlue);
  m_Grid.Objects[0, 1] := TCelluleImp.Create('', GetFontInfos, 100, clBlack, clWhite);
  m_Grid.Objects[1, 1] := TCelluleImp.Create('', GetFontInfos, 100, clBlack, clWhite);


  if TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie = nil then exit;

  RectImp.Left   := 0;
  RectImp.Top    := 0;
  RectImp.Right  := 800;
  RectImp.Bottom := 600;
  Rect.Left      := Round(RectImp.Left);
  Rect.Right     := Round(RectImp.Right);
  Rect.Top       := Round(RectImp.Top);
  Rect.Bottom    := Round(RectImp.Bottom);
  XMax := 0;
  YMax := 0;


  Stl_Impr := GenerationPoints(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson), Rect, XMax, YMax);
        if Stl_Impr <> nil then
                begin
                TCelluleImp(m_Grid.Objects[0, 1]).IImage := DrawCourbeBezier(Stl_Impr, RectImp);
                end;
  Stl_Impr := GenerationPoints(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson), Rect, XMax, YMax, True);
          if Stl_Impr <> nil then
                begin
                TCelluleImp(m_Grid.Objects[1, 1]).IImage  := DrawCourbeBezier(Stl_Impr, RectImp);
                end;


   for cptrow := 2 to m_Grid.RowCount - 1 do
   begin
   m_Grid.Objects[0, cptrow] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
   m_Grid.Objects[1, cptrow] := TCelluleImp.Create('', GetFontInfos, 0, clBlack, clWhite);
   TCelluleImp(m_Grid.Objects[0, cptrow]).ISeparateurBas := false;
   TCelluleImp(m_Grid.Objects[1, cptrow]).ISeparateurBas := false;   
   end;                

  TCelluleImp(m_Grid.Objects[0, 1]).IImageAdapt := true;
  TCelluleImp(m_Grid.Objects[1, 1]).IImageAdapt := true;
  TCelluleImp(m_Grid.Objects[0, 1]).IImageMergeable := false;
  TCelluleImp(m_Grid.Objects[1, 1]).IImageMergeable := false;
  TCelluleImp(m_Grid.Objects[0, 1]).IMerged := false;
  TCelluleImp(m_Grid.Objects[1, 1]).IMerged := false;
  TCelluleImp(m_Grid.Objects[0, 1]).ISeparateurBas := false;
  TCelluleImp(m_Grid.Objects[1, 1]).ISeparateurBas := false;
  TCelluleImp(m_Grid.Objects[0, 1]).IImageAdaptNbLignes := 20;
  TCelluleImp(m_Grid.Objects[1, 1]).IImageAdaptNbLignes := 20;

  TCelluleImp(m_Grid.Objects[0, 1]).ILargeur := 50;
  TCelluleImp(m_Grid.Objects[1, 1]).ILargeur := 50;
                  
  ApercuImp_AjouteGrille_GestionImage(m_Grid, True, '', False, False, False, True);


  //l�gende
  m_Grid := TStringGrid.Create(nil);
  m_Grid.RowCount := 1;
  m_Grid.ColCount := 4;

  m_Grid.Objects[0, 0] := TCelluleImp.Create(#149 + ' Courbe mini', GetFontInfos, 0, clRed, clWhite);
  m_Grid.Objects[1, 0] := TCelluleImp.Create(#149 + ' Courbe s�lectionn�e', GetFontInfos, 0, clBlue, clWhite);
  m_Grid.Objects[2, 0] := TCelluleImp.Create(#149 + ' Courbe maxi', GetFontInfos, 0, clGreen, clWhite);

        if Etude.SaveOptions_SelectionCaisson.CheckBox_DTU_Checked then
                  m_Grid.Objects[3, 0] := TCelluleImp.Create(#149 + ' Points demand�s', GetFontInfos, 0, clBlack, clWhite)
        else
                  m_Grid.Objects[3, 0] := TCelluleImp.Create(#149 + ' Point demand�', GetFontInfos, 0, clBlack, clWhite);

  ApercuImp_AjouteGrille_GestionImage(m_Grid, False, '', False, False, False, False);


End;
{ ************************************************************************************************************************************************* }
Procedure SetDataImp_FicheCaissonOld;
Var
  m_Ventil: TEdibatec_Caisson;
//  m_DPQMax,
//  m_DPQMin : Real;
  m_NoImg  : Integer;
  VentilSchema : TJPegImage;
  VentilPhoto  : TJPegImage;

Begin
  m_Ventil:= TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie;
  FreeAndNil(Etude.DataImp_FicheCaisson);

  If m_Ventil = nil Then Exit;

  Etude.DataImp_FicheCaisson := TStringList.Create;
  Etude.DataImp_FicheCaisson.Add(cst_Titre1 + 'FicheCaisson');

  Etude.DataImp_FicheCaisson.Add(cst_Titre2 + m_Ventil.Reference);
  Etude.DataImp_FicheCaisson.Add(m_Ventil.Desc_Ventil);

  Etude.DataImp_FicheCaisson.Add('L5' + ';' + 'D�bit mini (m3/h);D�bit maxi (m3/h);D�bit moyen (m3/h);Puissance moyenne "ThC" (W);Coefficient RT;');
  Etude.DataImp_FicheCaisson.Add('L5' + ';' + IntToStr(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).Reseau_DebitMinimum) +';'+ IntToStr(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).Reseau_DebitMaximum) +';'
                                 + Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.QMoy, 2) +';'+ Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.PMoy, 2) +';'+ Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.CoeffRT, 2) +';');

  Etude.DataImp_FicheCaisson.Add('NO');


  DataImpCaractCaisson.GetDataImpElementsFromTypeVentil(Etude.DataImp_FicheCaisson,VentilPhoto, VentilSchema, m_Ventil.Type_Ventil);

  { Destruction �ventuelle des images d�j� pr�sentes }
  If Etude.DataImp_ImagesCaisson <> Nil Then Begin
    For m_NoImg := Etude.DataImp_ImagesCaisson.Count - 1 DownTo 0 Do
        Begin
                If Etude.DataImp_ImagesCaisson[m_NoImg] <> nil then
                        TJpegImage(Etude.DataImp_ImagesCaisson[m_NoImg]).Destroy;
        End;
    FreeAndNil(Etude.DataImp_ImagesCaisson);
  End;
  Etude.DataImp_ImagesCaisson := TList.Create;
  if (VentilSchema <> nil) and (VentilPhoto <> nil) then
  begin                                                                       
  Etude.DataImp_ImagesCaisson.Add(VentilSchema);
  Etude.DataImp_ImagesCaisson.Add(VentilPhoto);
  end;

End;
{ ************************************************************************************************************************************************* }
Procedure SetDataImp_FicheCaisson;
Var
  m_Ventil: TEdibatec_Caisson;
//  m_DPQMax,
//  m_DPQMin : Real;
  m_NoImg  : Integer;
  VentilSchema : TJPegImage;
  VentilPhoto  : TJPegImage;
  TempText1     : String;
  TempText2     : String;
  TempText3     : String;

Begin
  m_Ventil:= TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie;
  FreeAndNil(Etude.DataImp_FicheCaisson);

  If m_Ventil = nil Then Exit;

  Etude.DataImp_FicheCaisson := TStringList.Create;
  Etude.DataImp_FicheCaisson.Add(cst_Titre1 + 'FicheCaisson');

  Etude.DataImp_FicheCaisson.Add(cst_Titre2 + m_Ventil.Reference);
  Etude.DataImp_FicheCaisson.Add(m_Ventil.Desc_Ventil);

        if Etude.SaveOptions_SelectionCaisson.CheckBox_DTU_Checked then
                Begin
                TempText1 := 'L7;';
                TempText2 := 'L7;';
                TempText3 := 'L7;';

                TempText1 := TempText1 + ';';
                TempText2 := TempText2 + 'Mini;';
                TempText3 := TempText3 + 'Maxi;';

                TempText1 := TempText1 + 'D�bits "DTU" (m3/h);';
                TempText2 := TempText2 + IntToStr(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).Reseau_DebitMinimum) + ';';
                TempText3 := TempText3 + IntToStr(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).Reseau_DebitMaximum) + ';';

                TempText1 := TempText1 + 'Dp (Pa);';
                TempText2 := TempText2 + Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.DpCalcQMin, 0) + ';';
                TempText3 := TempText3 + Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.DpCalcQMax, 0) + ';';

                TempText1 := TempText1 + 'D�bits "RT" (m3/h);';
                TempText2 := TempText2 + Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.QMinRT, 0) + ';';
                TempText3 := TempText3 + Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.QMaxRT, 0) + ';';

                TempText1 := TempText1 + 'Puissances "RT" (m3/h);';
                TempText2 := TempText2 + Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.PMinRT, 0) + ';';
                TempText3 := TempText3 + Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.PMaxRT, 0) + ';';

                TempText1 := TempText1 + 'D�bit et Puissance Th-C;';
                TempText2 := TempText2 + Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.QMoy, 0) + ' m3/h;';
                TempText3 := TempText3 + Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.PMoy, 0) + ' W;';

                TempText1 := TempText1 + 'Conso. RT 2005 (W/m3/h);';
                TempText2 := TempText2 + Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.CoeffRT, 2) + ';';
                TempText3 := TempText3 + ';';

                Etude.DataImp_FicheCaisson.Add(TempText1);
                Etude.DataImp_FicheCaisson.Add(TempText2);
                Etude.DataImp_FicheCaisson.Add(TempText3);
                End
        else
                Begin
                TempText1 := 'L3;';
                TempText2 := 'L3;';

                TempText1 := TempText1 + 'D�bits "DTU" (m3/h);';
                TempText2 := TempText2 + IntToStr(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).Reseau_DebitMaximum) + ';';

                TempText1 := TempText1 + 'DP (Pa);';
                TempText2 := TempText2 + Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.DpCalcQMax, 0) + ';';

                TempText1 := TempText1 + 'Puissance (m3/h);';
                TempText2 := TempText2 + Float2Str(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.PMaxRT, 2) + ';';

                Etude.DataImp_FicheCaisson.Add(TempText1);
                Etude.DataImp_FicheCaisson.Add(TempText2);
                End;



  Etude.DataImp_FicheCaisson.Add('NO');

  DataImpCaractCaisson.GetDataImpElementsFromTypeVentil(Etude.DataImp_FicheCaisson,VentilPhoto, VentilSchema, m_Ventil.Type_Ventil);

  TempText1 := 'r�glage : ' + TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.Position;
        If (TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.Entrainement <> c_Edibatec_Caisson_Entrainement_Direct) and (TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.TypeSelection <> c_CaissonSelect_DebitCst) then
                TempText1 := TempText1 + ' tr/min';
        if TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.Position = '' then
                TempText1 := '';
  Etude.DataImp_FicheCaisson.Add('L1;'+TempText1+';');
  Etude.DataImp_FicheCaisson.Add('NO');

  { Destruction �ventuelle des images d�j� pr�sentes }
  If Etude.DataImp_ImagesCaisson <> Nil Then Begin
    For m_NoImg := Etude.DataImp_ImagesCaisson.Count - 1 DownTo 0 Do
        Begin
                If Etude.DataImp_ImagesCaisson[m_NoImg] <> nil then
                        TJpegImage(Etude.DataImp_ImagesCaisson[m_NoImg]).Destroy;
        End;
    FreeAndNil(Etude.DataImp_ImagesCaisson);
  End;
  Etude.DataImp_ImagesCaisson := TList.Create;
  if (VentilSchema <> nil) and (VentilPhoto <> nil) then
  begin                                                                       
  Etude.DataImp_ImagesCaisson.Add(VentilSchema);
  Etude.DataImp_ImagesCaisson.Add(VentilPhoto);
  end;

End;
{ ************************************************************************************************************************************************* }
Procedure SetDataImp_CourbesCaisson;
Var
  m_NoImg : Integer;
//  m_NoRes : Integer;
//  m_Img   : TJPEGImage;
//  m_List  : TList;

  RectImp  : TGmRect;
  Rect     : TRect;
  Stl_Impr : TStringList;
//  PenColor : TColor;
  XMax     : Real;
  YMax     : Real;
//  Stl      : TBrushStyle;
  Bmp      : TBitmap;
  JpgCourbeDp    : TJpegImage;
  JpgCourbePuiss : TJpegImage;

Begin

  FreeAndNil(Etude.DataImp_CourbesCaisson);

  if TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie = nil then exit;

  RectImp.Left   := 0;
  RectImp.Top    := 0;
  RectImp.Right  := 800;
  RectImp.Bottom := 600;
  Rect.Left       := Round(RectImp.Left);
  Rect.Right      := Round(RectImp.Right);
  Rect.Top        := Round(RectImp.Top);
  Rect.Bottom     := Round(RectImp.Bottom);
  XMax := 0;
  YMax := 0;

  JpgCourbeDp := nil;
  JpgCourbePuiss := nil;

  Stl_Impr := GenerationPoints(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson), Rect, XMax, YMax);
        if Stl_Impr <> nil then
                begin
                Bmp := DrawCourbeBezier(Stl_Impr, RectImp);
                JpgCourbeDp := TJPEGImage.Create;
                JpgCourbeDp.Assign(Bmp);
                FreeAndNil(Stl_Impr);
                FreeAndNil(Bmp);
                end;
  Stl_Impr := GenerationPoints(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson), Rect, XMax, YMax, True);
          if Stl_Impr <> nil then
                begin
                Bmp := DrawCourbeBezier(Stl_Impr, RectImp);
                JpgCourbePuiss := TJPEGImage.Create;
                JpgCourbePuiss.Assign(Bmp);
                FreeAndNil(Stl_Impr);
                FreeAndNil(Bmp);                
                end;

  { Destruction �ventuelle des images d�j� pr�sentes }
  If Etude.DataImp_CourbesCaisson <> Nil Then Begin
    For m_NoImg := Etude.DataImp_CourbesCaisson.Count - 1 DownTo 0 Do
        Begin
                If Etude.DataImp_CourbesCaisson[m_NoImg] <> nil then
                        TJpegImage(Etude.DataImp_CourbesCaisson[m_NoImg]).Destroy;
        End;
  End;
  Etude.DataImp_CourbesCaisson := TList.Create;
  Etude.DataImp_CourbesCaisson.Add(JpgCourbeDp);
  Etude.DataImp_CourbesCaisson.Add(JpgCourbePuiss);

End;
{ ************************************************************************************************************************************************* }
Procedure SetDataImp_ObjetAcoustique(Const _Objet: TVentil_Bouche);
Const
        ColHeaders : Array[1..7] Of string = ('63 Hz', '125 Hz', '250 Hz', '500 Hz', '1 kHz', '2 kHz', '4 kHz') ;
Var
        NbColonnes   : String;
//        m_NoFils     : Integer;
        m_Frequence  : Integer;
        VolumePiece  : Single;
        LpG          : Single;
        ImpTxtHeader : String;
        ImpTxtL1     : String;
        ImpTxtL2     : String;
        ImpTxtL3     : String;
        ImpTxtL4     : String;
        ImpTxtL5     : String;
        ImpTxtL6     : String;
        ImpTxtL7     : String;
        ImpTxtL8     : String;
        ImpTxtL9     : String;
        ImpTxtL10    : String;
        ImpTxtLpG    : String;
        ImpTxtVolume : String;
        ImpTxtNRA    : String;
        ArrayL1      : Array[1..7] of Single;
        ArrayL2      : Array[1..7] of Single;
        ArrayL3      : Array[1..7] of Single;
        ArrayL4      : Array[1..7] of Single;
        ArrayL5      : Array[1..7] of Single;
        ArrayL6      : Array[1..7] of Single;
        ArrayL7      : Array[1..7] of Single;
        ArrayL8      : Array[1..7] of Single;
        ArrayL9      : Array[1..7] of Single;
        ArrayL10     : Array[1..7] of Single;
Begin

VolumePiece := 0;

if Etude.Batiment.IsFabricantVIM then
Begin
        if (_Objet <> nil) and (_Objet.TypePiece = c_cuisine) then
                Begin

                        If IsNotNil(_Objet.Logement) then
                                VolumePiece := TVentil_Bouche(_Objet).Logement.InfosPieces[_Objet.TypePiece, 1] * TVentil_Bouche(_Objet).Logement.InfosPieces[_Objet.TypePiece, 2];

                        For m_Frequence := Low((_Objet.LienCad As IAero_Bouche).IAero_BoucheBase_GetLp) + 1 to High((_Objet.LienCad As IAero_Bouche).IAero_BoucheBase_GetLp) - 1 do
                                Begin
                                ArrayL1[m_Frequence]  := TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).IAero_Fredo_Caisson_GetSpectreAcoustique[m_Frequence];
                                ArrayL2[m_Frequence]  := TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).IAero_Fredo_Caisson_GetSpectreAcoustique[m_Frequence] - (_Objet.LienCad As IAero_Bouche).IAero_BoucheBase_GetLwAmont[m_Frequence];
                                ArrayL3[m_Frequence]  := (_Objet.LienCad As IAero_Bouche).IAero_BoucheBase_GetLwAmont[m_Frequence] - (_Objet.LienCad As IAero_Bouche).IAero_BoucheBase_GetLw[m_Frequence];
                                ArrayL4[m_Frequence]  := 10 * Log10(TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie.DiametreAspiration / _Objet.Diametre);
                                ArrayL5[m_Frequence]  := ArrayL1[m_Frequence] - (ArrayL2[m_Frequence] + ArrayL3[m_Frequence] + ArrayL4[m_Frequence]);
                                ArrayL6[m_Frequence]  := (_Objet.LienCad As IAero_Bouche).IAero_BoucheBase_GetLw[m_Frequence];
                                ArrayL7[m_Frequence]  := SommeLog10([ArrayL5[m_Frequence], ArrayL6[m_Frequence]], 0, 1);
                                ArrayL8[m_Frequence]  := -10 * Log10(VolumePiece / 12.5);
                                ArrayL9[m_Frequence]  := ct_PonderationA[m_Frequence - 1];
                                ArrayL10[m_Frequence] := ArrayL7[m_Frequence] + ArrayL8[m_Frequence] + ArrayL9[m_Frequence];
                                End;

                NbColonnes   := 'L' + IntToStr(High((_Objet.LienCad As IAero_Bouche).IAero_BoucheBase_GetLp) + 1);
                ImpTxtHeader := NbColonnes + cst_ImpSep;
                ImpTxtL1     := NbColonnes + 'Lw rayonn� en conduit' + cst_ImpSep;
                ImpTxtL2     := NbColonnes + 'Att�nuations du r�seau' + cst_ImpSep;
                ImpTxtL3     := NbColonnes + 'Att. Bouche / R�seau' + cst_ImpSep;
                ImpTxtL4     := NbColonnes + '10 Log(S1/S2)' + cst_ImpSep;
                ImpTxtL5     := NbColonnes + 'Lw du r�seau = 1 -(2+3+4)' + cst_ImpSep;
                ImpTxtL6     := NbColonnes + 'Lw de la bouche' + cst_ImpSep;
                ImpTxtL7     := NbColonnes + 'Lw TOTAL = SLog(5;6)' + cst_ImpSep;
                ImpTxtL8     := NbColonnes + '10 Log(V/12.5)' + cst_ImpSep;
                ImpTxtL9     := NbColonnes + 'Correctif --> Db(A)' + cst_ImpSep;
                ImpTxtL10    := NbColonnes + 'Lp Global en dB(A)' + cst_ImpSep;

                        For m_Frequence := Low((_Objet.LienCad As IAero_Bouche).IAero_BoucheBase_GetLp) + 1 to High((_Objet.LienCad As IAero_Bouche).IAero_BoucheBase_GetLp) - 1 do
                                Begin
                                ImpTxtHeader := ImpTxtHeader + ColHeaders[m_Frequence] + cst_ImpSep;
                                ImpTxtL1  := ImpTxtL1  + Float2Str(ArrayL1[m_Frequence] , 0) + cst_ImpSep;
                                ImpTxtL2  := ImpTxtL2  + Float2Str(ArrayL2[m_Frequence] , 0) + cst_ImpSep;
                                ImpTxtL3  := ImpTxtL3  + Float2Str(ArrayL3[m_Frequence] , 0) + cst_ImpSep;
                                ImpTxtL4  := ImpTxtL4  + Float2Str(ArrayL4[m_Frequence] , 0) + cst_ImpSep;
                                ImpTxtL5  := ImpTxtL5  + Float2Str(ArrayL5[m_Frequence] , 0) + cst_ImpSep;
                                ImpTxtL6  := ImpTxtL6  + Float2Str(ArrayL6[m_Frequence] , 0) + cst_ImpSep;
                                ImpTxtL7  := ImpTxtL7  + Float2Str(ArrayL7[m_Frequence] , 0) + cst_ImpSep;
                                ImpTxtL8  := ImpTxtL8  + Float2Str(ArrayL8[m_Frequence] , 0) + cst_ImpSep;
                                ImpTxtL9  := ImpTxtL9  + Float2Str(ArrayL9[m_Frequence] , 0) + cst_ImpSep;
                                ImpTxtL10 := ImpTxtL10 + Float2Str(ArrayL10[m_Frequence], 0) + cst_ImpSep;
                                End;


                                LpG := SommeLog10(ArrayL10, Low(ArrayL10) + 1, High(ArrayL10) - 1);
                                ImpTxtLpG := cst_TxtGras + 'Lp Global en dB(A) : ' + Float2Str(LpG, 0) + ' dB(A)';

                                If IsNotNil(_Objet.Logement) then
                                        Begin
                                        ImpTxtVolume := cst_TxtNorm + 'Volume de la cuisine ';
                                                If _Objet.Logement.CuisineFermee = C_Oui then
                                                        ImpTxtVolume := ImpTxtVolume + 'ferm�e : '
                                                Else
                                                        ImpTxtVolume := ImpTxtVolume + 'ouverte : ';

                                        ImpTxtVolume := ImpTxtVolume + Float2Str(VolumePiece, 0) + ' m3.';
                                        End
                                Else
                                        ImpTxtVolume := '';


                                        If IsNotNil(_Objet.Logement) then
                                                Begin
                                                        If (_Objet.Logement.CuisineFermee = C_Oui) And (LpG <= 35) then
                                                                ImpTxtNRA := cst_TxtGras + 'Cette installation REPOND aux exigences de la N.R.A.'
                                                        Else
                                                        If (_Objet.Logement.CuisineFermee = C_Non) And (LpG <= 30) then
                                                                ImpTxtNRA := cst_TxtGras + 'Cette installation REPOND aux exigences de la N.R.A.'
                                                        Else
                                                                ImpTxtNRA := cst_TxtGras + 'Cette installation NE REPOND PAS aux exigences de la N.R.A.';

                                                End
                                        Else
                                                ImpTxtNRA := cst_TxtGras + 'Cette installation NE REPOND PAS aux exigences de la N.R.A.';



                                Etude.DataImp_Acoustique.Add(cst_TxtNorm + 'Calculs effectu�s selon les indications du cahier technique N� 1876 du C.S.T.B.');
                                Etude.DataImp_Acoustique.Add(ImpTxtHeader);
                                Etude.DataImp_Acoustique.Add(ImpTxtL1);
                                Etude.DataImp_Acoustique.Add(ImpTxtL2);
                                Etude.DataImp_Acoustique.Add(ImpTxtL3);
                                Etude.DataImp_Acoustique.Add(ImpTxtL4);
                                Etude.DataImp_Acoustique.Add(ImpTxtL5);
                                Etude.DataImp_Acoustique.Add(ImpTxtL6);
                                Etude.DataImp_Acoustique.Add(ImpTxtL7);
                                Etude.DataImp_Acoustique.Add(ImpTxtL8);
                                Etude.DataImp_Acoustique.Add(ImpTxtL9);
                                Etude.DataImp_Acoustique.Add(ImpTxtL10);
                                Etude.DataImp_Acoustique.Add(ImpTxtLpG);
                                Etude.DataImp_Acoustique.Add(ImpTxtVolume);
                                Etude.DataImp_Acoustique.Add(ImpTxtNRA);
                                Etude.DataImp_Acoustique.Add(cst_TxtNorm + 'Le Lw rayonn� en conduit par le ventilateur a �t� estim� suivant les indications du cahier technique N� 1876 du C.S.T.B.');
                End;

End;

End;
{ ************************************************************************************************************************************************* }
Procedure SetDataImp_Acoustique;
Var
        m_Ventil         : TEdibatec_Caisson;
        MyBoucheDefav    : TVentil_Bouche;
        MyHasCuisineVol0 : Boolean;

Begin
  FreeAndNil(Etude.DataImp_Acoustique);

  m_Ventil:= TVentil_Caisson(Etude.Batiment.Reseaux.GetCaisson).ProduitAssocie;
  If m_Ventil = nil Then Exit;

  MyBoucheDefav := nil;
  MyHasCuisineVol0 := False;
  GetBoucheCuisineDefav(Etude.Batiment.Reseaux.GetCaisson, MyBoucheDefav, MyHasCuisineVol0);

        if (MyBoucheDefav = nil) or (MyHasCuisineVol0) then
                exit;

  Etude.DataImp_Acoustique := TStringList.Create;
  Etude.DataImp_Acoustique.Add(cst_Titre1 + 'Calculs acoustiques');
  SetDataImp_ObjetAcoustique(MyBoucheDefav);

End;
{ ************************************************************************************************************************************************* }
Procedure Impression_PageDeGarde(_ImpressionEnCours: Boolean; _Lanceur: TForm);
Var
  m_Grid: TStringGrid;
Begin
  If IsNil(Etude) Then Exit;
  ApercuImp_Debut(_Lanceur, 'DONNEES GENERALES',
                  Etude.Batiment.Reference, GetPiedDePage, Not _ImpressionEnCours);

  {$IfDef ANJOS_OPTIMA3D}
  ImpressionHautDePageAnjos;
  {$EndIf}

  m_Grid := TStringGrid.Create(nil);
  m_Grid.ColCount := 3;
  {$IfDef ANJOS_OPTIMA3D}
  m_Grid.RowCount := 9;
  {$Else}
  m_Grid.RowCount := 8;
  {$EndIf}                  

  m_Grid.Objects[0, 0] := TCelluleImp.Create('Nom: ' , GetFontInfos, 15, clBlack, clWhite, [fsBold, fsUnderLine], taLeftJustify);
  m_Grid.Objects[1, 0] := TCelluleImp.Create(Etude.Reference, GetFontInfos, 0, clBlack, clWhite, [fsBold], taLeftJustify);
  m_Grid.Objects[2, 0] := TCelluleImp.Create('' , GetFontInfos, 45, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[0, 1] := TCelluleImp.Create('Date: ' , GetFontInfos, 15, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[1, 1] := TCelluleImp.Create(DateTimeToStr(Etude.DateEtude), GetFontInfos, 50, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[2, 1] := TCelluleImp.Create('' , GetFontInfos, 45, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[0, 2] := TCelluleImp.Create('Adresse: ', GetFontInfos, 15, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[1, 2] := TCelluleImp.Create(Etude.Adresse, GetFontInfos, 50, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[2, 2] := TCelluleImp.Create('' , GetFontInfos, 45, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[0, 3] := TCelluleImp.Create('Descriptif', GetFontInfos, 15, clWhite, clWhite, [], taLeftJustify);
  m_Grid.Objects[1, 3] := TCelluleImp.Create(Etude.Descriptif, GetFontInfos, 50, clWhite, clWhite, [], taLeftJustify);
  m_Grid.Objects[2, 3] := TCelluleImp.Create('' , GetFontInfos, 45, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[0, 4] := TCelluleImp.Create('Ma�tre d''ouvrage: ', GetFontInfos, 15, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[1, 4] := TCelluleImp.Create(Etude.MOuvrage, GetFontInfos, 50, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[2, 4] := TCelluleImp.Create('' , GetFontInfos, 45, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[0, 5] := TCelluleImp.Create('Ma�tre d''oeuvre: ', GetFontInfos, 15, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[1, 5] := TCelluleImp.Create(Etude.MOeuvre , GetFontInfos, 50, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[2, 5] := TCelluleImp.Create('' , GetFontInfos, 45, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[0, 6] := TCelluleImp.Create('Installateur: ', GetFontInfos, 15, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[1, 6] := TCelluleImp.Create(Etude.Installateur , GetFontInfos, 50, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[2, 6] := TCelluleImp.Create('' , GetFontInfos, 45, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[0, 7] := TCelluleImp.Create('Objet: ', GetFontInfos, 15, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[1, 7] := TCelluleImp.Create(Etude.Objet, GetFontInfos, 50, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[2, 7] := TCelluleImp.Create('' , GetFontInfos, 45, clBlack, clWhite, [], taLeftJustify);
  {$IfDef ANJOS_OPTIMA3D}
  m_Grid.Objects[0, 8] := TCelluleImp.Create('Remarques:', GetFontInfos, 50, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[1, 8] := TCelluleImp.Create(Etude.Remarques.Text, GetFontInfos, 50, clBlack, clWhite, [], taLeftJustify);
  m_Grid.Objects[2, 8] := TCelluleImp.Create('' , GetFontInfos, 45, clBlack, clWhite, [], taLeftJustify);  

  {$EndIf}

  ApercuImp_AjouteGrille(m_Grid, False, '', False, False, False, False);

  {$IfDef ANJOS_OPTIMA3D}
  ImpressionBasDePageAnjos;
  {$EndIf}

  If Not _ImpressionEnCours Then ApercuImp_Show;
End;
{ ************************************************************************************************************************************************* }
Function GetPiedDePage : String;
Begin
Result := ' ';
{$IfDef ANJOS_OPTIMA3D}
        if Trim(Etude.Reference) <> '' then
                Result := Trim(Etude.Reference);

        if Trim(DateToStr(Etude.DateEtude)) <> '' then
                Begin
                        if (Result <> '') then
                                Result := Result + ' - ';
                Result := Result + Trim(DateToStr(Etude.DateEtude));
                end;
{$EndIf}
End;
{ ************************************************************************************************************************************************* }
Procedure SetDataImp;
Begin

  If Etude.Batiment.Reseaux.count = 0 Then Exit;
  FStepProgress('Dossier - Gen');
  SetDataImp_DonneesGenerale;
  FStepProgress('Dossier - ATech');
  SetDataImp_DonneesAvisTechniques;
  FStepProgress('Dossier - EA');
  SetDataImp_DonneesEntreesAir;
  FStepProgress('Dossier - R�sultats');
  SetDataImp_CalculRetour;

  FStepProgress('Dossier - Fiche Caisson');
  SetDataImp_FicheCaisson;

  FStepProgress('Dossier - Courbes Caisson');
  SetDataImp_CourbesCaisson;

  FStepProgress('Dossier - Acoustique');
  SetDataImp_Acoustique;

  FStepProgress('Dossier - Sch�mas');
  SetDataImp_Schemas;
End;
{ ************************************************************************************************************************************************** }

End.
