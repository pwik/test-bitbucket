Unit Ventil_ApercuAvantImpressions;

{$Include '..\Ventil_Directives.pas'}

Interface

Uses
  Windows, BBS_Message, SysUtils, Variants, Classes, Graphics, Controls, Forms, Math,
  Dialogs, GmTypes, GmClasses, GmPageNavigator, ExtCtrls, GmCanvas,
  GmGridPrint, GmRtfPreview, Menus, GmThumbnails, GmPageList, GmPreview, Grids,
  ImgList, ComCtrls, ToolWin,
  jpeg;

Const
  c_CouleurFond = clInfoBk;
  {$IfDef ANJOS_OPTIMA3D}
        {$IfDef CALADAIR_VMC}
        MaxHeightLogo = 100;
        {$Else}
        MaxHeightLogo = 25;
        {$EndIf}
  {$Else}
  MaxHeightLogo = 100;
  {$EndIf}

Type
{ ************************************************************************************************************************************************** }
  TSommaireImp = Class(TStringList)
  Public
    Function Add(Value:String):Integer; reintroduce;
  End;
{ ************************************************************************************************************************************************** }
  TDlg_ApercuImpressions = class(TForm)
    Grid_Print: TGmGridPrint;
    Panel1: TPanel;
    GmPageNavigator1: TGmPageNavigator;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    PrintDialog: TPrintDialog;
    Apercu: TGmPreview;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    Grid_Exchange: TStringGrid;
    Vignettes: TGmThumbnails;
    ImageList1: TImageList;
    Procedure ToolButton1Click(Sender: TObject);
    Procedure ToolButton2Click(Sender: TObject);
    Procedure ToolButton5Click(Sender: TObject);
    Procedure ApercuDrawFooter(Sender: TObject; ARect: TGmValueRect; ACanvas: TGmCanvas);
    procedure ApercuPageChanged(Sender: TObject);
    procedure ApercuNewPage(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Grid_PrintFinishGrid(Sender: TObject; YPos: TGmValue);
    procedure Grid_PrintGetRowHeight(Sender: TObject; Row: Integer; RowHeight: TGmValue);
    procedure GestionImage_Grid_PrintGetRowHeight(Sender: TObject; Row: Integer; RowHeight: TGmValue);
    procedure Grid_PrintDrawCell(Sender: TObject; Col, Row: Integer; ARect: TGmValueRect; ACanvas: TGmCanvas);
    procedure GestionImage_Grid_PrintDrawCell(Sender: TObject; Col, Row: Integer; ARect: TGmValueRect; ACanvas: TGmCanvas);
    procedure Grid_PrintGridNewPage(Sender: TObject; var ATopMargin, ABottomMargin: TGmValue);
  Private
    OwnerForm         : TForm;
    FooterList        : Array Of String;
    FillInProgress    : Boolean; 
    LastPageNum       : SmallInt;
    PageOnCreate      : Boolean;
    TempTitle         : String;
    Procedure AddHeader(_Str: String);
    Procedure AddFooter(_Str: String; _Visible: Boolean);
    Procedure AddTitle (_Str: String; _WithBorder: Boolean = True);
    Procedure AddGridTitle(_Str: String; _WithBorder: Boolean);
    Procedure SetOrientation(_Page: TgmPage; _Portrait: Boolean);
    Procedure PrintGrid;
    Procedure PrintCartouche(_Center: Boolean);
    Procedure AutoNewPageImp(_Portrait: Boolean);
    Procedure GetGrid2Print(_Grid: TStringGrid);
    Function  NbEmptyLines(_Grid: TStringGrid): Integer;
    Function  IsEmptyLine(_Grid: TStringGrid; _Row: Integer): Boolean;
    Function  GetTextHeight(_Texte: String; _ColWidth: Extended; _Font: TFont): Extended;
    Procedure PrintLogo;
  Public
    Logo      : TBitmap;
    CurrentYPosOnPage : Extended;
  End;
{ ************************************************************************************************************************************************** }
{ Pour D�marrer le remplissage d'une page }
Procedure ApercuImp_Debut(_Lanceur: TForm;                   { Fen�tre parente pour la fiche - nil si ce n'est pas la premi�re page }
                          _Titre: String;                    { Titre de la page  }
                          _EnTete: String;                   { Texte de l'en-t�te pour tout l'aper�u }
                          _PiedDePage: String;               { Texte du pied de page pour tout l'aper�u }
                          _PremierePage: Boolean = True;     { Premi�re page de l'aper�u ie il n'a pas encore �t� cr�� }
                          _Portrait: Boolean = True;         { Page en mode portrait }
                          _PiedDePagePresent: Boolean = True;{ Pied de page pr�sent }
                          _NoPremPage: Integer = 1);         { Num�ro de la premi�re page si diff�rent de 1 }

{ Lance l'aper�u avant impression }
Procedure ApercuImp_Show;

{ Ajoute un tableur dans l'apercu en passant la grille � imprimer en param�tre }
Procedure ApercuImp_AjouteGrille(_Grille: TStringGrid;
                                 _QuadrillageVertical: Boolean = True;    { Affichage des lignes verticales }
                                 _TitreAvant: String = '';                { Impression d'un titre au dessus  du tableau }
                                 _Nue: Boolean = False;
                                 _CartouchePossible: Boolean= True;
                                 _ForceCartouche: Boolean = False;
                                 _Quadrillagehoritontal: Boolean = False);

Procedure ApercuImp_AjouteGrille_GestionImage(_Grille: TStringGrid;
                                 _QuadrillageVertical: Boolean = True;    { Affichage des lignes verticales }
                                 _TitreAvant: String = '';                { Impression d'un titre au dessus  du tableau }
                                 _Nue: Boolean = False;
                                 _CartouchePossible: Boolean= True;
                                 _ForceCartouche: Boolean = False;
                                 _Quadrillagehoritontal: Boolean = False);


Procedure ApercuImp_SauteLigne(_NbLignes: SmallInt);                        { Saute Nb Lignes apres le dernier element insere }
Procedure ApercuImp_NouvellePage(_Portrait: Boolean = True);                { Ajout d'une page � l'aper�u courant }
Procedure ApercuImp_TraitSeparation(_Texte : String);                       { Ajoute un trait de s�paration sur la largeur de la page }
Procedure ApercuImp_LigneTitreGauche(_Texte: String);                       { Ajoute une ligne de titre avec texte � gauche }
Procedure ApercuImp_Titre(_Texte: String);                                  { Ajoute un titre }
Procedure ApercuImp_Sommaire(_Sommaire: TSommaireImp; _Deb: Integer = 1);   { Ajoute un sommaire en premiere page }
Procedure ApercuImp_PageDeGarde;                                          { Insertion page de garde }
Function  ApercuImp_NombrePages : Integer;                                { Nombre total de pages de l'aper�u }
Procedure ApercuImp_AjouteBitmapRtfForCurrentPage(PrintLogo : boolean = false; ScaleValue: double = -1);                        { Insertion des images pour le rtf }
Function  GetFontInfos: TFont;
Function NextColHasPic(_Grid: TStringGrid; _Col, _Row: Integer): Boolean;

Var
  Dlg_ApercuImpressions: TDlg_ApercuImpressions;

Implementation
Uses
  GmFuncs,
  BBS_Progress,
  Ventil_Utils, Ventil_Declarations, Ventil_Const,
  {$IfDef PRINTRTF}
  BBSRTF,
  BBSImpressions,
  {$EndIf}

  {$IfDef VIM_OPTAIR}
  {$IfnDef AERAU_CLIMAWIN}
  {$IfDef MVN_MVNAIR}
  MVN_Main,
  {$Else}
  Main,
  {$EndIf}
  {$EndIf}
  {$Else}
        {$IfDef FRANCEAIR_COLLECTAIR}
  COLLECTAIR_MAIN,
        {$Else}
                {$IfDef OUESTVENTIL}
  OuestVentil3D_Main,
                {$Else}
                        {$IfDef CALADAIR_VMC}
                        Caladair_Main,
                        {$Else}
                        {$IfDef LINDAB_VMC}
                        Lindab_Main,
                        {$Else}
                        Optima3D_Main,
                        {$EndIf}
                        {$EndIf}                        
                {$EndIf}
        {$EndIf}
  {$EndIf}
  GmObjects,
  printers;
{$R *.dfm}

{ ************************************************************************************************************************************************** }
Procedure ApercuImp_Debut(_Lanceur: TForm; _Titre: String; _EnTete: String; _PiedDePage: String; _PremierePage: Boolean = True;
                          _Portrait: Boolean = True; _PiedDePagePresent: Boolean = True; _NoPremPage: Integer = 1);
var
        ConditionLogoPerso : Boolean;
Begin

  If _PremierePage Then Begin
    If IsNotNil(_Lanceur) Then LockWindowUpdate(_Lanceur.Handle); { Pour le flash d'apparition de la fen�tre }
    Application.CreateForm(TDlg_ApercuImpressions, Dlg_ApercuImpressions);
    Dlg_ApercuImpressions.Logo := TBitmap.Create;


  {$IfnDef AERAU_CLIMAWIN}
ConditionLogoPerso := not(Options.LogoOriginal);
{$IfDef VIM_OPTAIR}
{$IfNDef MVN_MVNAIR}
ConditionLogoPerso := ConditionLogoPerso And MainForm.VersionAutonome;
{$EndIf}
{$EndIf}
  
        if ConditionLogoPerso then
                begin
                        if FileExists(ExtractFilePath(Application.Exename) + 'Bibliotheque\Logo.bmp') then
                                begin
                                Dlg_ApercuImpressions.Logo := TBitmap.Create;
                                Dlg_ApercuImpressions.Logo.LoadFromFile(ExtractFilePath(Application.Exename) + 'Bibliotheque\Logo.bmp');
                                end
                        else
                                begin
                                Dlg_ApercuImpressions.Logo := nil;
                                end;
                end
        else
 {$EndIf}

                begin
                Dlg_ApercuImpressions.Logo := TBitmap.Create;
                Dlg_ApercuImpressions.Logo.LoadFromFile(ExtractFilePath(Application.Exename) + 'Bibliotheque\Progress.bmp');
                end;

    Dlg_ApercuImpressions.OwnerForm      := _Lanceur;
    Dlg_ApercuImpressions.FillInProgress := True;
    Dlg_ApercuImpressions.LastPageNum    := 0;
    Dlg_ApercuImpressions.Show;
    If IsNotNil(_Lanceur) Then Begin
      _Lanceur.Hide;
      LockWindowUpdate(0);
    End;
    Dlg_ApercuImpressions.Repaint;
    Application.ProcessMessages;
    Dlg_ApercuImpressions.Apercu.BeginUpdate;
    If _EnTete <> '' Then Dlg_ApercuImpressions.AddHeader(_EnTete);
    Dlg_ApercuImpressions.Apercu.SetNoPageUn(_NoPremPage);
//    Dlg_ApercuImpressions.PrintLogo;
  {$IfDef PRINTRTF}
    RTF_BeginDoc(_Titre, _EnTete, _PiedDePage, _PremierePage, _Portrait);
  {$EndIf}
  End;
  Dlg_ApercuImpressions.SetOrientation(Dlg_ApercuImpressions.Apercu.CurrentPage, _Portrait);
  Dlg_ApercuImpressions.PrintLogo;  
{$IfDef ANJOS_OPTIMA3D}
  Dlg_ApercuImpressions.TempTitle := _Titre;
{$EndIf}  
  If _PiedDePage <> '' Then Dlg_ApercuImpressions.AddFooter(_PiedDePage, _PiedDePagePresent);
  If _Titre  <> '' Then Dlg_ApercuImpressions.AddTitle(_Titre);
  
End;
{ ************************************************************************************************************************************************** }
Procedure ApercuImp_Show;
Begin
  If IsNotNil(Dlg_ApercuImpressions) Then Begin
    Dlg_ApercuImpressions.Apercu.GmPrinter.Title := cst_NomImpression;
    Dlg_ApercuImpressions.FillInProgress := False;
    Dlg_ApercuImpressions.Apercu.CurrentPageNum := 1;
    Dlg_ApercuImpressions.ApercuPageChanged(Dlg_ApercuImpressions.Apercu);
    Dlg_ApercuImpressions.Apercu.EndUpdate;
    Dlg_ApercuImpressions.Apercu.SetFocus;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure ApercuAvantImpression(_Owner: TForm = Nil);
Begin
  Application.CreateForm(TDlg_ApercuImpressions, Dlg_ApercuImpressions);
  Dlg_ApercuImpressions.OwnerForm := _Owner;
  If _Owner <> Nil Then _Owner.Hide;
  Dlg_ApercuImpressions.ShowModal;
  If _Owner <> Nil Then _Owner.ShowModal;
  FreeAndNil(Dlg_ApercuImpressions);
End;
{ ************************************************************************************************************************************************** }
Procedure ApercuImp_AjouteGrille(_Grille: TStringGrid; _QuadrillageVertical: Boolean = True; _TitreAvant: String = '';  _Nue: Boolean = False;
                                 _CartouchePossible: Boolean= True; _ForceCartouche: Boolean = False; _Quadrillagehoritontal: Boolean = False);
Begin
  If IsNotNil(Dlg_ApercuImpressions) Then Begin
    If _TitreAvant <> '' Then Dlg_ApercuImpressions.AddGridTitle(_TitreAvant, Not _Nue);
    Dlg_ApercuImpressions.GetGrid2Print(_Grille);
    If _QuadrillageVertical Then Dlg_ApercuImpressions.Grid_Print.GridOptions := Dlg_ApercuImpressions.Grid_Print.GridOptions + [gmVertLine]
                           Else Dlg_ApercuImpressions.Grid_Print.GridOptions := Dlg_ApercuImpressions.Grid_Print.GridOptions - [gmVertLine];
    If _Quadrillagehoritontal Then Dlg_ApercuImpressions.Grid_Print.GridOptions := Dlg_ApercuImpressions.Grid_Print.GridOptions + [gmHorzLine]
                              Else Dlg_ApercuImpressions.Grid_Print.GridOptions := Dlg_ApercuImpressions.Grid_Print.GridOptions - [gmHorzLine];
    If _Nue Then Dlg_ApercuImpressions.Grid_Print.GridOptions := Dlg_ApercuImpressions.Grid_Print.GridOptions - [gmGridBorder]
            Else Dlg_ApercuImpressions.Grid_Print.GridOptions := Dlg_ApercuImpressions.Grid_Print.GridOptions + [gmGridBorder];
    If (_ForceCartouche) Or ((Dlg_ApercuImpressions.Grid_Exchange.ColCount < 2) And (_CartouchePossible)) Then Dlg_ApercuImpressions.PrintCartouche(True)
                                                                                                          Else Dlg_ApercuImpressions.PrintGrid;
  {$IfDef PRINTRTF}
  RTF_AddGridSimpleDPE(_Grille, _QuadrillageVertical, _Quadrillagehoritontal);
  {$EndIf}
  End;


End;
{ ************************************************************************************************************************************************** }
Procedure ApercuImp_AjouteGrille_GestionImage(_Grille: TStringGrid; _QuadrillageVertical: Boolean = True; _TitreAvant: String = '';  _Nue: Boolean = False;
                                 _CartouchePossible: Boolean= True; _ForceCartouche: Boolean = False; _Quadrillagehoritontal: Boolean = False);
Begin

If IsNotNil(Dlg_ApercuImpressions) Then
Begin

Dlg_ApercuImpressions.Grid_Print.OnGetRowHeight := Dlg_ApercuImpressions.GestionImage_Grid_PrintGetRowHeight;
Dlg_ApercuImpressions.Grid_Print.OnDrawCell := Dlg_ApercuImpressions.GestionImage_Grid_PrintDrawCell;

    If _TitreAvant <> '' Then Dlg_ApercuImpressions.AddGridTitle(_TitreAvant, Not _Nue);
//    Dlg_ApercuImpressions.GetGrid2Print_GestionImage(_Grille);
    Dlg_ApercuImpressions.GetGrid2Print(_Grille);
    If _QuadrillageVertical Then Dlg_ApercuImpressions.Grid_Print.GridOptions := Dlg_ApercuImpressions.Grid_Print.GridOptions + [gmVertLine]
                           Else Dlg_ApercuImpressions.Grid_Print.GridOptions := Dlg_ApercuImpressions.Grid_Print.GridOptions - [gmVertLine];
    If _Quadrillagehoritontal Then Dlg_ApercuImpressions.Grid_Print.GridOptions := Dlg_ApercuImpressions.Grid_Print.GridOptions + [gmHorzLine]
                              Else Dlg_ApercuImpressions.Grid_Print.GridOptions := Dlg_ApercuImpressions.Grid_Print.GridOptions - [gmHorzLine];
    If _Nue Then Dlg_ApercuImpressions.Grid_Print.GridOptions := Dlg_ApercuImpressions.Grid_Print.GridOptions - [gmGridBorder]
            Else Dlg_ApercuImpressions.Grid_Print.GridOptions := Dlg_ApercuImpressions.Grid_Print.GridOptions + [gmGridBorder];
    If (_ForceCartouche) Or ((Dlg_ApercuImpressions.Grid_Exchange.ColCount < 2) And (_CartouchePossible)) Then Dlg_ApercuImpressions.PrintCartouche(True)
                                                                                                          Else Dlg_ApercuImpressions.PrintGrid;
  {$IfDef PRINTRTF}
  RTF_AddGridSimpleDPE(_Grille, _QuadrillageVertical, _Quadrillagehoritontal);
  {$EndIf}


Dlg_ApercuImpressions.Grid_Print.OnGetRowHeight := Dlg_ApercuImpressions.Grid_PrintGetRowHeight;
Dlg_ApercuImpressions.Grid_Print.OnDrawCell := Dlg_ApercuImpressions.Grid_PrintDrawCell;

End;
End;
{ ************************************************************************************************************************************************** }
Procedure ApercuImp_SauteLigne(_NbLignes: SmallInt);
Begin

  If IsNotNil(Dlg_ApercuImpressions) Then Begin
    If Dlg_ApercuImpressions.CurrentYPosOnPage + 30 * _NbLignes < (Dlg_ApercuImpressions.Apercu.PageHeight[gmMillimeters] -
                                                                  Dlg_ApercuImpressions.Apercu.Margins.Top.AsMillimeters -
                                                                  Dlg_ApercuImpressions.Apercu.Margins.Bottom.AsMillimeters) Then
      Dlg_ApercuImpressions.CurrentYPosOnPage := Dlg_ApercuImpressions.CurrentYPosOnPage + 3 * _NbLignes
    Else Dlg_ApercuImpressions.AutoNewPageImp(Dlg_ApercuImpressions.Apercu.CurrentPage.Orientation = gmPortrait);
  {$IfDef PRINTRTF}
  RTF_SauteLigne(_NbLignes);
  {$EndIf}
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure ApercuImp_NouvellePage(_Portrait: Boolean = True);
Begin

  If IsNotNil(Dlg_ApercuImpressions) Then Begin
    Dlg_ApercuImpressions.PageOnCreate := True;
    Dlg_ApercuImpressions.Apercu.NewPage;
    Dlg_ApercuImpressions.SetOrientation(Dlg_ApercuImpressions.Apercu.CurrentPage, _Portrait);
    Dlg_ApercuImpressions.CurrentYPosOnPage := 10 + Dlg_ApercuImpressions.Apercu.Margins.Top.AsMillimeters + Dlg_ApercuImpressions.Apercu.Header.Height[GmMillimeters];
  {$IfDef PRINTRTF}
    RTF_NewPageImp(_Portrait);
  {$EndIf}
  End;

End;
{ ************************************************************************************************************************************************** }
Procedure ApercuImp_TraitSeparation(_Texte : String);
Var
  m_Font : TFont;
Begin

  If IsNotNil(Dlg_ApercuImpressions) Then Begin
    If _Texte <> '' Then Begin
      Dlg_ApercuImpressions.CurrentYPosOnPage := Dlg_ApercuImpressions.CurrentYPosOnPage + 2;
      m_Font := Dlg_ApercuImpressions.Apercu.Canvas.Font;
      Dlg_ApercuImpressions.Apercu.Canvas.Font.Name := 'arial';
      Dlg_ApercuImpressions.Apercu.Canvas.Font.Size := 6;
      Dlg_ApercuImpressions.Apercu.Canvas.Font.Style := [fsItalic];
      Dlg_ApercuImpressions.Apercu.Canvas.Brush.Color := clWhite;
      Dlg_ApercuImpressions.Apercu.Canvas.TextOut(Dlg_ApercuImpressions.Apercu.Margins.Left.AsMillimeters, Dlg_ApercuImpressions.CurrentYPosOnPage, _Texte, gmMillimeters);
      Dlg_ApercuImpressions.Apercu.Canvas.Font := m_Font;
    End;
    Dlg_ApercuImpressions.Apercu.Canvas.Line(Dlg_ApercuImpressions.Apercu.Margins.Left.AsMillimeters,
                                       Dlg_ApercuImpressions.CurrentYPosOnPage + 3,
                                       Dlg_ApercuImpressions.Apercu.PageWidth[gmMillimeters] - Dlg_ApercuImpressions.Apercu.Margins.Right.AsMillimeters,
                                       Dlg_ApercuImpressions.CurrentYPosOnPage + 3, gmMillimeters);
    Dlg_ApercuImpressions.CurrentYPosOnPage := Dlg_ApercuImpressions.CurrentYPosOnPage + 3;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure ApercuImp_LigneTitreGauche(_Texte: String);
Var
  m_Font : TFont;
  m_Grid : TStringGrid;
Begin

  If IsNotNil(Dlg_ApercuImpressions) Then Begin
    m_Grid := TStringGrid.Create(Nil);
    m_grid.ColCount := 1;
    m_Grid.RowCount := 1;
    m_Font := TFont.Create;
    m_Font.Name := 'arial';
    m_Font.Size := 6;
    m_Font.Style := [fsBold, fsItalic];
    m_Grid.Objects[0, 0] := TCelluleImp.Create(_Texte, m_Font, 100, clBlack, ClSkyBlue, m_Font.Style, taLeftJustify);
    ApercuImp_AjouteGrille(m_Grid, True, '', True, False);
    Dlg_ApercuImpressions.CurrentYPosOnPage := Dlg_ApercuImpressions.CurrentYPosOnPage + 3;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure ApercuImp_Titre(_Texte: String);
Begin
  If IsNotNil(Dlg_ApercuImpressions) Then Dlg_ApercuImpressions.AddTitle(_Texte);
End;
{ ************************************************************************************************************************************************** }
Procedure ApercuImp_Sommaire(_Sommaire: TSommaireImp; _Deb: Integer = 1);
Var
  m_Grid    : TStringGrid;
  m_No      : Integer;
  m_Page    : Integer;
  m_NbPages : Integer;
  m_Hauteur : Real;
  m_Nb      : Real;
  m_AsInches: TGmSize;
Begin

//AjouteSommaireImp(_Sommaire, _Deb);
//exit;

  If IsNotNil(Dlg_ApercuImpressions) And IsNoTNil(_Sommaire) And (_Sommaire.Count > 0) Then Begin
    Dlg_ApercuImpressions.PrintLogo;
    m_AsInches := Dlg_ApercuImpressions.Apercu.CurrentPage.PageSize[gmInches];
    m_Hauteur  := m_AsInches.Height - (Dlg_ApercuImpressions.Apercu.Margins.Bottom.AsInches + Dlg_ApercuImpressions.Apercu.Footer.Height[gmInches]);
    m_Nb := Dlg_ApercuImpressions.Grid_Print.Grid.RowHeights[0] / Screen.PixelsPerInch;
    m_Nb := (_Sommaire.Count  + 2) * m_Nb * 2;
    m_Nb := m_Nb / m_Hauteur;
    If Frac(m_Nb) > 0 Then m_NbPages := Round(Int(m_Nb)) + 1
                      Else m_NbPages := Round(Int(m_Nb));
    m_Page := _Deb + m_NbPages - 1;
    Dlg_ApercuImpressions.Apercu.InsertPage(0);
    Dlg_ApercuImpressions.Apercu.ModeInsertion := True;
    Dlg_ApercuImpressions.Apercu.NoInsertion   := 1;
    Dlg_ApercuImpressions.Apercu.CurrentPageNum := 1;
    Dlg_ApercuImpressions.AddTitle('Sommaire', True);
    m_Grid := TStringGrid.Create(Nil);
    m_Grid.ColCount := 2;
    m_Grid.RowCount := _Sommaire.Count + 1;
    m_Grid.Objects[0, 0] := TCelluleImp.Create('Titre', GetFontInfos, 90, ClWhite, clMedGray, [fsBold], taCenter);
    m_Grid.Objects[1, 0] := TCelluleImp.Create('Page n�', GetFontInfos, 10, ClWhite, clMedGray, [fsBold], taRightJustify);
    For m_No := 0 To _Sommaire.Count - 1 Do Begin
      If Odd(M_No)
        Then m_Grid.Objects[0, m_No + 1] := TCelluleImp.Create(GetSubStr(_Sommaire[m_No], 1, '|'), GetFontInfos, 90, ClBlack, clLtGray, [],
                                                               taLeftJustify)
        Else m_Grid.Objects[0, m_No + 1] := TCelluleImp.Create(GetSubStr(_Sommaire[m_No], 1, '|'), GetFontInfos, 90, ClBlack, clWhite, [],
                                                               taLeftJustify);
      m_Page := m_Page + 1;
      If Odd(M_No)
        Then m_Grid.Objects[1, m_No + 1] := TCelluleImp.Create( IntToStr(m_Page), GetFontInfos, 10, ClBlack, clLtGray, [], taRightJustify)
        Else m_Grid.Objects[1, m_No + 1] := TCelluleImp.Create( IntToStr(m_Page), GetFontInfos, 10, ClBlack, clWhite, [], taRightJustify);
      m_Page := _Deb + m_NbPages - 1+ Str2Int(GetSubStr(_Sommaire[m_No], 2, '|'));
    End;
    ApercuImp_AjouteGrille(m_Grid, False, '', True);
    FreeAndNil(m_Grid);
    Dlg_ApercuImpressions.Apercu.ModeInsertion := False;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure ApercuImp_PageDeGarde;
Begin


  If IsNotNil(Dlg_ApercuImpressions) Then Begin
    Dlg_ApercuImpressions.Apercu.InsertPage(0);
    Dlg_ApercuImpressions.Apercu.CurrentPageNum := 1;
  End;
End;
{ ************************************************************************************************************************************************** }
Function  ApercuImp_NombrePages : Integer;
Begin

  Result := 0;
  If IsNotNil(Dlg_ApercuImpressions) Then Result := Dlg_ApercuImpressions.Apercu.CurrentPageNum;
End;
{ ************************************************************************************************************************************************** }
Procedure ApercuImp_AjouteBitmapRtfForCurrentPage(PrintLogo : boolean = false; ScaleValue: double = -1);
var
  m_Count : integer;
  isLogo  : boolean;
Begin
    isLogo := true;
            for m_Count := 0 to Dlg_ApercuImpressions.Apercu.CurrentPage.Count - 1 do
                if Dlg_ApercuImpressions.Apercu.CurrentPage.GmObject[m_Count].InheritsFrom(TGmGraphicObject) then
                        if isLogo and not(PrintLogo) then
                                isLogo := false
                        else
                                Begin
//                                RTF_SauteLigne;
                                {$IfDef PRINTRTF}
                                RTF_AddImage(TBitmap(TGmGraphicObject(Dlg_ApercuImpressions.Apercu.CurrentPage.GmObject[m_Count]).Graphic), ScaleValue);
                                RTF_SauteLigne;
                                {$EndIf}                                
                                        if PrintLogo then
                                                exit;
                                End;
End;
{ ************************************************************************************************************************************************** }
Function GetFontInfos: TFont;
Var
  m_Font : TFont;
Begin
  m_Font := TFont.Create;
  m_Font.Size := 6;
  m_Font.Name := 'arial';
  Result := m_Font;
End;
{ ************************************************************************************************************************************************** }
Function NextColHasPic(_Grid: TStringGrid; _Col, _Row: Integer): Boolean;
Var
  NoRow     : SmallInt;
Begin
  Result := False;
  If _Col < _Grid.ColCount Then Begin
    For NoRow := 0 To _Row Do If TCelluleImp(_Grid.Objects[_Col, NoRow]).IImage <> Nil Then Begin
      Result := True;
      Break;
    End;
  End;
End;
{ ************************************************************************************************************************************************** }

{ ******************************************************************* TDlg_ApercuImpressions ***************************************************************** }
Procedure TDlg_ApercuImpressions.ToolButton1Click(Sender: TObject);
Begin
  Close;
End;
{ ************************************************************************************************************************************************** }
procedure TDlg_ApercuImpressions.ToolButton2Click(Sender: TObject);
begin
  {$IfDef PRINTRTF}
RTF_ShowDoc;
exit;
  {$EndIf}
  
  PrintDialog.MinPage  := 1;
  PrintDialog.FromPage := 1;
  PrintDialog.MaxPage  := Apercu.NumPages;
  PrintDialog.ToPage   := Apercu.NumPages;
  If PrintDialog.Execute Then Begin
    FBeginProgress(PrintDialog.ToPage - PrintDialog.FromPage, 'Impression en cours');
    Apercu.PrintRange(PrintDialog.FromPage, PrintDialog.ToPage);
    FEndProgress;
  End;
end;
{ ************************************************************************************************************************************************** }
procedure TDlg_ApercuImpressions.ToolButton5Click(Sender: TObject);
begin
  Vignettes.Visible := Not Vignettes.Visible;
  ToolButton5.Down  := not ToolButton5.Down;
  ToolButton4.Down  := not ToolButton4.Down;
  Apercu.FitWholePage;
end;
{ ************************************************************************************************************************************************** }
Procedure TDlg_ApercuImpressions.ApercuDrawFooter(Sender: TObject; ARect: TGmValueRect; ACanvas: TGmCanvas);
Var
  m_OldSize  : SmallInt;
  m_OldStyle : TFontStyles;
Begin
  m_OldSize := ACanvas.Font.Size;
  ACanvas.Font.Size := 6;
  ACanvas.Pen.Mode := pmBlack;
  m_OldStyle := ACanvas.Font.Style;
  ACanvas.Font.Style := [];
{
  If FooterList[Apercu.CurrentPageNum - 1] <> '' Then
    ACanvas.TextOut(ARect.Left.AsMillimeters, ARect.Top.AsMillimeters, FooterList[Apercu.CurrentPageNum - 1], GmMillimeters);
}
  Apercu.Footer.CaptionLeft.Caption := FooterList[Apercu.CurrentPageNum - 1];
  ACanvas.Font.Size  := m_OldSize;
  ACanvas.Font.Style := m_OldStyle;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_ApercuImpressions.ApercuPageChanged(Sender: TObject);
Begin
 If Apercu.CurrentPageNum > LastPageNum Then LastPageNum := Apercu.CurrentPageNum;
 If Not FillInProgress Then Caption := 'Aper�u avant impression - page ' + IntToStr(Apercu.CurrentPageNum) + ' / ' + IntToStr(LastPageNum)
                       Else Caption := 'Aper�u avant impression - g�n�ration de la page ' + IntToStr(Apercu.CurrentPageNum);
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_ApercuImpressions.ApercuNewPage(Sender: TObject);
Begin

{$IfDef ANJOS_OPTIMA3D}
Apercu.Margins.Top.AsMillimeters := 5;
Apercu.Margins.Bottom.AsMillimeters := 5;
Apercu.Margins.Left.AsMillimeters := 5;
Apercu.Margins.Right.AsMillimeters := 5;
{$EndIf}

  SetLength(FooterList, Length(FooterList) + 1);
  If Not PageOnCreate And (High(FooterList) > 0) And (FooterList[High(FooterList) - 1] <> '') Then FooterList[High(FooterList)] := FooterList[High(FooterList) - 1];
  PageOnCreate := False;
  PrintLogo;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_ApercuImpressions.FormClose(Sender: TObject; var Action: TCloseAction);
Begin
  If IsNotNil(OwnerForm) Then Begin
    OwnerForm.Show;
    OwnerForm.BringToFront;
    OwnerForm.SetFocus;
  End;
  Action := caFree;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_ApercuImpressions.Grid_PrintFinishGrid(Sender: TObject; YPos: TGmValue);
Begin
  CurrentYPosOnPage := YPos.AsMillimeters;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_ApercuImpressions.Grid_PrintGetRowHeight(Sender: TObject; Row: Integer; RowHeight: TGmValue);
Var
  Largeur : Extended;
  ColNo   : SmallInt;
  HtDefaut: Extended;
Begin
  ColNo := 0;
  HtDefaut := Grid_Print.Grid.RowHeights[Row] / Screen.PixelsPerInch;
  RowHeight.AsInches := HtDefaut;
  While ColNo < Grid_Print.Grid.ColCount Do Begin
    Largeur := ConvertValue(Grid_Print.GetColWidth(ColNo, gmPixels), gmPixels, gmInches);
    If (Grid_Print.Grid.Objects[ColNo, Row] <> Nil) And (Grid_Print.Grid.Objects[ColNo, Row].InheritsFrom(TCelluleImp)) Then Begin
        If TCelluleImp(Grid_Print.Grid.Objects[ColNo, Row]).IMerged Then
          Largeur := Largeur + ConvertValue(Grid_Print.GetColWidth(ColNo + 1, gmPixels), gmPixels, gmInches);
        If TCelluleImp(Grid_Print.Grid.Objects[ColNo, Row]).ITexte <> '' Then Begin
          RowHeight.AsInches := Max(GetTextHeight(TCelluleImp(Grid_Print.Grid.Objects[ColNo, Row]).ITexte, Largeur, TCelluleImp(Grid_Print.Grid.Objects[ColNo, Row]).IFont),
                                    RowHeight.AsInches);
        End;
    End;
    Inc(ColNo);
  End;
End;
{ ************************************************************************************************************************************************** }
procedure TDlg_ApercuImpressions.GestionImage_Grid_PrintGetRowHeight(Sender: TObject; Row: Integer; RowHeight: TGmValue);
Var
  Largeur : Extended;
  ColNo   : SmallInt;
  ColNo2   : SmallInt;
  HtDefaut: Extended;
  i : integer;
  TempRowHeight : Extended;
  TempText, TempTextReference : String;
  TempPos : Integer;
Const
//  EpsilonLargeur = 0.2;
  EpsilonLargeur = - 0.2;
begin

  ColNo := 0;
  HtDefaut := Grid_Print.Grid.RowHeights[Row] / Screen.PixelsPerInch;
  RowHeight.AsInches := HtDefaut;
  While ColNo < Grid_Print.Grid.ColCount Do Begin
    Largeur := ConvertValue(Grid_Print.GetColWidth(ColNo, gmPixels), gmPixels, gmInches);
    If (Grid_Print.Grid.Objects[ColNo, Row] <> Nil) And (Grid_Print.Grid.Objects[ColNo, Row].InheritsFrom(TCelluleImp)) Then Begin
        i := ColNo;
        while (i < Grid_Print.Grid.ColCount) and (Grid_Print.Grid.Objects[i, Row] <> nil) and (TCelluleImp(Grid_Print.Grid.Objects[i, Row]).IMerged) do
                begin
                Largeur := Largeur + ConvertValue(Grid_Print.GetColWidth(i, gmPixels), gmPixels, gmInches);
                inc(i);
                end;
        {
        If TCelluleImp(GridPrint.Grid.Objects[ColNo, Row]).IMerged Then
          Largeur := Largeur + ConvertValue(GridPrint.GetColWidth(ColNo + 1, gmPixels), gmPixels, gmInches);
        }
        
        if (TCelluleImp(Grid_Print.Grid.Objects[ColNo, Row]).IHorMerged) and (TCelluleImp(Grid_Print.Grid.Objects[ColNo, Row]).ITexte <> '') and ((Row = 0) or not(TCelluleImp(Grid_Print.Grid.Objects[ColNo, Row - 1]).IHorMerged))then
                begin
                RowHeight.AsInches := GetTextHeight(TCelluleImp(Grid_Print.Grid.Objects[ColNo, Row]).ITexte, Largeur, TCelluleImp(Grid_Print.Grid.Objects[ColNo, Row]).IFont);
                        for ColNo2 := 0 to Grid_Print.Grid.ColCount - 1 do    // Iterate
                          begin
                          if (TCelluleImp(Grid_Print.Grid.Objects[ColNo2, Row]).ITexte <> '') then
                                begin
                                TempRowHeight := 0;
                                TempPos := Pos('#$D#$A', TCelluleImp(Grid_Print.Grid.Objects[ColNo2, Row]).ITexte);
                                TempTextReference := TCelluleImp(Grid_Print.Grid.Objects[ColNo2, Row]).ITexte;
                                        while TempPos > 0 do
                                                begin
                                                TempText := Copy(TempTextReference, 0, TempPos);
                                                TempRowHeight := TempRowHeight + GetTextHeight(TempText, Largeur - EpsilonLargeur, TCelluleImp(Grid_Print.Grid.Objects[ColNo2, Row]).IFont);
                                                TempTextReference := Copy(TempTextReference, TempPos, Length(TempTextReference) - 1);
                                                TempPos := Pos('#$D#$A', TempTextReference);
                                                end;

                                TempRowHeight := TempRowHeight + GetTextHeight(TempTextReference, Largeur - EpsilonLargeur, TCelluleImp(Grid_Print.Grid.Objects[ColNo2, Row]).IFont);

                                RowHeight.AsInches := Min(TempRowHeight,
                                                          RowHeight.AsInches);
                                end;
                          end;    // for
                exit;
                end;

        If TCelluleImp(Grid_Print.Grid.Objects[ColNo, Row]).ITexte <> '' Then
                Begin
                TempRowHeight := 0;
                TempPos := Pos(#13#10, TCelluleImp(Grid_Print.Grid.Objects[ColNo, Row]).ITexte);
                TempTextReference := TCelluleImp(Grid_Print.Grid.Objects[ColNo, Row]).ITexte;
                        while TempPos > 0 do
                                begin
                                TempText := Copy(TempTextReference, 0, TempPos - 1);
                                TempRowHeight := TempRowHeight + GetTextHeight(TempText, Largeur - EpsilonLargeur, TCelluleImp(Grid_Print.Grid.Objects[ColNo, Row]).IFont);
                                TempTextReference := Copy(TempTextReference, TempPos + 3, Length(TempTextReference));
                                TempPos := Pos(#13#10, TempTextReference);
                                end;

                TempRowHeight := TempRowHeight + GetTextHeight(TempTextReference, Largeur - EpsilonLargeur, TCelluleImp(Grid_Print.Grid.Objects[ColNo, Row]).IFont);

                RowHeight.AsInches := Max(TempRowHeight, RowHeight.AsInches);

                End;
    End;
    Inc(ColNo);
  End;
end;
{ ************************************************************************************************************************************************** }
Procedure TDlg_ApercuImpressions.Grid_PrintGridNewPage(Sender: TObject; var ATopMargin, ABottomMargin: TGmValue);
Begin
  ATopMargin.AsMillimeters := 5 + Apercu.Margins.Top.AsMillimeters + Apercu.Header.Height[GmMillimeters];
{$IfDef ANJOS_OPTIMA3D}
        If Trim(TempTitle) <> '' then
                Begin
                ATopMargin.AsMillimeters := ATopMargin.AsMillimeters + 15;
                ApercuImp_Titre(TempTitle);
                End;
{$EndIf}  
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_ApercuImpressions.Grid_PrintDrawCell(Sender: TObject; Col, Row: Integer; ARect: TGmValueRect; ACanvas: TGmCanvas);
Var
  Cellule     : TCelluleImp;
  CellulePrec : TCelluleImp;
  Haut        : Extended;
  Droite      : Extended;
  Obj         : TObject;
Begin
  Obj := Grid_Print.Grid.Objects[Col, Row];
  CellulePrec := Nil;
  If (Obj <> Nil) And (Obj.InheritsFrom(TCelluleImp)) Then Begin

    Cellule := TCelluleImp(Obj);
    If Row > 0 Then CellulePrec := TCelluleImp(Grid_Print.Grid.Objects[Col, Row - 1]);

    ACanvas.Font.Style  := Cellule.IFont.Style;
    ACanvas.Brush.Color := Cellule.IBrushColor;
    ACanvas.Pen.Color   := Cellule.IPenColor;
    ACanvas.Pen.Style   := psClear;

    If Cellule.IMerged Then Droite := ARect.Right.AsInches + Grid_Print.GetColWidth(Col + 1, gmInches)
                       Else Droite := ARect.Right.AsInches;

    If (CellulePrec <> Nil) And (CellulePrec.IHorMerged) Then Haut := ARect.Top.AsInches - Grid_Print.GetRowHeight(Row - 1, gmInches)
                                                         Else Haut := ARect.Top.AsInches;

   If (Cellule.Itexte <> '') Then Begin
     If (Cellule.IAngle <> 0) And (Droite<> ARect.Left.AsInches) Then Begin
       ACanvas.TextBoxExt(ARect.Left.AsInches, ARect.Top.AsInches, Droite, ARect.Bottom.AsInches, 0.025, '', Cellule.IAlign, gmTop, gmInches);
       case Cellule.IAlign of
         taCenter : droite := ((droite+ARect.Left.AsInches)+ConvertValue(Cellule.IFont.Height,gmpixels,gminches))/2-0.025;
         taLeftJustify : droite := ARect.Left.AsInches;
         taRightJustify :  droite := droite+ConvertValue(Cellule.IFont.Height,gmpixels,gminches)-0.025;
       end;
       ACanvas.RotateOut(droite, ARect.Bottom.AsInches, Cellule.IAngle, Cellule.ITexte, gminches);
     End Else ACanvas.TextBoxExt(ARect.Left.AsInches, Haut, Droite, ARect.Bottom.AsInches, 0.025, Cellule.ITexte, Cellule.IAlign, gmMiddle, gmInches);
    End;
   End Else Begin
    If (Row = 0) Then
    Begin
     ACanvas.Brush.Color:= $00E1E1E1;// clBtnFace;
     ACanvas.Font.Style := [fsBold];
    End
    Else
    Begin
     If Grid_Print.Grid.Objects[0, Row] <> Nil Then ACanvas.Brush.Color := $00FF8000
                                                  Else If  Grid_Print.Grid.Objects[1, Row] <> Nil Then ACanvas.Brush.Color := ClMoneyGreen
                                                  Else If  Grid_Print.Grid.Objects[2, Row] <> Nil Then ACanvas.Brush.Color := ClSkyBlue
                                                  Else ACanvas.Brush.Color := ClWhite;
     ACanvas.Font.Style := [];
    End;
    ACanvas.Pen.Color := ClBlack;
    ACanvas.TextBoxExt(ARect.Left.AsInches, ARect.Top.AsInches, ARect.Right.AsInches, ARect.Bottom.AsInches,
                       0.025, Grid_Print.Grid.Cells[Col,Row], taCenter, gmMiddle, gmInches);
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_ApercuImpressions.GestionImage_Grid_PrintDrawCell(Sender: TObject; Col, Row: Integer; ARect: TGmValueRect; ACanvas: TGmCanvas);
Var
  Cellule       : TCelluleImp;
  Droite, Bas        : Extended;
  CellValueRect : TGmValueRect;
  i : integer;
  ImgDep, ImgScale : Extended;
Begin
  If Grid_Print.Grid.Objects[Col, Row] <> Nil Then Begin
    Cellule := TCelluleImp(Grid_Print.Grid.Objects[Col, Row]);
    ACanvas.Font.Style  := Cellule.IFont.Style;
    ACanvas.Font.Size   := Cellule.IFont.Size;
    ACanvas.Font.Name   := Cellule.IFont.Name;
    ACanvas.Brush.Color := Cellule.IBrushColor;
    ACanvas.Font.Color   := Cellule.IPenColor;
    ACanvas.Pen.Style   := psClear;
    If Cellule.IImage = Nil Then Begin
      If TCelluleImp(Grid_Print.Grid.Objects[Col, Row]).IMerged Then Begin
        CellValueRect := TGmValueRect.Create;
        CellValueRect.AsInchRect := GmRect(ARect.Right.AsInches, ARect.Top.AsInches, ARect.Right.AsInches - ARect.Left.AsInches, ARect.Bottom.AsInches);
        i := Col ;
        Droite := ARect.Right.AsInches;
        while (TCelluleImp(Grid_Print.Grid.Objects[i, Row]).IMerged) and (i < Grid_Print.Grid.ColCount-1) do
                begin
                //Droite := Droite + (ARect.Right.AsInches - ARect.Left.AsInches);
                Droite := Droite + Grid_Print.GetColWidth(i + 1, gmInches);
                inc(i);
                end;
//        Droite := ARect.Right.AsInches + 2*(ARect.Right.AsInches - ARect.Left.AsInches);
      End Else Droite := ARect.Right.AsInches;
      If TCelluleImp(Grid_Print.Grid.Objects[Col, Row]).IHorMerged Then Begin
        CellValueRect := TGmValueRect.Create;
        CellValueRect.AsInchRect := GmRect(Droite, ARect.Top.AsInches, Droite - ARect.Left.AsInches, ARect.Bottom.AsInches);
        i := Row;
        Bas := ARect.Bottom.AsInches;
        while (TCelluleImp(Grid_Print.Grid.Objects[Col, i]).IHorMerged) and (i < Grid_Print.Grid.RowCount-1) do
                begin
                //Droite := Droite + (ARect.Right.AsInches - ARect.Left.AsInches);
                Bas := Bas + Grid_Print.GetRowHeight(i, gmInches);
                inc(i);
                end;
//        Droite := ARect.Right.AsInches + 2*(ARect.Right.AsInches - ARect.Left.AsInches);
      End Else Bas := ARect.Bottom.AsInches;
      If (Cellule.ITexte <> '') And (Cellule.ITexte <> '*||*') Then
        ACanvas.TextBoxExt(ARect.Left.AsInches, ARect.Top.AsInches, Droite, Bas, 0.025, Cellule.ITexte, Cellule.IAlign, gmTop, gmInches)
    End Else If Not Cellule.IImgDrawn Then Begin
      If Cellule.IImage.Width > 0 Then
      begin
        i := Col;
        Droite := ARect.Right.AsInches;

                while (TCelluleImp(Grid_Print.Grid.Objects[i, Row]).IMerged) and (i < Grid_Print.Grid.ColCount-1) do
                        begin
                        Droite := Droite + Grid_Print.GetColWidth(i + 1, gmInches);
                        inc(i);
                        end;
                                                
              if Cellule.IImageAdapt then
                        //ImgScale := Cellule.GetImgScaleComplete(ConvertValue(Droite - ARect.Left.AsInches, gmInches, gmPixels), GridPrint.GetRowHeight(Row, gmPixels))
                        ImgScale := Cellule.GetImgScaleComplete(ConvertValue(Droite - ARect.Left.AsInches, gmInches, gmPixels), 14.7{GridPrint.GetRowHeight(Row, gmPixels) }* Cellule.IImageAdaptNbLignes)
              else
              if Cellule.IImageMergeable then
                        ImgScale := Cellule.GetImgScale(ConvertValue(Droite - ARect.Left.AsInches - 1, gmInches, gmPixels))
              else
                        ImgScale := Cellule.GetImgScale(Grid_Print.GetColWidth(Col, gmPixels));

                case Cellule.IAlign of
                      taLeftJustify : begin
                                      ImgDep := ARect.Left.AsInches;
                                      end;
                      taRightJustify : begin
                                       ImgDep := Droite - ConvertValue(ImgScale * Cellule.IImage.Width, gmPixels, gmInches);
                                       end;
                      taCenter : begin
                                 ImgDep := ((ARect.Left.AsInches + Droite) - ConvertValue(ImgScale * Cellule.IImage.Width, gmPixels, gmInches)) / 2;
                                 end;
                     else ImgDep := 0;            
                end;

                //s�curit� : au cas o� l'alignement de l'image se fait avant la cellule, on fait un alignement gauche par d�faut
                if ARect.Left.AsInches > ImgDep then
                        ImgDep := ARect.Left.AsInches;

                if not Cellule.IImageAdapt then
                        ACanvas.Draw(ImgDep, ARect.Top.AsInches + 0.1, Cellule.IImage,
                                     ImgScale, gmInches)
                else
                        ACanvas.Draw(ImgDep, ARect.Top.AsInches, Cellule.IImage,
                                     ImgScale, gmInches)

      end;
      Cellule.IImgDrawn := True;
    End;
  End;
End;




{ ************************************************************************************************************************************************** }
Procedure TDlg_ApercuImpressions.AddHeader(_Str: String);
var
        DateVersion : String;
        DateJour : String;
Begin
  Apercu.Header.CaptionLeft.Font.Size := 6;
  Apercu.Header.CaptionLeft.Caption   := _Str;

  Apercu.Header.CaptionRight.Font.Size := 6;
  Apercu.Canvas.Font.Color := clBlack;
  {$IfDef AERAU_CLIMAWIN}
  Apercu.Header.CaptionRight.Caption  := 'CLIMAWIN';
  {$Else}
  {$IfDef FRANCEAIR_COLLECTAIR}
  Apercu.Header.CaptionRight.Caption  := 'COLLECTAIR';
  {$Else}
    {$IfDef CALADAIR_VMC}
     Apercu.Header.CaptionRight.Caption  := 'BATIVENT';
    {$Else}
        DateVersion:=FormatDateTime('dd mmmm yyyy', FileDateToDateTime(FileAge(Application.EXEName)));
        DateJour :=  FormatDateTime('dd mmmm yyyy', Now);
        {$IfDef ANJOS_OPTIMA3D}
        Apercu.Header.CaptionRight.Caption  := 'OPTIMA3D - version du ' + DateVersion +', imprim� le ' + DateJour;
        {$Else}
        Apercu.Header.CaptionRight.Caption  := 'OPTAIR - version du ' + DateVersion;
        {$EndIf}
    {$EndIf}
  {$EndIf}
  {$EndIf}

{$IfNDef ANJOS_OPTIMA3D}
  If (Logo <> nil) and (Logo.Height > 0) Then Apercu.Header.Height[GmPixels] := max(min(Logo.Height, MaxHeightLogo) + 6, Apercu.Header.Height[GmPixels])
  else
{$EndIf}
{$IfDef CALADAIR_VMC}
  If (Logo <> nil) and (Logo.Height > 0) Then Apercu.Header.Height[GmPixels] := max(min(Logo.Height, MaxHeightLogo) + 10, Apercu.Header.Height[GmPixels])
  else
{$EndIf}
                                              Apercu.Header.Height[GmPixels] := Apercu.Header.Height[GmPixels];


End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_ApercuImpressions.AddFooter(_Str: String; _Visible: Boolean);
Begin
  Apercu.Footer.CaptionLeft.Font.Size := 6;
  Apercu.Footer.CaptionLeft.Caption    := '';
  Apercu.Canvas.Brush.Color := clWhite;
  Apercu.Canvas.Font.Color := clBlack;
  If Length(FooterList) <  Apercu.CurrentPageNum Then SetLength(FooterList, Length(FooterList) + 1);
  If _Visible Then FooterList[Apercu.CurrentPageNum - 1] :=  _Str;
  ApercuDrawFooter(Self, Apercu.FooterRect, Apercu.Canvas);
  Apercu.Footer.CaptionRight.Font.Size := 6;
  Apercu.Footer.CaptionCenter.Font.Size := 6;

{$IfDef ANJOS_OPTIMA3D}
  If _Visible Then Apercu.Footer.CaptionCenter.Caption := 'Page {PAGE}';// / { NUMPAGES }';
{$Else}
  If _Visible Then Apercu.Footer.CaptionRight.Caption := 'Page {PAGE}';// / { NUMPAGES }';
{$EndIf}
{$IfDef VIM_OPTAIR}
  If _Visible Then Apercu.Footer.CaptionLeft.Caption := ExtractFileName(Etude.Fichier) + ' - ' + FormatDateTime('dd mmmm yyyy', Date);
{$EndIf}

{$IfDef ANJOS_OPTIMA3D}
        {$IfNDef CALADAIR_VMC}
  If (Logo <> nil) and (Logo.Height > 0) Then Apercu.Footer.Height[GmPixels] := max(min(Logo.Height, MaxHeightLogo), Apercu.Footer.Height[GmPixels])
  else
        {$EndIf}
{$EndIf}
                                              Apercu.Footer.Height[GmPixels] := Apercu.Footer.Height[GmPixels];

End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_ApercuImpressions.AddTitle(_Str: String; _WithBorder: Boolean = True);
Var
  m_OldBrush    : TColor;
  m_OldFontSize : Integer;
Begin
  m_OldBrush := Apercu.Canvas.Brush.Color;
  m_OldFontSize := Apercu.Canvas.Font.Size;
  Apercu.Canvas.Font.Size := 10;
  Apercu.Canvas.Font.Color := ClBlack;
  Apercu.Canvas.Brush.Color := c_CouleurFond;
  If _WithBorder Then
    Apercu.Canvas.TextBoxExt(Apercu.Margins.Left.AsMillimeters, 5 + Apercu.Margins.Top.AsMillimeters + Apercu.Header.Height[GmMillimeters],
                             Apercu.PageWidth[gmMillimeters] - Apercu.Margins.Right.AsMillimeters, Apercu.Margins.Top.AsMillimeters +
                             Apercu.Header.Height[GmMillimeters] + 15, 0.25, _Str, taCenter, gmMiddle, GmMillimeters )
  Else Apercu.Canvas.TextOutCenter((Apercu.PageWidth[GmMillimeters]) / 2 ,
                                    5 + Apercu.Margins.Top.AsMillimeters + Apercu.Header.Height[GmMillimeters], _Str, gmMillimeters);
  CurrentYPosOnPage := 5 + Apercu.Margins.Top.AsMillimeters + Apercu.Header.Height[GmMillimeters] + 15;
  Apercu.Canvas.Brush.Color := m_OldBrush;
  Apercu.Canvas.Font.Size   := m_OldFontSize;

  {$IfDef PRINTRTF}
//  RTF_Titre(_Str);
  {$EndIf}
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_ApercuImpressions.AddGridTitle(_Str: String; _WithBorder: Boolean);
Var
  m_Font : TFont;
Begin
  Grid_Exchange.ColCount := 1;
  Grid_Exchange.RowCount := 1;
  If _WithBorder Then Grid_Print.GridOptions := Grid_Print.GridOptions + [gmGridBorder]
                 Else Grid_Print.GridOptions := Grid_Print.GridOptions - [gmGridBorder];
  m_Font := GetFontInfos;
  m_Font.Style := [fsBold];
  m_Font.Size := 6;
  Grid_Exchange.Objects[0, 0] := TCelluleImp.Create(_Str, m_Font);
  TCelluleImp(Grid_Exchange.Objects[0, 0]).IBrushColor := c_CouleurFond;
  If Not _WithBorder Then TCelluleImp(Grid_Exchange.Objects[0, 0]).IBrushColor := ClWhite;
  PrintGrid;

  {$IfDef PRINTRTF}
  RTF_Titre(_Str);
  {$EndIf}
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_ApercuImpressions.AutoNewPageImp(_Portrait: Boolean);
Begin
  Apercu.NewPage;
  SetOrientation(Apercu.CurrentPage, _Portrait);
  CurrentYPosOnPage := 10 + Apercu.Margins.Top.AsMillimeters + Apercu.Header.Height[GmMillimeters];

End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_ApercuImpressions.GetGrid2Print(_Grid: TStringGrid);
Var
  m_TmpGrille : TStringGrid;
  m_NoCol     : SmallInt;
  m_NoLigne   : SmallInt;
  m_CurLigne  : SmallInt;
Begin
  m_TmpGrille :=  _Grid;
  Grid_Exchange.ColCount := m_TmpGrille.ColCount;
  Grid_Exchange.RowCount := m_TmpGrille.RowCount - NbEmptyLines(m_TmpGrille);
  For m_NoCol := 0 To Grid_Exchange.ColCount - 1 Do
    If IsNotNil(m_TmpGrille.Objects[m_NoCol, 0]) And (TCelluleImp(m_TmpGrille.Objects[m_NoCol, 0]).ILargeur <> 0) Then
     Grid_Exchange.ColWidths[m_NoCol] := Round(Grid_Exchange.Width * TCelluleImp(m_TmpGrille.Objects[m_NoCol, 0]).ILargeur / 100)
    Else Grid_Exchange.ColWidths[m_NoCol] := Round(Grid_Exchange.Width / Grid_Exchange.ColCount);
//  For m_NoLigne := 0 To Grid_Exchange.RowCount - 1 Do Grid_Exchange.RowHeights[m_NoLigne] := 10;
  For m_NoLigne := 0 To Grid_Exchange.RowCount - 1 Do Grid_Exchange.RowHeights[m_NoLigne] := 10;

  m_CurLigne := 0;
  For m_NoLigne := 0 To m_TmpGrille.RowCount - 1 Do If Not IsEmptyLine(m_TmpGrille, m_NoLigne) Then Begin
    For m_NoCol := 0 To m_TmpGrille.ColCount - 1 Do Begin
      Grid_Exchange.Objects[m_NoCol, m_CurLigne] := m_TmpGrille.Objects[m_NoCol, m_NoLigne];
      If Grid_Exchange.Objects[m_NoCol, m_CurLigne] <> Nil Then Grid_Exchange.Cells[m_NoCol, m_CurLigne] := TCelluleImp(m_TmpGrille.Objects[m_NoCol, m_NoLigne]).ITexte
                                                           Else Grid_Exchange.Cells[m_NoCol, m_CurLigne] := '';
    End;
    Inc(m_CurLigne);
  End;
End;
{ ************************************************************************************************************************************************** }
Function TDlg_ApercuImpressions.IsEmptyLine(_Grid: TStringGrid; _Row: Integer): Boolean;
Var
  m_ColNo: Integer;
Begin
  Result := True;
  m_ColNo := 0;
  While m_ColNo <= _Grid.ColCount - 1 Do Begin
    If IsNotNil(_Grid.Objects[m_ColNo, _Row]) And (TCelluleImp(_Grid.Objects[m_ColNo, _Row]).ITexte <> ' ') Then Begin
      Result := False;
      Break;
    End;
    Inc(m_ColNo);
  End;
End;
{ ************************************************************************************************************************************************** }
Function TDlg_ApercuImpressions.NbEmptyLines(_Grid: TStringGrid): Integer;
Var
  m_Sum : SmallInt;
  m_Row : SmallInt;
Begin
  m_Sum := 0;
  For m_Row := 0 To _Grid.RowCount - 1 Do If IsEmptyLine(_Grid, m_Row) Then Inc(m_Sum);
  Result := m_Sum;
End;
{ ************************************************************************************************************************************************** }
Function TDlg_ApercuImpressions.GetTextHeight(_Texte: String; _ColWidth: Extended; _Font: TFont): Extended;
Var
  NbLignes: Integer;
  Text_Ht: Extended;
  Text_L : Extended;
  CalcTaille: TSize;
  LargTemp  : Extended;
  StrTemp   : String;
  NoChar    : Integer;
  BeginStr  : Integer;
  LastBack  : Integer;
  OldSize   : Integer;
Begin
  Grid_Print.Grid.Canvas.Font.Assign(_Font);
  OldSize   := Grid_Print.Grid.Canvas.Font.Size;

  { Si on est en gras le calcul de GetTextExtentPoint32 renvoie trop juste du coup je prends la taille au dessus en police !! }
  If fsBold In Grid_Print.Grid.Canvas.Font.Style Then Grid_Print.Grid.Canvas.Font.Size := Grid_Print.Grid.Canvas.Font.Size + 1;
  Windows.GetTextExtentPoint32(Grid_Print.Grid.Canvas.Handle, PChar(_Texte), Length(_Texte), CalcTaille);
  Text_L  := ConvertValue(CalcTaille.cx, gmPixels, gmInches);
  Text_Ht := ConvertValue(CalcTaille.cy, gmPixels, gmInches);
  If _ColWidth - Apercu.Canvas.TextBoxPadding.AsInches > Text_L Then NbLignes := 1
  Else Begin
    LastBack:= -1;
    StrTemp := '';
    NoChar  := 1;
    NbLignes := 1;
    While NoChar <= length(_Texte) Do Begin
      If LastBack >= NoChar Then Break;
      BeginStr := NoChar;
      LastBack := BeginStr;
      While (NoChar <= length(_Texte)) And (_Texte[NoChar] <> ' ') And (_Texte[NoChar] <> #13) Do Begin
        StrTemp := StrTemp + _Texte[NoChar];
        Inc(NoChar);
      End;
      If NoChar <= Length(_Texte) Then Begin
        If _Texte[NoChar] = #13 Then Begin
           StrTemp := '';
           Inc(NoChar);
           Inc(NbLignes);
        End Else If (_Texte[NoChar] = ' ') Or (_Texte[NoChar] = #0) Then Begin
          StrTemp := StrTemp + ' ';
          Inc(NoChar);
          Windows.GetTextExtentPoint32(Grid_Print.Grid.Canvas.Handle, PChar(StrTemp), Length(StrTemp), CalcTaille);
          LargTemp := ConvertValue(CalcTaille.cx, gmPixels, gmInches);
          If _ColWidth - Apercu.Canvas.TextBoxPadding.AsInches < LargTemp Then Begin
            Inc(NbLignes);
            StrTemp := Copy(StrTemp, BeginStr, Length(StrTemp));
          End;
        End;
      End;
    End;
  End;
  Grid_Print.Grid.Canvas.Font.Size := OldSize;
  Result := Text_Ht * NbLignes + Apercu.Canvas.TextBoxPadding.AsInches * NbLignes;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_ApercuImpressions.SetOrientation(_Page: TgmPage; _Portrait: Boolean);
Begin
  If _Portrait Then _Page.Orientation := gmPortrait
               Else _Page.Orientation := gmLandscape;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_ApercuImpressions.PrintGrid;
Begin

  Grid_Print.GridToPage(Apercu.Margins.Left.AsMillimeters, CurrentYPosOnPage,
                       Apercu.PageWidth[gmMillimeters] - Apercu.Margins.Right.AsMillimeters - Apercu.Margins.Left.AsMillimeters, GmMillimeters);
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_ApercuImpressions.PrintCartouche(_Center: Boolean);
Begin
  If _Center Then Grid_Print.GridToPage(Apercu.PageWidth[gmMillimeters] / 2 - ConvertValue(Grid_Exchange.ColWidths[0], GmPixels, GmMillimeters) / 2,
                                        CurrentYPosOnPage, ConvertValue(Grid_Exchange.ColWidths[0], GmPixels, GmMillimeters), GmMillimeters)
             Else Grid_Print.GridToPage(Apercu.Margins.Left.AsMillimeters, CurrentYPosOnPage,
                                        ConvertValue(Grid_Exchange.ColWidths[0], GmPixels, GmMillimeters), GmMillimeters)
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_ApercuImpressions.PrintLogo;
Var
  RectImp: TGmRect;
  Ratio : Extended;
Const
  Centrage = true;
  Encadre = false;
Begin
  if (Logo = nil) Or (Logo.Height = 0) Then exit;
  { Facteur de l'image }
  Ratio := 1;

  if MaxHeightLogo < Logo.Height then Ratio := MaxHeightLogo / Logo.Height;

  {$IfDef ANJOS_OPTIMA3D}
        {$IfDef CALADAIR_VMC}
        RectImp.Left   := Apercu.Margins.Left.AsMillimeters;
        RectImp.Top    := Apercu.Margins.Top.AsMillimeters;
        RectImp.Right  := ConvertValue(ratio * Logo.Width, GmPixels, GmMillimeters) + RectImp.Left;
        RectImp.Bottom := ConvertValue(ratio * Logo.Height, GmPixels, GmMillimeters) + RectImp.Top;
        {$Else}
        RectImp.Right  := Apercu.PageWidth[GmMillimeters] - Apercu.Margins.Right.AsMillimeters;
        RectImp.Bottom := Apercu.PageHeight[GmMillimeters] - Apercu.Margins.Bottom.AsMillimeters;
        RectImp.Left   := RectImp.Right - ConvertValue(ratio * Logo.Width, GmPixels, GmMillimeters);
        RectImp.Top    := RectImp.Bottom - ConvertValue(ratio * Logo.Height, GmPixels, GmMillimeters);
        {$EndIf}
  {$Else}
  RectImp.Left   := Apercu.Margins.Left.AsMillimeters;
  RectImp.Top    := Apercu.Margins.Top.AsMillimeters;
  RectImp.Right  := ConvertValue(ratio * Logo.Width, GmPixels, GmMillimeters) + RectImp.Left;
  RectImp.Bottom := ConvertValue(ratio * Logo.Height, GmPixels, GmMillimeters) + RectImp.Top;
  {$EndIf}
  Apercu.Canvas.Draw(RectImp.Left, RectImp.Top, Logo, ratio, gmMillimeters);
//  ApercuImp_AjouteBitmapRtfForCurrentPage(true);
End;
{ ******************************************************************* \TDlg_ApercuImpressions *************************************************************** }





{ ******************************************************************* TSommaireImp *************************************************************************** }
Function TSommaireImp.Add(Value: String): Integer;
Begin
  Result := Inherited Add(Value + '|' + IntToStr(ApercuImp_NombrePages) + '|');
End;
{ ******************************************************************* \ TSommaireImp ************************************************************************* }










End.
