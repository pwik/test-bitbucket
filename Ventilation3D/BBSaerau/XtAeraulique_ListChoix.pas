unit XtAeraulique_ListChoix;
interface

uses
  WinProcs, Classes, Forms, StdCtrls, Buttons, ExtCtrls, AdvGrid, Grids, BaseGrid, Controls,
  BbsGType,
  AdvObj;

type
  TChoix_ListeXtApp = class(TForm)
    BoutonOK: TBitBtn;
    BoutonAnnuler: TBitBtn;
    Grid_Choix: TAdvStringGrid;
    procedure ListBox1DblClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Grid_ChoixSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure Grid_ChoixKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Grid_ChoixDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure FormCreate(Sender: TObject);
  private
    { D�clarations private }
  public
    WithElements: Boolean;
    { D�clarations public }
    Procedure Empty;
    Function  AddElement(_Valeur: String; _ClearBefore: Boolean = False): Integer;
  end;

var Choix_ListeXtApp: TChoix_ListeXtApp;

   Function ChoixElementSurListe(_Titre: String; _PoliceFixe: Boolean = False; _ClearAfter: Boolean = True): Integer;

implementation
{$R *.DFM}
Uses
  SysUtils, Dialogs,
  bbsgtec,
  BBS_Message,
  BbsCom;

procedure TChoix_ListeXtApp.ListBox1DblClick(Sender: TObject);
begin
  Modalresult:=idOK;
end;

procedure TChoix_ListeXtApp.FormActivate(Sender: TObject);
begin
  Grid_Choix.SetFocus;
end;

{
 1- g�n�rateur r�f�rence                                     MIXTE+CHAUFFAGE+EAU CHAUDE
 1- chaud stand. atmos. sans veill.                          MIXTE+CHAUFFAGE+EAU CHAUDE
 1- chaud stand. atmos. avec veill.                          MIXTE+CHAUFFAGE+EAU CHAUDE
 1- chaud stand.  Air Puls� sans veill.                      MIXTE+CHAUFFAGE+EAU CHAUDE
 1- chaud stand.  Air Puls� avec veill.                      MIXTE+CHAUFFAGE+EAU CHAUDE
 1- chaud BT atmos. sans veill.                              MIXTE+CHAUFFAGE+EAU CHAUDE
 1- chaud BT atmos. avec veill.                              MIXTE+CHAUFFAGE+EAU CHAUDE
 1- chaud BT  Air Puls� sans veill.                          MIXTE+CHAUFFAGE+EAU CHAUDE
 1- chaud BT  Air Puls� avec veill.                          MIXTE+CHAUFFAGE+EAU CHAUDE
 1- chaud cond. flux forc� sans veill.                       MIXTE+CHAUFFAGE+EAU CHAUDE
 5- Chaudi�re bois classe 3                                  MIXTE+CHAUFFAGE+EAU CHAUDE
 5- Chaudi�re bois classe 2                                  MIXTE+CHAUFFAGE+EAU CHAUDE
 5- Chaudi�re bois classe 1                                  MIXTE+CHAUFFAGE+EAU CHAUDE
 5- Chaudi�re bois classe 1 Air puls� Alim man               MIXTE+CHAUFFAGE+EAU CHAUDE
 3- pac simple                                               CHAUFFAGE
10- Panneau Radiant                                          CHAUFFAGE
10- Tube radiant                                             CHAUFFAGE
11- Radiateur GAZ                                            CHAUFFAGE
12- G�n�rateur Air Chaud                                     CHAUFFAGE
 7- chauffe eau <10kW avec veilleuse                         EAU CHAUDE
 7- chauffe eau <10kW sans veilleuse                         EAU CHAUDE
 7- chauffe eau >10kW avec veilleuse                         EAU CHAUDE
 7- chauffe eau >10kW sans veilleuse                         EAU CHAUDE
 9- Accu gaz                                                 EAU CHAUDE
}

Function TChoix_ListeXtApp.AddElement(_Valeur: String; _ClearBefore: Boolean = False): Integer;
Var
  IdxLibre: Integer;
  Row : Integer;
begin
  If _ClearBefore Then Empty;
  IdxLibre := -1;
  For Row := 0 TO Grid_Choix.RowCount - 1 Do If Grid_Choix.Cells[1, Row] = '' Then Begin
    IdxLibre := Row;
    Break;
  End;
  If IdxLibre = -1 Then Begin
    Grid_Choix.RowCount := Grid_Choix.RowCount + 1;
    IdxLibre := Grid_Choix.RowCount - 1;
  End;
  Grid_Choix.Cells[1, IdxLibre] :=  _Valeur;
  WithElements := True;
  Result := IdxLibre;
end;

procedure TChoix_ListeXtApp.Empty;
Var
  Row: Integer;
begin
  WithElements := False;
  For Row := 0 To Grid_Choix.RowCount - 1 Do begin
    Grid_Choix.Cells[1, Row] := '';
    Grid_Choix.Objects[1, Row] := nil;
  end;
  Grid_Choix.RowCount := 12;
  Grid_Choix.Row := 0;
end;

procedure TChoix_ListeXtApp.Grid_ChoixSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  CanSelect := Grid_Choix.Cells[1, ARow] <> '';
end;

procedure TChoix_ListeXtApp.Grid_ChoixKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  If Key = vk_return Then ModalResult := Id_Ok;
end;

procedure TChoix_ListeXtApp.Grid_ChoixDblClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  If Grid_Choix.Cells[1, AROw] <> '' Then ModalResult := ID_OK;
end;

Function ChoixElementSurListe(_Titre: String; _PoliceFixe: Boolean = False; _ClearAfter: Boolean = True): Integer;
Begin
  Result := -1;
  If _PoliceFixe Then Choix_ListeXtApp.Grid_Choix.Font.Name := 'Lucida Console'
                 Else Choix_ListeXtApp.Grid_Choix.Font.Name := 'Segoe UI';
  Choix_ListeXtApp.Caption := _Titre;
  If Choix_ListeXtApp.WithElements Then Begin
    If Choix_ListeXtApp.ShowModal = Id_Ok Then Result := Choix_ListeXtApp.Grid_Choix.Row;
    If _ClearAfter Then Choix_ListeXtApp.Empty;
  End Else BBS_ShowMessage('Aucun �l�ment n''est disponible', MtError,[MbOk],0);
End;

procedure TChoix_ListeXtApp.FormCreate(Sender: TObject);
begin
  WithElements := False;
end;


end.

