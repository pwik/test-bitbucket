object XtAerauliqueMainForm: TXtAerauliqueMainForm
  Left = 411
  Top = 102
  ActiveControl = Grid_Batiment
  Caption = 'A'#233'raulique'
  ClientHeight = 597
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Segoe UI'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 17
  object Panel_Fond: TPanel
    Left = 0
    Top = 46
    Width = 1008
    Height = 551
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object StatusBar1: TStatusBar
      Left = 0
      Top = 532
      Width = 1008
      Height = 19
      Panels = <
        item
          Text = 'Version'
          Width = 100
        end>
    end
    object Panel_CAD: TPanel
      Left = 0
      Top = 0
      Width = 619
      Height = 532
      Align = alClient
      BevelOuter = bvNone
      Color = 15784647
      TabOrder = 1
      OnResize = Panel_CADResize
    end
    object Panel_VMC: TPanel
      Left = 619
      Top = 0
      Width = 364
      Height = 532
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 2
      object Panel_TitreRes: TPanel
        Left = 0
        Top = 297
        Width = 364
        Height = 23
        Align = alBottom
        BevelOuter = bvNone
        Caption = 'R'#233'sultats (B'#226'timent / Logements)'
        Color = 15784647
        TabOrder = 0
        object SpeedButton2: TSpeedButton
          Left = 1
          Top = 1
          Width = 23
          Height = 22
          Caption = '^'
          Flat = True
          OnClick = Panel_TitreResClick
        end
      end
      object Panel_Resultats: TPanel
        Left = 0
        Top = 320
        Width = 364
        Height = 212
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object Grid_ResultatsReseau: TAdvStringGrid
          Left = 0
          Top = 0
          Width = 364
          Height = 212
          Cursor = crDefault
          Align = alClient
          Color = clInfoBk
          ColCount = 4
          Ctl3D = False
          DefaultColWidth = 90
          DefaultRowHeight = 21
          DrawingStyle = gdsClassic
          FixedColor = clSilver
          FixedCols = 0
          FixedRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected]
          ParentCtl3D = False
          ParentFont = False
          ScrollBars = ssBoth
          TabOrder = 0
          OnSelectCell = Grid_ResultatsTronconSelectCell
          HoverRowCells = [hcNormal, hcSelected]
          OnCanClickCell = Grid_ResultatsReseauCanClickCell
          ActiveCellFont.Charset = DEFAULT_CHARSET
          ActiveCellFont.Color = clWindowText
          ActiveCellFont.Height = -11
          ActiveCellFont.Name = 'Segoe UI'
          ActiveCellFont.Style = [fsBold]
          ControlLook.FixedGradientHoverFrom = clGray
          ControlLook.FixedGradientHoverTo = clWhite
          ControlLook.FixedGradientDownFrom = clGray
          ControlLook.FixedGradientDownTo = clSilver
          ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
          ControlLook.DropDownHeader.Font.Color = clWindowText
          ControlLook.DropDownHeader.Font.Height = -11
          ControlLook.DropDownHeader.Font.Name = 'Tahoma'
          ControlLook.DropDownHeader.Font.Style = []
          ControlLook.DropDownHeader.Visible = True
          ControlLook.DropDownHeader.Buttons = <>
          ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
          ControlLook.DropDownFooter.Font.Color = clWindowText
          ControlLook.DropDownFooter.Font.Height = -11
          ControlLook.DropDownFooter.Font.Name = 'MS Sans Serif'
          ControlLook.DropDownFooter.Font.Style = []
          ControlLook.DropDownFooter.Visible = True
          ControlLook.DropDownFooter.Buttons = <>
          ControlLook.ProgressBorderColor = clNone
          ControlLook.ProgressXP = True
          Filter = <>
          FilterDropDown.Font.Charset = DEFAULT_CHARSET
          FilterDropDown.Font.Color = clWindowText
          FilterDropDown.Font.Height = -11
          FilterDropDown.Font.Name = 'MS Sans Serif'
          FilterDropDown.Font.Style = []
          FilterDropDownClear = '(All)'
          FilterEdit.TypeNames.Strings = (
            'Starts with'
            'Ends with'
            'Contains'
            'Not contains'
            'Equal'
            'Not equal'
            'Clear')
          FixedColWidth = 90
          FixedFont.Charset = DEFAULT_CHARSET
          FixedFont.Color = clWhite
          FixedFont.Height = -11
          FixedFont.Name = 'Segoe UI'
          FixedFont.Style = [fsBold]
          Flat = True
          FloatFormat = '%.2f'
          GridImages = ImageList3
          PrintSettings.DateFormat = 'dd/mm/yyyy'
          PrintSettings.Font.Charset = DEFAULT_CHARSET
          PrintSettings.Font.Color = clWindowText
          PrintSettings.Font.Height = -11
          PrintSettings.Font.Name = 'Segoe UI'
          PrintSettings.Font.Style = []
          PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
          PrintSettings.FixedFont.Color = clWindowText
          PrintSettings.FixedFont.Height = -11
          PrintSettings.FixedFont.Name = 'Segoe UI'
          PrintSettings.FixedFont.Style = []
          PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
          PrintSettings.HeaderFont.Color = clWindowText
          PrintSettings.HeaderFont.Height = -11
          PrintSettings.HeaderFont.Name = 'Segoe UI'
          PrintSettings.HeaderFont.Style = []
          PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
          PrintSettings.FooterFont.Color = clWindowText
          PrintSettings.FooterFont.Height = -11
          PrintSettings.FooterFont.Name = 'Segoe UI'
          PrintSettings.FooterFont.Style = []
          PrintSettings.PageNumSep = '/'
          RowHeaders.Strings = (
            ''
            ''
            ''
            ''
            ''
            ''
            'Colonne de la bouche'
            'Collecteur de la bouche'
            'Bouche'
            'S'#233'lectionner')
          SearchFooter.FindNextCaption = 'Find next'
          SearchFooter.FindPrevCaption = 'Find previous'
          SearchFooter.Font.Charset = DEFAULT_CHARSET
          SearchFooter.Font.Color = clWindowText
          SearchFooter.Font.Height = -11
          SearchFooter.Font.Name = 'Segoe UI'
          SearchFooter.Font.Style = []
          SearchFooter.HighLightCaption = 'Highlight'
          SearchFooter.HintClose = 'Close'
          SearchFooter.HintFindNext = 'Find next occurence'
          SearchFooter.HintFindPrev = 'Find previous occurence'
          SearchFooter.HintHighlight = 'Highlight occurences'
          SearchFooter.MatchCaseCaption = 'Match case'
          SelectionColor = clNone
          ShowSelection = False
          SortSettings.DefaultFormat = ssAutomatic
          Version = '7.2.8.0'
          RowHeights = (
            21
            21
            21
            21
            21
            21
            21
            21
            21
            21)
          object Grid_ResBat: TAdvStringGrid
            Left = 0
            Top = 0
            Width = 362
            Height = 210
            Cursor = crDefault
            Align = alClient
            Color = clSilver
            ColCount = 4
            Ctl3D = False
            DefaultColWidth = 90
            DefaultRowHeight = 21
            DrawingStyle = gdsClassic
            Enabled = False
            FixedCols = 0
            FixedRows = 0
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Segoe UI'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected]
            ParentCtl3D = False
            ParentFont = False
            ScrollBars = ssBoth
            TabOrder = 0
            GridLineColor = clBlack
            HoverRowCells = [hcNormal, hcSelected]
            ActiveCellFont.Charset = DEFAULT_CHARSET
            ActiveCellFont.Color = clWindowText
            ActiveCellFont.Height = -11
            ActiveCellFont.Name = 'Segoe UI'
            ActiveCellFont.Style = [fsBold]
            Bands.Active = True
            ControlLook.FixedGradientHoverFrom = clGray
            ControlLook.FixedGradientHoverTo = clWhite
            ControlLook.FixedGradientDownFrom = clGray
            ControlLook.FixedGradientDownTo = clSilver
            ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
            ControlLook.DropDownHeader.Font.Color = clWindowText
            ControlLook.DropDownHeader.Font.Height = -11
            ControlLook.DropDownHeader.Font.Name = 'Tahoma'
            ControlLook.DropDownHeader.Font.Style = []
            ControlLook.DropDownHeader.Visible = True
            ControlLook.DropDownHeader.Buttons = <>
            ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
            ControlLook.DropDownFooter.Font.Color = clWindowText
            ControlLook.DropDownFooter.Font.Height = -11
            ControlLook.DropDownFooter.Font.Name = 'MS Sans Serif'
            ControlLook.DropDownFooter.Font.Style = []
            ControlLook.DropDownFooter.Visible = True
            ControlLook.DropDownFooter.Buttons = <>
            Filter = <>
            FilterDropDown.Font.Charset = DEFAULT_CHARSET
            FilterDropDown.Font.Color = clWindowText
            FilterDropDown.Font.Height = -11
            FilterDropDown.Font.Name = 'MS Sans Serif'
            FilterDropDown.Font.Style = []
            FilterDropDownClear = '(All)'
            FilterEdit.TypeNames.Strings = (
              'Starts with'
              'Ends with'
              'Contains'
              'Not contains'
              'Equal'
              'Not equal'
              'Clear')
            FixedColWidth = 90
            FixedFont.Charset = DEFAULT_CHARSET
            FixedFont.Color = clWindowText
            FixedFont.Height = -11
            FixedFont.Name = 'Segoe UI'
            FixedFont.Style = [fsBold]
            Flat = True
            FloatFormat = '%.2f'
            HideFocusRect = True
            PrintSettings.DateFormat = 'dd/mm/yyyy'
            PrintSettings.Font.Charset = DEFAULT_CHARSET
            PrintSettings.Font.Color = clWindowText
            PrintSettings.Font.Height = -11
            PrintSettings.Font.Name = 'Segoe UI'
            PrintSettings.Font.Style = []
            PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
            PrintSettings.FixedFont.Color = clWindowText
            PrintSettings.FixedFont.Height = -11
            PrintSettings.FixedFont.Name = 'Segoe UI'
            PrintSettings.FixedFont.Style = []
            PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
            PrintSettings.HeaderFont.Color = clWindowText
            PrintSettings.HeaderFont.Height = -11
            PrintSettings.HeaderFont.Name = 'Segoe UI'
            PrintSettings.HeaderFont.Style = []
            PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
            PrintSettings.FooterFont.Color = clWindowText
            PrintSettings.FooterFont.Height = -11
            PrintSettings.FooterFont.Name = 'Segoe UI'
            PrintSettings.FooterFont.Style = []
            PrintSettings.PageNumSep = '/'
            SearchFooter.FindNextCaption = 'Find next'
            SearchFooter.FindPrevCaption = 'Find previous'
            SearchFooter.Font.Charset = DEFAULT_CHARSET
            SearchFooter.Font.Color = clWindowText
            SearchFooter.Font.Height = -11
            SearchFooter.Font.Name = 'Segoe UI'
            SearchFooter.Font.Style = []
            SearchFooter.HighLightCaption = 'Highlight'
            SearchFooter.HintClose = 'Close'
            SearchFooter.HintFindNext = 'Find next occurence'
            SearchFooter.HintFindPrev = 'Find previous occurence'
            SearchFooter.HintHighlight = 'Highlight occurences'
            SearchFooter.MatchCaseCaption = 'Match case'
            ShowSelection = False
            SortSettings.DefaultFormat = ssAutomatic
            Version = '7.2.8.0'
          end
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 152
        Width = 364
        Height = 23
        Align = alTop
        BevelOuter = bvNone
        Caption = 'Composant du r'#233'seau'
        Color = 15784647
        TabOrder = 2
        object SpeedButton1: TSpeedButton
          Left = 1
          Top = 1
          Width = 23
          Height = 22
          Caption = '^'
          Flat = True
          OnClick = SpeedButton4Click
        end
      end
      object Panel_TitreBatiment: TPanel
        Left = 0
        Top = 0
        Width = 364
        Height = 23
        Align = alTop
        BevelOuter = bvNone
        Caption = 'B'#226'timent'
        Color = 15784647
        TabOrder = 3
        object SpeedButton4: TSpeedButton
          Left = 1
          Top = 1
          Width = 23
          Height = 22
          Caption = '^'
          Flat = True
          OnClick = SpeedButton4Click
        end
      end
      object Panel_Batiment: TPanel
        Left = 0
        Top = 23
        Width = 364
        Height = 129
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 4
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 364
          Height = 129
          Align = alClient
          BevelOuter = bvNone
          Caption = 'Panel3'
          TabOrder = 0
          object ArbreBatiment: TVirtualStringTree
            Left = 25
            Top = 0
            Width = 339
            Height = 129
            Align = alClient
            DefaultNodeHeight = 25
            Header.AutoSizeIndex = 0
            Header.DefaultHeight = 17
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Segoe UI'
            Header.Font.Style = []
            Header.Height = 17
            Header.MainColumn = -1
            Header.Options = [hoColumnResize, hoDrag]
            Images = ImageList2
            TabOrder = 0
            TreeOptions.PaintOptions = [toHideFocusRect, toShowButtons, toShowDropmark, toShowRoot, toShowTreeLines, toThemeAware, toUseBlendedImages]
            TreeOptions.SelectionOptions = [toExtendedFocus, toFullRowSelect, toCenterScrollIntoView]
            OnChange = ArbreBatimentChange
            OnGetText = ArbreBatimentGetText
            OnGetImageIndex = ArbreBatimentGetImageIndex
            Columns = <>
          end
          object ToolBar2: TToolBar
            Left = 0
            Top = 0
            Width = 25
            Height = 129
            Align = alLeft
            Caption = 'ToolBar2'
            Images = ImageList3
            TabOrder = 1
            object ToolBtn_AddLgt: TToolButton
              Tag = 6
              Left = 0
              Top = 0
              Hint = 'Ajout d'#39'un logement'
              Caption = '+'
              ImageIndex = 3
              ParentShowHint = False
              Wrap = True
              ShowHint = True
              OnClick = ToolBarButtonClick
            end
            object ToolBtn_SuppLgt: TToolButton
              Tag = 9
              Left = 0
              Top = 22
              Hint = 'Supprimer le logement courant'
              Caption = '-'
              ImageIndex = 2
              ParentShowHint = False
              Wrap = True
              ShowHint = True
              OnClick = ToolBarButtonClick
            end
            object ToolBtn_DuplicateLgt: TToolButton
              Tag = 27
              Left = 0
              Top = 44
              Hint = 'Dupliquer le logement courant'
              Caption = 'ToolBtn_DuplicateLgt'
              ImageIndex = 4
              ParentShowHint = False
              ShowHint = True
              OnClick = ToolBarButtonClick
            end
          end
          object Grid_Batiment: TAdvStringGrid
            Tag = 1
            Left = 0
            Top = -62
            Width = 364
            Height = 191
            Cursor = crDefault
            Align = alBottom
            ColCount = 2
            Ctl3D = False
            DefaultColWidth = 172
            DefaultRowHeight = 21
            DrawingStyle = gdsClassic
            RowCount = 11
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Segoe UI'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goEditing]
            ParentCtl3D = False
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 2
            HoverRowCells = [hcNormal, hcSelected]
            OnCellValidate = Grid_BatimentCellValidate
            OnHasComboBox = Grid_BatimentHasComboBox
            OnGetEditorType = Grid_BatimentGetEditorType
            OnGetEditorProp = Grid_BatimentGetEditorProp
            OnEllipsClick = Grid_BatimentEllipsClick
            OnComboCloseUp = Grid_BatimentComboCloseUp
            ActiveCellFont.Charset = DEFAULT_CHARSET
            ActiveCellFont.Color = clWindowText
            ActiveCellFont.Height = -11
            ActiveCellFont.Name = 'Segoe UI'
            ActiveCellFont.Style = [fsBold]
            ColumnHeaders.Strings = (
              'Propri'#233't'#233
              'Valeur')
            ControlLook.FixedGradientHoverFrom = clGray
            ControlLook.FixedGradientHoverTo = clWhite
            ControlLook.FixedGradientDownFrom = clGray
            ControlLook.FixedGradientDownTo = clSilver
            ControlLook.DropDownAlwaysVisible = True
            ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
            ControlLook.DropDownHeader.Font.Color = clWindowText
            ControlLook.DropDownHeader.Font.Height = -11
            ControlLook.DropDownHeader.Font.Name = 'Tahoma'
            ControlLook.DropDownHeader.Font.Style = []
            ControlLook.DropDownHeader.Visible = True
            ControlLook.DropDownHeader.Buttons = <>
            ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
            ControlLook.DropDownFooter.Font.Color = clWindowText
            ControlLook.DropDownFooter.Font.Height = -11
            ControlLook.DropDownFooter.Font.Name = 'MS Sans Serif'
            ControlLook.DropDownFooter.Font.Style = []
            ControlLook.DropDownFooter.Visible = True
            ControlLook.DropDownFooter.Buttons = <>
            Filter = <>
            FilterDropDown.Font.Charset = DEFAULT_CHARSET
            FilterDropDown.Font.Color = clWindowText
            FilterDropDown.Font.Height = -11
            FilterDropDown.Font.Name = 'MS Sans Serif'
            FilterDropDown.Font.Style = []
            FilterDropDownClear = '(All)'
            FilterEdit.TypeNames.Strings = (
              'Starts with'
              'Ends with'
              'Contains'
              'Not contains'
              'Equal'
              'Not equal'
              'Clear')
            FixedColWidth = 172
            FixedFont.Charset = DEFAULT_CHARSET
            FixedFont.Color = clWindowText
            FixedFont.Height = -11
            FixedFont.Name = 'Segoe UI'
            FixedFont.Style = [fsBold]
            Flat = True
            FloatFormat = '%.2f'
            MouseActions.DirectComboDrop = True
            MouseActions.DirectEdit = True
            PrintSettings.DateFormat = 'dd/mm/yyyy'
            PrintSettings.Font.Charset = DEFAULT_CHARSET
            PrintSettings.Font.Color = clWindowText
            PrintSettings.Font.Height = -11
            PrintSettings.Font.Name = 'Segoe UI'
            PrintSettings.Font.Style = []
            PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
            PrintSettings.FixedFont.Color = clWindowText
            PrintSettings.FixedFont.Height = -11
            PrintSettings.FixedFont.Name = 'Segoe UI'
            PrintSettings.FixedFont.Style = []
            PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
            PrintSettings.HeaderFont.Color = clWindowText
            PrintSettings.HeaderFont.Height = -11
            PrintSettings.HeaderFont.Name = 'Segoe UI'
            PrintSettings.HeaderFont.Style = []
            PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
            PrintSettings.FooterFont.Color = clWindowText
            PrintSettings.FooterFont.Height = -11
            PrintSettings.FooterFont.Name = 'Segoe UI'
            PrintSettings.FooterFont.Style = []
            PrintSettings.PageNumSep = '/'
            ScrollBarAlways = saVert
            SearchFooter.FindNextCaption = 'Find next'
            SearchFooter.FindPrevCaption = 'Find previous'
            SearchFooter.Font.Charset = DEFAULT_CHARSET
            SearchFooter.Font.Color = clWindowText
            SearchFooter.Font.Height = -11
            SearchFooter.Font.Name = 'Segoe UI'
            SearchFooter.Font.Style = []
            SearchFooter.HighLightCaption = 'Highlight'
            SearchFooter.HintClose = 'Close'
            SearchFooter.HintFindNext = 'Find next occurence'
            SearchFooter.HintFindPrev = 'Find previous occurence'
            SearchFooter.HintHighlight = 'Highlight occurences'
            SearchFooter.MatchCaseCaption = 'Match case'
            ShowSelection = False
            SortSettings.DefaultFormat = ssAutomatic
            Version = '7.2.8.0'
            RowHeights = (
              21
              21
              21
              21
              22
              21
              20
              21
              21
              21
              21)
          end
        end
      end
      object PanelReseau: TPanel
        Left = 0
        Top = 175
        Width = 364
        Height = 122
        Align = alClient
        BevelOuter = bvNone
        Caption = 'PanelReseau'
        TabOrder = 5
        object PageControl1: TPageControl
          Left = 0
          Top = -118
          Width = 364
          Height = 240
          ActivePage = TabSheet1
          Align = alBottom
          TabOrder = 1
          object TabSheet1: TTabSheet
            Caption = 'Fonctionnement'
            object Grid_ResultatsTroncon: TAdvStringGrid
              Left = 0
              Top = 0
              Width = 356
              Height = 208
              Cursor = crDefault
              Align = alClient
              Color = clInfoBk
              ColCount = 3
              Ctl3D = False
              DefaultColWidth = 130
              DefaultRowHeight = 21
              DrawingStyle = gdsClassic
              FixedColor = clSilver
              FixedRows = 0
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'Segoe UI'
              Font.Style = []
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected]
              ParentCtl3D = False
              ParentFont = False
              ScrollBars = ssBoth
              TabOrder = 0
              OnSelectCell = Grid_ResultatsTronconSelectCell
              HoverRowCells = [hcNormal, hcSelected]
              OnClickCell = Grid_ResultatsTronconClickCell
              ActiveCellFont.Charset = DEFAULT_CHARSET
              ActiveCellFont.Color = clWindowText
              ActiveCellFont.Height = -11
              ActiveCellFont.Name = 'Segoe UI'
              ActiveCellFont.Style = [fsBold]
              ControlLook.FixedGradientHoverFrom = clGray
              ControlLook.FixedGradientHoverTo = clWhite
              ControlLook.FixedGradientDownFrom = clGray
              ControlLook.FixedGradientDownTo = clSilver
              ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
              ControlLook.DropDownHeader.Font.Color = clWindowText
              ControlLook.DropDownHeader.Font.Height = -11
              ControlLook.DropDownHeader.Font.Name = 'Tahoma'
              ControlLook.DropDownHeader.Font.Style = []
              ControlLook.DropDownHeader.Visible = True
              ControlLook.DropDownHeader.Buttons = <>
              ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
              ControlLook.DropDownFooter.Font.Color = clWindowText
              ControlLook.DropDownFooter.Font.Height = -11
              ControlLook.DropDownFooter.Font.Name = 'MS Sans Serif'
              ControlLook.DropDownFooter.Font.Style = []
              ControlLook.DropDownFooter.Visible = True
              ControlLook.DropDownFooter.Buttons = <>
              ControlLook.ProgressBorderColor = clNone
              Filter = <>
              FilterDropDown.Font.Charset = DEFAULT_CHARSET
              FilterDropDown.Font.Color = clWindowText
              FilterDropDown.Font.Height = -11
              FilterDropDown.Font.Name = 'MS Sans Serif'
              FilterDropDown.Font.Style = []
              FilterDropDownClear = '(All)'
              FilterEdit.TypeNames.Strings = (
                'Starts with'
                'Ends with'
                'Contains'
                'Not contains'
                'Equal'
                'Not equal'
                'Clear')
              FixedColWidth = 94
              FixedFont.Charset = DEFAULT_CHARSET
              FixedFont.Color = clWhite
              FixedFont.Height = -11
              FixedFont.Name = 'Segoe UI'
              FixedFont.Style = [fsBold]
              Flat = True
              FloatFormat = '%.2f'
              GridImages = ImageList3
              HideFocusRect = True
              PrintSettings.DateFormat = 'dd/mm/yyyy'
              PrintSettings.Font.Charset = DEFAULT_CHARSET
              PrintSettings.Font.Color = clWindowText
              PrintSettings.Font.Height = -11
              PrintSettings.Font.Name = 'Segoe UI'
              PrintSettings.Font.Style = []
              PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
              PrintSettings.FixedFont.Color = clWindowText
              PrintSettings.FixedFont.Height = -11
              PrintSettings.FixedFont.Name = 'Segoe UI'
              PrintSettings.FixedFont.Style = []
              PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
              PrintSettings.HeaderFont.Color = clWindowText
              PrintSettings.HeaderFont.Height = -11
              PrintSettings.HeaderFont.Name = 'Segoe UI'
              PrintSettings.HeaderFont.Style = []
              PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
              PrintSettings.FooterFont.Color = clWindowText
              PrintSettings.FooterFont.Height = -11
              PrintSettings.FooterFont.Name = 'Segoe UI'
              PrintSettings.FooterFont.Style = []
              PrintSettings.PageNumSep = '/'
              RowHeaders.Strings = (
                'Log. / Type'
                'Bouche'
                'D'#233'bit'
                'Diam. / Long.'
                'Vitesse'
                'Pdc'
                'R'#233'guli'#232'res'
                'Singuli'#232'res'
                'Totales')
              SearchFooter.FindNextCaption = 'Find next'
              SearchFooter.FindPrevCaption = 'Find previous'
              SearchFooter.Font.Charset = DEFAULT_CHARSET
              SearchFooter.Font.Color = clWindowText
              SearchFooter.Font.Height = -11
              SearchFooter.Font.Name = 'Segoe UI'
              SearchFooter.Font.Style = []
              SearchFooter.HighLightCaption = 'Highlight'
              SearchFooter.HintClose = 'Close'
              SearchFooter.HintFindNext = 'Find next occurence'
              SearchFooter.HintFindPrev = 'Find previous occurence'
              SearchFooter.HintHighlight = 'Highlight occurences'
              SearchFooter.MatchCaseCaption = 'Match case'
              SelectionColor = clNone
              ShowSelection = False
              SortSettings.DefaultFormat = ssAutomatic
              Version = '7.2.8.0'
              ColWidths = (
                94
                130
                130)
              RowHeights = (
                21
                21
                21
                21
                21
                21
                21
                21
                21
                21)
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Acoustique'
            ImageIndex = 1
            TabVisible = False
            object Grid_Acoust: TAdvStringGrid
              Left = 57
              Top = 21
              Width = 241
              Height = 170
              Cursor = crDefault
              Color = clInfoBk
              ColCount = 2
              Ctl3D = False
              DefaultColWidth = 140
              DefaultRowHeight = 21
              DrawingStyle = gdsClassic
              FixedColor = clSilver
              RowCount = 8
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'Segoe UI'
              Font.Style = []
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected]
              ParentCtl3D = False
              ParentFont = False
              ScrollBars = ssBoth
              TabOrder = 0
              HoverRowCells = [hcNormal, hcSelected]
              ActiveCellFont.Charset = DEFAULT_CHARSET
              ActiveCellFont.Color = clWindowText
              ActiveCellFont.Height = -11
              ActiveCellFont.Name = 'Segoe UI'
              ActiveCellFont.Style = [fsBold]
              Bands.Active = True
              ColumnHeaders.Strings = (
                ''
                'Lw (dB)')
              ControlLook.FixedGradientHoverFrom = clGray
              ControlLook.FixedGradientHoverTo = clWhite
              ControlLook.FixedGradientDownFrom = clGray
              ControlLook.FixedGradientDownTo = clSilver
              ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
              ControlLook.DropDownHeader.Font.Color = clWindowText
              ControlLook.DropDownHeader.Font.Height = -11
              ControlLook.DropDownHeader.Font.Name = 'Tahoma'
              ControlLook.DropDownHeader.Font.Style = []
              ControlLook.DropDownHeader.Visible = True
              ControlLook.DropDownHeader.Buttons = <>
              ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
              ControlLook.DropDownFooter.Font.Color = clWindowText
              ControlLook.DropDownFooter.Font.Height = -11
              ControlLook.DropDownFooter.Font.Name = 'MS Sans Serif'
              ControlLook.DropDownFooter.Font.Style = []
              ControlLook.DropDownFooter.Visible = True
              ControlLook.DropDownFooter.Buttons = <>
              ControlLook.ProgressBorderColor = clNone
              Filter = <>
              FilterDropDown.Font.Charset = DEFAULT_CHARSET
              FilterDropDown.Font.Color = clWindowText
              FilterDropDown.Font.Height = -11
              FilterDropDown.Font.Name = 'MS Sans Serif'
              FilterDropDown.Font.Style = []
              FilterDropDownClear = '(All)'
              FilterEdit.TypeNames.Strings = (
                'Starts with'
                'Ends with'
                'Contains'
                'Not contains'
                'Equal'
                'Not equal'
                'Clear')
              FixedColWidth = 100
              FixedFont.Charset = DEFAULT_CHARSET
              FixedFont.Color = clWhite
              FixedFont.Height = -11
              FixedFont.Name = 'Segoe UI'
              FixedFont.Style = [fsBold]
              Flat = True
              FloatFormat = '%.2f'
              HideFocusRect = True
              PrintSettings.DateFormat = 'dd/mm/yyyy'
              PrintSettings.Font.Charset = DEFAULT_CHARSET
              PrintSettings.Font.Color = clWindowText
              PrintSettings.Font.Height = -11
              PrintSettings.Font.Name = 'Segoe UI'
              PrintSettings.Font.Style = []
              PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
              PrintSettings.FixedFont.Color = clWindowText
              PrintSettings.FixedFont.Height = -11
              PrintSettings.FixedFont.Name = 'Segoe UI'
              PrintSettings.FixedFont.Style = []
              PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
              PrintSettings.HeaderFont.Color = clWindowText
              PrintSettings.HeaderFont.Height = -11
              PrintSettings.HeaderFont.Name = 'Segoe UI'
              PrintSettings.HeaderFont.Style = []
              PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
              PrintSettings.FooterFont.Color = clWindowText
              PrintSettings.FooterFont.Height = -11
              PrintSettings.FooterFont.Name = 'Segoe UI'
              PrintSettings.FooterFont.Style = []
              PrintSettings.PageNumSep = '/'
              RowHeaders.Strings = (
                ''
                '63  Hz'
                '125 Hz'
                '250 Hz'
                '500 Hz'
                '1000 Hz'
                '2000 Hz'
                '4000 Hz'
                '')
              SearchFooter.FindNextCaption = 'Find next'
              SearchFooter.FindPrevCaption = 'Find previous'
              SearchFooter.Font.Charset = DEFAULT_CHARSET
              SearchFooter.Font.Color = clWindowText
              SearchFooter.Font.Height = -11
              SearchFooter.Font.Name = 'Segoe UI'
              SearchFooter.Font.Style = []
              SearchFooter.HighLightCaption = 'Highlight'
              SearchFooter.HintClose = 'Close'
              SearchFooter.HintFindNext = 'Find next occurence'
              SearchFooter.HintFindPrev = 'Find previous occurence'
              SearchFooter.HintHighlight = 'Highlight occurences'
              SearchFooter.MatchCaseCaption = 'Match case'
              SelectionColor = clNone
              ShowSelection = False
              SortSettings.DefaultFormat = ssAutomatic
              Version = '7.2.8.0'
              ColWidths = (
                100
                140)
              RowHeights = (
                21
                21
                21
                21
                21
                21
                21
                21)
            end
          end
          object TabSheet3: TTabSheet
            Caption = 'Accidents'
            ImageIndex = 2
          end
        end
        object Grid_Properties: TAdvStringGrid
          Tag = 2
          Left = 0
          Top = 0
          Width = 364
          Height = 323
          Cursor = crDefault
          Align = alClient
          ColCount = 2
          Ctl3D = False
          DefaultColWidth = 172
          DefaultRowHeight = 21
          DrawingStyle = gdsClassic
          RowCount = 13
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goEditing]
          ParentCtl3D = False
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 0
          HoverRowCells = [hcNormal, hcSelected]
          OnCellValidate = Grid_BatimentCellValidate
          OnHasComboBox = Grid_BatimentHasComboBox
          OnGetEditorType = Grid_BatimentGetEditorType
          OnGetEditorProp = Grid_BatimentGetEditorProp
          OnEllipsClick = Grid_PropertiesEllipsClick
          OnComboCloseUp = Grid_BatimentComboCloseUp
          ActiveCellFont.Charset = DEFAULT_CHARSET
          ActiveCellFont.Color = clWindowText
          ActiveCellFont.Height = -11
          ActiveCellFont.Name = 'Segoe UI'
          ActiveCellFont.Style = [fsBold]
          ColumnHeaders.Strings = (
            'Propri'#233't'#233
            'Valeur')
          ControlLook.FixedGradientHoverFrom = clGray
          ControlLook.FixedGradientHoverTo = clWhite
          ControlLook.FixedGradientDownFrom = clGray
          ControlLook.FixedGradientDownTo = clSilver
          ControlLook.DropDownAlwaysVisible = True
          ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
          ControlLook.DropDownHeader.Font.Color = clWindowText
          ControlLook.DropDownHeader.Font.Height = -11
          ControlLook.DropDownHeader.Font.Name = 'Tahoma'
          ControlLook.DropDownHeader.Font.Style = []
          ControlLook.DropDownHeader.Visible = True
          ControlLook.DropDownHeader.Buttons = <>
          ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
          ControlLook.DropDownFooter.Font.Color = clWindowText
          ControlLook.DropDownFooter.Font.Height = -11
          ControlLook.DropDownFooter.Font.Name = 'MS Sans Serif'
          ControlLook.DropDownFooter.Font.Style = []
          ControlLook.DropDownFooter.Visible = True
          ControlLook.DropDownFooter.Buttons = <>
          Filter = <>
          FilterDropDown.Font.Charset = DEFAULT_CHARSET
          FilterDropDown.Font.Color = clWindowText
          FilterDropDown.Font.Height = -11
          FilterDropDown.Font.Name = 'MS Sans Serif'
          FilterDropDown.Font.Style = []
          FilterDropDownClear = '(All)'
          FilterEdit.TypeNames.Strings = (
            'Starts with'
            'Ends with'
            'Contains'
            'Not contains'
            'Equal'
            'Not equal'
            'Clear')
          FixedColWidth = 172
          FixedFont.Charset = DEFAULT_CHARSET
          FixedFont.Color = clWindowText
          FixedFont.Height = -11
          FixedFont.Name = 'Segoe UI'
          FixedFont.Style = [fsBold]
          Flat = True
          FloatFormat = '%.2f'
          HideFocusRect = True
          MouseActions.DirectComboDrop = True
          MouseActions.DirectEdit = True
          PrintSettings.DateFormat = 'dd/mm/yyyy'
          PrintSettings.Font.Charset = DEFAULT_CHARSET
          PrintSettings.Font.Color = clWindowText
          PrintSettings.Font.Height = -11
          PrintSettings.Font.Name = 'Segoe UI'
          PrintSettings.Font.Style = []
          PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
          PrintSettings.FixedFont.Color = clWindowText
          PrintSettings.FixedFont.Height = -11
          PrintSettings.FixedFont.Name = 'Segoe UI'
          PrintSettings.FixedFont.Style = []
          PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
          PrintSettings.HeaderFont.Color = clWindowText
          PrintSettings.HeaderFont.Height = -11
          PrintSettings.HeaderFont.Name = 'Segoe UI'
          PrintSettings.HeaderFont.Style = []
          PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
          PrintSettings.FooterFont.Color = clWindowText
          PrintSettings.FooterFont.Height = -11
          PrintSettings.FooterFont.Name = 'Segoe UI'
          PrintSettings.FooterFont.Style = []
          PrintSettings.PageNumSep = '/'
          ScrollBarAlways = saVert
          SearchFooter.FindNextCaption = 'Find next'
          SearchFooter.FindPrevCaption = 'Find previous'
          SearchFooter.Font.Charset = DEFAULT_CHARSET
          SearchFooter.Font.Color = clWindowText
          SearchFooter.Font.Height = -11
          SearchFooter.Font.Name = 'Segoe UI'
          SearchFooter.Font.Style = []
          SearchFooter.HighLightCaption = 'Highlight'
          SearchFooter.HintClose = 'Close'
          SearchFooter.HintFindNext = 'Find next occurence'
          SearchFooter.HintFindPrev = 'Find previous occurence'
          SearchFooter.HintHighlight = 'Highlight occurences'
          SearchFooter.MatchCaseCaption = 'Match case'
          ShowSelection = False
          SortSettings.DefaultFormat = ssAutomatic
          Version = '7.2.8.0'
          RowHeights = (
            21
            21
            21
            21
            21
            21
            20
            20
            19
            21
            21
            22
            21)
        end
      end
    end
    object Panel_Resize: TPanel
      Left = 983
      Top = 0
      Width = 25
      Height = 532
      Align = alRight
      BevelOuter = bvNone
      Color = 15784647
      TabOrder = 3
      object Button_AllCad: TSpeedButton
        Left = 1
        Top = 2
        Width = 23
        Height = 22
        Caption = '>>'
        Flat = True
        OnClick = Button_AllCadClick
      end
    end
  end
  object AdvDockPanel1: TAdvDockPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 46
    MinimumSize = 3
    LockHeight = False
    Persistence.Location = plRegistry
    Persistence.Enabled = False
    ToolBarStyler = AdvToolBarOfficeStyler1
    UseRunTimeHeight = True
    Version = '6.0.2.1'
    object AdvToolBar1: TAdvToolBar
      Left = 3
      Top = 1
      Width = 146
      Height = 44
      AllowFloating = True
      AutoDockOnClose = True
      Caption = 'Etude'
      CaptionFont.Charset = DEFAULT_CHARSET
      CaptionFont.Color = clWindowText
      CaptionFont.Height = -11
      CaptionFont.Name = 'Segoe UI'
      CaptionFont.Style = []
      CaptionPosition = cpBottom
      CaptionAlignment = taCenter
      CompactImageIndex = -1
      ShowCaption = True
      ShowRightHandle = False
      ShowOptionIndicator = False
      TextAutoOptionMenu = 'Add or Remove Buttons'
      TextOptionMenu = 'Options'
      ToolBarStyler = AdvToolBarOfficeStyler1
      Images = Img_Menus
      ParentOptionPicture = True
      ToolBarIndex = -1
      object Btn_Quitter: TAdvToolBarButton
        Tag = 1
        Left = 2
        Top = 2
        Width = 24
        Height = 24
        Hint = 'Quitter'
        Appearance.CaptionFont.Charset = DEFAULT_CHARSET
        Appearance.CaptionFont.Color = clWindowText
        Appearance.CaptionFont.Height = -11
        Appearance.CaptionFont.Name = 'Segoe UI'
        Appearance.CaptionFont.Style = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ImageIndex = 0
        ParentFont = False
        Position = daTop
        Version = '6.0.2.1'
        OnClick = ToolBarButtonClick
      end
      object Btn_NouvEtude: TAdvToolBarButton
        Tag = 2
        Left = 36
        Top = 2
        Width = 24
        Height = 24
        Hint = 'Nouvelle '#233'tude'
        Appearance.CaptionFont.Charset = DEFAULT_CHARSET
        Appearance.CaptionFont.Color = clWindowText
        Appearance.CaptionFont.Height = -11
        Appearance.CaptionFont.Name = 'Segoe UI'
        Appearance.CaptionFont.Style = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ImageIndex = 1
        ParentFont = False
        Position = daTop
        Version = '6.0.2.1'
        OnClick = ToolBarButtonClick
      end
      object AdvToolBarSeparator1: TAdvToolBarSeparator
        Left = 26
        Top = 2
        Width = 10
        Height = 40
        LineColor = clBtnShadow
      end
      object Btn_OuvrirEtude: TAdvToolBarButton
        Tag = 3
        Left = 60
        Top = 2
        Width = 24
        Height = 24
        Hint = 'Ouverture d'#39'une '#233'tude'
        Appearance.CaptionFont.Charset = DEFAULT_CHARSET
        Appearance.CaptionFont.Color = clWindowText
        Appearance.CaptionFont.Height = -11
        Appearance.CaptionFont.Name = 'Segoe UI'
        Appearance.CaptionFont.Style = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ImageIndex = 2
        ParentFont = False
        Position = daTop
        Version = '6.0.2.1'
        OnClick = ToolBarButtonClick
      end
      object Btn_SauverEtude: TAdvToolBarButton
        Tag = 4
        Left = 84
        Top = 2
        Width = 24
        Height = 24
        Hint = 'Sauvegarde'
        Appearance.CaptionFont.Charset = DEFAULT_CHARSET
        Appearance.CaptionFont.Color = clWindowText
        Appearance.CaptionFont.Height = -11
        Appearance.CaptionFont.Name = 'Segoe UI'
        Appearance.CaptionFont.Style = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ImageIndex = 3
        ParentFont = False
        Position = daTop
        Version = '6.0.2.1'
        OnClick = ToolBarButtonClick
      end
      object AdvToolBarButton5: TAdvToolBarButton
        Tag = 7
        Left = 118
        Top = 2
        Width = 24
        Height = 24
        Hint = 'Syst'#232'mes de VMC'
        Appearance.CaptionFont.Charset = DEFAULT_CHARSET
        Appearance.CaptionFont.Color = clWindowText
        Appearance.CaptionFont.Height = -11
        Appearance.CaptionFont.Name = 'Segoe UI'
        Appearance.CaptionFont.Style = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ImageIndex = 12
        ParentFont = False
        Position = daTop
        Version = '6.0.2.1'
        OnClick = ToolBarButtonClick
      end
      object AdvToolBarSeparator4: TAdvToolBarSeparator
        Left = 108
        Top = 2
        Width = 10
        Height = 40
        LineColor = clBtnShadow
      end
    end
    object AdvToolBar2: TAdvToolBar
      Left = 151
      Top = 1
      Width = 242
      Height = 44
      AllowFloating = False
      AutoArrangeButtons = False
      Caption = 'B'#226'timent'
      CaptionFont.Charset = DEFAULT_CHARSET
      CaptionFont.Color = clWindowText
      CaptionFont.Height = -11
      CaptionFont.Name = 'Segoe UI'
      CaptionFont.Style = []
      CaptionPosition = cpBottom
      CaptionAlignment = taCenter
      CompactImageIndex = -1
      ShowCaption = True
      ShowRightHandle = False
      ShowOptionIndicator = False
      TextAutoOptionMenu = 'Add or Remove Buttons'
      TextOptionMenu = 'Options'
      ToolBarStyler = AdvToolBarOfficeStyler1
      Images = Img_Menus
      ParentOptionPicture = True
      ToolBarIndex = -1
      object AdvToolBarButton_AddLgt: TAdvToolBarButton
        Tag = 6
        Left = 2
        Top = 2
        Width = 24
        Height = 24
        Hint = 'Ajout d'#39'un logement'
        Appearance.CaptionFont.Charset = DEFAULT_CHARSET
        Appearance.CaptionFont.Color = clWindowText
        Appearance.CaptionFont.Height = -11
        Appearance.CaptionFont.Name = 'Segoe UI'
        Appearance.CaptionFont.Style = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ImageIndex = 4
        ParentFont = False
        Position = daTop
        Version = '6.0.2.1'
        OnClick = ToolBarButtonClick
      end
      object AdvToolBarButton_SuppLgt: TAdvToolBarButton
        Tag = 9
        Left = 26
        Top = 2
        Width = 24
        Height = 24
        Hint = 'Suppression du logement courant'
        Appearance.CaptionFont.Charset = DEFAULT_CHARSET
        Appearance.CaptionFont.Color = clWindowText
        Appearance.CaptionFont.Height = -11
        Appearance.CaptionFont.Name = 'Segoe UI'
        Appearance.CaptionFont.Style = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ImageIndex = 5
        ParentFont = False
        Position = daTop
        Version = '6.0.2.1'
        OnClick = ToolBarButtonClick
      end
      object AdvToolBarSeparator2: TAdvToolBarSeparator
        Left = 74
        Top = 2
        Width = 10
        Height = 40
        LineColor = clBtnShadow
      end
      object Btn_Recalcul: TAdvToolBarButton
        Tag = 5
        Left = 84
        Top = 2
        Width = 24
        Height = 24
        Hint = 'Recalcul'
        Appearance.CaptionFont.Charset = DEFAULT_CHARSET
        Appearance.CaptionFont.Color = clWindowText
        Appearance.CaptionFont.Height = -11
        Appearance.CaptionFont.Name = 'Segoe UI'
        Appearance.CaptionFont.Style = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ImageIndex = 6
        ParentFont = False
        Position = daTop
        Version = '6.0.2.1'
        OnClick = ToolBarButtonClick
      end
      object Btn_Arbre: TAdvToolBarButton
        Tag = 8
        Left = 108
        Top = 2
        Width = 24
        Height = 24
        Hint = 'Arbre de calcul'
        Appearance.CaptionFont.Charset = DEFAULT_CHARSET
        Appearance.CaptionFont.Color = clWindowText
        Appearance.CaptionFont.Height = -11
        Appearance.CaptionFont.Name = 'Segoe UI'
        Appearance.CaptionFont.Style = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ImageIndex = 7
        ParentFont = False
        Position = daTop
        Version = '6.0.2.1'
        OnClick = ToolBarButtonClick
      end
      object ToolBtn_Chiffrage: TAdvToolBarButton
        Tag = 18
        Left = 132
        Top = 2
        Width = 24
        Height = 24
        Hint = 'S'#233'lection caisson'
        Appearance.CaptionFont.Charset = DEFAULT_CHARSET
        Appearance.CaptionFont.Color = clWindowText
        Appearance.CaptionFont.Height = -11
        Appearance.CaptionFont.Name = 'Segoe UI'
        Appearance.CaptionFont.Style = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ImageIndex = 8
        ParentFont = False
        Position = daTop
        Version = '6.0.2.1'
        OnClick = ToolBarButtonClick
      end
      object Btn_Print: TAdvToolBarButton
        Tag = 29
        Left = 156
        Top = 2
        Width = 24
        Height = 24
        Hint = 'Aper'#231'u avant impression'
        Appearance.CaptionFont.Charset = DEFAULT_CHARSET
        Appearance.CaptionFont.Color = clWindowText
        Appearance.CaptionFont.Height = -11
        Appearance.CaptionFont.Name = 'Segoe UI'
        Appearance.CaptionFont.Style = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ImageIndex = 9
        ParentFont = False
        Position = daTop
        Version = '6.0.2.1'
        Visible = False
      end
      object ToolBtn_Verrou: TAdvToolBarButton
        Tag = 25
        Left = 190
        Top = 2
        Width = 24
        Height = 24
        Hint = 'Verrouiller le calcul'
        Appearance.CaptionFont.Charset = DEFAULT_CHARSET
        Appearance.CaptionFont.Color = clWindowText
        Appearance.CaptionFont.Height = -11
        Appearance.CaptionFont.Name = 'Segoe UI'
        Appearance.CaptionFont.Style = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ImageIndex = 11
        ParentFont = False
        Position = daTop
        Version = '6.0.2.1'
        OnClick = ToolBarButtonClick
      end
      object AdvToolBarSeparator3: TAdvToolBarSeparator
        Left = 180
        Top = 2
        Width = 10
        Height = 40
        LineColor = clBtnShadow
      end
      object AdvBtn_Print: TAdvToolBarButton
        Tag = 26
        Left = 214
        Top = 2
        Width = 24
        Height = 24
        Hint = 'Impression'
        Appearance.CaptionFont.Charset = DEFAULT_CHARSET
        Appearance.CaptionFont.Color = clWindowText
        Appearance.CaptionFont.Height = -11
        Appearance.CaptionFont.Name = 'Segoe UI'
        Appearance.CaptionFont.Style = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ImageIndex = 14
        ParentFont = False
        Position = daTop
        Version = '6.0.2.1'
        OnClick = ToolBarButtonClick
      end
      object AdvToolBarButton_DuplicateLgt: TAdvToolBarButton
        Tag = 27
        Left = 50
        Top = 2
        Width = 24
        Height = 24
        Hint = 'Dupliquer le logement courant'
        Appearance.CaptionFont.Charset = DEFAULT_CHARSET
        Appearance.CaptionFont.Color = clWindowText
        Appearance.CaptionFont.Height = -11
        Appearance.CaptionFont.Name = 'Segoe UI'
        Appearance.CaptionFont.Style = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ImageIndex = 15
        ParentFont = False
        Position = daTop
        Version = '6.0.2.1'
        OnClick = ToolBarButtonClick
      end
    end
  end
  object MainMenu1: TMainMenu
    Images = Img_Menus
    Left = 592
    Top = 8
    object Fichier1: TMenuItem
      Caption = 'Etude'
      Hint = 'Affichage des param'#232'tres g'#233'n'#233'raux'
      object Nouvelle12: TMenuItem
        Tag = 2
        Caption = 'Nouveau'
        ImageIndex = 1
        OnClick = ToolBarButtonClick
      end
      object Ouvrir1: TMenuItem
        Tag = 3
        Caption = 'Ouvrir'
        ImageIndex = 2
        OnClick = ToolBarButtonClick
      end
      object Supprimer1: TMenuItem
        Tag = 28
        Caption = 'Supprimer'
        OnClick = ToolBarButtonClick
      end
      object Enregistrer1: TMenuItem
        Tag = 4
        Caption = 'Enregistrer'
        Enabled = False
        ImageIndex = 3
        OnClick = ToolBarButtonClick
      end
      object Menu_Enregistrersous: TMenuItem
        Tag = 10
        Caption = 'Enregister sous'
        Enabled = False
        ImageIndex = 3
        Visible = False
        OnClick = ToolBarButtonClick
      end
      object Quitter1: TMenuItem
        Tag = 1
        Caption = 'Quitter'
        ImageIndex = 0
        OnClick = ToolBarButtonClick
      end
    end
    object Btiment1: TMenuItem
      Caption = 'B'#226'timent'
      object Ajouterunlogement1: TMenuItem
        Tag = 6
        Caption = 'Ajouter un logement'
        ImageIndex = 4
        OnClick = ToolBarButtonClick
      end
      object Supprimerlelogementslectionn1: TMenuItem
        Tag = 9
        Caption = 'Supprimer le logement courant'
        ImageIndex = 5
        OnClick = ToolBarButtonClick
      end
      object Dupliquerlelogementslection1: TMenuItem
        Tag = 27
        Caption = 'Dupliquer le logement courant'
        ImageIndex = 15
        OnClick = ToolBarButtonClick
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Recalculer1: TMenuItem
        Tag = 5
        Caption = 'Recalculer'
        ImageIndex = 6
        OnClick = ToolBarButtonClick
      end
      object Calculerlematriel1: TMenuItem
        Tag = 18
        Caption = 'Calculer le mat'#233'riel'
        ImageIndex = 8
        OnClick = ToolBarButtonClick
      end
      object Arbredecalcul1: TMenuItem
        Tag = 8
        Caption = 'Arbre de calcul'
        ImageIndex = 7
        OnClick = ToolBarButtonClick
      end
      object Imprimer1: TMenuItem
        Caption = 'Impressions'
        ImageIndex = 9
        Visible = False
        object Rsultats1: TMenuItem
          Caption = 'R'#233'sultats'
          object Rcapitulatifdesbouchesetentresdair1: TMenuItem
            Tag = 11
            Caption = 'R'#233'capitulatif des bouches et entr'#233'es d'#39'air'
            OnClick = ToolBarButtonClick
          end
          object caractristiquesdbitmaxi1: TMenuItem
            Tag = 13
            Caption = 'Caract'#233'ristiques '#224' d'#233'bit maxi'
            OnClick = ToolBarButtonClick
          end
          object Caractristiquesdbitmini1: TMenuItem
            Tag = 15
            Caption = 'Caract'#233'ristiques '#224' d'#233'bit mini'
            OnClick = ToolBarButtonClick
          end
          object Pressionslimitespourunbonfonctionnementauxbouches1: TMenuItem
            Tag = 20
            Caption = 'Pressions limites pour un bon fonctionnement aux bouches'
            OnClick = ToolBarButtonClick
          end
          object Pertesdechargeentrelecaissonetlesbouches1: TMenuItem
            Tag = 24
            Caption = 'Pertes de charge entre le caisson et les bouches'
            OnClick = ToolBarButtonClick
          end
        end
        object Schmas1: TMenuItem
          Caption = 'Sch'#233'mas'
          object Schmadescollones1: TMenuItem
            Tag = 14
            Caption = 'Colonnes'
            OnClick = ToolBarButtonClick
          end
          object Schmadtages1: TMenuItem
            Tag = 19
            Caption = 'Etages'
            OnClick = ToolBarButtonClick
          end
        end
      end
      object Donnesgnrales1: TMenuItem
        Tag = 26
        Caption = 'Impressions'
        OnClick = ToolBarButtonClick
      end
    end
    object Consultation1: TMenuItem
      Caption = 'Banques'
      object SystmesdeVMC1: TMenuItem
        Tag = 7
        Caption = 'Syst'#232'mes de VMC'
        ImageIndex = 12
        OnClick = ToolBarButtonClick
      end
      object Banquesdedonnes1: TMenuItem
        Tag = 17
        Caption = 'Mat'#233'riel'
        ImageIndex = 13
        Visible = False
        OnClick = ToolBarButtonClick
      end
    end
    object N1: TMenuItem
      Caption = '?'
      GroupIndex = 3
      object Options1: TMenuItem
        Tag = 22
        Caption = 'Options'
        ImageIndex = 17
        OnClick = ToolBarButtonClick
      end
      object Apropos1: TMenuItem
        Caption = 'A propos'
        Visible = False
        OnClick = ToolBarButtonClick
      end
      object Vrifierlesmisesjour1: TMenuItem
        Tag = -100
        Caption = 'V'#233'rifier les mises '#224' jour'
        Visible = False
        OnClick = ToolBarButtonClick
      end
      object Aide1: TMenuItem
        Tag = 29
        Caption = 'Aide'
        Hint = 'Aide'
        ImageIndex = 16
        OnClick = ToolBarButtonClick
      end
    end
  end
  object ImageList1: TImageList
    Height = 32
    Width = 32
    Left = 14
    Top = 88
    Bitmap = {
      494C010127002C00280020002000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000800000004001000001002000000000000080
      0200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FDFDFDFFF5F5
      F5FFE6E6E6FFDADADAFFDFDFDFFFF5F5F5FF000000000000000000000000FBFB
      FBFFF0F0F0FFE4E4E4FFDCDCDCFFDCDCDCFFE3E3E3FFF0F0F0FFFBFBFBFF0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FDFDFDFFF4F4F4FFD3D2D2FF9E91
      91FF987575FFA57D7DFF998585FFD8D8D8FFFBFBFBFFFEFEFEFFF2F2F2FFC4BC
      B5FFBC9373FFD07D48FFDC793FFFCF7642FFB88A67FFB3A89DFFD5D5D5FFF0F0
      F0FFFEFEFEFF0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FEFEFEFFF6F6F6FFD7D7D7FF978787FF976F6FFFC49F
      9FFFE2BFBFFFF1DEDEFFB48D8DFFABA7A7FFECECECFFE5E4E3FFCDAC88FFF9CA
      99FFE79C77FFEEB699FFF1C2A8FFF6D4B8FFFDEBCAFFFDDAA9FFC9A179FFC0BE
      BBFFEBEBEBFFFEFEFEFF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F9F9F9FFE3E2E2FF9D9292FF8B6262FFBF9797FFE5C3C3FFECCF
      CFFFE7C8C8FFE9CACAFFE7D5D5FF977878FFC4C3C1FFDCB384FFFCE0B7FFFEF0
      D0FFFFF6E0FFFFFAF0FFFFFBF4FFFFF8E7FFFFF3D7FFFEE9C3FFFAD6A9FFDDA8
      73FFC0BEBBFFF0F0F0FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FCFC
      FCFFEEEEEEFFADA7A7FF7F5D5DFFA97B7BFFD9AEAEFFE1BCBCFFE7C6C6FFEDD0
      D0FFEED3D3FFE9CACBFFF0DCDCFFBD9B9BFFC69974FFE89263FFFCE1B8FFFEF0
      CFFFFFF6E0FFFFFAEFFFFFFBF3FFFFF7E6FFFFF3D6FFFEE9C3FFFAD6AAFFF7C3
      90FFC99B70FFD5D5D5FFFBFBFBFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FAFAFAFFCCCA
      CAFF836B6BFF905F5FFFC79494FFD5A6A6FFDCB4B4FFE3BFBFFFE8C7C7FFECD0
      D0FFF4DDDDFFE9CCCCFFEBCFCFFFE9D4C7FFDD6A32FFEDA274FFFBDCB2FFFEEC
      C8FFFFF3D7FFFFF6E1FFFFF7E4FFFCEAD0FFF9DDBAFFFCE3BBFFEEAA7CFFF3B3
      80FFEA8A4EFFB2A79CFFEEEEEEFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000586BD00289F
      D3002DA3D50029A0D300219ACE001491C7000787BF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000002184C600084263000842630008426300084263000842
      6300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C0BBBBFF7A4E
      4EFFB07676FFC89090FFD09E9DFFD8ABABFFDFB7B7FFE4C0C1FFE8C8C8FFECCE
      CEFFF5DFDFFFEED3D3FFEAD0D0FFECBB96FFCE3B06FFDF6F39FFF6C394FFFCE2
      B9FFFEEDC9FFFFF1D2FFFFF2D3FFEEA97AFFD8591FFFEEA979FFF4BC8BFFDD69
      37FFCD3904FFC1936BFFE0E0E0FFFEFEFEFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000188BF000E92C6007ACF
      F70081D0FB007ECFF8007ACEF70075CBF6006CC6F20050B7E600289FD3000686
      BF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000002184C60073C6DE00FFFFFF0073C6DE0073C6DE0042A5C6002184
      C600084263000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008F7B7BFFB175
      75FFC28687FFCB9595FFD3A3A3FFDAAFAFFFE0BABAFFE5C2C2FFE8C7C7FFECCE
      CEFFF5DEDFFFF4DDDDFFE8CDCDFFE89B6AFFDA5A1DFFE2722EFFEC9050FFFAD4
      A9FFFDE5BEFFFEEAC5FFFEEBC5FFF7A55CFFED863BFFE37537FFF3B782FFE072
      3DFFDF692FFFE99552FFD5D5D5FFFDFDFDFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000489C0000696CA001196C9007DD3
      F3007DD1F70076CBF70078CBF60076CBF6004DB1DE0066C1EB0081D1FB0066C4
      EF00269ED1000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000004ABDFF004ABDFF004ABDFF004ABDFF004ABDFF004ABD
      FF002184C6000842630008426300084263000842630008426300000000000000
      0000000000000000000000000000000000000000000000000000938080FFB47B
      7BFFC58C8BFFCE9A9AFFD6A7A7FFDCB3B2FFE1BCBCFFE5C2C2FFE7C5C5FFEFD4
      D4FFF5DEDEFFF5E0DFFFEBD1D1FFEC9C60FFE57933FFED8F43FFF8AF55FFFEE3
      AFFFFFEFCCFFFFF0CEFFFFEFCDFFFFAF5DFFFFA851FFFDA54FFFF7A55EFFE986
      4EFFDA5B22FFF28F44FFCECDCCFFFCFCFCFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000078EC4000F9FD1001599CA0087DD
      F30088DAF7007BD1F70077CCF60077CCF70039A6D4004AB0DD007BCFFA007BCE
      F7007DCFF80041AFE00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000002184C6000842630008426300084263000842
      630008426300FFFFFF0073C6DE0073C6DE0042A5C6002184C600084263000000
      0000000000000000000000000000000000000000000000000000D1CCCCFF9564
      64FFCA9393FFD1A09FFFD8ABABFFDDB6B6FFE2BDBDFFE5C2C1FFE7C6C6FFF4DC
      DDFFF5DEDEFFF5E0E0FFF2DBDBFFF0A86AFFED8F44FFFAB55AFFFFE4ADFFFFF1
      D2FFFFF2D5FFFFE2B5FFFFDAA5FFFFBB6EFFFFB260FFFFA953FFFFA953FFF092
      4EFFDC5A20FFE6702AFFD1CFCDFFFDFDFDFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000B92C6001BA9D8001B9ECC0095E5
      F30093E5F70088DCF70083D8FB0050A5CE002689B5003AA6D5006BC5F00078CC
      F70077CBF6007BCEF8000B8FC200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000002184C60073C6DE00FFFFFF0073C6DE0073C6DE0042A5
      C6002184C600084263004ABDFF004ABDFF004ABDFF004ABDFF00000000000000
      0000000000000000000000000000000000000000000000000000FDFDFDFF8564
      64FFCB9A9AFFD4A4A4FFDBAFAFFFDFB8B8FFE3BEBEFFE6C3C3FFF2D9D9FFF4DD
      DDFFF5DEDEFFF5E0DFFFF4E0E0FFF3B982FFF2A25FFFFFEDC6FFFFF2D4FFFFF3
      D9FFFFE9C1FFFFD697FFFFD392FFFFCC87FFFFC076FFFFB362FFFFAE5CFFFEAA
      57FFD54B11FFE27532FFDEDEDEFFFEFEFEFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000F96CA0027B2E10022A0D000ACEA
      F400A6EEF80093E6F70091E5FC004C9DBD0015658900278EBA0058BAE6007ACE
      F80076CBF60076CBF6000B8FC200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CAB4A60069473100694731006947310069473100694731006947
      31006947310069473100694731004ABDFF004ABDFF004ABDFF004ABDFF004ABD
      FF004ABDFF000842630008426300084263000842630008426300694731006947
      310000000000000000000000000000000000000000000000000000000000BCB4
      B4FFA57A7AFFD8AAAAFFDCB3B3FFE1BABBFFE3BEBEFFF0D5D5FFF4DCDCFFF5DD
      DDFFF5DEDEFFF5E0DFFFF6E1E1FFF5D2B0FFF7AE67FFFFF1D2FFFFF4DAFFFFF5
      DFFFFFF0CFFFFFE9B4FFFFE4ACFFFFDA9DFFFFCD89FFFFBE72FFFFAE59FFFFA8
      51FFE26825FFC68B5DFFECECECFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001399CC0034BBE90028A5D300C9EF
      F600C2F3FA00A7EEF70099EAF8008BDEF600216D8F0023769C0072C9F40077CC
      F70076CBF60076CBF6000B8FC200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CAB4A600FFFFFF00B7A29300B7A29300B7A29300B7A29300B7A2
      9300B7A29300B7A293002184C600084263000842630008426300084263000842
      630073C6DE00FFFFFF0073C6DE0073C6DE0042A5C6002184C600084263006947
      310000000000000000000000000000000000000000000000000000000000FCFC
      FCFF896565FFDBB3B3FFDEB6B6FFE2BCBCFFE8C7C8FFF4DBDBFFF4DCDCFFF5DD
      DDFFF5DEDEFFF5E0E0FFF6E1E1FFF6E6DFFFF8B571FFFFDFAFFFFFF5DFFFFFF7
      E5FFFFF9EAFFFFF9E5FFFFF5C8FFFFEABDFFFFE1B0FFFFCA87FFFFB463FFFFA8
      51FFEB7B31FFBBAD9FFFF9F9F9FF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000189DD00045C7F20032AAD700F4FB
      FC00F3FFFF00D5FAFE00BCF6FB00A7F0FB004190AC00307E9F0080D4F70078CE
      F70076CBF60076CBF6000B8FC200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CAB4A600FFFFFF00FFFFFF00FFFFFF00FEFCFB00FCF9F800FAF6
      F400F8F3F1002184C60073C6DE00FFFFFF0073C6DE0073C6DE0042A5C6002184
      C600084263004ABDFF004ABDFF004ABDFF004ABDFF004ABDFF00B7A293006947
      3100000000000000000000000000000000000000000000000000000000000000
      0000B5ABABFFB5908FFFE1BABAFFE2BCBCFFEFD3D3FFF4DBDBFFF4DCDCFFF5DD
      DDFFF5DEDEFFF6E0E0FFDEC4C3FF92AA76FFC7D992FFFECE8DFFFFE4B2FFFFF6
      DEFFFFFAF0FFFFFCF5FFFFFCDAFFFFEEBAFFFFDC9FFFFFCA84FFFFB768FFFCA3
      4EFFD5925BFFECECECFF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000189DD00033B5E5001A98CB0083CE
      E60081CEE70073C5E10073C7E1007DD1E7008BDEEF008BDDEE0095E2FA0088D9
      FA007ACFF70076CBF7000B8FC200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CAB4A600FFFFFF0030C8600030C8600030C860002DC55A002AC2
      540027BF4E001EB63C004ABDFF004ABDFF004ABDFF004ABDFF004ABDFF004ABD
      FF00069E0C00039B060000980000009800000098000000980000B7A293006947
      3100000000000000000000000000000000000000000000000000000000000000
      0000F7F7F7FF8B6666FFE4C2C2FFE4C0C0FFF4DADAFFF4DBDBFFF4DCDCFFF5DD
      DDFFEBD1D2FF9CB481FF98D786FFE9FDE9FFADF4A7FFE7D4A8FFFEE2ABFFFFF1
      C0FFFFFDDDFFFFFEEEFFFFFCD6FFFFEDB9FFFFDB9EFFFFC983FFFEB86BFFE898
      52FFD5D2CFFFFDFDFDFF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000A8CC2002EB5D80034B6D70025A5
      CA0029ACCF0028B1D5001CA4CE001398C7001C9ECC0038AFD70064C9E70088DC
      F70092DEFE0033A6D8000B8FC200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CAB4A600FFFFFF0030C86000FFFFFF0030C8600030C860002DC5
      5A007BB57B00F8F8F700F5F5F400F3F3F200F0F0EF00EDEDEC00EAEAE900E8E8
      E70039A54200069E0C00039B0600FFFFFF000098000000980000B7A293006947
      3100000000000000000000000000000000000000000000000000000000000000
      000000000000A69898FFC6A3A3FFE8C7C7FFF4DADAFFF4DBDBFFF0D6D7FFB3B8
      9BFF7CCD72FFC9FBC9FFEBFEEEFFEDFEEFFFD6FED8FF7AB55CFFF3DCBAFFFCE2
      B4FFFDF6D9FFFFEFD0FFFFF6E0FFFFEDC4FFFED89CFFFBBF80FFE2A974FFD6D3
      D0FFFDFDFDFF0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000098CC0006EEBF6004AB7CB000488
      BA001088B20065DEEA0054DEF00032C1E00018A9D3000493C7000389C0001698
      CA005DC2E60039ACD8000B8FC200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CAB4A600FFFFFF0030C8600030C8600030C8600030C860007BB5
      7B00FEFEFD00FBFBFA00CDFFCD0000C800000099000000C80000CDFFCD00ECEC
      EB00E4E4E40039A54200069E0C00039B06000098000000980000B7A293006947
      3100000000000000000000000000000000000000000000000000000000000000
      000000000000F2F1F1FF906B6BFFEDD1D1FFF3D9D9FFCFC1B7FF7CBA70FF91EF
      93FFBEFEC3FFC5FECAFFCFFED4FFCCFED2FFA8FEA7FF4BE833FFC0A59DFFF8E8
      D5FFF8D3AAFFF9C791FFF9C288FFF7BB7FFFDCAD80FFC6B8A8FFF5F5F5FF0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000A91C500168FB8000084
      BA00087FAF007FF4F70070F2FB004DD7EB0032C0E0001392BD000486B600017F
      B2001EA4CB000C8CC1000B8FC200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CAB4A600FFFFFF0030C8600030C8600030C860007BB57B00FEFE
      FD0000C80000FEFEFD0000C80000009D000000A50000009D000000C80000EEEE
      ED0000C80000E4E4E40039A54200069E0C00039B060000980000B7A293006947
      3100000000000000000000000000000000000000000000000000000000000000
      000000000000000000009D8B8BFFD0B5B6FF8BB57AFF65DE5AFF7FFE70FF7CFE
      6DFF86FE7FFF93FE91FF93FE92FF82FE74FF4AFC27FF20F900FF5FA145FFEFDD
      DDFFFBF6F6FFF8F4F4FFBB8F8FFFC5C4C4FFF5F5F5FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000003699C000389
      BD00057BAD0041C2DD0065E5F30060E5F30049D1E9001184AF000280B400057B
      AB001EA4CB000182BB0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CAB4A600FFFFFF0030C8600030C8600030C860007BB57B00FEFE
      FD0000A50000FEFEFD0000A10000CDFFCD0000A50000CDFFCD0000990000F1F1
      F00000A50000E9E9E80039A5420009A11200069E0C00039B0600B7A293006947
      3100000000000000000000000000000000000000000000000000000000000000
      00000000000000000000EFEEEEFF967475FF7ADA63FF25F801FF1EF600FF24F9
      00FF34FB06FF3BFC0EFF2FF908FF18F200FF04E600FF00E000FF0ECA09FFC4A4
      A1FFFBF5F5FFFDFBFBFFE2C9C9FFA19393FFE5E5E5FFFEFEFEFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000038C
      C0000376A70000000000000000000B90C200139AC900097FAF000383B600057A
      AD000288C0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CAB4A600FFFFFF0030C86000FFFFFF0030C860007BB57B00FEFE
      FD0000C80000FEFEFD0000C8000000A10000CDFFCD00009D000000C80000F4F4
      F30000C80000ECECEB0039A54200FFFFFF0009A11200069E0C00B7A293006947
      3100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A38F8FFFB2B5A2FF03CA03FF00D400FF00D9
      00FF00DD00FF00DC00FF00D400FF00CE00FF00CF00FF00CE00FF00CB00FF6594
      4AFFF0E0E0FFFDFBFBFFFBF8F8FFBB9090FFCBCBCBFFF8F8F8FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000023A5
      D7000E83B50000000000000000000000000000000000046D9D000388BB00056A
      9A00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CAB4A600FFFFFF0030C86000FFFFFF00FFFFFF0030C860007BB5
      7B00FEFEFD00FEFEFD00CDFFCD0000C8000000A1000000C80000CDFFCD00F7F7
      F600F1F1F00039A5420014AC2700FFFFFF00FFFFFF000AA21500B7A293006947
      3100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E4E2E2FF9F7E7EFF4FB14AFF00AE00FF00B3
      00FF00B600FF00B400FF00B600FF00BB00FF00BD00FF00BE00FF00BB00FF15A7
      0FFFCAA9A8FFFDFAFAFFFEFEFEFFE1C4C4FFA59A9AFFEDEDEDFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003BAC
      D40051CAEE0007689800000000000000000000000000046B9A00038ABD000567
      9600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CAB4A600FFFFFF0030C86000FFFFFF0030C8600030C8600030C8
      60007BB57B00FEFEFD00FEFEFD00FEFEFD00FEFEFD00FEFEFD00FCFCFB00FAFA
      F90039A5420019B1330017AF2D00FFFFFF0010A821000DA51B00B7A293006947
      3100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009D8585FFB0BA9FFF018C01FF0095
      00FF009800FF009E00FF00A500FF00AA00FF00AC00FF00AC00FF2A9A1FFF939C
      78FFE4CCCCFFF4E7E7FFFFFFFEFFF4EAEAFFA98E8EFFE9E9E9FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001077
      A00070EAF40059CFE500137DA7000000000006669600047EB1000388BB000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CAB4A600FFFFFF0030C8600030C8600030C8600030C8600030C8
      600030C8600030C8600030C8600030C860002FC75D002CC4570028C0510025BD
      4B001FB73F001CB4390019B1330017AF2D0014AC270010A82100B7A293006947
      3100000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E0DDDDFFA78787FF4C9640FF0078
      00FF008200FF008C00FF009400FF009900FF1E9417FF8D9F76FFE5CFCEFFF8E8
      E8FFF8E9E9FFEFDADAFFFFFEFEFFF4EBEBFFA98F8FFFEAEAEAFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000147DA5005ED5E30075EEF8005AD1EA004BC4EB0027ACDE000674A5000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CAB4A600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFCFB00FCF9
      F800F8F3F100F6F0EE00F4EEEA00F2EBE700F0E8E400EEE5E000EBE0DA006947
      3100000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A08585FFAFB599FF0168
      01FF007000FF007800FF228314FF829E70FFE4CDCDFFF7E5E5FFF7E6E6FFF8E8
      E7FFF8E9E9FFF0DBDBFFFDFCFCFFEFE0E0FFAA9494FFEFEFEFFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000157EA5002A99BA002795BA000E74A100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CAB4A600CAB4A600CAB4A600CAB4A600CAB4A600CAB4A600CAB4
      A600CAB4A600CAB4A600CAB4A600CAB4A600CAB4A600CAB4A600CAB4A600CAB4
      A600CAB4A600CAB4A600CAB4A600CAB4A600CAB4A600CAB4A600CAB4A600CAB4
      A600000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DDD9D9FFAE8F8EFF4183
      36FF186A0CFF8A9D6DFFE6CFCCFFF6E2E2FFF6E4E4FFF7E5E5FFF8E6E6FFF8E8
      E8FFF9EBEBFFF8EFEFFFE7D3D3FFCE9F9FFFA49999FFE9E9E9FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A28686FFC8C2
      B2FFE4CFC9FFF5E0E0FFF6E1E1FFF6E2E2FFF6E4E3FFF7E5E5FFF8E8E8FFFBF2
      F2FFEDDCDDFFCA9D9DFFD2A7A7FFF8F0F0FFB59C9CFFD0D0D0FFF9F9F9FF0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DED9D9FFB796
      96FFF7E4E4FFF5E0E0FFF6E1E1FFF6E2E2FFF7E4E4FFFAEEEEFFF2E7E7FFCBA5
      A5FFAF9292FFC5BEBEFFD0A7A8FFF2E1E2FFEEE2E2FFA39A9AFFE8E8E8FFFEFE
      FEFF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000AA8F
      8FFFE3CFCFFFF6E2E2FFF6E1E1FFF8E9E9FFF6EDEDFFCEAFAFFFAF8989FFC8C3
      C3FFFBFBFBFF00000000B9A3A3FFE6C9CAFFF9F2F2FFBEA4A4FFCBCBCBFFF7F7
      F7FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F1F0
      F0FFA48383FFCDB1B1FFE8D7D7FFD3BABAFFA98282FFB8AFAFFFF7F7F7FF0000
      00000000000000000000F5F4F4FFCCA2A3FFF0DCDCFFF3EAEAFF9F9292FFE4E4
      E4FFFEFEFEFF0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FBFAFAFFC0B4B3FFA89090FFB4A6A6FFF0EFEFFFFEFEFEFF000000000000
      0000000000000000000000000000C3B2B2FFE3C2C3FFF7EDEDFFC8AFAFFFC7C6
      C6FFF8F8F8FF0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F8F8F8FFC9A0A0FFEDD6D7FFEEE1E1FFA89D
      9DFFF6F6F6FF0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C8BBBBFFCB9FA0FFB89999FFDEDD
      DDFFFEFEFEFF0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000002DA5CD00016A9200016A9200016A9200016A9200016A9200016A
      9200016A9200016A9200016A9200016A9200016A9200016A9200016A9200016A
      9200016A9200016A9200016A9200016A9200016A9200016A9200016A92000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000035363400283132000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009060600090606000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000586BD00289F
      D3002DA3D50029A0D300219ACE001491C7000787BF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000002DA5CD0030A8D00030A8D00030A8D00030A8D00030A8D00030A8
      D00030A8D00030A8D00030A8D00030A8D00030A8D00030A8D00030A8D00030A8
      D00030A8D00030A8D00030A8D00030A8D00030A8D00030A8D000016A92000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C9762B00A24F2200A24F2200A24F2200A24F2200A24F
      2200A24F2200A24F2200A24F2200A24F2200A24F2200A24F2200A24F2200A24F
      2200A24F2200A24F2200A24F2200A24F2200A24F2200A24F2200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000002831320016596F00006890002831320000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000090606000D8A8A800E2C0C000906060000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000188BF000E92C6007ACF
      F70081D0FB007ECFF8007ACEF70075CBF6006CC6F20050B7E600289FD3000686
      BF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000002DA5CD006CC7F2006CC7F2006CC7F2006CC7F2006CC7F2006CC7
      F2006CC7F2006CC7F2006CC7F2006CC7F2006CC7F2006CC7F2006CC7F2006CC7
      F2006CC7F2006CC7F2006CC7F2006CC7F2006CC7F2006CC7F200016A92000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C9762B00F3F3F300F1F1F100F0F0F000EFEFEF00EEEE
      EE00EDEDED00ECECEC00EBEBEB00EAEAEA00E9E9E900E7E7E700E4E4E400E2E2
      E200E0E0E000DDDDDD00DBDBDB00D9D9D900D7D7D700A24F2200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000012526D00388CAD000987B0000090C8000068900028313200000000000000
      0000000000000000000000000000000000000000000000000000000000009060
      6000D8A8A800E2C0C000F0DEDE00FFFFFF009060600000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000489C0000696CA001196C9007DD3
      F3007DD1F70076CBF70078CBF60076CBF6004DB1DE0066C1EB0081D1FB0066C4
      EF00269ED1000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000002DA5CD006CC7F2006CC7F2006CC7F2006CC7F2006CC7F200117F
      A7006CC7F2006CC7F2006CC7F2006CC7F2006CC7F200117FA7006CC7F2006CC7
      F2006CC7F2006CC7F2006CC7F200117FA7006CC7F2006CC7F200016A92000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C9762B00F4F4F400868686006B6B6B006B6B6B006B6B
      6B006B6B6B006B6B6B006B6B6B006B6B6B006B6B6B006B6B6B006B6B6B006B6B
      6B006B6B6B006B6B6B006B6B6B006B6B6B00D9D9D900A24F2200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000006890000090C80060C8F80030A8D0000090C80000689000283132000000
      000000000000000000000000000000000000000000000000000090606000D8A8
      A800E2C0C000F0DEDE00FFFFFF00F0DEDE009060600000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000078EC4000F9FD1001599CA0087DD
      F30088DAF7007BD1F70077CCF60077CCF70039A6D4004AB0DD007BCFFA007BCE
      F7007DCFF80041AFE00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000002DA5CD00B9E6FB00B9E6FB00117FA700B9E6FB00B9E6FB00117F
      A700B9E6FB00B9E6FB00117FA700B9E6FB00B9E6FB00117FA700B9E6FB00B9E6
      FB00117FA700B9E6FB00B9E6FB00117FA700B9E6FB00B9E6FB00016A92000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C9762B00F5F5F50086868600FF860500F1F1F100F0F0
      F000EFEFEF00EEEEEE00EDEDED00ECECEC00EBEBEB00EAEAEA00E9E9E900E7E7
      E700E4E4E400E2E2E2000E3DC400DDDDDD00DBDBDB00A24F2200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000006890000090C80060C8F80030A8D0000090C800006890002831
      3200000000000000000000000000000000000000000090606000D8A8A800E2C0
      C000F0DEDE00FFFFFF00F0DEDE00D08080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000B92C6001BA9D8001B9ECC0095E5
      F30093E5F70088DCF70083D8FB0050A5CE002689B5003AA6D5006BC5F00078CC
      F70077CBF6007BCEF8000B8FC200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000002DA5CD002DA5CD002DA5CD002DA5CD002DA5CD002DA5CD002D2D
      2D006094A8002DA5CD002DA5CD002DA5CD002DA5CD002DA5CD002DA5CD002DA5
      CD002DA5CD002DA5CD002DA5CD002DA5CD002DA5CD002DA5CD002DA5CD000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C9762B00F6F6F60086868600F4F4F400FF860500F1F1
      F100F0F0F000EFEFEF00EEEEEE00EDEDED00ECECEC00EBEBEB00EAEAEA00E9E9
      E9000E3DC400526AE400E2E2E200FF860500DDDDDD00A24F2200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000006890000090C80060C8F80030A8D0000090C8000068
      90002831320000000000000000000000000090606000D8A8A800E2C0C000F0DE
      DE00FFFFFF00F0DEDE00D0808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000F96CA0027B2E10022A0D000ACEA
      F400A6EEF80093E6F70091E5FC004C9DBD0015658900278EBA0058BAE6007ACE
      F80076CBF60076CBF6000B8FC200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8686000C8686000C8686000C8686000C86860005652
      4F002D2D2D002C424C006094A800C8686000C8686000C8686000C8686000C868
      6000C8686000C8686000C8686000C86860000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C9762B00F7F7F70086868600F5F5F500FFB25F00F3F3
      F300F1F1F100F0F0F000EFEFEF00EEEEEE00EDEDED00ECECEC000E3DC400526A
      E400E9E9E900E7E7E700FF860500E2E2E200E0E0E000A24F2200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000006890000090C80060C8F80030A8D0000090
      C80000689000283132000000000090606000D8A8A800E2C0C000F0DEDE00FFFF
      FF00F0DEDE00D080800000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001399CC0034BBE90028A5D300C9EF
      F600C2F3FA00A7EEF70099EAF8008BDEF600216D8F0023769C0072C9F40077CC
      F70076CBF60076CBF6000B8FC200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8686000F1AB7200F0A76B00EEA26500EE9E5F006094
      A800215A7300428CDE00396BAD00215A7300E6823900E57D3300E57B3000E57B
      3000E57B3000E57B3000C8686000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C9762B00F8F8F800868686000E3DC400F5F5F500FF86
      0500F3F3F300F1F1F100F0F0F000EFEFEF000E3DC400526AE400ECECEC00EBEB
      EB00EAEAEA00FFB25F00E7E7E700E4E4E400E2E2E200A24F2200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000006890000090C80060C8F80030A8
      D0000090C8000068900090606000D8A8A800E2C0C000F0DEDE00FFFFFF00F0DE
      DE00D08080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000189DD00045C7F20032AAD700F4FB
      FC00F3FFFF00D5FAFE00BCF6FB00A7F0FB004190AC00307E9F0080D4F70078CE
      F70076CBF60076CBF6000B8FC200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8686000F2B07800F1AB7200F0A76B00EEA26500EE9E
      5F0054777B005AB5EF003173CE00396BAD00424D5A00E7843C00E67F3600E57B
      3000E57B3000C868600000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C9762B00F9F9F90086868600F7F7F7000E3DC400FFB2
      5F000E3DC4000E3DC400F1F1F1000E3DC400EFEFEF00EEEEEE00EDEDED00ECEC
      EC00EBEBEB00FF860500E9E9E900E7E7E700E4E4E400A24F2200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000006890000090C80060C8
      F80030A8D00090606000D8A8A800E2C0C000F0DEDE00FFFFFF00F0DEDE00D080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000189DD00033B5E5001A98CB0083CE
      E60081CEE70073C5E10073C7E1007DD1E7008BDEEF008BDDEE0095E2FA0088D9
      FA007ACFF70076CBF7000B8FC200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8686000F4B57F00F2B07800F1AB7200F0A76B00EFA4
      68006094A80054777B00396BAD00A37676000098C8001C1C1C00E7843C00E67F
      3600C86860000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C9762B00FAFAFA0086868600F8F8F800F7F7F700F6F6
      F600FF860500F4F4F4000E3DC400F1F1F100F0F0F000EFEFEF00EEEEEE00FF86
      0500FFB25F00EBEBEB00EAEAEA00E9E9E900E7E7E700A24F2200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000006890000090
      C80090606000D8A8A800E2C0C000F0DEDE00FFFFFF00F0DEDE00D08080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000B8FC20038BDDC0038B7D5002093
      B6002198BB002DB6D70022ADD400159ECB001298C70020A0CE0040B2D8006ACC
      EA008ADCF80082D4F8000B8FC200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8686000F6BC8800F4B7820092380400923804009238
      04009238040054777B00A37676004ABEDF0030A8D0000098C8001C1C1C00C868
      6000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C9762B00FBFBFB0086868600F9F9F900F8F8F800F7F7
      F700F6F6F600FF860500F4F4F400F3F3F300F1F1F100F0F0F000FF860500EEEE
      EE00EDEDED00ECECEC00EBEBEB00EAEAEA00E9E9E900A24F2200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009060
      6000D8A8A800E2C0C000F0DEDE00FFFFFF00F0DEDE00D0808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000098CC0006BEDF70055B4C1001155
      7500145A7B0055C5D40060EAF8003ECBE50023B5D9000C9FCE00018BC1000388
      BF0028A3D10074D0EF000B8FC200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8686000F7C18F00F6BC880092380400000000000000
      0000C868600068A0C00094E4F60060C8F8004ABEDF0030A8D0000098C8001C1C
      1C00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C9762B00FCFCFC0086868600FAFAFA00F9F9F900F8F8
      F800F7F7F700F6F6F600FF860500F4F4F400F3F3F300FF860500F0F0F000EFEF
      EF00EEEEEE00EDEDED00ECECEC00EBEBEB00EAEAEA00A24F2200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D0808000906060009060600090606000D8A8
      A800E2C0C000F0DEDE00FFFFFF00F0DEDE00D080800000689000283132000000
      0000000000000000000000000000D08080009060600000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000587BF002DB0D3004BBB
      CE0065D1DC0087FEFE0074F4FB0053DCEE003AC9E5001EA4CB00078ABB000383
      B5000082B8000A8CC1000B8FC200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8686000F8C69500F7C18F009238040000000000C868
      6000F2AE7500F0A96E0068A0C00094E4F60060C8F8004ABEDF0030A8D0000098
      C8001C1C1C000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C9762B00FCFCFC0086868600FBFBFB00FAFAFA00F9F9
      F900F8F8F800F7F7F700FFB25F00F5F5F500FF860500F3F3F300F1F1F100F0F0
      F000EFEFEF00EEEEEE00EDEDED00ECECEC00EBEBEB00A24F2200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000090606000EBD3D300E5C5C500E5C5C500D8A8A800E2C0
      C000F0DEDE00FFFFFF00F0DEDE00D080800030A8D0000090C800006890002831
      3200000000000000000000000000D0808000F0DEDE0090606000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000A88
      BB0029B2D7004CD1E9006AEAF40068EAF40053DEF20027A0C200027EB1000479
      AB001EA4CB00018AC2000B8FC200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8686000F8C69500F8C6950092380400C8686000F4B7
      8200F3B37B00F2AE7500F0A96E0068A0C00094E4F60060C8F8004ABEDF0030A8
      D0000098C8001C1C1C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C9762B00FCFCFC0086868600FCFCFC00FBFBFB00FAFA
      FA00F9F9F900F8F8F800F7F7F700FF860500F5F5F500F4F4F400F3F3F300F1F1
      F100F0F0F000EFEFEF00EEEEEE00EDEDED00ECECEC00A24F2200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D0808000EBD3D300F7EDED00F1E0E000EBD3D300E8CCCC00F0DE
      DE00FFFFFF00F0DEDE00D08080000090C80060C8F8000497C7000090C8001A42
      5100626665000000000000000000D0808000F0DEDE00F0DEDE00906060000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000487BF000F93C50019A3CF00128FBB000382B600047B
      AD000386BC000283BC0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8686000F8C69500F8C69500C8686000F7C18F00F6BC
      8800F4B78200F3B37B00F2AE7500C868600068A0C00094E4F60060C8F8004ABE
      DF0030A8D0000098C8001C1C1C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C9762B00FCFCFC0086868600FCFCFC00FCFCFC00FBFB
      FB00FAFAFA00F9F9F900F8F8F800F7F7F700F6F6F600F5F5F500F4F4F400F3F3
      F300F1F1F100F0F0F000EFEFEF00EEEEEE00EDEDED00A24F2200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D0808000FFFFFF00FDFAFA00FAF3F300F4E6E600EED9D900E8CC
      CC00F0DEDE00D080800000000000006890000090C800289AC5001D6074005F65
      6400D08080009060600090606000D0808000F0DEDE00FFFFFF00F0DEDE009060
      6000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000383B6000478
      AA00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8686000F8C69500F8C69500F8C69500F8C69500F7C1
      8F00F6BC8800F4B78200C8686000000000000000000068A0C00094E4F60060C8
      F8004ABEDF0030A8D0000098C8001C1C1C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C9762B00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFC
      FC00FBFBFB00FAFAFA00F9F9F900F8F8F800F7F7F700F6F6F600F5F5F500F4F4
      F400F3F3F300F1F1F100F0F0F000EFEFEF00EEEEEE00A24F2200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D0808000FFFFFF00FFFFFF0090606000FAF3F300F4E6E600EED9
      D900E8CCCC00D08080000000000000000000006890002B647C006C777900D080
      8000E7C9C900E0BABA00DAACAC00D8A8A800C8989800C8989800C8989800C898
      9800000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000036B
      9C00036B9C0000000000000000000000000000000000000000000383B7000478
      AA00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8686000F8C69500F8C69500F8C69500F8C69500F8C6
      9500F7C18F00C86860000000000000000000000000000000000068A0C00094E4
      F60060C8F8004ABEDF0030A8D000989898001C1C1C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C9762B00A24F2200A24F2200A24F2200A24F2200A24F
      2200A24F2200A24F2200A24F2200A24F2200A24F2200A24F2200A24F2200A24F
      2200A24F2200A24F2200A24F2200A24F2200A24F2200A24F2200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D0808000FFFFFF009060600000000000D0808000FAF3F300F4E6
      E600EED9D900D080800000000000000000000000000000689000D0808000F0DE
      DE00EBD3D300E4C3C300DEB6B600DAACAC00D080800000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000068B
      C000037EB10000000000000000000000000000000000000000000383B7000478
      AA00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8686000F8C69500F8C69500F8C69500F8C69500F8C6
      9500C868600000000000000000000000000000000000000000000000000068A0
      C00094E4F60060C8F80098989800B0B0B000989898001C1C1C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C9762B00D06F0100D06F0100D06F0100D06F0100D06F
      0100D06F0100D06F0100D06F0100D06F0100D06F0100D06F0100D06F0100D06F
      0100D06F0100D06F0100D06F0100D06F0100D06F0100A24F2200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D0808000D08080000000000000000000D0808000FFFFFF00FAF3
      F300F4E6E600D080800000000000000000000000000000000000D0808000F5E9
      E900F0DEDE00E9CDCD00E2C0C000DEB6B600C898980000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000028A0
      D0001F9ACE0000000000000000000000000000000000000000000383B7000478
      AA00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8686000F8C69500F8C69500F8C69500F8C69500C868
      6000000000000000000000000000000000000000000000000000000000000000
      000068A0C00098989800D0D0D000BBC0BF000130B1000018C60000009A000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CE630000ED973300ED973300ED973300ED973300ED97
      3300ED973300ED973300ED973300ED973300ED973300ED973300ED973300F6CA
      9A00F3AD6100F6CA9A00F3AD6100306DF9007F748800DA7B0D00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D0808000FFFFFF00FFFFFF00FFFF
      FF00D08080000000000000000000000000000000000000000000D0808000FAF4
      F400F5E9E900EDD7D700E7C9C900C89898000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002592
      BA0066DEFC00117AA900000000000000000000000000000000000384B7000478
      AB00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8686000F8C69500F8C69500F8C69500C86860000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099A8AC00E8E8E8001464C6000031FF001029D6001029D6000000
      9A00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DA7B0D00DA7B0D00DA7B0D00DA7B0D00DA7B
      0D00DA7B0D00DA7B0D00DA7B0D00DA7B0D00DA7B0D00DA7B0D00DA7B0D00DA7B
      0D00DA7B0D00DA7B0D00DA7B0D00DA7B0D00DA7B0D0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D0808000D0808000D0808000D080
      800000000000C8989800D0808000906060009060600090606000F0DEDE00FFFF
      FF00FAF4F400F3E5E500C8989800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000055CCE00075EDF8002898BC00096C9C00076897000876A700038FC4000470
      A100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8686000F8C69500F8C69500C8686000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099A8AC000018C600106BFF000031FF001029D6000000
      9A00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8989800E2C0C000F0DEDE00FFFFFF00FFFFFF00FFFF
      FF00C8989800C898980000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000096D990049BDD30079F3F8006CE6F60059D1F20040C1F0000F88BB000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8686000F8C69500C868600000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000018C6006D8AFD00106BFF0000009A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C8989800C8989800C8989800C8989800C898
      9800000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000F76A0002593B5002A99BB001780AC00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8686000C86860000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007823DF0000009A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A7A9A90096989900969899009698
      9900969899009698990096989900969899009698990096989900969899009698
      9900969899009698990096989900969899009698990096989900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000006000B00000705000004080000020A0000000A00000109000700
      090010000900090008000B000600020006000002080000010800000009000400
      09000700090007000600060005000608000004030000000A0000001100000000
      0300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000636B7300636B7300F2F2F200F0F0
      F000EFEFEF00EDEDED00ECECEC00EAEAEA00E8E8E800E7E7E700E4E4E400E0E0
      E000DDDDDD00DADADA00D6D6D600D3D3D300D2D2D20096989900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A6A8A9009B9B9B009B9B9B009B9B9B009B9B9B009B9B9B009B9B9B009B9B
      9B009B9B9B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000615000046AE71000AD267000ACE640003CE670005CC6A0008CF63000AD0
      600005D35D0000D65F0000D4600003D25F000AD25C0001D35F0001D260000CCF
      61000AD0600000D55D0000D45C0000CE6B0000C9720003CB6D0010B56A000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000636B7300218CEF002173B500636B7300F2F2
      F200F0F0F000EFEFEF00EDEDED00ECECEC00EBEBEB00E9E9E900E8E8E800E5E5
      E500E2E2E200DFDFDF00DBDBDB00D8D8D800D5D5D50096989900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A6A8A900FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF009B9B9B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000902000043B482000ECD5E0010CD5A0000D25F0000C5580000CA690000C8
      5D0013DA68000CCB620000C6600000CB620004D2620000D2640000D3610006D7
      690000CE610000C75C0010C45F001AD45C0000CD550007E3670003CB6D000009
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C
      0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C
      0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000031A5FF005ABDFF00218CEF002173B500636B
      7300F3F3F300F1F1F100F0F0F000EEEEEE00ECECEC00EBEBEB00E9E9E900E8E8
      E800E5E5E500E2E2E200DFDFDF00DBDBDB00D8D8D80096989900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006B6B
      6300A6A8A900FFFFFF00E2E2E200AFAFAF00AFAFAF00AFAFAF00AFAFAF00FFFF
      FF009B9B9B006B6B630000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000100003FB2860003D05D0009D45F0000D26800002B000002CB700000D1
      650000DA640010C261000CC66C0000C6650001D0670000C9610009DA6C0012D1
      620011D2640000D0710000C26B001FD66E0000CF590000CC540000C972000302
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD4C0000FFF6ED00FFF3E600FFEFDE00FFEBD500FEE7CE00811E0000FEE0
      BD00FEDCB600FFD8AF00FFD5A700FED1A100811E0000FECC9800FECB9600FECB
      9600FECB9600FECB9600811E0000FECB9600FECB9600FECB9600FECB9600BD4C
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000031A5FF005ABDFF00218CEF002173
      B500636B7300F3F3F300F1F1F100F0F0F000EEEEEE00ECECEC00EBEBEB00E9E9
      E900E8E8E800E5E5E500E2E2E200DFDFDF00DBDBDB0096989900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000004A4A4A004A4A4A006B6B
      6300A6A8A900FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF009B9B9B006B6B63004A4A4A004A4A4A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000005000061B07F0000D45D0013C2610000180000000000000900000071A8
      8F005BB08400FFFCF800FFF5FF00FFFEFF00FBFFF900FFFDF400E5FFE5003FB7
      800030A976000400080000001400000F000018CF67001AD45C0000CE6B000507
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD4C0000FFFBF400FFF7EE00FEF4E700FFEFE000FEEBD700811E0000FEE5
      C600FEE0BF00FEDCB700FED8B000FED5A900811E0000FECF9E00FECC9900FECB
      9600FECB9600FECB9600811E0000FECB9600FECB9600FECB9600FECB9600BD4C
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A7A9A90031A5FF005ABDFF00218C
      EF002173B500636B7300F3F3F300F1F1F100F0F0F000EEEEEE00ECECEC00EBEB
      EB00E9E9E900E8E8E800E5E5E500E2E2E200DFDFDF0096989900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000004A4A4A004A4A4A00636363006F7070006B6B
      6300A6A8A900FFFFFF00E2E2E200AFAFAF00AFAFAF00AFAFAF00AFAFAF00FFFF
      FF009B9B9B006B6B63004A4A4A00636363004A4A4A004A4A4A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000400000051B47C0013C8650001CB5A0000D85C00002F0000050302000002
      0000FAFFF200C5FFF300CDFFFB0040BA7A0059B97300DAFFF200FFFBFF00FFFA
      FF0004070000001600000019000000D4680000D1650017C86B0011CF60000004
      0200000000000000000000000000000000000000000000000000000000000000
      0000BD4C0000FFFCFA00FFFAF400FFF7EF00FFF3E700FFF0E000811E0000FFE9
      D000FEE5C900FEE1C000FFDDB700FFD9B100811E0000FED1A400FECF9E00FECC
      9900FECB9600FECB9600811E0000FECB9600FECB9600FECB9600FECB9600BD4C
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A7A9A900FDFDFD0031A5FF005ABD
      FF00218CEF00636B7300F5E1D100D9BBB000C4A09A00C7A29D00CFAAA400DDBB
      AD00EBEBEB00E9E9E900E8E8E800E5E5E500E2E2E20096989900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000004A4A4A006363630072747400909595007B7E7E006B6B
      6300A6A8A900FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF009B9B9B006B6B63004A4A4A007E81810072747400636363004A4A4A000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000900000043B77C0000D0620015CA5E000ED7680011C77900FCFAFF000400
      00006B9993001A000000F4FEED00ECFDFF00FFFAFF00FFFFF4000018000030AD
      850000000600DAFFF10000CF620000D0660000D5680000C765000ECF60000002
      0200000000000000000000000000000000000000000000000000000000000000
      0000BD4C0000FFFFFF00FFFEFB00FFFBF600FFF7EF00FFF4E900811E0000FFED
      D900FFE9D100FEE5CA00FEE1C100FEDDBA00811E0000FED5AB00FED1A400FED0
      9F00FECE9900FECB9700811E0000FECB9600FECB9600FECB9600FECB9600BD4C
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A7A9A900FEFEFE00FDFDFD0031A5
      FF00AFAFAF004A423900C29E9800D8CBA000F3E4B100F6E7B400D8CBA000DEC9
      B600C5A09900EBCCB600E9E9E900E8E8E800E5E5E50096989900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000636363007274740081858500B0B0B000ADB5B5007B7E7E006B6B
      63009C9C9C00D2D2D200D2D2D200D2D2D200D2D2D200D2D2D200D2D2D200D2D2
      D2009C9C9C006B6B63004A4A4A00818585007B7E7E0072747400636363004A4A
      4A00000000000000000000000000000000000000000000000000000000000000
      0000090000003FB77D0001D15B0000CC660021BF7C00FFFFF500DDFFF0000000
      040000000300002F000000CB6E0010C758000CC764000BC96A00001300001500
      070000020A00FFFBFE00F2F3FF002AAF690000CC5E0007D3680000D35F000100
      0400000000000000000000000000000000000000000000000000000000000000
      0000BD4C0000811E0000811E0000811E0000811E0000811E0000811E0000811E
      0000811E0000811E0000811E0000811E0000811E0000811E0000811E0000811E
      0000811E0000811E0000811E0000811E0000811E0000811E0000811E0000BD4C
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A7A9A900FFFFFF00FEFEFE00FDFD
      FD00E6D7D100BA938E00EFDFAE00F5E5B300F9E9B600FCECB800FDEDB900FDED
      B800D8CBA000C19C9600EBEBEB00E9E9E900E8E8E80096989900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000073737300ADB5B500D2D2D200ADB5B500ADB5B500696A6A004A4A
      4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A
      4A004A4A4A004A4A4A0063636300848888007E818100757777006C6D6D004A4A
      4A00000000000000000000000000000000000000000000000000000000000000
      00000003000056B27F0000D65A0008CF7400DCFFF000EDFFFC0000000E000000
      14000302040005D1600000D35B0000D4570011C96B0000D0560026C067000004
      00001400140015000000F1F6FF006EAD850000D566001ACF660000D45F000001
      0500000000000000000000000000000000000000000000000000000000000000
      0000BD4C0000FFFFFF00FFFFFF00FFFFFF00FFFEFC00FFFBF700811E0000FFF6
      EB00FFF2E300FEEFDC00FFEAD300FFE7CB00811E0000FFDEBC00FFDAB500FED7
      AD00FFD4A600FED0A000811E0000FECC9700FECB9600FECB9600FECB9600BD4C
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A7A9A900FFFFFF00FFFFFF00FEFE
      FE00D7B7B500E8DAAA00F4E4B200FBE9B700FFEEBA00FFF0BC00FFF1BC00FFF0
      BB00FEEEB900D8CBA000CAA59A00ECECEC00EAEAEA0096989900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000073737300D2D2D200B4B4B400ADB5B500ADB5B500ADB5B500ADB5
      B500ADB5B500ADB5B500ADB5B500ADB5B500ADB5B500AAB2B200A4ABAB00A0A7
      A7009AA0A000949999009499990008B51800108C3100787A7A006F7070004A4A
      4A00000000000000000000000000000000000000000000000000000000000000
      0000000500005FAE820000D15D001CBF7400FDFFFB00F8FEFD00D8FFE3000013
      00000E06000038AF8800DDFFE600FAF9F500F3FFFF007DFFC90008D76F001308
      0000000300009AFFEA00FFFCEE00EDFBEF0000D066001ED56D0000D35F000001
      0700000000000000000000000000000000000000000000000000000000000000
      0000BD4C0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFE00811E0000FFF8
      F200FEF6ED00FEF3E500FFEFDD00FFEAD500811E0000FEE3C400FFE0BD00FFDC
      B500FFD7AD00FED4A700811E0000FECE9C00FECC9800FECB9600FECB9600BD4C
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A7A9A900FFFFFF00FFFFFF00FFFF
      FF00CEAAA300E9DAAB00F5E5B400FDECB900FFEFBC00FFF1BD00FFF3BE00FFF3
      BE00FFF0BB00FAEBB600C09C9400EDEDED00ECECEC0096989900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000073737300D2D2D200ADB5B500ADB5B500ADB5B50094999900847B
      7B00846B7300846B6B00846B6B0084636B0084636B0084636B0084636B008463
      6B0084636B00947B84009499990039C66B0008B518007B7E7E00727474004A4A
      4A00000000000000000000000000000000000000000000000000000000000000
      00000005000053B0830016CB5F0050AC7700FAFEFF00FFFFFE0000C64B0017D2
      5E0009CA7300CFFFE60064A87300A3FFEA0097FFDA00EDFFEE0000BC73002EC1
      6F0000D57400C6FFEA00FFFFED00FBFEFF0003CA680001D6690005D160000000
      0800000000000000000000000000000000000000000000000000000000000000
      0000BD4C0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00811E0000FFFC
      F800FFFAF400FFF7EE00FFF3E600FFEFDE00811E0000FEE7CE00FFE3C500FEDE
      BD00FEDAB600FED8AF00811E0000FED1A100FECF9D00FECC9800FECC9800BD4C
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A7A9A900FFFFFF00FFFFFF00FFFF
      FF00CCA79D00E6D7A900F4E4B300FBEAB800FDECBA00FFEEBB00FFF0BD00FFF2
      BE00FFEFBB00F9E9B500BE9D9400EFEFEF00EDEDED0096989900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000073737300D2D2D200ADB5B500949999008C6B73009C6B7300A584
      8400A57B7B00BD8C9400BD8C9400BD8C9400BD8C9400BD8C9400C4969E00C496
      9E00A57B7B00A5848400A56B7B007B636300848888007B7E7E00727474004A4A
      4A00000000000000000000000000000000000000000000000000000000000000
      0000000000004CB380000ACE5D00FAFFF100F6FAFB0024B66E0000CE5E0005D1
      680000C16F0050B77A00FFFEF500F4FFFC00F7FEF90039B87B00FFF7FF0005CC
      640000CB6000FFF8FF00C1FFD100FBFFE50000D15C000BCF650000C976001602
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD4C0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00811E0000FFFF
      FE00FFFCFA00FFFAF400FFF7EE00FEF3E600811E0000FEEBD700FFE7CF00FEE5
      C700FFE1BF00FFDCB700811E0000FFD5AA00FED1A300FED09E00FED09E00BD4C
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A7A9A900FFFFFF00FFFFFF00FFFF
      FF00CEAA9F00E0D2A500F0E0B100F8E8B800FBEDC300FCECBA00FEEDBB00FFEF
      BC00FDECB900F5E5B300C09B9000F0F0F000EFEFEF0096989900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000073737300D2D2D200ADB5B5008C7B8400844A5200A5848400A57B
      7B00A57B7B00A57B7B00A57B7B00A57B7B00A57B7B00A57B7B00AA838200AA83
      8200AA838200AA838200A58484008C4A5A00AD7B9C007B7E7E00727474004A4A
      4A00000000000000000000000000000000000000000000000000000000000000
      0000020100004AB2830003D15A00FFFFFC00CCFFF000629A7D0000CC600007E2
      6A0000D26C00BDFFE70000CC720000C8620003C26700F4FFF100F6FDFF0000CF
      630018C65400E4F6FF00D0FFFF00CEFFF00000D15D0000C3570000C972000C04
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD4C0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00811E0000FFFF
      FF00FFFFFE00FFFEFA00FFFAF600FFF7EF00811E0000FEF0E000FEEDD900FFE9
      D000FEE5C700FEE0C000811E0000FED8B000FED5A900FED1A300FED1A300BD4C
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A7A9A900FFFFFF00FFFFFF00FFFF
      FF00D7B9B200D8CBA000EADBAD00F6E9C000FDF7E800FAE9B900FBEBB900FAEA
      B800F5E5B400D8CBA000C29C9300F2F2F200F0F0F00096989900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084737300D2D2D200BEBEBE00947B84007B4A5200844A52005A31
      31005252520052525200525252004A4A4A004A4A4A004A4A4A004A4A4A004A4A
      4A004A4A4A005A313100844A52007B4A52009C848C007B7E7E00727474006363
      6300000000000000000000000000000000000000000000000000000000000000
      0000040200004AB1840000D35A00FFF8FE0044BC8200FFEBF80009CA630000D1
      550001CE6700FFF8F70000BA5D0003CD520019CD6400FFF0FF00CBFFF00007CC
      6A0003D95C00DEFFFF00F0EFFF00A4FFED0000C8590002CD5E0003CC6B000105
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD4C0000811E0000811E0000811E0000811E0000811E0000811E0000811E
      0000811E0000811E0000811E0000811E0000811E0000811E0000811E0000811E
      0000811E0000811E0000811E0000811E0000811E0000811E0000811E0000BD4C
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A7A9A900FFFFFF00FFFFFF00FFFF
      FF00E7D5D500D6BCAD00E3D6AC00F1E5C200F3E5BA00F4E4B300F3E3B200EFE0
      B000D8CBA000D0A99700E0C4B800F4F4F400E6E6E60096989900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008C8C8C00ADB5B500D2D2D200AD949C00421821004A4A4A004A29
      2900736363008575750085757500857575008575750085757500857575008575
      75008575750052313100393939004A212100A59C9C007B7E7E00727474006C6D
      6D00000000000000000000000000000000000000000000000000000000000000
      0000000300004FB2800000CE62007AA48B004FAD8400FFFAFF0018C766000ED1
      6B0002CE7500F8F9FF00FFFFED0093FFE30091FFEA00F7F1FF001ABB6D00001B
      000007C66300F2FFFC007F9D7A00C6FFF8000AD266000DDB6A0001CF66000005
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD4C0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00811E0000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFEFB00811E0000FFF7F000FFF4EA00FFF0
      E300FEEEDA00FEEAD100811E0000FEE2C200FFDDBA00FFD9B200FFD9B200BD4C
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A7A9A900FFFFFF00FFFFFF00FFFF
      FF00FFF8F000D4B5B200D7C1BA00E3D7B100E5D7AA00E6D7A900E5D6A800E1D3
      A500D7AD9700C9A39D00F7F7F700E6E6E600CFCFCF0096989900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084737300D2D2D200BDA5AD00422121004A4A4A00A6A8
      A900F1F1F100F3F3F300F4F4F400F5F5F500F7F7F700F9F9F900FBFBFB00FDFD
      FD00FFFFFF009B9B9B006F7070004A212100B5A5A5007B7E7E00636363000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000200005EB3790000C5730010D15800E2FFFE00B5FFE900000B00000C00
      07001B03030021C66F0032BA7A0095FFEA00AAFFDD001AD36D004CBB85000000
      12001E070000FFF8FF0049BB8500FBFFE90000CF650006D0650000D360000800
      0200000000000000000000000000000000000000000000000000000000000000
      0000BD4C0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00811E0000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00811E0000FFFCF700FFF8F000FFF6
      EA00FFF2E300FFEEDC00811E0000FEE6CC00FEE2C400FEDEBC00FEDEBC00BD4C
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A7A9A900FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFF8F000D4BABA00C8A59900CDAFA000CFB0A200D4B3A300D0AA
      9D00D9BBB600E6E6E600E6E6E600CFCFCF00B1B1B10096989900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000008C8C8C00ADB5B500B5B5B5009C94940084737300A6A8
      A900F6F6F600F1F1F100F2F2F200F3F3F300F5F5F500F6F6F600F8F8F800FAFA
      FA00FCFCFC009B9B9B007B636B009C949400ADB5B500ADB5B5006C6D6D000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000300005EB27C0000C96F0002CC50009DFFDE0069AC8C00FDFBF3000A00
      1200000007000BDA600000D96D0006CF6D0006CA600000D85D00000300000000
      0F00030D0000E3FFEF00D0FFF70052B37A0000CD62000AD46F000AD060000500
      0700000000000000000000000000000000000000000000000000000000000000
      0000BD4C0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00811E0000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00811E0000FFFFFE00FFFBF800FFF8
      F300FFF6EB00FFF2E500811E0000FEEBD400FEE7CC00FFE2C400FFE2C400BD4C
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A7A9A900FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFF8F000E6D9D900EDDDDB00F1E1DE00FFF8
      F000A9AAAA009698990096989900969899009698990096989900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008C8C8C00847373008473730084737300A6A8
      A900FAFAFA00F6F6F600F1F1F100F2F2F200F3F3F300F5F5F500F6F6F600F8F8
      F800EBEBEB009B9B9B0084737300847373008473730084737300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000300005CB0800000CD690016D0600002C86A00F8FFFC00FFFDFA000001
      080000000E000033000006CD610011D0670000CF68002BC46700080110000802
      0000FFFFF30057B87500F2FEFE0001BF6E0000CD630000CD6A0018CD61000000
      0900000000000000000000000000000000000000000000000000000000000000
      0000BD4C0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00811E0000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00811E0000FFFFFF00FFFEFE00FFFB
      F800FFFAF300FFF7ED00811E0000FFEFDE00FFEBD700FFE7CE00FFE7CE00BD4C
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A7A9A900FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00A9AAAA00FFFFFF00FFFFFF00FFFFFF00DCDCDC0096989900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A6A8
      A900FFFFFF00FAFAFA00F6F6F600F1F1F100F2F2F200EAEAEA00E2E2E200E3E3
      E300DFDFDF009B9B9B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000090004004EB37F0000D35E0000CB5A0000CD5E0010C671000D040000000D
      0400DFFFFF0023011200FFF8FF00F2F6FF00F4F8FF00000C0500FFF7FF000000
      0A0000080B0096FFD0000DCB5E0004C3620000C5600000D3690008CD67000002
      0400000000000000000000000000000000000000000000000000000000000000
      0000BD4C0000D9640100D9640100D9640100D9640100D9640100D9640100D964
      0100D9640100D9640100D9640100D9640100D9640100D9640100D9640100D964
      0100D9640100D9640100D9640100D9640100D9640100D9640100D9640100BD4C
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A7A9A900FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00A9AAAA00FFFFFF00FFFFFF00DCDCDC009698990000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A6A8
      A900FFFFFF00FDFDFD00F6F6F600F6F6F600F1F1F100A6A8A9009B9B9B009B9B
      9B009B9B9B009B9B9B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000900040061B07F0000D1620000D56A0024C2680000160000000006001200
      0000F3FCE80030B47F005CAF7B00CDFFDD009AFFD80054BD74004EB88300FFFC
      F50009000700000006000C0000002BBC6D0000C4600000C1580017CB66000003
      0200000000000000000000000000000000000000000000000000000000000000
      0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C
      0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C
      0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A7A9A900FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00A9AAAA00FFFFFF00DCDCDC00969899000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A6A8
      A900FFFFFF00FEFEFE00FAFAFA00F6F6F600EFEFEF00A6A8A900FFFFFF00FFFF
      FF00E6E6E6009B9B9B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000003000041B87B0000CC660024C06C00001A0000150200000009000026B9
      7B0012C576007F9F8700F2FAF900FFFBFF00FFFFFE00D1FFF90055C4800000D7
      620000CC5E0000060000040007002DC2680000CF610000D360001ACC61000002
      0100000000000000000000000000000000000000000000000000000000000000
      0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C
      0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C
      0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C0000BD4C
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A7A9A900FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00A9AAAA00DCDCDC0096989900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A6A8
      A900FFFFFF00FFFFFF00FFFFFF00FAFAFA00F6F6F600A6A8A900FFFFFF00E6E6
      E6009B9B9B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000005000038AE6D0000CD600000D971001AC87000001A000010B76E0000CC
      680000CA500000D3600000CD670010CB650002D15E0020C8630007CD630000CF
      690000C76F003DC07D0024BC73000BCE600000D45F0000D55E000BCE6000030B
      0A00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A7A9A900A7A9A900A7A9A900A7A9
      A900A7A9A900A7A9A900A7A9A900A7A9A900A7A9A900A7A9A900A7A9A900A7A9
      A900A7A9A900A7A9A90000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A6A8
      A900FFFFFF00FFFFFF00FFFFFF00FFFFFF00FAFAFA00A6A8A900E6E6E6009B9B
      9B00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000040B006B9377003EA8730050BD850039B66C0042BC70004EBC810050AF
      7C0048AE7E0043B47B0041B47B0035B481002FB287004CB37A005FB077004FB3
      79004BB47B004DB16F0057BA700048B27D0051AD820054AE800049BA80000000
      0E00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A6A8
      A9009B9B9B009B9B9B009B9B9B009B9B9B009B9B9B009B9B9B009B9B9B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000007001009000000020100140311000500010000020300020400000900
      010000000800120100000406000000060000150000000B000000070000000000
      0400000D12000000080000000400020100000400010013000000001700000700
      1500000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000524339004E3F36004E4036004E4036004E4037004E4037004E3F
      37004E4037004E3F37004E3F37004E3F37004E3F36004E3E36004D3E37004D3E
      35004D3F35004E3F35004E3E35004E3E36004E3E36004E3F36004F3F36005547
      3A00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005B4B3D0084774B0090844E008B7D53008A7C55008D804D008E8149008D80
      4C008D804D008D804D008E8149008E8148008B7E50008B7E51008C804E008B7C
      5200897B5700887A58008B7F50008C7F4E008B7D53008C7D54008F8153006E60
      4200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000594A3C00E0D68400FFFFAA00FFFFA500FFFFA300FFFFA000FFFF9A00FFFF
      9B00FFFF9A00FFFF9B00FFFF9B00FFFFA300FFFFA500FFFFA400FFFFA200FFFF
      A300FFFFA500FFFFA500FFFFA200FFFF9D00FFFFA200FFFFA700FFFFA2009487
      50004F4038000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000584A3C00DDD39200EAE69A00DFD38B00D9CD8600D9CE8900FFFCAB00FFFC
      AB00FFFBAB00FFFDAD00FFFDAB00E4DA8800E3DA8700E2D99100DDD59900DED5
      9900DFD69700DFD69200DCCE8700D1D6A300DDD38800E0D58A00FAF3A0009789
      5900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000033669900194C7F00254A6F00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005C4D3C00DAD1990078684B004E3C2F008695A500767A7D00F6ECAF00FFFF
      C300FFFABE00FFFFC200FCF6BB0064543C005A4B3800604F3F005C4D40005A4A
      3F0058463900513D2B00616D7800ACDCFF006A747C004A322000B8AC8300A095
      6700000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000033669900194C
      7F003A6DA100467CB400255485002F5072000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005C4D3B00DBD5A6006F5D420073797E00CDEFFF00C4E3FF00B5C4BB00F8F5
      C300FFFFD600FFFFD500FEF8D100605040000000000000000000000000000000
      00004D3B29006C7C8900BBE4FF009DC4DD0099C4EA004B484500B7A98300A196
      7000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033669900194C7F0032689E005085
      BB006195C9005E90C300305F8F00255587002E50720000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00001DA3D600107FAD00107FAD00107FAD00107FAD00107FAD00107FAD00107F
      AD00107FAD00107FAD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A56B4A00A56B4A00A56B4A00A56B4A00A56B
      4A00A56B4A00A56B4A00A56B4A00A56B4A00A56B4A00A56B4A00A56B4A00A56B
      4A00A56B4A00A56B4A00A56B4A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005B4D3B00D7CFAD008A877D00B4CBE10060574B008D939500D7F0FF00B4C1
      BB00F0EFCC00FFFFEE00FFFDE000645641000000000000000000574431004D3F
      2F00738DA000EAFFFF00AEC5D700382714008E9EAC009EBFDD00B1AD9600A094
      7500000000000000000000000000000000000000000000000000000000000000
      0000000000000000000033669900194C7F002F659C00568ABE005E92C7006699
      CC006699CC005E90C300306091003264970026568700274B6E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BD5A1000AD4A0000AD4A0000AD4A0000AD4A
      000044B1E400FCFCFC00FCFCFC00FCFCFC00FCFCFC00F5F5F500EEEEEE00E7E7
      E700D2D2D200107FAD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CE845A00FFEFD600F7DEC600E7C6AD00E7BDA500E7BD
      A500DEB59C00DEAD9400DEAD9400D6A58C00D6A58C00D6A58400D69C7B00D69C
      7B00CE947300CE8C6300C67B4A00A56B4A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005B4B3B00D2CAB300B4C1C100DFF1FF00564B3D00868A8A00FAFFFF00E5F3
      FF00AEC2C600E8EAD900FFFFF30067583E00000000005E4E3900544B3C00859F
      B100F1FFFF00FFFFFF00CFE0EE0065748000B7CFDF00A1BDD500B1B0A300A094
      7800000000000000000000000000000000000000000000000000000000000000
      000000000000194C7F00396CA0005287BD006195C90071A4D7008EC1F40090C3
      F6006FA2D5005E90C3003060910033669900326597002959890026496C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CE6B00000000000000000000000000000000
      000044B1E400FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00F9F9F900F2F2
      F200EAEAEA00107FAD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D6947300FFEFD600F7DEC600F7D6A500BD7B5A00E7BD
      A500F7D6A500BD7B5A00DEAD9400F7D6A500BD7B5A00D6A58400D69C7B0029C6
      5A0000630800CE8C6300C6845A00A56B4A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005B4C3B00DAD4C100796F53009FA9B200ECF9FF00F6FEFF00FDFFFF00FEFF
      FF00F4FAFF00BECDD600D8DBD100675639004C372500666A6200B1D4EA00FEFF
      FF00FFFFFF00FAFEFF00FFFFFF00F9FFFF0097B2C50045392D00BBB09C00A29A
      7A00000000000000000000000000000000000000000000000000000000000000
      0000194C7F005087BF006093CC006295CC0080B3E6006FA2D5003366990071A4
      D70070A3D6005E90C30034649400386B9E003366990032659800285788002B4D
      7000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CE6B00000000000000000000000000000000
      000044B1E40044B1E40044B1E40044B1E40044B1E40044B1E40044B1E40044B1
      E40044B1E4001DA3D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D6947B00FFEFD600F7DEC600FFF7E700F7D6A500E7BD
      A500FFF7E700F7D6A500DEAD9400FFF7E700F7D6A500D6A58400D69C7B009CE7
      B50029C65A00CE8C6300C6845A00A56B4A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005B4C3C00DCD8CB007E6E4A004C3D2D009BA1A300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C8D5DA00584E3E00564C4000B8CAD700FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00F6FAFD00C3E3F500767A720048301900BBB5A600A59C
      7900000000000000000000000000000000000000000000000000000000000000
      0000194C7F006699CC004073B7004477B30036699C002E6194001F5285005D90
      C3006C9FD2005E90C300436C97007FA9D200396C9F0033669900326497002455
      8600315272000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CE6B00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D6947B00FFEFD600F7DEC600EFD6BD00E7C6AD00E7BD
      A500E7BD9C00DEB59C00DEAD9400DEAD8C00D6A58C00D6A58400D69C7B00D69C
      7B00CE947300CE8C6300C6845A00A56B4A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005B4C3C00DCD8CD0081744E000000000053422F0085868300F0F4FB00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CAD2D800A3B5C300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00E8F2FA00B7D7E800F9FFFF00968D820045311C00BCB6A700A69F
      7600000000000000000000000000000000000000000000000000000000000000
      0000194C7F006699CC002F62950024578A00194C7F001F528500386B9E005487
      BA005C92C9007DAFE2006B8BAD0069758E00436DA00033669900336699003264
      9700245486002E4F710000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CE6B00000000000000000000000000000000
      00001DA3D600107FAD00107FAD00107FAD00107FAD00107FAD00107FAD00107F
      AD00107FAD00107FAD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D6947300FFEFD600F7DEC600F7D6A500BD7B5A00E7BD
      A500F7D6A500BD7B5A00DEAD9400F7D6A500BD7B5A00D6A58400D69C7B00FF9C
      0000BD5A0000CE8C6300C6845A00A56B4A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005C4C3C00DCD9CC0082764D0000000000000000005A4B3600C3CFCE00E2EB
      EF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E3EBF300BED3E400FCFEFF00FFFF
      FF00DCE9F300B5D1DF00F9FDFF00FFFFFF008E8B7F0045311D00BCB5A600A49C
      7A00000000000000000000000000000000000000000000000000000000000000
      0000194C7F006699CC002F6295000A3D70004376A9005083B6005F94CA0076AB
      E10086BAEF00C7D7E600CDD7E0008A8A91007FA9D2003D70A300336699003366
      990032649700265687002A4B6D00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BD5A1000AD4A0000AD4A0000AD4A0000AD4A
      000044B1E400FCFCFC00FCFCFC00FCFCFC00FCFCFC00F5F5F500EEEEEE00E7E7
      E700D2D2D200107FAD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CE947B00FFEFD600F7DEC600FFF7E700F7D6A500E7BD
      A500FFF7E700F7D6A500DEAD9400FFF7E700F7D6A500D6A58400D69C7B00FFCE
      7B00FF9C0000CE8C6300C6845A00A56B4A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005B4C3C00DCD8C70082764D00000000000000000067574300FFFFF300D2DE
      DC00D3E0EA00FFFFFF00FBFFFF00FBFFFF00FFFFFF00DAE6EF00BBCEE000C6DC
      E900AED8E300EBFFFC00F1FFFF00F2FFFF00898B800046311D00BBB4A200A19A
      7A00000000000000000000000000000000000000000000000000000000000000
      0000194C7F006699CC006093C6005A8DC100669ACD00689ED60097C7F700BED4
      EA00D8DDE200FAFAFA00FCFCFC009A9A9A001B1F23007FA9D200386B9E003366
      99003366990032659800285788002A4C6D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CE6B00000000000000000000000000000000
      000044B1E400FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00F9F9F900F2F2
      F200EAEAEA00107FAD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D6947300FFEFD600F7DEC600EFD6BD00E7C6AD00E7BD
      A500E7BD9C00DEB59C00DEAD9400DEAD8C00D6A58C00D6A58400D69C7B00D69C
      7B00CE947300CE8C6300C6845A00A56B4A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005445
      350056453200DAD5BC0081754B00000000000000000066574000FFFFFB00FFFE
      EC0096B1C300B8CFE400F6FDFF00F2FBFF00F1F9FF00F8FFFF00E0EBF50093B2
      C100C8F4EF00E0FFFF00DEFFFF00E3FFFF00868F830048311F00BBB3A0009D92
      7E004A3A27000000000000000000000000000000000000000000000000000000
      0000194C7F006497CB006A9DD100669DD500A1CAF300B8D5F200E5E5E500FDFD
      FD00FFFFFF00FFFFFF00FFFFFF00AAAAAA001B1B1B00656768007FA9D2004174
      A700336699003366990032659800265687003050710000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CE6B00000000000000000000000000000000
      000044B1E40044B1E40044B1E40044B1E40044B1E40044B1E40044B1E40044B1
      E40044B1E4001DA3D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D6947300FFEFD600F7DEC600F7D6A500BD7B5A00E7BD
      A500F7D6A500BD7B5A00DEAD9400F7D6A500BD7B5A00D6A58400D69C7B00FF9C
      0000BD5A0000CE8C6300C6845A00A56B4A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006050
      41008C848400DCD6B4007F734A00000000000000000063513C00CEC8AB00C7D6
      CE00D7EFFC00B5D2EB00A8C5DF00E6F5FF00E7F7FF00E5F3FF00ECF9FF00DEED
      F900A8C7D300ABC7BE00B5CFC600BAD9D4007D7D6F004B372400B9B19500BAB2
      A4007A706C005344330000000000000000000000000000000000000000000000
      0000194C7F0076ABE0006B94BE009FBCDA00E5E8EB00FDFDFD00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00AAAAAA001B1B1B00706F6F001D242A007FA9
      D2004578AB003366990033669900326497002354850033527200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CE6B00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D6947300FFEFD600F7DEC600FFF7E700F7D6A500E7BD
      A500FFF7E700F7D6A500DEAD9400FFF7E700F7D6A500D6A58400D69C7B00FFCE
      7B00FF9C0000CE8C6300C6845A00A56B4A000000000000000000000000000000
      000000000000000000000000000000000000000000000000000053412D009791
      9C00F9FCFF00FFFFD30078694600000000005745330057453400879CAB00D2EE
      FF00DBF2FF00DEF3FF00B5D7F200A2C7E600D8F1FF00D7F0FF00D4ECFF00DAF1
      FF00D6EFFF008C9BA700544231005A4531005C4C3A004D3E2C00BAB18600FFFF
      FF00DADEFF00685D570055453200000000000000000000000000000000000000
      0000194C7F005D93C9002F475F00B5B5B500FDFDFD00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00DBDBDB00787878001E1E1E0086838300191919006169
      71007FA9D2004174A700336699003366990032649600235485002D4E6F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CE6B00000000000000000000000000000000
      00001DA3D600107FAD00107FAD00107FAD00107FAD00107FAD00107FAD00107F
      AD00107FAD00107FAD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D6947300FFEFD600F7DEC600EFD6BD00E7C6AD00E7BD
      A500E7BD9C00DEB59C00DEAD9400DEAD8C00D6A58C00D6A58400D69C7B00D69C
      7B00CE947300CE8C6300C6845A00A56B4A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000685D5700D6D8
      F900857C7A00DBD39B0080724800513F2F00574531008BAAC100CFF4FF00CAE9
      FF00C5E5FF00C9E9FF00D2F8FF007B8E9C008093A200CEF3FF00C8E9FF00C4E5
      FF00C9E8FF00CFF1FF0090A8BC0055422E00544230004C3D2F00C0B68600B0AA
      8C00AAA6B800B6B4C80054453300000000000000000000000000000000000000
      000000000000194C7F002E486300BFBFBF00FFFFFF00E2F5EB0066CC990066CC
      9900FFFFFF007676760088888800C3C7C8004244450086838300202020005C5A
      5A00484F56007FA9D200386B9E00336699003366990032649700093B6E000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BD5A1000AD4A0000AD4A0000AD4A0000AD4A
      000044B1E400FCFCFC00FCFCFC00FCFCFC00FCFCFC00F5F5F500EEEEEE00E7E7
      E700D2D2D200107FAD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D6947300FFEFD600F7DEC600F7D6A500BD7B5A00E7BD
      A500F7D6A500BD7B5A00DEAD9400F7D6A500BD7B5A00D6A58400D69C7B001063
      D600105AA500CE8C6300C6845A00A56B4A000000000000000000000000000000
      0000000000000000000000000000000000000000000059483500A5A5BC00BBBC
      D40049382400716143006E5C3E0057463700819EB400BAEEFF00BCE7FF00B8E0
      FF00BBE4FF00BBEBFF0087A3B9005B483400574735008CA2B400C5EBFF00C2E3
      FF00BEDFFC00C5E6FF00CFF7FF0092A5B20056473400564331007D6F53005D4D
      2F00695A4D00DADEFF00726C72005B4B35000000000000000000000000000000
      0000000000000000000042444500B4BCB80066CC99000080000000800000426B
      3A00645E5400D2D1D100DCDCDC00C8C7C70047464700CFCFCF00232222006360
      60004646460026292B007CA7D3003A6DA0003366990033669900093C6F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CE6B00000000000000000000000000000000
      000044B1E400FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00F9F9F900F2F2
      F200EAEAEA00107FAD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CE947300FFEFD600F7DEC600FFF7E700F7D6A500E7BD
      A500FFF7E700F7D6A500DEAD9400FFF7E700F7D6A500D6A58400D69C7B0084A5
      E7001063D600CE8C6300C6845A00A56B4A000000000000000000000000000000
      0000000000000000000000000000000000000000000061513D00776D6900B5B4
      CD00867D7F00533F2B005846340076A4CC009BD6FF0063696C00779AB600A8E2
      FF009EDDFF006C8EA7005C48340000000000000000005A4835007E8E9A00BAE0
      FF00BEE0FE00B0D2ED008899A700ABCAE50097B2C7005F55490052402D005B4B
      3900A6A2B100AFAFC500655A5400000000000000000000000000000000000000
      000000000000000000004244450000800000008000002B6D210077694D00C983
      4B00FF934900CDC9C600B0B0B00096969600E7E7E700CBCBCB00282727007270
      70005D5B5B00292828006D7175007FA9D2004275A80033669900093C6F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CE6B00000000000000000000000000000000
      000044B1E40044B1E40044B1E40044B1E40044B1E40044B1E40044B1E40044B1
      E40044B1E4001DA3D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CE947300FFEFD600F7DEC600EFD6BD00E7C6AD00E7BD
      A500E7BD9C00DEB59C00DEAD9400DEAD8C00D6A58C00D6A58400D69C7B00D69C
      7B00CE947300CE8C6300C6845A00A56B4A000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005E4D3600695D
      5200AEAFCC00918A95005B452E0065869E0078B9EF00593D2300617D910084D5
      FF005F7C91005B473200000000000000000000000000000000005B473100757B
      8000B4DBFC0092AFC800442D130078818700ADD7FC00605A4F0064534100B2B1
      C300A4A0AC005F4E3B00614F3A00000000000000000000000000000000000000
      00000000000000000000424445004244450042444500B17C4C00FF703800CC3E
      2800AB1E1E0082517D00A98AA900EAE9E900B9B8B80099999900909090009A9A
      9A005D5B5B002928280072707000182635007FA9D2004376A900093C6F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CE6B00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D6947300FFEFD600D6A5A500CE949400CE949400CE94
      9400CE949400CE949400CE949400CE949400CE949400CE949400CE949400CE94
      9400CE949400CE949400C6845A00A56B4A000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000604E
      39005F514400A09FB4009994A6005C4632005C89AD0060B2F20062B4F6005D72
      8100624A30000000000000000000000000000000000000000000000000005D49
      32006671780094BADB00778A990088A8C3005F6B6E0066575000ACADCA008F8A
      95005D4C36000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000042444500E1442700862525008F42
      6900985998009C289C009647960093939300AFAFAF00F4F4F400E6E6E6005454
      54007D7C7C003736360086838300000E1D001A4D80005E91C40014477A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001DA3D600107FAD00107FAD00107FAD00107FAD00107FAD00107F
      AD00107FAD00107FAD00107FAD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D6947300FFEFD600B5848400A5DEA5009CD69C0094D6
      94008CD68C0084CE84007BCE7B0073CE730073C673006BC66B006BC66B006BC6
      6B006BC66B00CE949400C6845A00A56B4A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000604F3B008E8998009C98AC0062564B005591BD005C6C77006147
      2D00000000000000000000000000000000000000000000000000000000000000
      00005E4A34006367680097BEDF006B808F006F625B00ABABC7007D757A005D4B
      3500000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000004244450042444500424445009A30
      9A008F158F0052345200556A7C0067E1E100D1F8F8008080800082828200D2D2
      D200E8E8E8003F3F3F0086838300000E1D001D538900548DC700114579000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000044B1E400FCFCFC00FCFCFC00FCFCFC00FCFCFC00F5F5F500EEEE
      EE00E7E7E700D2D2D200107FAD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D6947300FFEFD600B5848400ADE7AD00A5DEA5009CD6
      9C0094D694008CD68C0084CE84007BCE7B0073CE730073C673006BC66B006BC6
      6B006BC66B00CE949400C6845A00A56B4A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005D4C37007D7781009997AF006C5C51005B452A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005D4A35005B5548006C615800A2A0B800746763005F4D36000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000424445004244
      45004551550057EFEF0036E1E1004A93930062414100CDC8C800D5D4D400B0AE
      AE00AFADAD0086858500C2C2C3000C1F330028478700252F7E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000044B1E400FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00F9F9
      F900F2F2F200EAEAEA00107FAD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D6947300FFF7EF00B5848400BDE7BD00ADE7AD00A5DE
      A5009CD69C0094D694008CD68C0084CE84007BCE7B0073CE730073C673006BC6
      6B006BC66B00CE949400C6845A00A56B4A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005E4C360070686D009696B500726764005848
      3100594B39000000000000000000000000000000000000000000000000000000
      0000584A38000000000000000000786F700087838D0055442E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004244
      450042444500424445007E2E2E00B0303000CE1E1E007A4661008D748D00FFFF
      FF00DDDDDD00C4C4C40044444400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000044B1E40044B1E40044B1E40044B1E40044B1E40044B1E40044B1
      E40044B1E40044B1E4001DA3D600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D6947300FFF7EF00B5848400B5848400B5848400B584
      8400B5848400B5848400B5848400B5848400B5848400B5848400B5848400B584
      8400B5848400D6A5A500C6845A00A56B4A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000614F3900655C5F008D90B0007972
      74005A4A3600584A390000000000000000000000000000000000000000005949
      3600625343006557480000000000746B6C00817D8B0056473300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000042444500C43E3E0044444400CC66CC00CC66CC0080008000A4A4
      A400444444004444440000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D6947300FFEFD600E7C6A500DEB59C00DEB59C00DEB5
      9C00DEB59C00DEB59C00DEB59C00DEB59C00DEB59C00DEB59C00DEB59C00DEB5
      9C00DEB59C00D6947B00C67B4A00A56B4A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000062503A005F5554008286
      A6007D7883005F4E3A0059493700000000000000000000000000584934006457
      4D008888A3006E65680000000000756B66007C798A0057473500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004244450042444500800080008000800080008000444444004444
      4400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D6947300D6947300D6947300D6947300D694
      7300D6947300D6947300D6947300D6947300D6947300D6947300D6947300D694
      7300D6947300D6947300D6947300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005F52
      46007D7B91007F7B8B006253410058493600594B3A005A4A35006A5D54008786
      9B008A8CA7006A62630000000000706769007976850058493500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000004244450044444400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005F513E00726E8100817F9500635649005B4A32006F64600085869D006F63
      5A00716A6F006961660057452B0068626B007471830059493600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000062503B006D656A007A7A9200736F7C007D7D97006A5D54005D49
      3000716B76006A636D00614F39006C666B00716F830057483600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000062503A006B605E007674880064574C0063513C00604F
      39006F6767007778920073728700797A92006E6971005E4E3A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000064533E0062523F0065533F00000000000000
      0000655542006556470065564700655646006554420000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000DDBAA900D0B5AA00CCB0A400DCB7A600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000046474700464747004647470046474700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000E9DCC0040B3
      DD0046B6DF0041B4DD0037AFD90026A7D400129ECD0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E0BDAF00CCC1BD00BCB6B600ADA5A500AF9F9A00D5B2A4000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004647470046474700DCD8DD00B9B7BB00A39D9F00A0969600464747004647
      4700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000049FCD001DA8D30093DA
      F90099DBFC0096DAFA0093D9F9008ED7F80086D3F5006BC7EC003FB3DD00109D
      CD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E0BDAF00DCD1D000D1CFD000C9C6C700BFB8BA00ADA5A500AD9D9900D4B4
      A400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000E80
      AA000E80AA000E80AA000E80AA000E80AA000E80AA000E80AA000E80AA000E80
      AA00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004343
      4400BAB8B900F5F4F400959595007270730068686A00656363009B9192009D92
      9300515051000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000BA0CE000FABD60021ABD50095DD
      F60095DCF9008FD7F90091D7F8008FD7F80068C2E60080CFF00099DCFC0080D1
      F3003DB2DC000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E1BF
      AF00E6DCDA00E2E2E300DCDADD00D0BCB500C7B2AB00BFBABB00AFA6A700AC9D
      9800D5B4A5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000E80AA000E80AA0021AA
      CF0021AACF0015AFD90026C2E90043C8E90060CEEA0062C3DE003DAACC00198E
      B7000E80AA000E80AA0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003739
      3A0077787A00949595007B7A7A009E919400AD9CA100847D7F00585657006461
      6100515051000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000012A4D1001FB3DC0027AED6009EE5
      F6009FE3F90094DCF90090D8F80090D8F90053B9DE0065C1E50094DAFB0094D9
      F90095DAFA005CC0E70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E2C1B100EDE5
      E200EEEFF300E9E9EB00D0917400C0522300C0522300C2816400BFB8BA00AFA7
      A900AD9D9900D3B1A30000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000E80AA002898BA0064D9E90060EC
      FE003CE0FE001BD4FE002ED8FE0055E0FF007BE8FF0095EBFD006CD4EE0039B8
      DE000C9BCB000C85B0000E80AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000767373008D8C
      910095969F008989910078747B00736B700075686B00665B5E00393B3A005150
      5100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000018A8D3002FBBE1002FB2D800AAEB
      F600A9EBF9009FE4F9009BE1FC006BB8D9003DA0C50054B9DF0085D2F40091D8
      F90090D7F80094D9FA0018A5D000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E2C0B000F2EBE900F4F8
      FB00F3F7FB00D3906F00BA400F00BA411100BB411200BB411100C1795800C0BA
      BA00AFA7A900A99A9700D3B1A300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000078CBA0076DFE90083F8FE0060EC
      FE003CE0FE001BD4FE002ED8FE0055E0FF007BE8FF0095EBFD006CD4EE0039B8
      DE000C9BCB00078CBA000E80AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000066666600D5C2
      AC00F1D6A900E8D2AF00D2C0AA00B6ADA600A19CA0008882890065636C004E50
      56004E5056000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001FABD6003EC3E80038B4DB00BEEF
      F700B9F2FA00A9ECF900A7EBFD0067B1CC00277FA0003EA4C90073C9EC0093D9
      FA008FD7F8008FD7F80018A5D000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1BDAD00F4EDE900FBFEFF00F8FE
      FF00D99F8200C2562700D9906D00DD9C7B00DD9C7B00D9906D00C4542400C176
      5300BFBABB00AFA7A900A99A9700D3B1A3000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000078CBA0076DFE90083F8FE0060EC
      FE003CE0FE001BD4FE002ED8FE0055E0FF007BE8FF0095EBFD006CD4EE0039B8
      DE000C9BCB000493C4000E80AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000066666600D4A4
      5A00FFAD0F00FFBA3100FFC35200FFCF7000FBD18A00E6C79900CCB7A300A599
      9600747174004848490000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000025AED8004DCAEE0040B8DD00D5F3
      F800D0F6FB00BAF2F900AEEFFA00A2E6F8003787A500398FB0008BD5F70090D8
      F9008FD7F8008FD7F80018A5D000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E1BDAD00F6EDE900FEFFFF00FEFFFF00DEA6
      8B00BC491900BC491900DD9D7E00FFFFFF00FFFFFF00DA977600BD4A1A00BF49
      1800C27B5B00BFB8BA00B0A9AA00AB9C9800D3B1A30000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000078CBA0076DFE90083F8FE0060EC
      FE003CE0FE001BD4FE002ED8FE0055E0FF007BE8FF0095EBFD006CD4EE0039B8
      DE000C9BCB000493C4000E80AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000066666600D1A3
      5B00FBA30500FFAC1900FFAF2600FFB12500FFB62C00FFBC3E00FFBC4900F6BB
      5900D7AA6C0056534D0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000002BB1DB0060D4F5004BBCE000F7FC
      FD00F6FFFF00DFFBFE00CBF8FC00BAF4FC005CA6BE004996B30098DEF90091D9
      F9008FD7F8008FD7F80018A5D000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E1BFAF00F8F0EE00FFFFFF00FFFFFF00E0A58800BD4A
      1A00BD4B1C00BC471600D17F5600FFFFFF00FFFFFF00CF784F00BC471800BF4E
      1E00BF4A1800C1755200C0B7B700B0A9AA00A99A9700CEAFA100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000078CBA0076DFE90083F8FE0060EC
      FE003CE0FE001BD4FE002ED8FE0055E0FF007BE8FF0095EBFD006CD4EE0039B8
      DE000C9BCB000493C4000E80AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000066666600D3AC
      6B00FBB32A00FFBF4200FEC05100FFC35400FFC35100FFBE4400FFB83400FFB6
      2600FEAF2C005E54450000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000002CB1DB004CC5EB002EADD7009BD9
      EC0099D9ED008CD2E8008CD4E80095DCED00A2E6F300A2E5F200AAE9FB009FE2
      FB0093DAF9008FD7F90018A5D000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E1BCAC00F8EFEB00FFFFFF00FFFFFF00E7B79D00C5592600C454
      2400C0512200BC471600D3815900FFFFFF00FFFFFF00D07A5200BC471800BF4E
      1F00BF4E1E00BF491800C1724E00BDB5B500AFA7A900A6999700CEAFA1000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000078CBA0076DFE90083F8FE0060EC
      FE003CE0FE001BD4FE002ED8FE0055E0FF007BE8FF0095EBFD006CD4EE0039B8
      DE000A9DCE000493C4000E80AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000066666600D3B1
      7400FABE4000FFCC5C00FFCE6B00FFD16F00FFD16F00FFCD6700FEC65C00FFC2
      4300F7B034005C53450000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000018A5D00052CCE40052C7DF0036A9
      C60037ADCA0046C6E00038BFDE0027B2D70023ADD40036B4D9005AC3E10084D8
      EF00A1E4FA009ADEFA0018A5D000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E0BAA900F4E9E200FFFFFF00FFFFFF00EECAB000CF703A00CB683300C964
      3000C65C2A00C04D1C00D4825A00FFFFFF00FFFFFF00D07A5200BC471800BF4E
      1F00BF4E1F00BF4E1E00BF471700C1785700BFB8BA00AFA6A500AA9A9700D8B5
      A500000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000078CBA0076DFE90097F9FE0093F2
      FE0095EBFD009EEDFF00B6F1FF00CCF6FF00D1F6FE00D1F6FE00AFE8F60079CF
      E90031AED7000493C4000E80AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000066666600D3B5
      7D00FAC95400FFD77100FFD98000FFDC8500FFDB8500FFD97C00FFD16F00FFCD
      5700EEB74E005A53480000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000014A3CE0085F1F90070C4CF002270
      8E002675940070D2DE007BEFFA0058D7EB0039C5E2001AB3D90004A2CF00089F
      CD003FB6DC008DDBF30018A5D000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7CABC00FFFFFF00FFFFFF00FEFBF800DC935D00D47A4100D1774000CF70
      3A00CC693500C5592600D78A6300FFFFFF00FFFFFF00D07A5200BC471800BF4E
      1F00BF4E1F00BF4E1F00BF4C1D00C0502100C6B0A700BFBABB00AFA6A500CBAF
      A400000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000078CBA00B4EEF300BCF2F8008ADA
      EB0059C2DD0032AACE00229BC2001E94BC0035A0C30056B1CE006BBDD7007AC6
      DF008FD0E60076C4DF000E80AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000066666600D1B9
      8400F9D06600FFDF8300FEE19300FEE69A00FEE59900FFE08E00FFDA8000FFD4
      6800ECBB5F0059544A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000E9ECD0046C1DD0066CA
      D9007FDCE4009EFEFE008DF7FC006EE4F20054D5EB0033B7D70011A1CA00099B
      C500019AC80016A3CF0018A5D000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7C9BC00FFFFFF00FFFFFF00FEFBF800E2A06900DA894B00D8844A00D47D
      4400D1763F00CB673000DA926A00FFFFFF00FFFFFF00D17D5400BC471800BF4E
      1F00BF4E1F00BF4E1F00BD4A1A00C0562800D1C0BB00C9C6C700BDB6B600D3B5
      A900000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000098AB7006FC2D6003FB0CB0039B8
      DE002FC6E70019C7F1002CD3F90054DFFE0075E2FA0081DBF10052BDDB0026A0
      C800118EB90046A6C9000E80AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000066666600D4BD
      8800FEDC7100FFE78F00FFEBA400FFF2B100FFF0AE00FFE89E00FFDE8C00FFD9
      7300ECBE660059544C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000179F
      CA0041C3E00067DCEE0084EFF70082EFF7006EE6F5003EB4D0000796C2000C92
      BD0033B7D70004A1D00018A5D000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E0BAA900F6E9E300FFFFFF00FFFFFF00F4DCC200E19A5C00DD8F5200DA89
      4F00D7824800D0723900E1A37B00FFFFFF00FFFFFF00D5875E00C04F1D00C050
      1F00BF4E1F00BD4B1C00BD4A1A00D4A08800DCDDE000D1CFD000CFC0BC00E0BA
      A900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000C85B00059C2DD0080F5FC0060EC
      FE003CE0FE001BD4FE002ED8FE0055E0FF007BE8FF0095EBFD006CD4EE0039B8
      DE000C9BCB00078CBA000E80AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000066666600C6B4
      8800E6C36700FFE28B00FFF3A900FFFABD00FFF9B900FFF0A400FFE48D00FFE1
      7300F0C568005A554C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000C9ECD001EA9D2002DB6DA0023A5CA00099AC6000B94
      BF000A9DCB00069BCB0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E1BCAC00F8EFEA00FFFFFF00FFFFFF00F4DCC000E29E5F00DE95
      5600DA8E5100DC8E5600EEC6A900F4DECE00F4DDCC00D7865800C75C2800C55A
      2800C0501F00BD4B1C00D89F8400E9EDF000E2E2E300DCCFCB00E0BBAB000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000078CBA0076DFE90083F8FE0060EC
      FE003CE0FE001BD4FE002ED8FE0055E0FF007BE8FF0095EBFD006CD4EE0039B8
      DE000C9BCB000493C4000E80AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000066666600A6A2
      9B00B2A99900B2AB9900B3AD9800BCB99F00C7C09F00D3C39500E0C58600F1CF
      7100EDC16E005B554E0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000A9BC6000B91
      BC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E1BCAC00F6EBE500FFFFFF00FFFFFF00F7E2CA00E6A6
      6700E19A5A00E0985C00DD905500D9884E00D5804700D0743C00CE6E3800CA65
      3000C65C2800E0AF9700F2F7FB00EDEFF200E5D9D500E0BBAB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000078CBA0076DFE90083F8FE0060EC
      FE003CE0FE001BD4FE002ED8FE0055E0FF007BE8FF0095EBFD006CD4EE0039B8
      DE000C9BCB000493C4000E80AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004F51
      5300535558004D4E51007373760099979900999597009E9A9A00A39D9F009C94
      9000A6968A005552500000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000A85
      B0000A85B00000000000000000000000000000000000000000000A9BC7000B91
      BC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E0BBAB00F4E9E200FFFFFF00FFFFFF00F8E5
      CE00E9AC6D00E29D5B00E3A56D00F7E3D300F6DECE00DA8C5700D1763D00CF6F
      3700E7B79D00F8FEFF00F4F8FB00EDE2DE00E0BCAC0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000078CBA0076DFE90083F8FE0060EC
      FE003CE0FE001BD4FE002ED8FE0055E0FF007BE8FF0095EBFD006CD4EE0039B8
      DE000C9BCB000493C4000E80AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000004A4B4C005051520037373B004F5053004F5055004C4E
      52004E4F51000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000010A2
      CE000996C20000000000000000000000000000000000000000000A9BC7000B91
      BC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E0BBAB00F4E9E300FFFFFF00FFFF
      FF00FAE9D300E9AB6700EFC99E00FFFFFF00FFFFFF00E7B28700D57D3F00E9BB
      9C00FEFFFF00FBFEFF00F0E9E600E1BFAF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000078CBA0076DFE90083F8FE0060EC
      FE003CE0FE001BD4FE002ED8FE0055E0FF007BE8FF0095EBFD006CD4EE0039B8
      DE000A9DCE000493C4000E80AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000919B9E0037373B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003FB4
      DB0034AFD90000000000000000000000000000000000000000000A9BC7000B91
      BC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E1BCAC00F4E7E100FFFF
      FF00FFFFFF00F8E6CE00EFC08600F7E0C200F6DABD00E3A06500EEC7A500FFFF
      FF00FEFFFF00F6EDE900E2C0B000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000078CBA0076DFE90083F8FE0060EC
      FE003CE0FE001BD4FE002ED8FE0055E0FF007BE8FF0095EBFD006CD4EE0039B8
      DE000C9BCB000493C4000E80AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000CCFF000099
      CC00000000000000000000000000919B9E0037373B0000000000000000000000
      000000CCFF000099CC0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003CA8
      C90080E6FD002293BB0000000000000000000000000000000000099CC7000B91
      BD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E0BAA900F3E5
      DE00FFFFFF00FFFFFF00FBEDD700F0C48A00EBB77A00F4D8B800FFFFFF00FFFF
      FF00F7EEEA00E1BFAF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000078CBA007DDFE900ADFBFE00B3F9
      FF00ADFBFE00ADFBFE0099FFFE0099FFFE0099FFFE0099FFFE008FF6FB006BE3
      F40029BBE0000C9BCB000E80AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000F0FF000099CC000000
      00000000000000CCFF000099CC00919B9E0037373B0000CCFF000099CC000000
      00000000000000F0FF000099CC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000070D8E7008EF1FA0040ADCB001586B0001182AC00138FBA000AA5D1000C8A
      B500000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E0BA
      A900F3E5DE00FFFFFF00FFFFFF00FFFEFC00FEFBF800FFFFFF00FFFFFF00FAF3
      F000E2C0B0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000078CBA00C3DFEA00FAFFFF00E5FF
      FF00CFFFFF00B3F9FF00A4FFFE0099FFFE0099FFFE0099FFFE0099FFFE0097F9
      FE0071F2FE003ECDE9000E80AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000F0FF000099CC000000
      000001FFFF00007FB30000000000919B9E0037373B000000000001FFFF00007F
      B3000000000000F0FF000099CC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00001587AE0064CCDD0092F6FA0086ECF80074DCF5005ACFF4001E9FCA000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E0BAA900F4E7E100FFFFFF00FFFFFF00FFFFFF00FFFFFF00FAF3F000E3C2
      B400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000078CBA0068B0CA00F0F7FA00F5FF
      FF00DFFFFF00CFFFFF00B3F9FF0099FFFE0099FFFE0099FFFE0099FFFE0099FF
      FE0080F5FC0032AACE000E80AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000F0FF000099CC000000
      000001FFFF00007FB3000000000000000000000000000000000001FFFF00007F
      B3000000000000F0FF000099CC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000001E8FB4003CA9C50042AECA002A98BE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E0BAA900F2E0D700FEFCFC00FFFEFE00F7EEEA00E3C2B2000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000078CBA000E91BE007CC0
      D5007CC0D500A4DFEA0094DFE90083DFE9007DDFE90076DFE90054C0D40054C0
      D4001788AF000E80AA0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000F0FF000099CC000000
      00000000000000CCFF000099CC00000000000000000000CCFF000099CC000000
      00000000000000F0FF000099CC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E1BCAB00E6C5B700E7C9BC00E1BCAB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000078C
      BA00078CBA00078CBA00078CBA00078CBA00078CBA00078CBA00078CBA00078C
      BA00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000CCFF000099
      CC00000000000000000000000000000000000000000000000000000000000000
      000000CCFF000099CC0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000007AB200007AB200007AB200007A
      B200007AB200007AB200007AB200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000009C0000009C0000009C0000009C0000009C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000007AB2001196C90072C7F20084D3FC007BCE
      F70079CCF70076CCF6006BC5F100007AB200007AB200007AB200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000031313100000000000000
      000000009C00424AE700424AE7009CA5EF0000009C0000000000000000003131
      3100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A5006B4231006B4231006B4231006B4231006B4231006B4231006B42
      31006B4231006B4231006B4231006B4231006B4231006B4231006B4231006B42
      31006B4231006B4231006B4231006B4231006B4231006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009D64
      3F009D643F0089594000007AB2000694C600149ACB0075CEF00084D3FC0076CC
      F60079CCF70075CBF5006BC5F10081D0FA007BCEF70049B4E200007AB2000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008C4ABD004A737B004A73
      7B004A737B004A737B004A737B004A737B008C4ABD0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000009C000010DE000008DE008484EF0000009C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BB794500BB794500BB79
      4500BB7945009D643F00007AB2000D9DD0001B9FCE007FD8F00094E4FB007DD1
      F8007ACFF8006BC5F1003BA5D40069C2EE0081D0FA0085D5FB005DBEEA00007A
      B200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000021528C003973AD00295A
      9C00215A940021528C0021528C0021528C00184A84005A5A5A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000009C000010DE000010DE00848CEF0000009C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500FFFFFF0010942900108C210010842100087B180008731000006B
      0800006B000000630000006300000063000000630000005A0000005A0000006B
      0000006B0000005A000000185A0000840000FFFFFF006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D2853B00D2853B00D2853B00D2792600CC69
      1800B1530E00B1530E00007AB20018A7D40023A4D0008FE0EF00A1EFFB008ADE
      F50083D6F70058B5DE002D9CCA005DBEEA007BD0FB0076CCF60081D0FA00007A
      B200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000021528C003973AD003163
      9C00295A9C00215A940021528C0021528C00184A8400528CC6005A5A5A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000009C000018E7000010E700848CEF0000009C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500FFFFFF001894310010942900108C210008841800087B18000873
      1000006B0800006B080000630000006300000063000000630000006B0000006B
      0800006B0800006B0000005A0000004A4200FFFFFF006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000DC863500E28B3700EA862400E46C0900E6620100E662
      0100DD5D0000C74B0100007AB20023B3DD002BAAD500A9E5F000BBF6FB0099EA
      F8008ADEF5003283A6001C7BA70040A9D70072CAF4007ACFF80079CCF700007A
      B200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000021528C003973AD00316B
      A50031639C00295A9C00215A940021528C00184A8400528CC600528CC6005A5A
      5A00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000009C000018E7000018E700848CF70000009C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500FFFFFF00189C31001894310010942900108C210008841800087B
      18000873100000730800006B08000063000000630000006B0800006B0800006B
      0800006B0800006B000000630000637B0000FFFFFF006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000EA862400F47E1100F36E0300F36E0300F4700500FC70
      0500F46B0500595D0000007AB20030BAE20035ADD900C5EAF300DDF9FD00B1EF
      F800A1EFFB0066B7CE00105B810041A4C0007BD0FB0079CCF70079CCF700007A
      B200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000021528C003973AD003973
      AD00316BA50031639C00295A9C00215A9400184A8400528CC600528CC600528C
      C6005A5A5A000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003131310031313100000000000000
      000000009C000018EF000018EF00848CF70000009C0000000000000000003131
      3100313131000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500FFFFFF0018A53900189C3900189C310010942900108C21001084
      2100087B18000873100000731000006B0800006B0800006B0800006B0800006B
      0800006B0800006300007B8C0000D6A50000FFFFFF006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F78B2400F47B0F00F3740800F3790E00FD790E00FE7E1200C278
      0C00567607002A620100007AB2003EC4EF0041B6D800DAEEF400FFFFFF00DDF9
      FD00CAF9FC0098DBE7000F6B880054A6C5008DE1FD0079CCF70079CCF700007A
      B200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000021528C003973AD00427B
      B5003973AD00316BA50031639C00295A9C00184A8400528CC600528CC600528C
      C600528CC6005A5A5A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000009C000021EF000021EF007B8CF70000009C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500FFFFFF0021A5420018A53900189C39001894310010942900108C
      290010842100087B1800087B100000730800106B0800395A3900006300000063
      0000006B0000848C0000FFBD0000FFB50000FFFFFF006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F4801300F3790E00F37F1400FE841B00E1861C00567607001676
      030073801200D06F1600007AB2004BCAF7003CB1DD00C4E2EF00EAF5F900EAF5
      F900DDF9FD00CAF9FC0098DBE70098DBE70094E4FB0083D6F7007DD1F800007A
      B200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000021528C003973AD004284
      B500427BB5003973AD00316BA50031639C00184A8400528CC600528CC600528C
      C600528CC6004A84BD005A5A5A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000009C000021EF000021E7007B8CEF0000009C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500FFFFFF00219C390021AD420018A54200189C3900189C31001094
      2900108C290010842100087B180010731000638421009C7B390063A500000073
      00007B940000FFC60000FFB55A00FFB50800FFFFFF006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F3790E00F37F1400F4861C00F78B2400D18F260017810C00097F0500C08F
      2800F4903400D2792600007AB20030BAE2002BAAD50057BDDB0062C5E00057BD
      DB005ABBDA0069C4E00083D5EB0096E1F200A1ECFA00A1EFFB008DE1FD00007A
      B200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000021528C003973AD004A8C
      C6004A84BD00427BB5003973AD003973AD00184A8400528CC600528CC600528C
      C6004A84BD00427BB5005A5A5A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000009C000021CE000021C6006B7BCE0000009C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500FFFFFF00529C4200219C390021AD420018A53900189C3900189C
      310010942900108C2900087B18005A842100D6A54200FFB55A00FFB55A00FFBD
      0000FFCE0000FFB55A00FFB55A00FFB50800FFFFFF006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F37E1200F4861C00F5912B00FE96350050911D000284070054932700FEA7
      5300F29D4200D2853B000689BC0056D3E7004EC5D8000C93C000189EC50040C7
      E00030BAE20018A7D4000F9ACA00149ACB0031AAD3006BCEE700A1ECFA00007A
      B200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000021528C003973AD005294
      C6004A8CC6004A84BD00427BB5003973AD00184A8400528CC600528CC6004A84
      BD00427BB500397BAD005A5A5A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000009C0000189C0000108400525A9C0000009C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500FFFFFF00BDB57300529442002194310021AD420018A54200189C
      3900189C3100108C2900297B1800CEA54A00F7B55200FFB55A00FFB55A00E7C6
      2100FFB55A00FFB55A00FFB55A00FFB51800FFFFFF006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F4861C00F5912B00FA9A3B00E5A04400269E2300149C1C008CAF4A00FCB2
      6500F0AB5B00E1984A006A81650026ACD1002BA6C500007AB200108CB80075E7
      EE006BF2FA0042D0E90023B3DD000694C6000182BA000689BC0031AAD300007A
      B200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000021528C003973AD005A9C
      CE005294C6004A8CC6004A84BD00427BB500184A8400528CC6004A84BD00427B
      B500397BAD003973AD005A5A5A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000031313100313131003131310031313100000000000000
      0000000018000018840000105A00424A7B000000180000000000000000003131
      3100313131003131310031313100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500FFFFFF00F7C69400D6BD840073944A00108C290018A5390018A5
      4200189C310021842100A59C4A00F7B56300FFB55A00FFB55A00F7B55200F7AD
      4A00F7AD4200FFAD3900FFAD3100FFA52900FFFFFF006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F78B2400F4983600FCA44B00CDA94D0031AB32002DAC320065B84D00FDBE
      7F00FDBE7F00F0AD6500E2995000987B3E005F796200007BB4000D8DBB006AE0
      EA007EFBFF0069E7F70040C7E000108CB800027BAF000D8DBB00048CC1000182
      BA00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000021528C003973AD0063A5
      D6005A9CCE005294C6004A8CC6004A84BD00184A84004A84BD00427BB500397B
      AD003973AD00316BA5005A5A5A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000063
      9C0000639C0000639C0000639C0000639C0000639C0000639C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500FFFFFF00FFCE9C00FFCE9C00FFCE9C00BDB57300528C3900298C
      2900428C3100ADA55A00F7BD7300FFBD6B00FFBD6B00F7B56300F7B55A00F7B5
      5200F7AD5200F7AD4200F7AD3900FFAD3100FFFFFF006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F5912B00F29D4200FEA753008CAF4A0041BB44004BC04D0049BE4A0078C0
      5C00B0C06F00D9B86B00EFAC6500E1984A0078764E000182BA00027BAF001385
      B3002BA6C50035B0C70026ACD1000A86B500027BAF000689BC00048CC1000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000021528C003973AD0063A5
      D60063A5D6005A9CCE005294C6004A8CC600184A8400427BB500397BAD003973
      AD00316BA50029639C005A5A5A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000B5E70000B5E70000B5E70000B5E70000B5E70000639C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500FFFFFF00FFCE9C00FFCE9C00FFCE9C00F7C69400D6BD8400A5A5
      6300CEB57300F7BD7B00F7BD7B00FFBD7300F7BD6B00EFB56B00D6AD7B00B5AD
      8C00ADA59400C6AD7300E7AD5200F7AD4200FFFFFF006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F4983600FCA44B00FCB2650078C05C0055CA5B006AD36B006AD36B0055CA
      5B0046BF4A003FB13C0047A4350054932700126F33001196C900077BAD00785B
      2700C659030082500100046215000F6B88000382B50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000021528C003973AD0063A5
      D60063A5D60063A5D6005A9CCE005294C600184A8400397BAD003973AD00316B
      A50029639C00295A94005A5A5A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000639C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500FFFFFF00FFCE9C00FFCE9C00FFCE9C00FFCE9400FFC69400FFC6
      9400FFC69400FFC68C00FFC68400F7BD7B00EFBD7B00B5B59C0063A5D600429C
      EF00399CEF00529CDE0094A5A500DEAD5A00FFFFFF006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F79A3C00FEA75300D9B86B0061CC610072D9740084E3860083E2840072D9
      740059C95B0040B8410027A42900128B1600126F330038ADD8003CB1DD004B5B
      4300C65903006F5600000A590000015D46000382B50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000396B0000396B000039
      6B0000396B0000396B0000396B0000396B0000396B003973AD00316BA5002963
      9C00295A9400215294005A5A5A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00009CCE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500FFFFFF00FFCE9C00FFCE9C00FFCE9C00FFCE9C00FFCE9C00FFC6
      9400FFC69400FFC69400FFC68C00F7C68400CEBD94005AADD60029ADF70029A5
      F700299CFF00319CF700429CE700A5A59400FFFFFF006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CDA94D0065B84D005FD0650084E38600A3F6A3009DF09D0081E2
      810064CF650048BB470030AA31001A921C0009721C002B83900069E7F7003FB2
      CF0056684E0032543100015D4600037DB0000382B50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000021528C003973AD00CEE7
      F700BDDEEF00ADCEE7009CBDDE008CB5D6007BA5CE0000396B0029639C00295A
      94002152940018528C005A5A5A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000003131310031313100313131003131310031313100000000000000
      000000009C00184AFF00104AFF008CA5FF0000009C0000000000000000003131
      3100313131003131310031313100313131000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500FFFFFF00FFCE9C00FFCE9C00FFCE9C00FFCE9C00FFCE9C00FFCE
      9C00FFC69400FFC69400FFC69400F7C68C00A5B5B50029B5F70018BDF70021AD
      FF0029A5FF00299CF700319CF700739CBD00FFFFFF006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000003BAB350046BF4A0064CF650081E281009DF09D009DF09D007EDE
      7F0063CF640046BC470030A9310019971C0017810C00987B3E0046989D0065DD
      EB0056D3E70041B6D80037B0D900189ED3000474A50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000021528C003973
      AD00CEE7F700BDDEEF00ADCEE7009CBDDE008CB5D6007BA5CE0000396B002152
      940018528C00184A84005A5A5A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000009C002152FF00184AFF008CA5FF0000009C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500FFFFFF00FFCE9C00FFCE9C00FFCE9C00FFCE9C00FFCE9C00FFCE
      9C00FFCE9C00FFC69400FFC69400EFC6940094BDC60021C6F70018CEFF0018BD
      FF0021B5FF0029A5FF00299CF700639CCE00FFFFFF006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000041BB440058C759006FD870007FDE80007EDE7F006AD3
      6B0057C7580041B6410029A62B00189518000E820B00B1842400B4742800987B
      3E0041A4C0003FB2CF0035A9CC001385B3000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002152
      8C003973AD00CEE7F700BDDEEF00ADCEE7009CBDDE008CB5D6007BA5CE000039
      6B00184A8400184A84005A5A5A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000009C002152FF002152FF008CADFF0000009C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500FFFFFF00FFCE9C00FFCE9C00FFCE9C00FFCE9C00FFCE9C00FFCE
      9C00FFCE9C00FFCE9C00FFCE9C00F7C69C00B5BDB50031C6EF0010D6F70018CE
      F70018C6FF0021B5F70031A5F70084A5BD00FFFFFF006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000048BB470057C7580061CC610061CC610054C5
      550046BB470035AF3500269E230013911400037F050073801200F27B1700E46C
      0900C65903000A59000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000021528C003973AD00CEE7F700BDDEEF00ADCEE7009CBDDE008CB5D6007BA5
      CE0000396B00184A84005A5A5A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000009C00295AFF00295AFF0094ADFF0000009C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500FFFFFF00FFCE9C00FFCE9C00FFCE9C00FFCE9C00FFCE9C00FFCE
      9C00FFCE9C00FFCE9C00FFCE9C00F7C69C00DEC6A50073BDD60021CEF70018CE
      F70018C6F70029B5F70052ADDE00BDB59400FFFFFF006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000041B6410046BC470046BC470041B6
      410035AF350026A3270019961A000D8A0D00007A0200777C0C00FE7E1200EF6E
      0500E66201000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000021528C003973AD00CEE7F700BDDEEF00ADCEE7009CBDDE008CB5
      D6007BA5CE0000396B005A5A5A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000009C003163FF00295AFF0094ADFF0000009C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500FFFFFF00FFCE9C00FFCE9C00FFCE9C00FFCE9C00FFCE9C00FFCE
      9C00FFCE9C00FFCE9C00FFCE9C00FFCE9C00F7C69C00CEC6AD0084BDCE005ABD
      DE004ABDE70073B5CE00B5B5A500EFBD7300FFFFFF006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000030AA31002DA92D0029A6
      2B00269E2300189618000D8A0D00037F05000A760200D47E1000F8750900F46B
      0500000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000021528C003973AD00CEE7F700BDDEEF00ADCEE7009CBD
      DE008CB5D6007BA5CE005A5A5A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003131
      3100313131003131310031313100313131003131310031313100000000000000
      000000009C00396BFF003163FF009CB5FF0000009C0000000000000000003131
      3100313131003131310031313100313131003131310031313100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF006B423100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001996
      1A00139114000D8A0D00037F050000760000777C0C00FD790E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000083973000839730008397300083973000839
      7300083973000839730008397300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000009C00396BFF00396BFF009CB5FF0000009C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6B5A500C6B5A500C6B5A500C6B5A500C6B5A500C6B5A500C6B5A500C6B5
      A500C6B5A500C6B5A500C6B5A500C6B5A500C6B5A500C6B5A500C6B5A500C6B5
      A500C6B5A500C6B5A500C6B5A500C6B5A500C6B5A500C6B5A500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000009C0000009C0000009C0000009C0000009C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300DE737300DE737300DE737300DE737300DE737300DE73
      7300DE737300DE737300DE737300DE737300DE737300DE737300DE7373000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBD
      BD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00DE737300DE73
      7300DE7373000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBD
      BD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00DE737300EFBDBD00DE737300EFBD
      BD00EFBDBD00DE737300DE737300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C
      7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C
      7B00A58C7B00D6B5A50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBD
      BD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00DE737300EFBD
      BD00EFBDBD00EFBDBD00EFBDBD00DE737300DE73730000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B0029396B0029396B0029396B0029396B0029396B0029396B002939
      6B0029396B0029396B0029396B0029396B0029396B0029396B0029396B002939
      6B0029396B0029396B0029396B0029396B0029396B0029396B0029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A58C7B00FFFFFF00BDA59400BDA59400BDA59400BDA59400BDA59400BDA5
      9400BDA59400BDA59400BDA59400BDA59400BDA59400BDA59400BDA59400BDA5
      9400BDA59400A58C7B0084634A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300848484008484840084848400EFBDBD00EFBDBD00EFBD
      BD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00DE737300EFBD
      BD00EFBDBD00EFB5B500EFB5B500EFB5B500EFB5B500EFB5B500DE7373000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029428C003152A5003152A5003152A5003152A5003152A5003152A5003152
      A5003152A5003152A5003152A5003152A5003152A5003152A5003152A5003152
      A5003152A5003152A5003152A5003152A5003152A5003152A50029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008C4ABD004A737B004A737B004A737B004A737B004A737B004A737B008C4A
      BD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A58C7B00FFFFFF00FFFFFF00FFF7F700FFF7EF00FFEFE700F7E7DE00F7E7
      D600F7DECE00F7DEC600F7D6BD00F7CEB500EFCEAD00EFC6A500313194003131
      9400EFBD9400A58C7B00735A4A0084634A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBD
      BD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00DE737300EFBD
      BD00EFB5B500EFB5B500EFB5B500EFB5B500EFB5B500EFB5B500DE7373000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B00314A4A00394A4200394A420039524A0039524A00315A4A00315A
      4A00315A5200316352003152A50029396B00316B5200316B5200396B5A003973
      5A0039735A00396B5A00396B5A0039735A0031636B003152A50029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000021528C003973AD00295A9C00215A940021528C0021528C0021528C00184A
      84005A5A5A000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A58C7B00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFF7EF00FFEFE700FFEF
      DE00F7E7D600F7DECE00F7DEC600F7D6BD00F7CEB500EFCEAD000031FF003131
      9400EFBD9400A58C7B007B634A00846B5A0084634A0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300DE737300DE737300DE737300DE737300DE737300DE73
      7300DE737300DE737300DE737300DE737300DE737300DE737300DE737300EFB5
      B500EFB5B500EFB5B500EFB5B500EFB5B500EFB5B500EFADAD00DE7373000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B0031525200315A4A003163520031635200316B5200316B5200316B
      5200316B5200316B5A003152A50029396B0039735A00397B6300397B63003984
      6B0039846B0039846B0039846B0039846B00317B7B003152A50029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000021528C003973AD0031639C00295A9C00215A940021528C0021528C00184A
      8400528CC6005A5A5A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A58C7B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFF7EF00FFEF
      E700FFEFDE00F7E7D600F7DECE00F7DEC600F7D6BD00F7CEB500009C0000009C
      0000EFBD9C00A58C7B00846B5A00947B6B00A58C730084634A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBD
      BD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00DE737300EFB5
      B500EFB5B500EFB5B500EFB5B500EFB5B500EFB5B500EFADAD00DE7373000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B00316B6300317B6300317B630031846B0031846B0031846B00318C
      6B00318C6B00399473003152A50029396B00399473003994730039947300399C
      7B00399C7B00399C7B00399C7B0039947300397B7B003152A50029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000021528C003973AD00316BA50031639C00295A9C00215A940021528C00184A
      8400528CC600528CC6005A5A5A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A58C7B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFF7
      EF00FFEFE700FFEFDE00F7E7D600F7DECE00F7DEC600F7D6BD00B5A594006B42
      31006B4231006B4231006B4231006B4231006B42310084634A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBD
      BD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00DE737300EFB5
      B500EFB5B500EFB5B500EFB5B500EFB5B500EFADAD00EFADAD00DE7373000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B00317B7300319C7B00319C7B00319C7B00319C7B00319C7B0039A5
      7B00399C7B00399C7B003152A50029396B00399C7B00399C7B00399473003994
      7300398C730039846B0039846B0039846300316B6B003152A50029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000021528C003973AD003973AD00316BA50031639C00295A9C00215A9400184A
      8400528CC600528CC600528CC6005A5A5A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A58C7B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      F700FFF7EF00FFEFE700FFEFDE00F7E7D600F7DECE00F7DEC600B5A59400FFFF
      FF0094A5FF00738CFF00EFEFFF00FFFFFF006B42310084634A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300EFBDBD00EFBDBD005252520052525200525252005252
      5200525252005252520052525200EFBDBD00EFBDBD00EFBDBD00DE737300EFB5
      B500EFB5B500EFB5B500EFB5B500EFADAD00EFADAD00EFADAD00DE7373000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B00318C7B0039A5840039A5840039A5840039A5840039A58400399C
      7B00399C7B0039947B003152A50029396B00398C6B0039846B00397B63003973
      5A0039735A00396B5A00396B520039635200395A63003152A50029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000021528C003973AD00427BB5003973AD00316BA50031639C00295A9C00184A
      8400528CC600528CC600528CC600528CC600EF8C6B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C
      7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00B5A59400DEE7
      FF004A63FF000031FF009CADFF00FFFFFF006B42310084634A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300EFBDBD00EFBDBD00525252005A5A5A005A5A5A00FFFF
      84005A5A5A005A5A5A0052525200EFBDBD00EFBDBD00EFBDBD00DE737300EFB5
      B500EFB5B500EFB5B500EFADAD00EFADAD00EFADAD00EFADAD00DE7373000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B00398C7B0039A58400399C8400429C7B0042947B0039947300398C
      7300398C6B0039846B003152A50029396B0039735A00396B5200396352003963
      5200426352005A736300737B73008C8C8400848C9C003152A50029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000021528C003973AD004284B500427BB5003973AD00316BA50031639C00184A
      8400528CC600528CC600528CC600528CC600EF947300F7947300E75210004A73
      7B004A737B004A737B004A737B004A737B008C4ABD0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A58C7B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFF700FFF7EF00FFEFE700FFEFDE00F7E7D600B5A594008C9C
      FF000031FF000031FF000031FF00E7E7FF006B42310084634A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300EFBDBD00EFBDBD00525252005A5A5A00FFFF8400FF00
      0000FFFF84005A5A5A0052525200EFBDBD00EFBDBD00EFBDBD00DE737300EFB5
      B500EFB5B500EFADAD00EFADAD00EFADAD00EFADAD00EFADAD00DE7373000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B00528C84005AA584005AA584005A9C7B005A947B005A947B005A94
      7B00638C73006B846B003152A50029396B009C8C7300B5947B00CEA58C00E7BD
      AD00EFD6CE00F7CEC600EFC6B500EFC6AD00BDA5AD003152A50029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000021528C003973AD004A8CC6004A84BD00427BB5003973AD003973AD00184A
      8400528CC600528CC600528CC6004A84BD00F79C7300FFA57300D65A0800295A
      9C00215A940021528C0021528C0021528C00184A84005A5A5A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A58C7B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFF700FFF7EF00FFEFE700FFEFDE00B5A594000031
      FF000031FF00D6DEFF000031FF007394FF006B42310084634A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300EFBDBD00EFBDBD00525252005A5A5A00FFFF8400FF00
      0000FFFF84005252520052525200EFBDBD00EFBDBD00EFBDBD00DE737300EFB5
      B500EFADAD00EFADAD00EFADAD00EFADAD00EFADAD00EFADAD00DE7373000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B00B59C8C00E7BD9400E7B59400DEB59400DEAD9400DEB59C00E7BD
      9C00EFBD9C00F7AD94003152A50029396B00EFAD9400EFA58400EFAD9400EFBD
      A500EFB59C00EFB59C00F7B59C00EFAD8C00BD9494003152A50029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000021528C003973AD005294C6004A8CC6004A84BD00427BB5003973AD00184A
      8400528CC600528CC6004A84BD00427BB500F79C7300F79C6B00D65A08003163
      9C00295A9C00215A940021528C0021528C00184A8400528CC6005A5A5A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A5948400A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C
      7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00B5A59400E7EF
      FF007394FF00FFFFFF00E7E7FF000031FF009CADFF0084634A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300EFBDBD00EFBDBD00525252005A5A5A005A5A5A00FFFF
      8400525252005252520052525200EFBDBD00EFBDBD00EFBDBD00DE737300EFAD
      AD00EFADAD00EFADAD00EFADAD00EFADAD00EFADAD00EFA5A500DE7373000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B00BD948400F7B58C00F7B58C00F7AD8C00F7AD8C00F7AD8400F7AD
      8400F7A58400F7A584003152A50029396B00EF9C7300EF9C7B00EFA58C00EFB5
      9C00EFB5A500EFAD9400F7AD8C00F7B59C00BDADB5003152A50029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000021528C003973AD005A9CCE005294C6004A8CC6004A84BD00427BB500184A
      8400528CC6004A84BD00427BB500397BAD00F7946B00F7946300D65A0800316B
      A50031639C00295A9C00215A940021528C00184A8400528CC600528CC6000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A58C7B00D6BDAD00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFF7
      F700FFF7F700F7F7F700F7F7EF00F7EFEF00F7EFEF00F7EFE700B5A59400B5A5
      9400B5A59400B5A59400B5A594009CADFF000031FF0084634A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300EFBDBD00EFBDBD005252520052525200525252005252
      5200525252005252520052525200EFBDBD00EFBDBD00EFBDBD00DE737300EFAD
      AD00EFADAD00EFADAD00EFADAD00EFADAD00EFA5A500E7A5A500DE7373000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B00BD948400F7B59400F7B59400F7AD8400F7A57B00F7A57B00F7A5
      7B00F7A57B00F79C7B003152A50029396B00F7CEB500F7BDA500EFA58400EFB5
      9400EFAD8C00F7E7DE00F7C6AD00F7CEC600BDC6DE003152A50029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000021528C003973AD0063A5D6005A9CCE005294C6004A8CC6004A84BD00184A
      84004A84BD00427BB500397BAD003973AD00F78C6300F78C5A00D65A08003973
      AD00316BA50031639C00295A9C00215A9400184A8400528CC600528CC6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D6BDAD00FFFFFF00FFFFFF00DECEC600D6BD
      AD00D6BDAD00D6BDAD00D6BDAD00D6BDAD00D6BDAD00D6BDAD00DECEC600F7EF
      E70084634A00EFBD9400F7DECE00A58C7B009CADFF000031FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBD
      BD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00DE737300EFAD
      AD00EFADAD00EFADAD00EFADAD00EFA5A500EFA5A500E7A5A500DE7373000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B00BD9C8C00F7D6BD00F7CEAD00F7AD8400F79C7300F79C7300F79C
      7300F79C7300F79C73003152A50029396B00F7DECE00F7C6AD00EF946B00EF94
      6300EF8C5A00EFAD8C00EFA58400F7C6AD00BDADBD003152A50029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000021528C003973AD0063A5D60063A5D6005A9CCE005294C6004A8CC600184A
      8400427BB500397BAD003973AD00316BA500F78C5A00F7844A00D65A0800427B
      B5003973AD00316BA50031639C00295A9C00184A8400528CC600528CC6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D6BDAD00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFF700FFF7F700F7F7F700F7F7EF00F7F7EF00B5A594006B42
      31006B4231006B4231006B4231006B4231006B4231009CADFF000031FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBD
      BD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00DE737300EFAD
      AD00EFADAD00EFADAD00EFA5A500EFA5A500E7A5A500E7A5A500DE7373000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B00BD9C9400F7BD9400F7AD8400F79C7300F79C6B00F7946B00F794
      6B00F7946300F79463003152A50029396B00EF946B00EF8C6300EF845200EF84
      5200EF845200EF845200EFA58400F7DECE00BDB5C6003152A50029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000021528C003973AD0063A5D60063A5D60063A5D6005A9CCE005294C600184A
      8400397BAD003973AD00316BA50029639C00F7845A00F77B4200D65A08004284
      B500427BB5003973AD00316BA50031639C00184A8400528CC600528CC6000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D6BDAD00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFF700FFF7F700F7F7F700F7F7EF00B5A59400FFFF
      FF0094A5FF00738CFF00EFEFFF00FFFFFF006B423100D6BDAD009CADFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBD
      BD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00DE737300EFAD
      AD00EFADAD00EFA5A500EFA5A500E7A5A500E7A5A500E7A5A500DE7373000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B002942730029427B0029427B0029427B0029427B0029427B002942
      7B0029427B0029427B003152A50029396B0029427B0029427B0029427B002942
      7B0029427B0029427B002942730029427300294284003152A50029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000396B0000396B0000396B0000396B0000396B0000396B0000396B000039
      6B003973AD00316BA50029639C00295A9400F7845200F7733900D65A08004A8C
      C6004A84BD00427BB5003973AD003973AD00184A8400528CC6004A84BD000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D6BDAD00FFFFFF00FFFFFF00DECE
      C600D6BDAD00D6BDAD00D6BDAD00D6BDAD00D6BDAD00D6BDAD00B5A59400DEE7
      FF004A63FF000031FF009CADFF00FFFFFF006B42310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBD
      BD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFBDBD00DE737300EFAD
      AD00EFA5A500EFA5A500E7A5A500E7A5A500E7A5A500E7A5A500DE7373000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B006B637B00846B840084738C00847B9C0084738C00847B9400846B
      7B0084637B0084637B003152A50029396B00846373008463730084637B00846B
      840084637B00845A6B00845A6B00845A6B006B5A7B003152A50029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000021528C003973AD00CEE7F700BDDEEF00ADCEE7009CBDDE008CB5D6007BA5
      CE0000396B0029639C00295A940021529400F77B4A00F76B3100D65A08005294
      C6004A8CC6004A84BD00427BB5003973AD00184A84004A8CBD00427BB5000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6BDAD00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFF7F700B5A594008C9C
      FF000031FF000031FF000031FF00E7E7FF006B42310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300DE737300DE737300DE737300DE737300DE737300DE73
      7300DE737300DE737300DE737300DE737300DE737300DE737300DE737300EFA5
      A500EFA5A500E7A5A500E7A5A500E7A5A500E7A5A500E79C9C00DE7373000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B00BD735A00EF8C5200EF8C5A00EF8C5A00EF845200EF8C5A00EF84
      4A00EF7B4A00EF7B4A003152A50029396B00EF7B4A00EF8C5A00EF9C7300EF94
      6B00EF7B4A00EF733900EF6B3900EF6B3900B56352003152A50029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000021528C003973AD00CEE7F700BDDEEF00ADCEE7009CBDDE008CB5
      D6007BA5CE0000396B002152940018528C00F77B4A00F7632900D66B10005A9C
      CE005294C6004A8CC6004A84BD00427BB500184A84004A84BD00427BB5000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6BDAD00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700B5A594000031
      FF000031FF00D6DEFF000031FF007394FF006B42310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000DE737300EFBDBD00EFBDBD00EFBDBD00EFBDBD00EFB5
      B500EFB5B500EFB5B500EFB5B500EFADAD00EFADAD00EFADAD00EFADAD00DE73
      7300DE737300E7A5A500E7A5A500E7A5A500E7A5A500E79C9C00DE7373000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B00B5735200EF844A00EF844A00EF7B4A00EF7B4A00EF7B4A00EF7B
      4A00EF7B4A00EF7B4A003152A50029396B00EF947300EFA58C00EF9C7B00EF8C
      6300EF7B4A00E76B3900E76B3900E76B3900B5634A003152A50029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000021528C003973AD00CEE7F700BDDEEF00ADCEE7009CBD
      DE008CB5D6007BA5CE0000396B00184A8400EF734200F75A2100D663080063A5
      D6005A9CCE005294C6004A8CC6004A84BD00184A8400427BB5003973AD000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D6BDAD00FFFF
      FF00DECEC600D6BDAD00D6BDAD00D6BDAD00D6BDAD00D6BDAD00B5A59400E7EF
      FF007394FF00FFFFFF00E7E7FF000031FF009CADFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DE737300DE737300EFBDBD00EFB5B500EFB5
      B500EFB5B500EFB5B500EFB5B500ADADAD00ADADAD00EFADAD00EFADAD00EFA5
      A500E7A5A500DE737300DE737300E7A5A500E79C9C00E79C9C00DE7373000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B00B5735A00EF845200EF7B4A00EF7B4A00EF7B4A00EF7B4A00EF7B
      4A00EF7B4A00EF8452003152A50029396B00EFA58400EFAD8C00EFAD8C00EF94
      6B00EF7B5200E76B3900E76B3900E7633100B55A4A003152A50029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000021528C003973AD00CEE7F700BDDEEF00ADCE
      E7009CBDDE008CB5D6007BA5CE0000396B00EF6B4200F7521800D663080063A5
      D60063A5D6005A9CCE005294C6004A8CC600184A84003973AD003973AD000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D6BDAD00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00B5A59400B5A5
      9400B5A59400B5A59400B5A594009CADFF000031FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DE737300DE737300EFB5
      B500EFB5B500EFB5B500EFADAD007B7B7B00ADADAD007B7B7B00EFA5A500E7A5
      A500E7A5A500E7A5A500E7A5A500DE737300DE737300E79C9C00DE7373000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B00B58C7B00EFB59400EFAD8400EF8C6300EF8C5A00EF7B4A00EF7B
      5200EF845A00EF8C63003152A50029396B00EFB59400EFBD9C00EFAD8C00EF8C
      6B00E77B4A00E7633100E7734200E76B3900AD635A003152A50029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000021528C003973AD00CEE7F700BDD6
      EF00A5CEE7009CBDDE008CB5D6007BA5CE005A424A00F7521800DE5A080063A5
      D60063A5D60063A5D6005A9CCE005294C600184A8400316BA500316BA5000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D6BD
      AD00D6BDAD00D6BDAD00D6BDAD00D6BDAD00D6BDAD00D6BDAD00D6BDAD00D6BD
      AD00D6BDAD00D6BDAD00D6BDAD00D6BDAD009CADFF000031FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE737300DE7373007B7B7B00ADADAD007B7B7B00DE737300DE73
      7300DE737300DE737300DE737300DE737300DE737300DE737300DE7373000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B0094949C00BDADAD00B5847300B5735A00B56B5A00B5735A00B573
      6300B5847300B58473003152A50029396B00B58C8400B5847300B57B6B00B573
      6B00AD5A4A00AD5A4200AD6B5A00B57B7300947B8C003152A50029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000021528C003973AD00CEE7
      F700BDD6EF00ADCEE7009CBDDE008CB5D6007BA5CE000839730000396B000039
      6B0000396B0000396B0000396B0000396B0000396B0029639C0029639C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009CADFF000031FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000007B7B7B00ADADAD007B7B7B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003152
      A50029396B0029396B0029396B0029396B0029396B0029396B0029396B002939
      6B0029396B0029396B0029396B0029396B0029396B0029396B0029396B002939
      6B0029396B0029396B0029396B0029396B0029396B0029396B0029396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000021528C003973
      AD00CEE7F700BDD6EF00ADCEE7008CB5D6008CB5D6007BA5CE00BDD6EF00ADCE
      E7008CB5D6007BA5CE007BA5CE007BA5CE007BA5CE0000396B0029639C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009CADFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000007B7B7B00ADADAD007B7B7B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003152
      A5003152A5003152A5003152A5003152A5003152A5003152A5003152A5003152
      A5003152A5003152A5003152A5003152A5003152A5003152A5003152A5003152
      A5003152A5003152A5003152A5003152A5003152A5003152A5003152A5000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002152
      8C003973AD00CEE7F700BDD6EF00ADCEE7009CBDDE008CB5D6007BA5CE00BDD6
      EF00ADCEE7008CB5D6007BA5CE007BA5CE007BA5CE007BA5CE0000396B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000007B7B7B00ADADAD007B7B7B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009494940094948C008C8C8C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000031313100293131000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009463630094636300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6
      D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6
      D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009494
      94009C9C9400CECECE00ADADAD009C9494008C8C8C008C8C8C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000AD7B7B00B5848400B5848400B5848400B584
      8400B5848400B5848400B5848400B5848400B5848400B5848400B5848400B584
      8400B5848400B5848400B5848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000029313100105A6B00006B94002931310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094636300DEADAD00E7C6C600946363000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000094949400ADA5
      A500F7F7F700F7F7F700E7E7E700E7E7E700CECECE00ADADAD00949494008484
      8400848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000AD738400F7DEC600FFE7C600F7DEBD00F7DE
      B500F7D6AD00F7D6A500F7D6A500F7CE9C00F7CE9C00F7CE9C00F7CE9C00F7CE
      9C00F7CE9C00EFCE9400BD848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000010526B00398CAD000884B5000094CE00006B940029313100000000000000
      0000000000000000000000000000000000000000000000000000000000009463
      6300DEADAD00E7C6C600F7DEDE00FFFFFF009463630000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D60010423100104231001042310010423100104231001042
      3100104231001042310010423100104231001042310010423100104231001042
      3100104231001042310010423100104231001042310010423100D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094949400ADADAD00FFFF
      FF00FFFFFF00F7F7F700EFEFEF00E7E7E700E7E7E700DEDEDE003184AD00186B
      94004A738C006B7B840084848400848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000AD738400F7DECE00F7E7CE00008400000084
      000000840000008400000084000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF00EFCE9400BD848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000006B94000094CE0063CEFF0031ADD6000094CE00006B9400293131000000
      000000000000000000000000000000000000000000000000000094636300DEAD
      AD00E7C6C600F7DEDE00FFFFFF00F7DEDE009463630000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D600104A3100104A3900104A3900104A3900104A3100104A
      3100104A390010523900104A3900104A3900104A3900104A3100104A3100104A
      3100104231001042310010423100104229001042290010422900D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C949400BDBDBD00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00EFEFEF00EFEFEF00E7E7E700E7E7E7003984AD000063
      9C0000639C0000639C0094ADB500ADADAD008C8C8C007B7B7B007B7B7B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000AD738400F7E7CE00FFE7CE00008400008484
      840084848400848484008484840000FFFF0000FFFF0000FFFF000000B50000FF
      FF0000FFFF00EFCE9400BD848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000006B94000094CE0063CEFF0031ADD6000094CE00006B94002931
      3100000000000000000000000000000000000000000094636300DEADAD00E7C6
      C600F7DEDE00FFFFFF00F7DEDE00D68484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D6001831290018312900183929001839310018423100184A
      3900104A3900104A390010523900105239001852390018524200185239001852
      3900184A3900184A3900184A39001842310018423100184A3100D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C9C9C00BDBDBD00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00F7F7F700EFEFEF00EFEFEF00E7E7E7003984AD000063
      9C0000639C0000639C009CB5C600CECECE00CECECE00C6C6C6007B7B7B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000AD738400F7E7D600FFEFD600008400000084
      000000840000008400000084000000FFFF0000FFFF000000B50000FFFF000000
      B50000FFFF00EFCE9400BD848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000006B94000094CE0063CEFF0031ADD6000094CE00006B
      94002931310000000000000000000000000094636300DEADAD00E7C6C600F7DE
      DE00FFFFFF00F7DEDE00D6848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D600104231001042310010423100104A3900104A39001052
      390010523900105A3900105A420010634200106B4A001873520018735200187B
      5A00187B5A00187B63001884630021846300187B5A0021735A00D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C9C9C00DEDEDE00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00F7F7F700F7F7F700EFEFEF00EFEFEF003984AD000063
      9C0000639C0000639C009CBDC600D6D6D600CECECE00CECECE007B7B7B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000AD738400F7E7DE00FFEFDE00008400008484
      840000840000008400008484840000FFFF0000FFFF0000FFFF000000B50000FF
      FF0000FFFF00EFCE9400BD848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000006B94000094CE0063CEFF0031ADD6000094
      CE00006B9400293131000000000094636300DEADAD00E7C6C600F7DEDE00FFFF
      FF00F7DEDE00D684840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D6001073520010735200107B5A00107B5A00108463001084
      6300188C6300188C6B00188C6B0018946B0018946B0021946B00219C73002194
      7300218C6B00188C630018845A00187B52001873520021735200D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C9C9C00C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00F7F7F700F7F7F700EFEFEF00398CAD000063
      9C0000639C0000639C00A5BDCE00D6D6D600D6D6D600CECECE007B7B7B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000AD7B8400F7EFDE00FFEFE700008400000084
      000084848400848484000084000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF00EFCE9400BD848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000006B94000094CE0063CEFF0031AD
      D6000094CE00006B940094636300DEADAD00E7C6C600F7DEDE00FFFFFF00F7DE
      DE00D68484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D600109C7300109C7300109C7300189C7300189C7300189C
      7300189C73001894730018947300218C6B00218C6B0021845A00187B5A00186B
      4A00186B4A00185A420018523900184A3100184A3100294A3900D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C9C9C00CECECE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7F700F7F7F700398CB5000063
      9C0000639C0000639C00A5C6CE00DEDEDE00D6D6D600D6D6D6007B7B7B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B5848400F7EFEF00FFF7EF00FFFF0000FF00
      0000FF000000FF000000FF0000000000FF000000FF000000FF000000FF000000
      FF000000FF00EFCE9400BD848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000006B94000094CE0063CE
      FF0031ADD60094636300DEADAD00E7C6C600F7DEDE00FFFFFF00F7DEDE00D684
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D60021AD840021A5840021A57B002194730021947300218C
      6B00218463002184630021735A00216B52002163420021523900295242004263
      520063736300848C8400A59C8C00B59C9400C6948400CE948400D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C9C9C00CECECE00FFFFFF00FFFFFF00FFFFFF00EFF7F700C6D6
      DE00C6D6DE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7F700398CB5000063
      9C0000639C0000639C00ADC6D600DEDEDE00DEDEDE00D6D6D6007B7B7B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BD848C00FFF7EF00FFFFF700FFFF0000FF00
      0000FF000000FF000000FF0000000000FF000000FF000000FF0094B5FF000000
      FF000000FF00EFCE9C00BD848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000006B94000094
      CE0094636300DEADAD00E7C6C600F7DEDE00FFFFFF00F7DEDE00D68484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D6009CBD9C0084B58C0073AD84006BA57B00639473006B9C
      7B007B9C840084846B009C8C7300B59C8400CE9C7B00E7BDAD00F7D6CE00F7CE
      BD00F7BDA500F7BDAD00F7C6B500F7C6AD00F7C6AD00F7CEBD00D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C9C9C00CECECE00FFFFFF00FFFFFF00C6D6DE00216B8C000063
      8C0000638C006394AD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00398CB5000063
      9C0000639C0000639C00ADC6D600E7E7E700DEDEDE00DEDEDE007B7B7B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BD848C00FFF7F700FFFFFF00FFFF0000FFFF
      0000FF000000FF000000FF0000000000FF0094B5FF000000FF0094B5FF000000
      FF000000FF00EFCE9C00BD848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009463
      6300DEADAD00E7C6C600F7DEDE00FFFFFF00F7DEDE00D6848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D600F7C69C00F7BD9400F7BD9400F7B58C00F7AD8C00F7AD
      8400F7AD8400F7A57B00F7A57B00F79C7300F79C7B00F7AD8C00EFAD8C00F7A5
      8400F7AD8C00F7B59C00F7BD9C00F7C6B500F7BDA500F7AD8C00D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C9C9C00CECECE00EFF7F700739CB500005A8400008CBD0000A5
      D600007BAD00006B9C006394AD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00DEE7
      EF009CBDD6006BA5BD00BDD6DE00E7E7E700E7E7E700DEDEDE007B7B7B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C68C8C00FFFFF700FFFFFF00FFFF0000FFFF
      0000FFFF0000FF000000FF0000000000FF0094B5FF000000FF0094B5FF000000
      FF000000FF00EFCEA500BD848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D6848400946363009463630094636300DEAD
      AD00E7C6C600F7DEDE00FFFFFF00F7DEDE00D6848400006B9400293131000000
      0000000000000000000000000000D68484009463630000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D600F7BD8C00F7B58C00F7B58C00F7B58C00F7A57B00F7A5
      7300F79C7300F79C6B00F79C7300F7C6AD00F7B59400F7AD8C00F7B59400F7DE
      D600F7BDAD00F7F7F700F7EFE700F7C6AD00F7B59400F7946B00D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C9C9C00CECECE00216B8C00006B940000A5D60000CEFF0000C6
      F70000A5D600007BAD00006B94006394AD00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7F7F700F7F7F700EFEFEF00EFEFEF00E7E7E700E7E7E7007B7B7B00105A
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C68C8C00FFFFF700FFFFFF00FFFF0000FFE7
      CE00FFFF0000FFFF0000FF0000000000FF0094B5FF000000FF000000FF000000
      FF000000FF00F7D6AD00BD848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000094636300EFD6D600E7C6C600E7C6C600DEADAD00E7C6
      C600F7DEDE00FFFFFF00F7DEDE00D684840031ADD6000094CE00006B94002931
      3100000000000000000000000000D6848400F7DEDE0094636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D600F7AD8400F7C69C00F7DECE00F7B59400F79C6B00F79C
      6300F7946300F7946300F7A58400F7CEB500F7AD8400F7845200EF844A00F79C
      7300F7BDA500F7C6B500F7B59400F78C5A00F7844A00F78C5A00D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C9C9C00005284000084B50000BDEF0000CEFF0000CEFF0000C6
      F70000BDF70000A5D600007BAD0000638C006394AD00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00F7F7F700F7F7F700EFEFEF00EFEFEF009CB5C600105A8400006B
      9C00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CE948C00FFFFF700FFFFFF00FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF00000000FF000000FF000000FF000000FF000000
      FF000000FF00F7D6AD00B5848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6848400EFD6D600F7EFEF00F7E7E700EFD6D600EFCECE00F7DE
      DE00FFFFFF00F7DEDE00D68484000094CE0063CEFF000094C6000094CE001842
      5200636363000000000000000000D6848400F7DEDE00F7DEDE00946363000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D600F7A57300F7A57B00F79C6B00F7946300F7946300F78C
      5A00F78C5200F7844A00F77B4A00EF7B4200F77B4200EF7B4200EF733900EF73
      3900F7AD8C00F7AD8C00F77B3900EF733100F7946300F78C6300D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000638C0000A5D60000CEFF0000CEFF0000CEFF0000CEFF0000C6
      F70000BDF70000BDEF0000A5D600007BAD0000638C006394AD00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00F7F7F700F7F7F7006B94AD00005284000073A5000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CE948C00FFFFF700FFFFFF00EFCE9400EFCE
      9400EFCE9400EFCE9400EFCE9400EFCE9400EFCE9400EFCE9400EFCE9400EFCE
      9400F7DEC600DEC6A500A57B8400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6848400FFFFFF00FFFFFF00FFF7F700F7E7E700EFDEDE00EFCE
      CE00F7DEDE00D684840000000000006B94000094CE00299CC600186373005A63
      6300D68484009463630094636300D6848400F7DEDE00FFFFFF00F7DEDE009463
      6300000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D600F7946300F7945A00F7945A00F7A57300F79C6B00F7A5
      7300EF7B4200F77B4200F7733900EF733900EF7B4200EF7B4A00EF734200EF6B
      2900EF6B2900EF6B2900EF6B2900EF6B2900EF8C6300F7A57B00D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084B50000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000C6
      F70000BDF70000BDEF0000B5EF00009CCE00007BAD0000638C006394AD00FFFF
      FF00FFFFFF00FFFFFF00DEE7EF00316B9400005A8400007BA500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D69C9400FFFFF700FFFFFF00E79C0000FFFF
      FF00E79C0000E79C0000E79C0000FFF7EF00E79C0000FFEFDE00FFEFDE00E7DE
      CE00CEBDAD00B5AD9C00A57B8400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6848400FFFFFF00FFFFFF0094636300FFF7F700F7E7E700EFDE
      DE00EFCECE00D68484000000000000000000006B940029637B006B737B00D684
      8400E7CECE00E7BDBD00DEADAD00DEADAD00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D600F78C5200F78C5200F7844A00F7844A00F77B4200EF7B
      3900F7733900F7733900EF733900EF7B4A00F79C7B00F7947300EF733900EF63
      2900EF632100EF632100EF632900EF734200EF6B2900EF632100D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084B50000BDEF0000CEFF0000CEFF0000CEFF0000CEFF0000C6
      F70000BDF70000BDEF0000B5EF0000B5E700009CCE00007BAD0000638C006394
      AD00FFFFFF00B5C6D600105A840000638C00007BAD0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DE9C9400FFFFF700FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFF7EF00FFF7EF00EFDED600B5847300AD84
      7300AD7B7300AD7B7300AD7B7300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6848400FFFFFF009463630000000000D6848400FFF7F700F7E7
      E700EFDEDE00D6848400000000000000000000000000006B9400D6848400F7DE
      DE00EFD6D600E7C6C600DEB5B500DEADAD00D684840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D600F7844A00F7844A00F77B4200EF7B4200EF733900EF73
      3900EF733900EF7B4200EF845200F7A58400F7AD9400F7A58400EF734200E763
      2900E75A2100E75A2900EFA58400F7CEBD00F7B59C00F7C6AD00D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084B50000BDEF0000CEFF0000CEFF0000CEFF0000C6
      F70000C6F70000BDEF0000B5EF0000B5E70000ADE700009CCE00007BAD000063
      8C0031739400004A7B00006B9C00007BAD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DEA59400FFFFFF00FFFFFF00E79C0000E79C
      0000E79C0000E79C0000E79C0000E79C0000FFFFF700E7CEC600B5847300E7B5
      8400E7AD6B00EFA53900BD848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6848400D68484000000000000000000D6848400FFFFFF00FFF7
      F700F7E7E700D684840000000000000000000000000000000000D6848400F7EF
      EF00F7DEDE00EFCECE00E7C6C600DEB5B500CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D600F7BD9C00F7BDA500F7C6B500F79C7300EF845A00EF7B
      4200EF7B5200F78C6300F79C8400F7B5A500F7BDA500EF9C7B00EF6B4200E75A
      2900E76B3900E7734A00EF8C7300F7DED600F7CEBD00F7E7D600D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000084B50000C6FF0000CEFF0000CEFF0000C6
      F70000C6F70000BDEF0000B5EF0010BDEF0000ADE70000ADDE000094C600007B
      AD0000638C00006B9C00007BAD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7A59400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7D6CE00B5847300EFC6
      8C00F7BD6B00BD84840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D6848400FFFFFF00FFFFFF00FFFF
      FF00D68484000000000000000000000000000000000000000000D6848400FFF7
      F700F7EFEF00EFD6D600E7CECE00CE9C9C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D600F7BD9C00F79C8400F79C8400F79C8400F79C8400F79C
      8400F79C8400F79C8400F79C8400F79C8400F79C8400F79C8400F79C8400F79C
      8400F79C8400F79C8400F79C8400F79C8400F79C8400F79C8400D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000084B50000C6FF0000CEFF0000C6
      F70000C6F70000BDEF0029C6EF004ADEFF0000C6FF0000ADE70000A5D6000094
      C600007BAD00007BAD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7A59400FFFFFF00FFFFFF00E79C0000E79C
      0000FFFFFF00FFFFFF00E79C0000E79C0000FFFFFF00E7D6D600B5847300EFC6
      8C00F7BD6B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D6848400D6848400D6848400D684
      840000000000CE9C9C00D6848400946363009463630094636300F7DEDE00FFFF
      FF00FFF7F700F7E7E700CE9C9C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000084B50000C6F70000C6
      F70000C6F70000BDEF0031C6EF004ADEFF0000CEFF0000CEFF0000A5D6000084
      B500000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7A59400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EFDEDE00B5847300F7C6
      8C00BD8484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CE9C9C00E7C6C600F7DEDE00FFFFFF00FFFFFF00FFFF
      FF00CE9C9C00CE9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFE7D600FFE7D600FFE7D600FFE7D600FFE7D600FFE7D600FFE7
      D600FFE7D600FFE7D600FFE7D600FFE7D600FFE7D600FFE7D600FFE7D600FFE7
      D600FFE7D600FFE7D600FFE7D600FFE7D600FFE7D600FFE7D600FFE7D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000084B50000C6
      F70000C6F70000BDEF0031C6EF004ADEFF0000CEFF0000CEFF000084B5000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DEA58C00EFD6BD00EFD6C600EFD6C600EFD6
      C600EFCEC600E7CEBD00E7CEBD00DEC6BD00DEC6BD00CEADA500B5847300BD84
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      B50000C6F70000BDEF000084B50018739400006B94000094BD000084B5000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000084B5000084B500000000000073A5000063940000739C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000AD5A5A00A54A4A00A56B6B00BDBDBD00CED6CE00D6CECE00CECE
      CE00C6C6C600A56B6B0094393900A54A4A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A5A5A5009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9CA5009C9CA5009C9CA5009C9C
      A5009C9CA5009C9CA5009C9CA5009CA5A5009CA5A5009CA5A500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD737300D6636300CE5A5A00A56B6B00AD949400DEC6BD00FFF7F700FFFF
      FF00EFEFEF00B56B6B008C212100BD525200B55A5A0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A5A5A500F7F7F700F7F7F700F7F7F700F7F7
      F700EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00E7E7E700E7E7E700E7E7
      E700DEDEDE00DEDEDE00D6D6D600D6D6D600D6D6D6009CA5A500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000AD7B6300845252008452520073424200734242006B3131006B31
      31006B3131006B31310000000000000000000000000000000000AD7B63008452
      52008452520073424200734242006B3131006B3131006B3131006B3131000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000094423900944239009442390094423900B59C9C00B59C9C00B59C
      9C00B59C9C00B59C9C00B59C9C00B59C9C00B59C9C00B59C9C00B59C9C00B59C
      9C00944239009442390094423900944239000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD736B00CE636300CE636300AD7373009C525200AD5A5A00E7D6D600FFFF
      FF00FFF7F700BD7373008C212100B5525200B55A5A0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A5A5A500FFFFFF00F7F7F700F7F7F700F7F7
      F700F7F7F700F7F7F700EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00E7E7
      E700E7E7E700DEDEDE00DEDEDE00DEDEDE00D6D6D6009C9CA500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000AD7B6300E7CEBD00FFFFFF00EFDED600DEBDAD00CE9C8400BD84
      5A00945231006B31310000000000000000000000000000000000AD7B6300E7CE
      BD00FFFFFF00EFDED600DEBDAD00CE9C8400BD845A00945231006B3131000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094423900C66B6300D6636300CE636300C65A5A00A5737300EFE7E700D6CE
      CE00D6C6BD00EFDEDE00F7F7EF00F7F7EF00EFE7E700E7E7E700E7E7E700C67B
      7B00942929009C313100BD525200BD5A63009442390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD736B00CE636300C65A5A00B57B7B009C52520094212100BDA5A500F7FF
      F700FFFFFF00BD7B73008C181800B5525200B55A5A0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A5A5AD00FFFFFF00FFFFFF00F7F7F700F7F7
      F700F7F7F700F7F7F700F7F7F700EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00E7E7E700E7E7E700DEDEDE00DEDEDE00DEDEDE009C9CA500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000AD7B6300E7CEBD00FFFFFF00EFDED600DEBDAD00CE9C8400BD84
      5A00945231006B31310000000000000000000000000000000000AD7B6300E7CE
      BD00FFFFFF00EFDED600DEBDAD00CE9C8400BD845A00945231006B3131000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094423900C66B6B00D6636300D66B6B00CE636300A5525200EFE7E7009429
      290094292900BD6B6300F7EFEF00FFFFFF00FFFFF700F7FFF700F7FFF700C67B
      7B00942121009C313100BD525200C66363009442390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD736B00CE636300C65A5A00BD7B7B00B5848400A5636300A5949400CECE
      CE00F7EFEF00BD7373008C212100007308000073080000730800007308000073
      0800000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A5A5AD00FFFFFF00FFFFFF00FFFFFF00F7F7
      F700F7F7F700F7F7F700F7F7F700F7F7F700EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00E7E7E700E7E7E700DEDEDE00DEDEDE009C9CA500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000AD7B6300E7CEBD00FFFFFF00EFDED600DEBDAD00CE9C8400BD84
      5A00945231006B31310000000000000000000000000000000000AD7B6300E7CE
      BD00FFFFFF00EFDED600DEBDAD00CE9C8400BD845A00945231006B3131000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094423900C66B6B00D6636300CE6B6B00CE5A5A00A55A5A00EFE7E7009429
      290094292900C66B6B00EFDEDE00FFFFF700FFFFF700FFFFFF00FFFFFF00C67B
      7B00942121009C313100BD525200C66363009442390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD737300CE636300CE636300C6636300CE7B7300CE737300C66B6B00C66B
      6B00D67B7B00C663630000730800007308000884100008941800089418000894
      1000087B08000073080000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A5ADAD00FFFFFF00FFFFFF00DEDEDE00CECE
      CE00CECECE00BDBDBD00292929005A524A00BDBDBD00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00E7E7E700E7E7E700DEDEDE009C9CA500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000AD7B6300E7CEBD00FFFFFF00EFDED600DEBDAD00CE9C8400BD84
      5A00945231006B31310000000000000000000000000000000000AD7B6300E7CE
      BD00FFFFFF00EFDED600DEBDAD00CE9C8400BD845A00945231006B3131000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094423900C66B6B00D6636300CE6B6B00C65A5A00A55A5A00EFE7E7009429
      290094292900C66B6B00DECECE00F7EFEF00FFF7F700FFFFFF00FFFFFF00C67B
      7B00942121009C313100BD525200C66363009442390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD736B00BD525200BD6B6300CE8C8C00D69C9C00D69C9C00D69C9C00D694
      9400D6949400D69C9C00D69C9C00D6737300B55252000000000008841000089C
      180008A51800088C100000730800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A5ADAD00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00F7F7F7005A524A003139390052636300D6D6D600EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00E7E7E700E7E7E7009C9C9C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000AD7B6300E7CEBD00FFFFFF00EFDED600DEBDAD00CE9C8400BD84
      5A00945231006B31310000000000000000000000000000000000AD7B6300E7CE
      BD00FFFFFF00EFDED600DEBDAD00CE9C8400BD845A00945231006B3131000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094423900C66B6B00D6636300CE6B6B00C65A5A00A55A5A00EFE7E7009429
      290094292900C66B6B00C6B5B500DEDEDE00F7EFEF00FFFFFF00FFFFFF00C67B
      7B00942121009C313100BD525200C66363009442390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD6B6300BD636300EFDEDE00FFF7F700F7F7F700F7F7F700F7F7F700FFF7
      F700FFF7F700FFFFFF00EFDEDE00CE737300AD52520000000000000000000073
      0800089C180008AD180008841000007308000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A5ADAD00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00EFEFEF0052737B00428CDE0018526B00425A9400D6D6
      D600EFEFEF00EFEFEF00EFEFEF00EFEFEF00E7E7E7009C9C9C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000AD7B6300E7CEBD00FFFFFF00EFDED600DEBDAD00CE9C8400BD84
      5A00945231006B31310000000000000000000000000000000000AD7B6300E7CE
      BD00FFFFFF00EFDED600DEBDAD00CE9C8400BD845A00945231006B3131000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094423900C66B6B00D6636300CE6B6B00C65A5A00AD5A5A00EFE7E7009429
      290094292900C66B6B00A59C9C00BDC6BD00DEDEDE00FFFFFF00FFFFFF00C67B
      7B008C2121009C313100BD525200C66363009442390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD6B6300BD6B6B00F7F7EF00F7F7F700F7EFEF00F7EFEF00F7EFEF00F7EF
      EF00F7EFEF00F7F7F700EFDEDE00CE6B6B00AD525200B5848400B5848400B584
      84000884100010AD2100089C180000730800B5848400B5848400B5848400B584
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A5ADAD00FFFFFF00FFFFFF00DEDEDE00CECE
      CE00CECECE00CECECE00CECECE0052737B00297B9C00427BD6004273CE00425A
      9400BDBDBD00EFEFEF00EFEFEF00EFEFEF00EFEFEF009C9C9C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000AD7B6300E7CEBD00FFFFFF00EFDED600DEBDAD00CE9C8400BD84
      5A00945231006B31310000000000000000000000000000000000AD7B6300E7CE
      BD00FFFFFF00EFDED600DEBDAD00CE9C8400BD845A00945231006B3131000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094423900C66B6B00D6636300CE636300C65A5A00B55A5A00EFE7E700EFE7
      E700EFE7E700EFE7E700EFE7E700EFE7E700EFE7E700EFE7E700EFE7E700C67B
      7B009C313100A5393900BD525200C66363009442390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD6B6300C66B6B00F7EFEF00E7E7E700D6D6D600DED6D600DED6D600DED6
      D600D6D6D600E7E7E700EFDED600CE6B6B00AD525200F7E7C600F7DEBD00F7DE
      B5000873080018AD290010AD210000730800F7CE9C00F7CE9C00F7D69C00B584
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00427BD6006BADEF00428CDE004273
      CE00425A9400EFEFEF00EFEFEF00EFEFEF00EFEFEF009C9C9C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000AD7B6300AD7B6300B5949400BD949400AD8C8C00AD7B84009463
      63008C5252006B3131006B31310000000000000000006B313100844A4A00B594
      9400B5949400B58C8C00AD8484009C73730094636300844A4A006B3131000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094423900C66B6B00D6636300CE636300CE636300C6636300CE737300DE94
      9400DE949400DE8C8C00DE8C8C00D6848400D6848400D6848400CE737300C65A
      5A00C65A5A00C65A5A00CE636300C66363009442390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD6B6300BD6B6B00F7EFEF00EFEFEF00E7DEDE00E7DEDE00E7DEDE00E7DE
      DE00E7DEDE00EFEFEF00EFDEDE00CE6B6B00AD525200F7E7CE00F7DEC600F7DE
      BD000073080021AD390021BD390000730800EFCE9400EFCE9400F7D69C00B584
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00427BD600ADD6F7006BADEF00428C
      DE004273CE00425A9400EFEFEF00EFEFEF00EFEFEF009C9C9C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000AD7B6300E7CEBD00FFFFFF00EFDED600DEBDAD00CE9C
      8400BD845A00945231006B2929006B3131006B2929006B313100E7CEBD00FFFF
      FF00EFDED600DEBDAD00CE9C8400BD845A00945231006B313100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094423900D6636300D6636300C65A5A00C6636300CE6B6B00CE6B6B00CE6B
      6300CE6B6B00CE6B6B00CE6B6B00CE6B6300CE6B6300CE636300CE6B6B00CE73
      7300CE737300CE737300CE6B6B00C65A63009442390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD6B6300C66B6B00F7EFEF00E7E7E700DED6D600DEDEDE00DEDEDE00DEDE
      DE00DED6D600E7E7E700EFDED600CE736B00AD52520000730800007308000073
      08000873100031BD4A0029C64A0000730800007308000073080000730800B584
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD00FFFFFF00FFFFFF00DEDEDE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00427BD600ADD6F7006BAD
      EF00428CDE004273CE00425A9400EFEFEF00EFEFEF009C9C9C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000AD7B6300E7CEBD00FFFFFF00EFDED600DEBDAD00CE9C
      8400BD845A00945231006B31310084525200632121006B313100E7CEBD00FFFF
      FF00EFDED600DEBDAD00CE9C8400BD845A00945231006B313100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094423900D6636300D6636300D6A5A500D6A5A500D6A5A500DEA5A500DEA5
      A500DEA5A500DEA5A500DEA5A500DEA5A500DEA5A500DEA5A500DEA5A500DEA5
      A500DEA5A500D6A5A500CE737300D66363009442390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD6B6300BD6B6B00F7EFEF00FFF7F700F7EFEF00F7EFEF00F7EFEF00F7EF
      EF00F7EFEF00FFF7F700EFDEDE00CE736B00AD5252000073080021AD310031AD
      520039BD5A0042CE6B0039C65A0029BD4A0021AD310021AD310000730800B584
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00427BD600ADD6
      F7006BADEF00428CDE004273CE00425A9400EFEFEF009C9C9C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000AD7B6300E7CEBD00EFDED600DEBDAD00CE9C
      8400BD845A00945231006B3131009C6B6B00845252006B313100E7CEBD00FFFF
      FF00EFDED600DEBDAD00CE9C840094523100945231006B313100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094423900D6636300D6636300E7C6C600FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00E7C6C600C6636300D66363009442390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B5636300D6CECE00DEDEDE00DED6D600DED6D600DED6D600DED6
      D600DED6D600DEDEDE00D6C6C600AD636300FFFFF700FFEFE7000073080029AD
      4A005AEF940052DE84004AD66B0021AD310021AD390000730800F7D69C00B584
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00427B
      D600ADD6F7006BADEF00428CDE004273CE008C8C8C009C9C9C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000AD7B6300AD7B6300BD9C9C00B5949400A57B
      7B008C5A5A00844A4A006B313100A57B7B009C6B6B006B313100A57B7B00C6A5
      A500BD9C9C00AD84840094636300845252006B31310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094423900D6636300D6636300E7CEC600FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00E7C6C600C6636300D66363009442390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BD8C8400FFFFFF00FFF7EF00FFEFE700006B
      000031AD52005AEF94005AE78C0039BD5A0008730800F7D6AD00F7D6A500B584
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD00FFFFFF00FFFFFF00DEDEDE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00427BD600ADD6F7004273CE00FFD6A500FFCE94008C8C8C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AD7B6300E7CEBD00FFFFFF00E7CE
      C600C6947B00945231006B3131006B3131006B31310063292900E7CEBD00FFFF
      FF00E7CEC600C6947B00945231006B3131000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094423900D6636300D6636300E7C6C600FFFFFF00FFFFFF00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00FFFF
      FF00FFFFFF00E7C6C600C6636300D66363009442390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CE9C8400FFFFFF00FFFFF700FFF7EF00FFEF
      E7000073080039BD63004AD67B0010841800F7DEBD00F7DEB500F7DEAD00B584
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00427BD600FFF7E700FFDEBD00FFD6A5000018AD0000009C000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AD7B6300E7CEBD00FFFFFF00E7CE
      C600C6947B00945231006B3131000000000000000000AD7B6300E7CEBD00FFFF
      FF00E7CEC600C6947B00945231006B3131000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094423900D6636300D6636300E7C6C600FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00E7C6C600C6636300D66363009442390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CE9C8400FFFFFF00FFFFFF00FFFFF700FFF7
      EF00FFEFE700087B100010841800F7E7CE00F7DEC600F7DEBD00FFDEB500B584
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CECECE00A5A5A500FFF7E7000831CE003152F7003152F7000018
      C600000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AD7B6300AD7B6300845252007342
      42006B3131006B3131006B3131000000000000000000AD7B6300AD7B63007B4A
      4A00734242006B3131006B3131006B3131000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094423900D6636300D6636300E7C6C600FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00E7C6C600C6636300D66363009442390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DEAD8400FFFFFF00FFFFFF00FFFFFF00FFFF
      F700FFF7EF00FFEFE700FFEFDE00FFE7D600F7E7CE00F7E7C600F7DEB500B584
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD00FFFFFF00FFFFFF00DEDEDE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00FFFFFF00FFFF
      FF00A5ADAD0094949C00949C9C000018C6007B94FF00526BF7000018C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD7B6300DED6D600D6BD
      BD00B58C94006B31310000000000000000000000000000000000AD7B6300DED6
      D600D6BDBD00AD8484006B313100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094423900D6636300D6636300E7C6C600FFFFFF00FFFFFF00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00FFFF
      FF00FFFFFF00E7C6C600C6636300D66363009442390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DEAD8400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFF700FFF7EF00FFEFE700FFEFDE00FFEFDE00E7DEC600C6BDAD00B584
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00ADADAD00FFFFFF00FFFFFF00FFFFFF000029E7000029E700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD7B6300DED6D600D6BD
      BD00B59494006B31310000000000000000000000000000000000AD7B6300DED6
      D600D6BDBD00AD8484006B313100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094423900D6636300D6636300E7C6C600FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00E7C6C600C6636300D66363009442390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7B58C00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFF700FFF7EF00F7E7D600BD847B00BD847B00BD847B00B584
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00ADADAD00FFFFFF00FFFFFF00DEDEDE009C9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD7B63009C6B6B009C6B
      6B009C6B6B006B31310000000000000000000000000000000000AD7B63009C6B
      6B009C6B6B009C6B6B006B313100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094423900D6636300D6636300E7C6C600FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00E7C6C600C6636300D66363009442390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7B58C00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00E7CECE00BD847B00EFB57300EFA54A00C684
      6B00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00ADADAD00FFFFFF00DEDEDE009C9C9C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094423900D6636300D6636300E7C6C600FFFFFF00FFFFFF00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00FFFF
      FF00FFFFFF00E7C6C600C6636300D66363009442390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000EFBD9400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00E7D6D600BD847B00FFC67300CE9473000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00ADADAD00DEDEDE009C9C9C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094423900D6636300D6636300DEC6C600FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00E7CEC600D6636300D66363009442390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000EFBD9400FFF7F700FFF7F700FFF7F700FFF7
      F700FFF7F700FFF7F700FFF7F700E7D6CE00BD847B00CE9C8400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009442390094423900CEB5B500FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00E7CEC60094423900944239000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000EFBD9400DEAD8400DEAD8400DEAD8400DEAD
      8400DEAD8400DEAD8400DEAD8400DEAD8400BD847B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C6
      C600ADADAD00AD9C9C00ADA59C00AD9C9400A5949400AD9C9C00ADADAD00C6C6
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C6363006B31310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000AD7B7B00B5848400B5848400B5848400B584
      8400B5848400B5848400B5848400B5848400B5848400B5848400B5848400B584
      8400B5848400B5848400B5848400B5848400B5848400B5848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000219CCE0021A5D60029ADD6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600ADADAD00E7D6
      CE00F7E7DE00F7EFE700EFEFEF00EFEFEF00EFEFEF00EFE7E700EFDED600BDAD
      A5009C9C9C00ADADAD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009C6363009C636300A5636300A56363006B31310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000AD738400F7DEC600FFE7C600F7DEBD00F7DE
      BD00F7DEB500F7D6AD00F7D6A500F7D6A500F7CE9C00F7CE9C00F7CE9C00F7CE
      9C00F7CE9C00F7CE9C00F7CE9C00F7CE9C00EFCE9400BD848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000109CD60063BDDE0039B5D600219CCE00219CCE00219CCE00219C
      CE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000ADADAD00E7D6CE00F7EFEF00EFEF
      EF00EFEFEF00E7DEDE00E7E7E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEF
      EF00EFE7E700B5A59C008C847B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C6363009C636300A563
      6300B5636300C6636300CE636300B56363006B31310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000AD738400F7DECE00F7E7CE00F7DEC600F7DE
      BD00F7DEB500F7D6B500F7D6AD00F7D6A500F7CE9C00EFCE9C00EFCE9400EFCE
      9400EFCE9400EFCE9400EFCE9400EFCE9400EFCE9400BD848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000219CCE00BDDEE700B5F7F70084F7FF006BD6E7006BC6DE0039B5
      D60029A5C600109CD600219CCE00219CCE00219CCE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C6C600EFE7E700EFEFEF00EFEFEF00E7DE
      DE00DE8C5200BD5A2900BD5A2900BD5A2900BD5A2900BD5A2900DECEC600E7E7
      E700EFEFEF00F7EFEF00CEBDB5008C847B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C6363009C636300AD6B6B00C66B6B00CE6B
      6B00CE6B6B00CE636B00CE636B00B56363006B313100AD6363009C6363009C63
      63009C6363009C6363009C6363009C6363009C6363009C636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000AD738400F7E7CE00FFE7CE00F7E7CE00F7DE
      C600F7DEBD00F7DEB500F7D6B500F7D6AD00F7D6A500F7CEA500EFCE9C00EFCE
      9400EFCE9400EFCE9400EFCE9400EFCE9400EFCE9400BD848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000219CCE0063BDDE00D6FFF7008CFFFF009CFFFF009CFFFF009CFF
      FF0094F7F70094E7EF006BD6E7004AB5D60029A5C600219CCE00219CCE00219C
      CE00219CCE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600F7EFE700EFEFEF00EFEFEF00DE8C5200BD5A
      2900BD5A2900BD633900C6947B00DEBDA500C6947B00BD6331009C4A2100BD5A
      2900E7D6CE00EFEFEF00F7EFEF00C6BDB5008C847B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C636300D66B7300D66B7300D66B6B00D66B
      6B00D66B6B00D66B6B00D66B6B00B5636B006B313100FFA5A500FFADAD00FFAD
      B500FFB5B500FFBDBD00FFC6C600FFC6C600FFC6C6009C636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000AD738400F7E7D600FFEFD600F7E7CE00F7E7
      C600F7DEC600F7DEBD00F7DEB500F7D6B500F7D6AD00F7D6A500F7CEA500EFCE
      9C00EFCE9400EFCE9400EFCE9400EFCE9400EFCE9400BD848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000219CCE0031A5C600D6FFFF009CFFFF009CFFFF0094F7FF0094F7
      FF008CF7FF008CF7FF0094F7FF0094F7FF0094F7FF0094E7EF0073D6E7006BC6
      DE0031A5C60029A5C600109CD600219CCE000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DECEC600F7E7DE00F7F7F700EFEFEF00DE8C5200BD5A2900C66B
      3900C66B3900C66B3900E7C6B500FFFFFF00E7C6B500BD633100BD633100BD5A
      31009C4A2100DECEC600EFEFEF00F7EFEF00C6BDB500A59C9400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C636300DE737300DE737300D6737300D673
      7300D66B7300D66B6B00D66B6B00B56B6B006B3131005AB5630031CE630031CE
      630031CE630031CE630031CE630031CE6300FFC6C6009C636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000AD738400F7E7DE00FFEFDE00FFE7D600F7E7
      CE00F7E7C600F7DEC600F7DEBD00F7DEB500F7D6B500F7D6AD00F7D6A500F7CE
      A500EFCE9C00EFCE9400EFCE9400EFCE9400EFCE9400BD848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000219CCE0031A5C6009CDEEF00BDFFFF008CF7FF009CFFFF0094F7
      FF0094F7FF0094F7FF0094F7FF0094F7FF0094F7FF008CFFFF008CFFFF009CFF
      FF0094F7FF0094EFEF0063C6E700219CCE000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C6C600F7F7F700EFEFEF00DE8C5200BD5A2900C66B4200C66B
      4200C66B4200C66B3900DEAD9400F7F7F700DEAD9400BD633900BD633100BD63
      3100BD5A31009C4A2100E7DED600EFEFEF00F7E7DE008C847B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C636300DE737300DE737300DE737300DE73
      7300DE737300D6737300D6737300BD6B6B006B3131005AB5630031CE630031CE
      630031CE630031CE630031CE630031CE6300FFC6C6009C636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000AD7B8400F7EFDE00FFEFE700FFEFDE00F7E7
      D600F7E7CE00F7E7CE00F7DEC600F7DEBD00F7DEB500F7D6B500F7D6AD00F7D6
      A500F7CEA500EFCE9C00EFCE9400EFCE9400EFCE9400BD848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000736B6300B584730073C6DE00299CC600CEEFEF0094E7EF0094E7EF0094E7
      EF009CE7EF009CE7EF009CE7EF0084F7FF0094EFEF008CFFFF0094EFEF0084EF
      FF0084F7FF0084F7FF0073E7F70042BDDE00109CD60000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DECEC600F7E7E700F7F7F700E7D6CE00BD5A2900CE734200CE734200CE73
      4200CE734200CE734200CE734200CE734200C66B3900C66B3900C66B3900BD63
      3100BD633100BD5A3100BD5A2900EFEFEF00F7F7F700C6BDB5009C9C9C000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C636300DE7B7B00DE7B7B00DE7B7B00DE73
      7B00DE737300DE737300DE737300BD6B6B006B3131005AB5630031CE630031CE
      630031CE630031CE630031CE630031CE6300FFC6C6009C636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B57B8400F7EFE700FFF7E700FFEFDE00FFEF
      D600F7E7D600F7E7CE00F7E7C600F7DEC600F7DEBD00F7DEB500F7D6B500F7D6
      AD00F7D6A500F7CEA500EFCE9C00EFCE9400EFCE9400BD848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B5848400F7E7C6009CDEEF0031A5C600CEEFEF00A5F7F7009CF7EF009CF7
      EF009CF7EF009CF7EF0094E7EF0094F7FF008CFFFF008CF7FF0084EFFF008CFF
      FF008CEFFF008CF7FF0073DEFF0073DEF700219CCE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D6CECE00F7F7F700F7F7F700DE845200CE734200CE734200CE734200CE73
      4200CE734200CE734200CE846300DE946B00C66B4200C66B3900C66B3900BD63
      3900BD633100BD6331009C4A2100E7B59400F7F7F700EFDED6008C847B000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C636300E77B7B00E77B7B00E77B7B00E77B
      7B00DE7B7B00DE7B7B00DE737B00BD6B6B006B3131005AB5630031CE630031CE
      630031CE630031CE630031CE630031CE6300FFC6C6009C636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B5848400F7EFEF00FFF7EF00FFEFE700FFEF
      DE00FFEFDE00F7E7D600F7E7CE00F7E7C600F7DEC600F7DEBD00F7DEB500F7D6
      B500F7D6AD00F7D6A500F7CE9C00EFCE9C00EFCE9400BD848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B5847300FFEFD600F7E7C60063CEDE0031ADCE00BDFFF7009CF7EF009CF7
      EF009CF7EF009CF7EF0094E7EF008CF7FF0094EFFF008CF7FF008CF7FF008CEF
      FF008CEFFF008CEFFF006BDEFF008CEFFF0039B5D60000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DECEC600F7F7F700EFE7E700CE7B4200D67B4A00D67B4A00D67B4A00D67B
      4A00D67B4A00CE7B4200D6CECE00FFFFFF00DE9C7B00C66B4200C66B3900C66B
      3900BD633900BD633100BD5A3100C66B4200EFEFEF00F7EFE7008C847B000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C636300E7848400E77B8400E77B7B00E77B
      7B00E77B7B00E77B7B00E77B7B00BD7373006B3131005AB5630031CE630031CE
      630031CE630031CE630031CE630031CE6300FFC6C6009C636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BD848C00FFF7EF00FFFFF700FFF7EF00FFEF
      E700FFEFDE00FFEFDE00F7E7D600F7E7CE00F7E7C600F7DEC600F7DEBD00F7DE
      B500F7D6B500F7D6AD00F7D6A500F7D69C00EFCE9C00BD848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD8C8400FFF7E700F7E7D6006BD6E70031ADCE00D6FFF7009CF7EF009CF7
      EF00A5F7F7009CF7EF009CE7EF008CF7FF008CF7FF008CEFFF0094F7FF0084EF
      FF0094EFFF008CF7FF006BDEFF0084EFFF006BD6F700109CD600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DECEC600FFFFFF00E7CEC600D67B4A00D67B4A00D67B4A00D67B4A00D67B
      4A00D67B4A00D67B4A00CEC6C600FFFFFF00EFC6AD00CE734200C66B3900C66B
      3900C66B3900BD633100BD6331009C4A2100EFEFEF00F7F7EF00A59494000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C636300EF848400EF848400E7848400E784
      8400E7848C00EFA5A500E77B7B00BD7373006B31310073BD730031CE630031CE
      630031CE630031CE630031CE630031CE6300FFC6C6009C636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BD848C00FFF7F700FFFFFF00FFF7F700FFF7
      EF00FFF7E700FFEFDE00FFEFDE00F7E7D600F7E7CE00F7E7C600F7DEC600F7DE
      BD00F7DEB500F7D6B500F7D6AD00F7D6A500EFCE9C00BD848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CE9C8400FFFFEF00FFEFD6009CF7F70073CEE700D6EFF700B5FFF70094F7
      FF009CE7EF009CF7EF0094E7EF0094EFEF0084E7FF0084DEFF007BDEFF0084DE
      FF007BD6FF0084DEFF006BDEFF007BE7FF0094EFEF00219CCE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DECEC600FFFFFF00E7BDAD00D67B4A00D6844A00DE845200DE845200DE84
      5200D6844A00D67B4A00BD947B00EFEFEF00F7F7F700DEA58C00CE734200C66B
      3900C66B3900BD633100BD6331009C4A2100F7EFEF00F7F7F700AD9C94000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C636300EF848C00EF848400EF848400EF84
      8400F7BDBD00FFFFFF00F7B5B500C67373006B313100F7DEC600C6F7BD007BDE
      94007BDE940063D6840063D6840063D68400FFC6C6009C636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C68C8C00FFFFF700FFFFFF00FFFFF700FFF7
      EF00FFF7EF00FFF7E700FFEFDE00FFEFDE00FFE7D600F7E7CE00F7E7C600F7DE
      C600F7DEBD00F7DEB500F7D6B500F7D6AD00EFCEA500BD848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CE9C8400FFFFFF00FFF7E700FFEFD6005AC6DE0039ADD600D6EFF700D6F7
      F700D6F7F700C6F7F700BDEFEF0094E7FF0084DEFF0084DEF70084DEF70084DE
      F70084DEF7007BDEFF0063C6FF0073DEFF009CFFFF0042BDDE00109CD6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DECEC600FFFFFF00EFC6AD00DE845200DE8C5200E78C5A00E78C5A00E78C
      5A00DE8C5200DE845200D67B4A00C6A59400F7F7EF00F7F7F700E7A58400C66B
      3900C66B3900BD633900BD6331009C4A2100F7F7EF00FFF7F700ADA59C000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C636300F78C8C00EF8C8C00EF8C8C00EF84
      8C00F7BDBD00FFFFFF00F7ADAD00C67373006B313100F7DEC600FFFFDE00FFFF
      DE00FFFFDE00FFFFDE00E7FFCE00E7FFCE00FFC6C6009C636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C68C8C00FFFFF700FFFFFF00FFFFFF00FFFF
      F700FFF7EF00FFF7EF00FFEFE700FFEFDE00FFEFD600FFE7D600F7E7CE00F7E7
      C600F7DEC600F7DEBD00F7DEB500F7D6B500F7D6AD00BD848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DEAD8400FFFFFF00FFF7F700FFF7E700FFEFD60073D6E7004AB5D60031AD
      CE0031ADCE0031ADCE004A9CD600BDDEF700BDEFF70094F7FF0094EFFF0094EF
      F7009CEFFF008CE7F7006BC6F70073CEFF0094EFFF0084F7FF00219CCE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DECEC600FFFFFF00EFD6C600DE8C5200E78C5A00EF946300EF946300EF94
      6300E78C5A00DE8C5200D6844A00D67B4A00D6B5A500FFFFFF00F7F7EF00C66B
      4200C66B3900C66B3900BD6331009C4A2100F7F7F700FFF7F700ADA59C000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C636300F78C8C00F78C8C00F78C8C00F78C
      8C00EF8C8C00F7A5A500EF848C00C67373006B313100F7DEC600FFFFDE00FFFF
      DE00FFFFDE00FFFFDE00FFFFDE00FFFFDE00FFC6C6009C636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CE948C00FFFFF700FFFFFF00FFFFFF00FFFF
      FF00FFFFF700FFF7F700FFF7EF00FFF7E700FFEFDE00FFEFDE00FFE7D600F7E7
      CE00F7E7C600F7DEC600F7DEBD00F7DEB500F7D6AD00B5848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7B58C00FFFFFF00FFFFFF00FFF7F700FFF7E700FFEFDE00F7E7D600F7E7
      C600F7DEBD00EFDEBD00429CCE00BDBDCE0063BDDE00D6F7F7009CF7EF009CE7
      EF0094EFF7009CF7FF007BDEF70073D6F70084DEFF0094EFF70029ADD6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DECEC600FFFFFF00FFEFE700E78C5A00EF946300F79C6300F7A56B00F79C
      6300DEAD9400E78C5A00DE845200D67B4A00D67B4A00EFE7E700FFFFFF00E7B5
      9C00C66B3900C66B3900BD6331009C4A2100FFFFFF00FFF7EF00A59C9C000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C636300F7949400F7949400F78C9400F78C
      8C00F78C8C00F78C8C00F78C8C00C67B7B006B313100F7DEC600FFFFDE00FFFF
      DE00FFFFDE00FFFFDE00FFFFDE00FFFFDE00FFC6C6009C636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CE948C00FFFFF700FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFF700FFF7EF00FFF7EF00FFEFE700FFEFDE00FFEFD600FFE7
      D600F7E7CE00F7E7C600F7DEC600F7DEBD00DEC6A500A57B8400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFBD9400FFFFFF00FFFFFF00FFFFFF00FFF7F700FFF7E700FFEFDE00FFEF
      D600FFEFCE00EFDEBD00BD8C8C00FFFFDE0039B5D6004AB5D600D6EFF700CEF7
      F700BDEFEF00B5F7F70094E7F70094DEF70094E7FF009CEFFF0063D6EF00109C
      D600000000000000000000000000000000000000000000000000000000000000
      0000DECEC600FFFFF700FFFFFF00F7AD8400EF946300F7A56B00FFAD7300D6C6
      BD00FFFFFF00DEA58C00DE845200D67B4A00D67B4A00EFE7E700FFFFFF00E7BD
      AD00C66B3900C66B3900BD633100C66B4200FFFFFF00F7E7E700A5A59C000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C636300FF949400FF949400FF949400F794
      9400F7949400F78C9400F78C8C00C67B7B006B313100F7DEC600FFFFDE00FFFF
      DE00FFFFDE00FFFFDE00FFFFDE00FFFFDE00FFC6C6009C636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D6949400FFFFF700FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFF700FFF7EF00FFF7EF00FFF7E700FFEFDE00FFEF
      D600FFE7D600F7E7CE00EFDEC600D6C6AD00B5AD94009C7B8400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFBD9400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7F700FFF7E700F7E7
      D600D6C6B500B5A59C00B5848400FFFFE700FFFFDE004AB5D60029ADD60073C6
      DE00BDDEE700BDDEE700C6E7E700CEEFF700BDDEEF00BDDEFF00CEEFEF0021AD
      D600000000000000000000000000000000000000000000000000000000000000
      0000DECEC600EFDED600FFFFFF00E7B59400EF946300F79C6300F7A56B00D6C6
      BD00FFFFFF00F7EFE700DE946B00D67B4A00DEAD9400F7F7F700FFFFFF00DEA5
      8C00C66B3900C66B39009C4A2100FFF7F700FFFFFF00DECEC600C6BDB5000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C636300FF949C00FF949400FF949400FF94
      9400FF949400FF949400F7949400CE7B7B006B313100F7DEC600FFFFDE00FFFF
      DE00FFFFDE00FFFFDE00FFFFDE00FFFFDE00FFC6C6009C636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D69C9400FFFFF700FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFF7EF00FFF7EF00FFEFE700FFEF
      DE00FFEFDE00E7DECE00CEBDAD00BDB5A500B5AD9C00A57B8400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFBD9400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7F700B584
      8400B5848400B5848400B5848400FFFFFF00FFFFE700FFFFDE00C6C6AD0063CE
      DE0031ADCE0029A5C60031A5C60031A5D600219CCE001894DE0029ADDE0021A5
      D600000000000000000000000000000000000000000000000000000000000000
      000000000000F7E7DE00FFFFFF00FFFFFF00E7B59400EF946300EF946300DE9C
      7B00E7E7E700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7DED600EF8C
      6300C66B3900C66B3900C66B4200FFFFFF00FFF7EF00C6BDB500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C636300FF9C9C00FF9C9C00FF9C9C00FF94
      9C00FF949C00FF949400FF949400CE7B7B006B313100F7DEC600FFFFDE00FFFF
      DE00FFFFDE00FFFFDE00FFFFDE00FFFFDE00FFC6C6009C636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DE9C9400FFFFF700FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFF7EF00FFF7EF00EFDE
      D600B5847300AD847300AD7B7300AD7B7300AD7B7300AD7B7300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFBD9400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00B584
      8400E7AD7300CE947B00CEDEEF00FFFFFF00FFFFFF00FFFFE700C6C6AD005252
      520052524A009C947B00E7B58C00BDBDCE006363BD004A42FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DECEC600EFDED600FFFFFF00FFFFF700E7B59400E78C5A00E78C
      5A00E7B59C00E7DED600E7DEDE00E7DEDE00E7E7E700EFDED600F7AD8400C66B
      3900C66B39009C4A2100FFFFFF00FFFFFF00E7D6CE009C9C9C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C636300FF9C9C00FF9C9C00FF9C9C00FF9C
      9C00FF9C9C00FF9C9C00FF9C9C00CE7B7B006B313100F7DEC600FFFFDE00FFFF
      DE00FFFFDE00FFFFDE00FFFFDE00FFFFDE00FFC6C6009C636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DEA59400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFFFF700E7CE
      C600B5847300E7B58400E7AD6B00EFA55200EFA53900BD848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DEAD8400FFF7F700FFF7F700FFF7F700FFF7F700FFF7F700EFEFF700B584
      8400CE947B008C84EF00DED6E700FFFFFF00FFFFFF00FFFFFF00FFFFE700E7E7
      C60073736300B5AD8C00FFDEAD00EFDECE005A5ACE003939FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000DECEC600EFDED600FFFFFF00FFFFFF00E7B59400DE84
      5200D6844A00D67B4A00D67B4A00D67B4A00CE734200CE734200CE7342009C4A
      2100BD5A2900FFFFFF00FFFFFF00F7E7DE00ADADAD0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C636300FF949400FF9C9C00FF9C9C00FF9C
      9C00FF9C9C00FF9C9C00FF9C9C00CE8484006B313100F7DEC600FFFFDE00FFFF
      DE00FFFFDE00FFFFDE00FFFFDE00FFFFDE00FFC6C6009C636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7A59400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7D6
      CE00B5847300EFC68C00F7BD6B00FFB55200BD84840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DEAD8400DEAD8400DEAD8400DEAD8400DEAD8400DEAD8400DEAD8400DEAD
      840084F7FF008484FF00A5A5DE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7
      F700EFEFD60073736300CEC6A500A5A5CE005252EF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DECEC600EFDED600FFFFFF00FFFFFF00FFFF
      F700E7B59400DE845200D67B4A00CE734200CE734200CE734200DE845200FFCE
      AD00FFFFFF00FFFFFF00F7EFE700ADADAD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C6363009C636300C67B7B00DE8C8C00FF9C
      9C00FF9C9C00FF9C9C00FF9C9C00CE8484006B313100F7DEC600FFFFDE00FFFF
      DE00FFFFDE00FFFFDE00FFFFDE00FFFFDE00FFC6C6009C636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7A59400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7D6
      D600B5847300EFC68C00F7BD6B00BD8484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000109CD60073C6DE009CEFF70094EFF7009CEFFF00ADF7
      FF0073DEEF005A5AFF008484F700CEC6DE00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFDE00BDBDC6005A5ADE003939FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DECEC600EFDED600FFFFFF00FFFF
      FF00FFFFFF00FFF7EF00FFF7E700FFF7E700FFF7E700FFF7E700FFF7EF00FFFF
      FF00FFF7F700EFDED600ADADAD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C6363009C636300A56B
      6B00C67B7B00F7949400FF9C9C00CE8484006B313100A56363009C6363009C63
      63009C6363009C6363009C6363009C6363009C6363009C636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7A59400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EFDE
      DE00B5847300F7C68C00BD848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000109CD600219CCE0021A5D60021ADDE0021AD
      DE0021ADDE0018A5D6006363FF007373F700ADA5DE00EFE7E700EFF7F700D6EF
      F700EFEFF700ADADE7005A5AE7004A4AFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DECEC600EFDED600F7EF
      EF00FFF7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7F700F7EF
      E700DECEC600C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009C6363009C636300AD737300B57373006B31310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DEA58C00EFD6BD00EFD6C600EFD6C600EFD6
      C600EFD6C600EFCEC600E7CEBD00E7CEBD00E7CEBD00DEC6BD00DEC6BD00CEAD
      A500B5847300BD84840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000002929FF005A5AE7006363EF007B7BEF007B7B
      EF005A5AE7004A4AFF003939FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7D6CE00DECEC600DECEC600DECEC600DECEC600DECEC600DECEC600E7D6
      CE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C6363006B31310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000002929FF002929FF002929
      FF002929FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000080000000400100000100010000000000001400000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFFFFFFFFC0E01F00000000
      FFFFFFFFFFFFFFFFFF00000700000000FFFFFFFFFFFFFFFFFC00000300000000
      FFFFFFFFFFFFFFFFF800000300000000FFFFFFFFFFFFFFFFE000000100000000
      FFFFFFFFFFFFFFFFC000000100000000FFC07FFFFFFC0FFFC000000000000000
      FF800FFFFFF807FFC000000000000000FF0007FFFFFC003FC000000000000000
      FF0003FFFFFE001FC000000000000000FF0001FFFFFC003FC000000000000000
      FF0001FFF800000FE000000100000000FF0001FFF800000FE000000100000000
      FF0001FFF800000FF000000300000000FF0001FFF800000FF000000300000000
      FF0001FFF800000FF800000700000000FF0001FFF800000FF800001F00000000
      FF8001FFF800000FFC00007F00000000FFC003FFF800000FFC00003F00000000
      FFE607FFF800000FFE00003F00000000FFE78FFFF800000FFE00003F00000000
      FFE38FFFF800000FFF00003F00000000FFE11FFFF800000FFF00003F00000000
      FFF01FFFF800000FFF80003F00000000FFFC3FFFF800000FFF80003F00000000
      FFFFFFFFFFFFFFFFFFC0001F00000000FFFFFFFFFFFFFFFFFFC0000F00000000
      FFFFFFFFFFFFFFFFFFE0040F00000000FFFFFFFFFFFFFFFFFFE01C0700000000
      FFFFFFFFFFFFFFFFFFF03E0700000000FFFFFFFFFFFFFFFFFFFFFE0700000000
      FFFFFFFFFFFFFFFFFFFFFF0700000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      F800001FFFFFFFFFFCFFF9FFFFC07FFFF800001FFC00003FF87FF0FFFF800FFF
      F800001FFC00003FF03FE07FFF0007FFF800001FFC00003FF01FC07FFF0003FF
      F800001FFC00003FF80F80FFFF0001FFF800001FFC00003FFC0701FFFF0001FF
      FC0000FFFC00003FFE0203FFFF0001FFFC0001FFFC00003FFF0007FFFF0001FF
      FC0003FFFC00003FFF800FFFFF0001FFFC0007FFFC00003FFFC01FFFFF0001FF
      FC000FFFFC00003FFFE03FFFFF0001FFFC300FFFFC00003FFE001E7FFF8001FF
      FC2007FFFC00003FFC000E3FFFE001FFFC0003FFFC00003FF800061FFFFC03FF
      FC0001FFFC00003FF802000FFFFFCFFFFC0180FFFC00003FF803000FFFE7CFFF
      FC03C07FFC00003FF883807FFFE7CFFFFC07E03FFC00003FF983C07FFFE7CFFF
      FC0FF01FFC00003FFF07C0FFFFE3CFFFFC1FF80FFE00007FFF0801FFFFF00FFF
      FC3FFC0FFFFFFFFFFFFC03FFFFF01FFFFC7FFE1FFFFFFFFFFFFE0FFFFFFC3FFF
      FCFFFF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00003FFFFFFFFFF800000FFFFFFFFF
      FF00003FFFF007FFF000000FFFFFFFFFFE00003FFFF007FFF000000FF000000F
      FE00003FFFE003FFF000000FF000000FFF00003FFF8000FFF000000FF000000F
      FF00003FFE00003FF000000FF000000FFF00003FFC00001FF000000FF000000F
      FF00003FF800000FF000000FF000000FFF00003FF800000FF000000FF000000F
      FF00003FF800000FF000000FF000000FFF00003FF800000FF000000FF000000F
      FF00003FF800000FF000000FF000000FFF00003FF800000FF000000FF000000F
      FF00003FF800000FF000000FF000000FFF00003FF800000FF000000FF000000F
      FF00003FFC00001FF000000FF000000FFF00003FFC00001FF000000FF000000F
      FF00003FFE00003FF000000FF000000FFF00003FFFE003FFF000000FF000000F
      FF00007FFFE003FFF000000FF000000FFF0000FFFFE003FFF000000FF000000F
      FF0001FFFFE007FFF000000FFFFFFFFFFF0003FFFFE00FFFF000000FFFFFFFFF
      FFFFFFFFFFE01FFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFF
      FFFFFFFFF0000007FFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF1FFFFFFFFFFFF
      FFFFFFFFF000000FFFC0FFFFFFFFFFFFFFFFFFFFF000F00FFF007FFFFFF003FF
      FE0001FFF000C00FFC003FFFFE0003FFFC0000FFF000800FF8001FFFFEF003FF
      FC0000FFF000000FF0000FFFFEF003FFFC0000FFF000000FF00007FFFEFFFFFF
      FC0000FFF100000FF00003FFFEF003FFFC0000FFF180000FF00001FFFE0003FF
      FC0000FFF180000FF00000FFFEF003FFFC0000FFE1800007F000007FFEF003FF
      FC0000FFE1800003F000003FFEFFFFFFFC0000FFC1000001F000001FFEF003FF
      FC0000FFC0000001F800001FFE0003FFFC0000FF80000000FC00001FFEF003FF
      FC0000FF80018001FC00001FFEF003FFFC0000FFC003C001FC00001FFEFFFFFF
      FC0000FFE007E007FF00001FF801FFFFFC0000FFF80FF00FFF00001FF801FFFF
      FC0000FFFC1FF81FFFC0003FF801FFFFFC0000FFFE07F63FFFE001FFF801FFFF
      FC0000FFFF03E23FFFF803FFFFFFFFFFFC0000FFFF81C23FFFF80FFFFFFFFFFF
      FE0001FFFFE0023FFFFF3FFFFFFFFFFFFFFFFFFFFFF0003FFFFFFFFFFFFFFFFF
      FFFFFFFFFFF8003FFFFFFFFFFFFFFFFFFFFFFFFFFFFC003FFFFFFFFFFFFFFFFF
      FFFFFFFFFFFE307FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3FFFFFFFFFFFFFFC3FFF
      FFC07FFFFFF81FFFFFFFFFFFFFF00FFFFF800FFFFFF00FFFFFE00FFFFFE007FF
      FF0007FFFFE007FFFF8003FFFFE007FFFF0003FFFFC003FFFF0001FFFFC00FFF
      FF0001FFFF8001FFFF0001FFFFC007FFFF0001FFFF0000FFFF0001FFFFC003FF
      FF0001FFFE00007FFF0001FFFFC003FFFF0001FFFC00003FFF0001FFFFC003FF
      FF0001FFF800001FFF0001FFFFC003FFFF0001FFF000000FFF0001FFFFC003FF
      FF0001FFF000000FFF0001FFFFC003FFFF8001FFF000000FFF0001FFFFC003FF
      FFE001FFF000000FFF0001FFFFC003FFFFFC03FFF800001FFF0001FFFFC003FF
      FFFFCFFFFC00003FFF0001FFFFE003FFFFE7CFFFFE00007FFF0001FFFFFC07FF
      FFE7CFFFFF0000FFFF0001FFFFFE7FFFFFE7CFFFFF8001FFFF0001FFFFCE73FF
      FFE3CFFFFFC003FFFF0001FFFF9819FFFFF00FFFFFE007FFFF0001FFFF9249FF
      FFF01FFFFFF00FFFFF0001FFFF93C9FFFFFC3FFFFFF81FFFFF8003FFFF9999FF
      FFFFFFFFFFFC3FFFFFE00FFFFFCFF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF01FF
      FFFFFFFFFFF07FFFFFFFFFFFFFFE003FFFFFFFFFFFB06FFFF000003FFFE0001F
      FF807FFFFFF07FFFF000003FFF80000FFF803FFFFFF07FFFF000003FFE00000F
      FF801FFFFFF07FFFF000003FFC00000FFF800FFFFFF07FFFF000003FFC00000F
      FF8007FFFF3067FFF000003FF800000FFF8003FFFFF07FFFF000003FF800000F
      FF8001FFFFF07FFFF000003FF000000FFF8001FFFFF07FFFF000003FF000000F
      FF8001FFFFF07FFFF000003FF000000FFF8001FFFC3061FFF000003FF000000F
      FF8001FFFFE03FFFF000003FF000001FFF8001FFFFE03FFFF000003FF000007F
      FF8001FFFFE03FFFF000003FF000007FFF8001FFFFE03FFFF000003FF800007F
      FF8001FFF83060FFF000003FF800007FFFC001FFFFF07FFFF000003FFC0000FF
      FFE001FFFFF07FFFF000003FFE0003FFFFF001FFFFF07FFFF000003FFF0007FF
      FFF801FFFFF07FFFF000003FFF800FFFFFFC01FFE030603FF000003FFFE03FFF
      FFFE01FFFFF07FFFF000003FFFFFFFFFFFFFFFFFFFF07FFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFF8001FFFFFFFFFFFFFFFFFFFFFFFFFFFF80007FFFFFFFFFFFFFFFFFF
      FFFFFFFFF80001FFFFFFFFFFFFFFFFFFF80003FFF800007FE000001FFFFFFFFF
      F00001FFF800001FE000001FF00FFFFFF00000FFF800001FE000001FF007FFFF
      F000007FF800001FE000001FF003FFFFF000003FF800001FE000001FF001FFFF
      F000003FF800001FE000001FF000FFFFF000003FF800001FE000001FF0007FFF
      F000003FF800001FE000001FF000007FF000003FF800001FE000001FF000003F
      F000003FF800001FE000001FF000001FF000003FF800001FE000001FF000001F
      F800003FF800001FE000001FF000001FFE00003FF800001FE000001FF000001F
      FE00001FF800001FE000001FF000001FFF00001FF800001FE000001FF000001F
      FF00007FF800001FE000001FF000001FFF80007FF800001FE000001FF800001F
      FF80007FFC00001FE000001FFC00001FFFC0007FFE00001FE000001FFE00001F
      FFC0007FFF80001FE000001FFF00001FFFE0003FFFF8001FE000001FFF80001F
      FFFFFF9FFFFE3FFFE000001FFFC0001FFFFFFFDFFFFE3FFFE000001FFFE0001F
      FFFFFFFFFFFE3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1FFFF
      FFFFFFFFFCFFF9FFF800001FFFE03FFFFE0001FFF87FF0FFF800001FFFC007FF
      FE0001FFF03FE07FF800001FFF8000FFFE0001FFF01FC07FF800001FFF00001F
      FE0001FFF80F80FFF800001FFE00001FFE0001FFFC0701FFF800001FFC00001F
      FE0001FFFE0203FFF800001FF800001FFE0001FFFF0007FFF800001FF800001F
      FE0001FFFF800FFFF800001FF800001FFE0001FFFFC01FFFF800001FF800001F
      FE0001FFFFE03FFFF800001FF800001FFE0001FFFE001E7FF800001FF800000F
      FE0001FFFC000E3FF800001FF800000FFE0001FFF800061FF800001FF800001F
      FE0001FFF802000FF800001FF800003FFE0001FFF803000FF800001FF800007F
      FE0001FFF883807FF800001FFC0000FFFE0001FFF983C07FF800001FFE0001FF
      FE0003FFFF07C0FFF800001FFF0003FFFE0007FFFF0801FFF800001FFF800FFF
      FE0007FFFFFC03FFF800001FFFC01FFFFE000FFFFFFE0FFFFFFFFFFFFFE01FFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFF23FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800FFFFFE00003FFFFFFFFF
      FFFFFFFFF0007FFFFE00003FF803C01FF80000FFF0007FFFFE00003FF803C01F
      F000007FF0007FFFFE00003FF803C01FF000007FF0000FFFFE00003FF803C01F
      F000007FF00003FFFE00003FF803C01FF000007FF00041FFFE00003FF803C01F
      F000007FF00060FFFE00003FF803C01FF000007FF000000FFE00003FF803C01F
      F000007FF000000FFE00003FF801801FF000007FF000000FFE00003FFC00003F
      F000007FF000000FFE00003FFC00003FF000007FF000000FFE00003FFE00003F
      F000007FF800000FFE00003FFE00007FF000007FFFFE000FFE00003FFF0000FF
      F000007FFFFE000FFE00001FFF0180FFF000007FFFFE000FFE00000FFF0180FF
      F000007FFFFE000FFE00001FFF83C1FFF000007FFFFE000FFE00003FFF83C1FF
      F000007FFFFE000FFE00007FFF83C1FFF000007FFFFE000FFE0000FFFFFFFFFF
      F000007FFFFE001FFE0001FFFFFFFFFFF000007FFFFE003FFE0003FFFFFFFFFF
      F80000FFFFFE007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00FFFFFFE7FFFFE00003FF8FFFFFF
      FF8003FFFFF07FFFFE00003FF80FFFFFFF0001FFFF807FFFFE00003FF8007FFF
      FE0000FFFE00003FFE00003FF80007FFFC00007FFE00003FFE00003FF80000FF
      F800003FFE00003FFE00003FF80000FFF800003FFE00003FFE00003FF000007F
      F000001FFE00003FFE00003FF000007FF000001FFE00003FFE00003FF000007F
      F000001FFE00003FFE00003FF000003FF000001FFE00003FFE00003FF000003F
      F000001FFE00003FFE00003FF000001FF000001FFE00003FFE00003FF000001F
      F000001FFE00003FFE00003FF000001FF000001FFE00003FFE00003FF000000F
      F000001FFE00003FFE00003FF000000FF000001FFE00003FFE00003FF000000F
      F800003FFE00003FFE00003FF000003FF800003FFE00003FFE00003FF000003F
      FC00007FFE00003FFE00007FF000007FFE0000FFFE00003FFE0000FFFC00007F
      FF0001FFFF80003FFE0001FFFE0000FFFF8003FFFFF07FFFFE0003FFFFFE01FF
      FFF00FFFFFFE7FFFFFFFFFFFFFFF87FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '.VIM'
    Filter = 'Fichiers Aeraulique|*.AER'
    Left = 621
    Top = 8
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '.VIM'
    Filter = 'Fichiers Aeraulique|*.AER'
    Left = 533
    Top = 8
  end
  object ImageList2: TImageList
    Height = 25
    Width = 25
    Left = 54
    Top = 80
    Bitmap = {
      494C010103000400280019001900FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000640000001900000001002000000000001027
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009796
      9900979699009796990097969900979699009796990097969900979699009796
      9900979699009796990097969900979699000000000097969900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000092635D008E5D59008E5D59008E5D59008E5D59008E5D59008E5D59008E5D
      59008E5D59008E5D59008E5D59008E5D59008E5D59008E5D59008E5D59008E5D
      59008E5D59008E5D590080504B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000979699000000000000000000000000009796
      9900000000000000000000000000979699000000000000000000000000009796
      9900000000009796990097969900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000093655E00EFD3B400F6DAB600F3D5
      AD00F2D1A500F0CE9E00EFCB9700EFC79100EEC58900EBC18200EBC08000EBC0
      8000EBC08000EBC08000EBC08000EBC08000EDC18000EABF7F0080504B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009796
      990000000000E7E6E900000000009796990000000000E7E6E900000000009796
      990000000000E7E6E90000000000979699000000000097969900979699009796
      990000000000000000000000000000000000000000000000000000000000D1CF
      D400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD4000000
      0000D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CF
      D400D1CFD400D1CFD400D1CFD400000000000000000000000000000000000000
      000093655F00EED4B800F4DABB00F2D5B100F0D1AA00EFCFA300EECB9D00EDC7
      9600EDC58F00EBC18800EABF8200E9BD7F00E9BD7F00E9BD7F00E9BD7F00E9BD
      7F00EABF7F00E7BC7E0080504B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000979699000000000000000000000000009796
      9900000000000000000000000000979699000000000000000000000000009796
      9900000000009796990000000000979699000000000000000000000000000000
      0000000000000000000000000000D1CFD400D1CFD400D1CFD400D1CFD400D1CF
      D400D1CFD400D1CFD400D1CFD40000000000D1CFD400D1CFD400D1CFD400D1CF
      D400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD4000000
      00000000000000000000000000000000000093655F00EED7C000F6E0C100F2D9
      B800F2D4B100F0D1AA00EFCFA400EECB9D00EDC79600EDC58F00EBC18900EABF
      8200E9BD7F00E9BD7F00E9BD7F00E9BD7F00EABF7F00E7BC7E0080504B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009796
      9900979699009796990097969900979699009796990097969900979699009796
      9900979699009796990097969900979699000000000097969900979699009796
      990000000000000000000000000000000000000000000000000000000000D1CF
      D400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD4000000
      0000D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CF
      D400D1CFD400D1CFD400D1CFD400000000000000000000000000000000000000
      000093655F00EFDAC500F7E2C700F3DCBF00F2D8B700F2D4B000F0D1AA00EFCE
      A300EECB9C00EDC79500EDC58F00EBC18900E9BD8100E9BD7F00E9BD7F00E9BD
      7F00EABF7F00E7BC7E0080504B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000979699000000000000000000000000009796
      9900000000000000000000000000979699000000000000000000000000009796
      9900000000009796990097969900979699000000000000000000000000000000
      0000000000000000000000000000D1CFD400D1CFD400D1CFD400D1CFD400D1CF
      D400D1CFD400D1CFD400D1CFD40000000000D1CFD400D1CFD400D1CFD400D1CF
      D400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD4000000
      00000000000000000000000000000000000093656000F0DECC00F8E6CF00811E
      0000811E0000811E0000811E0000811E0000811E0000EECB9C00EDC79600EDC5
      9000EBC18900E9BF8200E9BD7F00E9BD7F00EABF7F00E7BC7E0080504B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009796
      990000000000E7E6E900000000009796990000000000E7E6E900000000009796
      990000000000E7E6E90000000000979699000000000097969900979699009796
      990000000000000000000000000000000000000000000000000000000000D1CF
      D400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD4000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000095666000F2E2D300FAEAD700811E0000FEFEFE00CCD4F2002346FC00566F
      DD00811E0000EFCEA300EECB9D00EDC79700EDC59000EBC18900E9BD8100E9BD
      7F00EABF7F00E7BC7E0080504B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000979699000000000000000000000000009796
      9900000000000000000000000000979699000000000000000000000000009796
      9900000000009796990000000000979699000000000000000000000000000000
      0000000000000000000000000000D1CFD400D1CFD400D1CFD400D1CFD400D1CF
      D400D1CFD400D1CFD400D1CFD40000000000D1CFD400D1CFD400D1CFD400D1CF
      D40000000000D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD4000000
      00000000000000000000000000000000000098696300F2E6DA00FAEEDE00811E
      0000BAC6FA00183EFF005B78FE001032F400751C0100F0D1A900EFCEA300EECB
      9D00EDC79600EDC58F00EBC18900E9BD8100EABF7F00E7BC7E0080504B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009796
      9900979699009796990097969900979699009796990097969900979699009796
      9900979699009796990097969900979699000000000097969900979699009796
      990000000000000000000000000000000000000000000000000000000000D1CF
      D400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD4000000
      0000D1CFD400D1CFD400D1CFD400D1CFD40000000000D1CFD400D1CFD400D1CF
      D400D1CFD400D1CFD400D1CFD400000000000000000000000000000000000000
      00009E6E6400F4EAE100FBF2E600811E0000D8E0FE00BBC7FE00FEFEFE006480
      F7001E185E00F2D5B000F0D1AA00EFCEA400EDCB9C00EDC79600EBC58F00EBC1
      8800EABF8200E7BB7E0080504B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000979699000000000000000000000000009796
      9900000000000000000000000000979699000000000000000000000000009796
      9900000000009796990097969900979699000000000000000000000000000000
      0000000000000000000000000000D1CFD400D1CFD400D1CFD400D1CFD400D1CF
      D400D1CFD400D1CFD400D1CFD40000000000D1CFD400D1CFD400D1CFD400D1CF
      D40000000000D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD4000000
      000000000000000000000000000000000000A3726600F6EEE900FCF6ED00811E
      0000FEFEFE00FEFEFE00FEFEFE00F6F6F8000E1A9A006073DA00F0D5B000F0D1
      AA00EFCEA300EECA9C00EDC79500EBC48E00EBC28800E7BC800080504B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009796
      990000000000E7E6E900000000009796990000000000E7E6E900000000009796
      990000000000E7E6E90000000000979699000000000097969900979699009796
      990000000000000000000000000000000000000000000000000000000000D1CF
      D400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD4000000
      0000D1CFD400D1CFD400D1CFD400D1CFD40000000000D1CFD400D1CFD400D1CF
      D400D1CFD400D1CFD400D1CFD400000000000000000000000000000000000000
      0000A7756800F8F3F000FEFBF600811E0000811E0000811E0000811E0000811E
      0000741D04002948EE002F4BE6009695C600F0D1A900EFCEA300EDCB9C00EDC7
      9500EDC58F00E9BF870080504B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000979699000000000000000000000000009796
      9900000000000000000000000000979699000000000000000000000000009796
      9900000000009796990000000000979699000000000000000000000000000000
      0000000000000000000000000000D1CFD400D1CFD400D1CFD400D1CFD400D1CF
      D400D1CFD400D1CFD400D1CFD40000000000D1CFD400D1CFD400D1CFD400D1CF
      D40000000000D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD4000000
      000000000000000000000000000000000000AC796900FAF6F400FFFFFE00FEF8
      F300FBF2EA00F8EEE300F8EBDA00F7E6D300F7E2CC00EFDCC6006877DA005367
      DC00F2D4B000F0D1AA00EFCEA300EECB9C00EEC99600EAC18E0080504B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009796
      9900979699009796990097969900979699009796990097969900979699009796
      9900979699009796990097969900979699000000000097969900979699009796
      9900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D1CFD400D1CFD400D1CFD400D1CFD40000000000D1CFD400D1CFD400D1CF
      D400D1CFD400D1CFD400D1CFD400000000000000000000000000000000000000
      0000B17E6B00FAF6F400FFFFFF00FFFEFB00FEF7F000FBF3EA00FAEFE300F8EA
      DA00F7E7D300F6E2CB00F6E0C500F3DCBF00F2D8B600F2D4B000F0D1A900EFCE
      A100EECB9C00EBC5920080504B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000979699000000000000000000000000009796
      9900000000000000000000000000979699000000000000000000000000009796
      9900000000009796990097969900979699000000000000000000000000000000
      0000000000000000000000000000D1CFD400D1CFD400D1CFD400D1CFD400D1CF
      D40000000000D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CF
      D40000000000D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD4000000
      000000000000000000000000000000000000B6816C00FAF6F400FFFFFF00811E
      0000811E0000811E0000811E0000811E0000811E0000F7E6D300F6E3CC00F6DE
      C500F3DCBD00F3D8B600F2D4AF00F0D1A900F0CFA300EDC9990080504B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009796
      990000000000E7E6E900000000009796990000000000E7E6E900000000009796
      990000000000E7E6E90000000000979699000000000097969900979699009796
      990000000000000000000000000000000000000000000000000000000000D1CF
      D400D1CFD400D1CFD400D1CFD400D1CFD40000000000D1CFD400D1CFD400D1CF
      D400D1CFD400D1CFD400D1CFD400D1CFD40000000000D1CFD400D1CFD400D1CF
      D400D1CFD400D1CFD400D1CFD400000000000000000000000000000000000000
      0000BB846E00FAF6F400FFFFFF00811E0000FEFEFE00CCD4F2002346FC00566F
      DD00811E0000F8EADA00F7E6D300F6E2CB00F6DEC400F3DABC00F2D8B600F0D4
      AF00EFD0A700CEB4910080504B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000979699000000000000000000000000009796
      9900000000000000000000000000979699000000000000000000000000009796
      9900000000009796990000000000979699000000000000000000000000000000
      0000000000000000000000000000D1CFD400D1CFD400D1CFD400D1CFD400D1CF
      D40000000000D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CF
      D40000000000D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD4000000
      000000000000000000000000000000000000C0896F00FBF7F400FFFFFF00811E
      0000BAC6FA00183EFF005B78FE001032F400751C0100FAEEE200F8EBDA00F7E6
      D300F6E2CB00F6DEC400F3DCBD00E5CEAF00C4B09600A1927F0080504B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009796
      9900979699009796990097969900979699009796990097969900979699009796
      9900979699009796990097969900979699000000000097969900979699009796
      990000000000000000000000000000000000000000000000000000000000D1CF
      D400D1CFD400D1CFD400D1CFD400D1CFD4000000000000000000000000000000
      00000000000000000000000000000000000000000000D1CFD400D1CFD400D1CF
      D400D1CFD400D1CFD400D1CFD400000000000000000000000000000000000000
      0000C58C7000FBF7F400FFFFFF00811E0000D8E0FE00BBC7FE00FEFEFE006480
      F7001E185E00FBF3EA00F8EEE200F8EAD900F8E7D400FAE9D000E0D0BA00B8AB
      9A00A79C8B00A497860080504B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009796990097969900979699000000000000000000000000000000
      0000000000000000000000000000D1CFD400D1CFD400D1CFD400D1CFD400D1CF
      D40000000000D1CFD400D1CFD400D1CFD40000000000D1CFD400D1CFD400D1CF
      D40000000000D1CFD400D1CFD400D1CFD400D1CFD400D1CFD400D1CFD4000000
      000000000000000000000000000000000000CB917300FBF7F400FFFFFF00811E
      0000FEFEFE00FEFEFE00FEFEFE00F6F6F8000E1A9A006580F800FBF2E900FBF2
      E500E9D3C400A0675B00A0675B00A0675B00A0675B00A0675B00A0675B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009796
      9900979699009796990097969900979699009796990097969900979699009796
      9900979699009796990097969900979699009796990000000000979699009796
      990000000000000000000000000000000000000000000000000000000000D1CF
      D400D1CFD400D1CFD400D1CFD400D1CFD40000000000D1CFD400D1CFD400D1CF
      D40000000000D1CFD400D1CFD400D1CFD40000000000D1CFD400D1CFD400D1CF
      D400D1CFD400D1CFD400D1CFD400000000000000000000000000000000000000
      0000CF967400FBF7F600FFFFFF00811E0000811E0000811E0000811E0000811E
      0000741D04002B4EFE003252F7009EACF200DAC0B600A0675B00DAA16B00DD98
      4F00E2903A00EA892300A5686B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009796990097969900979699009796
      9900979699009796990097969900979699009796990097969900979699009796
      9900979699009796990000000000979699000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D4987500FCF8F600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F8FAFF006C86FB005875
      F800DDC4BC00A0675B00EAB47400EFA95200F6A03600A5686B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009796990097969900979699009796990097969900979699009796
      9900979699009796990097969900979699009796990097969900979699000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D4987500FCF8F600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00DDC7C200A0675B00EAB27300EFA7
      5100A5686B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D4987500FFFEFE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00E2D0CE00A0675B00EDB57200A5686B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CF8E6800CF8E6800CF8E6800CF8E6800CF8E6800CF8E6800CF8E6800CF8E
      6800CF8E6800CF8E6800CF8E6800CF8E6800CF8E6800A0675B00A5686B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424D3E000000000000003E00000028000000640000001900000001000100
      00000000900100000000000000000000000000000000000000000000FFFFFF00
      FFFFFFFFFFFFFFFFFFE0000000000000C0003FFFFFFFFFFFFFE0000000000000
      C0001FFFFFFFF00001E0000000000000C0000FC00000F00001E0000000000000
      C00007C00000F00001E0000000000000C00007C00000F00001E0000000000000
      C00007C00000F00001E0000000000000C00007C00000F00001E0000000000000
      C00007C00000F00001E0000000000000C00007C00000F00001E0000000000000
      C00007C00000F00001E0000000000000C00007C00000F00001E0000000000000
      C00007C00000F00001E0000000000000C00007C00000F00001E0000000000000
      C00007C00000F00001E0000000000000C00007C00000F00001E0000000000000
      C00007C00000F00001E0000000000000C00007C00000F00001E0000000000000
      C00007C00000F00001E0000000000000C00007C00000F00001E0000000000000
      C00007C00000F00001E0000000000000E00007C00000F00003E0000000000000
      F00007FFFFFFF00007E0000000000000F80007FFFFFFF0000FE0000000000000
      FFFFFFFFFFFFF0001FE000000000000000000000000000000000000000000000
      000000000000}
  end
  object ImageList3: TImageList
    Left = 158
    Top = 112
    Bitmap = {
      494C010105000900280010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000002000000001002000000000000020
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A4787400A4787400A4787400A4787400A4787400A4787400A478
      7400A4787400A47874008C5D5C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A87C7500FEE5CB00FFE2C400FFDFBE00FFDCB800FFD9B100FED6
      AC00FFD4A600FFD1A2008C5D5C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000AD807800FFEAD400E5A65700E5A65700E5A65700E5A65700E5A6
      5700E5A65700FFD4A8008C5D5C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B4867A00FEEEDD00FFEBD600FFE8CF00FFE4C900FEE1C200FEDD
      BB00FFDBB500FFD8AF008C5D5C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A4787400A4787400A4787400A478
      7400A4787400BA8D7D00FEF2E500E5A65700E5A65700E5A65700E5A65700E5A6
      5700E5A65700FEDCB7008C5D5C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A87C7500FEE5CB00FFE2C400FFDF
      BE00FFDCB800C2958100FEF6EC00FEF3E600FEEFE100FFEDDA00FEE9D400FEE6
      CC00FFE2C600FEDFBF008C5D5C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AD807800FFEAD400E5A65700E5A6
      5700E5A65700CA9B8300FFF9F300E5A65700E5A65700E5A65700E5A65700E5A6
      5700E5A65700FEE3C8008C5D5C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B4867A00FEEEDD00FFEBD600FFE8
      CF00FFE4C900D1A28600FEFBF900FEF9F400FEF7EF00FEF5EA00FEF1E400FEEE
      DE00FEEBD700FEE8D0008C5D5C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BA8D7D00FEF2E500E5A65700E5A6
      5700E5A65700D8A98A00FEFEFD00FEFCFA00FEFAF600FEF8F100FEF5EC00EBDF
      DB00D3C2C000BAA9AA008C5D5C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C2958100FEF6EC00FEF3E600FEEF
      E100FFEDDA00DFB08D00FEFEFE00FEFEFE00FEFCFB00FEFBF700FEF8F200B481
      7600B4817600B4817600B17F7400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CA9B8300FFF9F300E5A65700E5A6
      5700E5A65700E4B58E00FEFEFE00FEFEFE00FEFEFE00FEFDFC00FEFBF800B481
      7600EBB56F00E49B420000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D1A28600FEFBF900FEF9F400FEF7
      EF00FEF5EA00E8B89000DCA88700DCA88700DCA88700DCA88700DCA88700B481
      7600F0B25E000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D8A98A00FEFEFD00FEFCFA00FEFA
      F600FEF8F100FEF5EC00EBDFDB00D3C2C000BAA9AA008C5D5C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DFB08D00FEFEFE00FEFEFE00FEFC
      FB00FEFBF700FEF8F200B4817600B4817600B4817600B17F7400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E4B58E00FEFEFE00FEFEFE00FEFE
      FE00FEFDFC00FEFBF800B4817600EBB56F00E49B420000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E8B89000DCA88700DCA88700DCA8
      8700DCA88700DCA88700B4817600F0B25E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BE5600000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AB7E6200A4775A00955F3F008E57
      39007E4020007E402000000000000000000000000000AB7E6200A4775A00955F
      3F008E5739007E4020007E4020000000000000000000BE560000AD450000C565
      1800000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C5651800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000FA528000FA528000FA528000FA52800000000000000
      000000000000000000000000000000000000AB7E6200FCFAFA00E3C8B800CDA3
      8C00AC7655007E402000000000000000000000000000AB7E6200FCFAFA00E3C8
      B800CDA38C00AC7655007E40200000000000BE560000B7510600AF470000AB44
      0100B78262000000000000000000000000000000000000000000000000000000
      000000000000C56518009F3C0B00C56518000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000FA528007EE09E0051CD83000FA52800000000000000
      000000000000000000000000000000000000AB7E6200FCFAFA00E3C8B800CDA3
      8C00B9805F007E402000000000000000000000000000AB7E6200FCFAFA00E3C8
      B800CDA38C00B9805F007E40200000000000C0672500D0701F00B1490100AF47
      0000C56518000000000000000000000000000000000000000000000000000000
      0000C5651800A0390100A5502500D17B2E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000FA528007EE09E0038C273000FA52800000000000000
      000000000000000000000000000000000000AB7E6200FCFAFA00E3C8B800CDA3
      8C00B9805F007E402000000000000000000000000000AB7E6200FCFAFA00E3C8
      B800CDA38C00B9805F007E40200000000000D0701F00E0A05000C5651800B048
      0000AF470000C565180000000000000000000000000000000000C5651800A542
      0C00A33B0000A7512300D17B2E00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000FA528007EE09E0038C273000FA52800000000000000
      000000000000000000000000000000000000AB7E6200FCFAFA00E3C8B800CDA3
      8C00B9805F007E402000000000000000000000000000AB7E6200FCFAFA00E3C8
      B800CDA38C00B9805F007E4020000000000000000000D0701F00E0A05000B956
      0B00B0480000AE470100C56518000000000000000000AF663D00A7400100A63E
      0000A8501F00D17B2E0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000FA528007EE09E0038C273000FA52800000000000000
      000000000000000000000000000000000000AB7E62009F6B4E009F6B4E009153
      3100915331007E4020007E4020006F432400AB7E62009F6B4E009F6B4E009153
      3100915331007E4020007E402000000000000000000000000000D0701F00E0A0
      5000B9560B00B0480000AE470100C5651800AA480D00AB430000A9410000A950
      1D00D17B2E000000000000000000000000000000000000189000001890000018
      9000001890000018900000189000001890000018900000189000001890000018
      900000189000001890000018900000000000000000000FA528000FA528000FA5
      28000FA528000FA528000FA528007EE09E0038C273000FA528000FA528000FA5
      28000FA528000FA528000FA5280000000000DDC5A900AB7E6200ECD8CD00ECD8
      CD00CAA08900AC7655007E4020009F6B4E00AB7E6200ECD8CD00ECD8CD00CAA0
      8900AC7655007E4020009060600000000000000000000000000000000000D070
      1F00D47F3300B9560B00B0480000AF470000AE460000AC440000AC521C00D17B
      2E000000000000000000000000000000000000000000001890008B9EF1002952
      E7002952E7002952E7002952E7002952E7002952E7002952E7002952E7002952
      E7002952E7003D59DB000018900000000000000000000FA528007EE09E0038C2
      730038C2730038C2730038C2730038C2730038C2730038C2730038C2730038C2
      730038C2730051CD83000FA528000000000000000000AB7E6200FFFFFF00F1E8
      E500EED2C100AC7655007E402000C8989000AB7E6200FFFFFF00F1E8E500EED2
      C100AC7655009060600000000000000000000000000000000000000000000000
      0000D0701F00B44C0000B24A0000B1490000AF470000AC501600D17B2E000000
      00000000000000000000000000000000000000000000001890008B9EF100718B
      ED00718BED00718BED00718BED00718BED00718BED00718BED00718BED00718B
      ED00718BED003D59DB000018900000000000000000000FA528005FD38B005FD3
      8B005FD38B005FD38B005FD38B0051CD830038C273005FD38B005FD38B005FD3
      8B005FD38B005FD38B000FA52800000000000000000000000000AB7E62009F6B
      4E00955F3F007E4020007E4020009F6B4E00AB7E62009F6B4E00955F3F007E40
      20007E402000000000000000000000000000000000000000000000000000B44C
      0000B34E0600B54D0000B44C0000B24A0000B1490000AD4D1100D17B2E000000
      0000000000000000000000000000000000000000000000189000001890000018
      9000001890000018900000189000001890000018900000189000001890000018
      900000189000001890000018900000000000000000000FA528000FA528000FA5
      28000FA528000FA528000FA528007EE09E0038C273000FA528000FA528000FA5
      28000FA528000FA528000FA52800000000000000000000000000AB7E6200FCFA
      FA00DEC3B100AC7655007E40200000000000AB7E6200FCFAFA00DEC3B100AC76
      55007E4020000000000000000000000000000000000000000000B44C0000B553
      0A00B8500000B74F0000B54E0000BB590D00B44D0100B1490000AF562000D17B
      2E00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000FA528007EE09E0038C273000FA52800000000000000
      0000000000000000000000000000000000000000000000000000AB7E62009F6B
      4E00955F3F007E4020009F6B4E0000000000AB7E62009F6B4E00955F3F007E40
      20009F6B4E0000000000000000000000000000000000B44C0000B7560E00BB53
      0000BA520000E0A05000DD973B00BE560000D17B2E00DD973B00B1490000AF56
      2000D17B2E000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000FA528007EE09E0038C273000FA52800000000000000
      000000000000000000000000000000000000000000000000000000000000AB7E
      6200FCFAFA007E402000000000000000000000000000AB7E6200FCFAFA007E40
      200000000000000000000000000000000000B44C0000B95A1300BE560000BD55
      0000E0A05000DD973B00BE5600000000000000000000D17B2E00DD973B00B149
      0000AD511600D17B2E0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000FA528007EE09E0038C273000FA52800000000000000
      000000000000000000000000000000000000000000000000000000000000AB7E
      6200AB7E62009F6B4E00000000000000000000000000AB7E6200AB7E62009F6B
      4E0000000000000000000000000000000000B44C0000C1590000C0590100E0A0
      5000DD973B00BE56000000000000000000000000000000000000D17B2E00BB66
      2700B44E0400AD4B0D00D17B2E00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000FA5280051CD83005FD38B000FA52800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B44C0000CF742300DD97
      3B00BE5600000000000000000000000000000000000000000000000000000000
      0000D17B2E00B6561000AE480600D17B2E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000FA528000FA528000FA528000FA52800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B44C0000BE56
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D17B2E00BB662700BA560D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000200000000100010000000000000100000000000000000000
      000000000000000000000000FFFFFF00F801000000000000F801000000000000
      F801000000000000F80100000000000000010000000000000001000000000000
      0001000000000000000100000000000000010000000000000001000000000000
      00030000000000000007000000000000003F000000000000003F000000000000
      007F00000000000000FF000000000000FFFFDFFFFFFFFFFF03818FFDFFFFFC3F
      038107F8FFFFFC3F038107F0FFFFFC3F038103C1FFFFFC3F03818183FFFFFC3F
      0001C007800180010001E00F800180018003F01F80018001C007E01F80018001
      C107C00FFFFFFC3FC1078007FFFFFC3FE38F0183FFFFFC3FE38F03C1FFFFFC3F
      FFFF87F0FFFFFC3FFFFFCFF8FFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object AdvToolBarOfficeStyler1: TAdvToolBarOfficeStyler
    AppColor.AppButtonColor = 13005312
    AppColor.AppButtonHoverColor = 16755772
    AppColor.TextColor = clWhite
    AppColor.HoverColor = 16246477
    AppColor.HoverTextColor = clBlack
    AppColor.HoverBorderColor = 15187578
    AppColor.SelectedColor = 15187578
    AppColor.SelectedTextColor = clBlack
    AppColor.SelectedBorderColor = 15187578
    Style = bsOffice2010Blue
    BorderColor = 13087391
    BorderColorHot = 14074033
    ButtonAppearance.Color = 16643823
    ButtonAppearance.ColorTo = 15784647
    ButtonAppearance.ColorChecked = 7131391
    ButtonAppearance.ColorCheckedTo = 7131391
    ButtonAppearance.ColorDown = 7131391
    ButtonAppearance.ColorDownTo = 8122111
    ButtonAppearance.ColorHot = 9102333
    ButtonAppearance.ColorHotTo = 14285309
    ButtonAppearance.BorderDownColor = 3181250
    ButtonAppearance.BorderHotColor = 5819121
    ButtonAppearance.BorderCheckedColor = 3181250
    ButtonAppearance.CaptionFont.Charset = DEFAULT_CHARSET
    ButtonAppearance.CaptionFont.Color = clWindowText
    ButtonAppearance.CaptionFont.Height = -11
    ButtonAppearance.CaptionFont.Name = 'Segoe UI'
    ButtonAppearance.CaptionFont.Style = []
    CaptionAppearance.CaptionColor = 16181724
    CaptionAppearance.CaptionColorTo = 16181724
    CaptionAppearance.CaptionTextColor = 5978398
    CaptionAppearance.CaptionBorderColor = 16181724
    CaptionAppearance.CaptionColorHot = 16117737
    CaptionAppearance.CaptionColorHotTo = 16117737
    CaptionAppearance.CaptionTextColorHot = 5978398
    CaptionAppearance.CaptionBorderColorHot = 16117737
    CaptionFont.Charset = DEFAULT_CHARSET
    CaptionFont.Color = clWindowText
    CaptionFont.Height = -11
    CaptionFont.Name = 'Segoe UI'
    CaptionFont.Style = []
    ContainerAppearance.LineColor = clBtnShadow
    ContainerAppearance.Line3D = True
    Color.Color = 16643823
    Color.ColorTo = 15784647
    Color.Direction = gdVertical
    Color.Mirror.Color = 16775925
    Color.Mirror.ColorTo = 16445413
    Color.Mirror.ColorMirror = 16445413
    Color.Mirror.ColorMirrorTo = 16181724
    ColorHot.Color = 16248291
    ColorHot.ColorTo = 16643823
    ColorHot.Direction = gdVertical
    ColorHot.Mirror.Color = 16775925
    ColorHot.Mirror.ColorTo = 16445413
    ColorHot.Mirror.ColorMirror = 16445413
    ColorHot.Mirror.ColorMirrorTo = 16117737
    CompactGlowButtonAppearance.BorderColor = 13087391
    CompactGlowButtonAppearance.BorderColorHot = 5819121
    CompactGlowButtonAppearance.BorderColorDown = 3181250
    CompactGlowButtonAppearance.BorderColorChecked = 3181250
    CompactGlowButtonAppearance.Color = 16643823
    CompactGlowButtonAppearance.ColorTo = 15784647
    CompactGlowButtonAppearance.ColorChecked = 14285309
    CompactGlowButtonAppearance.ColorCheckedTo = 7131391
    CompactGlowButtonAppearance.ColorDisabled = 15921906
    CompactGlowButtonAppearance.ColorDisabledTo = 15921906
    CompactGlowButtonAppearance.ColorDown = 7131391
    CompactGlowButtonAppearance.ColorDownTo = 8122111
    CompactGlowButtonAppearance.ColorHot = 9102333
    CompactGlowButtonAppearance.ColorHotTo = 14285309
    CompactGlowButtonAppearance.ColorMirror = 15784647
    CompactGlowButtonAppearance.ColorMirrorTo = 15784647
    CompactGlowButtonAppearance.ColorMirrorHot = 14285309
    CompactGlowButtonAppearance.ColorMirrorHotTo = 9102333
    CompactGlowButtonAppearance.ColorMirrorDown = 8122111
    CompactGlowButtonAppearance.ColorMirrorDownTo = 7131391
    CompactGlowButtonAppearance.ColorMirrorChecked = 7131391
    CompactGlowButtonAppearance.ColorMirrorCheckedTo = 7131391
    CompactGlowButtonAppearance.ColorMirrorDisabled = 11974326
    CompactGlowButtonAppearance.ColorMirrorDisabledTo = 15921906
    CompactGlowButtonAppearance.GradientHot = ggVertical
    CompactGlowButtonAppearance.GradientMirrorHot = ggVertical
    CompactGlowButtonAppearance.GradientDown = ggVertical
    CompactGlowButtonAppearance.GradientMirrorDown = ggVertical
    CompactGlowButtonAppearance.GradientChecked = ggVertical
    DockColor.Color = 15784647
    DockColor.ColorTo = 16643823
    DockColor.Direction = gdHorizontal
    DockColor.Steps = 128
    DragGripStyle = dsNone
    FloatingWindowBorderColor = 14074033
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    GlowButtonAppearance.BorderColor = 13087391
    GlowButtonAppearance.BorderColorHot = 5819121
    GlowButtonAppearance.BorderColorDown = 3181250
    GlowButtonAppearance.BorderColorChecked = 3181250
    GlowButtonAppearance.Color = 16643823
    GlowButtonAppearance.ColorTo = 15784647
    GlowButtonAppearance.ColorChecked = 14285309
    GlowButtonAppearance.ColorCheckedTo = 7131391
    GlowButtonAppearance.ColorDisabled = 15921906
    GlowButtonAppearance.ColorDisabledTo = 15921906
    GlowButtonAppearance.ColorDown = 7131391
    GlowButtonAppearance.ColorDownTo = 8122111
    GlowButtonAppearance.ColorHot = 9102333
    GlowButtonAppearance.ColorHotTo = 14285309
    GlowButtonAppearance.ColorMirror = 15784647
    GlowButtonAppearance.ColorMirrorTo = 15784647
    GlowButtonAppearance.ColorMirrorHot = 14285309
    GlowButtonAppearance.ColorMirrorHotTo = 9102333
    GlowButtonAppearance.ColorMirrorDown = 8122111
    GlowButtonAppearance.ColorMirrorDownTo = 7131391
    GlowButtonAppearance.ColorMirrorChecked = 7131391
    GlowButtonAppearance.ColorMirrorCheckedTo = 7131391
    GlowButtonAppearance.ColorMirrorDisabled = 11974326
    GlowButtonAppearance.ColorMirrorDisabledTo = 15921906
    GlowButtonAppearance.GradientHot = ggVertical
    GlowButtonAppearance.GradientMirrorHot = ggVertical
    GlowButtonAppearance.GradientDown = ggVertical
    GlowButtonAppearance.GradientMirrorDown = ggVertical
    GlowButtonAppearance.GradientChecked = ggVertical
    GroupAppearance.Background = clInfoBk
    GroupAppearance.BorderColor = 1340927
    GroupAppearance.Color = 4636927
    GroupAppearance.ColorTo = 4636927
    GroupAppearance.ColorMirror = 4636927
    GroupAppearance.ColorMirrorTo = 4636927
    GroupAppearance.Font.Charset = DEFAULT_CHARSET
    GroupAppearance.Font.Color = clWindowText
    GroupAppearance.Font.Height = -11
    GroupAppearance.Font.Name = 'Segoe UI'
    GroupAppearance.Font.Style = []
    GroupAppearance.Gradient = ggVertical
    GroupAppearance.GradientMirror = ggVertical
    GroupAppearance.TextColor = clWhite
    GroupAppearance.CaptionAppearance.CaptionColor = 16181724
    GroupAppearance.CaptionAppearance.CaptionColorTo = 16181724
    GroupAppearance.CaptionAppearance.CaptionTextColor = 5978398
    GroupAppearance.CaptionAppearance.CaptionBorderColor = 16181724
    GroupAppearance.CaptionAppearance.CaptionColorHot = 16117737
    GroupAppearance.CaptionAppearance.CaptionColorHotTo = 16117737
    GroupAppearance.CaptionAppearance.CaptionTextColorHot = 5978398
    GroupAppearance.CaptionAppearance.CaptionBorderColorHot = 16117737
    GroupAppearance.PageAppearance.BorderColor = 13087391
    GroupAppearance.PageAppearance.Color = 16775925
    GroupAppearance.PageAppearance.ColorTo = 16445413
    GroupAppearance.PageAppearance.ColorMirror = 16445413
    GroupAppearance.PageAppearance.ColorMirrorTo = 16181724
    GroupAppearance.PageAppearance.Gradient = ggVertical
    GroupAppearance.PageAppearance.GradientMirror = ggVertical
    GroupAppearance.PageAppearance.ShadowColor = 15126975
    GroupAppearance.PageAppearance.HighLightColor = 13416873
    GroupAppearance.TabAppearance.BorderColor = 13087391
    GroupAppearance.TabAppearance.BorderColorHot = 1340927
    GroupAppearance.TabAppearance.BorderColorSelected = 1340927
    GroupAppearance.TabAppearance.BorderColorSelectedHot = 1340927
    GroupAppearance.TabAppearance.BorderColorDisabled = clNone
    GroupAppearance.TabAppearance.BorderColorDown = clNone
    GroupAppearance.TabAppearance.Color = clBtnFace
    GroupAppearance.TabAppearance.ColorTo = clWhite
    GroupAppearance.TabAppearance.ColorSelected = 16775925
    GroupAppearance.TabAppearance.ColorSelectedTo = 16775925
    GroupAppearance.TabAppearance.ColorDisabled = 15921906
    GroupAppearance.TabAppearance.ColorDisabledTo = 15921906
    GroupAppearance.TabAppearance.ColorHot = 16446701
    GroupAppearance.TabAppearance.ColorHotTo = 16710903
    GroupAppearance.TabAppearance.ColorMirror = clWhite
    GroupAppearance.TabAppearance.ColorMirrorTo = clWhite
    GroupAppearance.TabAppearance.ColorMirrorHot = 16710906
    GroupAppearance.TabAppearance.ColorMirrorHotTo = 16710906
    GroupAppearance.TabAppearance.ColorMirrorSelected = 16775925
    GroupAppearance.TabAppearance.ColorMirrorSelectedTo = 16775925
    GroupAppearance.TabAppearance.ColorMirrorDisabled = 15921906
    GroupAppearance.TabAppearance.ColorMirrorDisabledTo = 15921906
    GroupAppearance.TabAppearance.Font.Charset = DEFAULT_CHARSET
    GroupAppearance.TabAppearance.Font.Color = clWindowText
    GroupAppearance.TabAppearance.Font.Height = -11
    GroupAppearance.TabAppearance.Font.Name = 'Segoe UI'
    GroupAppearance.TabAppearance.Font.Style = []
    GroupAppearance.TabAppearance.Gradient = ggVertical
    GroupAppearance.TabAppearance.GradientMirror = ggVertical
    GroupAppearance.TabAppearance.GradientHot = ggVertical
    GroupAppearance.TabAppearance.GradientMirrorHot = ggVertical
    GroupAppearance.TabAppearance.GradientSelected = ggVertical
    GroupAppearance.TabAppearance.GradientMirrorSelected = ggVertical
    GroupAppearance.TabAppearance.GradientDisabled = ggVertical
    GroupAppearance.TabAppearance.GradientMirrorDisabled = ggVertical
    GroupAppearance.TabAppearance.TextColor = 5978398
    GroupAppearance.TabAppearance.TextColorHot = 5978398
    GroupAppearance.TabAppearance.TextColorSelected = 5978398
    GroupAppearance.TabAppearance.TextColorDisabled = clGray
    GroupAppearance.TabAppearance.ShadowColor = 13087391
    GroupAppearance.TabAppearance.HighLightColor = 16775871
    GroupAppearance.TabAppearance.HighLightColorHot = 16643823
    GroupAppearance.TabAppearance.HighLightColorSelected = 13087391
    GroupAppearance.TabAppearance.HighLightColorSelectedHot = 15784647
    GroupAppearance.TabAppearance.HighLightColorDown = 16181209
    GroupAppearance.ToolBarAppearance.BorderColor = 13087391
    GroupAppearance.ToolBarAppearance.BorderColorHot = 11780804
    GroupAppearance.ToolBarAppearance.Color.Color = 16775925
    GroupAppearance.ToolBarAppearance.Color.ColorTo = 16181724
    GroupAppearance.ToolBarAppearance.Color.Direction = gdHorizontal
    GroupAppearance.ToolBarAppearance.ColorHot.Color = 16775925
    GroupAppearance.ToolBarAppearance.ColorHot.ColorTo = 16117737
    GroupAppearance.ToolBarAppearance.ColorHot.Direction = gdHorizontal
    PageAppearance.BorderColor = 13087391
    PageAppearance.Color = 16775925
    PageAppearance.ColorTo = 16445413
    PageAppearance.ColorMirror = 16445413
    PageAppearance.ColorMirrorTo = 16181724
    PageAppearance.Gradient = ggVertical
    PageAppearance.GradientMirror = ggVertical
    PageAppearance.ShadowColor = 15126975
    PageAppearance.HighLightColor = 13416873
    PagerCaption.BorderColor = 13087391
    PagerCaption.Color = 16775925
    PagerCaption.ColorTo = 15389631
    PagerCaption.ColorMirror = 15389631
    PagerCaption.ColorMirrorTo = 15389631
    PagerCaption.Gradient = ggVertical
    PagerCaption.GradientMirror = ggVertical
    PagerCaption.TextColor = clGray
    PagerCaption.TextColorExtended = clBlue
    PagerCaption.Font.Charset = DEFAULT_CHARSET
    PagerCaption.Font.Color = clWindowText
    PagerCaption.Font.Height = -13
    PagerCaption.Font.Name = 'Segoe UI'
    PagerCaption.Font.Style = []
    QATAppearance.BorderColor = 13087391
    QATAppearance.Color = 16643823
    QATAppearance.ColorTo = 15784647
    QATAppearance.FullSizeBorderColor = 13087391
    QATAppearance.FullSizeColor = 16643823
    QATAppearance.FullSizeColorTo = 15784647
    RightHandleColor = 16643823
    RightHandleColorTo = 15784647
    RightHandleColorHot = 14285309
    RightHandleColorHotTo = 9102333
    RightHandleColorDown = 8122111
    RightHandleColorDownTo = 7131391
    TabAppearance.BorderColor = 13087391
    TabAppearance.BorderColorHot = 12236209
    TabAppearance.BorderColorSelected = 14404024
    TabAppearance.BorderColorSelectedHot = 14404024
    TabAppearance.BorderColorDisabled = clNone
    TabAppearance.BorderColorDown = clNone
    TabAppearance.Color = clBtnFace
    TabAppearance.ColorTo = clWhite
    TabAppearance.ColorSelected = 16775925
    TabAppearance.ColorSelectedTo = 16775925
    TabAppearance.ColorDisabled = 15921906
    TabAppearance.ColorDisabledTo = 15921906
    TabAppearance.ColorHot = 16446701
    TabAppearance.ColorHotTo = 16710903
    TabAppearance.ColorMirror = clWhite
    TabAppearance.ColorMirrorTo = clWhite
    TabAppearance.ColorMirrorHot = 16710906
    TabAppearance.ColorMirrorHotTo = 16710906
    TabAppearance.ColorMirrorSelected = 16775925
    TabAppearance.ColorMirrorSelectedTo = 16775925
    TabAppearance.ColorMirrorDisabled = 15921906
    TabAppearance.ColorMirrorDisabledTo = 15921906
    TabAppearance.Font.Charset = DEFAULT_CHARSET
    TabAppearance.Font.Color = clWindowText
    TabAppearance.Font.Height = -11
    TabAppearance.Font.Name = 'Segoe UI'
    TabAppearance.Font.Style = []
    TabAppearance.Gradient = ggVertical
    TabAppearance.GradientMirror = ggVertical
    TabAppearance.GradientHot = ggVertical
    TabAppearance.GradientMirrorHot = ggVertical
    TabAppearance.GradientSelected = ggVertical
    TabAppearance.GradientMirrorSelected = ggVertical
    TabAppearance.GradientDisabled = ggVertical
    TabAppearance.GradientMirrorDisabled = ggVertical
    TabAppearance.TextColor = 5978398
    TabAppearance.TextColorHot = 5978398
    TabAppearance.TextColorSelected = 5978398
    TabAppearance.TextColorDisabled = clGray
    TabAppearance.ShadowColor = 13087391
    TabAppearance.HighLightColor = 16775871
    TabAppearance.HighLightColorHot = 16643823
    TabAppearance.HighLightColorSelected = 13087391
    TabAppearance.HighLightColorSelectedHot = 15784647
    TabAppearance.HighLightColorDown = 16181209
    TabAppearance.BackGround.Color = 16446701
    TabAppearance.BackGround.ColorTo = 16710906
    TabAppearance.BackGround.Direction = gdVertical
    Left = 48
    Top = 112
  end
  object Img_Menus: TImageList
    Left = 120
    Top = 112
    Bitmap = {
      494C010112001300280010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000005000000001002000000000000050
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C6C600AD9C9C00AD9C9400AD9C9C00C6C6C600000000000000
      000000000000000000000000000000000000000000000000000000000000C8B3
      A400694731006947310069473100694731006947310069473100694731006947
      3100694731000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7D6CE00EFEFEF00E7DEDE00E7E7E700EFEFEF00EFEFEF00B5A59C000000
      000000000000000000000000000000000000000000000000000000000000C8B3
      A400EDE5E000EBE0DB00E6D9D300E0D3CC00CDBFB700D8C7BE00D9C7BE00DBC9
      BF00694731000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F7EF
      E700EFEFEF00BD5A2900BD633900DEBDA500BD633100BD5A2900EFEFEF00C6BD
      B50000000000000000000000000000000000000000000000000000000000C8B3
      A400F2ECE800EDE5E100B7B8C3009FA2B2004E80D200AAA19F00B7A9A100D9C7
      BE00694731000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C600EFEF
      EF00BD5A2900C66B4200C66B3900F7F7F700BD633900BD6331009C4A2100EFEF
      EF008C847B00000000000000000000000000000000000000000000000000C8B3
      A400F6F1EF00EFE9E5005D8EDF003F7EE6005087DF003C7FEA00ABA3A200D8C7
      BE00694731000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F7F7F700DE84
      5200CE734200CE734200CE734200DE946B00C66B3900BD633900BD633100E7B5
      9400EFDED600000000000000000000000000000000000000000000000000C8B3
      A400FAF6F600BBC8E0004983DE00B4AFAD00E9DFDA004F87E0006E84AC00C2B5
      AD00694731000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00D67B
      4A00D67B4A00D67B4A00D67B4A00FFFFFF00CE734200C66B3900BD6331009C4A
      2100F7F7EF00000000000000000000000000000000000000000000000000C8B3
      A400FDFCFC00E2E6F2006795DE008692A800BEB9B5004380E4009DA1B100DFD1
      CB00694731000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00DE84
      5200E78C5A00E78C5A00DE845200C6A59400F7F7F700C66B3900BD6339009C4A
      2100FFF7F700000000000000000000000000000000000000000000000000C8B3
      A400FFFFFF00FDFCFD003F81E9006795DE003E7FE7005B8DDD00B1B6C600E7DB
      D500694731000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00E78C
      5A00F79C6300F79C6300E78C5A00D67B4A00EFE7E700E7B59C00C66B39009C4A
      2100FFF7EF00000000000000000000000000000000000000000000000000C8B3
      A400FFFFFF00FFFFFF00FDFCFD00E3E8F3008CADE300EEE8E500EDE6E100ECE2
      DE00694731000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFDED600E7B5
      9400F79C6300D6C6BD00F7EFE700D67B4A00F7F7F700DEA58C00C66B3900FFF7
      F700DECEC600000000000000000000000000000000000000000000000000C8B3
      A400FFFFFF00FFFFFF00FFFFFF00FEFDFE00FBF8F700F7F2F100D5BFB100B7A2
      9300694731000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DECEC600FFFF
      FF00E7B59400E78C5A00E7DED600E7DEDE00EFDED600C66B39009C4A2100FFFF
      FF009C9C9C00000000000000000000000000000000000000000000000000C8B3
      A400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FCF9F900F3EBE600E8DC
      D400694731000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DECE
      C600FFFFFF00FFFFF700DE845200CE734200CE734200FFCEAD00FFFFFF00ADAD
      AD0000000000000000000000000000000000000000000000000000000000C8B3
      A400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E8DCD4006947
      3100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DECEC600F7EFEF00FFFFFF00FFFFFF00FFFFFF00F7EFE700C6C6C6000000
      000000000000000000000000000000000000000000000000000000000000C8B3
      A400C8B3A400C8B3A400C8B3A400C8B3A400C8B3A400C8B3A400694731000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000707070005858580050505000505050005050500050505000585858007070
      7000000000000000000000000000000000000000000064687300183D60000000
      0000000000007070700058585800505050005050500050505000505050005858
      5800707070000000000000000000000000000000000000000000000000000000
      00006C6A6A006C6A6A0000000000000000006C6A6A006C6A6A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A4787400A4787400A4787400A4787400A4787400A4787400A478
      7400A4787400A47874008C5D5C00000000000000000000000000000000005050
      500093B4C200C5D7DE00B4D4E0009BC7D7007FB7CD00D1ACA700694731006947
      310069473100694731006947310069473100646873004995E1004D5A9100183D
      60005050500093B4C200C5D7DE00B4D4E0009BC7D7007FB7CD005FA2BE00488B
      A600306B85005050500000000000000000000000000000000000000000006C6A
      6A00AAA7A700A19F9F006C6A6A006C6A6A006C6A6A00E5E3E3006C6A6A006C6A
      6A006C6A6A000000000000000000000000000000000000000000000000000000
      000000000000A87C7500FEE5CB00FFE2C400FFDFBE00FFDCB800FFD9B100FED6
      AC00FFD4A600FFD1A2008C5D5C00000000000000000000000000337C9E00B5DA
      E800EFF7FA00E2EFF300C1DCE600A3CCDB0086BCD000D1ACA700C86000009830
      0000FFFFFF00C860000098300000694731003C98E40098D8F800468EDD004D5A
      9100183D6000EFF7FA00E2EFF300C1DCE600A3CCDB0086BCD0006AABC5004E9B
      BA00368BAF001A709600707070000000000000000000000000006C6A6A00DAD9
      D900A19F9F00A19F9F00A19F9F0037363600353535006C6D6D00BFBFBF00E1E2
      E200B7B6B6006C6A6A006C6A6A006C6A6A000000000000000000000000000000
      000000000000AD807800FFEAD400E5A65700E5A65700E5A65700E5A65700E5A6
      5700E5A65700FFD4A8008C5D5C00000000000000000000000000387A9B00CDEC
      F600EFF7FA00E2EFF300C1DCE600A3CCDB0086BCD000D1ACA700F8980000C860
      0000FFFFFF00F8980000C860000069473100000000003C98E40098D8F8006394
      CA009B949A00AA888200AA888200AA888200AA88820086BCD0006AABC5004E9B
      BA00368BAF001C779F005050500000000000000000006C6A6A00D4D3D300CACA
      CA008E8C8C008E8C8C008E8C8C003C3B3B000A090A00070707000B0B0B000707
      07007A7A7A00BBBBBB006C6A6A00000000000000000000000000000000000000
      000000000000B4867A00FEEEDD00FFEBD600FFE8CF00FFE4C900FEE1C200FEDD
      BB00FFDBB500FFD8AF008C5D5C00000000000000000000000000387A9B00A7D0
      E100A5C6D60088B6CB0081B1C5006DA5BE00599AB600D1ACA700FFFFFF009830
      0000FFFFFF0098300000FFFFFF006947310000000000000000003C98E400AFB0
      B600AA888200F6E9C400FFEABD00FFF2D500FCF5DE00AA8882004791AF003388
      AD002983A90019759D0050505000000000006C6A6A00CACACA00CACACA008E8C
      8C00D7D4D400CECBCB00BFBCBC00B1AFAF00A3A0A000888686005E5B5C000707
      070009090900080808006C6A6A0076737300A4787400A4787400A4787400A478
      7400A4787400BA8D7D00FEF2E500E5A65700E5A65700E5A65700E5A65700E5A6
      5700E5A65700FEDCB7008C5D5C00000000000000000000000000387A9B00A5CA
      DA00D8E8EF00D3E6ED00C1DCE600A3CCDB0086BCD000D1ACA700FFFFFF00FFFF
      FF0098300000FFFFFF00FFFFFF0069473100000000000000000000000000AA88
      8200EFE0C000FFE1AF00FFECCC00FFF2DD00FFFBF500FDFBF300AA8882004897
      B7003188AD0017739B0050505000000000006C6A6A00CACACA008E8C8C00EFEE
      EE00FFFEFE00FBFAFA00E3E0E100DEDEDE00DEDDDD00CFCECE00BDBCBC00ADAB
      AB008B898900585656007A78780075737300A87C7500FEE5CB00FFE2C400FFDF
      BE00FFDCB800C2958100FEF6EC00FEF3E600FEEFE100FFEDDA00FEE9D400FEE6
      CC00FFE2C600FEDFBF008C5D5C00000000000000000000000000387A9B00CDEC
      F600EFF7FA00E2EFF300C1DCE600A3CCDB0086BCD000D1ACA700FFFFFF00C860
      0000C860000098300000FDFDFC0069473100000000000000000000000000AA88
      8200FBECC300FFDBA600FFE9C700FFEFD800FFF4E200FFF1D700AA8882004E9B
      BA00368BAF001C779F0050505000000000006C6A6A008E8C8C00FFFFFF00FEFC
      FC00FAFAFA00D5D4D50098919300A0989900B2ABAC00C4C0C100D7D7D700D8D8
      D800C7C6C600B7B6B600918F8F006C696900AD807800FFEAD400E5A65700E5A6
      5700E5A65700CA9B8300FFF9F300E5A65700E5A65700E5A65700E5A65700E5A6
      5700E5A65700FEE3C8008C5D5C00000000000000000000000000387A9B00CDEC
      F600EBF5F800B7D2DE0081B1C5006DA5BE00599AB600D1ACA700FFFFFF00F898
      0000C860000098300000FFFFFF0069473100000000000000000000000000AA88
      8200F9EEC900FFE6C200FFDFB300FFE8C600FFEAC900FFE4B200AA8882004694
      B500368BAF001C779F005050500000000000000000006C6A6A006C6A6A00EDEB
      EB00B1A6A7007A6F72008A83880096929500969091009D9798009A9395009E98
      9900BBBABA00D1D1D100C2C2C2006C6A6A00B4867A00FEEEDD00FFEBD600FFE8
      CF00FFE4C900D1A28600FEFBF900FEF9F400FEF7EF00FEF5EA00FEF1E400FEEE
      DE00FEEBD700FEE8D0008C5D5C00000000000000000000000000387A9B0085B5
      CA0092BACC00A4CADA00C1DCE600A3CCDB0086BCD000D1ACA700D1ACA700D1AC
      A700D1ACA700D1ACA700D1ACA700D1ACA700000000000000000000000000AA88
      8200E9DCCB00FEFEFB00FFE8C600FFDAA300FFDFA900F9EBC200AA888200368B
      AF002580A60015729A0050505000000000000000000000000000000000006C6A
      6A00BB897F00A7876D008B6F64007D6760006F626500797379008F8B8E00A9A3
      A400CBCACA00C1C1C1006C6A6A0000000000BA8D7D00FEF2E500E5A65700E5A6
      5700E5A65700D8A98A00FEFEFD00FEFCFA00FEFAF600FEF8F100FEF5EC00EBDF
      DB00D3C2C000BAA9AA008C5D5C00000000000000000000000000387A9B00C6E5
      F100EFF7FA00E2EFF300C1DCE600A3CCDB0086BCD0006AABC5004E9BBA00368B
      AF001B779E00505050000000000000000000000000000000000000000000387A
      9B00AA888200EBE0CE00FAEFCA00FCEDC300EFE0BF00AA8882006AABC5004E9B
      BA00368BAF001B779E0050505000000000000000000000000000000000000000
      0000BD828100FFE3B400FFD39F00E9B28100C9997300BA916C00BD828100807D
      7E006C6A6A006C6A6A000000000000000000C2958100FEF6EC00FEF3E600FEEF
      E100FFEDDA00DFB08D00FEFEFE00FEFEFE00FEFCFB00FEFBF700FEF8F200B481
      7600B4817600B4817600B17F7400000000000000000000000000387A9B00CDEC
      F600EDF6F900D7E8EE00B1D1DE0095C2D4007BB3CA0063A5C0004C99B800358B
      AF001C779F00505050000000000000000000000000000000000000000000387A
      9B00CDECF600AA888200AA888200AA888200AA8882007BB3CA0063A5C0004C99
      B800358BAF001C779F0050505000000000000000000000000000000000000000
      0000BD828100FFE0B800FFD3A700FFD09D00FFCE9000FFC68800BD8281000000
      000000000000000000000000000000000000CA9B8300FFF9F300E5A65700E5A6
      5700E5A65700E4B58E00FEFEFE00FEFEFE00FEFEFE00FEFDFC00FEFBF800B481
      7600EBB56F00E49B420000000000000000000000000000000000387A9B009AC8
      DA00619DB800619DB800619DB800619DB800619DB800619DB8002C87B000217A
      A1001A769E00505050000000000000000000000000000000000000000000387A
      9B009AC8DA00619DB800619DB800619DB800619DB800619DB800619DB8002C87
      B000217AA1001A769E005050500000000000000000000000000000000000C086
      8300FFE7CF00FFE0C000FFD9B200FFD3A500FFD09900BD828100000000000000
      000000000000000000000000000000000000D1A28600FEFBF900FEF9F400FEF7
      EF00FEF5EA00E8B89000DCA88700DCA88700DCA88700DCA88700DCA88700B481
      7600F0B25E000000000000000000000000000000000000000000387A9B004A90
      AE00F3F8FC00DCF0F900BDE4F400A1D9F00086CFEC006BC4E8004FB8E40035AB
      DD001989BB00505050000000000000000000000000000000000000000000387A
      9B004A90AE00F3F8FC00DCF0F900BDE4F400A1D9F00086CFEC006BC4E8004FB8
      E40035ABDD001989BB005050500000000000000000000000000000000000BD82
      8100FEEBD800FFE6CC00FFDEBD00FFD8B100FED3A400BD828100000000000000
      000000000000000000000000000000000000D8A98A00FEFEFD00FEFCFA00FEFA
      F600FEF8F100FEF5EC00EBDFDB00D3C2C000BAA9AA008C5D5C00000000000000
      0000000000000000000000000000000000000000000000000000387A9B00A1C2
      D200FAFBFE00F4F9FC00DCF0F900BFE5F400A3DBF10088D0ED006BC4E8004EB8
      E4002CA3D500707070000000000000000000000000000000000000000000387A
      9B00A1C2D200FAFBFE00F4F9FC00DCF0F900BFE5F400A3DBF10088D0ED006BC4
      E8004EB8E4002CA3D50070707000000000000000000000000000BD828100FFFF
      F200FFFFF200FFEBD800FFE5CA00FFE1BD00F3C7A700BD828100000000000000
      000000000000000000000000000000000000DFB08D00FEFEFE00FEFEFE00FEFC
      FB00FEFBF700FEF8F200B4817600B4817600B4817600B17F7400000000000000
      0000000000000000000000000000000000000000000000000000000000003E84
      A300A1C2D200DDE8EF00E0ECF300D0E7F200B3DBED0095CFE60060B6DA003399
      C5003E84A3000000000000000000000000000000000000000000000000000000
      00003E84A300A1C2D200DDE8EF00E0ECF300D0E7F200B3DBED0095CFE60060B6
      DA003399C5003E84A30000000000000000000000000000000000BD828100BD82
      8100BD828100FBEFE200FBE3CF00FBDDC200BD82810000000000000000000000
      000000000000000000000000000000000000E4B58E00FEFEFE00FEFEFE00FEFE
      FE00FEFDFC00FEFBF800B4817600EBB56F00E49B420000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004C90AF003E84A300387A9B00306B8500306B8500387A9B003E84A3004C90
      AF00000000000000000000000000000000000000000000000000000000000000
      0000000000004C90AF003E84A300387A9B00306B8500306B8500387A9B003E84
      A3004C90AF000000000000000000000000000000000000000000000000000000
      000000000000BD828100BD828100BD8281000000000000000000000000000000
      000000000000000000000000000000000000E8B89000DCA88700DCA88700DCA8
      8700DCA88700DCA88700B4817600F0B25E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C8B3A4006947
      3100694731006947310069473100694731006947310069473100694731006947
      310069473100183D600064687300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000069D
      CC0049B7E00060C1E80054BCE3003DB2DC004DB9E10000000000000000000000
      00000000000000000000000000000000000000000000C9762B00A24F2200A24F
      2200A24F2200A24F2200A24F2200A24F2200A24F2200A24F2200A24F2200A24F
      2200A24F2200A24F2200A24F2200A24F22000000000000000000C8B3A400EEE6
      E100B7A29300B7A29300B7A29300B7A29300B7A29300B7A29300B7A29300B7A2
      9300183D60004D5A91005171980064687300000000000000000000000000069D
      CC0049B7E00060C1E80054BCE3003DB2DC004DB9E10000000000000000000000
      000000000000000000000000000000000000000000000000000008A1CE000EA5
      D1007FD2F100A0DFFE0097DAFB0093D9FA007CCDEF0073CAEE0048B7E0000000
      00000000000000000000000000000000000000000000C9762B00FEFEFD00FEFE
      FD00FEFEFD00FEFEFD00FBFBFA00F8F8F700F5F5F400F1F1F000EEEEED00EBEB
      EA00E8E8E700DEDEDE00D2D2D200A24F22000000000000000000C8B3A400F1EA
      E600EEE6E100EBE2DD00E9DED800E6D9D300E3D5CE00E0D1C900DDCDC400183D
      60004D5A91004995E10098D8F8003C98E400000000000000000008A1CE000EA5
      D1007FD2F100A0DFFE0097DAFB0093D9FA007CCDEF0073CAEE0048B7E0000000
      000000000000000000000000000000000000000000000000000013A8D4001DAF
      D80083D8EE00A0E3FB0093DAF90082D0F20059BBE0008FD7F7009ADCFB0076CB
      F0000000000000000000000000000000000000000000C9762B00FEFEFD008686
      8600868686008686860086868600868686008686860086868600868686008686
      86008686860086868600DEDEDE00A24F22000000000000000000C8B3A400F4EE
      EB00F1EAE600EEE6E100EBE2DD00E9DED800E6D9D300E3D5CE00183D60004D5A
      9100468EDD0098D8F8003C98E40000000000000000000000000013A8D4001DAF
      D80083D8EE00A0E3FB0093DAF90082D0F20059BBE0008FD7F7009ADCFB0076CB
      F00000000000000000000000000000000000000000000000000020AFD8002EB7
      DE0094DFEF00B1EEFA00A2E7FD0064B2D100369BC0006FC7EB0095DAFB009ADC
      FB0034B1DA0000000000000000000000000000000000C9762B00FEFEFD008686
      8600FF860500FEFEFD00FEFEFD00FEFEFD00FBFBFA00F8F8F700F5F5F4000E3D
      C400EEEEED00FF860500E8E8E700A24F22000000000000000000C8B3A400F6F2
      F000F4EEEB00F1EAE600AA888200AA888200AA888200AA8882009B949A006394
      CA0098D8F8003C98E4006947310000000000000000000000000020AFD8002EB7
      DE0094DFEF00B1EEFA00A2E7FD0064B2D100369BC0006FC7EB0095DAFB009ADC
      FB0034B1DA0000000000000000000000000000000000000000002DB5DD003FBE
      E400B1E5F100CFF8FD00B6F4FF0083C9DE00287FA10069BDE00094DBFC0095DA
      FA0033B0D90000000000000000000000000000000000C9762B00FEFEFD008686
      8600FEFEFD00FF860500FEFEFD00FEFEFD00FEFEFD000E3DC400526AE400F5F5
      F400FF860500EEEEED00EBEBEA00A24F22000000000000000000C8B3A400F9F6
      F500E6D9D300AA888200F6E9C400FFEABD00FFF2D500FCF5DE00AA888200AFB0
      B6003C98E400B7A29300694731000000000000000000000000002DB5DD003FBE
      E400B1E5F100CFF8FD00B6F4FF0083C9DE00287FA10069BDE00094DBFC0095DA
      FA0033B0D900000000000000000000000000000000000000000039BBE30048C1
      E800C5E8F400ECFAFD00CAF5FB00B3ECF50063A9BF008BCFE80099DFFE0094D8
      FA0033B0D90000000000000000000000000000000000C9762B00FEFEFD008686
      86000E3DC400FFB25F000E3DC400FEFEFD000E3DC400FEFEFD00FBFBFA00FF86
      0500F5F5F400F1F1F000EEEEED00A24F22000000000000000000C8B3A400FCFB
      FA00AA888200EFE0C000FFE1AF00FFECCC00FFF2DD00FFFBF500FDFBF300AA88
      8200E3D5CE00B7A293006947310000000000000000000000000039BBE30048C1
      E800C5E8F400ECFAFD00CAF5FB00B3ECF50063A9BF008BCFE80099DFFE0094D8
      FA0033B0D90000000000000000000000000000000000000000002EB3DB0044C1
      E2005EC0DB006CC8E00062C8E20060C7E2006ECFE70086D8EE009DE2F900A2E1
      FE0034B1DA0000000000000000000000000000000000C9762B00FEFEFD008686
      8600FEFEFD00FEFEFD00FF8605000E3DC400FEFEFD00FEFEFD00FF860500FDFD
      FC00F8F8F700F5F5F400F1F1F000A24F22000000000000000000C8B3A400FFFF
      FF00AA888200FBECC300FFDBA600FFE9C700FFEFD800FFF4E200FFF1D700AA88
      8200E6D9D300B7A29300694731000000000000000000000000002EB3DB0044C1
      E2005EC0DB006CC8E00062C8E20060C7E2006ECFE70086D8EE009DE2F900A2E1
      FE0034B1DA00000000000000000000000000000000000000000029B3D80075DC
      E700328BA5003A9AB30069E1F00044CCE70021B5D90014AAD3002CAFD60079D0
      ED0037B3DA0000000000000000000000000000000000C9762B00FEFEFD008686
      8600FEFEFD00FEFEFD00FFB25F00FEFEFD00FEFEFD00FF860500FEFEFD00FEFE
      FD00FDFDFC00FAFAF900F6F6F500A24F22000000000000000000C8B3A400FFFF
      FF00AA888200F9EEC900FFE6C200FFDFB300FFE8C600FFEAC900FFE4B200AA88
      8200EAE0DA00B7A29300694731000000000000000000000000002BB1D80058CD
      E40040B8D70046BEDB0045C2DF0037B7D9004AC2E2006BCFEA009AE1F80086D4
      F40025AAD40000000000000000000000000000000000000000000000000030B2
      D6004FBBD10074D7E30098FDFF0080F1FA0050CFE60012A1C8000397C30013A4
      CF0018A5D00000000000000000000000000000000000C9762B00FEFEFD008686
      8600FEFEFD00FEFEFD00FEFEFD00FF860500FF860500FEFEFD00FEFEFD00FEFE
      FD00FEFEFD00FDFDFC00FAFAF900A24F22000000000000000000C8B3A400FFFF
      FF00AA888200E9DCCB00FEFEFB00FFE8C600FFDAA300FFDFA900F9EBC200AA88
      8200EDE4DF00B7A293006947310000000000000000000000000028B1D7005CCC
      DF000C9BC4003CB6D1008AF5FB0055D7EC0024B6DA00049DCA0025ABD20052C0
      E0001FA7D2000000000000000000000000000000000000000000000000000000
      0000000000000000000054CFE7004ACAE40045C1DD00129AC3000F98C20011A5
      D00011A2CF0000000000000000000000000000000000C9762B00FEFEFD008686
      8600FEFEFD00FEFEFD00FEFEFD00FFB25F00FEFEFD00FEFEFD00FEFEFD00FEFE
      FD00FEFEFD00FEFEFD00FDFDFC00A24F22000000000000000000C8B3A400FFFF
      FF00F6F2F000AA888200EBE0CE00FAEFCA00FCEDC300EFE0BF00AA888200E6D9
      D300D5BFB100B7A2930069473100000000000000000000000000000000000C94
      C1000091BF002FAECF0087F0F8007AECF80047C2DC000B96C000119DC4001CA9
      D000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000109CC5000894C0000000
      00000000000000000000000000000000000000000000C9762B00FEFEFD00FEFE
      FD00FEFEFD00FEFEFD00FEFEFD00FEFEFD00FEFEFD00FEFEFD00FEFEFD00FEFE
      FD00FEFEFD00FEFEFD00FEFEFD00A24F22000000000000000000C8B3A400FFFF
      FF00FFFFFF00F6F2F000AA888200AA888200AA888200AA888200E6D9D300C8B3
      A400694731006947310069473100000000000000000000000000000000000000
      00000B99C600199BC300000000003ABFE00020A7CD000A95C0000D96C2000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000078CB8000587B300000000000000000000000000089AC6000B91BC000000
      00000000000000000000000000000000000000000000C9762B00D06F0100D06F
      0100D06F0100D06F0100D06F0100D06F0100D06F0100D06F0100D06F0100D06F
      0100D06F0100D06F0100D06F0100A24F22000000000000000000C8B3A400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FDFDFD00FBF9F800C8B3
      A400F3EBE600DBC9BF0069473100000000000000000000000000000000000000
      00004FC3E6002A9DC6000000000000000000000000000994BF000C8AB5000000
      0000000000000000000000000000000000000000000000000000000000000000
      00002EB0DA001E9FCA00000000000000000000000000099CC8000B92BD000000
      00000000000000000000000000000000000000000000CE630000ED973300ED97
      3300ED973300ED973300ED973300ED973300ED973300F6CA9A00ED973300F6CA
      9A00ED973300306DF9007F748800CE6300000000000000000000C8B3A400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FDFDFD00C8B3
      A400DBC9BF006947310000000000000000000000000000000000000000000000
      00003BA6C60077DEEE003DAAC80000000000148AB6000C9DC800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004CB5D50068D0EA000000000000000000000000000999C5000A92BE000000
      0000000000000000000000000000000000000000000000000000DA7B0D00DA7B
      0D00DA7B0D00DA7B0D00DA7B0D00DA7B0D00DA7B0D00DA7B0D00DA7B0D00DA7B
      0D00DA7B0D00DA7B0D00DA7B0D00000000000000000000000000C8B3A400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C8B3
      A400694731000000000000000000000000000000000000000000000000000000
      0000000000004AB5CD007BE0EC006BD1E6004ABCE0001898C300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005BC4D90078DFEB004FB9D4003EAED2001DA6D100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C8B3A400C8B3
      A400C8B3A400C8B3A400C8B3A400C8B3A400C8B3A400C8B3A400C8B3A400C8B3
      A400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000055BFD40055C0D6004FBAD80000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A3684900A368
      4900A3684900A3684900A3684900A3684900A3684900A3684900A3684900A368
      4900A3684900A368490000000000000000000000000064687300183D60000000
      000000000000000000000000000000000000C2652B00A53C0000A53C0000A53C
      0000A53C0000A53C0000A53C0000A53C00000000000000000000000000000000
      000000000000000000000FA528000FA528000FA528000FA52800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0967700E6C5AB00E2BD
      A300E2BDA300E2BDA300E2BDA300D6A68800D7A68900D7A68900D29D7E00D098
      7900CB8F6700C2784C00A36849000000000064687300517198004D5A9100183D
      6000A22E0800A22E0800A22E0800A22E0800C2652B00FFD4A500FFD4A500FFD4
      A500FFCD9700FFC07D00FFAD5500A53C00000000000000000000000000000000
      000000000000000000000FA528007EE09E0051CD83000FA52800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0967700E6C5AB00E2BD
      A300F7D6A500BE7B5A00E2BDA300F7D6A500BE7B5A00D7A6890029C65A000063
      0800CB8F6700C7825C00A3684900000000003C98E40098D8F8004995E1004D5A
      9100183D6000000000000000000000000000C2652B00C2652B00C2652B00C265
      2B00C2652B00C2652B00C2652B00C2652B000000000000000000000000000000
      000000000000000000000FA528007EE09E0038C273000FA52800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0967700E6C5AB00E2BD
      A300FFF7E600F7D6A500E2BDA300FFF7E600F7D6A500D7A689009CE7B50029C6
      5A00CB8F6700C7825C00A368490000000000000000003C98E40098D8F800468E
      DD004D5A9100183D600000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000FA528007EE09E0038C273000FA52800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0967700E6C5AB00E2BD
      A300E2BDA300E2BDA300E2BDA300D6A68800D7A68900D7A68900D29D7E00D098
      7900CB8F6700C7825C00A36849000000000000000000000000003C98E40098D8
      F8006394CA009B949A00AA888200AA888200AA888200AA888200A53C0000A53C
      0000A53C0000A53C0000A53C0000A53C00000000000000000000000000000000
      000000000000000000000FA528007EE09E0038C273000FA52800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0967700E6C5AB00E2BD
      A300F7D6A500BE7B5A00E2BDA300F7D6A500BE7B5A00D7A68900FF9C0000BA5E
      0000CB8F6700C7825C00A3684900000000000000000000000000000000003C98
      E400AFB0B600AA888200F6E9C400FFEABD00FFCF9B00FCF5DE00AA888200FFD4
      A500FFCD9700FFC07D00FFAD5500A53C0000000000000FA528000FA528000FA5
      28000FA528000FA528000FA528007EE09E0038C273000FA528000FA528000FA5
      28000FA528000FA528000FA52800000000000000000000189000001890000018
      9000001890000018900000189000001890000018900000189000001890000018
      90000018900000189000001890000000000000000000D0967700E6C5AB00E2BD
      A300FFF7E600F7D6A500E2BDA300FFF7E600F7D6A500D7A68900FFCE7B00FF9C
      0000CB8F6700C7825C00A3684900000000000000000000000000000000000000
      0000AA888200EFE0C000FFE1AF00FFECCC00FFCF9B00FFCF9B00FFCF9B00AA88
      8200C2652B00C2652B00C2652B00C2652B00000000000FA528007EE09E0038C2
      730038C2730038C2730038C2730038C2730038C2730038C2730038C2730038C2
      730038C2730051CD83000FA528000000000000000000001890008B9EF1002952
      E7002952E7002952E7002952E7002952E7002952E7002952E7002952E7002952
      E7002952E7003D59DB00001890000000000000000000D0967700E6C5AB00E2BD
      A300E2BDA300E2BDA300E2BDA300D6A68800D7A68900D7A68900D29D7E00D098
      7900CB8F6700C7825C00A3684900000000000000000000000000000000000000
      0000AA888200FBECC300FFDBA600FFE9C700FFEFD800FFF4E200FFF1D700AA88
      820000000000000000000000000000000000000000000FA528005FD38B005FD3
      8B005FD38B005FD38B005FD38B0051CD830038C273005FD38B005FD38B005FD3
      8B005FD38B005FD38B000FA528000000000000000000001890008B9EF100718B
      ED00718BED00718BED00718BED00718BED00718BED00718BED00718BED00718B
      ED00718BED003D59DB00001890000000000000000000D0967700E6C5AB00E2BD
      A300F7D6A500BE7B5A00E2BDA300F7D6A500BE7B5A00D7A689001063D6001459
      A500CB8F6700C7825C00A3684900000000000000000000000000000000000000
      0000AA888200F9EEC900FFE6C200FFDFB300FFCF9B00FFCF9B00FFCF9B00AA88
      8200A53C0000A53C0000A53C0000A53C0000000000000FA528000FA528000FA5
      28000FA528000FA528000FA528007EE09E0038C273000FA528000FA528000FA5
      28000FA528000FA528000FA52800000000000000000000189000001890000018
      9000001890000018900000189000001890000018900000189000001890000018
      90000018900000189000001890000000000000000000D0967700E6C5AB00E2BD
      A300FFF7E600F7D6A500E2BDA300FFF7E600F7D6A500D7A6890085A6E0001063
      D600CB8F6700C7825C00A3684900000000000000000000000000000000000000
      0000AA888200E9DCCB00FEFEFB00FFE8C600FFCF9B00FFDFA900F9EBC200AA88
      8200FFCD9700FFC07D00FFAD5500A53C00000000000000000000000000000000
      000000000000000000000FA528007EE09E0038C273000FA52800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0967700E6C5AB00E2BD
      A300E2BDA300E2BDA300E2BDA300D6A68800D6A68800D6A68800D29D7E00D098
      7900CB8F6700C7825C00A3684900000000000000000000000000000000000000
      0000A22E0800AA888200EBE0CE00FAEFCA00FFCF9B00FFCF9B00AA888200C265
      2B00C2652B00C2652B00C2652B00C2652B000000000000000000000000000000
      000000000000000000000FA528007EE09E0038C273000FA52800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0967700B3848300CE94
      9400CE949400CE949400CE949400CE949400CE949400CE949400CE949400CE94
      9400CE949400C7825C00A3684900000000000000000000000000000000000000
      0000A22E080000000000AA888200AA888200AA888200AA888200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000FA528007EE09E0038C273000FA52800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0967700B3848300A5D0
      9E00A5D09E009ACC920091C989008AC6820081C27A007BBF730076BD6E0075BD
      6D00CE949400C7825C00A3684900000000000000000000000000C2652B00A22E
      0800A22E0800A22E0800A22E0800A22E0800A22E0800A22E1600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000FA5280051CD83005FD38B000FA52800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0967700B3848300A5D0
      9E00ADDFAD009ED99E0091D4910088D188007ECC7E0076C976006DC56D0068C4
      6800CE949400C7825C00A3684900000000000000000000000000C2652B00FFD4
      A500FFD4A500FFD4A500FFCD9700FFC07D00FFAD5500A22E0800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000FA528000FA528000FA528000FA52800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0967700B3848300B384
      8300B3848300B3848300B3848300B3848300B3848300B3848300B3848300B384
      8300B3848300C2784C00A3684900000000000000000000000000C2652B00C265
      2B00C2652B00C2652B00C2652B00C2652B00C2652B00C2652B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D0967700D096
      7700D0967700D0967700D0967700D0967700D0967700D0967700D0967700D096
      7700D0967700D096770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000004A5E860042557C00313F5B0000000000000000000000
      0000000000000000000000000000000000000000000000000000AEAEAE00999C
      9E00999C9E00999C9E00999C9E00999C9E00999C9E00999C9E00999C9E00999C
      9E00999C9E00999C9E00999C9E00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004A5E860042557C003861900038619000313F5B0000000000000000000000
      0000000000000000000000000000000000000000000000000000AEAEAE00F7F7
      F700F4F4F400F2F2F200EFEFEF00EDEDED00EAEAEA00E8E8E800E2E2E200DDDD
      DD00D7D7D700D2D2D2009C9FA100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005E5E5E005454
      5400B7B7B700B7B7B700B7B7B700B7B7B700B7B7B700B7B7B700B7B7B7005E5E
      5E005E5E5E0064646400000000000000000000000000000000004A5E86004255
      7C003F6797003C6594003962910038619000313F5B00394A6B00394A6B00394A
      6B00394A6B00394A6B00394A6B00000000000000000000000000AEAEAE00F9F9
      F900F7F7F700F4F4F400F2F2F200EFEFEF00EDEDED00EAEAEA00E8E8E800E2E2
      E200DDDDDD00D7D7D7009C9FA100000000000000000000000000238FBB00218B
      B7002187B2002187B2002187B2002187B2002187B2002187B20026983E002187
      B2002C8BB3002F8CB400000000000000000000000000515151006D6D6D005B5B
      5B00B7B7B700606060007777770052525200CBCBCB00D6D6D600DCDCDC005050
      500050505000585858003F3F3F000000000000000000000000004A5E8600446D
      9C00416A99003F6797003C6594003A639200313F5B003A577A0042638C004263
      8C0042638C0042638C00394A6B00000000000000000000000000AEAEAE00FCFC
      FC00F9F9F900F7F7F700F4F4F400F2F2F200EFEFEF00EDEDED00EAEAEA00E8E8
      E800E2E2E200DDDDDD009C9FA1000000000000000000248CB800259DCA002CAC
      DB0031AEDC0035B0DC0041B5DF004BB9E0004BB9E00021AD39004BCD6A001E78
      310083CFEA0083CFEA0039A5CE00000000000000000051515100717171005D5D
      5D00B7B7B700545454006060600052525200C5C5C500CBCBCB00D6D6D6004848
      48004848480055555500424242000000000000000000000000004A5E8600476F
      9F00446D9C00416A99003F6797003D669500313F5B000F4934000F4732000F46
      32000F45310010453100394A6B00000000000000000000000000AEAEAE00FEFE
      FE00FCFCFC00F9F9F900F7F7F700F4F4F400F2F2F200EFEFEF00EDEDED00EAEA
      EA00E8E8E800E2E2E2009C9FA1000000000000000000248CB80030A5D0004AB8
      E0005BC0E3006AC6E6007BCDE80086D1EA0021AD39004BCD6A0032A749002DA0
      41001E783100ADE0F10039A5CE00000000000000000051515100767676005E5E
      5E00B7B7B7004C4C4C005050500052525200BDBDBD00C5C5C500CBCBCB004242
      42004242420055555500454545000000000000000000000000004A5E86004A72
      A200476F9F00446D9C00416A990040699800313F5B00164D3800164A36001549
      35001646330015443100394A6B00000000000000000000000000AEAEAE00FFFF
      FF00FEFEFE00FCFCFC00F9F9F900F7F7F700F4F4F400F2F2F200EFEFEF00EDED
      ED00EAEAEA00E8E8E8009C9FA1000000000000000000248CB80037A8D10064C3
      E50080CEE9008CD3EB0096D7ED0021AD390021AD390021AD390021AD390021AD
      390021AD390026983E0039A5CE0000000000000000004F4F4F00797979006060
      6000B7B7B700B7B7B700B7B7B700B7B7B700BABABA00BDBDBD00C5C5C5003D3D
      3D003D3D3D0055555500494949000000000000000000000000004A5E86004D75
      A5004A72A200476F9F00446D9C00436B9B00313F5B001C7A5B001C7C5E001D7D
      60001E785B001E725700394A6B00000000000000000000000000AEAEAE00FFFF
      FF00FFFFFF00FEFEFE00FCFCFC00F9F9F900F7F7F700F4F4F400F2F2F200EFEF
      EF00EDEDED00EAEAEA009C9FA1000000000000000000248CB80039A5CE007ECE
      E9009DDAEE00A2DCEF00A8DEF000B7E4F300B7E4F30021AD39004BCD6A002288
      3500B7E4F300B7E4F30039A5CE000000000000000000515151007F7F7F006060
      6000606060005F5F5F005D5D5D005C5C5C005B5B5B005A5A5A00595959005757
      570056565600555555004C4C4C000000000000000000000000004A5E86004F77
      A7004D75A5006188B9006188B900466E9E00313F5B001F8360001E7958001E6E
      4F001D624500205E4300394A6B00000000000000000000000000AEAEAE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FDFDFD00FAFAFA00F8F8F800F5F5F500F3F3
      F300F0F0F000EDEDED009C9FA1000000000000000000248CB80039A5CE009DDA
      EE00A9DFF000AEE1F100B4E3F200B8E5F300BEE7F40021AD39004BCD6A002698
      3E00D4F0F800DAF2FA0039A5CE0000000000000000004F4F4F0099999800BDBD
      BD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00505050000000000000000000000000004A5E8600527A
      AA004F77A7006188B900FFFFFF004871A000313F5B005A766200798370009594
      8400AF9C8A00BB988700394A6B00000000000000000000000000AEAEAE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FDFDFD00FAFAFA00F8F8F800F5F5
      F500F3F3F300F0F0F0009C9FA1000000000000000000248CB80039A5CE00CFEE
      F800E6F8FC00E6F8FC00CEEEF700CDEDF700CDECD30021AD390026983E00B6DA
      BD00E7F8FC00ECFAFD0039A5CE00000000000000000057575700AFAFAF00F7F7
      F700F7F7F700F8F8F800F8F8F800F8F8F800F9F9F900F9F9F900FAFAFA00FBFB
      FB00FCFCFC00FEFEFE00535353000000000000000000000000004A5E8600567E
      AE00527AAA004F77A7004E76A6004B73A300313F5B00F0BAA400F0B29700F1BF
      A800F0BFA800F1BFA900394A6B00000000000000000000000000AEAEAE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FDFDFD00FAFAFA00F8F8
      F800F5F5F500EAEAEA009C9FA1000000000000000000248CB80039A5CE0039A5
      CE0039A5CE0039A5CE0029ADD600CDECD30021AD390026983E009ED1A900F0FB
      FE00F5FDFE00F7FEFE0039A5CE00000000000000000057575700B5B5B500F7F7
      F700D5D5D400B9B9B900B9B9B900B9B9B900B9B9B900B9B9B900B9B9B900B9B9
      B900EBEBEB00FEFEFE00565656000000000000000000000000004A5E86005A81
      B200567EAE00527AAA005179A9004E76A600313F5B00F0B79E00F2BFA900F4E0
      D700F2C6AE00F1A58100394A6B00000000000000000000000000AEAEAE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FDFDFD00EAEA
      EA00E2E2E200DDDDDD009C9FA1000000000000000000248CB8002BADD80036B1
      DD0054BDE3006EC7E70069C5E50021AD390026983E0039A5CE0039A5CE0039A5
      CE0039A5CE0039A5CE0039A5CE0000000000000000005E5E5E00BABABA00F6F6
      F600F7F7F700F7F7F700F7F7F700F7F7F700F8F8F800F8F8F800F9F9F900F9F9
      F900FBFBFB00FCFCFC005A5A5A000000000000000000000000004A5E86005D85
      B5005A81B200567EAE00547CAC005179A900313F5B00EE7B4500F0A27D00F1A9
      8600EF7D4500F0895600394A6B00000000000000000000000000AEAEAE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A8A9
      AA009C9FA1009C9FA1009C9FA1000000000000000000248CB80043B7DC0080CE
      EA0092D5ED009EDAEE00A8DEF0008DD4EA008DD4EA008DD4EA008CD3EA008CD3
      EA008CD3EA0054BDE3000000000000000000000000005E5E5E00BDBDBD00F6F6
      F600D5D5D400B9B9B900B9B9B900B9B9B900B9B9B900B9B9B900B9B9B900B9B9
      B900EBEBEA00FBFBFB005D5D5D000000000000000000000000004A5E86006188
      B9005D85B5005A81B200587FB000547CAC00313F5B00ED733B00EC6F3400EB6F
      3300EC6E3200EE865500394A6B00000000000000000000000000AEAEAE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A8A9
      AA00FFFFFF00E6E6E6009C9FA1000000000000000000248CB8004EB8DB00AADF
      F000C0E8F400E6F8FC00E6F8FC00CBECF60039A5CE0000000000000000000000
      0000000000000000000000000000000000000000000066666600C7C7C700F6F6
      F600F6F6F600F6F6F600F6F6F600F6F6F600F6F6F600F6F6F600F6F6F600F6F6
      F600F6F6F600F9F9F9006161610000000000000000000000000042557C004A5E
      86006188B9005D85B5005B83B300587FB000313F5B00E96F3C00E65F2400E767
      3100EE9D7B00ED946D00394A6B00000000000000000000000000AEAEAE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A8A9
      AA00E6E6E6009B9D9D000000000000000000000000000000000039A5CE0039A5
      CE0039A5CE0039A5CE0039A5CE0039A5CE000000000000000000000000000000
      0000000000000000000000000000000000000000000066666600CBCBCB00F5F5
      F500F5F5F500F6F6F600F6F6F600F6F6F600F6F6F600F7F7F700F7F7F700F7F7
      F700F8F8F800EBEBEA0063636300000000000000000000000000000000000000
      00004A5E860042557C005F86B7005B83B300313F5B00394A6B00394A6B00394A
      6B00394A6B00394A6B00394A6B00000000000000000000000000AEAEAE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A8A9
      AA009B9D9D000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000066666600ED91
      3100E68B2C00E0852600D97E2000D57A1C00D1761900CE731600C96E1300C66B
      1000C2670D006666660000000000000000000000000000000000000000000000
      000000000000000000004A5E860042557C00313F5B0000000000000000000000
      0000000000000000000000000000000000000000000000000000AEAEAE009B9B
      9C009B9B9C009B9B9C009B9B9C009B9B9C009B9B9C009B9B9C009B9B9C009C9F
      A100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000500000000100010000000000800200000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF00000000FFFFFFFF00000000
      F83FE00700000000F01FE00700000000E00FE00700000000C007E00700000000
      C007E00700000000C007E00700000000C007E00700000000C007E00700000000
      C007E00700000000C007E00700000000E00FE00F00000000F01FE01F00000000
      FFFFFFFF00000000FFFFFFFF00000000F00F9807F33FF801E0000003E007F801
      C0000001C000F801C00080018001F801C000C00100000001C000E00100000001
      C000E00100000001C000E00180000001C000E001E0010001C003E001F0030001
      C003E001F01F0003C003E001E03F0007C003E001E03F003FC003E001C03F003F
      E007F003C07F007FF00FF807F8FF00FFFFFFC001FFFFE07F8000C000E07FC01F
      8000C000C01FC00F8000C001C00FC0078000C001C007C0078000C001C007C007
      8000C001C007C0078000C001C007C0078000C001C007E0078000C001C007FC07
      8000C001E00FFF9F8000C001F21FF39F8000C001F39FF39F8000C003F13FF39F
      C001C007F83FF83FFFFFC00FFFFFFC7FFFFFFFFFC0039F00FC3FFFFF80010000
      FC3FFFFF80010700FC3FFFFF800183FFFC3FFFFF8001C000FC3FFFFF8001E000
      800180018001F000800180018001F00F800180018001F000800180018001F000
      FC3FFFFF8001F000FC3FFFFF8001F43FFC3FFFFF8001C03FFC3FFFFF8001C03F
      FC3FFFFF8001C03FFFFFFFFFC003FFFFFC7FC001FFFFFFFFF07FC001FFFFC003
      C001C001C0038001C001C00180018001C001C00180018001C001C00180018001
      C001C00180018001C001C00180018001C001C00180018001C001C00180018001
      C001C00180018001C001C00180038001C001C001807F8001C001C003C0FF8001
      F001C007FFFFC003FC7FC00FFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object TimerAutoSave: TTimer
    OnTimer = TimerAutoSaveTimer
    Left = 112
    Top = 56
  end
end
