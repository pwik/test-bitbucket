unit XtAeraulique_ChoixTypeEtude;

interface

uses
  Windows, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ImgList, Buttons, AdvGrid, BaseGrid, Grids;

type
  TXtAeraulique_FormChoixTypeEtude= class(TForm)
    Image1: TImage;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    Image2: TImage;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
  private
    FGRID: TAdvStringGrid;
  public
    { D�clarations publiques }
  end;

Function CreationEtude(_Grid: TAdvStringGrid): Integer; // Renvoie 0 Si annul� 1 Pour OK


var
  XtAeraulique_FormChoixTypeEtude: TXtAeraulique_FormChoixTypeEtude;

implementation
Uses Ventil_Declarations, Ventil_Const;

Function CreationEtude(_Grid: TAdvStringGrid): Integer;
Var
  SaveAlignFGrid : TAlign;
Begin
  Application.CreateForm(TXtAeraulique_FormChoixTypeEtude, XtAeraulique_FormChoixTypeEtude);
  XtAeraulique_FormChoixTypeEtude.FGRID := _Grid;
  SaveAlignFGrid := XtAeraulique_FormChoixTypeEtude.FGRID.Align;
  XtAeraulique_FormChoixTypeEtude.FGRID.Align := alClient;
  Etude.Batiment.Creating := True;
  If XtAeraulique_FormChoixTypeEtude.ShowModal = mrOk Then Result := 0
                                          Else Result := 1;
  Etude.Batiment.Creating := False;
  XtAeraulique_FormChoixTypeEtude.FGRID.Align := SaveAlignFGrid;
  _Grid.Parent := nil;
  FreeAndNil(XtAeraulique_FormChoixTypeEtude);
End;

{$R *.dfm}

procedure TXtAeraulique_FormChoixTypeEtude.FormShow(Sender: TObject);
begin
  FGRID.Parent := Panel1;
  Etude.Batiment.Affiche(FGRID);
//fait planter lors de la cr�ation d'une nouvelle �tude alors qu'une �tude existante est en cours...
//ne sait pas pourquoi un tel plantage...!
try
  FGRID.Row := 1;
except
end;
end;

procedure TXtAeraulique_FormChoixTypeEtude.RadioButton1Click(Sender: TObject);
begin
  If RadioButton1.Checked Then Etude.Batiment.TypeBat := c_BatimentCollectif
                          Else Etude.Batiment.TypeBat := c_BatimentTertiaire;

        if Etude.Batiment.TypeBat = c_BatimentTertiaire then
                Etude.Batiment.CodeFabricant := 'VIM';

  Etude.Batiment.Affiche(FGRID);
  FGRID.ROW := 1;
end;

end.
