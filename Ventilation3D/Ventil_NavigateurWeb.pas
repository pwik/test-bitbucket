unit Ventil_NavigateurWeb;
      
{$Include 'Ventil_Directives.pas'}

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, OleCtrls, SHDocVw, AdvToolBar, AdvToolBarStylers,
  ImgList,
  Ventil_Const{$IfDef VER310}, System.ImageList {$EndIf};

type
  TFic_Navigateur = class(TForm)
    AdvToolBarOfficeStyler1: TAdvToolBarOfficeStyler;
    AdvDockPanel1: TAdvDockPanel;
    AdvToolBar1: TAdvToolBar;
    Bouton_Back: TAdvToolBarButton;
    Navigateur: TWebBrowser;
    ImageList2: TImageList;
    Bouton_Home: TAdvToolBarButton;
    Bouton_Next: TAdvToolBarButton;
    AdvToolBar2: TAdvToolBar;
    ButtonPrint: TAdvToolBarButton;
    BitBtn1: TBitBtn;
    Panel1: TPanel;
    procedure Bouton_BackClick(Sender: TObject);
    procedure NavigateurCommandStateChange(Sender: TObject; Command: Integer; Enable: WordBool);
    procedure ButtonPrintClick(Sender: TObject);
    procedure Panel1Resize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    HomePage: String;
  public
    { Public declarations }
  end;

var
  Fic_Navigateur: TFic_Navigateur;

Procedure Navigation(_Page: String; _NavigateurInterne : Boolean = False);
Procedure Aide(_Rubrique: Integer = Help_Accueil);
procedure AidePDF;

implementation

uses
  ShellAPI;
{$R *.dfm}

{ ************************************************************************************************************************************************** }
Procedure Navigation(_Page: String; _NavigateurInterne : Boolean = False);
Begin
  if _NavigateurInterne then
    begin
    Application.CreateForm(TFic_Navigateur, Fic_Navigateur);
    Fic_Navigateur.HomePage := _Page;
    Fic_Navigateur.Navigateur.Navigate(_Page);
    Fic_Navigateur.AdvToolBar1.Caption := cst_NomLogiciel;
    Fic_Navigateur.ShowModal;
    end
  else
  ShellExecute(0,'OPEN',PChar(_Page), Nil, Nil, SW_SHOW);

End;
{ ************************************************************************************************************************************************** }
Procedure TFic_Navigateur.Bouton_BackClick(Sender: TObject);
Begin
  Try
    Case TWinControl(Sender).Tag Of
      1: Navigateur.GoBack;
      2: Navigateur.GoForward;
      3: Navigateur.Navigate(HomePage);
    End;
  Except End;
End;
{ ************************************************************************************************************************************************** }
procedure TFic_Navigateur.NavigateurCommandStateChange(Sender: TObject; Command: Integer; Enable: WordBool);
begin
  case Command of
    CSC_NAVIGATEBACK   : Bouton_Back.Enabled := Enable;
    CSC_NAVIGATEFORWARD: Bouton_Next.Enabled := Enable;
  end;
end;
{ ************************************************************************************************************************************************** }
Procedure Aide(_Rubrique: Integer = Help_Accueil);
Begin
  If Fic_Navigateur = Nil Then Application.CreateForm(TFic_Navigateur, Fic_Navigateur);
  Case _Rubrique Of
    Help_Accueil   : Fic_Navigateur.HomePage := ExtractFilePath(Application.ExeName) + 'Hlp\Accueil.htm';
    Help_Etude     : Fic_Navigateur.HomePage := ExtractFilePath(Application.ExeName) + 'Hlp\Pages\Etude.htm';
    Help_Chiffrage : Fic_Navigateur.HomePage := ExtractFilePath(Application.ExeName) + 'Hlp\Pages\Chiffrage.htm';
    Help_CW5       : Fic_Navigateur.HomePage := ExtractFilePath(Application.ExeName) + 'Aide\Aer\Tutaer.html';
  End;
  Fic_Navigateur.Navigateur.Navigate(Fic_Navigateur.HomePage);
  Fic_Navigateur.ShowModal;
End;
{ ************************************************************************************************************************************************** }
procedure AidePDF;

begin
//Navigation(ExtractFilePath(Application.ExeName) + 'hlp\Aide.pdf');
//exit;
//  ShellExecute(, Nil, PCHAR(ExtractFilePath(Application.ExeName) + 'hlp\Aide.pdf'), nil, nil, SW_SHOWNORMAL);
end;
{ ************************************************************************************************************************************************** }

procedure TFic_Navigateur.ButtonPrintClick(Sender: TObject);
begin
  Navigateur.ExecWB(OLECMDID_PRINT, OLECMDEXECOPT_DODEFAULT);
end;
{ ************************************************************************************************************************************************** }

procedure TFic_Navigateur.Panel1Resize(Sender: TObject);
begin
BitBtn1.Left := round((Panel1.Width - BitBtn1.Width) / 2);
end;
{ ************************************************************************************************************************************************** }
procedure TFic_Navigateur.FormCreate(Sender: TObject);
begin
  Fic_Navigateur.AdvToolBar1.Caption := cst_NomLogiciel;
  Fic_Navigateur.WindowState := wsMaximized;
end;
{ ************************************************************************************************************************************************** }
end.
