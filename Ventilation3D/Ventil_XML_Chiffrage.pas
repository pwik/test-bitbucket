
{**********************************************************************************************}
{                                                                                              }
{                                    Liaison de donn�es XML                                    }
{                                                                                              }
{         G�n�r� le : 05/07/2018 08:38:37                                                      }
{       G�n�r� depuis : D:\Developpement\DelphSou\Ventilation3D\XSD\Ventil_XML_Chiffrage.xsd   }
{                                                                                              }
{**********************************************************************************************}

unit Ventil_XML_Chiffrage;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ D�cl. Forward }

  IXMLCorresp_Chiffrage = interface;
  IXMLProduits = interface;
  IXMLProduit = interface;
  IXMLGammes = interface;
  IXMLGamme = interface;
  IXMLBouchesEquivalentes = interface;
  IXMLBoucheEquivalente = interface;
  IXMLElementChiffr = interface;
  IXMLBouche = interface;
  IXMLDomaine = interface;
  IXMLEAs_Acoustiques = interface;
  IXMLEA_Acoustique = interface;
  IXMLEA_AcoustiqueList = interface;
  IXMLAccessoires = interface;
  IXMLMethode_0_DN = interface;
  IXMLMethode_1_Classement_Debit = interface;

{ IXMLCorresp_Chiffrage }

  IXMLCorresp_Chiffrage = interface(IXMLNode)
    ['{0974F092-7A33-456F-90AA-AC991BA99C8F}']
    { Accesseurs de propri�t�s }
    function Get_Produits: IXMLProduits;
    function Get_Gammes: IXMLGammes;
    function Get_BouchesEquivalentes: IXMLBouchesEquivalentes;
    function Get_EAs_Acoustiques: IXMLEAs_Acoustiques;
    { M�thodes & propri�t�s }
    property Produits: IXMLProduits read Get_Produits;
    property Gammes: IXMLGammes read Get_Gammes;
    property BouchesEquivalentes: IXMLBouchesEquivalentes read Get_BouchesEquivalentes;
    property EAs_Acoustiques: IXMLEAs_Acoustiques read Get_EAs_Acoustiques;
  end;

{ IXMLProduits }

  IXMLProduits = interface(IXMLNodeCollection)
    ['{177E6E06-9EE8-4D3D-A4B9-B0489E57940D}']
    { Accesseurs de propri�t�s }
    function Get_Produit(Index: Integer): IXMLProduit;
    { M�thodes & propri�t�s }
    function Add: IXMLProduit;
    function Insert(const Index: Integer): IXMLProduit;
    property Produit[Index: Integer]: IXMLProduit read Get_Produit; default;
  end;

{ IXMLProduit }

  IXMLProduit = interface(IXMLNode)
    ['{14C908F7-AF10-4E2E-8D0D-B94D415A452E}']
    { Accesseurs de propri�t�s }
    function Get_Code_Logiciel: UnicodeString;
    function Get_Code_Aerau: UnicodeString;
    function Get_Code_Article: UnicodeString;
    procedure Set_Code_Logiciel(Value: UnicodeString);
    procedure Set_Code_Aerau(Value: UnicodeString);
    procedure Set_Code_Article(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property Code_Logiciel: UnicodeString read Get_Code_Logiciel write Set_Code_Logiciel;
    property Code_Aerau: UnicodeString read Get_Code_Aerau write Set_Code_Aerau;
    property Code_Article: UnicodeString read Get_Code_Article write Set_Code_Article;
  end;

{ IXMLGammes }

  IXMLGammes = interface(IXMLNodeCollection)
    ['{84665B28-9AF4-489D-9B8B-B336EE0B39FE}']
    { Accesseurs de propri�t�s }
    function Get_Gamme(Index: Integer): IXMLGamme;
    { M�thodes & propri�t�s }
    function Add: IXMLGamme;
    function Insert(const Index: Integer): IXMLGamme;
    property Gamme[Index: Integer]: IXMLGamme read Get_Gamme; default;
  end;

{ IXMLGamme }

  IXMLGamme = interface(IXMLNode)
    ['{1E8BAFAE-3F01-42BC-AD0D-67F2B91CD4D2}']
    { Accesseurs de propri�t�s }
    function Get_Code_Logiciel: UnicodeString;
    function Get_Code_Aerau: UnicodeString;
    procedure Set_Code_Logiciel(Value: UnicodeString);
    procedure Set_Code_Aerau(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property Code_Logiciel: UnicodeString read Get_Code_Logiciel write Set_Code_Logiciel;
    property Code_Aerau: UnicodeString read Get_Code_Aerau write Set_Code_Aerau;
  end;

{ IXMLBouchesEquivalentes }

  IXMLBouchesEquivalentes = interface(IXMLNodeCollection)
    ['{ABB2F4EE-EE0F-49FA-A5C6-21968D25B74A}']
    { Accesseurs de propri�t�s }
    function Get_BoucheEquivalente(Index: Integer): IXMLBoucheEquivalente;
    { M�thodes & propri�t�s }
    function Add: IXMLBoucheEquivalente;
    function Insert(const Index: Integer): IXMLBoucheEquivalente;
    property BoucheEquivalente[Index: Integer]: IXMLBoucheEquivalente read Get_BoucheEquivalente; default;
  end;

{ IXMLBoucheEquivalente }

  IXMLBoucheEquivalente = interface(IXMLNode)
    ['{32B37C60-0279-4AB8-A229-9C316C190933}']
    { Accesseurs de propri�t�s }
    function Get_BoucheBase: IXMLBouche;
    function Get_Neuf: IXMLDomaine;
    function Get_Renovation: IXMLDomaine;
    { M�thodes & propri�t�s }
    property BoucheBase: IXMLBouche read Get_BoucheBase;
    property Neuf: IXMLDomaine read Get_Neuf;
    property Renovation: IXMLDomaine read Get_Renovation;
  end;

{ IXMLElementChiffr }

  IXMLElementChiffr = interface(IXMLNode)
    ['{4B033AA6-88B2-465F-B10F-8F522894EDB2}']
    { Accesseurs de propri�t�s }
    function Get_Code_Aerau: UnicodeString;
    function Get_Code_Article: UnicodeString;
    procedure Set_Code_Aerau(Value: UnicodeString);
    procedure Set_Code_Article(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property Code_Aerau: UnicodeString read Get_Code_Aerau write Set_Code_Aerau;
    property Code_Article: UnicodeString read Get_Code_Article write Set_Code_Article;
  end;

{ IXMLBouche }

  IXMLBouche = interface(IXMLElementChiffr)
    ['{DE163FCC-FFD9-4873-9EF2-D0FDCBEDFE7C}']
  end;

{ IXMLDomaine }

  IXMLDomaine = interface(IXMLNode)
    ['{59AF82A1-30C9-49BD-8843-1474B8525ED7}']
    { Accesseurs de propri�t�s }
    function Get_Cordon: IXMLBouche;
    function Get_Pile: IXMLBouche;
    function Get_Elec: IXMLBouche;
    function Get_DouzeVolts: IXMLBouche;
    function Get_CordonNonMinute: IXMLBouche;
    function Get_ThAir: IXMLBouche;
    { M�thodes & propri�t�s }
    property Cordon: IXMLBouche read Get_Cordon;
    property Pile: IXMLBouche read Get_Pile;
    property Elec: IXMLBouche read Get_Elec;
    property DouzeVolts: IXMLBouche read Get_DouzeVolts;
    property CordonNonMinute: IXMLBouche read Get_CordonNonMinute;
    property ThAir: IXMLBouche read Get_ThAir;
  end;

{ IXMLEAs_Acoustiques }

  IXMLEAs_Acoustiques = interface(IXMLNode)
    ['{A1EBF52B-3399-4EDC-BF83-2E44CABA7421}']
    { Accesseurs de propri�t�s }
    function Get_EA_Acoustique: IXMLEA_AcoustiqueList;
    function Get_Methode_Acoustique: Integer;
    procedure Set_Methode_Acoustique(Value: Integer);
    { M�thodes & propri�t�s }
    property EA_Acoustique: IXMLEA_AcoustiqueList read Get_EA_Acoustique;
    property Methode_Acoustique: Integer read Get_Methode_Acoustique write Set_Methode_Acoustique;
  end;

{ IXMLEA_Acoustique }

  IXMLEA_Acoustique = interface(IXMLNode)
    ['{F934EAD2-6A3C-450D-BDD1-4167F9BBF97C}']
    { Accesseurs de propri�t�s }
    function Get_Reference: UnicodeString;
    function Get_Type_Pose: Integer;
    function Get_EA_Chiffrage: IXMLElementChiffr;
    function Get_Accessoires: IXMLAccessoires;
    function Get_Methode_0_DN: IXMLMethode_0_DN;
    function Get_Methode_1_Classement_Debit: IXMLMethode_1_Classement_Debit;
    procedure Set_Reference(Value: UnicodeString);
    procedure Set_Type_Pose(Value: Integer);
    { M�thodes & propri�t�s }
    property Reference: UnicodeString read Get_Reference write Set_Reference;
    property Type_Pose: Integer read Get_Type_Pose write Set_Type_Pose;
    property EA_Chiffrage: IXMLElementChiffr read Get_EA_Chiffrage;
    property Accessoires: IXMLAccessoires read Get_Accessoires;
    property Methode_0_DN: IXMLMethode_0_DN read Get_Methode_0_DN;
    property Methode_1_Classement_Debit: IXMLMethode_1_Classement_Debit read Get_Methode_1_Classement_Debit;
  end;

{ IXMLEA_AcoustiqueList }

  IXMLEA_AcoustiqueList = interface(IXMLNodeCollection)
    ['{51390CD6-5E25-4D86-9B66-D2E94617E84C}']
    { M�thodes & propri�t�s }
    function Add: IXMLEA_Acoustique;
    function Insert(const Index: Integer): IXMLEA_Acoustique;

    function Get_Item(Index: Integer): IXMLEA_Acoustique;
    property Items[Index: Integer]: IXMLEA_Acoustique read Get_Item; default;
  end;

{ IXMLAccessoires }

  IXMLAccessoires = interface(IXMLNodeCollection)
    ['{5CD97392-09F8-40CF-909F-E1DC7326AB2C}']
    { Accesseurs de propri�t�s }
    function Get_Accessoire(Index: Integer): IXMLElementChiffr;
    { M�thodes & propri�t�s }
    function Add: IXMLElementChiffr;
    function Insert(const Index: Integer): IXMLElementChiffr;
    property Accessoire[Index: Integer]: IXMLElementChiffr read Get_Accessoire; default;
  end;

{ IXMLMethode_0_DN }

  IXMLMethode_0_DN = interface(IXMLNode)
    ['{A7E5CAE3-0621-44F5-8D6B-A39A73BD1D89}']
    { Accesseurs de propri�t�s }
    function Get_EA_Base: IXMLElementChiffr;
    function Get_Numero: Integer;
    function Get_DN: Integer;
    procedure Set_Numero(Value: Integer);
    procedure Set_DN(Value: Integer);
    { M�thodes & propri�t�s }
    property EA_Base: IXMLElementChiffr read Get_EA_Base;
    property Numero: Integer read Get_Numero write Set_Numero;
    property DN: Integer read Get_DN write Set_DN;
  end;

{ IXMLMethode_1_Classement_Debit }

  IXMLMethode_1_Classement_Debit = interface(IXMLNode)
    ['{8345529F-CBC9-4010-96A9-110FA66207E1}']
    { Accesseurs de propri�t�s }
    function Get_Classe_Debit: Integer;
    function Get_Classement: Integer;
    procedure Set_Classe_Debit(Value: Integer);
    procedure Set_Classement(Value: Integer);
    { M�thodes & propri�t�s }
    property Classe_Debit: Integer read Get_Classe_Debit write Set_Classe_Debit;
    property Classement: Integer read Get_Classement write Set_Classement;
  end;

{ D�cl. Forward }

  TXMLCorresp_Chiffrage = class;
  TXMLProduits = class;
  TXMLProduit = class;
  TXMLGammes = class;
  TXMLGamme = class;
  TXMLBouchesEquivalentes = class;
  TXMLBoucheEquivalente = class;
  TXMLElementChiffr = class;
  TXMLBouche = class;
  TXMLDomaine = class;
  TXMLEAs_Acoustiques = class;
  TXMLEA_Acoustique = class;
  TXMLEA_AcoustiqueList = class;
  TXMLAccessoires = class;
  TXMLMethode_0_DN = class;
  TXMLMethode_1_Classement_Debit = class;

{ TXMLCorresp_Chiffrage }

  TXMLCorresp_Chiffrage = class(TXMLNode, IXMLCorresp_Chiffrage)
  protected
    { IXMLCorresp_Chiffrage }
    function Get_Produits: IXMLProduits;
    function Get_Gammes: IXMLGammes;
    function Get_BouchesEquivalentes: IXMLBouchesEquivalentes;
    function Get_EAs_Acoustiques: IXMLEAs_Acoustiques;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLProduits }

  TXMLProduits = class(TXMLNodeCollection, IXMLProduits)
  protected
    { IXMLProduits }
    function Get_Produit(Index: Integer): IXMLProduit;
    function Add: IXMLProduit;
    function Insert(const Index: Integer): IXMLProduit;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLProduit }

  TXMLProduit = class(TXMLNode, IXMLProduit)
  protected
    { IXMLProduit }
    function Get_Code_Logiciel: UnicodeString;
    function Get_Code_Aerau: UnicodeString;
    function Get_Code_Article: UnicodeString;
    procedure Set_Code_Logiciel(Value: UnicodeString);
    procedure Set_Code_Aerau(Value: UnicodeString);
    procedure Set_Code_Article(Value: UnicodeString);
  end;

{ TXMLGammes }

  TXMLGammes = class(TXMLNodeCollection, IXMLGammes)
  protected
    { IXMLGammes }
    function Get_Gamme(Index: Integer): IXMLGamme;
    function Add: IXMLGamme;
    function Insert(const Index: Integer): IXMLGamme;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLGamme }

  TXMLGamme = class(TXMLNode, IXMLGamme)
  protected
    { IXMLGamme }
    function Get_Code_Logiciel: UnicodeString;
    function Get_Code_Aerau: UnicodeString;
    procedure Set_Code_Logiciel(Value: UnicodeString);
    procedure Set_Code_Aerau(Value: UnicodeString);
  end;

{ TXMLBouchesEquivalentes }

  TXMLBouchesEquivalentes = class(TXMLNodeCollection, IXMLBouchesEquivalentes)
  protected
    { IXMLBouchesEquivalentes }
    function Get_BoucheEquivalente(Index: Integer): IXMLBoucheEquivalente;
    function Add: IXMLBoucheEquivalente;
    function Insert(const Index: Integer): IXMLBoucheEquivalente;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLBoucheEquivalente }

  TXMLBoucheEquivalente = class(TXMLNode, IXMLBoucheEquivalente)
  protected
    { IXMLBoucheEquivalente }
    function Get_BoucheBase: IXMLBouche;
    function Get_Neuf: IXMLDomaine;
    function Get_Renovation: IXMLDomaine;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLElementChiffr }

  TXMLElementChiffr = class(TXMLNode, IXMLElementChiffr)
  protected
    { IXMLElementChiffr }
    function Get_Code_Aerau: UnicodeString;
    function Get_Code_Article: UnicodeString;
    procedure Set_Code_Aerau(Value: UnicodeString);
    procedure Set_Code_Article(Value: UnicodeString);
  end;

{ TXMLBouche }

  TXMLBouche = class(TXMLElementChiffr, IXMLBouche)
  protected
    { IXMLBouche }
  end;

{ TXMLDomaine }

  TXMLDomaine = class(TXMLNode, IXMLDomaine)
  protected
    { IXMLDomaine }
    function Get_Cordon: IXMLBouche;
    function Get_Pile: IXMLBouche;
    function Get_Elec: IXMLBouche;
    function Get_DouzeVolts: IXMLBouche;
    function Get_CordonNonMinute: IXMLBouche;
    function Get_ThAir: IXMLBouche;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEAs_Acoustiques }

  TXMLEAs_Acoustiques = class(TXMLNode, IXMLEAs_Acoustiques)
  private
    FEA_Acoustique: IXMLEA_AcoustiqueList;
  protected
    { IXMLEAs_Acoustiques }
    function Get_EA_Acoustique: IXMLEA_AcoustiqueList;
    function Get_Methode_Acoustique: Integer;
    procedure Set_Methode_Acoustique(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEA_Acoustique }

  TXMLEA_Acoustique = class(TXMLNode, IXMLEA_Acoustique)
  protected
    { IXMLEA_Acoustique }
    function Get_Reference: UnicodeString;
    function Get_Type_Pose: Integer;
    function Get_EA_Chiffrage: IXMLElementChiffr;
    function Get_Accessoires: IXMLAccessoires;
    function Get_Methode_0_DN: IXMLMethode_0_DN;
    function Get_Methode_1_Classement_Debit: IXMLMethode_1_Classement_Debit;
    procedure Set_Reference(Value: UnicodeString);
    procedure Set_Type_Pose(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEA_AcoustiqueList }

  TXMLEA_AcoustiqueList = class(TXMLNodeCollection, IXMLEA_AcoustiqueList)
  protected
    { IXMLEA_AcoustiqueList }
    function Add: IXMLEA_Acoustique;
    function Insert(const Index: Integer): IXMLEA_Acoustique;

    function Get_Item(Index: Integer): IXMLEA_Acoustique;
  end;

{ TXMLAccessoires }

  TXMLAccessoires = class(TXMLNodeCollection, IXMLAccessoires)
  protected
    { IXMLAccessoires }
    function Get_Accessoire(Index: Integer): IXMLElementChiffr;
    function Add: IXMLElementChiffr;
    function Insert(const Index: Integer): IXMLElementChiffr;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLMethode_0_DN }

  TXMLMethode_0_DN = class(TXMLNode, IXMLMethode_0_DN)
  protected
    { IXMLMethode_0_DN }
    function Get_EA_Base: IXMLElementChiffr;
    function Get_Numero: Integer;
    function Get_DN: Integer;
    procedure Set_Numero(Value: Integer);
    procedure Set_DN(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLMethode_1_Classement_Debit }

  TXMLMethode_1_Classement_Debit = class(TXMLNode, IXMLMethode_1_Classement_Debit)
  protected
    { IXMLMethode_1_Classement_Debit }
    function Get_Classe_Debit: Integer;
    function Get_Classement: Integer;
    procedure Set_Classe_Debit(Value: Integer);
    procedure Set_Classement(Value: Integer);
  end;

{ Fonctions globales }

function GetCorresp_Chiffrage(Doc: IXMLDocument): IXMLCorresp_Chiffrage;
function LoadCorresp_Chiffrage(const FileName: string): IXMLCorresp_Chiffrage;
function NewCorresp_Chiffrage: IXMLCorresp_Chiffrage;

const
  TargetNamespace = '';

implementation

{ Fonctions globales }

function GetCorresp_Chiffrage(Doc: IXMLDocument): IXMLCorresp_Chiffrage;
begin
  Result := Doc.GetDocBinding('Corresp_Chiffrage', TXMLCorresp_Chiffrage, TargetNamespace) as IXMLCorresp_Chiffrage;
end;

function LoadCorresp_Chiffrage(const FileName: string): IXMLCorresp_Chiffrage;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Corresp_Chiffrage', TXMLCorresp_Chiffrage, TargetNamespace) as IXMLCorresp_Chiffrage;
end;

function NewCorresp_Chiffrage: IXMLCorresp_Chiffrage;
begin
  Result := NewXMLDocument.GetDocBinding('Corresp_Chiffrage', TXMLCorresp_Chiffrage, TargetNamespace) as IXMLCorresp_Chiffrage;
end;

{ TXMLCorresp_Chiffrage }

procedure TXMLCorresp_Chiffrage.AfterConstruction;
begin
  RegisterChildNode('Produits', TXMLProduits);
  RegisterChildNode('Gammes', TXMLGammes);
  RegisterChildNode('BouchesEquivalentes', TXMLBouchesEquivalentes);
  RegisterChildNode('EAs_Acoustiques', TXMLEAs_Acoustiques);
  inherited;
end;

function TXMLCorresp_Chiffrage.Get_Produits: IXMLProduits;
begin
  Result := ChildNodes['Produits'] as IXMLProduits;
end;

function TXMLCorresp_Chiffrage.Get_Gammes: IXMLGammes;
begin
  Result := ChildNodes['Gammes'] as IXMLGammes;
end;

function TXMLCorresp_Chiffrage.Get_BouchesEquivalentes: IXMLBouchesEquivalentes;
begin
  Result := ChildNodes['BouchesEquivalentes'] as IXMLBouchesEquivalentes;
end;

function TXMLCorresp_Chiffrage.Get_EAs_Acoustiques: IXMLEAs_Acoustiques;
begin
  Result := ChildNodes['EAs_Acoustiques'] as IXMLEAs_Acoustiques;
end;

{ TXMLProduits }

procedure TXMLProduits.AfterConstruction;
begin
  RegisterChildNode('Produit', TXMLProduit);
  ItemTag := 'Produit';
  ItemInterface := IXMLProduit;
  inherited;
end;

function TXMLProduits.Get_Produit(Index: Integer): IXMLProduit;
begin
  Result := List[Index] as IXMLProduit;
end;

function TXMLProduits.Add: IXMLProduit;
begin
  Result := AddItem(-1) as IXMLProduit;
end;

function TXMLProduits.Insert(const Index: Integer): IXMLProduit;
begin
  Result := AddItem(Index) as IXMLProduit;
end;

{ TXMLProduit }

function TXMLProduit.Get_Code_Logiciel: UnicodeString;
begin
  Result := ChildNodes['Code_Logiciel'].Text;
end;

procedure TXMLProduit.Set_Code_Logiciel(Value: UnicodeString);
begin
  ChildNodes['Code_Logiciel'].NodeValue := Value;
end;

function TXMLProduit.Get_Code_Aerau: UnicodeString;
begin
  Result := ChildNodes['Code_Aerau'].Text;
end;

procedure TXMLProduit.Set_Code_Aerau(Value: UnicodeString);
begin
  ChildNodes['Code_Aerau'].NodeValue := Value;
end;

function TXMLProduit.Get_Code_Article: UnicodeString;
begin
  Result := ChildNodes['Code_Article'].Text;
end;

procedure TXMLProduit.Set_Code_Article(Value: UnicodeString);
begin
  ChildNodes['Code_Article'].NodeValue := Value;
end;

{ TXMLGammes }

procedure TXMLGammes.AfterConstruction;
begin
  RegisterChildNode('Gamme', TXMLGamme);
  ItemTag := 'Gamme';
  ItemInterface := IXMLGamme;
  inherited;
end;

function TXMLGammes.Get_Gamme(Index: Integer): IXMLGamme;
begin
  Result := List[Index] as IXMLGamme;
end;

function TXMLGammes.Add: IXMLGamme;
begin
  Result := AddItem(-1) as IXMLGamme;
end;

function TXMLGammes.Insert(const Index: Integer): IXMLGamme;
begin
  Result := AddItem(Index) as IXMLGamme;
end;

{ TXMLGamme }

function TXMLGamme.Get_Code_Logiciel: UnicodeString;
begin
  Result := ChildNodes['Code_Logiciel'].Text;
end;

procedure TXMLGamme.Set_Code_Logiciel(Value: UnicodeString);
begin
  ChildNodes['Code_Logiciel'].NodeValue := Value;
end;

function TXMLGamme.Get_Code_Aerau: UnicodeString;
begin
  Result := ChildNodes['Code_Aerau'].Text;
end;

procedure TXMLGamme.Set_Code_Aerau(Value: UnicodeString);
begin
  ChildNodes['Code_Aerau'].NodeValue := Value;
end;

{ TXMLBouchesEquivalentes }

procedure TXMLBouchesEquivalentes.AfterConstruction;
begin
  RegisterChildNode('BoucheEquivalente', TXMLBoucheEquivalente);
  ItemTag := 'BoucheEquivalente';
  ItemInterface := IXMLBoucheEquivalente;
  inherited;
end;

function TXMLBouchesEquivalentes.Get_BoucheEquivalente(Index: Integer): IXMLBoucheEquivalente;
begin
  Result := List[Index] as IXMLBoucheEquivalente;
end;

function TXMLBouchesEquivalentes.Add: IXMLBoucheEquivalente;
begin
  Result := AddItem(-1) as IXMLBoucheEquivalente;
end;

function TXMLBouchesEquivalentes.Insert(const Index: Integer): IXMLBoucheEquivalente;
begin
  Result := AddItem(Index) as IXMLBoucheEquivalente;
end;

{ TXMLBoucheEquivalente }

procedure TXMLBoucheEquivalente.AfterConstruction;
begin
  RegisterChildNode('BoucheBase', TXMLBouche);
  RegisterChildNode('Neuf', TXMLDomaine);
  RegisterChildNode('Renovation', TXMLDomaine);
  inherited;
end;

function TXMLBoucheEquivalente.Get_BoucheBase: IXMLBouche;
begin
  Result := ChildNodes['BoucheBase'] as IXMLBouche;
end;

function TXMLBoucheEquivalente.Get_Neuf: IXMLDomaine;
begin
  Result := ChildNodes['Neuf'] as IXMLDomaine;
end;

function TXMLBoucheEquivalente.Get_Renovation: IXMLDomaine;
begin
  Result := ChildNodes['Renovation'] as IXMLDomaine;
end;

{ TXMLElementChiffr }

function TXMLElementChiffr.Get_Code_Aerau: UnicodeString;
begin
  Result := ChildNodes['Code_Aerau'].Text;
end;

procedure TXMLElementChiffr.Set_Code_Aerau(Value: UnicodeString);
begin
  ChildNodes['Code_Aerau'].NodeValue := Value;
end;

function TXMLElementChiffr.Get_Code_Article: UnicodeString;
begin
  Result := ChildNodes['Code_Article'].Text;
end;

procedure TXMLElementChiffr.Set_Code_Article(Value: UnicodeString);
begin
  ChildNodes['Code_Article'].NodeValue := Value;
end;

{ TXMLBouche }

{ TXMLDomaine }

procedure TXMLDomaine.AfterConstruction;
begin
  RegisterChildNode('Cordon', TXMLBouche);
  RegisterChildNode('Pile', TXMLBouche);
  RegisterChildNode('Elec', TXMLBouche);
  RegisterChildNode('DouzeVolts', TXMLBouche);
  RegisterChildNode('CordonNonMinute', TXMLBouche);
  RegisterChildNode('ThAir', TXMLBouche);
  inherited;
end;

function TXMLDomaine.Get_Cordon: IXMLBouche;
begin
  Result := ChildNodes['Cordon'] as IXMLBouche;
end;

function TXMLDomaine.Get_Pile: IXMLBouche;
begin
  Result := ChildNodes['Pile'] as IXMLBouche;
end;

function TXMLDomaine.Get_Elec: IXMLBouche;
begin
  Result := ChildNodes['Elec'] as IXMLBouche;
end;

function TXMLDomaine.Get_DouzeVolts: IXMLBouche;
begin
  Result := ChildNodes['DouzeVolts'] as IXMLBouche;
end;

function TXMLDomaine.Get_CordonNonMinute: IXMLBouche;
begin
  Result := ChildNodes['CordonNonMinute'] as IXMLBouche;
end;

function TXMLDomaine.Get_ThAir: IXMLBouche;
begin
  Result := ChildNodes['ThAir'] as IXMLBouche;
end;

{ TXMLEAs_Acoustiques }

procedure TXMLEAs_Acoustiques.AfterConstruction;
begin
  RegisterChildNode('EA_Acoustique', TXMLEA_Acoustique);
  FEA_Acoustique := CreateCollection(TXMLEA_AcoustiqueList, IXMLEA_Acoustique, 'EA_Acoustique') as IXMLEA_AcoustiqueList;
  inherited;
end;

function TXMLEAs_Acoustiques.Get_EA_Acoustique: IXMLEA_AcoustiqueList;
begin
  Result := FEA_Acoustique;
end;

function TXMLEAs_Acoustiques.Get_Methode_Acoustique: Integer;
begin
  Result := ChildNodes['Methode_Acoustique'].NodeValue;
end;

procedure TXMLEAs_Acoustiques.Set_Methode_Acoustique(Value: Integer);
begin
  ChildNodes['Methode_Acoustique'].NodeValue := Value;
end;

{ TXMLEA_Acoustique }

procedure TXMLEA_Acoustique.AfterConstruction;
begin
  RegisterChildNode('EA_Chiffrage', TXMLElementChiffr);
  RegisterChildNode('Accessoires', TXMLAccessoires);
  RegisterChildNode('Methode_0_DN', TXMLMethode_0_DN);
  RegisterChildNode('Methode_1_Classement_Debit', TXMLMethode_1_Classement_Debit);
  inherited;
end;

function TXMLEA_Acoustique.Get_Reference: UnicodeString;
begin
  Result := ChildNodes['Reference'].Text;
end;

procedure TXMLEA_Acoustique.Set_Reference(Value: UnicodeString);
begin
  ChildNodes['Reference'].NodeValue := Value;
end;

function TXMLEA_Acoustique.Get_Type_Pose: Integer;
begin
  Result := ChildNodes['Type_Pose'].NodeValue;
end;

procedure TXMLEA_Acoustique.Set_Type_Pose(Value: Integer);
begin
  ChildNodes['Type_Pose'].NodeValue := Value;
end;

function TXMLEA_Acoustique.Get_EA_Chiffrage: IXMLElementChiffr;
begin
  Result := ChildNodes['EA_Chiffrage'] as IXMLElementChiffr;
end;

function TXMLEA_Acoustique.Get_Accessoires: IXMLAccessoires;
begin
  Result := ChildNodes['Accessoires'] as IXMLAccessoires;
end;

function TXMLEA_Acoustique.Get_Methode_0_DN: IXMLMethode_0_DN;
begin
  Result := ChildNodes['Methode_0_DN'] as IXMLMethode_0_DN;
end;

function TXMLEA_Acoustique.Get_Methode_1_Classement_Debit: IXMLMethode_1_Classement_Debit;
begin
  Result := ChildNodes['Methode_1_Classement_Debit'] as IXMLMethode_1_Classement_Debit;
end;

{ TXMLEA_AcoustiqueList }

function TXMLEA_AcoustiqueList.Add: IXMLEA_Acoustique;
begin
  Result := AddItem(-1) as IXMLEA_Acoustique;
end;

function TXMLEA_AcoustiqueList.Insert(const Index: Integer): IXMLEA_Acoustique;
begin
  Result := AddItem(Index) as IXMLEA_Acoustique;
end;

function TXMLEA_AcoustiqueList.Get_Item(Index: Integer): IXMLEA_Acoustique;
begin
  Result := List[Index] as IXMLEA_Acoustique;
end;

{ TXMLAccessoires }

procedure TXMLAccessoires.AfterConstruction;
begin
  RegisterChildNode('Accessoire', TXMLElementChiffr);
  ItemTag := 'Accessoire';
  ItemInterface := IXMLElementChiffr;
  inherited;
end;

function TXMLAccessoires.Get_Accessoire(Index: Integer): IXMLElementChiffr;
begin
  Result := List[Index] as IXMLElementChiffr;
end;

function TXMLAccessoires.Add: IXMLElementChiffr;
begin
  Result := AddItem(-1) as IXMLElementChiffr;
end;

function TXMLAccessoires.Insert(const Index: Integer): IXMLElementChiffr;
begin
  Result := AddItem(Index) as IXMLElementChiffr;
end;

{ TXMLMethode_0_DN }

procedure TXMLMethode_0_DN.AfterConstruction;
begin
  RegisterChildNode('EA_Base', TXMLElementChiffr);
  inherited;
end;

function TXMLMethode_0_DN.Get_EA_Base: IXMLElementChiffr;
begin
  Result := ChildNodes['EA_Base'] as IXMLElementChiffr;
end;

function TXMLMethode_0_DN.Get_Numero: Integer;
begin
  Result := ChildNodes['Numero'].NodeValue;
end;

procedure TXMLMethode_0_DN.Set_Numero(Value: Integer);
begin
  ChildNodes['Numero'].NodeValue := Value;
end;

function TXMLMethode_0_DN.Get_DN: Integer;
begin
  Result := ChildNodes['DN'].NodeValue;
end;

procedure TXMLMethode_0_DN.Set_DN(Value: Integer);
begin
  ChildNodes['DN'].NodeValue := Value;
end;

{ TXMLMethode_1_Classement_Debit }

function TXMLMethode_1_Classement_Debit.Get_Classe_Debit: Integer;
begin
  Result := ChildNodes['Classe_Debit'].NodeValue;
end;

procedure TXMLMethode_1_Classement_Debit.Set_Classe_Debit(Value: Integer);
begin
  ChildNodes['Classe_Debit'].NodeValue := Value;
end;

function TXMLMethode_1_Classement_Debit.Get_Classement: Integer;
begin
  Result := ChildNodes['Classement'].NodeValue;
end;

procedure TXMLMethode_1_Classement_Debit.Set_Classement(Value: Integer);
begin
  ChildNodes['Classement'].NodeValue := Value;
end;

end.
