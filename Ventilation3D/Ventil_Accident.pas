Unit Ventil_Accident;

{$Include 'Ventil_Directives.pas'}

interface
Uses
  Classes, SysUtils, advGrid, BaseGrid, AdvObj, Math,
  Ventil_Types, Ventil_Logement, Ventil_SystemeVMC, Ventil_Collecteur, Ventil_Troncon, Ventil_Utils,
  Ventil_EdibatecProduits,
  Ventil_TronconDessin,
  BBScad_Interface_Aeraulique;

Type
{ ************************************************************************************************************************************************** }
  TVentil_Accident_Dessin = Class(TVentil_Accident)
    Constructor Create; Override;
  End;
{ ************************************************************************************************************************************************** }
  TVentil_Accident_CoudeDessin = Class(TVentil_Accident_Dessin)
    IsEffetSysteme : Boolean;
    AngleCoude : Integer;
    FEntretoise_L : Integer;
    FContreParoi : Integer;
    Constructor Create; Override;
  Protected
    class Function NomParDefaut : String; Override;
    Function IsCoudeT2AActhys : Boolean;
    Function IsCoudeThAirMVN : Boolean;
    Function CanHaveEntretoise : Boolean;
    Function CanHaveThAirContreParoi : Boolean;
    Procedure CalculerQuantitatifEntretoise;
    Procedure CalculerQuantitatifInternal; Override;
    Procedure Save; Override;
    Function HasCoudeParentMore45 : boolean;
  Public
    Procedure Load; Override;
    class Function FamilleObjet : String; Override;
    Procedure Paste(_Source: TVentil_Objet); Override;
    Procedure CalculeDiametreDebitCollectif(_Iteratif : Boolean); Override;
    Procedure CalculeDiametreDebitTertiaire(_Iteratif : Boolean); Override;
    Procedure Affiche(_Grid: TAdvStringGrid); Override;
    Function AfficheQuestion(_NoQuestion: Integer): Boolean; Override;
    Procedure Recupere(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType; Override;
    Procedure PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  HasComboBox(_Ligne: Integer): Boolean; Override;
    Procedure InfosFromCad; Override;
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override;
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    function GetEffetSystemeCoudesSuccessifs(_AutreCoude : TVentil_Accident_CoudeDessin; _Gaine : TVentil_TronconDessin) : Single;
    function GetEffetSystemeApresVentilateur(_SortieCaisson : TVentil_TronconDessin_SortieCaisson; _Gaine : TVentil_TronconDessin) : Single;
  End;
{ ************************************************************************************************************************************************** }
  TVentil_Accident_Generic  = Class(TVentil_Accident_Dessin)
    Constructor Create; Override;
  Protected
    Procedure Save; Override;
  Public
    Procedure Load; Override;
    Procedure Affiche(_Grid: TAdvStringGrid); Override;
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override;
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    Procedure Recupere(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  AfficheQuestion(_NoQuestion: Integer): Boolean; Override;
    Function  TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType; Override;
    Procedure PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
  End;
{ ************************************************************************************************************************************************** }
  TVentil_Accident_ClapetCoupeFeu = Class(TVentil_Accident_Dessin)
    Constructor Create; Override;
  Private
    function Avertissement : Boolean;
    function GetMessageAvert : String;
    function GetCoupeFeu :TEdibatec_CoupeFeu;
    Function  SelectionGammeClapetCF: String;
  Protected
    Procedure CalculerQuantitatifInternal; Override;
    Procedure Save; Override;
    Procedure SelectionneDiametre_SelonMethode; Override;
    Function IsEtiquetteVisible : boolean; Override;
    Procedure ManageEtiquetteVisible; Override;
  Public
    GammePareFlamme: TEdibatec_Gamme;
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override; 
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    Procedure Affiche(_Grid: TAdvStringGrid); Override;
    Procedure Recupere(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType; Override;
    Procedure PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  AfficheQuestion(_NoQuestion: Integer): Boolean; Override;
    Function  EllipsClick(_Grid: TAdvStringGrid; _Ligne: Integer): String; Override;
    Procedure Load; Override;
  End;
{ ************************************************************************************************************************************************** }
  TVentil_Accident_ChangementDeDiametre = Class(TVentil_Accident_Dessin)
    Constructor Create; Override;
  Private
    Angle       : Integer;
//    Forme       : Integer;
    FTypeChg_Diametre : Integer;
    Procedure SetForme(_Forme: Integer);
    Function  GetForme: Integer;
  Protected
    class Function NomParDefaut : String; Override;
    Procedure IAero_Fredo_Base_RemplacerPar(Objet : IAero_Fredo_Base); Override;
    Procedure Save; Override;
    Procedure CalculerQuantitatifInternal; Override;
    Procedure InitEtiquette; Override;
    Function  ImageAssociee: String; Override;
    Function HasChgtDiamParent : boolean;
  Public
    Automatique : Boolean;
    class Function FamilleObjet : String; Override;
    Property  TypeChg_Diametre : Integer read GetForme write SetForme;
    Procedure Paste(_Source: TVentil_Objet); Override;
    Procedure Load; Override;
    Procedure Affiche(_Grid: TAdvStringGrid); Override;
    Function CalculeDzetaElargissementBrusqueDT2014(D1, D2 : Real) : Real;      //cf Annexe A.3.2.1 du DTU 68.3 P1-1-1
    Function CalculeDzetaRetrecissementBrusqueDT2014(D1, D2 : Real) : Real;     //cf Annexe A.3.2.1 du DTU 68.3 P1-1-1
    Function CalculeDzetaElargissementConiqueDT2014(D1, D2 : Real) : Real;      //cf Annexe A.3.2.1 du DTU 68.3 P1-1-1
    Function CalculeDzetaRetrecissementConiqueDT2014(D1, D2 : Real) : Real;     //cf Annexe A.3.2.1 du DTU 68.3 P1-1-1
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override;
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    Procedure Recupere(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType; Override;
    Procedure PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  HasComboBox(_Ligne: Integer): Boolean; Override;
    Function  AfficheQuestion(_NoQuestion: Integer): Boolean; Override;
  End;
{ ************************************************************************************************************************************************** }
  TVentil_AccidentDevoiement = Class(TVentil_Accident_Dessin)
  Protected
    Procedure CalculerQuantitatifInternal; Override;
  Public
    LongueurDevoiement : Real;
    Procedure Affiche(_Grid: TAdvStringGrid); Override;
    Procedure InfosFromCad; Override;
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override; 
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
  End;
{ ************************************************************************************************************************************************** }
  TVentil_Accident_Genere = Class(TVentil_Accident)
    Constructor Create; Override;
  End;
{ ************************************************************************************************************************************************** }
  TVentil_Accident_BrancheLaterale = Class(TVentil_Accident_Genere)
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override; 
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
  Private
    Function CalculDzetaApresBifurc3SortiesMini: Real;
    Function CalculDzetaApresBifurc3SortiesMaxi: Real;
    Function CalculDzetaApresBifurc2SortiesMini: Real;
    Function CalculDzetaApresBifurc2SortiesMaxi: Real;
    Function CacluDzetaDTU2014DivergenceDebitMini : Real;
    Function CacluDzetaDTU2014DivergenceDebitMaxi : Real;
  End;
{ ************************************************************************************************************************************************** }
  TVentil_Accident_BrancheRectiligne = Class(TVentil_Accident_Genere)
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override; 
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
  Private
    Function CalculDzetaApresBifurc3SortiesMini: Real;
    Function CalculDzetaApresBifurc3SortiesMaxi: Real;
    Function CalculDzetaApresBifurc2SortiesMini: Real;
    Function CalculDzetaApresBifurc2SortiesMaxi: Real;
    Function CalculDzetaDTU2014DebitMini(_DebitLateral : Real; _DiamLateral : Real) : Real;
    Function CalculDzetaDTU2014DebitMaxi(_DebitLateral : Real; _DiamLateral : Real) : Real;
    Function CacluDzetaDTU2014DivergenceDebitMini : Real;
    Function CacluDzetaDTU2014DivergenceDebitMaxi : Real;
  End;

{ ************************************************************************************************************************************************** }
  TVentil_Accident_BrancheRaccord = Class(TVentil_Accident_Genere)
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override;
    Function  CalculeDzetaIdelCikMini: Real;
    Function  CalculeDzetaIdelCikMaxi: Real;    
  End;
{ ************************************************************************************************************************************************** }
  TVentil_Accident_TeSouche = Class(TVentil_Accident_Genere)
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override; 
  End;
{ ************************************************************************************************************************************************** }
  TVentil_Accident_TeSoucheBRect = Class(TVentil_Accident_Genere)
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override;
  End;

{ ************************************************************************************************************************************************** }
  TVentil_Accident_TeSoucheBRectAlsace = Class(TVentil_Accident_Genere)
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override;
  End;
{ ************************************************************************************************************************************************** }
  TVentil_Accident_TeSoucheBRectShunt = Class(TVentil_Accident_TeSoucheBRect)
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    Function CalculePDCSingulieresAdditionnelles : Real; Override;
  End;
{ ************************************************************************************************************************************************** }
  TVentil_Accident_TeSoucheCoude = Class(TVentil_Accident_Dessin)
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override;
    Procedure CalculerQuantitatifInternal; Override;
  End;
{ ************************************************************************************************************************************************** }
  TVentil_Accident_TeSouchePlenumColonneVMC = Class(TVentil_Accident_Genere)
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override;
  End;
{ ************************************************************************************************************************************************** }
  TVentil_Accident_TeSouchePlenumColonneShunt = Class(TVentil_Accident_Genere)
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override;
    Function CalculePDCSingulieresAdditionnelles : Real; Override;
  End;
{ ************************************************************************************************************************************************** }
  TVentil_Accident_TeSouchePlenumColonneIndividuel = Class(TVentil_Accident_Genere)
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override;
  End;
{ ************************************************************************************************************************************************** }
  TVentil_Accident_TeSouchePlenumColonneAlsace = Class(TVentil_Accident_Genere)
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override;
  End;
{ ************************************************************************************************************************************************** }
  TVentil_Accident_TeSouchePlenumColonneRamon = Class(TVentil_Accident_Genere)
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override;
  End;
{ ************************************************************************************************************************************************** }
  TVentil_Accident_Plenum = Class(TVentil_Accident_Genere)
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override;
  End;
{ ************************************************************************************************************************************************** }
  TVentil_Accident_TeSouchePlenumColonneShuntPiquage = Class(TVentil_Accident_Genere)
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override;
  End;
{ ************************************************************************************************************************************************** }
  TVentil_Accident_Registre = Class(TVentil_Accident_Genere)
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override;
  End;

  TVentil_Accident_TourelleBifurcation = Class(TVentil_Accident_Genere)
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override;
  End;
{ ************************************************************************************************************************************************** }
//Abandonn�
{
  TVentil_Accident_TourelleIntermediaire = Class(TVentil_Accident_Genere)
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override;
  End;
}
{ ************************************************************************************************************************************************** }

Implementation
Uses
  Graphics,
  Grids,
  BbsgFonc,
//  HelpersCAD,
//  CAD_Commun,
  Ventil_Edibatec,
  Ventil_Registre,
  Ventil_TourelleTete,
  Ventil_TourelleIntermediaire,
  Ventil_TourellePlenum,
  Ventil_TourelleBifurcation,
  Ventil_Plenum,
  Ventil_Colonne,
  Ventil_TeSouche,
  Ventil_EdibatecChoixGamme,
  Ventil_FIREBIRD, Ventil_Const,
  Ventil_Caisson, Ventil_Bifurcation,
  Ventil_Declarations;


{ ******************************************************************* TVentil_Accident_Dessin ************************************************************ }
Constructor TVentil_Accident_Dessin.Create;
Begin               
  inherited;
  AutoGenere := False;
End;
{ ******************************************************************* \TVentil_Accident_Dessin ********************************************************** }

{ ******************************************************************* TVentil_Accident_Genere ************************************************************ }
Constructor TVentil_Accident_Genere.Create;
Begin    
  inherited;
  AutoGenere := True;
End;
{ ******************************************************************* \ TVentil_Accident_Genere ********************************************************** }


{ ******************************************************************* TVentil_Accident_BrancheRaccord ***************************************************** }
Procedure TVentil_Accident_BrancheRaccord.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
Begin
  _Grid.Cells[_ColDep, _Row] := 'Branche lat�rale ';
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheRaccord.CalculeDzetaMini: Real;
Var
  m_DiamCollecteur : Real;
  m_DiamTroncon    : Real;
  A  : Real;
  A_ : Real;
  D1 : Real;
Begin

  TronconCalculPDCSinguliere := TronconPorteur.Parentcalcul;
  m_DiamCollecteur := TronconPorteur.Parentcalcul.Diametre;
  m_DiamTroncon    := TronconPorteur.Diametre;
  D1 :=   m_DiamCollecteur / m_DiamTroncon;
  {If TronconPorteur.DebitMaxi = TronconPorteur.Parentcalcul.DebitMaxi Then Result := CalculeDzetaIdelCik
  Else }Begin


    If TronconPorteur.Parentcalcul.DebitMini <> 0 Then A := TronconPorteur.DebitMini / TronconPorteur.Parentcalcul.DebitMini
                                                  Else A := 0.5;
    If m_DiamTroncon / m_DiamCollecteur < 0.5 Then A_ := 1
                                              Else If m_DiamTroncon / m_DiamCollecteur < 0.75 Then A_ := 0.75
                                                                                              Else A_ := 0.65;
    //Dzeta  := A_ * (Sqr(A) * (Sqr(Sqr(m_DiamCollecteur / m_DiamTroncon)) - 2) + 4 * A - 1);
    DzetaMini  := A_ * (Sqr(A) * (Sqr(Sqr(D1)) - 2) + 4 * A - 1);

    {$IfDef IDELCIK}

    If DzetaMini > 10 Then Result := CalculeDzetaIdelCikMini
                  Else
    {$EndIf}

if Etude.Batiment.IsFabricantVIM then
Begin
if ((TronconPorteur.Parentcalcul.InheritsFrom(tventil_collecteur)) and (tventil_collecteur(TronconPorteur.Parentcalcul).SortieRectiligne.HasObjetCalculAval = False)) or
   ((TronconPorteur.Parentcalcul.InheritsFrom(TVentil_Bifurcation))  and (TVentil_Bifurcation(TronconPorteur.Parentcalcul).SortieRectiligne.HasObjetCalculAval = False))      then
begin

  DzetaMini := TronconPorteur.Parentcalcul.TotalDzetaMini;
  //Dzeta d'un coude
//  Dzeta := Dzeta + c_Coude__Tableau5_Dzeta_CoudeMoyen[c_Coude_90];
  DzetaMini := c_Coude__Tableau5_Dzeta_CoudeMoyen[c_Coude_90];

  //Dzeta d'un changement de diam�tre
  if m_DiamCollecteur <> m_DiamTroncon then
        Begin
        DzetaMini := DzetaMini + 1.12 * Sqr(1 - Sqr(m_DiamTroncon / m_DiamCollecteur)); // Conique
        End;

end;
End;

                  Result := Inherited CalculeDzetaMini;
  End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheRaccord.CalculeDzetaMaxi: Real;
Var
  m_DiamCollecteur : Real;
  m_DiamTroncon    : Real;
  A  : Real;
  A_ : Real;
  D1 : Real;
Begin

  TronconCalculPDCSinguliere := TronconPorteur.Parentcalcul;
  m_DiamCollecteur := TronconPorteur.Parentcalcul.Diametre;
  m_DiamTroncon    := TronconPorteur.Diametre;
  D1 :=   m_DiamCollecteur / m_DiamTroncon;
  {If TronconPorteur.DebitMaxi = TronconPorteur.Parentcalcul.DebitMaxi Then Result := CalculeDzetaIdelCik
  Else }Begin
    If TronconPorteur.Parentcalcul.DebitMaxi <> 0 Then A := TronconPorteur.DebitMaxi / TronconPorteur.Parentcalcul.DebitMaxi
                                                  Else A := 0.5;
    If m_DiamTroncon / m_DiamCollecteur < 0.5 Then A_ := 1
                                              Else If m_DiamTroncon / m_DiamCollecteur < 0.75 Then A_ := 0.75
                                                                                              Else A_ := 0.65;
    //Dzeta  := A_ * (Sqr(A) * (Sqr(Sqr(m_DiamCollecteur / m_DiamTroncon)) - 2) + 4 * A - 1);
    DzetaMaxi  := A_ * (Sqr(A) * (Sqr(Sqr(D1)) - 2) + 4 * A - 1);



    {$IfDef IDELCIK}
    If DzetaMaxi > 10 Then Result := CalculeDzetaIdelCikMaxi
                  Else
    {$EndIf}

if Etude.Batiment.IsFabricantVIM then
Begin
if ((TronconPorteur.Parentcalcul.InheritsFrom(tventil_collecteur)) and (tventil_collecteur(TronconPorteur.Parentcalcul).SortieRectiligne.HasObjetCalculAval = False)) or
   ((TronconPorteur.Parentcalcul.InheritsFrom(TVentil_Bifurcation))  and (TVentil_Bifurcation(TronconPorteur.Parentcalcul).SortieRectiligne.HasObjetCalculAval = False))      then
begin


//  DzetaMaxi := TronconPorteur.Parentcalcul.TotalDzetaMaxi;
  //Dzeta d'un coude
//  Dzeta := Dzeta + c_Coude__Tableau5_Dzeta_CoudeMoyen[c_Coude_90];
  DzetaMaxi := c_Coude__Tableau5_Dzeta_CoudeMoyen[c_Coude_90];

  //Dzeta d'un changement de diam�tre
  if m_DiamCollecteur <> m_DiamTroncon then
        Begin
        DzetaMaxi := DzetaMaxi + 1.12 * Sqr(1 - Sqr(m_DiamTroncon / m_DiamCollecteur)); // Conique
        End;

end;
End;

                  Result := Inherited CalculeDzetaMaxi;
  End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheRaccord.CalculeDzetaIdelCikMini: Real;
Const
       TAB77 : ARRAY[1..11,1..7] Of Real = (
                                             (-1.00,-1.00,-1.00,-1.00,-1.00,-1.00,-1.00),
                                             (0.40,-0.37,-0.51,-0.54,-0.59,-0.60,-0.61),
                                             (3.80,0.72,0.17,-0.03,-0.17,-0.22,-0.30),
                                             (9.20,2.27,1.00,0.58,0.27,0.15,-0.11),
                                             (16.6,4.30,2.06,1.30,0.75,0.55,0.44),
                                             (25.5,6.75,3.23,2.06,1.20,0.89,0.77),
                                             (36.7,9.70,4.70,2.98,1.68,1.25,1.04),
                                             (42.9,13.0,6.30,3.90,2.20,1.60,1.30),
                                             (64.9,16.9,7.92,4.92,2.70,1.92,1.56),
                                             (82.0,21.2,9.70,6.10,3.20,2.25,1.80),
                                             (101,26.0,11.9,7.25,3.80,2.57,2.00)
                                           );
Var
  m_SectionLaterale : Real;
  m_SectionPpale    : Real;
  m_Rapport1        : Real;
  m_Rapport2        : Real;
  NoLigne           : Integer;
  NoCol             : Integer;
  DzetaInter        : Real;
  A                 : Real;
Begin
  { Calcul de la branche laterale d'une bifurcation selon l'IdelCik }
  m_SectionLaterale := Pi * Sqr(TronconPorteur.Diametre) / 4;
  m_SectionPpale    := Pi * Sqr(TronconPorteur.ParentCalcul.Diametre) / 4;

  m_Rapport1 := m_SectionLaterale / m_SectionPpale;
  m_Rapport2 := TronconPorteur.DebitMini / TronconPorteur.ParentCalcul.DebitMini;

  If m_Rapport1 <= 0.1 then NoCol := 1
  Else If m_Rapport1 <= 0.2 then NoCol := 2
  Else If m_Rapport1 <= 0.3 then NoCol := 3
  Else If m_Rapport1 <= 0.4 then NoCol := 4
  Else If m_Rapport1 <= 0.6 then NoCol := 5
  Else If m_Rapport1 <= 0.8 then NoCol := 6
  Else                           NoCol := 7;

  If m_Rapport2 = 0 then NoLigne := 1
  Else If m_Rapport2 <= 0.1 then NoLigne := 2
  Else If m_Rapport2 <= 0.2 then NoLigne := 3
  Else If m_Rapport2 <= 0.3 then NoLigne := 4
  Else If m_Rapport2 <= 0.4 then NoLigne := 5
  Else If m_Rapport2 <= 0.5 then NoLigne := 6
  Else If m_Rapport2 <= 0.6 then NoLigne := 7
  Else If m_Rapport2 <= 0.7 then NoLigne := 8
  Else If m_Rapport2 <= 0.8 then NoLigne := 9
  Else If m_Rapport2 <= 0.9 then NoLigne := 10
  Else                     NoLigne := 11;

  If m_Rapport1 <= 0.2 then A := 1.0
  Else If m_Rapport1 <= 0.4 then A := 0.75
  Else If m_Rapport1 <= 0.6 then A := 0.70
  Else If m_Rapport1 <= 0.8 then A := 0.65
  Else                    A := 0.60;

  DzetaInter := A * TAB77[NoLigne, NoCol];

  DzetaMini := DzetaInter / Sqr(m_Rapport2 / m_Rapport1);

  If DzetaMini < -5 Then DzetaMini := -5
                Else If DzetaMini > 10 Then DzetaMini := 10;
  Result := DzetaMini;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheRaccord.CalculeDzetaIdelCikMaxi: Real;
Const
       TAB77 : ARRAY[1..11,1..7] Of Real = (
                                             (-1.00,-1.00,-1.00,-1.00,-1.00,-1.00,-1.00),
                                             (0.40,-0.37,-0.51,-0.54,-0.59,-0.60,-0.61),
                                             (3.80,0.72,0.17,-0.03,-0.17,-0.22,-0.30),
                                             (9.20,2.27,1.00,0.58,0.27,0.15,-0.11),
                                             (16.6,4.30,2.06,1.30,0.75,0.55,0.44),
                                             (25.5,6.75,3.23,2.06,1.20,0.89,0.77),
                                             (36.7,9.70,4.70,2.98,1.68,1.25,1.04),
                                             (42.9,13.0,6.30,3.90,2.20,1.60,1.30),
                                             (64.9,16.9,7.92,4.92,2.70,1.92,1.56),
                                             (82.0,21.2,9.70,6.10,3.20,2.25,1.80),
                                             (101,26.0,11.9,7.25,3.80,2.57,2.00)
                                           );
Var
  m_SectionLaterale : Real;
  m_SectionPpale    : Real;
  m_Rapport1        : Real;
  m_Rapport2        : Real;
  NoLigne           : Integer;
  NoCol             : Integer;
  DzetaInter        : Real;
  A                 : Real;
Begin
  { Calcul de la branche laterale d'une bifurcation selon l'IdelCik }
  m_SectionLaterale := Pi * Sqr(TronconPorteur.Diametre) / 4;
  m_SectionPpale    := Pi * Sqr(TronconPorteur.ParentCalcul.Diametre) / 4;

  m_Rapport1 := m_SectionLaterale / m_SectionPpale;
  m_Rapport2 := TronconPorteur.DebitMaxi / TronconPorteur.ParentCalcul.DebitMaxi;

  If m_Rapport1 <= 0.1 then NoCol := 1
  Else If m_Rapport1 <= 0.2 then NoCol := 2
  Else If m_Rapport1 <= 0.3 then NoCol := 3
  Else If m_Rapport1 <= 0.4 then NoCol := 4
  Else If m_Rapport1 <= 0.6 then NoCol := 5
  Else If m_Rapport1 <= 0.8 then NoCol := 6
  Else                           NoCol := 7;

  If m_Rapport2 = 0 then NoLigne := 1
  Else If m_Rapport2 <= 0.1 then NoLigne := 2
  Else If m_Rapport2 <= 0.2 then NoLigne := 3
  Else If m_Rapport2 <= 0.3 then NoLigne := 4
  Else If m_Rapport2 <= 0.4 then NoLigne := 5
  Else If m_Rapport2 <= 0.5 then NoLigne := 6
  Else If m_Rapport2 <= 0.6 then NoLigne := 7
  Else If m_Rapport2 <= 0.7 then NoLigne := 8
  Else If m_Rapport2 <= 0.8 then NoLigne := 9
  Else If m_Rapport2 <= 0.9 then NoLigne := 10
  Else                     NoLigne := 11;

  If m_Rapport1 <= 0.2 then A := 1.0
  Else If m_Rapport1 <= 0.4 then A := 0.75
  Else If m_Rapport1 <= 0.6 then A := 0.70
  Else If m_Rapport1 <= 0.8 then A := 0.65
  Else                    A := 0.60;

  DzetaInter := A * TAB77[NoLigne, NoCol];

  DzetaMaxi := DzetaInter / Sqr(m_Rapport2 / m_Rapport1);

  If DzetaMaxi < -5 Then DzetaMaxi := -5
                Else If DzetaMaxi > 10 Then DzetaMaxi := 10;
  Result := DzetaMaxi;
End;
{ ******************************************************************* \ TVentil_Accident_BrancheRaccord *************************************************** }

{ ******************************************************************* TVentil_Accident_BrancheLaterale **************************************************** }
Procedure TVentil_Accident_BrancheLaterale.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
Begin
  If TronconPorteur.ObjetTerminalAmont.InheritsFrom(TVentil_Bifurcation) Then
    _Grid.Cells[_ColDep, _Row] := 'Branche lat�rale ' + c_Angle[TVentil_Bifurcation(TronconPorteur.ObjetTerminalAmont).AngleBifurcation]
  Else _Grid.Cells[_ColDep, _Row] := 'Branche lat�rale ';
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheLaterale.CalculDzetaApresBifurc2SortiesMini: Real;
Var
  m_DebitBifurc : Real;
  m_DiamBifurc  : Integer;
  m_DiamLat     : Integer;
  m_DebitLat    : Real;
  A             : Real;
  A_            : Real;
  m_Bifurc      : TVentil_Troncon;
Begin
  if Etude.DTU2014 and (TVentil_TronconDessin_SortieCaisson(TronconPorteur.SortieCaissonParent).TypeSortie = c_CaissonSortie_Insufflation) then
    begin
        Result := CacluDzetaDTU2014DivergenceDebitMini;
    end
  else
    begin
    m_Bifurc := TronconPorteur.ObjetTerminalAmont;
    TronconCalculPDCSinguliere := m_Bifurc;
    Result := 0;
    m_DebitBifurc := m_Bifurc.DebitMini;
    m_DiamBifurc  := m_Bifurc.Diametre;
    If (m_DebitBifurc <> 0) Then Begin
      m_DebitLat := TronconPorteur.DebitMini;
      m_DiamLat  := TronconPorteur.Diametre;
      A := m_DebitLat / m_DebitBifurc;
      If m_DiamLat / m_DiamBifurc < 0.5 Then A_ := 1
                                        Else If m_DiamLat / m_DiamBifurc < 0.75 Then A_ := 0.75
                                                                              ELse A_ := 0.65;
      If TVentil_Bifurcation(m_Bifurc).AngleBifurcation = c_Angle90 Then
        Result := A_ * (Sqr(A) * (Power(m_DiamBifurc / m_DiamLat ,4) - 2) + 4 * A - 1)
      Else Result := Sqr(A) * (Power(m_DiamBifurc / m_DiamLat, 4) - 1.41 * Sqr(m_DiamBifurc / m_DiamLat) - 2) + 4 * A + 1;
  End;
End;

  if Etude.Batiment.IsFabricantVIM then
  Begin
  if ((TronconPorteur.Parentcalcul.InheritsFrom(tventil_collecteur)) and (tventil_collecteur(TronconPorteur.Parentcalcul).SortieRectiligne <> nil) and (tventil_collecteur(TronconPorteur.Parentcalcul).SortieRectiligne.Fils <> nil) and (tventil_collecteur(TronconPorteur.Parentcalcul).SortieRectiligne.Fils.count = 0)) or
     ((TronconPorteur.Parentcalcul.InheritsFrom(TVentil_Bifurcation))  and (TVentil_Bifurcation(TronconPorteur.Parentcalcul).SortieRectiligne <> nil) and (TVentil_Bifurcation(TronconPorteur.Parentcalcul).SortieRectiligne.Fils <> nil) and  (TVentil_Bifurcation(TronconPorteur.Parentcalcul).SortieRectiligne.Fils.count = 0))      then
  begin
    //Dzeta d'un changement de diam�tre
    if m_DiamBifurc <> m_DiamLat then
          Begin
          DzetaMini := c_Coude__Tableau5_Dzeta_CoudeMoyen[c_Coude_90] + 1.12 * Sqr(1 - Sqr(m_DiamLat / m_DiamBifurc)); // Conique
          Result := DzetaMini;
          End;
    End;
  end;

End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheLaterale.CalculDzetaApresBifurc2SortiesMaxi: Real;
Var
  m_DebitBifurc : Real;
  m_DiamBifurc  : Integer;
  m_DiamLat     : Integer;
  m_DebitLat    : Real;
  A             : Real;
  A_            : Real;
  m_Bifurc      : TVentil_Troncon;
Begin

  if Etude.DTU2014 and (TVentil_TronconDessin_SortieCaisson(TronconPorteur.SortieCaissonParent).TypeSortie = c_CaissonSortie_Insufflation) then
    begin
        Result := CacluDzetaDTU2014DivergenceDebitMaxi;
    end
  else
    begin
    m_Bifurc := TronconPorteur.ObjetTerminalAmont;
    TronconCalculPDCSinguliere := m_Bifurc;
    Result := 0;
    m_DebitBifurc := m_Bifurc.DebitMaxi;
    m_DiamBifurc  := m_Bifurc.Diametre;
    If (m_DebitBifurc <> 0) Then Begin
      m_DebitLat := TronconPorteur.DebitMaxi;
      m_DiamLat  := TronconPorteur.Diametre;
      A := m_DebitLat / m_DebitBifurc;
      If m_DiamLat / m_DiamBifurc < 0.5 Then A_ := 1
                                        Else If m_DiamLat / m_DiamBifurc < 0.75 Then A_ := 0.75
                                                                                ELse A_ := 0.65;
      If TVentil_Bifurcation(m_Bifurc).AngleBifurcation = c_Angle90 Then
        Result := A_ * (Sqr(A) * (Power(m_DiamBifurc / m_DiamLat ,4) - 2) + 4 * A - 1)
      Else Result := Sqr(A) * (Power(m_DiamBifurc / m_DiamLat, 4) - 1.41 * Sqr(m_DiamBifurc / m_DiamLat) - 2) + 4 * A + 1;
    End;

End;

  if Etude.Batiment.IsFabricantVIM then
  Begin
  if ((TronconPorteur.Parentcalcul.InheritsFrom(tventil_collecteur)) and (tventil_collecteur(TronconPorteur.Parentcalcul).SortieRectiligne <> nil) and (tventil_collecteur(TronconPorteur.Parentcalcul).SortieRectiligne.Fils.count = 0)) or
     ((TronconPorteur.Parentcalcul.InheritsFrom(TVentil_Bifurcation)) and (TVentil_Bifurcation(TronconPorteur.Parentcalcul).SortieRectiligne <> nil) and (TVentil_Bifurcation(TronconPorteur.Parentcalcul).SortieRectiligne.Fils.count = 0))      then
  begin
    //Dzeta d'un changement de diam�tre
    if m_DiamBifurc <> m_DiamLat then
          Begin
          DzetaMaxi := c_Coude__Tableau5_Dzeta_CoudeMoyen[c_Coude_90] + 1.12 * Sqr(1 - Sqr(m_DiamLat / m_DiamBifurc)); // Conique
          Result := DzetaMaxi;
          End;

  end;
  End;

End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheLaterale.CalculDzetaApresBifurc3SortiesMini: Real;
Begin
  if Etude.DTU2014 then
    begin
      if TVentil_TronconDessin_SortieCaisson(TronconPorteur.SortieCaissonParent).TypeSortie = c_CaissonSortie_Insufflation then
        Result := CacluDzetaDTU2014DivergenceDebitMini
      else
        Result := CalculDzetaApresBifurc2SortiesMini;
    end
  else
    Result := 0;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheLaterale.CalculDzetaApresBifurc3SortiesMaxi: Real;
Begin
  if Etude.DTU2014 then
    begin
      if TVentil_TronconDessin_SortieCaisson(TronconPorteur.SortieCaissonParent).TypeSortie = c_CaissonSortie_Insufflation then
        Result := CacluDzetaDTU2014DivergenceDebitMaxi
      else
        Result := CalculDzetaApresBifurc2SortiesMaxi;
    end
  else
    Result := 0;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheLaterale.CacluDzetaDTU2014DivergenceDebitMini : Real;
begin
  Result := CacluDzetaDTU2014DivergenceDebitMaxi;
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheLaterale.CacluDzetaDTU2014DivergenceDebitMaxi : Real;
Var
  m_Bifurc      : TVentil_Troncon;
  m_DebitBifurc : Real;
  m_DiamBifurc  : Integer;
  m_VitesseBifurc : Real;
  m_DiamLat     : Integer;
  m_DebitLat    : Real;
  m_VitesseLat  : Real;
  APrim : Real;
  ValAngle      : Integer;
Begin
  Result := 0;
  TronconCalculPDCSinguliere := TronconPorteur;
  m_Bifurc := TronconPorteur.ObjetTerminalAmont;

    if m_Bifurc = Nil then
      Exit;

    if TronconPorteur = Nil then
      Exit;

  m_DebitBifurc := m_Bifurc.DebitMaxi;
  m_DiamBifurc  := m_Bifurc.Diametre;
  m_VitesseBifurc := m_Bifurc.Vitesse;

  m_DebitLat := TronconPorteur.DebitMaxi;
  m_DiamLat  := TronconPorteur.Diametre;
  m_VitesseLat := TronconPorteur.Vitesse;

  if (m_VitesseLat / m_VitesseBifurc) <= 0.8 then
    APrim := 1
  else
    APrim := 0.9;

  If (TVentil_Bifurcation(m_Bifurc).AngleBifurcation in [c_Angle30, c_Angle45]) or ((TVentil_Bifurcation(m_Bifurc).AngleBifurcation = c_Angle90) and (m_DiamLat / m_DiamBifurc < 1)) then
    begin
      Case TVentil_Bifurcation(m_Bifurc).AngleBifurcation of
        c_Angle90 : ValAngle := 90;
        c_Angle30 : ValAngle := 30;
        c_Angle45 : ValAngle := 45;
      End;
    Result := APrim * (1 + Sqr(m_VitesseLat / m_VitesseBifurc) - 2 + (m_VitesseLat / m_VitesseBifurc) * Cos(DegToRad(ValAngle)));
    end
  else
  if (TVentil_Bifurcation(m_Bifurc).AngleBifurcation = c_Angle90) and (m_DiamLat / m_DiamBifurc = 1) then
    begin
    Result := APrim * (0.34 + Sqr(m_VitesseLat / m_VitesseBifurc));
    end
  else
    Result := 0;

end;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheLaterale.CalculeDzetaMini: Real;
Begin
  If (TronconPorteur.ObjetTerminalAmont.InheritsFrom(TVentil_Bifurcation)) And
     (TVentil_Bifurcation(TronconPorteur.ObjetTerminalAmont).BifurcationTriple) Then DzetaMini := CalculDzetaApresBifurc3SortiesMini
                                                                             Else DzetaMini := CalculDzetaApresBifurc2SortiesMini;
  Result := Inherited CalculeDzetaMini;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheLaterale.CalculeDzetaMaxi: Real;
Begin
  If (TronconPorteur.ObjetTerminalAmont.InheritsFrom(TVentil_Bifurcation)) And
     (TVentil_Bifurcation(TronconPorteur.ObjetTerminalAmont).BifurcationTriple) Then DzetaMaxi := CalculDzetaApresBifurc3SortiesMaxi
                                                                             Else DzetaMaxi := CalculDzetaApresBifurc2SortiesMaxi;
  Result := Inherited CalculeDzetaMaxi;
End;
{ ******************************************************************* \ TVentil_Accident_BrancheLaterale ************************************************** }


{ ******************************************************************* TVentil_Accident_BrancheRectiligne ************************************************** }
Procedure TVentil_Accident_BrancheRectiligne.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
Var
  m_Bifurc : TVentil_Troncon;
Begin
  m_Bifurc := TronconPorteur.ObjetTerminalAmont;
  If m_Bifurc.InheritsFrom(TVentil_Bifurcation) Then _Grid.Cells[_ColDep, _Row] := 'Branche rectiligne ' + c_Angle[TVentil_Bifurcation(TronconPorteur.ObjetTerminalAmont).AngleBifurcation]
                                  Else _Grid.Cells[_ColDep, _Row] := 'Branche rectiligne ';
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheRectiligne.CalculDzetaApresBifurc2SortiesMini: Real;
Var
  m_DiamLat     : Integer;
  m_DebitLat    : Real;
  m_BrLat       : TVentil_Troncon;
  m_Bifurc      : TVentil_Troncon;
Begin

  if Etude.DTU2014 And (TVentil_TronconDessin_SortieCaisson(TronconPorteur.SortieCaissonParent).TypeSortie = c_CaissonSortie_Insufflation) then
        begin
        Result := CacluDzetaDTU2014DivergenceDebitMini;
        end
      else
        begin
        m_Bifurc := TronconPorteur.BifurcationParente;
        TronconCalculPDCSinguliere := m_Bifurc;
        m_BrLat := TVentil_Bifurcation(m_Bifurc).SortieLaterale;
        If IsNotNil(m_BrLat) Then
          Begin
          m_DebitLat := m_BrLat.DebitMini;
          m_DiamLat  := m_BrLat.Diametre;
          //Marche sur l'ancien DTU aussi
          Result := CalculDzetaDTU2014DebitMini(m_DebitLat, m_DiamLat);
          end;
        End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheRectiligne.CalculDzetaApresBifurc2SortiesMaxi: Real;
Var
  m_DiamLat     : Integer;
  m_DebitLat    : Real;
  m_BrLat       : TVentil_Troncon;
  m_Bifurc      : TVentil_Troncon;
Begin

  if Etude.DTU2014 And (TVentil_TronconDessin_SortieCaisson(TronconPorteur.SortieCaissonParent).TypeSortie = c_CaissonSortie_Insufflation) then
        begin
        Result := CacluDzetaDTU2014DivergenceDebitMaxi;
        end
      else
        begin
        m_Bifurc := TronconPorteur.BifurcationParente;
        TronconCalculPDCSinguliere := m_Bifurc;
        m_BrLat := TVentil_Bifurcation(m_Bifurc).SortieLaterale;
        If IsNotNil(m_BrLat) Then
          Begin
          m_DebitLat := m_BrLat.DebitMaxi;
          m_DiamLat  := m_BrLat.Diametre;
          //Marche sur l'ancien DTU aussi
          Result := CalculDzetaDTU2014DebitMaxi(m_DebitLat, m_DiamLat);
          end;
        end;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheRectiligne.CalculDzetaApresBifurc3SortiesMini: Real;
Var
  m_DiamLat     : Real;
  m_DebitLat    : Real;
  m_Bifurc      : TVentil_Troncon;
  m_NoSortie        : Integer;
Begin
  if Etude.DTU2014 then
    begin
      if TVentil_TronconDessin_SortieCaisson(TronconPorteur.SortieCaissonParent).TypeSortie = c_CaissonSortie_Insufflation then
        begin
        Result := CacluDzetaDTU2014DivergenceDebitMini;
        end
      else
        begin
        m_Bifurc := TronconPorteur.BifurcationParente;
        TronconCalculPDCSinguliere := m_Bifurc;
        m_DebitLat := 0;
        m_DiamLat  := 0;
          For m_NoSortie := 0 To m_Bifurc.Fils.Count - 1 Do
            If Supports(m_Bifurc.Fils[m_NoSortie].LienCad, IAero_Bifurcation_SortieL) Then
              Begin
              m_DebitLat := m_DebitLat + m_Bifurc.Fils[m_NoSortie].DebitMini;
              m_DiamLat := m_DiamLat + Sqr(m_Bifurc.Fils[m_NoSortie].Diametre);
              End;
          If m_DebitLat > 0 Then
            Begin
            m_DiamLat  := Sqrt(m_DiamLat);
            //Marche sur l'ancien DTU aussi
            Result := CalculDzetaDTU2014DebitMini(m_DebitLat, m_DiamLat);
            end;
        end;
    end
  else
    Result := 0;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheRectiligne.CalculDzetaApresBifurc3SortiesMaxi: Real;
Var
  m_DiamLat     : Real;
  m_DebitLat    : Real;
  m_Bifurc      : TVentil_Troncon;
  m_NoSortie    : Integer;
Begin
  if Etude.DTU2014 then
    begin
      if TVentil_TronconDessin_SortieCaisson(TronconPorteur.SortieCaissonParent).TypeSortie = c_CaissonSortie_Insufflation then
        begin
        Result := CacluDzetaDTU2014DivergenceDebitMaxi;
        end
      else
        begin
        m_Bifurc := TronconPorteur.BifurcationParente;
        TronconCalculPDCSinguliere := m_Bifurc;
        m_DebitLat := 0;
        m_DiamLat  := 0;
          For m_NoSortie := 0 To m_Bifurc.Fils.Count - 1 Do
            If Supports(m_Bifurc.Fils[m_NoSortie].LienCad, IAero_Bifurcation_SortieL) Then
              Begin
              m_DebitLat := m_DebitLat + m_Bifurc.Fils[m_NoSortie].DebitMaxi;
              m_DiamLat := m_DiamLat + Sqr(m_Bifurc.Fils[m_NoSortie].Diametre);
              End;
          If m_DebitLat > 0 Then
            Begin
            m_DiamLat  := Sqrt(m_DiamLat);
            //Marche sur l'ancien DTU aussi
            Result := CalculDzetaDTU2014DebitMaxi(m_DebitLat, m_DiamLat);
            end;
        end;
    end
  else
    Result := 0;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheRectiligne.CalculDzetaDTU2014DebitMini(_DebitLateral : Real; _DiamLateral : Real) : Real;
Var
  m_DebitBifurc : Real;
  m_DiamBifurc  : Integer;
  A             : Real;
  m_Bifurc      : TVentil_Troncon;
Begin
  m_Bifurc := TronconPorteur.BifurcationParente;
  TronconCalculPDCSinguliere := m_Bifurc;
  Result := 0;
  m_DebitBifurc := m_Bifurc.DebitMini;
  m_DiamBifurc  := m_Bifurc.Diametre;


  If (m_DebitBifurc <> 0) Then Begin
    A := _DebitLateral / m_DebitBifurc;
    If TVentil_Bifurcation(m_Bifurc).AngleBifurcation = c_Angle90 Then
      Result := 1.55 * A - Sqr(A)
    Else Result := Sqr(A) * ( -1 - 1.41 * Power(m_DiamBifurc / _DiamLateral, 4)) + 2 * A
  End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheRectiligne.CalculDzetaDTU2014DebitMaxi(_DebitLateral : Real; _DiamLateral : Real) : Real;
Var
  m_DebitBifurc : Real;
  m_DiamBifurc  : Integer;
  A             : Real;
  m_Bifurc      : TVentil_Troncon;
Begin
  m_Bifurc := TronconPorteur.BifurcationParente;
  TronconCalculPDCSinguliere := m_Bifurc;
  Result := 0;
  m_DebitBifurc := m_Bifurc.DebitMaxi;
  m_DiamBifurc  := m_Bifurc.Diametre;


  If (m_DebitBifurc <> 0) Then Begin
    A := _DebitLateral / m_DebitBifurc;
    If TVentil_Bifurcation(m_Bifurc).AngleBifurcation = c_Angle90 Then
      Result := 1.55 * A - Sqr(A)
    Else Result := Sqr(A) * ( -1 - 1.41 * Power(m_DiamBifurc / _DiamLateral, 4)) + 2 * A
  End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheRectiligne.CacluDzetaDTU2014DivergenceDebitMini : Real;
Begin
  Result := CacluDzetaDTU2014DivergenceDebitMaxi;
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheRectiligne.CacluDzetaDTU2014DivergenceDebitMaxi : Real;
Var
  m_Bifurc      : TVentil_Troncon;
  Section       : Real;
Begin

  m_Bifurc := TronconPorteur.BifurcationParente;
  TronconCalculPDCSinguliere := m_Bifurc;

  Section := Pi * Sqr(m_Bifurc.Diametre / 2);
  Result := 0.4 * Sqr(1 - Vitesse / Section);

end;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheRectiligne.CalculeDzetaMini: Real;
Begin
  If (TronconPorteur.ObjetTerminalAmont.InheritsFrom(TVentil_Bifurcation)) And
     TVentil_Bifurcation(TronconPorteur.ObjetTerminalAmont).BifurcationTriple Then DzetaMini := CalculDzetaApresBifurc3SortiesMini
                                                                           Else DzetaMini := CalculDzetaApresBifurc2SortiesMini;
  Result := Inherited CalculeDzetaMini;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_BrancheRectiligne.CalculeDzetaMaxi: Real;
Begin
  If (TronconPorteur.ObjetTerminalAmont.InheritsFrom(TVentil_Bifurcation)) And
     TVentil_Bifurcation(TronconPorteur.ObjetTerminalAmont).BifurcationTriple Then DzetaMaxi := CalculDzetaApresBifurc3SortiesMaxi
                                                                           Else DzetaMaxi := CalculDzetaApresBifurc2SortiesMaxi;
  Result := Inherited CalculeDzetaMaxi;
End;
{ ******************************************************************* \ TVentil_Accident_BrancheRectiligne ************************************************ }


{ ******************************************************************* TVentil_Accident_TeSouche *********************************************************** }
Procedure TVentil_Accident_TeSouche.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
Begin
  _Grid.Cells[_ColDep, _Row] := 'T�Souche';
  Inherited;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_TeSouche.CalculeDzetaMini: Real;
Begin
  TronconCalculPDCSinguliere := TronconPorteur;
  DzetaMini  := 2;
  Result := Inherited CalculeDzetaMini;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_TeSouche.CalculeDzetaMaxi: Real;
Begin
  TronconCalculPDCSinguliere := TronconPorteur;
  DzetaMaxi  := 2;
  Result := Inherited CalculeDzetaMaxi;
End;
{ ******************************************************************* \ TVentil_Accident_TeSouche ********************************************************* }

{ ******************************************************************* TVentil_Accident_TeSoucheBRect ****************************************************** }
Procedure TVentil_Accident_TeSoucheBRect.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
Begin
  _Grid.Cells[_ColDep, _Row] := 'Branche rectiligne';
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_TeSoucheBRect.CalculeDzetaMini: Real;
(*
Var
  m_DebitBifurc : Real;
  m_DiamBifurc  : Integer;
  A             : Real;
  m_Bifurc      : TVentil_Troncon;
Begin
  m_Bifurc := TronconPorteur.BifurcationParente;
  Result := 0;
  m_DebitBifurc := m_Bifurc.DebitMini;
  m_DiamBifurc  := m_Bifurc.Diametre;


  If (m_DebitBifurc <> 0) Then Begin
    A := _DebitLateral / m_DebitBifurc;
    If TVentil_Bifurcation(m_Bifurc).AngleBifurcation = c_Angle90 Then
      Result := 1.55 * A - Sqr(A)
    Else Result := Sqr(A) * ( -1 - 1.41 * Power(m_DiamBifurc / _DiamLateral, 4)) + 2 * A
  End;
End;
*)

Var
  m_TeSouche   : TVentil_TeSouche;
  m_GrandFrere : TVentil_Troncon;
  m_Somme      : Real;
  m_NoTr       : Integer;
  A            : Real;
Begin
  TronconCalculPDCSinguliere := TronconPorteur;
//  m_TeSouche := TVentil_TeSouche(TronconPorteur.GetParentColonnePlenum);
  m_TeSouche := TVentil_TeSouche(TronconPorteur.GetParentTeSouche);
  DzetaMini := 0;
  Result := Inherited CalculeDzetaMaxi;
    If (m_TeSouche <> Nil) and (m_TeSouche.InheritsFrom(TVentil_TeSouche) or (m_TeSouche.InheritsFrom(TVentil_Colonne) and (TVentil_Colonne(m_TeSouche).NatureConduit = C_NatureConduitMaconne))) Then
      Begin
      m_GrandFrere  := TronconPorteur.Parentcalcul.FilsCalcul[TronconPorteur.Parentcalcul.FilsCalcul.IndexOf(TronconPorteur) - 1];
      m_Somme := 0;
      For M_NoTr := 0 To m_GrandFrere.FilsCalcul.Count - 1 Do m_Somme := m_Somme + m_GrandFrere.FilsCalcul[m_NoTr].DebitMini;
      If m_GrandFrere.DebitMini > 0 Then A :=  m_Somme / m_GrandFrere.DebitMini
                                    Else A := 0.5;
      DzetaMini := - Sqr(A) + 1.55 * A;
      Result := Inherited CalculeDzetaMini;
      End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_TeSoucheBRect.CalculeDzetaMaxi: Real;
Var
  m_TeSouche   : TVentil_TeSouche;
  m_GrandFrere : TVentil_Troncon;
  m_Somme      : Real;
  m_NoTr       : Integer;
  A            : Real;
Begin
  TronconCalculPDCSinguliere := TronconPorteur;
//  m_TeSouche := TVentil_TeSouche(TronconPorteur.GetParentColonnePlenum);
  m_TeSouche := TVentil_TeSouche(TronconPorteur.GetParentTeSouche);
  DzetaMaxi := 0;
  Result := Inherited CalculeDzetaMaxi;
    If (m_TeSouche <> Nil) and (m_TeSouche.InheritsFrom(TVentil_TeSouche) or (m_TeSouche.InheritsFrom(TVentil_Colonne) and (TVentil_Colonne(m_TeSouche).NatureConduit = C_NatureConduitMaconne))) Then
      Begin
      m_GrandFrere  := TronconPorteur.Parentcalcul.FilsCalcul[TronconPorteur.Parentcalcul.FilsCalcul.IndexOf(TronconPorteur) - 1];
      m_Somme := 0;
      For M_NoTr := 0 To m_GrandFrere.FilsCalcul.Count - 1 Do m_Somme := m_Somme + m_GrandFrere.FilsCalcul[m_NoTr].DebitMaxi;
      If m_GrandFrere.DebitMaxi > 0 Then A :=  m_Somme / m_GrandFrere.DebitMaxi
                                    Else A := 0.5;
      DzetaMaxi := - Sqr(A) + 1.55 * A;
      Result := Inherited CalculeDzetaMaxi;
      End;
End;
{ ******************************************************************* \ TVentil_Accident_TeSoucheBRect *************************************************** }

{ ******************************************************************* TVentil_Accident_TeSoucheBRect ****************************************************** }
Procedure TVentil_Accident_TeSoucheBRectAlsace.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
Begin
  _Grid.Cells[_ColDep, _Row] := 'Branche rectiligne alsace';
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_TeSoucheBRectAlsace.CalculeDzetaMini: Real;
(*
Var
  m_DebitBifurc : Real;
  m_DiamBifurc  : Integer;
  A             : Real;
  m_Bifurc      : TVentil_Troncon;
Begin
  m_Bifurc := TronconPorteur.BifurcationParente;
  Result := 0;
  m_DebitBifurc := m_Bifurc.DebitMini;
  m_DiamBifurc  := m_Bifurc.Diametre;


  If (m_DebitBifurc <> 0) Then Begin
    A := _DebitLateral / m_DebitBifurc;
    If TVentil_Bifurcation(m_Bifurc).AngleBifurcation = c_Angle90 Then
      Result := 1.55 * A - Sqr(A)
    Else Result := Sqr(A) * ( -1 - 1.41 * Power(m_DiamBifurc / _DiamLateral, 4)) + 2 * A
  End;
End;
*)

Var
  m_TeSouche   : TVentil_TeSouche;
  m_GrandFrere : TVentil_Troncon;
  m_Somme      : Real;
  m_NoTr       : Integer;
  A            : Real;
Begin
  TronconCalculPDCSinguliere := TronconPorteur;
//  m_TeSouche := TVentil_TeSouche(TronconPorteur.GetParentColonnePlenum);
  m_TeSouche := TVentil_TeSouche(TronconPorteur.GetParentTeSouche);
  DzetaMini := 0;
  Result := Inherited CalculeDzetaMaxi;
    If (m_TeSouche <> Nil) and (m_TeSouche.InheritsFrom(TVentil_TeSouche) or (m_TeSouche.InheritsFrom(TVentil_Colonne) and (TVentil_Colonne(m_TeSouche).NatureConduit = C_NatureConduitMaconne))) Then
      Begin
//      m_GrandFrere  := TronconPorteur;
      m_GrandFrere  := TronconPorteur.Parentcalcul.FilsCalcul[TronconPorteur.Parentcalcul.FilsCalcul.IndexOf(TronconPorteur) - 1];
      m_Somme := 0;
      For M_NoTr := 0 To m_GrandFrere.FilsCalcul.Count - 1 Do m_Somme := m_Somme + m_GrandFrere.FilsCalcul[m_NoTr].DebitMini;
      If m_GrandFrere.DebitMini > 0 Then A :=  m_Somme / m_GrandFrere.DebitMini
                                    Else A := 0.5;
      DzetaMini := - Sqr(A) + 1.55 * A;
      Result := Inherited CalculeDzetaMini;
      End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_TeSoucheBRectAlsace.CalculeDzetaMaxi: Real;
Var
  m_TeSouche   : TVentil_TeSouche;
  m_GrandFrere : TVentil_Troncon;
  m_Somme      : Real;
  m_NoTr       : Integer;
  A            : Real;
Begin
  TronconCalculPDCSinguliere := TronconPorteur;
//  m_TeSouche := TVentil_TeSouche(TronconPorteur.GetParentColonnePlenum);
  m_TeSouche := TVentil_TeSouche(TronconPorteur.GetParentTeSouche);
  DzetaMaxi := 0;
  Result := Inherited CalculeDzetaMaxi;
    If (m_TeSouche <> Nil) and (m_TeSouche.InheritsFrom(TVentil_TeSouche) or (m_TeSouche.InheritsFrom(TVentil_Colonne) and (TVentil_Colonne(m_TeSouche).NatureConduit = C_NatureConduitMaconne))) Then
      Begin
//      m_GrandFrere  := TronconPorteur;
      m_GrandFrere  := TronconPorteur.Parentcalcul.FilsCalcul[TronconPorteur.Parentcalcul.FilsCalcul.IndexOf(TronconPorteur) - 1];
      m_Somme := 0;
      For M_NoTr := 0 To m_GrandFrere.FilsCalcul.Count - 1 Do m_Somme := m_Somme + m_GrandFrere.FilsCalcul[m_NoTr].DebitMaxi;
      If m_GrandFrere.DebitMaxi > 0 Then A :=  m_Somme / m_GrandFrere.DebitMaxi
                                    Else A := 0.5;
      DzetaMaxi := - Sqr(A) + 1.55 * A;
      Result := Inherited CalculeDzetaMaxi;
      End;
End;
{ ******************************************************************* \ TVentil_Accident_TeSoucheBRectAlsace *************************************************** }

{ ******************************************************************* TVentil_Accident_TeSoucheBRectShunt ****************************************************** }
Procedure TVentil_Accident_TeSoucheBRectShunt.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
Begin
  Inherited;
  _Grid.Cells[_ColDep, _Row] := 'Branche rectiligne shunt';
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_TeSoucheBRectShunt.CalculePDCSingulieresAdditionnelles : Real;
Var
  m_Collecteur  : TVentil_Collecteur;
  m_TeSouche    : TVentil_TeSouche;
  V1 : Real;
  V2 : Real;
  Q1 : Real;
  Q2 : Real;
Begin
//    m_Colonne := TVentil_Colonne(TronconPorteur.GetParentColonnePlenum);
    m_TeSouche := TVentil_TeSouche(TronconPorteur.GetParentTeSouche);
    Result := 0;
    PDCSingulieresAdditionnelles := Result;

    If (m_TeSouche <> Nil) and m_TeSouche.InheritsFrom(TVentil_Colonne) and (TVentil_Colonne(m_TeSouche).NatureConduit = C_NatureConduitMaconne) Then
      Begin
      m_Collecteur  := TVentil_Collecteur(TronconPorteur.Parentcalcul.FilsCalcul[TronconPorteur.Parentcalcul.FilsCalcul.IndexOf(TronconPorteur) - 1]);
      V1 := TronconPorteur.Vitesse;
      V2 := m_Collecteur.SommeVitesseMaxiSortiesLaterales;
      Q1 := TronconPorteur.DebitMaxi;
      Q2 := m_Collecteur.SommeDebitMaxiSortiesLaterales;
//      Result := (1.59 * V1 - 0.8) * V2;
      Result := (1.1 * Power(10,-4) * Q1 - 80 * Power(10, -4)) * Q2;
      PDCSingulieresAdditionnelles := Result;
      End;

End;
{ ******************************************************************* \ TVentil_Accident_TeSoucheBRectShunt *************************************************** }

{ ******************************************************************* TVentil_Accident_TeSoucheCoude ***************************************************** }
Procedure TVentil_Accident_TeSoucheCoude.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
Begin
  _Grid.Cells[_ColDep, _Row] := 'Coude 90�';
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_TeSoucheCoude.CalculeDzetaMini: Real;
Begin
  TronconCalculPDCSinguliere := TronconPorteur;
  DzetaMini := c_Coude__Tableau5_Dzeta_CoudeArrondi[c_Coude_90];
  Result := Inherited CalculeDzetaMini;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_TeSoucheCoude.CalculeDzetaMaxi: Real;
Begin
  TronconCalculPDCSinguliere := TronconPorteur;
  DzetaMaxi := c_Coude__Tableau5_Dzeta_CoudeArrondi[c_Coude_90];
  Result := Inherited CalculeDzetaMaxi;
End;
{ ******************************************************************* \ TVentil_Accident_TeSoucheCoude *************************************************** }

{ ******************************************************************* TVentil_Accident_TeSouchePlenumColonneVMC *********************************************************** }
Procedure TVentil_Accident_TeSouchePlenumColonneVMC.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
Begin
  _Grid.Cells[_ColDep, _Row] := 'Colonne VMC';
  Inherited;
    if Etude.Batiment.IsFabricantMVN and Not(VersionRnD) then
      begin
        If _ColDep = 0 Then
        _Grid.Cells[_ColDep + 1, _Row] := '-'
        else
        _Grid.Cells[_ColDep + 1, _Row] := 'Dz�ta: -';
      end;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_TeSouchePlenumColonneVMC.CalculeDzetaMini: Real;
Var
  m_DebitBifurc : Real;
  m_DiamBifurc  : Integer;
  m_DiamLat     : Integer;
  m_DebitLat    : Real;
  A             : Real;
  A_            : Real;
  m_Bifurc      : TVentil_Troncon;
Begin
//todo
//confluence lat�rale
    TronconCalculPDCSinguliere := TronconPorteur;
    m_Bifurc := TronconPorteur.ObjetTerminalAmont;
    Result := 0;
    m_DebitBifurc := m_Bifurc.DebitMini;
    m_DiamBifurc  := m_Bifurc.Diametre;
    If (m_DebitBifurc <> 0) Then Begin
      m_DebitLat := TronconPorteur.DebitMini;
      m_DiamLat  := TronconPorteur.Diametre;
      A := m_DebitLat / m_DebitBifurc;
      If m_DiamLat / m_DiamBifurc < 0.5 Then A_ := 1
                                        Else If m_DiamLat / m_DiamBifurc < 0.75 Then A_ := 0.75
                                                                                ELse A_ := 0.65;
//      If TVentil_Bifurcation(m_Bifurc).AngleBifurcation = c_Angle90 Then
        Result := A_ * (Sqr(A) * (Power(m_DiamBifurc / m_DiamLat ,4) - 2) + 4 * A - 1);
//      Else Result := Sqr(A) * (Power(m_DiamBifurc / m_DiamLat, 4) - 1.41 * Sqr(m_DiamBifurc / m_DiamLat) - 2) + 4 * A + 1;
    End;
  DzetaMini := Result;
  Result := Inherited CalculeDzetaMini;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_TeSouchePlenumColonneVMC.CalculeDzetaMaxi: Real;
Var
  m_DebitBifurc : Real;
  m_DiamBifurc  : Integer;
  m_DiamLat     : Integer;
  m_DebitLat    : Real;
  A             : Real;
  A_            : Real;
  m_Bifurc      : TVentil_Troncon;
Begin

    TronconCalculPDCSinguliere := TronconPorteur;
    m_Bifurc := TronconPorteur.ObjetTerminalAmont;
    Result := 0;
    m_DebitBifurc := m_Bifurc.DebitMaxi;
    m_DiamBifurc  := m_Bifurc.Diametre;
    If (m_DebitBifurc <> 0) Then Begin
      m_DebitLat := TronconPorteur.DebitMaxi;
      m_DiamLat  := TronconPorteur.Diametre;
      A := m_DebitLat / m_DebitBifurc;
      If m_DiamLat / m_DiamBifurc < 0.5 Then A_ := 1
                                        Else If m_DiamLat / m_DiamBifurc < 0.75 Then A_ := 0.75
                                                                                ELse A_ := 0.65;
//      If TVentil_Bifurcation(m_Bifurc).AngleBifurcation = c_Angle90 Then
        Result := A_ * (Sqr(A) * (Power(m_DiamBifurc / m_DiamLat ,4) - 2) + 4 * A - 1);
//      Else Result := Sqr(A) * (Power(m_DiamBifurc / m_DiamLat, 4) - 1.41 * Sqr(m_DiamBifurc / m_DiamLat) - 2) + 4 * A + 1;
    End;

  DzetaMaxi := Result;
  Result := Inherited CalculeDzetaMaxi;
End;
{ ******************************************************************* \ TVentil_Accident_TeSouchePlenumColonneVMC ********************************************************* }

{ ******************************************************************* TVentil_Accident_TeSouchePlenumColonneShunt *********************************************************** }
Procedure TVentil_Accident_TeSouchePlenumColonneShunt.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
Begin
  _Grid.Cells[_ColDep, _Row] := 'Colonne individuel shunt';
  Inherited;
    if Etude.Batiment.IsFabricantMVN and Not(VersionRnD) then
      begin
        If _ColDep = 0 Then
        _Grid.Cells[_ColDep + 1, _Row] := '-'
        else
        _Grid.Cells[_ColDep + 1, _Row] := 'Dz�ta: -';
      end;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_TeSouchePlenumColonneShunt.CalculeDzetaMini: Real;
Var
  m_DebitBifurc : Real;
  m_DiametreColonne : Integer;
  m_DiametreSortieLaterale : Integer;
  m_DebitLat    : Real;
  A             : Real;
//  A_            : Real;
  m_Collecteur  : TVentil_Troncon;
  m_Colonne     : TVentil_Colonne;
Begin
    TronconCalculPDCSinguliere := TronconPorteur;
    m_Colonne := TVentil_Colonne(TronconPorteur.GetParentColonnePlenum);
    Result := 0;
    DzetaMini := Result;
    If TVentil_Colonne(m_Colonne).NatureConduit = C_NatureConduitMetallique Then
      Begin
      m_Collecteur := TronconPorteur.ObjetTerminalAmont;


      m_DiametreColonne := m_Colonne.Diametre;
      m_DiametreSortieLaterale := TronconPorteur.Diametre;
      //Modif Acthys Faupin mail du 31/03/2017
      TronconPorteur.GetDiametreForPDC(m_DiametreSortieLaterale, True);

      m_DebitBifurc := m_Collecteur.DebitMini;
        If (m_DebitBifurc <> 0) Then
          Begin
          m_DebitLat := TronconPorteur.DebitMini;
      //Modif Acthys Faupin mail du 03/04/2017
          m_DebitLat := TVentil_Collecteur(m_Collecteur).SommeDebitMiniSortiesLaterales;
          A := m_DebitLat / m_DebitBifurc;
{
            If m_DiametreSortieLaterale / m_DiametreColonne < 0.5 Then
              A_ := 1
            Else
            If m_DiametreSortieLaterale / m_DiametreColonne < 0.75 Then
              A_ := 0.75
            ELse
              A_ := 0.65;
}
//      If TVentil_Colonne(m_Colonne).NatureConduit = C_NatureConduitMetallique Then
//          Result := A_ * (Sqr(A) * (Power(m_DiametreColonne / m_DiametreSortieLaterale ,4) - 2) + 4 * A - 1);
//      Else
      Result := Sqr(A) * (Power(m_DiametreColonne / m_DiametreSortieLaterale, 4) - 1.41 * Sqr(m_DiametreColonne / m_DiametreSortieLaterale) - 2) + 4 * A + 1;
          End;
      End;

    if Etude.Batiment.IsFabricantACT then
      Result := Result + c_Coude__Tableau5_Dzeta_CoudeArrondi[c_Coude_90]
    else
      Result := Result + c_Coude__Tableau5_Dzeta_CoudeMoyen[c_Coude_90];

  DzetaMini := Result;
  Result := Inherited CalculeDzetaMini;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_TeSouchePlenumColonneShunt.CalculeDzetaMaxi: Real;
Var
  m_DebitBifurc : Real;
  m_DiametreColonne : Integer;
  m_DiametreSortieLaterale : Integer;
  m_DebitLat    : Real;
  A             : Real;
  A_            : Real;
  m_Collecteur  : TVentil_Troncon;
  m_Colonne     : TVentil_Colonne;
Begin
    TronconCalculPDCSinguliere := TronconPorteur;
    m_Colonne := TVentil_Colonne(TronconPorteur.GetParentColonnePlenum);
    Result := 0;
    DzetaMaxi := Result;
    If TVentil_Colonne(m_Colonne).NatureConduit = C_NatureConduitMetallique Then
      Begin
      m_Collecteur := TronconPorteur.ObjetTerminalAmont;


      m_DiametreColonne := m_Colonne.Diametre;
      m_DiametreSortieLaterale := TronconPorteur.Diametre;
      //Modif Acthys Faupin mail du 31/03/2017
      TronconPorteur.GetDiametreForPDC(m_DiametreSortieLaterale, True);

      m_DebitBifurc := m_Collecteur.DebitMaxi;
        If (m_DebitBifurc <> 0) Then
          Begin
//          m_DebitLat := TronconPorteur.DebitMaxi;
      //Modif Acthys Faupin mail du 03/04/2017
          m_DebitLat := TVentil_Collecteur(m_Collecteur).SommeDebitMaxiSortiesLaterales;
          A := m_DebitLat / m_DebitBifurc;
            If m_DiametreSortieLaterale / m_DiametreColonne < 0.5 Then
              A_ := 1
            Else
            If m_DiametreSortieLaterale / m_DiametreColonne < 0.75 Then
              A_ := 0.75
            ELse
              A_ := 0.65;
//      If TVentil_Colonne(m_Colonne).NatureConduit = C_NatureConduitMetallique Then
//          Result := A_ * (Sqr(A) * (Power(m_DiametreColonne / m_DiametreSortieLaterale ,4) - 2) + 4 * A - 1);
//      Else
      Result := Sqr(A) * (Power(m_DiametreColonne / m_DiametreSortieLaterale, 4) - 1.41 * Sqr(m_DiametreColonne / m_DiametreSortieLaterale) - 2) + 4 * A + 1;
          End;
      End;

    if Etude.Batiment.IsFabricantACT then
      Result := Result + c_Coude__Tableau5_Dzeta_CoudeArrondi[c_Coude_90]
    else
      Result := Result + c_Coude__Tableau5_Dzeta_CoudeMoyen[c_Coude_90];

  DzetaMaxi := Result;
  Result := Inherited CalculeDzetaMaxi;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_TeSouchePlenumColonneShunt.CalculePDCSingulieresAdditionnelles : Real;
Var
  m_Collecteur  : TVentil_Collecteur;
  m_Colonne     : TVentil_Colonne;
//  V1 : Real;
//  V2 : Real;
  Q1 : Real;
  Q2 : Real;
Begin
    m_Colonne := TVentil_Colonne(TronconPorteur.GetParentColonnePlenum);
    Result := 0;
    PDCSingulieresAdditionnelles := Result;

    If (TVentil_Colonne(m_Colonne).NatureConduit = C_NatureConduitMaconne) Then
      Begin
      m_Collecteur := TVentil_Collecteur(TronconPorteur.ObjetTerminalAmont);
        if (m_Collecteur.SortieRectiligne <> Nil) and (m_Collecteur.SortieRectiligne.Fils.Count > 0) then
          begin
//          V1 := m_Collecteur.SortieRectiligne.Fils[0].Vitesse;
          Q1 := m_Collecteur.SortieRectiligne.Fils[0].DebitMaxi;
          end
        else
          begin
//          V1 := 0;
          Q1 := 0;
          end;
        if (TronconPorteur.Parentcalcul <> Nil) and TronconPorteur.Parentcalcul.InheritsFrom(TVentil_Collecteur) then
          begin
//          V2 := TVentil_Collecteur(TronconPorteur.Parentcalcul).SommeVitesseMaxiSortiesLaterales;
          Q2 := TVentil_Collecteur(TronconPorteur.Parentcalcul).SommeDebitMaxiSortiesLaterales;
          end
        else
          begin
//          V2 := TronconPorteur.Vitesse;
          Q2 := TronconPorteur.DebitMaxi;
          end;
//          Q2 := TronconPorteur.DebitMaxi;
//      Result := (1.59 * V1 + 0.8) * V2;
      Result := (1.1 * Power(10,-4) * Q1 + 80 * Power(10, -4)) * Q2;
      PDCSingulieresAdditionnelles := Result;
      End;

End;
{ ******************************************************************* \ TVentil_Accident_TeSouchePlenumColonneShunt ********************************************************* }

{ ******************************************************************* TVentil_Accident_TeSouchePlenumColonneIndividuel *********************************************************** }
Procedure TVentil_Accident_TeSouchePlenumColonneIndividuel.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
Begin
  _Grid.Cells[_ColDep, _Row] := 'Colonne Individuelle';
  Inherited;
    if Etude.Batiment.IsFabricantMVN and Not(VersionRnD) then
      begin
        If _ColDep = 0 Then
        _Grid.Cells[_ColDep + 1, _Row] := '-'
        else
        _Grid.Cells[_ColDep + 1, _Row] := 'Dz�ta: -';
      end;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_TeSouchePlenumColonneIndividuel.CalculeDzetaMini: Real;
Begin
    TronconCalculPDCSinguliere := TronconPorteur;
    if Etude.Batiment.IsFabricantACT then
      DzetaMini  := c_Coude__Tableau5_Dzeta_CoudeArrondi[c_Coude_90]
    else
      DzetaMini  := c_Coude__Tableau5_Dzeta_CoudeMoyen[c_Coude_90];
  Result := Inherited CalculeDzetaMini;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_TeSouchePlenumColonneIndividuel.CalculeDzetaMaxi: Real;
Begin
    TronconCalculPDCSinguliere := TronconPorteur;
    if Etude.Batiment.IsFabricantACT then
      DzetaMaxi  := c_Coude__Tableau5_Dzeta_CoudeArrondi[c_Coude_90]
    else
      DzetaMaxi  := c_Coude__Tableau5_Dzeta_CoudeMoyen[c_Coude_90];
  Result := Inherited CalculeDzetaMaxi;
End;
{ ******************************************************************* \ TVentil_Accident_TeSouchePlenumColonneIndividuel ********************************************************* }

{ ******************************************************************* TVentil_Accident_TeSouchePlenumColonneAlsace *********************************************************** }
Procedure TVentil_Accident_TeSouchePlenumColonneAlsace.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
Begin
  _Grid.Cells[_ColDep, _Row] := 'Colonne Alsace';
  Inherited;
    if Etude.Batiment.IsFabricantMVN and Not(VersionRnD) then
      begin
        If _ColDep = 0 Then
        _Grid.Cells[_ColDep + 1, _Row] := '-'
        else
        _Grid.Cells[_ColDep + 1, _Row] := 'Dz�ta: -';
      end;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_TeSouchePlenumColonneAlsace.CalculeDzetaMini: Real;
Var
  m_DebitBifurc : Real;
  m_DiamBifurc  : Integer;
  m_DiamLat     : Integer;
  m_DebitLat    : Real;
  A             : Real;
  A_            : Real;
  m_Bifurc      : TVentil_Troncon;
Begin
//todo
//confluence lat�rale
    TronconCalculPDCSinguliere := TronconPorteur;
    m_Bifurc := TronconPorteur.ObjetTerminalAmont;
    Result := 0;
    m_DebitBifurc := m_Bifurc.DebitMini;
    m_DiamBifurc  := m_Bifurc.Diametre;
    If (m_DebitBifurc <> 0) Then Begin
      m_DebitLat := TronconPorteur.DebitMini;
      m_DiamLat  := TronconPorteur.Diametre;
      //Modif Acthys Faupin mail du 31/03/2017
      TronconPorteur.GetDiametreForPDC(m_DiamLat, True);
      A := m_DebitLat / m_DebitBifurc;
      If m_DiamLat / m_DiamBifurc < 0.5 Then A_ := 1
                                        Else If m_DiamLat / m_DiamBifurc < 0.75 Then A_ := 0.75
                                                                                ELse A_ := 0.65;
//      If TVentil_Bifurcation(m_Bifurc).AngleBifurcation = c_Angle90 Then
        Result := A_ * (Sqr(A) * (Power(m_DiamBifurc / m_DiamLat ,4) - 2) + 4 * A - 1);
//      Else Result := Sqr(A) * (Power(m_DiamBifurc / m_DiamLat, 4) - 1.41 * Sqr(m_DiamBifurc / m_DiamLat) - 2) + 4 * A + 1;
    End;
  DzetaMini := Result;
  Result := Inherited CalculeDzetaMini;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_TeSouchePlenumColonneAlsace.CalculeDzetaMaxi: Real;
Var
  m_DebitBifurc : Real;
  m_DiamBifurc  : Integer;
  m_DiamLat     : Integer;
  m_DebitLat    : Real;
  A             : Real;
  A_            : Real;
  m_Bifurc      : TVentil_Troncon;
Begin

    TronconCalculPDCSinguliere := TronconPorteur;
    m_Bifurc := TronconPorteur.ObjetTerminalAmont;
    Result := 0;
    m_DebitBifurc := m_Bifurc.DebitMaxi;
    m_DiamBifurc  := m_Bifurc.Diametre;
    If (m_DebitBifurc <> 0) Then Begin
      m_DebitLat := TronconPorteur.DebitMaxi;
      m_DiamLat  := TronconPorteur.Diametre;
      //Modif Acthys Faupin mail du 31/03/2017
      TronconPorteur.GetDiametreForPDC(m_DiamLat, True);
      A := m_DebitLat / m_DebitBifurc;
      If m_DiamLat / m_DiamBifurc < 0.5 Then A_ := 1
                                        Else If m_DiamLat / m_DiamBifurc < 0.75 Then A_ := 0.75
                                                                                ELse A_ := 0.65;
//      If TVentil_Bifurcation(m_Bifurc).AngleBifurcation = c_Angle90 Then
        Result := A_ * (Sqr(A) * (Power(m_DiamBifurc / m_DiamLat ,4) - 2) + 4 * A - 1);
//      Else Result := Sqr(A) * (Power(m_DiamBifurc / m_DiamLat, 4) - 1.41 * Sqr(m_DiamBifurc / m_DiamLat) - 2) + 4 * A + 1;
    End;

  DzetaMaxi := Result;
  Result := Inherited CalculeDzetaMaxi;
End;
{ ******************************************************************* \ TVentil_Accident_TeSouchePlenumColonneAlsace ********************************************************* }

{ ******************************************************************* TVentil_Accident_TeSouchePlenumColonneRamon *********************************************************** }
Procedure TVentil_Accident_TeSouchePlenumColonneRamon.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
Begin
  _Grid.Cells[_ColDep, _Row] := 'Colonne ramon';
  Inherited;
    if Etude.Batiment.IsFabricantMVN and Not(VersionRnD) then
      begin
        If _ColDep = 0 Then
        _Grid.Cells[_ColDep + 1, _Row] := '-'
        else
        _Grid.Cells[_ColDep + 1, _Row] := 'Dz�ta: -';
      end;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_TeSouchePlenumColonneRamon.CalculeDzetaMini: Real;
Begin
    TronconCalculPDCSinguliere := TronconPorteur;
    if Etude.Batiment.IsFabricantACT then
      DzetaMini  := c_Coude__Tableau5_Dzeta_CoudeArrondi[c_Coude_90]
    else
      DzetaMini  := c_Coude__Tableau5_Dzeta_CoudeMoyen[c_Coude_90];
  Result := Inherited CalculeDzetaMini;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_TeSouchePlenumColonneRamon.CalculeDzetaMaxi: Real;
Begin
    TronconCalculPDCSinguliere := TronconPorteur;
    if Etude.Batiment.IsFabricantACT then
      DzetaMaxi  := c_Coude__Tableau5_Dzeta_CoudeArrondi[c_Coude_90]
    else
      DzetaMaxi  := c_Coude__Tableau5_Dzeta_CoudeMoyen[c_Coude_90];
  Result := Inherited CalculeDzetaMaxi;
End;
{ ******************************************************************* \ TVentil_Accident_TeSouchePlenumColonneRamon ********************************************************* }

{ ******************************************************************* TVentil_Accident_TeSouchePlenumColonneShuntPiquage *********************************************************** }
Procedure TVentil_Accident_TeSouchePlenumColonneShuntPiquage.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
Begin
  _Grid.Cells[_ColDep, _Row] := 'Colonne Shunt - Piquage';
  Inherited;
    if Etude.Batiment.IsFabricantMVN and Not(VersionRnD) then
      begin
        If _ColDep = 0 Then
        _Grid.Cells[_ColDep + 1, _Row] := '-'
        else
        _Grid.Cells[_ColDep + 1, _Row] := 'Dz�ta: -';
      end;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_TeSouchePlenumColonneShuntPiquage.CalculeDzetaMini: Real;
Var
  m_DebitBifurc : Real;
  m_DiametreColonne : Integer;
  m_DebitLat    : Real;
  A             : Real;
  A_            : Real;
  m_Collecteur  : TVentil_Troncon;
  m_Colonne     : TVentil_Colonne;
Begin
    TronconCalculPDCSinguliere := TronconPorteur;
    m_Collecteur := TronconPorteur.ObjetTerminalAmont;
    m_Colonne := TVentil_Colonne(TronconPorteur.GetParentColonnePlenum);
    m_DiametreColonne := m_Colonne.Diametre;

    Result := 0;
    m_DebitBifurc := m_Collecteur.DebitMini;
    If (m_DebitBifurc <> 0) Then Begin
      m_DebitLat := TronconPorteur.DebitMini;
      A := m_DebitLat / m_DebitBifurc;
      If m_DiametreColonne / m_DiametreColonne < 0.5 Then A_ := 1
                                        Else If m_DiametreColonne / m_DiametreColonne < 0.75 Then A_ := 0.75
                                                                                ELse A_ := 0.65;
      If TVentil_Colonne(m_Colonne).NatureConduit = C_NatureConduitMetallique Then
        Result := A_ * (Sqr(A) * (Power(m_DiametreColonne / m_DiametreColonne ,4) - 2) + 4 * A - 1)
      Else
      Result := Sqr(A) * (Power(m_DiametreColonne / m_DiametreColonne, 4) - 1.41 * Sqr(m_DiametreColonne / m_DiametreColonne) - 2) + 4 * A + 1;
    End;

    if Etude.Batiment.IsFabricantACT then
      Result := Result + c_Coude__Tableau5_Dzeta_CoudeArrondi[c_Coude_90]
    else
      Result := Result + c_Coude__Tableau5_Dzeta_CoudeMoyen[c_Coude_90];

  DzetaMini := Result;
  Result := Inherited CalculeDzetaMini;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_TeSouchePlenumColonneShuntPiquage.CalculeDzetaMaxi: Real;
Var
  m_DebitBifurc : Real;
  m_DiametreColonne : Integer;
  m_DebitLat    : Real;
  A             : Real;
  A_            : Real;
  m_Collecteur  : TVentil_Troncon;
  m_Colonne     : TVentil_Colonne;
Begin
    TronconCalculPDCSinguliere := TronconPorteur;
    m_Collecteur := TronconPorteur.ObjetTerminalAmont;
    m_Colonne := TVentil_Colonne(TronconPorteur.GetParentColonnePlenum);
    m_DiametreColonne := m_Colonne.Diametre;

    Result := 0;
    m_DebitBifurc := m_Collecteur.DebitMaxi;
    If (m_DebitBifurc <> 0) Then Begin
      m_DebitLat := TronconPorteur.DebitMaxi;
      A := m_DebitLat / m_DebitBifurc;
      If m_DiametreColonne / m_DiametreColonne < 0.5 Then A_ := 1
                                        Else If m_DiametreColonne / m_DiametreColonne < 0.75 Then A_ := 0.75
                                                                                ELse A_ := 0.65;
      If TVentil_Colonne(m_Colonne).NatureConduit = C_NatureConduitMetallique Then
        Result := A_ * (Sqr(A) * (Power(m_DiametreColonne / m_DiametreColonne ,4) - 2) + 4 * A - 1)
      Else
      Result := Sqr(A) * (Power(m_DiametreColonne / m_DiametreColonne, 4) - 1.41 * Sqr(m_DiametreColonne / m_DiametreColonne) - 2) + 4 * A + 1;
    End;

    if Etude.Batiment.IsFabricantACT then
      Result := Result + c_Coude__Tableau5_Dzeta_CoudeArrondi[c_Coude_90]
    else
      Result := Result + c_Coude__Tableau5_Dzeta_CoudeMoyen[c_Coude_90];

  DzetaMaxi := Result;
  Result := Inherited CalculeDzetaMaxi;
End;
{ ******************************************************************* \ TVentil_Accident_TeSouchePlenumColonneShuntPiquage ********************************************************* }

{ ******************************************************************* TVentil_Accident_Plenum *********************************************************** }
Procedure TVentil_Accident_Plenum.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
Begin
  if TronconPorteur.InheritsFrom(TVentil_TourellePlenum) then
    begin
      if Etude.Batiment.IsFabricantMVN then
        _Grid.Cells[_ColDep, _Row] := 'Novat�Air'
      else
        _Grid.Cells[_ColDep, _Row] := 'Pl�num';
    end
  else
  if TronconPorteur.InheritsFrom(TVentil_Plenum) then
    begin
      if Etude.Batiment.IsFabricantMVN then
        begin
          if TVentil_Plenum(TronconPorteur).Caisson.InheritsFrom(TVentil_TourelleTete) then
            _Grid.Cells[_ColDep, _Row] := c_LibellesTypePlenum_Tourelle[TVentil_Plenum(TronconPorteur).TypePlenum]
          else
            _Grid.Cells[_ColDep, _Row] := c_LibellesTypePlenum[TVentil_Plenum(TronconPorteur).TypePlenum];
        end
      else
        _Grid.Cells[_ColDep, _Row] := 'Pl�num';
    end;
  Inherited;
    if Etude.Batiment.IsFabricantMVN and Not(VersionRnD) then
      begin
        If _ColDep = 0 Then
        _Grid.Cells[_ColDep + 1, _Row] := '-'
        else
        _Grid.Cells[_ColDep + 1, _Row] := 'Dz�ta: -';
      end;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_Plenum.CalculeDzetaMini: Real;
Var
  m_Plenum : TVentil_Plenum;
Begin
  TronconCalculPDCSinguliere := TronconPorteur;
  m_Plenum := TVentil_Plenum(TronconPorteur);
  DzetaMini := m_Plenum.GetDzeta;
  Result := Inherited CalculeDzetaMini;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_Plenum.CalculeDzetaMaxi: Real;
Var
  m_Plenum : TVentil_Plenum;
Begin
  TronconCalculPDCSinguliere := TronconPorteur;
  m_Plenum := TVentil_Plenum(TronconPorteur);
  DzetaMaxi := m_Plenum.GetDzeta;
  Result := Inherited CalculeDzetaMaxi;
End;
{ ******************************************************************* \ TVentil_Accident_Plenum ********************************************************* }
//Abandonn�
(*
{ ******************************************************************* TVentil_Accident_TourelleIntermediaire *********************************************************** }
Procedure TVentil_Accident_TourelleIntermediaire.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
Begin
  _Grid.Cells[_ColDep, _Row] := 'Tourelle adaptation';
  Inherited;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_TourelleIntermediaire.CalculeDzetaMini: Real;
Var
  m_TourelleIntermediaire : TVentil_TourelleIntermediaire;
Begin
  TronconCalculPDCSinguliere := TronconPorteur;
  m_TourelleIntermediaire := TVentil_TourelleIntermediaire(TronconPorteur);
  DzetaMini := m_TourelleIntermediaire.GetDzeta;
  Result := Inherited CalculeDzetaMini;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_TourelleIntermediaire.CalculeDzetaMaxi: Real;
Var
  m_TourelleIntermediaire : TVentil_TourelleIntermediaire;
Begin
  TronconCalculPDCSinguliere := TronconPorteur;
  m_TourelleIntermediaire := TVentil_TourelleIntermediaire(TronconPorteur);
  DzetaMaxi := m_TourelleIntermediaire.GetDzeta;
  Result := Inherited CalculeDzetaMaxi;
End;
{ ******************************************************************* \ TVentil_Accident_TourelleIntermediaire ********************************************************* }
*)
{ ******************************************************************* TVentil_Accident_CoudeDessin ******************************************************** }
Constructor TVentil_Accident_CoudeDessin.Create;
begin
  Inherited;
  NbQuestions := NbQuestions + c_NbQ_Coude;
  IsEffetSysteme := False;
  FEntretoise_L := c_Non;
  FContreParoi := c_Oui;
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_CoudeDessin.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
var
  tempLibelle : String;
Begin
  If isNotNil(Etude.GridProp.Objects[0, 0]) And (Etude.GridProp.Objects[0, 0] <> Self) Then Begin
  tempLibelle := 'Coude ' +  c_Coude_Angles[AngleCoude];
    if isEffetSysteme then
      tempLibelle := tempLibelle + ' (effets systeme)';
    _Grid.Cells[_ColDep, _Row] := tempLibelle;
  End;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_CoudeDessin.Affiche(_Grid: TAdvStringGrid);
Var
  m_Row      : Integer;
  m_Question : Integer;
  m_Str      : String;
Begin
if etude.Batiment.IsFabricantVIM then
exit;

  Inherited;
  m_Row := GetNbQuestionAfficheTroncon + 1;
  For m_Question := Low(c_LibellesQ_Coude) To High(c_LibellesQ_Coude) Do If AfficheQuestion(m_Question) Then Begin
    _Grid.Cells[0, m_Row] := c_LibellesQ_Coude[m_Question];
    Case m_Question Of
      c_QCoude_EntretoiseL  : m_Str := c_OuiNon[FEntretoise_L];
      c_QCoude_ThAirContreParoi  : m_Str := c_OuiNon[FContreParoi];
      Else m_Str := '';
    End;
    _Grid.Cells[1, m_Row] := m_Str;
    Inc(m_Row);
  End;

End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_CoudeDessin.AfficheQuestion(_NoQuestion: Integer): Boolean;
Begin
  If _NoQuestion <= c_NbQ_Troncon Then begin
  Result := (_NoQuestion = c_QTroncon_Diametre) and Not(IsTrainasseT2A);
  end
  Else
  Case _NoQuestion Of
    c_QCoude_EntretoiseL : Result := CanHaveEntretoise;
    c_QCoude_ThAirContreParoi : Result := CanHaveThAirContreParoi;
    Else
    Result := False;
  End;

End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_CoudeDessin.HasComboBox(_Ligne: Integer): Boolean;
Begin
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Result := Inherited HasComboBox(_Ligne)
  Else
  Result := AfficheQuestion(NoQuestion(_Ligne)) And (NoQuestion(_Ligne) In [c_QCoude_EntretoiseL, c_QCoude_ThAirContreParoi]);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_CoudeDessin.Recupere(_Grid: TAdvStringGrid; _Ligne: Integer);
Begin
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Inherited
  Else
  If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
      c_QCoude_EntretoiseL        : FEntretoise_L := _Grid.Combobox.ItemIndex;
      c_QCoude_ThAirContreParoi   : FContreParoi := _Grid.Combobox.ItemIndex;
  End;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_CoudeDessin.TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType;
//Var
//  m_NoChoix : Integer;
Begin
  Result := edNone;
  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
  //Parfois le GoEditing saute sans raison...
  _Grid.Options := _Grid.Options + [goEditing];
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Result := Inherited TypeEdition(_Grid, _Ligne)
  Else
  If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QCoude_EntretoiseL : Begin
      Result := edComboList;
    End;
    c_QCoude_ThAirContreParoi : Begin
      Result := edComboList;
    End;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_CoudeDessin.PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer);
Var
  m_NoChoix : Integer;
Begin

  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Inherited
  Else
  If AfficheQuestion(NoQuestion(_Ligne)) Then
  begin

    Case NoQuestion(_Ligne) Of
    c_QCoude_EntretoiseL : Begin
      _Grid.ClearComboString;
      For m_NoChoix := Low(C_OUINON) To High(C_OUINON) Do _Grid.AddComboString(C_OUINON[m_NoChoix]);
    End;
    c_QCoude_ThAirContreParoi : Begin
      _Grid.ClearComboString;
      For m_NoChoix := Low(C_OUINON) To High(C_OUINON) Do _Grid.AddComboString(C_OUINON[m_NoChoix]);
    End;
    End;

  end;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_CoudeDessin.InfosFromCad;
Begin
  Inherited;
  If Supports(LienCad, IAero_Coude90) Then AngleCoude := c_Coude_90 Else
  If Supports(LienCad, IAero_Coude45) Then AngleCoude := c_Coude_45 Else
  If Supports(LienCad, IAero_Coude30) Then AngleCoude := c_Coude_30 Else
  { Cas des coudes t�s souches dessin�s } AngleCoude := c_Coude_90 ;
End;
{ ************************************************************************************************************************************************** }
class Function TVentil_Accident_CoudeDessin.NomParDefaut : String;
Begin
Result := 'Coude';
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_CoudeDessin.Save;
Begin
  Table_Etude_Coudes.Donnees.Insert;
  Table_Etude_Coudes.Donnees.FieldByName('ID_COUDE').AsWideString := GUIDToString(Id);
  {$IfDef AERAU_CLIMAWIN}
  Table_Etude_Coudes.Donnees.FieldByName('IDRESEAU').AsWideString := Etude.IdReseau;
  {$EndIf}
  Table_Etude_Coudes.Donnees.FieldByName('ENTRETOISE_L').AsInteger := FEntretoise_L;
  Table_Etude_Coudes.Donnees.FieldByName('CONTREPAROI').AsInteger := FContreParoi;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_CoudeDessin.Load;
Begin
  Inherited;
    if Table_Etude_Coudes.Donnees.FieldByName('ENTRETOISE_L').AsWideString <> '' then
      FEntretoise_L := Table_Etude_Coudes.Donnees.FieldByName('ENTRETOISE_L').AsInteger;
    if Table_Etude_Coudes.Donnees.FieldByName('CONTREPAROI').AsWideString <> '' then
      FContreParoi := Table_Etude_Coudes.Donnees.FieldByName('CONTREPAROI').AsInteger;
End;
{ ************************************************************************************************************************************************** }
class Function TVentil_Accident_CoudeDessin.FamilleObjet : String;
Begin
Result := 'Coude';
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_CoudeDessin.IsCoudeT2AActhys : Boolean;
begin
  Result :=  Etude.Batiment.IsFabricantACT and IsTrainasseT2A;
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_CoudeDessin.IsCoudeThAirMVN : Boolean;
begin
  Result :=  Etude.Batiment.IsFabricantMVN and IsTrainasseT2A;
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_CoudeDessin.CanHaveEntretoise : Boolean;
begin
  Result :=  Etude.Batiment.IsFabricantACT and IsTrainasseT2A;
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_CoudeDessin.CanHaveThAirContreParoi : Boolean;
begin
  Result :=  Etude.Batiment.IsFabricantMVN and IsTrainasseT2A and (Fils.Count = 0);
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_CoudeDessin.CalculerQuantitatifEntretoise;
var
  m_Produit : TEdibatec_Produit;
  m_messageErreur : String;
begin
  if not(CanHaveEntretoise) then
    Exit;

  if (FEntretoise_L = C_Oui) then
    begin
      if AngleCoude = c_Coude_90 then
        inc(GetLongueursConduits.NbEntretoiseT2AL90)
      else
      if AngleCoude = c_Coude_45 then
        inc(GetLongueursConduits.NbEntretoiseT2AL45)
      else
        Exit;

    end;


end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_CoudeDessin.CalculerQuantitatifInternal;
Var
  m_Classe  : TEdibatec_Classe;
  m_NoGam   : Integer;
  m_Matiere : Integer;
  m_Gamme   : TEdibatec_Gamme;
  m_Produit : TEdibatec_Coude;
  m_NoProd  : Integer;
  m_Erreur  : Boolean;
  m_ProduitBP : TEdibatec_Produit;
Begin
  Inherited;

  if IsCoudeT2AActhys then
  begin
  m_Erreur := False;
    case AngleCoude of
      c_Coude_45 : m_ProduitBP := BaseEdibatec.Produit(BaseEdibatec.Assistant.Produit('cst_T2A_Coude45'));
      c_Coude_90 : m_ProduitBP := BaseEdibatec.Produit(BaseEdibatec.Assistant.Produit('cst_T2A_Coude90'));
    else
      begin
      m_ProduitBP := Nil;
      m_Erreur := True;
      end;
    end;

          If IsNotNil(m_ProduitBP) Then
            AddChiffrage(m_ProduitBP, 1, cst_Vertical, cst_Chiff_AC);
  exit;
  end
  else
  if IsCoudeThAirMVN then
  begin
    if (parent <> nil) and Parent.InheritsFrom(TVentil_TronconDessin_Conduit) then
      if (fils <> nil) and (fils.Count = 1) and fils[0].InheritsFrom(TVentil_TronconDessin_Conduit) then
        if (TVentil_TronconDessin_Conduit(fils[0]).TypeThAir = c_TypeThAirU) then
          begin
          m_ProduitBP := BaseEdibatec.Produit(BaseEdibatec.Assistant.Produit('cst_ThAir_Bouchon'));
          If IsNotNil(m_ProduitBP) Then
            AddChiffrage(m_ProduitBP, 1, cst_Vertical, cst_Chiff_AC)
          else
            AddChiffrageErreur('Bouchon de condamnation', '', '');
          end;

    if CanHaveThAirContreParoi and (FContreParoi = c_Non) then
      begin
          m_ProduitBP := BaseEdibatec.Produit(BaseEdibatec.Assistant.Produit('cst_ThAir_Bouchon'));
          If IsNotNil(m_ProduitBP) Then
            AddChiffrage(m_ProduitBP, 1, cst_Vertical, cst_Chiff_AC)
          else
            AddChiffrageErreur('Bouchon de condamnation', '', '');
      end;
  exit;
  end
  else
  if not(etude.Batiment.IsFabricantVIM) then
  begin
  m_Matiere := GetMatiereAccessoire;
  m_Gamme := BaseEdibatec.Gamme(BaseEdibatec.Assistant.c_GammesCoudes[m_Matiere]);
  m_Erreur := True;
  For m_NoProd := 0 To m_Gamme.Produits.Count - 1 Do Begin
    m_Produit := TEdibatec_Coude(m_Gamme.Produits.Produit(m_NoProd));
//    If ( ( TronconGaz And (m_Produit.Matiere = c_Alu) ) Or ( (Not TronconGaz) And (m_Produit.Matiere <> c_Alu) ) ) And //  Mati�re OK
      If (m_Produit.Matiere = m_Matiere) And //  Mati�re OK
       (m_Produit.Diametre = Diametre) And // Diametre OK
       (m_Produit.Angle = AngleCoude) Then Begin // Angle coude OK
       If SurTerrasse Then AddChiffrage(m_Produit, 1, Cst_Horizontal, cst_Chiff_AC)
       Else
       AddChiffrage(m_Produit, 1, Cst_Vertical, cst_Chiff_AC);
       m_Erreur := False;
       Break;
    End;
  End;
  end
  else
  begin
  m_Classe := BaseEdibatec.Classe(c_Edibatec_Coudes);
  m_Erreur := True;
  m_Matiere := GetMatiereAccessoire;
  For m_NoGam := 0 To m_Classe.Gammes.Count - 1 Do Begin
    m_Gamme := m_Classe.Gammes.Gamme(m_NoGam);
    For m_NoProd := 0 To m_Gamme.Produits.Count - 1 Do Begin
      m_Produit := TEdibatec_Coude(m_Gamme.Produits.Produit(m_NoProd));
      If (m_Produit.Matiere = m_Matiere) And //  Mati�re OK
         (m_Produit.Diametre = Diametre) And // Diametre OK
         (m_Produit.Angle = AngleCoude) Then Begin // Angle coude OK
         If VersSortieToit Then AddChiffrage(m_Produit, 1, cst_Rejet, cst_Chiff_RE)
         Else
         If SurTerrasse Then AddChiffrage(m_Produit, 1, Cst_Horizontal, cst_Chiff_AC)
         Else
         AddChiffrage(m_Produit, 1, Cst_Vertical, cst_Chiff_AC);
         m_Erreur := False;
         Break;
      End;
    End;
  End;
  end;
  If m_Erreur Then Begin
    If TronconGaz Then AddChiffrageErreur('Coude', IntToStr(Diametre) + ' mm \ ' + c_Coude_Angles[AngleCoude] + '� Alu'{$IfDeF VIM_OPTAIR}, inttostr(1) + ' unit�'{$EndIf})
                  Else AddChiffrageErreur('Coude', IntToStr(Diametre) + ' mm \ ' + c_Coude_Angles[AngleCoude] + '� Galva'{$IfDeF VIM_OPTAIR}, inttostr(1) + ' unit�'{$EndIf})
  End;


  if Not(Parent.InheritsFrom(TVentil_TronconDessin_Conduit)) then
    begin
      If SurTerrasse Then
        AddNipples(1, Diametre, Cst_Horizontal, m_Matiere)
      else
        AddNipples(1, Diametre, cst_Vertical, m_Matiere);
    end;

  CalculerQuantitatifEntretoise;

    if HasCoudeParentMore45 and not(IsTrainasseT2A) then
      TVentil_Caisson(Caisson).Trappes[Diam2Indice(Diametre)] := TVentil_Caisson(Caisson).Trappes[Diam2Indice(Diametre)] + 1;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_CoudeDessin.CalculeDiametreDebitCollectif(_Iteratif : Boolean);
Begin
  Inherited;
  If Supports(LienCad, IAero_TSoucheEtageCoude) Then
        Begin
        PassageDiametreUniqueColonne;
        PassageDiametreUniqueSurBifurRect;
        End;

if not(etude.Batiment.IsFabricantVIM) then
begin
If VersSortieToit And (Fils.Count > 0) Then
Diametre :=  Fils[0].Diametre;
end;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_CoudeDessin.Paste(_Source: TVentil_Objet);
Begin
  Inherited;
  FEntretoise_L := TVentil_Accident_CoudeDessin(_Source).FEntretoise_L;
  FContreParoi := TVentil_Accident_CoudeDessin(_Source).FContreParoi;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_CoudeDessin.CalculeDiametreDebitTertiaire(_Iteratif : Boolean);
Begin
  Inherited;
  If Supports(LienCad, IAero_TSoucheEtageCoude) Then
        Begin
        PassageDiametreUniqueColonne;
        PassageDiametreUniqueSurBifurRect;
        End;
if not(etude.Batiment.IsFabricantVIM) then
begin
If VersSortieToit And (Fils.Count > 0) Then
Diametre :=  Fils[0].Diametre;
end;
End;
{ ************************************************************************************************************************************************** }
function TVentil_Accident_CoudeDessin.GetEffetSystemeCoudesSuccessifs(_AutreCoude : TVentil_Accident_CoudeDessin; _Gaine : TVentil_TronconDessin) : Single;
Var
  tempAngle : Single;
  DiametreMm : Single;
begin
  Result := -1;
  DiametreMm := Diametre / 1000;
  if _Gaine.LongGaine <= 3 * DiametreMm then
    begin
    tempAngle := (LienCad As IAero_Coude).IAero_Coude_AngleTo((_AutreCoude.LienCad As IAero_Coude));
      //en S
      if RoundTo(tempAngle, -2) = RoundTo(Pi, -2) then
        Result := 0.4
      else
      // en U
      if RoundTo(tempAngle, -2) = RoundTo(0, -2) then
        Result := 0.2;
    end;
end;
{ ************************************************************************************************************************************************** }
function TVentil_Accident_CoudeDessin.GetEffetSystemeApresVentilateur(_SortieCaisson : TVentil_TronconDessin_SortieCaisson; _Gaine : TVentil_TronconDessin) : Single;
Var
  IsSortieVertical : Boolean;
  DiametreMm : Single;
begin
  Result := -1;
  DiametreMm := Diametre / 1000;
    if _SortieCaisson.TypeSortie in [c_CaissonSortie_Aspiration, c_CaissonSortie_Insufflation] then
      begin
      IsSortieVertical := _SortieCaisson.GetNumeroSortieVIM in [3, 4];
        if (_Gaine.LongGaine < 0.3 * DiametreMm) then
          begin
            if IsSortieVertical then
              Result := 1.2
            else
              Result := 1;
          end
        else
        if (_Gaine.LongGaine < 0.62 * DiametreMm) then
          begin
            if IsSortieVertical then
              Result := 1
            else
              Result := 0.8;
          end
        else
        if (_Gaine.LongGaine < 1.25 * DiametreMm) then
          begin
            if IsSortieVertical then
              Result := 0.7
            else
              Result := 0.6;
          end
        else
        if (_Gaine.LongGaine < 2.5 * DiametreMm) then
          begin
            if IsSortieVertical then
              Result := 0.35
            else
              Result := 0.3;
          end
      end
    else
    if _SortieCaisson.TypeSortie = c_CaissonSortie_Insufflation then
      begin
        if AngleCoude = c_Coude_90 then
          begin
            if (_Gaine.LongGaine < 2 * DiametreMm) then
              begin
              Result := 1.2;
              end
            else
            if (_Gaine.LongGaine < 5 * DiametreMm) then
              begin
              Result := 0.7;
              end
            else
              begin
              Result := 0.35;
              end;
          end;
      end;
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_CoudeDessin.CalculeDzetaMini: Real;
var
  tempEffetSysteme : Single;
Begin
  if ObjetTerminalAval <> Nil then
    TronconCalculPDCSinguliere := ObjetTerminalAval
  else
    TronconCalculPDCSinguliere := Self;
  if Etude.Batiment.IsFabricantACT then
    begin
      if IsTrainasseT2A then
        DzetaMini := c_Coude__Tableau5_Dzeta_ActhysT2A[AngleCoude]
      else
        DzetaMini := c_Coude__Tableau5_Dzeta_CoudeArrondi[AngleCoude];
    end
  else
    DzetaMini := c_Coude__Tableau5_Dzeta_CoudeMoyen[AngleCoude];
  Result := Inherited CalculeDzetaMini;

  IsEffetSysteme := False;
  if Etude.DTU2014 then
  begin
  if (parent <> nil) and (parent.parent <> nil) and parent.parent.inheritsfrom(TVentil_TronconDessin_SortieCaisson) then
    begin
    tempEffetSysteme := GetEffetSystemeApresVentilateur(TVentil_TronconDessin_SortieCaisson(parent.parent), TVentil_TronconDessin(parent));
      if tempEffetSysteme > 0 then
        begin
        DzetaMini := DzetaMini + tempEffetSysteme;
        Result := Result + tempEffetSysteme;
        IsEffetSysteme := True;
        end;
    end
  else
  if (parent <> nil) and (parent.parent <> nil) and parent.parent.inheritsfrom(TVentil_Accident_CoudeDessin) and parent.inheritsfrom(TVentil_TronconDessin) then
    begin
    tempEffetSysteme := GetEffetSystemeCoudesSuccessifs(TVentil_Accident_CoudeDessin(parent.parent), TVentil_TronconDessin(parent));
      if tempEffetSysteme > 0 then
        begin
        DzetaMini := tempEffetSysteme;
        Result := tempEffetSysteme;
        IsEffetSysteme := True;
        end;
    end
  else
  if (Fils.Count = 1) and Fils[0].inheritsfrom(TVentil_TronconDessin) and (Fils[0].Fils.Count = 1) and Fils[0].Fils[0].inheritsfrom(TVentil_Accident_CoudeDessin) then
    begin
    tempEffetSysteme := GetEffetSystemeCoudesSuccessifs(TVentil_Accident_CoudeDessin(Fils[0].Fils[0]), TVentil_TronconDessin(Fils[0]));
      if tempEffetSysteme > 0 then
        begin
        DzetaMini := 0;
        Result := 0;
        IsEffetSysteme := True;
        end;
    end
  end;

End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_CoudeDessin.CalculeDzetaMaxi: Real;
var
  tempEffetSysteme : Single;
Begin
  if ObjetTerminalAval <> Nil then
    TronconCalculPDCSinguliere := ObjetTerminalAval
  else
    TronconCalculPDCSinguliere := Self;
  if Etude.Batiment.IsFabricantACT then
    begin
      if IsTrainasseT2A then
        DzetaMaxi := c_Coude__Tableau5_Dzeta_ActhysT2A[AngleCoude]
      else
        DzetaMaxi := c_Coude__Tableau5_Dzeta_CoudeArrondi[AngleCoude];
    end
  else
    DzetaMaxi := c_Coude__Tableau5_Dzeta_CoudeMoyen[AngleCoude];
  Result := Inherited CalculeDzetaMaxi;

  IsEffetSysteme := False;

  IsEffetSysteme := False;
  if Etude.DTU2014 then
  begin
  if (parent <> nil) and (parent.parent <> nil) and parent.parent.inheritsfrom(TVentil_TronconDessin_SortieCaisson) then
    begin
    tempEffetSysteme := GetEffetSystemeApresVentilateur(TVentil_TronconDessin_SortieCaisson(parent.parent), TVentil_TronconDessin(parent));
      if tempEffetSysteme > 0 then
        begin
        DzetaMaxi := DzetaMaxi + tempEffetSysteme;
        Result := Result + tempEffetSysteme;
        IsEffetSysteme := True;
        end;
    end
  else
  if (parent <> nil) and (parent.parent <> nil) and parent.parent.inheritsfrom(TVentil_Accident_CoudeDessin) and parent.inheritsfrom(TVentil_TronconDessin) then
    begin
    tempEffetSysteme := GetEffetSystemeCoudesSuccessifs(TVentil_Accident_CoudeDessin(parent.parent), TVentil_TronconDessin(parent));
      if tempEffetSysteme > 0 then
        begin
        DzetaMaxi := tempEffetSysteme;
        Result := tempEffetSysteme;
        IsEffetSysteme := True;
        end;
    end
  else
  if (Fils.Count = 1) and Fils[0].inheritsfrom(TVentil_TronconDessin) and (Fils[0].Fils.Count = 1) and Fils[0].Fils[0].inheritsfrom(TVentil_Accident_CoudeDessin) then
    begin
    tempEffetSysteme := GetEffetSystemeCoudesSuccessifs(TVentil_Accident_CoudeDessin(Fils[0].Fils[0]), TVentil_TronconDessin(Fils[0]));
      if tempEffetSysteme > 0 then
        begin
        DzetaMaxi := 0;
        Result := 0;
        IsEffetSysteme := True;
        end;
    end
  end;

End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_CoudeDessin.HasCoudeParentMore45 : boolean;
begin

  Result := False;
  if LongGaine >= 7.5 then
    Exit;

  if AngleCoude in [c_Coude_90, c_Coude_75, c_Coude_60] then
    if (parent <> nil) and (parent.parent <> nil) and parent.parent.inheritsfrom(TVentil_Accident_CoudeDessin) and parent.inheritsfrom(TVentil_TronconDessin) then
      if TVentil_Accident_CoudeDessin(parent.parent).AngleCoude in [c_Coude_90, c_Coude_75, c_Coude_60] then
        Result := True;

end;
{ ******************************************************************* \ TVentil_Accident_CoudeDessin ****************************************************** }

{ ******************************************************************* TVentil_Accident_ClapetCoupeFeu ************************************************ }
Constructor TVentil_Accident_ClapetCoupeFeu.Create;
Begin
  inherited;
  NbQuestions := NbQuestions + c_NbQ_ClapetCoupeFeu;
  GammePareFlamme :=  nil;
  DzetaMini := 1;
  DzetaMaxi := 1;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_ClapetCoupeFeu.SelectionGammeClapetCF: String;
Var
  m_CodeGamme : String;
Begin
  If IsNotNil(GammePareFlamme) Then m_CodeGamme := GammePareFlamme.CodeGamme
                               Else m_CodeGamme := 'CLAPCPFVIM02';
  m_CodeGamme := SelectionDuneGamme(m_CodeGamme);// ChoixGammeDansClasse(Copy(m_CodeGamme, 1, 7), m_CodeGamme);
  If m_CodeGamme <> '' Then Begin
    GammePareFlamme :=  TEdibatec_Gamme(BaseEdibatec.Gamme(m_CodeGamme));
    Result := GammePareFlamme.Reference;
    SelectionneDiametre_SelonMethode;
  End Else Result := ' *** ';
End;
{ ************************************************************************************************************************************************** }
function TVentil_Accident_ClapetCoupeFeu.Avertissement : Boolean;
begin
    if GammePareFlamme = Nil then
      Result := True
    else
    if GetCoupeFeu = Nil then
      Result := True
    else
      Result := False;
end;
{ ************************************************************************************************************************************************** }
function TVentil_Accident_ClapetCoupeFeu.GetMessageAvert : String;
begin

  if Avertissement then
    begin
      if GammePareFlamme = Nil then
        Result := 'Aucune gamme s�lectionn�e'
      else
      if GetCoupeFeu = Nil then
        Result := 'Diam�tre ' + GetChrDiam + IntToStr(Diametre) + ' inexistant pour la gamme'
      else
        Result := '';
    end
  else
    Result := '';
end;
{ ************************************************************************************************************************************************** }
function TVentil_Accident_ClapetCoupeFeu.GetCoupeFeu :TEdibatec_CoupeFeu;
Var
  m_NoProd : Integer;
  m_CoupeFeu : TEdibatec_CoupeFeu;
begin

  Result := Nil;

  If GammePareFlamme <> nil Then
    Begin
      For m_NoProd := 0 To GammePareFlamme.Produits.Count - 1 Do
        Begin
        m_CoupeFeu := TEdibatec_CoupeFeu(GammePareFlamme.Produits.Produit(m_NoProd));
          If (m_CoupeFeu.Diametre = Diametre) Then
            begin
            Result := m_CoupeFeu;
            Exit;
            end;
        End;
    End;

end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_ClapetCoupeFeu.CalculerQuantitatifInternal;
Var
  m_CoupeFeu : TEdibatec_CoupeFeu;
  m_NoProd   : Integer;
  m_Erreur   : Boolean;
Begin
  Inherited;
if etude.Batiment.IsFabricantVIM then
begin
  Inc(TVentil_Caisson(Caisson).NbBouches);
    m_Erreur := True;

  m_CoupeFeu := GetCoupeFeu;
    if m_CoupeFeu <> nil then
      begin
        If SurTerrasse Then
          AddChiffrage(m_CoupeFeu, 1, cst_Horizontal, cst_Chiff_AC)
        Else
          AddChiffrage(m_CoupeFeu, 1, cst_Vertical, cst_Chiff_AC);
      m_Erreur := False;
      end;

    If m_Erreur Then
        Begin
                If GammePareFlamme <> nil then
                        AddChiffrageErreur('Clapet coupe-feu ' + GammePareFlamme.Reference, IntToStr(Diametre) + ' mm '{$IfDeF VIM_OPTAIR}, inttostr(1) + ' unit�'{$EndIf})
                Else
                        AddChiffrageErreur('Clapet coupe-feu ***', IntToStr(Diametre) + ' mm '{$IfDeF VIM_OPTAIR}, inttostr(1) + ' unit�'{$EndIf});
        End;
end;
End;
{ ************************************************************************************************************************************************** }
Function  TVentil_Accident_ClapetCoupeFeu.CalculeDzetaMini: Real;
Begin
  if ObjetTerminalAval <> Nil then
    TronconCalculPDCSinguliere := ObjetTerminalAval
  else
    TronconCalculPDCSinguliere := Self;
Result := DzetaMini;
End;
{ ************************************************************************************************************************************************** }
Function  TVentil_Accident_ClapetCoupeFeu.CalculeDzetaMaxi: Real;
Begin
  if ObjetTerminalAval <> Nil then
    TronconCalculPDCSinguliere := ObjetTerminalAval
  else
    TronconCalculPDCSinguliere := Self;
Result := DzetaMaxi;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_ClapetCoupeFeu.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row: Integer);
Begin
  If isNotNil(Etude.GridProp.Objects[0, 0]) And (Etude.GridProp.Objects[0, 0] <> Self) Then Begin
    _Grid.Cells[_ColDep, _Row] := 'Accident dessin�';
  End;
  inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_ClapetCoupeFeu.Affiche(_Grid: TAdvStringGrid);
Var
  m_Row      : Integer;
  m_Question : Integer;
  m_Str      : String;
  m_No       : Integer;
Begin
  _Grid.HideInplaceEdit;
  _Grid.HideSelection;
  _Grid.Combobox.Hide;
  _Grid.Objects[0, 0] := Self;
  For m_No := 1 To _Grid.RowCount - 1 Do _Grid.Cells[1, m_No] := '';
  _Grid.RowCount := NbQuestionsAff + 1;
  m_Row := GetNbQuestionAfficheTroncon + 1;
  For m_Question := Low(c_LibellesQ_ClapetCoupeFeu) To High(c_LibellesQ_ClapetCoupeFeu) Do If AfficheQuestion(m_Question) Then Begin
    _Grid.Cells[0, m_Row] := c_LibellesQ_ClapetCoupeFeu[m_Question];
    Case m_Question Of
      c_QClapetCoupeFeu_Gamme : If GammePareFlamme <> nil Then m_Str := GammePareFlamme.Reference
                                                            Else m_Str := ' *** ';
      c_QClapetCoupeFeu_Dzeta : m_Str := Float2Str(DzetaMaxi, 2);
      c_QClapetCoupeFeu_Avert :
                                Begin
                                  m_Str := GetMessageAvert;
                                  _Grid.CellProperties[0, m_Row].FontColor := clRed;
                                  _Grid.CellProperties[1, m_Row].FontColor := clRed;
                                End
      Else m_Str := '';
    End;
    _Grid.Cells[1, m_Row] := m_Str;
    Inc(m_Row);
  End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_ClapetCoupeFeu.AfficheQuestion(_NoQuestion: Integer): Boolean;
Begin
    Case _NoQuestion Of
    c_QClapetCoupeFeu_Gamme  : {$IfNDef AERAU_CLIMAWIN}
                                if etude.Batiment.IsFabricantVIM then
                                        result := True
                                else
                                {$EndIf}
                                result := False;
    c_QClapetCoupeFeu_Dzeta  : Result := True;
    c_QClapetCoupeFeu_Avert : {$IfNDef AERAU_CLIMAWIN}
                                if etude.Batiment.IsFabricantVIM then
                                  result := Avertissement
                                else
                                {$EndIf}
                                result := False;
    Else Result := False;
  End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_ClapetCoupeFeu.EllipsClick(_Grid: TAdvStringGrid; _Ligne: Integer): String;
Begin
  Case NoQuestion(_Ligne) Of
    c_QClapetCoupeFeu_Gamme : Result := SelectionGammeClapetCF;
    End;
  Recupere(_Grid, _Ligne);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_ClapetCoupeFeu.Recupere(_Grid: TAdvStringGrid; _Ligne: Integer);
Begin
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Inherited
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QClapetCoupeFeu_Gamme  : ;//r�cup�ration dans le EllipsClick
    c_QClapetCoupeFeu_Dzeta  : if etude.Batiment.IsFabricantVIM then
                                        begin
                                                If Not InheritsFrom(TVentil_Accident_ClapetCoupeFeu) then
                                                        DzetaMaxi := _Grid.Floats[1, _Ligne];
                                        end
                                else
                                        DzetaMaxi := _Grid.Floats[1, _Ligne];
  End;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_ClapetCoupeFeu.TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType;
Begin
  Result := edNone;
  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
  //Parfois le GoEditing saute sans raison...
  _Grid.Options := _Grid.Options + [goEditing];
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Result := Inherited TypeEdition(_Grid, _Ligne)
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QClapetCoupeFeu_Gamme  : Result := edEditBtn;
    c_QClapetCoupeFeu_Dzeta  : Result := edFloat;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_ClapetCoupeFeu.PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer);
Begin
  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Inherited
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then
  begin
  {
    Case NoQuestion(_Ligne) Of
    End;
  }
  end;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_ClapetCoupeFeu.Load;
Var
  m_Str : String;
Begin

  m_Str := Table_Etude_ClapetCoupeFeu.Donnees.FieldByName('GAMME_CF').AsWideString;
  DzetaMaxi := Table_Etude_ClapetCoupeFeu.Donnees.FieldByName('DZETA').AsFloat;
  If m_Str <> '' Then GammePareFlamme := TEdibatec_Gamme(BaseEdibatec.Gamme(m_Str))
                 Else GammePareFlamme := nil;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_ClapetCoupeFeu.Save;
Begin
  Table_Etude_ClapetCoupeFeu.Donnees.Insert;
  Table_Etude_ClapetCoupeFeu.Donnees.FieldByName('ID_CLAPETCOUPEFEU').AsWideString := GUIDToString(Id);
  {$IfDef AERAU_CLIMAWIN}
  Table_Etude_ClapetCoupeFeu.Donnees.FieldByName('IDRESEAU').AsWideString := Etude.IdReseau;
  {$EndIf}
  Table_Etude_ClapetCoupeFeu.Donnees.FieldByName('DZETA').AsFloat := DzetaMaxi;  
  If GammePareFlamme <> nil Then Table_Etude_ClapetCoupeFeu.Donnees.FieldByName('GAMME_CF').AsWideString := GammePareFlamme.CodeGamme
                            Else Table_Etude_ClapetCoupeFeu.Donnees.FieldByName('GAMME_CF').AsWideString := '';
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_ClapetCoupeFeu.SelectionneDiametre_SelonMethode;
Var
  m_NoProd : Integer;
  m_CoupeFeu : TEdibatec_CoupeFeu;
Begin
Inherited;

{$IfNDef AERAU_CLIMAWIN}
if etude.Batiment.IsFabricantVIM then
begin

  If GammePareFlamme <> nil Then Begin
    For m_NoProd := 0 To GammePareFlamme.Produits.Count - 1 Do Begin
      m_CoupeFeu := TEdibatec_CoupeFeu(GammePareFlamme.Produits.Produit(m_NoProd));
      If (m_CoupeFeu.Diametre = Diametre) Then Begin // Diametre OK
      DzetaMaxi := m_CoupeFeu.Dzeta;
      End;
    End;
  End;

end;
{$EndIf}
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_ClapetCoupeFeu.ManageEtiquetteVisible;
begin

FEtiquette := '';

    if Etude.Batiment.IsFabricantVIM then
      begin
            if LienCad <> nil then
              LienCad.IAero_Vilo_Base_SetCouleurEtiquette(Avertissement, clWebRed);
        if Avertissement then
          FEtiquette := GetMessageAvert
        else
          begin
            if IsNotNil(GammePareFlamme) then
              FEtiquette := GammePareFlamme.Reference
            else
              FEtiquette := '';

            if FEtiquette <> '' then
              FEtiquette := FEtiquette + #13;

          FEtiquette := FEtiquette + GetChrDiam + IntToStr(Diametre);
          end;
      end;

  if FEtiquette <> '' then
    if LienCad <> nil then
      LienCad.IAero_Vilo_Base_SetTextEtiquette(FEtiquette, Options.TypeEtiquette);
  Inherited;
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_ClapetCoupeFeu.IsEtiquetteVisible : boolean;
begin
  Result := Options.AffEtiq_ClapetCoupeFeu;
end;
{ ******************************************************************* \ TVentil_Accident_ClapetCoupeFeu ************************************************ }

{ ******************************************************************* TVentil_Accident_ChangementDeDiametre ************************************************ }
Procedure TVentil_Accident_ChangementDeDiametre.Affiche(_Grid: TAdvStringGrid);
Var
  m_Row      : Integer;
  m_Question : Integer;
  m_Str      : String;
Begin
  If Automatique Then Exit;
  Inherited;
  m_Row := GetNbQuestionAfficheTroncon + 1;
  For m_Question := Low(c_LibellesQ_ChgtDiametre) To High(c_LibellesQ_ChgtDiametre) Do If AfficheQuestion(m_Question) Then Begin
    _Grid.Cells[0, m_Row] := c_LibellesQ_ChgtDiametre[m_Question];
    Case m_Question Of
      c_QChgDiametre_Forme  : m_Str := c_FormesChgtDiametre[TypeChg_Diametre];
      Else m_Str := '';
    End;
    _Grid.Cells[1, m_Row] := m_Str;
    Inc(m_Row);
  End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_ChangementDeDiametre.AfficheQuestion(_NoQuestion: Integer): Boolean;
Begin

  if (LienCad As IAero_Accident_ChangementDiametre).IAero_Accident_ChangementDiametre_AutoCree then
        Begin
        Result := False;
        exit;
        End;



  If _NoQuestion <= c_NbQ_Troncon Then Result := Inherited AfficheQuestion(_NoQuestion)
  Else Case _NoQuestion Of
    c_QChgDiametre_Forme  : Result := True;
    Else Result := False;
  End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_ChangementDeDiametre.HasComboBox(_Ligne: Integer): Boolean;
Begin
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Result := Inherited HasComboBox(_Ligne)
  Else Result := AfficheQuestion(NoQuestion(_Ligne)) And (NoQuestion(_Ligne) In[c_QChgDiametre_Forme]);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_ChangementDeDiametre.Recupere(_Grid: TAdvStringGrid; _Ligne: Integer);
Begin
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Inherited
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QChgDiametre_Forme  : TypeChg_Diametre := _Grid.Combobox.ItemIndex;
  End;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_ChangementDeDiametre.TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType;
Var
  m_NoChoix : Integer;
Begin
  Result := edNone;
  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
  //Parfois le GoEditing saute sans raison...
  _Grid.Options := _Grid.Options + [goEditing];
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Result := Inherited TypeEdition(_Grid, _Ligne)
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QChgDiametre_Forme  : Begin
      Result := edComboList;
    End;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_ChangementDeDiametre.PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer);
Var
  m_NoChoix : Integer;
Begin
  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Inherited
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QChgDiametre_Forme  : Begin
      _Grid.ClearComboString;
      For m_NoChoix := Low(c_FormesChgtDiametre) To High(c_FormesChgtDiametre) Do _Grid.AddComboString(c_FormesChgtDiametre[m_NoChoix]);
    End;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_ChangementDeDiametre.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
Begin
  If isNotNil(Etude.GridProp.Objects[0, 0]) And (Etude.GridProp.Objects[0, 0] <> Self) Then Begin
    _Grid.Cells[_ColDep, _Row] := 'Changement de diam�tre ' +  c_FormesChgtDiametre[TypeChg_Diametre];
  End;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_ChangementDeDiametre.CalculerQuantitatifInternal;
Var
  m_Classe : TEdibatec_Classe;
  m_Gamme  : TEdibatec_Gamme;
  m_Produit: TEdibatec_Reduction;
  m_NoGam  : Integer;
  m_NoProd : Integer;
  m_Erreur : Boolean;
  ParentDiametre : Integer;
  ConditionMatiere : Boolean;
Begin
  Inherited;
  If Automatique Then Exit;


  ParentDiametre := ParentCalcul.Diametre;
        If Parentcalcul.InheritsFrom(TVentil_Caisson) and (TVentil_Caisson(Parentcalcul).ProduitAssocie <> nil) then
                Begin
                if VersSortieToit then
                        ParentDiametre := TVentil_Caisson(Parentcalcul).ProduitAssocie.DiametreRejet
                else
                        ParentDiametre := TVentil_Caisson(Parentcalcul).ProduitAssocie.DiametreAspiration;
                End;
//if etude.Batiment.IsFabricantVIM then
begin
  if Diametre = ParentDiametre then
          if (fils.Count > 0) and (fils[0].Diametre <> Diametre) then
                ParentDiametre := fils[0].Diametre;
  if Diametre = ParentDiametre then
        exit;
end;
  m_Classe := BaseEdibatec.Classe(c_Edibatec_Reductions);
  m_Erreur := True;
  For m_NoGam := 0 To m_Classe.Gammes.Count - 1 Do Begin
    if m_Erreur = false then
      break;
    m_Gamme := m_Classe.Gammes.Gamme(m_NoGam);
    For m_NoProd := 0 To m_Gamme.Produits.Count - 1 Do Begin
      m_Produit := TEdibatec_Reduction(m_Gamme.Produits.Produit(m_NoProd));
//if etude.Batiment.IsFabricantVIM then
begin
      ConditionMatiere := ( ( TronconGaz And (m_Produit.Matiere = c_Alu) ) Or ( (Not TronconGaz) And (m_Produit.Matiere = GetMatiereAccessoire) ) );
{
end
else
begin
      ConditionMatiere := ( ( TronconGaz And (m_Produit.Matiere = c_Alu) ) Or ( (Not TronconGaz) And (m_Produit.Matiere <> c_Alu) ) );
}      
end;

{$IfDeF VIM_OPTAIR}
if (parent <> nil) and (parent.InheritsFrom(TVentil_TronconDessin_SortieCaisson)) then
        TypeChg_Diametre := c_ChgtDiametreBruque;
{$EndIf}

      if ConditionMatiere And
         (m_Produit.GrandDiametre = Max(ParentDiametre, Diametre)) And // Diametre Grand OK
         (m_Produit.PetitDiametre = Min(Diametre, ParentDiametre)) And // Diametre petit OK
         (m_Produit.TypeReduction = TypeChg_Diametre + 1) Then Begin // Type chgt diam OK
         If etude.Batiment.IsFabricantVIM and VersSortieToit Then AddChiffrage(m_Produit, 1, cst_Rejet, cst_Chiff_RE)
         Else
         If SurTerrasse Then AddChiffrage(m_Produit, 1, Cst_Horizontal, cst_Chiff_AC)
         Else
         AddChiffrage(m_Produit, 1, Cst_Vertical, cst_Chiff_AC);
         m_Erreur := False;
         Break;
      End;
    End;
  End;
  If m_Erreur Then Begin
    If TronconGaz Then AddChiffrageErreur('Changement de diam�tre', IntToStr(Diametre) + ' mm \ ' + IntToStr(ParentDiametre) + ' Alu'{$IfDeF VIM_OPTAIR}, inttostr(1) + ' unit�'{$EndIf})
                  Else AddChiffrageErreur('Changement de diam�tre', IntToStr(Diametre) + ' mm \ ' + IntToStr(ParentDiametre) + ' ' + c_Matiere[GetMatiereAccessoire]{$IfDeF VIM_OPTAIR}, inttostr(1) + ' unit�'{$EndIf});
  End;


    if HasChgtDiamParent and not(IsTrainasseT2A) then
      TVentil_Caisson(Caisson).Trappes[Diam2Indice(Diametre)] := TVentil_Caisson(Caisson).Trappes[Diam2Indice(Diametre)] + 1;

End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_ChangementDeDiametre.InitEtiquette;
Begin
  If IsNotNil(Parentcalcul) Then FEtiquette := GetChrDiam + IntToStr(Diametre) + ' -> ' + IntToStr(Parentcalcul.Diametre)
                            Else FEtiquette := '';
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_ChangementDeDiametre.ImageAssociee: String;
Begin
  {$IfDef ACTHYS_DIMVMBP}
  Result := 'REDUCTION.jpg';
  {$Else}
  Result := '';
  {$EndIf}
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_ChangementDeDiametre.HasChgtDiamParent : boolean;
begin

  Result := False;

  if LongGaine >= 7.5 then
    Exit;

  if (parent <> nil) and (parent.parent <> nil) and parent.parent.inheritsfrom(TVentil_Accident_ChangementDeDiametre) and parent.inheritsfrom(TVentil_TronconDessin) then
    Result := True;

end;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_ChangementDeDiametre.CalculeDzetaElargissementBrusqueDT2014(D1, D2 : Real) : Real;
begin
//cf Annexe A.3.2.1 du DTU 68.3 P1-1-1
  Result := Sqr(1 - Sqr(D1 / D2));
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_ChangementDeDiametre.CalculeDzetaRetrecissementBrusqueDT2014(D1, D2 : Real) : Real;
Var
  A1 : Real;
  A2 : Real;
begin
//cf Annexe A.3.2.1 du DTU 68.3 P1-1-1
  A1 := Pi * Sqr(D1 / 2);
  A2 := Pi * Sqr(D2 / 2);
  Result := 0.5 * Sqr(1 - Sqr(A2 / A1));
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_ChangementDeDiametre.CalculeDzetaElargissementConiqueDT2014(D1, D2 : Real) : Real;
begin
//cf Annexe A.3.2.1 du DTU 68.3 P1-1-1
  Result := 1.12 * Sqr(1 - Sqr(D1 / D2));
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_ChangementDeDiametre.CalculeDzetaRetrecissementConiqueDT2014(D1, D2 : Real) : Real;
Var
  Facteur : Real;
begin
//cf Annexe A.3.2.1 du DTU 68.3 P1-1-1
  if   Angle in [c_Coude_90, c_Coude_75, c_Coude_60] then
    Result := CalculeDzetaRetrecissementBrusqueDT2014(D1, D2)
  else
    begin
    Facteur := Sqr(D2 / D1);
      if Facteur <= 0.2 then
        Result := 0.08
      else
      if Facteur <= 0.4 then
        Result := 0.08
      else
      if Facteur < 0.6 then
        Result := CalculeValeurIntermedaireTableau(0.4, 0.08, 0.6, 0.06, Facteur)
      else
      if Facteur = 0.6 then
        Result := 0.06
      else
      if Facteur < 0.8 then
        Result := CalculeValeurIntermedaireTableau(0.6, 0.06, 0.8, 0.02, Facteur)
      else
      if Facteur = 0.8 then
        Result :=  0.02
      else
        Result := 0.02;
    end;
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_ChangementDeDiametre.CalculeDzetaMini: Real;
Var
  D1 : Real;
  D2 : Real;
Begin
  D2 := Parentcalcul.Diametre;
        if (Fils <> Nil) And (FilsCalcul.Count > 0) then
                D1 := FilsCalcul[0].Diametre
        else
                D1 := Diametre;
  if D1 >= D2 then
    TronconCalculPDCSinguliere := ParentCalcul
  else
    begin
      if ObjetTerminalAval <> Nil then
        TronconCalculPDCSinguliere := ObjetTerminalAval
      else
        TronconCalculPDCSinguliere := Self;
    end;

    if Etude.DTU2014 then
      begin
        Case TypeChg_Diametre of
          c_ChgtDiametreBruque  :
                                  if D1 >= D2 then
                                    DzetaMini := CalculeDzetaRetrecissementBrusqueDT2014(D1, D2)
                                  else
                                    DzetaMini := CalculeDzetaElargissementBrusqueDT2014(D1, D2);
          c_ChgtDiametreConique :
                                  if D1 >= D2 then
                                    DzetaMini := CalculeDzetaRetrecissementConiqueDT2014(D1, D2)
                                  else
                                    DzetaMini := CalculeDzetaElargissementConiqueDT2014(D1, D2);
        End;
      end
    else
      begin
        If Angle < c_Coude_45 Then DzetaMini := 1.12 * Sqr(1 - Sqr(D1 / D2)) // Conique
          Else DzetaMini := Sqr(1 - Sqr(D1 / D2));
      end;
  Result := Inherited CalculeDzetaMini;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_ChangementDeDiametre.CalculeDzetaMaxi: Real;
Var
  D1 : Real;
  D2 : Real;
Begin
  D2 := Parentcalcul.Diametre;
        if (Fils <> Nil) And (FilsCalcul.Count > 0) then
                D1 := FilsCalcul[0].Diametre
        else
                D1 := Diametre;

  if D1 >= D2 then
    TronconCalculPDCSinguliere := ParentCalcul
  else
    begin
      if ObjetTerminalAval <> Nil then
        TronconCalculPDCSinguliere := ObjetTerminalAval
      else
        TronconCalculPDCSinguliere := Self;
    end;

    if Etude.DTU2014 then
      begin
        Case TypeChg_Diametre of
          c_ChgtDiametreBruque  :
                                  if D1 >= D2 then
                                    DzetaMaxi := CalculeDzetaRetrecissementBrusqueDT2014(D1, D2)
                                  else
                                    DzetaMaxi := CalculeDzetaElargissementBrusqueDT2014(D1, D2);
          c_ChgtDiametreConique :
                                  if D1 >= D2 then
                                    DzetaMaxi := CalculeDzetaRetrecissementConiqueDT2014(D1, D2)
                                  else
                                    DzetaMaxi := CalculeDzetaElargissementConiqueDT2014(D1, D2);
        End;
      end
    else
      begin
        If Angle < c_Coude_45 Then DzetaMaxi := 1.12 * Sqr(1 - Sqr(D1 / D2)) // Conique
          Else DzetaMaxi := Sqr(1 - Sqr(D1 / D2));
      end;
  Result := Inherited CalculeDzetaMaxi;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_ChangementDeDiametre.Load;
Begin
  TypeChg_Diametre := Table_Etude_ChgtDiam.Donnees.FieldByName('FORME').AsInteger;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_ChangementDeDiametre.Save;
Begin
  Table_Etude_ChgtDiam.Donnees.Insert;
  Table_Etude_ChgtDiam.Donnees.FieldByName('ID_CHGTDIAMETRE').AsWideString := GUIDToString(Id);
  {$IfDef AERAU_CLIMAWIN}
  Table_Etude_ChgtDiam.Donnees.FieldByName('IDRESEAU').AsWideString := Etude.IdReseau;
  {$EndIf}
  Table_Etude_ChgtDiam.Donnees.FieldByName('FORME').AsInteger := TypeChg_Diametre;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_ChangementDeDiametre.Paste(_Source: TVentil_Objet);
Begin
  inherited;
  TypeChg_Diametre := TVentil_Accident_ChangementDeDiametre(_Source).TypeChg_Diametre;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_ChangementDeDiametre.IAero_Fredo_Base_RemplacerPar(Objet : IAero_Fredo_Base);
Begin
inherited;
TVentil_Accident_ChangementDeDiametre(Objet.IAero_Fredo_Base_Objet).TypeChg_Diametre := TypeChg_Diametre;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_ChangementDeDiametre.SetForme(_Forme: Integer);
Begin
  FTypeChg_Diametre := _Forme;
  If TypeChg_Diametre = c_ChgtDiametreBruque Then Angle := c_Coude_90
                                  Else Angle := c_Coude_45;
End;
{ ************************************************************************************************************************************************** }
Function  TVentil_Accident_ChangementDeDiametre.GetForme: Integer;
Begin
  Result := FTypeChg_Diametre;
End;
{ ************************************************************************************************************************************************** }
Constructor TVentil_Accident_ChangementDeDiametre.Create;
Begin
  inherited;
  NbQuestions := NbQuestions + c_NbQ_ChgtDiametre;
  TypeChg_Diametre       := c_ChgtDiametreConique;
  Automatique := False;
End;
{ ************************************************************************************************************************************************** }
class Function TVentil_Accident_ChangementDeDiametre.NomParDefaut : String;
Begin
Result := 'Changement de diam�tre';
End;
{ ************************************************************************************************************************************************** }
class Function TVentil_Accident_ChangementDeDiametre.FamilleObjet : String;
Begin
Result := 'Changement de diam�tre';
End;
{ ******************************************************************* \ TVentil_Accident_ChangementDeDiametre ********************************************** }


{ ********************************************************************* TVentil_AccidentDevoiement ********************************************************* }
Procedure TVentil_AccidentDevoiement.Affiche(_Grid: TAdvStringGrid);
Begin
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_AccidentDevoiement.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row: Integer);
Begin
  If isNotNil(Etude.GridProp.Objects[0, 0]) And (Etude.GridProp.Objects[0, 0] <> Self) Then Begin
    _Grid.Cells[_ColDep, _Row] := 'D�voiement';
  End;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_AccidentDevoiement.CalculerQuantitatifInternal;
Var
  m_Classe : TEdibatec_Classe;
  m_NoGam  : Integer;
  m_Gamme  : TEdibatec_Gamme;
  m_Produit: TEdibatec_Coude;
  m_NoProd : Integer;
  m_Erreur : Boolean;
  m_Matiere : Integer;
Begin
  Inherited;
if not(etude.Batiment.IsFabricantVIM) then
begin
  m_Matiere := GetMatiereAccessoire;
  m_Gamme := BaseEdibatec.Gamme(BaseEdibatec.Assistant.c_GammesCoudes[m_Matiere]);
  m_Erreur := True;
  For m_NoProd := 0 To m_Gamme.Produits.Count - 1 Do Begin
    m_Produit := TEdibatec_Coude(m_Gamme.Produits.Produit(m_NoProd));
    If (m_Produit.Matiere = m_Matiere) And //  Mati�re OK
       (m_Produit.Diametre = Diametre) And // Diametre OK
       (m_Produit.Angle = c_Coude_90) Then Begin // Angle coude OK
       If SurTerrasse Then AddChiffrage(m_Produit, 2, Cst_Horizontal, cst_Chiff_AC)
       Else
       AddChiffrage(m_Produit, 2, Cst_Vertical, cst_Chiff_AC);
       m_Erreur := False;
       Break;
    End;
  End;
end
else
begin
  m_Classe := BaseEdibatec.Classe(c_Edibatec_Coudes);
  m_Erreur := True;
  m_Matiere := GetMatiereAccessoire;
  For m_NoGam := 0 To m_Classe.Gammes.Count - 1 Do Begin
    m_Gamme := m_Classe.Gammes.Gamme(m_NoGam);
    For m_NoProd := 0 To m_Gamme.Produits.Count - 1 Do Begin
      m_Produit := TEdibatec_Coude(m_Gamme.Produits.Produit(m_NoProd));
      If (m_Produit.Matiere = m_Matiere) And //  Mati�re OK) And //  Mati�re OK
         (m_Produit.Diametre = Diametre) And // Diametre OK
         (m_Produit.Angle = c_Coude_90) Then Begin // Angle coude OK
         If VersSortieToit Then AddChiffrage(m_Produit, 2, cst_Rejet, cst_Chiff_RE)
         Else
         If SurTerrasse Then AddChiffrage(m_Produit, 2, Cst_Horizontal, cst_Chiff_AC)
         Else
         AddChiffrage(m_Produit, 2, Cst_Vertical, cst_Chiff_AC);
         m_Erreur := False;
         Break;
      End;
    End;
  End;
end;
  If m_Erreur Then Begin
    If TronconGaz Then AddChiffrageErreur('Coude', IntToStr(Diametre) + ' mm \ ' + ' 90� Alu'{$IfDeF VIM_OPTAIR}, inttostr(2) + ' unit�(s)'{$EndIf})
                  Else AddChiffrageErreur('Coude', IntToStr(Diametre) + ' mm \ ' + ' 90� Galva'{$IfDeF VIM_OPTAIR}, inttostr(2) + ' unit�(s)'{$EndIf});
  End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_AccidentDevoiement.CalculeDzetaMini: Real;
Begin
  if ObjetTerminalAval <> Nil then
    TronconCalculPDCSinguliere := ObjetTerminalAval
  else
    TronconCalculPDCSinguliere := Self;
  DzetaMini := c_Coude__Tableau5_Dzeta_CoudeArrondi[c_Coude_90] * 2; // 1 devoiement = 2 coudes � 90
  Result := Inherited CalculeDzetaMini;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_AccidentDevoiement.CalculeDzetaMaxi: Real;
Begin
  if ObjetTerminalAval <> Nil then
    TronconCalculPDCSinguliere := ObjetTerminalAval
  else
    TronconCalculPDCSinguliere := Self;
  DzetaMaxi := c_Coude__Tableau5_Dzeta_CoudeArrondi[c_Coude_90] * 2; // 1 devoiement = 2 coudes � 90
  Result := Inherited CalculeDzetaMaxi;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_AccidentDevoiement.InfosFromCad;
Begin
  LongueurDevoiement := (LienCad As IAero_Devoiement).IAero_Devoiement_Longueur;
  Inherited;
End;
{ ******************************************************************* \ TVentil_AccidentDevoiement ********************************************************* }


{ *****************************************************************  TVentil_Accident_TeSoucheCoude ********************************************************* }
Procedure TVentil_Accident_TeSoucheCoude.CalculerQuantitatifInternal;
Var
  m_Classe : TEdibatec_Classe;
  m_NoGam  : Integer;
  m_Gamme  : TEdibatec_Gamme;
  m_Produit: TEdibatec_Coude;
  m_NoProd : Integer;
  m_Erreur : Boolean;
  m_Matiere : Integer;
Begin
exit;
  Inherited;
if not(Etude.Batiment.IsFabricantVIM) then
begin
  m_Matiere := GetMatiereAccessoire;
  m_Gamme := BaseEdibatec.Gamme(BaseEdibatec.Assistant.c_GammesCoudes[m_Matiere]);
  m_Erreur := True;
  For m_NoProd := 0 To m_Gamme.Produits.Count - 1 Do Begin
    m_Produit := TEdibatec_Coude(m_Gamme.Produits.Produit(m_NoProd));
    If (m_Produit.Matiere = m_Matiere) And //  Mati�re OK
       (m_Produit.Diametre = Diametre) And // Diametre OK
       (m_Produit.Angle = c_Coude_90) Then Begin // Angle coude OK
       If SurTerrasse Then AddChiffrage(m_Produit, 1, Cst_Horizontal, cst_Chiff_AC)
       Else
       AddChiffrage(m_Produit, 1, Cst_Vertical, cst_Chiff_AC);
       m_Erreur := False;
       Break;
    End;
  End;
end
else
begin
  m_Classe := BaseEdibatec.Classe(c_Edibatec_Coudes);
  m_Erreur := True;
  For m_NoGam := 0 To m_Classe.Gammes.Count - 1 Do Begin
    m_Gamme := m_Classe.Gammes.Gamme(m_NoGam);
    For m_NoProd := 0 To m_Gamme.Produits.Count - 1 Do Begin
      m_Produit := TEdibatec_Coude(m_Gamme.Produits.Produit(m_NoProd));
      If ( ( TronconGaz And (m_Produit.Matiere = c_Alu) ) Or ( (Not TronconGaz) And (m_Produit.Matiere <> c_Alu) ) ) And //  Mati�re OK
         (m_Produit.Diametre = Diametre) And // Diametre OK
         (m_Produit.Angle = c_Coude_90) Then Begin // Angle coude OK
         If VersSortieToit Then AddChiffrage(m_Produit, 1, cst_Rejet, cst_Chiff_RE)
         Else
         If SurTerrasse Then AddChiffrage(m_Produit, 1, Cst_Horizontal, cst_Chiff_AC)
         Else
         AddChiffrage(m_Produit, 1, Cst_Vertical, cst_Chiff_AC);
         m_Erreur := False;
         Break;
      End;
    End;
  End;
end;
  If m_Erreur Then Begin
    If TronconGaz Then AddChiffrageErreur('Coude', IntToStr(Diametre) + ' mm \ ' + c_Coude_Angles[c_Coude_90] + '� Alu'{$IfDeF VIM_OPTAIR}, inttostr(1) + ' unit�'{$EndIf})
                  Else AddChiffrageErreur('Coude', IntToStr(Diametre) + ' mm \ ' + c_Coude_Angles[c_Coude_90] + '� Galva'{$IfDeF VIM_OPTAIR}, inttostr(1) + ' unit�'{$EndIf})
  End;
End;
{ **************************************************************** \ TVentil_Accident_TeSoucheCoude ********************************************************* }

{ ****************************************************************   TVentil_Accident_Generic *************************************************************** }
Constructor TVentil_Accident_Generic.Create;
Begin
  Inherited;
  DzetaMini := 1;
  DzetaMaxi := 1;  
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_Generic.Affiche(_Grid: TAdvStringGrid);
Var
  m_Row      : Integer;
  m_Question : Integer;
  m_Str      : String;
  m_No       : Integer;
Begin
  _Grid.HideInplaceEdit;
  _Grid.HideSelection;
  _Grid.Combobox.Hide;
  _Grid.Objects[0, 0] := Self;
  For m_No := 1 To _Grid.RowCount - 1 Do _Grid.Cells[1, m_No] := '';
  _Grid.RowCount := NbQuestionsAff + 1;
  m_Row := 1;
  For m_Question := Low(c_LibellesQ_AccGeneric) To High(c_LibellesQ_AccGeneric) Do If AfficheQuestion(m_Question) Then Begin
    _Grid.Cells[0, m_Row] := c_LibellesQ_AccGeneric[m_Question];
    Case m_Question Of
      c_QAccGeneric_Dzeta  : m_Str := Float2Str(DzetaMaxi, 2);
      Else m_Str := '';
    End;
    _Grid.Cells[1, m_Row] := m_Str;
    Inc(m_Row);
  End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_Generic.AfficheQuestion(_NoQuestion: Integer): Boolean;
Begin
  Case _NoQuestion Of
    c_QAccGeneric_Dzeta  : Result := True;
    Else Result := False;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_Generic.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row: Integer);
Begin
  If isNotNil(Etude.GridProp.Objects[0, 0]) And (Etude.GridProp.Objects[0, 0] <> Self) Then Begin
    _Grid.Cells[_ColDep, _Row] := 'Accident dessin�';
  End;
  inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_Generic.CalculeDzetaMini: Real;
Begin
  if ObjetTerminalAval <> Nil then
    TronconCalculPDCSinguliere := ObjetTerminalAval
  else
    TronconCalculPDCSinguliere := Self;
  Result := DzetaMini;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_Generic.CalculeDzetaMaxi: Real;
Begin
  if ObjetTerminalAval <> Nil then
    TronconCalculPDCSinguliere := ObjetTerminalAval
  else
    TronconCalculPDCSinguliere := Self;
  Result := DzetaMaxi;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Accident_Generic.TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType;
Begin
  Result := edNone;
  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
  //Parfois le GoEditing saute sans raison...
  _Grid.Options := _Grid.Options + [goEditing];
  If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QAccGeneric_Dzeta  : Result := edFloat;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_Generic.PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer);
Begin
  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
{
  If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
  End;
}
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_Generic.Recupere(_Grid: TAdvStringGrid; _Ligne: Integer);
Begin
  If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QAccGeneric_Dzeta  : if Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantACT  or Etude.Batiment.IsFabricantMVN then
                                begin
                                        If Not InheritsFrom(TVentil_Accident_ClapetCoupeFeu) then
                                                Begin
                                                DzetaMaxi := _Grid.Floats[1, _Ligne];
                                                DzetaMini := _Grid.Floats[1, _Ligne];
                                                End;
                                end
                           else
                                DzetaMaxi := _Grid.Floats[1, _Ligne];
  End;
  Affiche(_Grid);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_Generic.Save;
Begin
  Table_Etude_AccGeneric.Donnees.Insert;
  Table_Etude_AccGeneric.Donnees.FieldByName('ID_ACCGENERIC').AsWideString := GUIDToString(Id);
  {$IfDef AERAU_CLIMAWIN}
  Table_Etude_AccGeneric.Donnees.FieldByName('IDRESEAU').AsWideString := Etude.IdReseau;
  {$EndIf}
  Table_Etude_AccGeneric.Donnees.FieldByName('DZETA').AsFloat := DzetaMaxi;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Accident_Generic.Load;
Begin
  DzetaMaxi := Table_Etude_AccGeneric.Donnees.FieldByName('DZETA').AsFloat;
  Inherited;
End;
{ ************************************************************************************************************************************************** }

{ **************************************************************** \ TVentil_Accident_Generic *************************************************************** }

{ ******************************************************************* TVentil_Accident_Registre *********************************************************** }
Procedure TVentil_Accident_Registre.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
Begin
  _Grid.Cells[_ColDep, _Row] := 'Registre';
  Inherited;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_Registre.CalculeDzetaMini: Real;
Var
  m_Registre : TVentil_Registre;
Begin
  TronconCalculPDCSinguliere := TronconPorteur;
  m_Registre := TVentil_Registre(TronconPorteur);
  DzetaMini := m_Registre.GetDzeta;
//Demande de MVN -> Dzeta peut �tre sup�rieur � 10
//  Result := Inherited CalculeDzetaMini;
  Result := DzetaMini;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_Registre.CalculeDzetaMaxi: Real;
Var
  m_Registre : TVentil_Registre;
Begin
  TronconCalculPDCSinguliere := TronconPorteur;
  m_Registre := TVentil_Registre(TronconPorteur);
  DzetaMaxi := m_Registre.GetDzeta;
//Demande de MVN -> Dzeta peut �tre sup�rieur � 10
//  Result := Inherited CalculeDzetaMaxi;
  Result := DzetaMaxi;
End;
{ **************************************************************** \ TVentil_Accident_Registre *************************************************************** }

{ ******************************************************************* TVentil_Accident_TourelleBifurcation *********************************************************** }
Procedure TVentil_Accident_TourelleBifurcation.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
Begin
  _Grid.Cells[_ColDep, _Row] := 'TCDZ11';
  Inherited;
    if Etude.Batiment.IsFabricantMVN and Not(VersionRnD) then
      begin
        If _ColDep = 0 Then
        _Grid.Cells[_ColDep + 1, _Row] := '-'
        else
        _Grid.Cells[_ColDep + 1, _Row] := 'Dz�ta: -';
      end;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_TourelleBifurcation.CalculeDzetaMini: Real;
Var
  m_TourelleBifurcation : TVentil_TourelleBifurcation;
Begin
  TronconCalculPDCSinguliere := TronconPorteur;
  m_TourelleBifurcation := TVentil_TourelleBifurcation(TronconPorteur);
  DzetaMini := m_TourelleBifurcation.GetDzeta;
  Result := Inherited CalculeDzetaMini;
End;
{ ******************************************************************************************************************************************************* }
Function TVentil_Accident_TourelleBifurcation.CalculeDzetaMaxi: Real;
Var
  m_TourelleBifurcation : TVentil_TourelleBifurcation;
Begin
  TronconCalculPDCSinguliere := TronconPorteur;
  m_TourelleBifurcation := TVentil_TourelleBifurcation(TronconPorteur);
  DzetaMaxi := m_TourelleBifurcation.GetDzeta;
  Result := Inherited CalculeDzetaMaxi;
End;
{ ******************************************************************* \ TVentil_Accident_TourelleBifurcation ********************************************************* }
End.
