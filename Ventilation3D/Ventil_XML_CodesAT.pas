
{********************************************************************************************}
{                                                                                            }
{                                   Liaison de donn�es XML                                   }
{                                                                                            }
{         G�n�r� le : 11/01/2018 10:25:13                                                    }
{       G�n�r� depuis : D:\Developpement\DelphSou\Ventilation3D\XSD\Ventil_XML_CodesAT.xsd   }
{                                                                                            }
{********************************************************************************************}

unit Ventil_XML_CodesAT;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ D�cl. Forward }

  IXMLCorrespondances = interface;
  IXMLConvert_ATs = interface;
  IXMLConvert_AT = interface;
  IXMLBouches = interface;
  IXMLElement = interface;
  IXMLEAs = interface;

{ IXMLCorrespondances }

  IXMLCorrespondances = interface(IXMLNode)
    ['{E6E1136F-934F-40FE-BC45-F307712C5921}']
    { Accesseurs de propri�t�s }
    function Get_CanLoad: Boolean;
    function Get_Convert_ATs: IXMLConvert_ATs;
    function Get_Bouches: IXMLBouches;
    function Get_EAs: IXMLEAs;
    procedure Set_CanLoad(Value: Boolean);
    { M�thodes & propri�t�s }
    property CanLoad: Boolean read Get_CanLoad write Set_CanLoad;
    property Convert_ATs: IXMLConvert_ATs read Get_Convert_ATs;
    property Bouches: IXMLBouches read Get_Bouches;
    property EAs: IXMLEAs read Get_EAs;
  end;

{ IXMLConvert_ATs }

  IXMLConvert_ATs = interface(IXMLNodeCollection)
    ['{7C4DA17D-A23E-483D-B874-A81C0F143265}']
    { Accesseurs de propri�t�s }
    function Get_Convert_AT(Index: Integer): IXMLConvert_AT;
    { M�thodes & propri�t�s }
    function Add: IXMLConvert_AT;
    function Insert(const Index: Integer): IXMLConvert_AT;
    property Convert_AT[Index: Integer]: IXMLConvert_AT read Get_Convert_AT; default;
  end;

{ IXMLConvert_AT }

  IXMLConvert_AT = interface(IXMLNode)
    ['{69BEEC9E-D5B1-47CE-AF4C-9415759A7EFC}']
    { Accesseurs de propri�t�s }
    function Get_ID_OldDataBase: UnicodeString;
    function Get_REF_AT: UnicodeString;
    procedure Set_ID_OldDataBase(Value: UnicodeString);
    procedure Set_REF_AT(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property ID_OldDataBase: UnicodeString read Get_ID_OldDataBase write Set_ID_OldDataBase;
    property REF_AT: UnicodeString read Get_REF_AT write Set_REF_AT;
  end;

{ IXMLBouches }

  IXMLBouches = interface(IXMLNodeCollection)
    ['{AA0D0027-4C91-4E1B-A0B8-CCD479C683C9}']
    { Accesseurs de propri�t�s }
    function Get_Bouche(Index: Integer): IXMLElement;
    { M�thodes & propri�t�s }
    function Add: IXMLElement;
    function Insert(const Index: Integer): IXMLElement;
    property Bouche[Index: Integer]: IXMLElement read Get_Bouche; default;
  end;

{ IXMLElement }

  IXMLElement = interface(IXMLNode)
    ['{A5A3EF10-A24B-48B0-9D78-DBDB88A570A2}']
    { Accesseurs de propri�t�s }
    function Get_Code_AT: UnicodeString;
    function Get_Code_Article: UnicodeString;
    function Get_Code_Aerau: UnicodeString;
    procedure Set_Code_AT(Value: UnicodeString);
    procedure Set_Code_Article(Value: UnicodeString);
    procedure Set_Code_Aerau(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property Code_AT: UnicodeString read Get_Code_AT write Set_Code_AT;
    property Code_Article: UnicodeString read Get_Code_Article write Set_Code_Article;
    property Code_Aerau: UnicodeString read Get_Code_Aerau write Set_Code_Aerau;
  end;

{ IXMLEAs }

  IXMLEAs = interface(IXMLNodeCollection)
    ['{4C046403-ADC6-420F-B1E4-B344F8171DDE}']
    { Accesseurs de propri�t�s }
    function Get_EA(Index: Integer): IXMLElement;
    { M�thodes & propri�t�s }
    function Add: IXMLElement;
    function Insert(const Index: Integer): IXMLElement;
    property EA[Index: Integer]: IXMLElement read Get_EA; default;
  end;

{ D�cl. Forward }

  TXMLCorrespondances = class;
  TXMLConvert_ATs = class;
  TXMLConvert_AT = class;
  TXMLBouches = class;
  TXMLElement = class;
  TXMLEAs = class;

{ TXMLCorrespondances }

  TXMLCorrespondances = class(TXMLNode, IXMLCorrespondances)
  protected
    { IXMLCorrespondances }
    function Get_CanLoad: Boolean;
    function Get_Convert_ATs: IXMLConvert_ATs;
    function Get_Bouches: IXMLBouches;
    function Get_EAs: IXMLEAs;
    procedure Set_CanLoad(Value: Boolean);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLConvert_ATs }

  TXMLConvert_ATs = class(TXMLNodeCollection, IXMLConvert_ATs)
  protected
    { IXMLConvert_ATs }
    function Get_Convert_AT(Index: Integer): IXMLConvert_AT;
    function Add: IXMLConvert_AT;
    function Insert(const Index: Integer): IXMLConvert_AT;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLConvert_AT }

  TXMLConvert_AT = class(TXMLNode, IXMLConvert_AT)
  protected
    { IXMLConvert_AT }
    function Get_ID_OldDataBase: UnicodeString;
    function Get_REF_AT: UnicodeString;
    procedure Set_ID_OldDataBase(Value: UnicodeString);
    procedure Set_REF_AT(Value: UnicodeString);
  end;

{ TXMLBouches }

  TXMLBouches = class(TXMLNodeCollection, IXMLBouches)
  protected
    { IXMLBouches }
    function Get_Bouche(Index: Integer): IXMLElement;
    function Add: IXMLElement;
    function Insert(const Index: Integer): IXMLElement;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLElement }

  TXMLElement = class(TXMLNode, IXMLElement)
  protected
    { IXMLElement }
    function Get_Code_AT: UnicodeString;
    function Get_Code_Article: UnicodeString;
    function Get_Code_Aerau: UnicodeString;
    procedure Set_Code_AT(Value: UnicodeString);
    procedure Set_Code_Article(Value: UnicodeString);
    procedure Set_Code_Aerau(Value: UnicodeString);
  end;

{ TXMLEAs }

  TXMLEAs = class(TXMLNodeCollection, IXMLEAs)
  protected
    { IXMLEAs }
    function Get_EA(Index: Integer): IXMLElement;
    function Add: IXMLElement;
    function Insert(const Index: Integer): IXMLElement;
  public
    procedure AfterConstruction; override;
  end;

{ Fonctions globales }

function GetCorrespondances(Doc: IXMLDocument): IXMLCorrespondances;
function LoadCorrespondances(const FileName: string): IXMLCorrespondances;
function NewCorrespondances: IXMLCorrespondances;

const
  TargetNamespace = '';

implementation

{ Fonctions globales }

function GetCorrespondances(Doc: IXMLDocument): IXMLCorrespondances;
begin
  Result := Doc.GetDocBinding('Correspondances', TXMLCorrespondances, TargetNamespace) as IXMLCorrespondances;
end;

function LoadCorrespondances(const FileName: string): IXMLCorrespondances;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Correspondances', TXMLCorrespondances, TargetNamespace) as IXMLCorrespondances;
end;

function NewCorrespondances: IXMLCorrespondances;
begin
  Result := NewXMLDocument.GetDocBinding('Correspondances', TXMLCorrespondances, TargetNamespace) as IXMLCorrespondances;
end;

{ TXMLCorrespondances }

procedure TXMLCorrespondances.AfterConstruction;
begin
  RegisterChildNode('Convert_ATs', TXMLConvert_ATs);
  RegisterChildNode('Bouches', TXMLBouches);
  RegisterChildNode('EAs', TXMLEAs);
  inherited;
end;

function TXMLCorrespondances.Get_CanLoad: Boolean;
begin
  Result := ChildNodes['CanLoad'].NodeValue;
end;

procedure TXMLCorrespondances.Set_CanLoad(Value: Boolean);
begin
  ChildNodes['CanLoad'].NodeValue := Value;
end;

function TXMLCorrespondances.Get_Convert_ATs: IXMLConvert_ATs;
begin
  Result := ChildNodes['Convert_ATs'] as IXMLConvert_ATs;
end;

function TXMLCorrespondances.Get_Bouches: IXMLBouches;
begin
  Result := ChildNodes['Bouches'] as IXMLBouches;
end;

function TXMLCorrespondances.Get_EAs: IXMLEAs;
begin
  Result := ChildNodes['EAs'] as IXMLEAs;
end;

{ TXMLConvert_ATs }

procedure TXMLConvert_ATs.AfterConstruction;
begin
  RegisterChildNode('Convert_AT', TXMLConvert_AT);
  ItemTag := 'Convert_AT';
  ItemInterface := IXMLConvert_AT;
  inherited;
end;

function TXMLConvert_ATs.Get_Convert_AT(Index: Integer): IXMLConvert_AT;
begin
  Result := List[Index] as IXMLConvert_AT;
end;

function TXMLConvert_ATs.Add: IXMLConvert_AT;
begin
  Result := AddItem(-1) as IXMLConvert_AT;
end;

function TXMLConvert_ATs.Insert(const Index: Integer): IXMLConvert_AT;
begin
  Result := AddItem(Index) as IXMLConvert_AT;
end;

{ TXMLConvert_AT }

function TXMLConvert_AT.Get_ID_OldDataBase: UnicodeString;
begin
  Result := ChildNodes['ID_OldDataBase'].Text;
end;

procedure TXMLConvert_AT.Set_ID_OldDataBase(Value: UnicodeString);
begin
  ChildNodes['ID_OldDataBase'].NodeValue := Value;
end;

function TXMLConvert_AT.Get_REF_AT: UnicodeString;
begin
  Result := ChildNodes['REF_AT'].Text;
end;

procedure TXMLConvert_AT.Set_REF_AT(Value: UnicodeString);
begin
  ChildNodes['REF_AT'].NodeValue := Value;
end;

{ TXMLBouches }

procedure TXMLBouches.AfterConstruction;
begin
  RegisterChildNode('Bouche', TXMLElement);
  ItemTag := 'Bouche';
  ItemInterface := IXMLElement;
  inherited;
end;

function TXMLBouches.Get_Bouche(Index: Integer): IXMLElement;
begin
  Result := List[Index] as IXMLElement;
end;

function TXMLBouches.Add: IXMLElement;
begin
  Result := AddItem(-1) as IXMLElement;
end;

function TXMLBouches.Insert(const Index: Integer): IXMLElement;
begin
  Result := AddItem(Index) as IXMLElement;
end;

{ TXMLElement }

function TXMLElement.Get_Code_AT: UnicodeString;
begin
  Result := ChildNodes['Code_AT'].Text;
end;

procedure TXMLElement.Set_Code_AT(Value: UnicodeString);
begin
  ChildNodes['Code_AT'].NodeValue := Value;
end;

function TXMLElement.Get_Code_Article: UnicodeString;
begin
  Result := ChildNodes['Code_Article'].Text;
end;

procedure TXMLElement.Set_Code_Article(Value: UnicodeString);
begin
  ChildNodes['Code_Article'].NodeValue := Value;
end;

function TXMLElement.Get_Code_Aerau: UnicodeString;
begin
  Result := ChildNodes['Code_Aerau'].Text;
end;

procedure TXMLElement.Set_Code_Aerau(Value: UnicodeString);
begin
  ChildNodes['Code_Aerau'].NodeValue := Value;
end;

{ TXMLEAs }

procedure TXMLEAs.AfterConstruction;
begin
  RegisterChildNode('EA', TXMLElement);
  ItemTag := 'EA';
  ItemInterface := IXMLElement;
  inherited;
end;

function TXMLEAs.Get_EA(Index: Integer): IXMLElement;
begin
  Result := List[Index] as IXMLElement;
end;

function TXMLEAs.Add: IXMLElement;
begin
  Result := AddItem(-1) as IXMLElement;
end;

function TXMLEAs.Insert(const Index: Integer): IXMLElement;
begin
  Result := AddItem(Index) as IXMLElement;
end;

end.
