Unit Ventil_TourellePlenum;
 
{$Include 'Ventil_Directives.pas'}

Interface
Uses
  Classes, SysUtils, advGrid, BaseGrid, AdvObj, Math,
  Ventil_Types, Ventil_Troncon, Ventil_Logement, Ventil_Utils,
  XMLIntf,
  AccesBDD,
  Ventil_Plenum,
  BBScad_Interface_Aeraulique;

Type
{ ************************************************************************************************************************************************** }
  TVentil_TourellePlenum = Class(TVentil_Plenum, IAero_Fredo_Plenum)
    Constructor Create; Override;
  Private
  Public
    class Function FamilleObjet : String; Override;
    Procedure Load; Override;
    Function  AfficheQuestion(_NoQuestion: Integer): Boolean; Override;
    Function GetDzetaMVN : Real; Override;
  Protected
    class Function NomParDefaut : String; Override;
    Function  ImageAssociee: String; Override;
    Function GetTableDonnees : TTableDeDonneesBBS; Override;
    Procedure Save; Override;
  End;
{ ************************************************************************************************************************************************** }

Implementation
Uses
  Ventil_Declarations,
  Ventil_Const,
  Ventil_Firebird,
  Ventil_Colonne,
  Grids,
  Graphics;

{ ******************************************************************* TVentil_TourellePlenum *************************************************************** }
Constructor TVentil_TourellePlenum.Create;
Begin
  Inherited;
  HauteurPredef := c_Plenum_H10;
End;
{ ************************************************************************************************************************************************** }
class Function TVentil_TourellePlenum.NomParDefaut : String;
Begin
    Result := 'Adapt�Air'
End;
{ ************************************************************************************************************************************************** }
class Function TVentil_TourellePlenum.FamilleObjet : String;
Begin
    Result := 'Adapt�Air'
End;
{ ************************************************************************************************************************************************** }
Function TVentil_TourellePlenum.ImageAssociee: String;
Begin
  if TypePlenum = c_TypePlenumRecouvrement then
    Result := 'CaissonCollecteur.jpg'

  else
    Result := 'PLENUM.jpg';
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourellePlenum.Load;
begin
  PiegeASon := GetTableDonnees.Donnees.FieldByName('PIEGEASON').AsInteger;
  Inherited;
end;
{ ************************************************************************************************************************************************** }
Function TVentil_TourellePlenum.GetTableDonnees : TTableDeDonneesBBS;
begin
  Result := Table_Etude_TourellePlenum;
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourellePlenum.Save;
begin
  GetTableDonnees.Donnees.Insert;
  GetTableDonnees.Donnees.FieldByName('ID_TOURPLENUM').AsWideString := GUIDToString(Id);
  GetTableDonnees.Donnees.FieldByName('PIEGEASON').AsInteger := PIEGEASON;
  Inherited;
end;
{ ************************************************************************************************************************************************** }
Function  TVentil_TourellePlenum.AfficheQuestion(_NoQuestion: Integer): Boolean;
Begin

  Result := Inherited AfficheQuestion(_NoQuestion);
  if _NoQuestion = c_QPlenum_PiegeASon then
    Result := Etude.Batiment.IsFabricantMVN
  else
  if _NoQuestion = c_QPlenum_HauteurPredef then
    Result := Etude.Batiment.IsFabricantMVN
  else
  if _NoQuestion = c_QPlenum_Hauteur then
    Result := not(Etude.Batiment.IsFabricantMVN)
  else
  if _NoQuestion = c_QTroncon_Diametre then
    Result := False;

End;
{ ************************************************************************************************************************************************** }
Function TVentil_TourellePlenum.GetDzetaMVN : Real;
var
  Cpt       : Integer;
  nbAlsace  : Integer;
  nbShunt   : Integer;
  nbIndiv   : Integer;
  nbRamon   : Integer;
  nbVMC     : Integer;
  TypeConfig : Integer;
Const
  ValDefaut = 10;
  ConfigA = 0;
  ConfigB = 1;
  ConfigC = 2;
  ConfigD = 3;
  ConfigE = 4;
  ConfigF = 5;
  ConfigG = 6;
  ConfigAutre = 7;

  DzetaRetour : Array[ConfigA..ConfigAutre, c_Plenum_H10..c_Plenum_H40] of Real =
    (
      (1.94 , 1.73  , 1.73  ), //A
      (1.5  , 1.22  , 1.28  ), //B
      (0.34 , 0.3   , 0.49  ), //C
      (-1.42, -1.58 , -1.34 ), //D
      (0.87 , 1.08  , 1.03  ), //E
      (1.52 , 1.01  , 1.03  ), //F
      (1.66 , 1.43  , 1.44  ), //G
      (ValDefaut , ValDefaut  , ValDefaut  )  //Autre
    );
begin

  Result := ValDefaut;
{
    if Diametre < 160 then
      begin
      Result := ValDefaut;
      Exit;
      end;

    if Diametre > 400 then
      begin
      Result := ValDefaut;
      Exit;
      end;
}

  nbAlsace  := 0;
  nbShunt   := 0;
  nbIndiv   := 0;
  nbRamon   := 0;
  nbVMC     := 0;


    for Cpt := 0 to FilsCalcul.Count - 1 do
//      if TVentil_Colonne(FilsCalcul[cpt]).FilsCalcul.Count > 0 then
        begin
          Case TVentil_Colonne(FilsCalcul[cpt]).TypeColonne of
            c_TypeColonneVMC                      : Inc(nbVMC);
            c_TypeColonneShunt                    : Inc(nbShunt);
            c_TypeColonneConduitIndividuel        : Inc(nbIndiv);
            c_TypeColonneAlsace                   : Inc(nbAlsace);
            c_TypeColonneRamon                    : Inc(nbRamon);
          End;
        end;

  TypeConfig := ConfigAutre;

    if ((nbIndiv >= 4) and (ConfigEnLigne(1, 4) or ConfigEnLigne(2, 4))) and (nbVMC = 0) and (nbShunt = 0) and (nbAlsace = 0) and (nbRamon = 0) then
      TypeConfig := configA
    else
    if (((nbIndiv <= 3) and ConfigEnLigne(1, 1, 3)) or  ((nbIndiv <= 6) and ConfigEnLigne(2, 1, 3))) and (nbVMC = 0) and (nbShunt = 0) and (nbAlsace = 0) and (nbRamon = 0) then
      TypeConfig := configB
    else
    if (nbShunt = 1) and (nbRamon >= 1) and ConfigEnLigne(1, 2) then
      TypeConfig := configC
    else
    if (nbShunt = 2) and (nbRamon = 2) and (NbLigne = 2) and (NbColonne = 2) then
      TypeConfig := configC
    else
    if ((nbShunt = 1) and (nbRamon = 0)) and (nbAlsace = 0) and (nbIndiv = 0) and (nbVMC = 0) then
      TypeConfig := configD
    else
    if ((nbShunt = 2) and (nbRamon = 0)) and (nbAlsace = 0) and (nbIndiv = 0) and (nbVMC = 0) then
      TypeConfig := configE
    else
    if ((nbShunt = 2) and (nbRamon > 0) and ConfigEnLigne(1, 3)) and (nbAlsace = 0) and (nbIndiv = 0) and (nbVMC = 0) then
      TypeConfig := configF
    else
    if ((nbShunt >= 3) and (nbRamon > 0) and ConfigEnLigne(1, 4)) and (nbAlsace = 0) and (nbIndiv = 0) and (nbVMC = 0) then
      TypeConfig := configG;


  Result := DzetaRetour[TypeConfig, HauteurPredef];

  if (ForceDzeta = c_Oui) and (Result <> ValDefaut) then
    begin
    Result := DzetaSaisie;
    Exit;
    end;

end;
{ ******************************************************************* \ TVentil_TourellePlenum ************************************************************* }

End.




