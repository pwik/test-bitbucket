Unit Ventil_Chiffrage;
 
{$Include 'Ventil_Directives.pas'}

Interface

Uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, Dialogs, Types, 
    Buttons, ExtCtrls, ComCtrls, ToolWin, Grids, BaseGrid, AdvGrid, ImgList, Math,
    Menus,
    Ventil_ChiffrageTypes,
    Ventil_EdibatecProduits, AdvObj{$IfDef VER310}, System.ImageList {$EndIf};

Type

  TDlg_Chiffrage = class(TForm)
    PanelChiffrage: TPanel;
    Toolbar_Chiffrage: TToolBar;
    Btn_Generer: TToolButton;
    btn_Export: TToolButton;
    ToolButton5: TToolButton;
    Btn_NewProd: TToolButton;
    Btn_NewCommentaire: TToolButton;
    Btn_NewSSTotal: TToolButton;
    Btn_Delete: TToolButton;
    ToolButton4: TToolButton;
    Btn_Recalcule: TToolButton;
    ImageList_ToolBar: TImageList;
    TabChapitres: TTabControl;
    Grid_Quantitatif: TAdvStringGrid;
    PopupMenu_Tabs: TPopupMenu;
    Supprimer1: TMenuItem;
    Renommer1: TMenuItem;
    PopupMenu_Grid: TPopupMenu;
    Insrerunproduit1: TMenuItem;
    Insrerunsoustotal1: TMenuItem;
    Supprimerleproduitslectionn1: TMenuItem;
    ToolButton1: TToolButton;
    Button_Imp: TToolButton;
    Btn_ProduitsNonTrouves: TToolButton;
    SaveDialog_Excel: TSaveDialog;
    Btn_Options: TToolButton;
    ButtonImp_Qualitatif: TToolButton;
    Insreruncommentaire1: TMenuItem;
    Btn_BIMetre: TToolButton;
    Procedure Grid_QuantitatifResize(Sender: TObject);
    Procedure ToolButtonClick(Sender: TObject);
    procedure TabChapitresChange(Sender: TObject);
    procedure Supprimer1Click(Sender: TObject);
    procedure Grid_QuantitatifKeyPress(Sender: TObject; var Key: Char);
    procedure Grid_QuantitatifGetEditorType(Sender: TObject; ACol, ARow: Integer; var AEditor: TEditorType);
    procedure Grid_QuantitatifCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: String; var Valid: Boolean);
    procedure Grid_QuantitatifRowChanging(Sender: TObject; OldRow, NewRow: Integer; var Allow: Boolean);
    procedure PopupMenu_GridPopup(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Btn_BIMetreClick(Sender: TObject);
  Private
    Function CurrentChiffrage : TChiffrage;
    Function  GenerationChiffrage(_AvecSelection: Boolean): Boolean;
    Procedure ExportExcel(GenerationExportVIM : Boolean = False);
    Procedure NouveauProduit;
    Procedure NouveauCommentaire;
    Procedure NouveauSousTotal;
    Procedure Suppression;
    Procedure RecalculChiffrage;
    Procedure ResetGrid;
    Procedure ClearGrid;
    Procedure DimensionneGrid;
    Procedure Suppression_Chiffrage(_No : Integer = -1);
    Procedure Renommer_Chiffrage;
    Function  LigneValide : Boolean;
    Procedure HighLightButtons(_CurLine: Integer);
    Function  ChoixChiffrage: Boolean;
  Public
    TimerAutoSave : TTimer;
    Procedure AffChiffrage(_Chiffrage: TChiffrage);
  End;

Var
  Dlg_Chiffrage: TDlg_Chiffrage;


Procedure Chiffrage(_Parent : TPanel; Var _TimerAutoSave : TTimer; _AvecSelection: Boolean = True);
Procedure CloseChiffrage;


Implementation
Uses
  Ventil_Utils, Ventil_Edibatec, Ventil_Const, Ventil_EdibatecChoixProduit,
  Ventil_ProduitsNonTrouves,
  Ventil_EdibatecChoixGamme,
  Ventil_SelectionCaisson,
  Ventil_Troncon,
  Ventil_Caisson, Ventil_Impressions, Ventil_Declarations,
  Ventil_Options,
  Ventil_SaisieMemo,
  BbsgTec,
  ComObj, Variants,
  {$IFDEF BIM_METRE}
  XtBIMetre_Main,
  {$ENDIF}
  {$IfNDef AERAU_CLIMAWIN}
  VMC_Etude,
  {$EndIf}
  BBS_Message;

{$R *.dfm}

{ ************************************************************************************************************************************************** }
Procedure Chiffrage(_Parent : TPanel; Var _TimerAutoSave : TTimer; _AvecSelection: Boolean = True);
Begin
        if Etude <> nil then
                Etude.AutoSaveAutorisee := False;
Etude.Calculer;
//if not(Etude.Batiment.IsFabricantVIM) then
        Etude.Batiment.Reseaux.CalculerQuantitatif(Etude.Batiment);
  Application.CreateForm(TDlg_Chiffrage, Dlg_Chiffrage);
  {$IFDEF BIM_METRE}
  Dlg_Chiffrage.Btn_BIMetre.Visible := true;
  {$ENDIF}
  Dlg_Chiffrage.TimerAutoSave := _TimerAutoSave;
    if not(Etude.Batiment.IsFabricantVIM) then
    begin
    Dlg_Chiffrage.Btn_Options.Visible := true;
    Dlg_Chiffrage.ButtonImp_Qualitatif.Visible := true;
    end
    else
    begin
    Dlg_Chiffrage.Btn_Options.Visible := false;
    Dlg_Chiffrage.ButtonImp_Qualitatif.Visible := false;
    end;

  {$IfDef ACTHYS_DIMVMBP}
  Dlg_Chiffrage.Btn_Generer.Visible := false;
//  Dlg_Chiffrage.Btn_Options.Visible := false;
  {$EndIf}

  If Dlg_Chiffrage.GenerationChiffrage(_AvecSelection) Then
  Begin
     {$IfDef VIM_Optair}
     {$IfNDef AERAU_CLIMAWIN}
     Dlg_Chiffrage.Insrerunproduit1.Visible := False;
     Dlg_Chiffrage.Insrerunsoustotal1.Visible := False;
     Dlg_Chiffrage.Insreruncommentaire1.Visible := False;
     Dlg_Chiffrage.Supprimerleproduitslectionn1.Visible := False;
     Dlg_Chiffrage.Btn_Generer.Visible := false;
     Dlg_Chiffrage.Btn_NewCommentaire.Visible := false;
    {$IfDef MVN_MVNAIR}
    {$Else}
      {$IfDef ACTHYS_DIMVMBP}
      {$Else}
      if Fic_Etude.VersionAutonome then
      {$EndIf}
    {$EndIf}
                Dlg_Chiffrage.ShowModal;
     {$EndIf}
     {$EndIf}
    if Etude.Batiment.IsFabricantVIM then
    begin
        If Dlg_Chiffrage.Btn_ProduitsNonTrouves.Visible then
                Dlg_Chiffrage.Btn_ProduitsNonTrouves.Click;
{$IfDef BIM_METRE}
        Dlg_Chiffrage.ShowModal;
{$Else}
{$EndIf}
    end                                                                                               
    else
    begin
    Dlg_Chiffrage.ShowModal;
    end;
    CloseChiffrage;
  End Else Begin
    FreeAndNil(Dlg_Chiffrage);
  End;
        if Etude <> nil then
                Etude.AutoSaveAutorisee := True;  
End;
{ ************************************************************************************************************************************************** }
Procedure CloseChiffrage;
Begin
  If IsNotNil(Dlg_Chiffrage) Then FreeAndNil(Dlg_Chiffrage);
End;

{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.AffChiffrage(_Chiffrage: TChiffrage);
Begin
//  TabChapitres.Tabs.Objects[0] := _Chiffrage;
//  TabChapitres.Tabs[0] := _Chiffrage.Reference;
  ResetGrid;
  _Chiffrage.Affiche(Grid_Quantitatif);
  If _Chiffrage.Erreurs.Count > 0 Then Begin
    BBS_ShowMessage('Certains produits n''ont pas �t� trouv�s,' + #13 +
               'cliquez sur l''ic�ne avertissement pour les consulter', mtWarning, [mbOK], 0);
    Btn_ProduitsNonTrouves.Visible := True;
  End Else Btn_ProduitsNonTrouves.Visible := False;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.ExportExcel(GenerationExportVIM : Boolean = False);
Var
  m_NoElt          : Integer;
  m_Ligne          : String;
  ExportExcel      : Boolean;
  Excel            : Variant;
  Classeur         : Variant;
  FileName         : String;
Begin
  { OPTAIR

    1er param�tre = r�seau
      2 --> vertical
      1 --> hortizontal
      3 --> Rejet

    2�me param�tre = type de mat�riel
      A --> accessoire de r�seau
      B --> bouche
      E --> entr�e d'air
      F --> T� souche
      K --> Caisson
      Z --> Autre

    3�me param�tre = Code du produit

    4�me param�tre = Qt�

  }

  { Cr�ation du fichier }
  FreeAndNil(Etude.DataImp_Quantitatif);
  Etude.DataImp_Quantitatif := TStringList.Create;

  If Not CurrentChiffrage.Sorted Then
        CurrentChiffrage.GenereChiffrageTrie;

  FileName := FileNameWithoutExt(Etude.Fichier);

  if not(GenerationExportVIM) then
        begin
          if Etude.Batiment.IsFabricantF_A then
            begin
            m_Ligne := 'Titre;' + FileName;
            end
          else
            begin
            m_Ligne := 'Partie r�seau;Code fabricant;';
              For m_NoElt := cst_XLS_Libelle To cst_XLS_PTotal Do
                If Options.ColonnesExcel[m_NoElt] Then
                  m_Ligne := m_Ligne + cst_Colonnes_Excel[m_NoElt] + ';';
            end;
        Etude.DataImp_Quantitatif.Add(m_Ligne);
        end;

  For m_NoElt := 0 To CurrentChiffrage.Count - 1 Do
        Begin
        m_Ligne := CurrentChiffrage.Element[m_NoElt].ExportDelimite(GenerationExportVIM);
                If m_Ligne <> '' Then
                        Etude.DataImp_Quantitatif.Add(m_Ligne);
        End;

  //demande de suppression demand�e par stephane bergeron le 17/08
  If FileExists(ExtractFilePath(Application.ExeName) + cst_RepEtudeOptair + '\' + Etude.NoOptair + '.err') Then DeleteFile(ExtractFilePath(Application.ExeName) + cst_RepEtudeOptair + '\' + Etude.NoOptair + '.err');

  If Etude.DataImp_Quantitatif.Count > 0 Then
        Begin

                if GenerationExportVIM then
                        begin
                        Etude.DataImp_Quantitatif.SaveToFile(ExtractFilePath(Application.ExeName) + cst_RepEtudeOptair + '\' + Etude.NoOptair + '.txt');
                                If CurrentChiffrage.Erreurs.Count > 0 Then
                                        Begin
                                        CurrentChiffrage.Erreurs.SaveToFile(ExtractFilePath(Application.ExeName) + cst_RepEtudeOptair + '\' + Etude.NoOptair + '.err');
//                                        Etude.DataImp_Erreurs := Etude.ChiffrageActif.Erreurs;
                                        Etude.DataImp_Erreurs := TStringList.Create;
                                        Etude.DataImp_Erreurs.Assign(CurrentChiffrage.Erreurs);
                                        End;
                        end
                else
                        begin

  {$IfNDef AERAU_CLIMAWIN}
                          if Etude.Batiment.IsFabricantF_A then
                            begin
                              If DirectoryExists('z:\') Then
                                SaveDialog_Excel.InitialDir := 'z:\'
                              Else
                                SaveDialog_Excel.InitialDir := Fic_Etude.InitialDir;
                            SaveDialog_Excel.FileName := 'Collectair.csv';
                            end
                          else
                            begin
                            SaveDialog_Excel.InitialDir := Fic_Etude.InitialDir;
                              if Etude.Batiment.IsFabricantACT then
                                SaveDialog_Excel.FileName := 'Export DimVMBP � ' + FileName + '.csv'
                              else
                                SaveDialog_Excel.FileName := 'Export.csv';
                            end;
  {$EndIf}
                          If SaveDialog_Excel.Execute Then
                                        Begin
                                        Etude.DataImp_Quantitatif.SaveToFile(SaveDialog_Excel.FileName);

                                                Try { Try except car le com c'est farceur }
                                                Excel := CreateOleObject('Excel.Application');
                                                        If Not VarIsEmpty(Excel) Then
                                                                Begin
                                                                Excel.Visible := True;
                                                                Classeur := Excel.Workbooks.Open(SaveDialog_Excel.FileName);
                                                                End;
                                                Except
                                                End;
                                        End;
                        end;

        End;
End;
{ ************************************************************************************************************************************************** }
Function TDlg_Chiffrage.ChoixChiffrage: Boolean;
Var
  m_No : Integer;
  tempListCaissons : TlisteCaissons;
  cptCaissons : Integer;
Begin


  Result := False;
Etude.Batiment.Reseaux.GetCaissons(tempListCaissons);


  for cptCaissons := 0 to tempListCaissons.count - 1 do
    Result := (SelectionDuCaisson(TVentil_Caisson(tempListCaissons[cptCaissons]), nil) <> nil) or Result;

tempListCaissons.Destroy;
End;
{ ************************************************************************************************************************************************** }
Function TDlg_Chiffrage.CurrentChiffrage : TChiffrage;
begin
  if TabChapitres.Tabs.Count > -1 then
    Result := TChiffrage(TabChapitres.Tabs.Objects[TabChapitres.TabIndex])
  else
    Result := Nil;
end;
{ ************************************************************************************************************************************************** }
Function TDlg_Chiffrage.GenerationChiffrage(_AvecSelection: Boolean): Boolean;
Var
        tempListCaissons : TlisteCaissons;
        cptCaissons : Integer;
        cptLogement : Integer;
Begin
//  TVentil_Caisson(tempListCaissons[cptCaissons])
  Result := False;
  TabChapitres.Tabs.Clear;
  If IsNotNil(Etude) Then
    Begin
    Etude.Batiment.Reseaux.GetCaissons(tempListCaissons);
      for cptCaissons := 0 to tempListCaissons.count - 1 do
        begin
        TabChapitres.Tabs.Add(TVentil_Caisson(tempListCaissons[cptCaissons]).Reference + ' - ' + TVentil_Caisson(tempListCaissons[cptCaissons]).ChiffrageActif.Reference);
        TabChapitres.Tabs.Objects[TabChapitres.Tabs.Count - 1] := TVentil_Caisson(tempListCaissons[cptCaissons]).ChiffrageActif;
        TChiffrage(TabChapitres.Tabs.Objects[TabChapitres.Tabs.Count - 1]).Clear;
        End;

      if Etude.Batiment.IsFabricantACT then
        for cptLogement := 0 to Etude.Batiment.Logements.Count - 1 do
          begin
            TabChapitres.Tabs.Add(Etude.Batiment.Logements[cptLogement].Reference + ' (T2A) - ' + Etude.Batiment.Logements[cptLogement].ChiffrageT2A.Reference);
            TabChapitres.Tabs.Objects[TabChapitres.Tabs.Count - 1] := Etude.Batiment.Logements[cptLogement].ChiffrageT2A;
            TChiffrage(TabChapitres.Tabs.Objects[TabChapitres.Tabs.Count - 1]).Clear;
          end;
    tempListCaissons.Destroy;
    End;

    if TabChapitres.Tabs.Count > -1 then
      TabChapitres.TabIndex := 0;

          If (Not _AvecSelection) Or ChoixChiffrage Then
            Begin
            Etude.Batiment.Reseaux.CalculerQuantitatif(Etude.Batiment);

            Etude.Batiment.Reseaux.GetCaissons(tempListCaissons);
              for cptCaissons := 0 to tempListCaissons.count - 1 do
                begin
                TabChapitres.Tabs[cptCaissons] := (TVentil_Caisson(tempListCaissons[cptCaissons]).Reference + ' - ' + TVentil_Caisson(tempListCaissons[cptCaissons]).ChiffrageActif.Reference);
                TabChapitres.Tabs.Objects[cptCaissons] := TVentil_Caisson(tempListCaissons[cptCaissons]).ChiffrageActif;
                end;

            if Etude.Batiment.IsFabricantACT then
              begin
                for cptLogement := 0 to Etude.Batiment.Logements.Count - 1 do
                  begin
                  TabChapitres.Tabs[tempListCaissons.Count + cptLogement] := (Etude.Batiment.Logements[cptLogement].Reference + ' (T2A) - ' + Etude.Batiment.Logements[cptLogement].ChiffrageT2A.Reference);
                  TabChapitres.Tabs.Objects[tempListCaissons.Count + cptLogement] := Etude.Batiment.Logements[cptLogement].ChiffrageT2A;
                  end;

                for cptLogement := Etude.Batiment.Logements.Count - 1 DownTo 0 do
                  if TChiffrage(TabChapitres.Tabs.Objects[tempListCaissons.Count + cptLogement]).Empty then
                    TabChapitres.Tabs.Delete(tempListCaissons.Count + cptLogement);

              end;
            tempListCaissons.Destroy;

            AffChiffrage(CurrentChiffrage);
              if Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN then
                Etude.Batiment.ChiffrageVerouille := c_OUI;
              if Etude.Batiment.IsFabricantVIM then
                ExportExcel(True);
            Result := True;
            end;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.Suppression_Chiffrage(_No : Integer = -1);
Var
  m_Idx   : Integer;
  m_Chiff : TChiffrage;
  cptCaisson : Integer;
Begin
  If _No <> - 1 Then m_Idx := _No
                Else m_Idx := TabChapitres.TabIndex;
  If m_Idx >= 0 Then Begin
    m_Chiff := TChiffrage(TabChapitres.Tabs.Objects[m_Idx]);
    If IsNotNil(m_Chiff) Then Begin
      for cptCaisson := 0 to Etude.Batiment.Reseaux.Count - 1 do
        if Etude.Batiment.Reseaux[cptCaisson].InheritsFrom(TVentil_Caisson) and
           (TVentil_Caisson(Etude.Batiment.Reseaux[cptCaisson]).Chiffrages.IndexOf(m_Chiff) > -1) then
           begin
           TVentil_Caisson(Etude.Batiment.Reseaux[cptCaisson]).Chiffrages.Remove(m_Chiff);
           FreeAndNil(m_Chiff);
           ClearGrid;
           ResetGrid;
           Exit;
           end;
    End;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.Renommer_Chiffrage;
Var
  m_Idx   : Integer;
  m_Str   : String;
Begin
  m_Idx := TabChapitres.TabIndex;
  If (m_Idx >= 0) And IsNotNil(TChiffrage(TabChapitres.Tabs.Objects[m_Idx])) Then Begin
    m_Str := TabChapitres.Tabs[m_Idx];
    If InputQuery('Renommer', 'Nouveau non de l''onglet: ', m_Str) Then Begin
      TChiffrage(TabChapitres.Tabs.Objects[m_Idx]).Reference := m_Str;
      TabChapitres.Tabs[m_Idx] := m_Str;
    End;
  End;
End;
{ ************************************************************************************************************************************************** }
Function TDlg_Chiffrage.LigneValide : Boolean;
Var
  m_Idx : Integer;
Begin
  m_Idx := TabChapitres.TabIndex;
  Result := (m_Idx > - 1) And IsNotNil(TChiffrage(TabChapitres.Tabs.Objects[m_Idx])) And IsNotNil(Grid_Quantitatif.Objects[0, Grid_Quantitatif.Row]);
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.HighLightButtons(_CurLine: Integer);
Begin
  Btn_NewSSTotal.Enabled := (Grid_Quantitatif.Objects[0, _CurLine] <> Nil) And
                            (
                             (TChiffrage_Element(Grid_Quantitatif.Objects[0, _CurLine]) Is TChiffrage_Titre) Or
                             (TChiffrage_Element(Grid_Quantitatif.Objects[0, _CurLine]) Is TChiffrage_Classe)
                            );
  Insrerunsoustotal1.Enabled := Btn_NewSSTotal.Enabled;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.Grid_QuantitatifResize(Sender: TObject);
Begin
  DimensionneGrid;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.NouveauCommentaire;
Var
  m_Idx       : Integer;
  m_Chiffrage : TChiffrage;
  m_Insertion : TChiffrage_Commentaire;
  _Text       : TStringList;
Begin
  m_Idx := TabChapitres.TabIndex;
  If m_Idx < 0 Then Exit;
  _Text := TStringList.Create;
  If InputQueryMemo('Nouveau commentaire', _Text) Then Begin
  m_Chiffrage := TChiffrage(TabChapitres.Tabs.Objects[m_Idx]);
  m_Insertion := TChiffrage_Commentaire.Create;
  m_Insertion.SetCommentaire(_Text);
  m_Insertion.FromChiffrage := m_Chiffrage;
  m_Chiffrage.InsertObject(Grid_Quantitatif.Row - 1, m_Insertion.Reference, m_Insertion);
  m_Chiffrage.CalculePrixTotal;
  ClearGrid;
  ResetGrid;
  m_Chiffrage.Affiche(Grid_Quantitatif);
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.NouveauProduit;
Var
  m_Idx       : Integer;
  m_Chiffrage : TChiffrage;
  m_Insertion : TChiffrage_Element;
  m_CodeProd  : String;
Begin
  m_Idx := TabChapitres.TabIndex;
  If m_Idx < 0 Then Exit;
  m_Chiffrage := TChiffrage(TabChapitres.Tabs.Objects[m_Idx]);
  If m_Chiffrage = nil Then exit;
  { Si on est sur un produit --> insertion juste avant que pour la meme classe }
{$IfDef TEST}
  If TObject(Grid_Quantitatif.Objects[0, Grid_Quantitatif.Row]) Is TChiffrage_Produit Then Begin
    m_CodeProd := SelectionDunProduitPourInsertChiffrage(TChiffrage_Produit(Grid_Quantitatif.Objects[0, Grid_Quantitatif.Row]).CodeProduit);
     // ChoisirProduit(TChiffrage_Produit(Grid_Quantitatif.Objects[0, Grid_Quantitatif.Row]).CodeProduit, False);
    if m_CodeProd <> '' Then
    if not(Etude.Batiment.IsFabricantVIM) then
        m_Chiffrage.InsereProduit(m_CodeProd, Grid_Quantitatif.Row)
    else
        m_Chiffrage.InsereProduit(m_CodeProd, Grid_Quantitatif.Row - 1);

  End Else
{$EndIf}
  { Sinon choix dans toutes les classes et insertion classe + produit }
  If Grid_Quantitatif.Row > 1 Then Begin
    m_CodeProd := ChoisirProduit('', True);
    If m_CodeProd <> '' Then Begin
//      m_Insertion := m_Chiffrage.InsereClasseAvant(Copy(m_CodeProd, 1, 7), Grid_Quantitatif.Row);
//      m_Chiffrage.InsereProduit(m_CodeProd, m_Chiffrage.IndexOfObject(m_Insertion) + 1);
    if not(Etude.Batiment.IsFabricantVIM) then
        m_Chiffrage.InsereProduit(m_CodeProd, Grid_Quantitatif.Row)
    else
        m_Chiffrage.InsereProduit(m_CodeProd, Grid_Quantitatif.Row - 1);

    End;
  End;
  ClearGrid;
  ResetGrid;
  m_Chiffrage.Affiche(Grid_Quantitatif);
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.NouveauSousTotal;
Var
  m_Idx       : Integer;
  m_Chiffrage : TChiffrage;
  m_Insertion : TChiffrage_SousTotal;
Begin
  m_Idx := TabChapitres.TabIndex;
  If m_Idx < 0 Then Exit;
  m_Chiffrage := TChiffrage(TabChapitres.Tabs.Objects[m_Idx]);
  m_Insertion := TChiffrage_SousTotal.Create;
  m_Insertion.FromChiffrage := m_Chiffrage;
  m_Chiffrage.InsertObject(Grid_Quantitatif.Row - 1, m_Insertion.Reference, m_Insertion);
  m_Chiffrage.CalculePrixTotal;
  ClearGrid;
  ResetGrid;
  m_Chiffrage.Affiche(Grid_Quantitatif);
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.RecalculChiffrage;
Begin               
  If IsNotNil(TabChapitres.Tabs.Objects[TabChapitres.Tabindex]) Then Begin
    ResetGrid;
    TChiffrage(TabChapitres.Tabs.Objects[TabChapitres.Tabindex]).CalculePrixTotal;
    TChiffrage(TabChapitres.Tabs.Objects[TabChapitres.Tabindex]).Affiche(Grid_Quantitatif);
  End;
End;
{ ************************************************************************************************************************************************** }
procedure TDlg_Chiffrage.ResetGrid;
Var
  m_NoColonne : SmallInt;
Begin
  ClearGrid;
  For m_NoColonne := Low(Chiffrage_ColHeaders) To High(Chiffrage_ColHeaders) Do
        if Chiffrage_ColShow[m_NoColonne] then
                Grid_Quantitatif.Cells[GetRealColChiffrage(m_NoColonne), 0] := Chiffrage_ColHeaders[m_NoColonne];
  DimensionneGrid;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.ClearGrid;
Var
  m_NoLigne   : SmallInt;
  m_NoColonne : Smallint;
Begin
  For m_NoLigne := 1 To Grid_Quantitatif.RowCount - 1 Do Begin
    Grid_Quantitatif.Objects[0, m_NoLigne] := Nil;
    For m_NoColonne := 0 To Grid_Quantitatif.ColCount - 1 Do Grid_Quantitatif.Cells[m_NoColonne, m_NoLigne] := '';
  End;
  Grid_Quantitatif.Clear;
  Grid_Quantitatif.RowCount := 4;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.DimensionneGrid;
Var
  UniteGrille : Single;
  i           : smallint;
Begin
  Grid_Quantitatif.ColCount := GetColCountChiffrage;
  UniteGrille := (Grid_Quantitatif.Width - Grid_Quantitatif.ScrollWidth - 1 ) / Grid_Quantitatif.ColCount;
  Grid_Quantitatif.ColWidths[GetRealColChiffrage(Chiffrage_ColObjet)] := Round(UniteGrille / 6);
  Grid_Quantitatif.ColWidths[GetRealColChiffrage(Chiffrage_ColCode)] := Round(UniteGrille / 2);
  Grid_Quantitatif.ColWidths[GetRealColChiffrage(Chiffrage_ColDesignation)] := Round(UniteGrille + 5 / 6 * UniteGrille + (Grid_Quantitatif.ColCount - 2) * 0.5 * UniteGrille);
  For i := 3 To Grid_Quantitatif.ColCount - 1 Do Grid_Quantitatif.ColWidths[i] := round(unitegrille / 2);
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.Suppression;
Var
  m_Chiffrage : TChiffrage;
  m_Element   : TChiffrage_Element;
  m_Idx       : Integer;
  m_Ligne     : Integer;
  m_Message   : String;
Begin
  If LigneValide Then Begin
    m_Idx := TabChapitres.TabIndex;
    m_Chiffrage := TChiffrage(TabChapitres.Tabs.Objects[m_Idx]);
    m_Element   := TChiffrage_Element(Grid_Quantitatif.Objects[0, Grid_Quantitatif.Row]);

    If m_Element Is TChiffrage_Classe Then m_Message := 'la classe ' + TChiffrage_Classe(m_Element).Reference + ' et tous les produits associ�s'
                                       Else m_Message := 'la ligne s�lectionn�e';
    m_Ligne := m_Chiffrage.IndexOfObject(m_Element);
    m_Chiffrage.Delete(m_Ligne);
    If BBS_ShowMessage(PChar('Souhaitez-vous supprimer ' + m_Message + '?'), '', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IdYES Then Begin
      If m_Element Is TChiffrage_Classe Then
        While (m_Ligne < m_Chiffrage.Count) And (m_Chiffrage.Element[m_Ligne] Is TChiffrage_Produit) Do  m_Chiffrage.Delete(m_Ligne);
      FreeAndNil(m_Element);

      ResetGrid;
      TChiffrage(TabChapitres.Tabs.Objects[m_Idx]).CalculePrixTotal;
      TChiffrage(TabChapitres.Tabs.Objects[m_Idx]).Affiche(Grid_Quantitatif);
    End;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.ToolButtonClick(Sender: TObject);
Var
  tempImpressionEnCours : Boolean;
Begin
tempImpressionEnCours := False;
  If Sender.InheritsFrom(TToolButton) Or Sender.InheritsFrom(TMenuItem) Then Case TComponent(Sender).Tag Of
      TagButton_GenerationChiffrage: GenerationChiffrage(False);
      TagButton_ExportExcel        : ExportExcel;
      TagButton_NouveauProduit     : NouveauProduit;
      TagButton_NouveauCommentaite : NouveauCommentaire;
      TagButton_SousTotal          : NouveauSousTotal;
      TagButton_Suppression        : Suppression;
      TagButton_Recalcul           : RecalculChiffrage;
      TagButton_OptionsImp         : ChoixChiffrage; //Regis ChangeOptions(TimerAutoSave);
      98                           : If IsNotNil(TabChapitres.Tabs.Objects[Dlg_Chiffrage.TabChapitres.Tabindex]) Then Begin
                                       Impression_Quantitatif(TChiffrage(TabChapitres.Tabs.Objects[Dlg_Chiffrage.TabChapitres.Tabindex]), tempImpressionEnCours, Self);
                                       //Close;
                                     End;
      97                           : If IsNotNil(TabChapitres.Tabs.Objects[Dlg_Chiffrage.TabChapitres.Tabindex]) Then Begin
                                       Impression_Quantitatif(TChiffrage(TabChapitres.Tabs.Objects[Dlg_Chiffrage.TabChapitres.Tabindex]), tempImpressionEnCours, Self, false);
                                       //Close;
                                     End;
      99                           : Close;
      -888                         : If IsNotNil(TabChapitres.Tabs.Objects[Dlg_Chiffrage.TabChapitres.Tabindex]) Then 
                                       AfficheProduitsNonTrouves((TChiffrage(TabChapitres.Tabs.Objects[Dlg_Chiffrage.TabChapitres.Tabindex]).Erreurs));
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.TabChapitresChange(Sender: TObject);
Begin
  ResetGrid;
  If IsNotNil(TabChapitres.Tabs.Objects[TabChapitres.Tabindex]) Then TChiffrage(TabChapitres.Tabs.Objects[TabChapitres.Tabindex]).Affiche(Grid_Quantitatif);
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.Supprimer1Click(Sender: TObject);
Begin
  Case (Sender as TMenuItem).Tag Of
    1: Suppression_Chiffrage;
    2: Renommer_Chiffrage;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.Grid_QuantitatifKeyPress(Sender: TObject; var Key: Char);
begin
  If Key <> #13 Then Begin
    If (Grid_Quantitatif.Objects[0, Grid_Quantitatif.Row] = Nil) Or
       (Not TObject(Grid_Quantitatif.Objects[0, Grid_Quantitatif.Row]).InheritsFrom(TChiffrage_Produit))
    {$IfDef ANJOS_OPTIMA3D}
        and (Not TObject(Grid_Quantitatif.Objects[0, Grid_Quantitatif.Row]).InheritsFrom(TChiffrage_ProduitNonTrouve))
     {$EndIf}
        Then
        Key := #0
    Else If (TObject(Grid_Quantitatif.Objects[0, Grid_Quantitatif.Row]).InheritsFrom(TChiffrage_Produit)
         {$IfDef ANJOS_OPTIMA3D}
             or TObject(Grid_Quantitatif.Objects[0, Grid_Quantitatif.Row]).InheritsFrom(TChiffrage_ProduitNonTrouve)
         {$EndIf}
        ) And
    {$IfDef ANJOS_OPTIMA3D}
            Not(GetColChiffrage(Grid_Quantitatif.Col) in [Chiffrage_ColDesignation, Chiffrage_ColPrixUnit, Chiffrage_ColQuantite, Chiffrage_ColMO]) Then
    {$Else}
            Not(GetColChiffrage(Grid_Quantitatif.Col) in [Chiffrage_ColQuantite, Chiffrage_ColMO]) Then
    {$EndIf}            
                Key := #0;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.Grid_QuantitatifGetEditorType(Sender: TObject; ACol, ARow: Integer; var AEditor: TEditorType);
Begin
  If (Grid_Quantitatif.Objects[0, Grid_Quantitatif.Row] <> Nil) And
     (
     TObject(Grid_Quantitatif.Objects[0, Grid_Quantitatif.Row]).InheritsFrom(TChiffrage_Produit)
     {$IfDef ANJOS_OPTIMA3D}
     or TObject(Grid_Quantitatif.Objects[0, Grid_Quantitatif.Row]).InheritsFrom(TChiffrage_ProduitNonTrouve)
     {$EndIf}
     ) then
        Begin
        Case GetColChiffrage(Grid_Quantitatif.Col) Of
                Chiffrage_ColObjet              : ;
                Chiffrage_ColCode               : AEditor := edPositiveNumeric;
                Chiffrage_ColDesignation        : AEditor := edNormal;
                Chiffrage_ColPrixUnit           : AEditor := edFloat;
                Chiffrage_ColQuantite           : AEditor := edPositiveNumeric;
                Chiffrage_ColRemise             : AEditor := edPositiveNumeric;
                Chiffrage_ColPCRemise           : AEditor := edPositiveNumeric;
                Chiffrage_ColMO                 : AEditor := edFloat;
                Chiffrage_ColPrixTotal          : AEditor := edFloat;
        End;
        End;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.Grid_QuantitatifCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: String; var Valid: Boolean);
Begin
  If (Grid_Quantitatif.Objects[0, ARow] <> Nil) And
     (
     TObject(Grid_Quantitatif.Objects[0, ARow]).InheritsFrom(TChiffrage_Produit)
     {$IfDef ANJOS_OPTIMA3D}
     or TObject(Grid_Quantitatif.Objects[0, Grid_Quantitatif.Row]).InheritsFrom(TChiffrage_ProduitNonTrouve)
     {$EndIf}     
     ) then
     Begin
        Case GetColChiffrage(Grid_Quantitatif.Col) Of

                Chiffrage_ColObjet       : ;
                Chiffrage_ColCode        : Begin
                                           TChiffrage_Produit(Grid_Quantitatif.Objects[0, ARow]).ParDefaut := False;
                                           TChiffrage_Produit(Grid_Quantitatif.Objects[0, ARow]).CodeProduit := Value;
                                           RecalculChiffrage;
                                           End;
                Chiffrage_ColDesignation : Begin
                                           TChiffrage_Produit(Grid_Quantitatif.Objects[0, ARow]).ParDefaut := False;
                                           TChiffrage_Produit(Grid_Quantitatif.Objects[0, ARow]).Reference := Value;
                                           RecalculChiffrage;
                                           End;
                Chiffrage_ColPrixUnit    : Begin
                                           TChiffrage_Produit(Grid_Quantitatif.Objects[0, ARow]).ParDefaut := False;
                                           TChiffrage_Produit(Grid_Quantitatif.Objects[0, ARow]).Prix := Str2Float(Value);
                                           RecalculChiffrage;
                                           End;
                Chiffrage_ColQuantite    : Begin
                                           TChiffrage_Produit(Grid_Quantitatif.Objects[0, ARow]).ParDefaut := False;
                                           TChiffrage_Produit(Grid_Quantitatif.Objects[0, ARow]).Qte := Str2Float(Value);
                                           RecalculChiffrage;
                                           End;
                Chiffrage_ColRemise      : Begin
                                           TChiffrage_Produit(Grid_Quantitatif.Objects[0, ARow]).ParDefaut := False;
                                           TChiffrage_Produit(Grid_Quantitatif.Objects[0, ARow]).Remise := Str2Int(Value);
                                           RecalculChiffrage;
                                           End;
                Chiffrage_ColPCRemise    : Begin
{
                                           TChiffrage_Produit(Grid_Quantitatif.Objects[0, ARow]).ParDefaut := False;
                                           TChiffrage_Produit(Grid_Quantitatif.Objects[0, ARow]).Remise := Str2Int(Value);
                                           RecalculChiffrage;
}                                           
                                           End;

                Chiffrage_ColMO          : Begin
                                           TChiffrage_Produit(Grid_Quantitatif.Objects[0, ARow]).ParDefaut := False;
                                           TChiffrage_Produit(Grid_Quantitatif.Objects[0, ARow]).Remise := Str2Int(Value);
                                           RecalculChiffrage;
                                           End;
                Chiffrage_ColPrixTotal   : Begin
                                           TChiffrage_Produit(Grid_Quantitatif.Objects[0, ARow]).ParDefaut := False;
                                           TChiffrage_Produit(Grid_Quantitatif.Objects[0, ARow]).PrixTotal := Str2Float(Value);
                                           RecalculChiffrage;
                                           End;
        End;

     End;
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.Grid_QuantitatifRowChanging(Sender: TObject; OldRow, NewRow: Integer; var Allow: Boolean);
Begin
  HighLightButtons(NewRow);
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.PopupMenu_GridPopup(Sender: TObject);
Begin
  HighLightButtons(Grid_Quantitatif.Row);
End;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Chiffrage.FormShow(Sender: TObject);
Begin
if not(Etude.Batiment.IsFabricantVIM) then
begin
  Btn_NewProd.Visible        := True;
  Btn_Recalcule.Visible      := True;
end;

if Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantMVN then
  begin
  Button_Imp.Visible := False;
  Btn_NewSSTotal.Visible := False;
  Insrerunsoustotal1.Visible := False;
  end;

  Align := AlClient;
End;
{ ************************************************************************************************************************************************** }
procedure TDlg_Chiffrage.Btn_BIMetreClick(Sender: TObject);
{$IFDEF BIM_METRE}
begin
  DoExport_BIMetre_Aeraulique(TChiffrage(TabChapitres.Tabs.Objects[TabChapitres.Tabindex]));
{$ELSE}
begin
//
{$ENDIF}
end;
{ ************************************************************************************************************************************************** }

End.
