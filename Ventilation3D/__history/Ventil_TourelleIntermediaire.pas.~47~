Unit Ventil_TourelleIntermediaire;

{$Include 'Ventil_Directives.pas'}

interface
Uses
  Classes, SysUtils, advGrid, BaseGrid, AdvObj, Math,
  Ventil_Types, Ventil_Logement, Ventil_SystemeVMC, Ventil_Collecteur, Ventil_Troncon, Ventil_Utils,
  Ventil_EdibatecProduits,
//  Ventil_Accident,
  Ventil_TronconDessin,
  BBScad_Interface_Aeraulique;

Type
{ ************************************************************************************************************************************************** }
  TVentil_TourelleIntermediaire  = Class(TVentil_TronconDessin_SortieCaisson, IAero_Fredo_TourelleIntermediaire)
    Constructor Create; Override;
  Private
    FDzeta: Real;
  Protected
    Procedure SetTypeSortie(_Value: Integer); Override;
    Function  GetTypeSortie: Integer; Override;
    Procedure Save; Override;
    Procedure GenerationAccidents; Override;
    Procedure MakeLienCalculs; Override;
  Public
    Procedure Affiche(_Grid: TAdvStringGrid); Override;
    Procedure Recupere(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType; Override;
    Procedure PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  HasComboBox(_Ligne: Integer): Boolean; Override;
    Function  AfficheQuestion(_NoQuestion: Integer): Boolean; Override;
    Function GetNumeroSortieVIM : Integer; Override;
    Procedure Load; Override;
    Function GetDzeta : Single;
  End;
{ ************************************************************************************************************************************************** }

Implementation
Uses
  Ventil_Declarations,
  Ventil_Const,
  Ventil_Accident,
  Ventil_Firebird,
  Grids;

{ ****************************************************************   TVentil_TourelleIntermediaire *************************************************************** }
Constructor TVentil_TourelleIntermediaire.Create;
Begin
  Inherited;
  Fdzeta := 1;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleIntermediaire.SetTypeSortie(_Value: Integer);
begin

end;
{ ************************************************************************************************************************************************** }
Function  TVentil_TourelleIntermediaire.GetTypeSortie: Integer;
begin
  Result := c_CaissonSortie_Aspiration;
end;
{ ************************************************************************************************************************************************** }
Function TVentil_TourelleIntermediaire.GetNumeroSortieVIM : Integer;
begin
  Result := 1;
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleIntermediaire.Affiche(_Grid: TAdvStringGrid);
Var
  m_Row      : Integer;
  m_Question : Integer;
  m_Str      : String;
Begin
  Inherited;
  m_Row := GetNbQuestionAfficheTroncon + GetNbQuestionAfficheSortieCaisson  + 1;
  For m_Question := Low(c_LibellesQ_TourelleInterm) To High(c_LibellesQ_TourelleInterm) Do If AfficheQuestion(m_Question) Then Begin
    _Grid.Cells[0, m_Row] := c_LibellesQ_TourelleInterm[m_Question];
    Case m_Question Of
      c_QTourelleInterm_Dzeta : m_Str := Float2Str(FDzeta, 2);
      Else m_Str := '';
    End;
    _Grid.Cells[1, m_Row] := m_Str;
    Inc(m_Row);
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleIntermediaire.Recupere(_Grid: TAdvStringGrid; _Ligne: Integer);
Begin
  If NoQuestion(_Ligne) <= c_NbQ_Troncon + GetNbQuestionAfficheSortieCaisson  Then Inherited
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QTourelleInterm_Dzeta   : FDzeta := _Grid.Floats[1, _Ligne];
  End;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_TourelleIntermediaire.TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType;
Var
  m_NoChoix : Integer;
Begin
  Result := edNone;
  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
    //Parfois le GoEditing saute sans raison...
  _Grid.Options := _Grid.Options + [goEditing];
  If NoQuestion(_Ligne) <= c_NbQ_Troncon + GetNbQuestionAfficheSortieCaisson  Then Result := Inherited TypeEdition(_Grid, _Ligne)
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
   c_QTourelleInterm_Dzeta   : Result := edFloat;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleIntermediaire.PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer);
Var
  m_NoChoix : Integer;
Begin
  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_TourelleIntermediaire.HasComboBox(_Ligne: Integer): Boolean;
Begin
  If NoQuestion(_Ligne) <= c_NbQ_Troncon + GetNbQuestionAfficheSortieCaisson  Then Result := Inherited HasComboBox(_Ligne)
  Else Result := AfficheQuestion(NoQuestion(_Ligne)) And (NoQuestion(_Ligne) In[]);
End;
{ ************************************************************************************************************************************************** }
Function TVentil_TourelleIntermediaire.AfficheQuestion(_NoQuestion: Integer): Boolean;
Begin
  If _NoQuestion <= c_NbQ_Troncon + c_NbQ_SortieCaisson Then Result := False
  Else Case _NoQuestion Of
    c_QTourelleInterm_Dzeta  : Result := True;
    Else Result := False;
  End;

End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleIntermediaire.Load;
begin
  FDzeta := Table_Etude_TourelleIntermediaire.Donnees.FieldByName('DZETA').AsFloat;
  Inherited;
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleIntermediaire.Save;
begin
  Table_Etude_TourelleIntermediaire.Donnees.Insert;
  Table_Etude_TourelleIntermediaire.Donnees.FieldByName('DZETA').AsFloat := FDzeta;
  Inherited;
end;
{ ************************************************************************************************************************************************** }
Function TVentil_TourelleIntermediaire.GetDzeta : Single;
begin
  Result := FDzeta;
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleIntermediaire.GenerationAccidents;
Var
  m_NoTr     : Integer;
  m_Tr       : TVentil_Troncon;
  m_Accident : TVentil_Accident;
Begin
  inherited;
(*
      for m_NoTr := Accidents.Count - 1 downto 0 do
        if Accidents[m_NoTr] <> Nil then
          try
          Accidents[m_NoTr].Destroy;
          except

          end;
    Accidents.Clear;
*)
    m_Accident := TVentil_Accident_TourelleIntermediaire.Create;
    m_Accident.TronconPorteur := Self;
    Accidents.Add(m_Accident);
{
  Inherited;
  For m_NoTr := 0 To FilsCalcul.Count - 1 Do Begin
    m_Tr := FilsCalcul[m_NoTr];
    While (m_Tr.FilsCalcul.Count > 0) And (m_Tr.InheritsFrom(TVentil_Accident_Dessin)) Do
        m_Tr := m_Tr.FilsCalcul[0];
    m_Accident := TVentil_Accident_Plenum.Create;
    m_Accident.TronconPorteur := m_Tr;
    m_Tr.Accidents.Add(m_Accident);
  End;
}
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleIntermediaire.MakeLienCalculs;
begin
  if Parent <> Nil then
    Parent.FilsCalcul.Add(Self);
  Inherited;
end;
{ **************************************************************** \ TVentil_TourelleIntermediaire *************************************************************** }







End.
