Unit Ventil_Bezier;

{$Include 'Ventil_Directives.pas'}

Interface
Uses Classes, Math, Ventil_Utils;

Type
  TPointDebitPression = Record
    Q : Real;
    DP: Real;
  End;

  TPointsControleBezier = Record
    Points: Array Of TPointDebitPression;
    Bezier_11Pts: Boolean;
    Bezier_XPts: Boolean;
    Bezier_NbPts: Integer;
  End;

{ **Points de la courbe de bezier** }
Function  PointDebitPressionBezier_4Points(_x: Double; _PointsDeControle: TPointsControleBezier): TPointDebitPression;
Function  PointDebitPressionBezier_11Points(_x: Double; _PointsDeControle: TPointsControleBezier): TPointDebitPression;
Function  StringToPointsBezier_4points(_Str: String): TPointsControleBezier;
Function  StringToPointsBezier_11points(_ValX, _ValY: TStringList): TPointsControleBezier;
Function  StringToPointsBezier_Xpoints(_ValX, _ValY: TStringList): TPointsControleBezier;
Function  CalculPointBezier_4Points(_PointX: Real; _Courbe: String): Real;
Function  CalculPointBezier(_PointX: Real;_PtsBezier: TPointsControleBezier): Real;
Function  CalculPointBezier_11Points(_PointX: Real; _X: TStringList; _Y: TStringList): Real;
Function  CalculPointBezier_XPoints(_PointX: Real; _X: TStringList; _Y: TStringList): Real;
Procedure EncadrePoint(Const _SurCourbe: TPointsControleBezier; Const _PointAEncadrer: TPointDebitPression; Var _PointAvant, _PointApres: TPointDebitPression);

Implementation


{ ************************************************************************************************************************************************** }
Procedure EncadrePoint(Const _SurCourbe: TPointsControleBezier; Const _PointAEncadrer: TPointDebitPression; Var _PointAvant, _PointApres: TPointDebitPression);
Const
  c_Pas = 0.1;
Var
  m_Loop        : Boolean;
  m_NoPoint     : Integer;
Begin

  m_Loop := True;
  _PointAvant.Q := _SurCourbe.Points[0].Q;
  _PointAvant.DP := _SurCourbe.Points[0].DP;
  _PointApres.Q := _SurCourbe.Points[10].Q;
  _PointApres.DP := _SurCourbe.Points[10].DP;

  m_NoPoint := _SurCourbe.Bezier_NbPts - 1;
  While m_Loop Do Begin
    If _SurCourbe.Points[m_NoPoint].Q < _PointAEncadrer.Q Then Begin
      _PointAvant := _SurCourbe.Points[m_NoPoint];
      m_Loop := False;
    End;
    Dec(m_NoPoint);
    m_Loop := m_Loop And (m_NoPoint >= 0);
  End;

  m_NoPoint := 0;
  m_Loop := True;
  While m_Loop Do Begin
    If _SurCourbe.Points[m_NoPoint].Q > _PointAEncadrer.Q Then Begin
      _PointApres := _SurCourbe.Points[m_NoPoint];
      m_Loop := False;
    End;
    Inc(m_NoPoint);
    m_Loop := m_Loop And (m_NoPoint <= 10);
  End;
End;
{ ************************************************************************************************************************************************** }
Function CalculPointBezier_Xpoints(_PointX: Real; _X: TStringList; _Y: TStringList): Real;
Var
  m_PtsBezier : TPointsControleBezier;
Begin
  m_PtsBezier := StringToPointsBezier_XPoints(_X, _Y);
  Result := CalculPointBezier(_PointX, m_PtsBezier);
End;
{ ************************************************************************************************************************************************** }
Function CalculPointBezier_11points(_PointX: Real; _X: TStringList; _Y: TStringList): Real;
Var
  m_PtsBezier : TPointsControleBezier;
Begin
  m_PtsBezier := StringToPointsBezier_11Points(_X, _Y);
  Result := CalculPointBezier(_PointX, m_PtsBezier);
End;
{ ************************************************************************************************************************************************** }
Function CalculPointBezier_4Points(_PointX: Real; _Courbe: String): Real;
Var
  m_PtsBezier : TPointsControleBezier;
Begin
  m_PtsBezier := StringToPointsBezier_4Points(_Courbe);
  Result := CalculPointBezier(_PointX, m_PtsBezier);
End;
{ ************************************************************************************************************************************************** }
Function CalculPointBezier(_PointX: Real;_PtsBezier: TPointsControleBezier): Real;
Var
  m_Point     : TPointDebitPression;
  m_P1Courbe,
  m_P2Courbe: TPointDebitPression;
  a, b      : Real;
Begin
  m_Point.Q := _PointX;
  EncadrePoint(_PtsBezier, m_Point, m_P1Courbe, m_P2Courbe);
  a := (m_P1Courbe.DP - m_P2Courbe.DP) / (m_P1Courbe.Q - m_P2Courbe.Q);
  b := m_P1Courbe.DP - a * m_P1Courbe.Q;
  Result := a * m_Point.Q + b;
End;
{ ************************************************************************************************************************************************** }
Function PointDebitPressionBezier_11Points(_x: Double; _PointsDeControle: TPointsControleBezier): TPointDebitPression;
Var
  t : double;
Begin
  t := 1 - _x;
  With _PointsDeControle Do Begin
    Result.Q := Points[0].Q * Power(t, 10) +
                10  * Points[1].Q * _x * Power(t, 9) +
                45  * Points[2].Q * Power(_x, 2) * Power(t, 8) +
                120 * Points[3].Q * Power(_x, 3) * Power(t, 7) +
                210 * Points[4].Q * Power(_x, 4) * Power(t, 6) +
                252 * Points[5].Q * Power(_x, 5) * Power(t, 5) +
                210 * Points[6].Q * Power(_x, 6) * Power(t, 4) +
                120 * Points[7].Q * Power(_x, 7) * Power(t, 3) +
                 45 * Points[8].Q * Power(_x, 8) * Power(t, 2) +
                 10 * Points[9].Q * Power(_x, 9) * t           +
                 Points[10].Q * Power(_x, 10);

    Result.DP := Points[0].DP * Power(t, 10) +
                  10  * Points[1].DP * _x * Power(t, 9) +
                  45  * Points[2].DP * Power(_x, 2) * Power(t, 8) +
                 120 * Points[3].DP * Power(_x, 3) * Power(t, 7) +
                 210 * Points[4].DP * Power(_x, 4) * Power(t, 6) +
                 252 * Points[5].DP * Power(_x, 5) * Power(t, 5) +
                 210 * Points[6].DP * Power(_x, 6) * Power(t, 4) +
                 120 * Points[7].DP * Power(_x, 7) * Power(t, 3) +
                  45 * Points[8].DP * Power(_x, 8) * Power(t, 2) +
                  10 * Points[9].DP * Power(_x, 9) * t           +
                  Points[10].DP * Power(_x, 10);
  End;
End;
{ ************************************************************************************************************************************************** }
Function PointDebitPressionBezier_4Points(_x: Double; _PointsDeControle: TPointsControleBezier): TPointDebitPression;
Var
  m_t : Double;
Begin
  m_t := 1.00 - _x;
  Result.Q := 1.00 * (( Power(m_t, 3) * _PointsDeControle.Points[0].Q) + (3 * _x * _PointsDeControle.Points[1].Q * Power(m_t, 2)) + (3 * Power(_x, 2) * m_t * _PointsDeControle.Points[3].Q) +
              (Power(_x, 3) * _PointsDeControle.Points[2].Q));

  Result.DP := 1.00 * (( Power(m_t, 3) * _PointsDeControle.Points[0].DP) + (3 * _x * _PointsDeControle.Points[1].DP * Power(m_t, 2)) + (3 * Power(_x, 2) * m_t * _PointsDeControle.Points[3].DP) +
             (Power(_x, 3) * _PointsDeControle.Points[2].DP));
End;
{ ************************************************************************************************************************************************** }
Function  StringToPointsBezier_4points(_Str: String): TPointsControleBezier;
Begin
  Result.Bezier_11Pts := False;
  Result.Bezier_XPts := False;
  Result.Bezier_NbPts := 4;
  SetLength(Result.Points, 4);
  Result.Points[0].Q  := Str2Float(GetSubStr(_Str, 1, '&'));
  Result.Points[0].DP := Str2Float(GetSubStr(_Str, 2, '&'));
  Result.Points[1].Q  := Str2Float(GetSubStr(_Str, 3, '&'));
  Result.Points[1].DP := Str2Float(GetSubStr(_Str, 4, '&'));
  Result.Points[2].Q  := Str2Float(GetSubStr(_Str, 5, '&'));
  Result.Points[2].DP := Str2Float(GetSubStr(_Str, 6, '&'));
  Result.Points[3].Q  := Str2Float(GetSubStr(_Str, 7, '&'));
  Result.Points[3].DP := Str2Float(GetSubStr(_Str, 8, '&'));
End;
{ ************************************************************************************************************************************************** }
Function  StringToPointsBezier_11points(_ValX, _ValY: TStringList): TPointsControleBezier;
Var
  m_NoPoint: Integer;
Begin
  Result.Bezier_11Pts := True;
  Result.Bezier_XPts := False;
  Result.Bezier_NbPts := 11;
  SetLength(Result.Points, 11);
  For m_NoPoint := 0 To 10 Do Begin
    Result.Points[m_NoPoint].Q  := Str2Float(_Valx[m_NoPoint]);
    Result.Points[m_NoPoint].DP := Str2Float(_ValY[m_NoPoint]);
  End;
End;
{ ************************************************************************************************************************************************** }
Function  StringToPointsBezier_Xpoints(_ValX, _ValY: TStringList): TPointsControleBezier;
Var
  m_NoPoint: Integer;
Begin
  Result.Bezier_11Pts := False;
  Result.Bezier_XPts := True;
  Result.Bezier_NbPts := _Valx.Count;
  SetLength(Result.Points, _Valx.Count);
  For m_NoPoint := 0 To _Valx.Count - 1 Do Begin
    Result.Points[m_NoPoint].Q  := Str2Float(_Valx[m_NoPoint]);
    Result.Points[m_NoPoint].DP := Str2Float(_ValY[m_NoPoint]);
  End;
End;
{ ************************************************************************************************************************************************** }

End.
