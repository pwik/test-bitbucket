unit Ventil_Registre;
           
{$Include 'Ventil_Directives.pas'}

interface
Uses
  Classes, SysUtils, advGrid, BaseGrid, AdvObj, Math,
  Ventil_Types, Ventil_Logement, Ventil_SystemeVMC, Ventil_Collecteur, Ventil_Troncon, Ventil_Utils,
  BBScad_Interface_Aeraulique,
  Ventil_EDIBATECProduits;

Type
{ ************************************************************************************************************************************************** }
  TVentil_Registre  = Class(TVentil_Troncon)
//  TVentil_Registre  = Class(TVentil_Accident)
    Constructor Create; Override;
  Private
    FProduit : TEdibatec_Registre;
    FPDC_Saisi : Integer;
    FAngle : Integer;
    Function GetDzetaGaz : Real;
    Function GetDzetaStandard : Real;
  Protected
    class Function NomParDefaut : String; Override;
    Procedure Save; Override;
    Procedure CalculerQuantitatifInternal; Override;
    Procedure IAero_Fredo_Base_RemplacerPar(Objet : IAero_Fredo_Base); Override;
    Function  ImageAssociee: String; Override;
    Function IsEtiquetteVisible : boolean; Override;
    Procedure InitEtiquette; Override;
    Procedure SelectionneDiametre_SelonMethode; Override;
    Procedure GenerationAccidents; Override;
  Public
    class Function FamilleObjet : String; Override;
    Procedure Load; Override;
    Procedure CalculeRecapitulatif(_Iteratif : Boolean); Override;
    Procedure CalculeDiametreDebitTertiaire(_Iteratif : Boolean); Override;
    Procedure CalculeDiametreDebitCollectif(_Iteratif : Boolean); Override;
    Procedure Paste(_Source: TVentil_Objet); Override;
    Procedure Affiche(_Grid: TAdvStringGrid); Override;
    Procedure AfficheResultats(_Grid: TAdvStringGrid); Override;
    Procedure Recupere(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType; Override;
    Procedure PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  AfficheQuestion(_NoQuestion: Integer): Boolean; Override;
    Function  HasComboBox(_Ligne: Integer): Boolean; Override;

    Function GetReglage(_ValDP : Real) : Real;
    Function GetDzeta : Real;

    Property RegistreSelection : TEdibatec_Registre read FProduit write FProduit;
  End;

{ ************************************************************************************************************************************************** }
Implementation
Uses
  Grids,
  Ventil_Accident,
  Ventil_Edibatec,
  Ventil_FIREBIRD, Ventil_Const,
  Ventil_Caisson, Ventil_Bifurcation,
  Ventil_Bouche, Ventil_SortieToiture,
  Ventil_Declarations,
  Ventil_EdibatecChoixProduit, Ventil_Options;


{ ****************************************************************   TVentil_Registre *************************************************************** }
Constructor TVentil_Registre.Create;
Begin
  Inherited;
  NbQuestions := NbQuestions + c_NbQ_Registre;
  FPDC_Saisi := 0;
  FAngle := 0;
//  AutoGenere := false;
//  Dzeta := 1;
End;
{ ************************************************************************************************************************************************** }
class Function TVentil_Registre.NomParDefaut : String;
Begin
Result := 'Registre';
End;
{ ************************************************************************************************************************************************** }
class Function TVentil_Registre.FamilleObjet : String;
Begin
Result := 'Registre';
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Registre.Affiche(_Grid: TAdvStringGrid);
Var
  m_Row      : Integer;
  m_Question : Integer;
  m_Str      : String;
Begin
  Inherited;
  m_Row := GetNbQuestionAfficheTroncon + 1;
  For m_Question := Low(c_LibellesQ_Registre) To High(c_LibellesQ_Registre) Do If AfficheQuestion(m_Question) Then Begin
    _Grid.Cells[0, m_Row] := c_LibellesQ_Registre[m_Question];
    Case m_Question Of
      c_QRegistre_PDC       : m_Str := IntToStr(FPDC_Saisi);
      c_QRegistre_Angle     : m_Str := IntToStr(FAngle);
      Else m_Str := '';
    End;
    _Grid.Cells[1, m_Row] := m_Str;
    Inc(m_Row);
  End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Registre.AfficheQuestion(_NoQuestion: Integer): Boolean;
Begin
  If _NoQuestion <= c_NbQ_Troncon Then Result := Inherited AfficheQuestion(_NoQuestion)
  Else Case _NoQuestion Of
    c_QRegistre_PDC        : Result := False;//Etude.Batiment.IsFabricantMVN;
    c_QRegistre_Angle      : Result := Etude.Batiment.IsFabricantMVN;
    Else Result := False;
  End;
end;
{ ************************************************************************************************************************************************** }
Function  TVentil_Registre.HasComboBox(_Ligne: Integer): Boolean;
Begin
  Result := Inherited HasComboBox(_Ligne)
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Registre.AfficheResultats(_Grid: TAdvStringGrid);
Begin
  inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Registre.TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType;
Begin
  Result := edNone;
  if Etude.Batiment.IsFabricantVIM then
  _Grid.Combobox.DropDownCount := Max(Etude.Batiment.Logements.Count, 8);
  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
  //Parfois le GoEditing saute sans raison...
  _Grid.Options := _Grid.Options + [goEditing];
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Result := Inherited TypeEdition(_Grid, _Ligne)
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QRegistre_PDC   : Result := edPositiveNumeric;
    c_QRegistre_Angle : Result := edPositiveNumeric;
  End;

if Not(Etude.Batiment.IsFabricantVIM) then
 _Grid.Combobox.DropDownCount := Min(_Grid.Combobox.DropDownCount, 20);

End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Registre.PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer);
Begin
Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Registre.Recupere(_Grid: TAdvStringGrid; _Ligne: Integer);
Begin
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Inherited
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QRegistre_PDC                    : FPDC_Saisi := _Grid.Ints[1, _Ligne];
    c_QRegistre_Angle                  : begin
                                         FAngle := _Grid.Ints[1, _Ligne];
                                         FAngle := Min(FAngle, 90);
                                         end;
  End;

  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Registre.Save;
Begin
  Table_Etude_Registre.Donnees.Insert;
  Table_Etude_Registre.Donnees.FieldByName('ID_REGISTRE').AsWideString := GUIDToString(Id);
  {$IfDef AERAU_CLIMAWIN}
  Table_Etude_Registre.Donnees.FieldByName('IDRESEAU').AsWideString := Etude.IdReseau;
  {$EndIf}
  If FProduit <> nil Then Table_Etude_Registre.Donnees.FieldByName('CODE_REGISTRE').AsWideString := FProduit.CodeProduit
                            Else Table_Etude_Registre.Donnees.FieldByName('CODE_REGISTRE').AsWideString := '';
  Table_Etude_Registre.Donnees.FieldByName('PDC_SAISI').AsInteger := FPDC_Saisi;
  Table_Etude_Registre.Donnees.FieldByName('ANGLE').AsInteger := FAngle;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Registre.Load;
Var
  m_Str : String;
Begin
  m_Str         := Table_Etude_Registre.Donnees.FieldByName('CODE_REGISTRE').AsWideString;
  If m_Str <> '' Then FProduit := TEdibatec_Registre(BaseEdibatec.Produit(m_Str))
                 Else FProduit := nil;
  m_Str         := Table_Etude_Registre.Donnees.FieldByName('PDC_SAISI').AsWideString;
  If m_Str <> '' Then FPDC_Saisi := Table_Etude_Registre.Donnees.FieldByName('PDC_SAISI').AsInteger
                 Else FPDC_Saisi := 0;
  m_Str         := Table_Etude_Registre.Donnees.FieldByName('ANGLE').AsWideString;
  If m_Str <> '' Then FAngle := Table_Etude_Registre.Donnees.FieldByName('ANGLE').AsInteger
                 Else FAngle := 0;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Registre.CalculeDiametreDebitCollectif(_Iteratif : Boolean);
Begin
  Inherited;
  SelectionneDiametre_SelonMethode;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Registre.CalculeRecapitulatif(_Iteratif : Boolean);
var
  cptTemperature : TTemperatureCalc;
  RegistrePdcADD : Integer;
Begin
Inherited;

  if (Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantMVN) and Etude.Batiment.SystemeCollectif.BassePression then
    begin
      if Etude.Batiment.IsFabricantMVN then
        RegistrePdcADD := FPDC_Saisi
      else
        RegistrePdcADD := 15;
    end
  else
    RegistrePdcADD := 150;

for cptTemperature := Low(TTemperatureCalc) to High(TTemperatureCalc) do
begin
DeltaP[cptTemperature].MaxAQmax := DeltaP[cptTemperature].MaxAQmax + RegistrePdcADD;
DeltaP[cptTemperature].MaxAQmin := DeltaP[cptTemperature].MaxAQmin + (RegistrePdcADD * DebitMini / power(DebitMaxi, 2));
end;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Registre.CalculeDiametreDebitTertiaire(_Iteratif : Boolean);
Begin
  Inherited;
  SelectionneDiametre_SelonMethode;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Registre.Paste(_Source: TVentil_Objet);
Begin
  Inherited;
  FProduit                 := TVentil_Registre(_Source).FProduit;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Registre.IAero_Fredo_Base_RemplacerPar(Objet : IAero_Fredo_Base);
Begin
inherited;
TVentil_Registre(Objet.IAero_Fredo_Base_Objet).FProduit := FProduit;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Registre.CalculerQuantitatifInternal;
Var
  m_Classe : TEdibatec_Classe;
  m_Gamme  : TEdibatec_Gamme;
  m_NoGam  : Integer;
  m_NoProd : Integer;
  m_ProduitRegistre : TEdibatec_Registre;
Begin
Inherited;

FProduit := nil;
//m_ProduitRegistre := nil;

m_Classe := BaseEdibatec.Classe(c_Edibatec_Registre);
        For m_NoGam := 0 To m_Classe.Gammes.Count - 1 Do
                Begin
                m_Gamme := m_Classe.Gammes.Gamme(m_NoGam);
                        For m_NoProd := 0 To m_Gamme.Produits.Count - 1 Do
                                Begin
                                m_ProduitRegistre := TEdibatec_Registre(m_Gamme.Produits.Produit(m_NoProd));
                                        If (m_ProduitRegistre.Diametre = Diametre) Then
                                                Begin
                                                FProduit := m_ProduitRegistre;
                                                        If SurTerrasse Then
                                                                AddChiffrage(m_ProduitRegistre, 1, cst_Horizontal, cst_Chiff_AC)
                                                        Else
                                                                AddChiffrage(m_ProduitRegistre, 1, cst_Vertical, cst_Chiff_AC);
                                                exit;
                                                End;
                                End;
                End;



AddChiffrageErreur('Registre', ''{$IfDeF VIM_OPTAIR}, inttostr(1) + ' unit�'{$EndIf});

End;
{ ************************************************************************************************************************************************** }
Function TVentil_Registre.ImageAssociee: String;
Begin
  Result := 'Registre.jpg';
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Registre.IsEtiquetteVisible : boolean;
Begin
//Result := Options.AffEtiq_Silenc;
Result := False;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Registre.InitEtiquette;
Begin
FEtiquette := Reference;
Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Registre.SelectionneDiametre_SelonMethode;
Begin
Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Registre.GenerationAccidents;
Var
  m_NoTr     : Integer;
  m_Tr       : TVentil_Troncon;
  m_Accident : TVentil_Accident;
Begin
  inherited;
      for m_NoTr := Accidents.Count - 1 downto 0 do
        if Accidents[m_NoTr] <> Nil then
          try
          Accidents[m_NoTr].Destroy;
          except

          end;
    Accidents.Clear;
    m_Accident := TVentil_Accident_Registre.Create;
    m_Accident.TronconPorteur := Self;
    Accidents.Add(m_Accident);
End;
{ ************************************************************************************************************************************************** }
function TVentil_Registre.GetReglage(_ValDP : Real) : Real;
var
        _Solutions : TStringList;
        i          : Integer;
        _Dzeta     : Real;
Begin
Result := -100;

        If FProduit = nil then
                Exit;

_Dzeta := _ValDP / (0.6 * sqr(Vitesse));

_Solutions := TStringList.Create;

GetSolutions3emeDegreDansR(_Solutions, FProduit.Coef_A, FProduit.Coef_B, FProduit.Coef_C, FProduit.Coef_D - _Dzeta);

        if _Solutions.Count > 0 then
                for i := 0 To _Solutions.Count - 1 do
                        if (Str2Float(_Solutions[i]) >= FProduit.ReglageMini) and (Str2Float(_Solutions[i]) <= FProduit.ReglageMaxi) then
                                Begin
                                Result :=Str2Float(_Solutions[i]);
                                break;
                                End;
FreeAndNil(_Solutions);

End;
{ ************************************************************************************************************************************************** }
Function TVentil_Registre.GetDzetaGaz : Real;
begin
  Result := 9.32011539138834 * Power(10, -6) * Power(FAngle, 4)
            - 7.12622501224416 * Power(10, -20) * Power(FAngle, 3)
            + 0.010039408976099 * Power(FAngle, 2)
            - 1.22716853296872 * Power(10, -15) * FAngle;
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Registre.GetDzetaStandard : Real;
begin
  Result := 1.17367185142242 * Power(10, -6) * Power(FAngle, 4)
            + 7.39594146887398 * Power(10, -22) * Power(FAngle, 3)
            + 0.00211645147766 * Power(FAngle, 2)
            - 1.5288045407748 * Power(10, -16) * FAngle;
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Registre.GetDzeta : Real;
begin
  if TronconGaz then
    Result := GetDzetaGaz
  else
    Result := GetDzetaStandard;
end;
{ ************************************************************************************************************************************************** }
{ **************************************************************** \ TVentil_Registre *************************************************************** }







End.
