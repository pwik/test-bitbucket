unit Ventil_Silencieux;

{$Include 'Ventil_Directives.pas'}

interface
Uses
  Classes, SysUtils, advGrid, BaseGrid, AdvObj, Math,
  Ventil_Types, Ventil_Logement, Ventil_SystemeVMC, Ventil_Collecteur, Ventil_Troncon, Ventil_Utils,
  BBScad_Interface_Aeraulique,
  Ventil_EDIBATECProduits;

Type
{ ************************************************************************************************************************************************** }
//  TVentil_Silencieux  = Class(TVentil_Troncon)
  TVentil_Silencieux  = Class(TVentil_Accident, IAero_Fredo_Silencieux)
    Constructor Create; Override;
  Private
    FProduit     : TEdibatec_Silencieux;
    Function  SelectionSilencieux: String;
    Function  GetSilencieuxAChiffrer: TEdibatec_Silencieux;
  Protected
    Procedure Save; Override;
    Procedure CalculerQuantitatifInternal; Override;
    Function  ImageAssociee: String; Override;
    Function IsEtiquetteVisible : boolean; Override;
    Procedure InitEtiquette; Override;
    Procedure SelectionneDiametre_SelonMethode; Override;
  Public
    SilencieuxSelection: TEdibatec_Silencieux;
    Property SilencieuxAChiffrer : TEdibatec_Silencieux read GetSilencieuxAChiffrer;
    Procedure Load; Override;
    Procedure CalculeDiametreDebitTertiaire(_Iteratif : Boolean); Override;
    Procedure CalculeDiametreDebitCollectif(_Iteratif : Boolean); Override;
    Procedure Paste(_Source: TVentil_Objet); Override;
    Procedure Affiche(_Grid: TAdvStringGrid); Override;
    Procedure AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer); Override;
    Procedure Recupere(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType; Override;
    Procedure PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  AfficheQuestion(_NoQuestion: Integer): Boolean; Override;
    Function  EllipsClick(_Grid: TAdvStringGrid; _Ligne: Integer): String; Override;
    Function  HasComboBox(_Ligne: Integer): Boolean; Override;
    Function  CalculeDzetaMini: Real; Override;
    Function  CalculeDzetaMaxi: Real; Override;

    Procedure MajAssociation;
    Procedure SetAssociationDefaut(_Association: TEdibatec_Association; _QteDefaut: Integer);
    Function IAero_Fredo_Silencieux_GetSpectreAcoustique : TBBSCad_TabAcoustique;
  End;

{ ************************************************************************************************************************************************** }
Implementation
Uses
  Grids,
  BbsgFonc,
  Ventil_Edibatec,
  Ventil_FIREBIRD, Ventil_Const,
  Ventil_Caisson, Ventil_Bifurcation,
  Ventil_Bouche, Ventil_SortieToiture,
  Ventil_Declarations,
  Ventil_EdibatecChoixProduit, Ventil_Etude;


{ ****************************************************************   TVentil_Silencieux *************************************************************** }
Constructor TVentil_Silencieux.Create;
Begin
  Inherited;
  NbQuestions := NbQuestions + c_NbQ_Silencieux;
  Reference      := 'Silencieux';  
  AutoGenere := false;
  DzetaMaxi := 1;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Silencieux.Affiche(_Grid: TAdvStringGrid);
Var
  m_Row      : Integer;
  m_Question : Integer;
  m_Str      : String;
Begin
  Inherited;
  m_Row := GetNbQuestionAfficheTroncon + 1;
  For m_Question := Low(c_LibellesQ_Silencieux) To High(c_LibellesQ_Silencieux) Do If AfficheQuestion(m_Question) Then Begin
    _Grid.Cells[0, m_Row] := c_LibellesQ_Silencieux[m_Question];
    Case m_Question Of
      c_QSilencieux_Selection       : If SilencieuxSelection <> nil Then m_Str := SilencieuxSelection.Reference
                                                       Else m_Str := ' *** ';
      c_QSilencieux_Dzeta           : m_Str := Float2Str(DzetaMaxi, 2);
      Else m_Str := '';
    End;
    _Grid.Cells[1, m_Row] := m_Str;
    Inc(m_Row);
  End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Silencieux.AfficheQuestion(_NoQuestion: Integer): Boolean;
Begin
  If _NoQuestion <= c_NbQ_Troncon Then Result := Inherited AfficheQuestion(_NoQuestion)
  Else Case _NoQuestion Of
    c_QSilencieux_Selection  : {$IfDef AERAU_CLIMAWIN}Result := False;{$Else}Result := True;{$EndIf}
    c_QSilencieux_Dzeta  : Result := True;
    Else Result := False;
  End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Silencieux.EllipsClick(_Grid: TAdvStringGrid; _Ligne: Integer): String;
Begin
  Case NoQuestion(_Ligne) Of
    c_QSilencieux_Selection : if not(Etude.Batiment.IsFabricantVIM) then Result := SelectionSilencieux;
  End;
  Recupere(_Grid, _Ligne);
End;
{ ************************************************************************************************************************************************** }
Function  TVentil_Silencieux.HasComboBox(_Ligne: Integer): Boolean;
Begin
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Result := Inherited HasComboBox(_Ligne)
  Else Result := AfficheQuestion(NoQuestion(_Ligne)) And (NoQuestion(_Ligne) In[]);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Silencieux.AfficheResultats(_Grid: TAdvStringGrid; _ColDep, _Row : Integer);
Begin
  If isNotNil(Etude.GridProp.Objects[0, 0]) And (Etude.GridProp.Objects[0, 0] <> Self) Then Begin
    _Grid.Cells[_ColDep, _Row] := 'Nom silencieux choisi';
  End;
  inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Silencieux.CalculeDzetaMaxi: Real;
Begin
  Result := DzetaMaxi;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Silencieux.CalculeDzetaMini: Real;
Begin
  Result := DzetaMini;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Silencieux.MajAssociation;
Var
  m_NoAsso : Integer;
Begin
  If FProduit <> nil Then For m_NoAsso := 0 To Associations.Count - 1 Do If TEdibatec_Association(Associations.Produit(m_NoAsso)).Defaut Then
    TEdibatec_Association(Associations.Produit(m_NoAsso)).Qte := TEdibatec_Association(FProduit.Associations.Produit(m_NoAsso)).Qte;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Silencieux.SetAssociationDefaut(_Association: TEdibatec_Association; _QteDefaut: Integer);
Begin
  If FProduit <> Nil Then TEdibatec_Association(FProduit.Associations.Produit(_Association.CodeProduit)).Qte := _QteDefaut;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Silencieux.TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType;
Begin

  Result := edNone;
  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
  //Parfois le GoEditing saute sans raison...
  _Grid.Options := _Grid.Options + [goEditing];
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Result := Inherited TypeEdition(_Grid, _Ligne)
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QSilencieux_Selection:if not(Etude.Batiment.IsFabricantVIM) then Result := edEditBtn;
    c_QSilencieux_Dzeta  : Result := edFloat;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Silencieux.PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer);
Begin

  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Inherited
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then
  begin
  {
    Case NoQuestion(_Ligne) Of

    End;
  }
  end;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Silencieux.Recupere(_Grid: TAdvStringGrid; _Ligne: Integer);
Begin

  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Inherited
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QSilencieux_Selection   : ; // Recuperation dans le ellipsclick
    c_QSilencieux_Dzeta  : DzetaMaxi := _Grid.Floats[1, _Ligne];
  End;
  Inherited;
  GetSilencieuxAChiffrer;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Silencieux.Save;
Begin
  Table_Etude_Silencieux.Donnees.Insert;
  Table_Etude_Silencieux.Donnees.FieldByName('ID_SILENCIEUX').AsWideString := GUIDToString(Id);
  {$IfDef AERAU_CLIMAWIN}
  Table_Etude_Silencieux.Donnees.FieldByName('IDRESEAU').AsWideString := Etude.IdReseau;
  {$EndIf}
  Table_Etude_Silencieux.Donnees.FieldByName('DZETA').AsFloat := DzetaMaxi;
  If SilencieuxSelection <> nil Then Table_Etude_Silencieux.Donnees.FieldByName('CODE_SILENCIEUX').AsWideString := SilencieuxSelection.CodeProduit
                            Else Table_Etude_Silencieux.Donnees.FieldByName('CODE_SILENCIEUX').AsWideString := '';
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Silencieux.Load;
Var
  m_Str : String;
Begin
  DzetaMaxi := Table_Etude_Silencieux.Donnees.FieldByName('DZETA').AsFloat;
  m_Str         := Table_Etude_Silencieux.Donnees.FieldByName('CODE_SILENCIEUX').AsWideString;
  If m_Str <> '' Then SilencieuxSelection := TEdibatec_Silencieux(BaseEdibatec.Produit(m_Str))
                 Else SilencieuxSelection := nil;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Silencieux.CalculeDiametreDebitCollectif(_Iteratif : Boolean);
Begin
  Inherited;
  GetSilencieuxAChiffrer;
  SelectionneDiametre_SelonMethode;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Silencieux.CalculeDiametreDebitTertiaire(_Iteratif : Boolean);
Begin
  Inherited;
  GetSilencieuxAChiffrer;
  SelectionneDiametre_SelonMethode;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Silencieux.Paste(_Source: TVentil_Objet);
Begin
  Inherited;
  NatureGaine              := TVentil_Silencieux(_Source).NatureGaine;
  HasRegulateur            := TVentil_Silencieux(_Source).HasRegulateur;
  SilencieuxSelection      := TVentil_Silencieux(_Source).SilencieuxSelection;
  DzetaMaxi                    := TVentil_Silencieux(_Source).DzetaMaxi;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Silencieux.SelectionSilencieux: String;
Var
  m_CodeProduit : String;
Begin
  If IsNotNil(SilencieuxSelection) Then m_CodeProduit := SilencieuxSelection.CodeProduit
                               Else m_CodeProduit := 'SILENCA';
  m_CodeProduit := SelectionDunProduit(m_CodeProduit);
  If m_CodeProduit <> '' Then Begin
    SilencieuxSelection :=  TEdibatec_Silencieux(BaseEdibatec.Produit(m_CodeProduit));
    Result := SilencieuxSelection.Reference;
//    LienCad.IAero_Vilo_Base_SetTextEtiquette(IntToStr(RegulateurDebitSelection.QMaxi) + ' / ' + IntToStr(RegulateurDebitSelection.QMini));
  End Else Result := ' *** ';
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Silencieux.GetSilencieuxAChiffrer: TEdibatec_Silencieux;
Var
  m_SilencieuxEdibatec : TEdibatec_Silencieux;
Begin
    m_SilencieuxEdibatec := SilencieuxSelection;
  If IsNotNil(m_SilencieuxEdibatec) Then Begin
    If (FProduit <> m_SilencieuxEdibatec) Then Begin
      FProduit := m_SilencieuxEdibatec;
      m_SilencieuxEdibatec.GetAssociation;
      Associations.InitFrom(m_SilencieuxEdibatec.Associations);
    End;
  End Else Begin
    Associations.Clear;
    FProduit := nil;
  End;
  Result := m_SilencieuxEdibatec;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Silencieux.CalculerQuantitatifInternal;
Var
  m_SilencieuxEdibatec : TEdibatec_Silencieux;
  m_Matiere            : integer;
Begin
  m_SilencieuxEdibatec := GetSilencieuxAChiffrer;
  Inherited;
  If IsNotNil(m_SilencieuxEdibatec) Then
        Begin
                if SurTerrasse then
                        AddChiffrage(m_SilencieuxEdibatec, 1, cst_Horizontal, cst_Chiff_AC)
                else
                        AddChiffrage(m_SilencieuxEdibatec, 1, cst_Vertical, cst_Chiff_AC);
        End
  Else
        Begin
        m_Matiere := GetMatiereAccessoire;
        AddChiffrageErreur('Silencieux',  IntToStr(Diametre) + ' mm ' + c_Matiere[m_Matiere]{$IfDeF VIM_OPTAIR}, inttostr(1) + ' unit�'{$EndIf});
        End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Silencieux.ImageAssociee: String;
Begin
  Result := 'Silencieux.jpg';
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Silencieux.IsEtiquetteVisible : boolean;
Begin
Result := Options.AffEtiq_Silenc;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Silencieux.InitEtiquette;
Begin
FEtiquette := Reference;
Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Silencieux.SelectionneDiametre_SelonMethode;
{$IfNDef AERAU_CLIMAWIN}
Var
  m_Matiere : Integer;
{$EndIf}
Begin
Inherited;


{$IfNDef AERAU_CLIMAWIN}
if Etude.Batiment.IsFabricantVIM then
Begin

SilencieuxSelection := nil;

m_Matiere := GetMatiereAccessoire;

Case m_Matiere of
        c_Alu      : m_Matiere := 1;
        c_Galva    : m_Matiere := 2;
//        c_VeloDuct : m_Matiere := 3;
        c_VeloDuct : m_Matiere := 2;
End;

        If not(TronconGaz) and (Etude.Batiment.MatiereAccessoires = c_AccessoiresAutre) Then
//                m_Matiere := 3;
                m_Matiere := 2;

m_Classe := BaseEdibatec.Classe(c_Edibatec_Silencieux);
        For m_NoGam := 0 To m_Classe.Gammes.Count - 1 Do
                Begin
                m_Gamme := m_Classe.Gammes.Gamme(m_NoGam);
                        For m_NoProd := 0 To m_Gamme.Produits.Count - 1 Do
                                Begin
                                m_ProduitSilencieux := TEdibatec_Silencieux(m_Gamme.Produits.Produit(m_NoProd));
                                        If (m_ProduitSilencieux.Diametre = Diametre) and (m_ProduitSilencieux.Matiere = m_Matiere) Then
                                                Begin
                                                SilencieuxSelection := m_ProduitSilencieux;
                                                exit;
                                                End;
                                End;
                End;

End;
{$EndIf}
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Silencieux.IAero_Fredo_Silencieux_GetSpectreAcoustique : TBBSCad_TabAcoustique;
Var
  m_Tab : TBBSCad_TabAcoustique;
  m_Ind : Integer;
  m_SilencieuxEdibatec : TEdibatec_Silencieux;
Begin
m_SilencieuxEdibatec := GetSilencieuxAChiffrer;
if (m_SilencieuxEdibatec <> nil) and (m_SilencieuxEdibatec.SpectreAcoustique.Count >= 7) then
        For m_Ind := Low(m_Tab) To High(m_Tab) Do m_Tab[m_Ind] := StrToFloat(m_SilencieuxEdibatec.SpectreAcoustique[m_Ind - 1])
else
        For m_Ind := Low(m_Tab) To High(m_Tab) Do m_Tab[m_Ind] := 0;
  Result := m_Tab;
End;
{ ************************************************************************************************************************************************** }
{ **************************************************************** \ TVentil_Silencieux *************************************************************** }







End.
