Unit Ventil_Debug;
                     
{$Include 'Ventil_Directives.pas'}

Interface

Uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, VirtualTrees,

  Ventil_Troncon,
  Ventil_Types, ComCtrls, ToolWin, ImgList;

Type
  TDlg_Debug = class(TForm)
    ArbreDebug: TVirtualStringTree;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ImageList1: TImageList;
    ImageList4: TImageList;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
{$if CompilerVersion>20}
    procedure ArbreDebugGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var CellText: String);
{$elseif CompilerVersion<20}
    procedure ArbreDebugGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var CellText: WideString);
{$ifend}
    procedure ArbreDebugChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
  private
    ModeCalcul : Boolean;
    Procedure FillTree;
    Procedure AddNode(_ParentNode: PVirtualNode; _Tr: TVentil_Troncon);
    Function  arbredebugNode(_Node: PVirtualNode): TArbreBatimentNode;
    Function  arbredebugNodeData(_Node: PVirtualNode): TVentil_Objet;
  public
    { Public declarations }
  end;

Var
  Dlg_Debug: TDlg_Debug;

Procedure ShowDebug;

Implementation
Uses
  Ventil_Utils, Ventil_Declarations,
  {$IfDef AERAU_CLIMAWIN}
        XtAeraulique
  {$ELSE}
    {$IfDef VIM_OPTAIR}
    {$ELSE}
        {$IfDef FRANCEAIR_COLLECTAIR}
                COLLECTAIR_ETUDE
        {$ELSE}
                {$IfDef CALADAIR_VMC}
                        CALADAIR_ETUDE
                {$ELSE}
                {$IfDef LINDAB_VMC}
                        LINDAB_ETUDE
                {$ELSE}
                        {$IfDef MVN_MVNAIR}
                        MVN_ETUDE
                        {$Else}
                        O3D_ETUDE
                        {$EndIf}
                {$EndIf}
                {$EndIf}
        {$EndIf}
    {$EndIf}
  {$EndIf};

{$R *.dfm}

Procedure ShowDebug;
Begin
  Application.CreateForm(TDlg_Debug,Dlg_Debug);
  Dlg_Debug.ModeCalcul := True;
  Dlg_Debug.ArbreDebug.NodeDataSize := SizeOf(ArbreBatimentVNode);
  Dlg_Debug.FillTree;
  Dlg_Debug.Show;
  {$IfDef AERAU_CLIMAWIN}
  Dlg_Debug.parent := XtAerauliqueMainForm.Panel_CAD;
  {$Else}
  Dlg_Debug.parent := Fic_Etude.PanelCad;
  {$EndIf}
  Dlg_Debug.Align := AlClient;
End;

function TDlg_Debug.arbredebugNode(_Node: PVirtualNode): TarbreBatimentNode;
begin
  If _Node <> Nil Then Result := ArbreDebug.GetNodeData(_Node)
                  Else Result := Nil;
end;

function TDlg_Debug.arbredebugNodeData(_Node: PVirtualNode): TVentil_Objet;
begin
  If arbredebugNode(_Node) <> Nil Then Result := arbredebugNode(_Node)^.Data
                                  Else Result := nil;
end;

Procedure TDlg_Debug.FillTree;
Var
  m_NoReseau : Integer;
  m_ResNode  : PVirtualNode;
Begin
  ArbreDebug.Clear;
  For m_NoReseau := 0 To Etude.Batiment.Reseaux.Count - 1 Do Begin
    m_ResNode := ArbreDebug.AddChild(Nil);
    ArbredebugNode(m_ResNode).Data := Etude.Batiment.Reseaux[m_NoReseau];
    If Not ModeCalcul Then Begin
      If Etude.Batiment.Reseaux[m_NoReseau].Fils.Count > 0 Then AddNode(m_ResNode, Etude.Batiment.Reseaux[m_NoReseau]);
    End Else Begin
      If Etude.Batiment.Reseaux[m_NoReseau].FilsCalcul.Count > 0 Then   AddNode(m_ResNode, Etude.Batiment.Reseaux[m_NoReseau]);
    End;
  End;
  ArbreDebug.FullExpand;
End;

procedure TDlg_Debug.AddNode(_ParentNode: PVirtualNode; _Tr: TVentil_Troncon);
Var
  m_No   : Integer;
  m_Node : PVirtualNode;
begin
  m_Node := ArbreDebug.AddChild(_ParentNode);
  ArbredebugNode(m_Node).Data := _tr;
  If ModeCalcul Then Begin
    For m_No := 0 To _Tr.FilsCalcul.Count - 1 Do AddNode(m_node, _Tr.FilsCalcul[m_No]);
  End Else Begin
    For m_No := 0 To _Tr.Fils.Count - 1 Do AddNode(m_node, _Tr.Fils[m_No]);
  End;
end;

procedure TDlg_Debug.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

{$if CompilerVersion>20}
procedure TDlg_Debug.ArbreDebugGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var CellText: String);
{$elseif CompilerVersion<20}
procedure TDlg_Debug.ArbreDebugGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var CellText: WideString);
{$ifend}
begin
  If IsNotNil(arbredebugNodeData(Node)) Then Begin
    If arbredebugNodeData(Node).InheritsFrom(TVentil_Troncon) Then CellText := arbredebugNodeData(Node).Reference + '(' + IntToStr(TVentil_Troncon(arbredebugNodeData(Node)).Diametre) + ' mm - '
                                                                            + Float2Str(TVentil_Troncon(arbredebugNodeData(Node)).Vitesse, 2) + ' m/s)'
                                                           Else CellText := arbredebugNodeData(Node).Reference;
   End Else CellText := '';
end;


procedure TDlg_Debug.ArbreDebugChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  If arbredebugNodeData(Node) <> Nil Then arbredebugNodeData(Node).SetSelected;
end;

procedure TDlg_Debug.ToolButton1Click(Sender: TObject);
begin
  ModeCalcul := True;
  FillTree;
end;

procedure TDlg_Debug.ToolButton2Click(Sender: TObject);
begin
  ModeCalcul := False;
  FillTree;
end;

End.
