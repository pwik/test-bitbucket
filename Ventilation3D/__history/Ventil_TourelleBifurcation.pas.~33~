Unit Ventil_TourelleBifurcation;
 
{$Include 'Ventil_Directives.pas'}

Interface
Uses
  Classes, SysUtils, advGrid, BaseGrid, AdvObj, Math, Ventil_Bifurcation,
  AccesBDD,
  Ventil_Types, Ventil_Troncon, Ventil_Logement, Ventil_Utils,
  BBScad_Interface_Aeraulique;

Type
{ ************************************************************************************************************************************************** }
  TVentil_TourelleBifurcation = Class(TVentil_Bifurcation)
    Constructor Create; Override;
    Destructor Destroy; Override;
  Private
    FDzetaSaisie   : Real;
    FForceDzeta    : Integer;
  Public
    Procedure Load; Override;
    class Function FamilleObjet : String; Override;
    Procedure UpdateAfterCreate; Override;
    Procedure Paste(_Source: TVentil_Objet); Override;
    Procedure Affiche (_Grid: TAdvStringGrid); Override;
    Procedure Recupere(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType; Override;
    Procedure PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  HasComboBox(_Ligne: Integer): Boolean; Override;
    Function  AfficheQuestion(_NoQuestion: Integer): Boolean; Override;
    Function  EllipsClick(_Grid: TAdvStringGrid; _Ligne: Integer): String; Override;

    Function GetDzeta : Real;
  Protected
    class Function NomParDefaut : String; Override;
    Procedure IAero_Fredo_Base_RemplacerPar(Objet : IAero_Fredo_Base); Override;
    Function  ImageAssociee: String; Override;
    Procedure GenerationAccidents; Override;
    Procedure Save; Override;
    Procedure CalculerQuantitatifInternal; Override;
    Procedure InitEtiquette; Override;
    Function IsEtiquetteVisible : boolean; Override;
    Procedure SelectionneDiametre_SelonMethode; Override;
    Function GetTableDonnees : TTableDeDonneesBBS; Override;
  End;
{ ************************************************************************************************************************************************** }

Implementation
Uses
  Grids,
  Ventil_Caisson,
  Ventil_Edibatec, Ventil_edibatecProduits,
  Ventil_Firebird,
  Ventil_Const, Ventil_Accident,
  Ventil_Declarations, Ventil_Options,
  Ventil_TronconDessin,
  Ventil_Collecteur, Ventil_Batiment;

{ ******************************************************************* TVentil_TourelleBifurcation *************************************************************** }
Constructor TVentil_TourelleBifurcation.Create;
Begin
  Inherited;
  NbQuestions := NbQuestions + c_NbQ_Bifurcation + c_NbQ_TourelleBif;
    if Etude.Batiment.IsFabricantMVN then
      FDzetaSaisie := 1.54
    else
      FDzetaSaisie := 2;
  FForceDzeta := c_Non;
End;
{ ************************************************************************************************************************************************** }
Destructor TVentil_TourelleBifurcation.Destroy;
begin
  Inherited;
end;
{ ************************************************************************************************************************************************** }
class Function TVentil_TourelleBifurcation.NomParDefaut : String;
Begin
  if Etude.Batiment.IsFabricantVIM then
    Result := 'Plenum'
  else
    Result := 'TCDZ11';
End;
{ ************************************************************************************************************************************************** }
class Function TVentil_TourelleBifurcation.FamilleObjet : String;
Begin
  if Etude.Batiment.IsFabricantVIM then
    Result := 'Plenum'
  else
    Result := 'TCDZ11';
End;
{ ************************************************************************************************************************************************** }
Function TVentil_TourelleBifurcation.ImageAssociee: String;
Begin
  if Etude.Batiment.IsFabricantVIM then
    Result := 'Plenum.jpg'
  else
    Result := 'TCDZ11';
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleBifurcation.GenerationAccidents;
Var
  m_NoTr     : Integer;
  m_Tr       : TVentil_Troncon;
  m_Accident : TVentil_Accident;
Begin
  inherited;
      for m_NoTr := Accidents.Count - 1 downto 0 do
        if Accidents[m_NoTr] <> Nil then
          try
          Accidents[m_NoTr].Destroy;
          except

          end;
    Accidents.Clear;
    m_Accident := TVentil_Accident_TourelleBifurcation.Create;
    m_Accident.TronconPorteur := Self;
    Accidents.Add(m_Accident);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleBifurcation.Load;
Begin
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleBifurcation.Paste(_Source: TVentil_Objet);
Begin
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleBifurcation.UpdateAfterCreate;
Begin
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleBifurcation.IAero_Fredo_Base_RemplacerPar(Objet : IAero_Fredo_Base);
Begin
inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleBifurcation.Affiche(_Grid: TAdvStringGrid);
Var
  m_Row      : Integer;
  m_Question : Integer;
  m_Str      : String;
Begin
  Inherited;
  m_Row := GetNbQuestionAfficheTroncon + GetNbQuestionAfficheTeSouche + 1;
  For m_Question := Low(c_LibellesQ_TourelleBif) To High(c_LibellesQ_TourelleBif) Do If AfficheQuestion(m_Question) Then Begin
    _Grid.Cells[0, m_Row] := c_LibellesQ_TourelleBif[m_Question];
    Case m_Question Of
      c_QTourelleBif_DzetaForce : m_Str := c_OuiNon[FForceDzeta];
      c_QTourelleBif_DzetaSaisie : m_Str := FloatToStr(FDzetaSaisie);
      Else m_Str := '';
    End;
    _Grid.Cells[1, m_Row] := m_Str;
    Inc(m_Row);
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleBifurcation.Recupere(_Grid: TAdvStringGrid; _Ligne: Integer);
Begin
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Inherited

  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QTourelleBif_DzetaForce : FForceDzeta  := _Grid.Combobox.ItemIndex;
    c_QTourelleBif_DzetaSaisie : FDzetaSaisie  := _Grid.Floats[1, _Ligne];
  End;
  Inherited;

End;
{ ************************************************************************************************************************************************** }
Function TVentil_TourelleBifurcation.TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType;
Var
  m_NoChoix : Integer;
Begin
  Result := edNone;
  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
  //Parfois le GoEditing saute sans raison...
  _Grid.Options := _Grid.Options + [goEditing];
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Result := Inherited TypeEdition(_Grid, _Ligne)
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QTourelleBif_DzetaForce: Result := edComboList;
    c_QTourelleBif_DzetaSaisie: Result := edPositiveFloat;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleBifurcation.PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer);
Var
  m_NoChoix : Integer;
Begin
  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Inherited
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QTourelleBif_DzetaForce: Begin
                          _Grid.ClearComboString;
                            For m_NoChoix := Low(c_OUINON) To High(c_OUINON) Do
                              _Grid.AddComboString(c_OUINON[m_NoChoix]);
                          End;
    c_QTourelleBif_DzetaSaisie: Begin
                           End;
  End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_TourelleBifurcation.HasComboBox(_Ligne: Integer): Boolean;
Begin
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Result := Inherited HasComboBox(_Ligne)
  Else Result := AfficheQuestion(NoQuestion(_Ligne)) And (NoQuestion(_Ligne) In[c_QTourelleBif_DzetaForce]);
End;
{ ************************************************************************************************************************************************** }
Function TVentil_TourelleBifurcation.AfficheQuestion(_NoQuestion: Integer): Boolean;
Begin
  If _NoQuestion <= c_NbQ_Troncon + c_NbQ_Bifurcation Then Result := Inherited AfficheQuestion(_NoQuestion)
  Else Case _NoQuestion Of
    c_QTourelleBif_DzetaForce   : Result := not(Etude.Batiment.IsFabricantMVN) or (Etude.Batiment.IsFabricantMVN and VersionRnD);
    c_QTourelleBif_DzetaSaisie  : Result := FForceDzeta = c_Oui;
    Else Result := False;
  End;

  if _NoQuestion = c_QTroncon_NatureGaine then
    Result := False;

End;
{ ************************************************************************************************************************************************** }
Function TVentil_TourelleBifurcation.EllipsClick(_Grid: TAdvStringGrid; _Ligne: Integer): String;
Begin
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleBifurcation.Save;
Begin
  GetTableDonnees.Donnees.Insert;
  GetTableDonnees.Donnees.FieldByName('ID_TOURBIFURC').AsWideString := GUIDToString(Id);
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleBifurcation.CalculerQuantitatifInternal;
Begin
//  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleBifurcation.InitEtiquette;
Begin
  inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_TourelleBifurcation.IsEtiquetteVisible : boolean;
Begin
Result := Options.AffEtiq_Bifurc;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleBifurcation.SelectionneDiametre_SelonMethode;
Begin
Inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_TourelleBifurcation.GetTableDonnees : TTableDeDonneesBBS;
begin
  Result := Table_Etude_TourelleBifurcation;
end;
{ ************************************************************************************************************************************************** }
Function TVentil_TourelleBifurcation.GetDzeta : Real;
begin
  if (FForceDzeta = c_Oui) then
    begin
    Result := FDzetaSaisie;
    Exit;
    end
  else
  if Etude.Batiment.IsFabricantMVN then
    Result := 1.54
  else
    Result := 2;
end;
{ ******************************************************************* \ TVentil_TourelleBifurcation ************************************************************* }

End.


