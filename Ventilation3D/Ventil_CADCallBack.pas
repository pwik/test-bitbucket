unit Ventil_CADCallBack;

interface

uses
  Windows,
  Classes,
  Menus,
  BBScad_Interface_Commun,
  BBScad_Interface_Aeraulique;

type

  TCallBackAeraulique = Class(TInterfacedObject, IBBScad_Aeraulique_Callback)
    function ModeAero : TBBScad_ModeAero;
    function MVN_TypeBouche(ObjCW : IAero_Fredo_Base) : TMVN_TypeBouche;
    function NewPage : TBBScad_Canvas;
    function Aero_Fredo2Vilo_Tertiaire : boolean;
    function Aero_Fredo2Vilo_Collectif : boolean;
    function Aero_Fredo2Vilo_Hotel : boolean;
    function Aero_Fredo2Vilo_PiquageExpress : boolean;
    procedure Aero_Fredo2Vilo_Calcul;
    function Aero_Fredo2Vilo_GetHauteurEtage(Etage : integer) : single;
    function Aero_Fredo2Vilo_GetNombreEtages : integer;
    function Aero_Fredo2Vilo_CreateObj(ViloObj : IAero_Vilo_Base) : IAero_Fredo_Base;
//    procedure Aero_Fredo2Vilo_UpdateObjXMLActhys(Obj : IAero_Fredo_Base);
    function Aero_Fredo2Vilo_Guid2Obj(Guid : WideString; ViloObj : IAero_Vilo_Base) : IAero_Fredo_Base;
    function Aero_Fredo2Vilo_CouleurTypeReseau(Obj : IAero_Fredo_Base; Mode : TBBScad_CouleurTypeReseau; DefaultColor : TBBScad_ColorVector) : TBBScad_ColorVector;
    procedure Aero_Fredo2Vilo_Emmanchements(Obj : IAero_Fredo_Base; var EmmanchementAmont, EmmanchementAval : single);
    procedure Aero_Fredo2Vilo_Bifurcation_Y_Valide(ObjCW : IAero_Fredo_Base; var D_amon, D_aval_Lat_1, D_aval_Lat_2 : single);
    procedure Aero_Fredo2Vilo_Bifurcation_T_Valide(ObjCW : IAero_Fredo_Base; var D_amon, D_aval_Rect, D_aval_Lat : single);
    procedure Aero_Fredo2Vilo_Bifurcation_YT_Valide(ObjCW : IAero_Fredo_Base; var D_amon, D_aval_Lat_1, D_aval_Lat_2 : single);
    procedure Aero_Fredo2Vilo_Bifurcation_X_Valide(ObjCW : IAero_Fredo_Base; var D_amon, D_aval_Rect, D_aval_Lat_1, D_aval_Lat_2 : single);
    procedure Aero_Fredo2Vilo_TSouche_ToitureT_Valide(ObjCW : IAero_Fredo_Base; var D_amon, D_aval : single);
    procedure Aero_Fredo2Vilo_TSouche_Piquage_Valide(ObjCW : IAero_Fredo_Base; var D_amon, D_aval_Rect, D_aval_Lat : single);
    procedure Aero_Vilo2Fredo_SauvegardeAutoOn;
    procedure Aero_Vilo2Fredo_SauvegardeAutoOff;
    function Aero_Vilo2Fredo_CanCreateRegistre : boolean;
    procedure Aero_Vilo2Fredo_ExternalZoom;
    procedure Aero_Vilo2Fredo_Lock;
    procedure Aero_Vilo2Fredo_UnLock;
    function Aero_Vilo2Fredo_Acthys_NomPopup(ObjetActhys : IAero_Vilo_XML) : string;
    function Aero_Vilo2Fredo_RedefRepereType : TRedefRepereType;
    function Aero_Vilo2Fredo_NewGestImpressions : boolean;
    function Aero_Vilo2Fredo_ObjectCaption(ObjCW : IAero_Fredo_Base) : string;
    function Aero_Vilo2Fredo_Logements : IStrings;
  End;

  TBBScad_Menus = class(TInterfacedObject, IBBScad_Menus)
  private
    FIntf : pointer;
    FMenu : TMenuItem;
    function FindMenu(Menu : TMenuItem; Id : integer) : TMenuItem;
    procedure OnClick(Sender : TObject);
  public
    constructor Create(Intf : IBBScad_Commun; Menu : TMenuItem);

    //IBBScad_Menus
    procedure IBBScad_Menus_New(Caption : WideString; Id, ParentId : integer);
    procedure IBBScad_Menus_ChangeEnable(Id : integer; Enable : boolean);
    procedure IBBScad_Menus_ChangeVisible(Id : integer; Visible : boolean);
    procedure IBBScad_Menus_ChangeChecked(Id : integer; Checked : boolean);
  end;

implementation

uses

{$IfDef AERAU_CLIMAWIN}
XtAeraulique,
{$Else}
VMC_Etude,
{$EndIf}
HelpersCommun,
SysUtils,
XMLIntf,
Math,
Graphics,
Ventil_Plenum,
Ventil_Troncon,
Ventil_TronconDessin,
Ventil_bouche,
Ventil_Caisson,
Ventil_TeSouche,
Ventil_Colonne,
Ventil_Bifurcation,
Ventil_Declarations,
Ventil_Const;

//********************************************************************************************************
//########################################################################################################
//# TCallBackAeraulique                                                                                  #
//########################################################################################################
//********************************************************************************************************
function TCallBackAeraulique.ModeAero : TBBScad_ModeAero;
Begin
Result := Ventil_Declarations.ModeAero;
End;
//********************************************************************************************************
function TCallBackAeraulique.MVN_TypeBouche(ObjCW : IAero_Fredo_Base) : TMVN_TypeBouche;
begin
  Result := mvnBouche;
  If ObjCW <> Nil then
    begin
      //� modifier
      if TVentil_Bouche(ObjCW.IAero_Fredo_Base_Objet).IsBoucheChaudiere then
        Result := mvnChaudi�re
      else
        Result := mvnBouche;
    end;
end;
//********************************************************************************************************
function TCallBackAeraulique.NewPage : TBBScad_Canvas;
Begin
//sens� ne plus �tre utilis�
Result := 0;
End;
//********************************************************************************************************
function TCallBackAeraulique.Aero_Fredo2Vilo_Tertiaire : boolean;
Begin
  if (Etude <> nil) and (Etude.Batiment <> nil) then Result := (Etude.Batiment.TypeBat = c_BatimentTertiaire)
                                                else Result := false;
End;
//********************************************************************************************************
function TCallBackAeraulique.Aero_Fredo2Vilo_Collectif : boolean;
Begin
  if (Etude <> nil) and (Etude.Batiment <> nil) then Result := (Etude.Batiment.TypeBat = c_BatimentCollectif)
                                                else Result := false;
End;
//********************************************************************************************************
function TCallBackAeraulique.Aero_Fredo2Vilo_Hotel : boolean;
Begin
  if (Etude <> nil) and (Etude.Batiment <> nil) then Result := (Etude.Batiment.TypeBat = c_BatimentHotel)
                                                else Result := false;
End;
//********************************************************************************************************
function TCallBackAeraulique.Aero_Fredo2Vilo_PiquageExpress : boolean;
Begin
                if (Etude <> nil) and (Etude.Batiment <> nil) then
                        begin
                                if Etude.Batiment.IsFabricantVIM then
                                        Result := (Etude.Batiment.PiquageExpressVertical = c_Oui)
                                else
                                        result := Options.TerrassePiquageExpress;
                        end
                else
                        Result := false;
End;
//********************************************************************************************************
procedure TCallBackAeraulique.Aero_Fredo2Vilo_Calcul;
Begin
  Etude.FromCallBack := True;
  Etude.NbRecalcul := 0;
  Etude.CalculerInternal(Etude.ModeIteratif);
//  Etude.CalculerInternal(False);
  Etude.FromCallBack := False;
End;
//********************************************************************************************************
function TCallBackAeraulique.Aero_Fredo2Vilo_GetHauteurEtage(Etage : integer) : single;
Begin
  Result := Etude.HtEtageDef;
End;
//********************************************************************************************************
function TCallBackAeraulique.Aero_Fredo2Vilo_GetNombreEtages : integer;
Begin
  Result := Etude.NbEtageBat + 1;
End;
//********************************************************************************************************
function TCallBackAeraulique.Aero_Fredo2Vilo_CreateObj(ViloObj : IAero_Vilo_Base) : IAero_Fredo_Base;
Begin
    result := Etude.AddComposant(ViloObj);
End;
//********************************************************************************************************
{
procedure TCallBackAeraulique.Aero_Fredo2Vilo_UpdateObjXMLActhys(Obj : IAero_Fredo_Base);
Var
  XmlDoc : IXMLDocument;
begin
  If Obj<> Nil then
    TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).ImportXMLActhys(TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).LienCad.IAero_Vilo_XML_Get_as_Node(XmlDoc));
end;
}
//********************************************************************************************************
function TCallBackAeraulique.Aero_Fredo2Vilo_Guid2Obj(Guid : Widestring; ViloObj : IAero_Vilo_Base) : IAero_Fredo_Base;
Begin
  Result := Etude.ObjFromGUID(Guid, ViloObj);
End;
//********************************************************************************************************
function TCallBackAeraulique.Aero_Fredo2Vilo_CouleurTypeReseau(Obj : IAero_Fredo_Base; Mode : TBBScad_CouleurTypeReseau; DefaultColor : TBBScad_ColorVector) : TBBScad_ColorVector;
  function VectorMake(R, G, B, A : single) : TBBScad_ColorVector;
  begin
  Result.R := R;
  Result.G := G;
  Result.B := B;
  Result.A := A;
  end;

Const
  {$IfDef ACTHYS_DIMVMBP}
        _VitesseMaxi = 2.5;
  {$Else}
  {$IfDef MVN_MVNAIR}
        _VitesseMaxi = 2.5;
  {$Else}
        _VitesseMaxi = 5;
  {$EndIf}
  {$EndIf}
        _PDCMaxi     = 20;
//        _DepressionMaxi = 20;
(*
        clrViolet               : TBBScad_ColorVector = (R:0.309804; G:0.184314; B:0.309804; A:1);
        clrGreenCopper          : TBBScad_ColorVector = (R:0.32;     G:0.49;     B:0.46;     A:1);
        clrOrange               : TBBScad_ColorVector = (R:1;        G:0.5;      B:0.0;      A:1);
        clrYellow               : TBBScad_ColorVector = (R:1;        G:1;        B:0.0;      A:1);

        clrBlue                 : TBBScad_ColorVector = (R:0;        G:0;      B:1;      A:1);
        clrRed                  : TBBScad_ColorVector = (R:1;        G:0;      B:0;      A:1);
*)
Var
        _DepressionMaxi : Real;
        _DebitMaxi      : Real;
        tempValue : Single;
begin

result := defaultcolor;
  if (Obj <> Nil) and (Obj.IAero_Fredo_Base_Objet <> Nil) and (TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).Caisson <> Nil) then
    begin
  {$IfDef MVN_MVNAIR}
    _DepressionMaxi := 30.49;
  {$Else}
    _DepressionMaxi := TVentil_Caisson(TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).Caisson).Reseau_DeltaMaxiAffichageVIM;
  {$EndIf}
    _DebitMaxi      := TVentil_Caisson(TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).Caisson).Reseau_DebitMaximum;
    end
  else
    begin
  {$IfDef MVN_MVNAIR}
    _DepressionMaxi := 30.49;
  {$Else}
    _DepressionMaxi := 20;
  {$EndIf}
    _DebitMaxi      := 500;
    end;
        If Obj<> Nil then
                Begin
                        Case Mode Of
                                ctrStandard :   begin

                                                        if  not(Obj.IAero_Fredo_Base_Objet.InheritsFrom(TVentil_Bouche))
                                                            and Not(Obj.IAero_Fredo_Base_Objet.InheritsFrom(TVentil_Plenum))
                                                            and (TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).GetParentTeSouche <> Nil)
                                                            and TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).GetParentTeSouche.InheritsFrom(TVentil_Colonne) then
//                                                            and supports(Obj, IAero_ObjetRaccordement)
//                                                            and (Obj as IAero_ObjetRaccordement).IAero_ObjetRaccordement_RaccordementReseauVertical then
                                                                begin
//                                                                        case TVentil_Colonne(TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).GetParentTeSouche).TypeColonne of
                                                                        case TVentil_Colonne(TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).GetParentTeSouche).TypeColonne of
                                                                                c_TypeColonneVMC                      : Result := DefaultColor;
                                                                                c_TypeColonneShunt                    : Result := clWebViolet;
                                                                                c_TypeColonneConduitIndividuel        : Result := VectorMake(0.32, 0.49, 0.46, 1); //clrGreenCopper
                                                                                c_TypeColonneAlsace                   : Result := clWebOrange;
                                                                                c_TypeColonneRamon                    : Result := clWebYellow;
                                                                        else
                                                                                Result := DefaultColor;
                                                                        end;
                                                                end

  {$IfDef MVN_MVNAIR}
                                                        else
                                                          if  Obj.IAero_Fredo_Base_Objet.InheritsFrom(TVentil_Plenum) then
                                                            begin
                                                              if Not(TVentil_Plenum(Obj.IAero_Fredo_Base_Objet).ConfigExist) then
                                                                Result := clWebRed
                                                              else
                                                              Case TVentil_Plenum(Obj.IAero_Fredo_Base_Objet).TypePlenum of
                                                                c_TypePlenumRecouvrement  : Result := clWebBlue;
                                                                c_TypePlenumPiquage       : Result := clWebViolet;
                                                              else
                                                                Result := DefaultColor;
                                                              end;
                                                            end
  {$EndIf}
                                                        else
                                                          if  Obj.IAero_Fredo_Base_Objet.InheritsFrom(TVentil_Bouche) then
                                                            begin
                                                              if TVentil_Bouche(Obj.IAero_Fredo_Base_Objet).BoucheDefautSelection then
                                                                Result := clWebRed
                                                              else
                                                                Result := DefaultColor;
                                                            end
                                                        else
                                                                Result := DefaultColor;
                                                End;
                                ctrVitesses   : Result := VectorMake(Min(1, 2*TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).Vitesse/_VitesseMaxi), Min(1, 2*(_VitesseMaxi-TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).Vitesse)/_VitesseMaxi), 0, 1);
                                ctrPDC        : Result := VectorMake(Min(1, 2*TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).PdcSinguliereMaxi/_PDCMaxi), Min(1, 2*(_PDCMaxi-TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).PdcSinguliereMaxi)/_PDCMaxi), 0, 1);
                                ctrDepression : begin
                                                  if (Obj.IAero_Fredo_Base_Objet.InheritsFrom(TVentil_TronconDessin) or Obj.IAero_Fredo_Base_Objet.InheritsFrom(TVentil_Accident)) and (TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).ObjetTerminalAval <> Nil) then
                                                    Result := VectorMake(Min(1, 2*TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).ObjetTerminalAval.DeltaMaxiAffichageVIM/_DepressionMaxi), Min(1, 2*(_DepressionMaxi-TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).ObjetTerminalAval.DeltaMaxiAffichageVIM)/_DepressionMaxi), 0, 1)
                                                  else
                                                    Result := VectorMake(Min(1, 2*TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).DeltaMaxiAffichageVIM/_DepressionMaxi), Min(1, 2*(_DepressionMaxi-TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).DeltaMaxiAffichageVIM)/_DepressionMaxi), 0, 1);
                                                end;
                                ctrDebit      : Result := VectorMake(Min(1, 2*TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).DebitMaxi/_DebitMaxi), Min(1, 2*(_DebitMaxi-TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).DebitMaxi)/_DebitMaxi), 0, 1);
                                ctrCalcul     : if Not(Obj.IAero_Fredo_Base_Objet.InheritsFrom(TVentil_TronconDessin_Conduit) or Obj.IAero_Fredo_Base_Objet.InheritsFrom(TVentil_Accident)) then
                                                  Result :=VectorMake(1, 0, 0, 1);
                                ctrDiam�tre   : begin
                                                  case TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).Diametre of
                                                    125 : Result := clWebPink;
                                                    160 : Result := clWebMaroon;
                                                    175 : Result := clWebCadetBlue;
                                                    200 : Result := clWebRed;
                                                    250 : Result := clWebMidnightBlue;
                                                    315 : Result := clWebOrange;
                                                    355 : Result := clWebGreen;
                                                    400 : Result := clWebViolet;
                                                    450 : Result := clWebYellow;
                                                    500 : Result := VectorMake(0.75, 0.75, 0.75, 1); //clWebGray75;
                                                    560 : Result := clWebGold;
                                                    630 : Result := clWebAqua;
                                                    710 : Result := VectorMake(0.55, 0.47, 0.14, 1); // clWebBronze;
                                                    800 : Result := clWebPurple;
                                                    900 : Result := clWebYellowGreen;
                                                  end;
                                                end;

                        End;

                End;
end;
//********************************************************************************************************
procedure TCallBackAeraulique.Aero_Fredo2Vilo_Emmanchements(Obj : IAero_Fredo_Base; var EmmanchementAmont, EmmanchementAval : single);
Begin


        if TVentil_Troncon(Obj.IAero_Fredo_Base_Objet).NeedDiminutionEmmanchement then
                Begin
                EmmanchementAmont := 0.10;
                EmmanchementAval  := 0.10;
                End
        else

                Begin
                EmmanchementAmont := 0;
                EmmanchementAval  := 0;
                End;

End;
//********************************************************************************************************
procedure TCallBackAeraulique.Aero_Fredo2Vilo_Bifurcation_Y_Valide(ObjCW : IAero_Fredo_Base; var D_amon, D_aval_Lat_1, D_aval_Lat_2 : single);
Begin
        If ObjCW <> Nil then
                TVentil_Bifurcation(ObjCW.IAero_Fredo_Base_Objet).DetermineDiametresValides(D_amon, D_aval_Lat_1, D_aval_Lat_2);
End;
//********************************************************************************************************
procedure TCallBackAeraulique.Aero_Fredo2Vilo_Bifurcation_T_Valide(ObjCW : IAero_Fredo_Base; var D_amon, D_aval_Rect, D_aval_Lat : single);
Begin
        If ObjCW <> Nil then
                TVentil_Bifurcation(ObjCW.IAero_Fredo_Base_Objet).DetermineDiametresValides(D_amon, D_aval_Rect, D_aval_Lat);
End;
//********************************************************************************************************
procedure TCallBackAeraulique.Aero_Fredo2Vilo_Bifurcation_YT_Valide(ObjCW : IAero_Fredo_Base; var D_amon, D_aval_Lat_1, D_aval_Lat_2 : single);
Begin
        If ObjCW <> Nil then
                TVentil_Bifurcation(ObjCW.IAero_Fredo_Base_Objet).DetermineDiametresValides(D_amon, D_aval_Lat_1, D_aval_Lat_2);
End;
//********************************************************************************************************
procedure TCallBackAeraulique.Aero_Fredo2Vilo_Bifurcation_X_Valide(ObjCW : IAero_Fredo_Base; var D_amon, D_aval_Rect, D_aval_Lat_1, D_aval_Lat_2 : single);
Begin
        If ObjCW <> Nil then
                Begin
                TVentil_Bifurcation(ObjCW.IAero_Fredo_Base_Objet).DetermineDiametresValides(D_amon, D_aval_Rect, D_aval_Lat_1);
                D_aval_Lat_2 := D_aval_Lat_1;
                End;
End;
//********************************************************************************************************
procedure TCallBackAeraulique.Aero_Fredo2Vilo_TSouche_ToitureT_Valide(ObjCW : IAero_Fredo_Base; var D_amon, D_aval : single);
Begin
        If ObjCW <> Nil then
                TVentil_TeSouche(ObjCW.IAero_Fredo_Base_Objet).DetermineDiametresValides(D_amon, D_aval);
End;
//********************************************************************************************************
procedure TCallBackAeraulique.Aero_Fredo2Vilo_TSouche_Piquage_Valide(ObjCW : IAero_Fredo_Base; var D_amon, D_aval_Rect, D_aval_Lat : single);
Begin
        If ObjCW <> Nil then
                Begin
                TVentil_TeSouche(ObjCW.IAero_Fredo_Base_Objet).DetermineDiametresValides(D_amon, D_aval_Lat);
                D_aval_Rect := D_aval_Lat;
                End;
End;
//********************************************************************************************************
procedure TCallBackAeraulique.Aero_Vilo2Fredo_SauvegardeAutoOn;
begin
        if (Etude <> nil) then
                Etude.AutoSaveAutorisee := True;
        Aero_Vilo2Fredo_UnLock;
end;
//********************************************************************************************************
procedure TCallBackAeraulique.Aero_Vilo2Fredo_SauvegardeAutoOff;
begin
        if (Etude <> nil) then
                Etude.AutoSaveAutorisee := False;
        Aero_Vilo2Fredo_Lock;
end;
//********************************************************************************************************
function TCallBackAeraulique.Aero_Vilo2Fredo_CanCreateRegistre : boolean;
begin
  Result := True;

  if (Etude <> nil) and (Etude.DTU2014) then
    if Etude.Batiment.IsFabricantVIM and (Etude.Batiment.TypeBat in [c_BatimentCollectif, c_BatimentHotel]) then
      Result := False;

end;
//********************************************************************************************************
procedure TCallBackAeraulique.Aero_Vilo2Fredo_ExternalZoom;
begin
{$IfDef ANJOS_OPTIMA3D}
  Fic_Etude.ZoomCad(Nil);
{$EndIf}
end;
//********************************************************************************************************
procedure TCallBackAeraulique.Aero_Vilo2Fredo_Lock;
begin
{$IfNDef AERAU_CLIMAWIN}
 Fic_Etude.DisabledMenus := True;
{$EndIf}
end;
//********************************************************************************************************
procedure TCallBackAeraulique.Aero_Vilo2Fredo_UnLock;
begin
{$IfNDef AERAU_CLIMAWIN}
 Fic_Etude.DisabledMenus := False;
{$EndIf}
end;
//********************************************************************************************************
function TCallBackAeraulique.Aero_Vilo2Fredo_Acthys_NomPopup(ObjetActhys : IAero_Vilo_XML) : string;
Const
  Cst_Espaceur = ' | ';
begin
  if (ObjetActhys.IAero_Vilo_XML_Get_as_Node <> Nil) and
     (ObjetActhys.IAero_Vilo_XML_Get_as_Node.DocumentElement <> Nil) then
    begin
      if ObjetActhys.IAero_Vilo_XML_Get_as_Node.DocumentElement.ChildNodes.FindNode('Ref_Plenum') <> Nil then
        begin
        Result := ObjetActhys.IAero_Vilo_XML_Get_as_Node.DocumentElement.ChildValues['Ref_Plenum'];
        Result := Result + Cst_Espaceur + ObjetActhys.IAero_Vilo_XML_Get_as_Node.DocumentElement.ChildValues['Label_Empilement'];
        Result := Result + Cst_Espaceur + ObjetActhys.IAero_Vilo_XML_Get_as_Node.DocumentElement.ChildValues['Ref_Souche'];
        end
      else
      if ObjetActhys.IAero_Vilo_XML_Get_as_Node.DocumentElement.ChildNodes.FindNode('Ref_Colonne') <> Nil  then
        begin
        Result := ObjetActhys.IAero_Vilo_XML_Get_as_Node.DocumentElement.ChildValues['Ref_Colonne'];
        end
      else
        Result := 'Pl�num';
//    Result := Result + Cst_Espaceur + ObjetActhys.IAero_Vilo_XML_Get_as_Node.DocumentElement.ChildValues['Label_Empilement'];
//    Result := Result + Cst_Espaceur + ObjetActhys.IAero_Vilo_XML_Get_as_Node.DocumentElement.ChildValues['Label_Empilement'];
//    Result := Result + Cst_Espaceur + ObjetActhys.IAero_Vilo_XML_Get_as_Node.DocumentElement.ChildValues['Label_Empilement'];
//    Result := Result + Cst_Espaceur + ObjetActhys.IAero_Vilo_XML_Get_as_Node.DocumentElement.ChildValues['Label_Empilement'];
    end
  else
    Result := 'Pl�num';
{
    Case _Node.ChildValues['Type_Plenum'] of
      1 : TypePlenum := c_TypePlenumRecouvrement;
      2 : TypePlenum := c_TypePlenumPiquage;
   End;

  Ligne_Souche := _Node.ChildValues['Ligne_Souche'];
  Colonne_Souche := _Node.ChildValues['Colonne_Souche'];
  Hauteur_Bas := _Node.ChildValues['Hauteur_Bas_Plenum'];
}
end;
//********************************************************************************************************
function TCallBackAeraulique.Aero_Vilo2Fredo_RedefRepereType : TRedefRepereType;
begin
                if (Etude <> nil) and (Etude.Batiment <> nil) then
                        begin
                                if Etude.Batiment.IsFabricantACT then
                                        Result := rrtDefinition
                                else
                                if Etude.Batiment.IsFabricantF_A then
                                        Result := rrtDefinition
                                else
//                                        result := rrtTranslation;
                                        result := rrtDefinition;
                        end
                else
//                        Result := rrtTranslation;
                        Result := rrtDefinition;

end;
//********************************************************************************************************
function TCallBackAeraulique.Aero_Vilo2Fredo_NewGestImpressions : boolean;
begin
  result := (Options <> Nil) and Options.GestionnaireVuesCAD;
end;
//********************************************************************************************************
function TCallBackAeraulique.Aero_Vilo2Fredo_ObjectCaption(ObjCW : IAero_Fredo_Base) : string;
begin
  If ObjCW <> Nil then
    Result := TVentil_Troncon(ObjCW.IAero_Fredo_Base_Objet).Reference;
end;
//********************************************************************************************************
function TCallBackAeraulique.Aero_Vilo2Fredo_Logements : IStrings;
var
  cpt : Integer;
begin

  Result := TStringList.Create.AsIstrings;
  if (Etude <> nil) and (Etude.Batiment <> nil) and (Etude.Batiment.Logements <> nil) then
    for cpt := 0 to Etude.Batiment.Logements.Count - 1 do
      Result.Add(Etude.Batiment.Logements.Item[cpt].Reference);
end;
//********************************************************************************************************

//########################################################################################################
//# TBBScad_Menus                                                                                        #
//########################################################################################################
//********************************************************************************************************
constructor TBBScad_Menus.Create(Intf: IBBScad_Commun; Menu : TMenuItem);
begin
  FIntf := pointer(Intf);
  FMenu := Menu;
end;
//********************************************************************************************************
function TBBScad_Menus.FindMenu(Menu : TMenuItem; Id : integer) : TMenuItem;
var i : integer;
begin
  if (Id = 0) then
    Result := Menu
  else begin
    Result := nil;
    for i:=0 to Menu.Count-1 do begin
      if (Menu[i].Tag = Id) then Result := Menu[i]
                            else Result := FindMenu(Menu[i], Id);
      if (Result <> nil) then Break;
    end;
  end;
end;
//********************************************************************************************************
procedure TBBScad_Menus.OnClick(Sender : TObject);
begin
  IBBScad_Commun(FIntf).IBBScad_Callbacks_Menus_OnClick(TMenuItem(Sender).Tag);
end;
//********************************************************************************************************
procedure TBBScad_Menus.IBBScad_Menus_New(Caption: WideString; Id, ParentId: integer);
var M : TMenuItem;
begin
  M := FindMenu(FMenu, ParentId);
  M.Add(TMenuItem.Create(nil));
  M.Items[M.Count-1].Caption := Caption;
  M.Items[M.Count-1].Tag := Id;
  M.Items[M.Count-1].OnClick := OnClick;
end;
//********************************************************************************************************
procedure TBBScad_Menus.IBBScad_Menus_ChangeEnable(Id: integer; Enable: boolean);
var M : TMenuItem;
begin
  M := FindMenu(FMenu, Id);
  M.Enabled := Enable;
end;
//********************************************************************************************************
procedure TBBScad_Menus.IBBScad_Menus_ChangeVisible(Id: integer; Visible: boolean);
var M : TMenuItem;
begin
  M := FindMenu(FMenu, Id);
  M.Visible := Visible;
end;
//********************************************************************************************************
procedure TBBScad_Menus.IBBScad_Menus_ChangeChecked(Id : integer; Checked : boolean);
var M : TMenuItem;
begin
  M := FindMenu(FMenu, Id);
  M.Checked := Checked;
end;
//********************************************************************************************************

end.

