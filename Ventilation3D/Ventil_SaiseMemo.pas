Unit Ventil_SaiseMemo;
                 
{$Include 'Ventil_Directives.pas'}

Interface

Uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
     Buttons, ExtCtrls,

     VMC_Types;

Type
  TFic_SaisieMemo = class(TForm)
    Bevel1: TBevel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Memo: TMemo;
  Private
    { Private declarations }
  Public
    { Public declarations }
  End;

  Function InputQueryMemo(_Caption: String; Var _Text: TMemoLignes): Boolean;

Var
  Fic_SaisieMemo: TFic_SaisieMemo;

Implementation

{$R *.dfm}

{ ************************************************************************************************************************************************** }

Function InputQueryMemo(_Caption: String; Var _Text: TMemoLignes): Boolean;
Var
  NoLigne : SmallInt;
Begin
  If Fic_SaisieMemo = Nil Then Application.CreateForm(TFic_SaisieMemo, Fic_SaisieMemo);
  Fic_SaisieMemo.Memo.Clear;
  For NoLigne := Low(_Text) To High(_Text) Do Fic_SaisieMemo.Memo.Lines.Add(_Text[NoLigne]);
  Fic_SaisieMemo.Caption   := _Caption;
  If Fic_SaisieMemo.ShowModal = Id_Ok Then
  Begin
    Finalize(_Text);
    SetLength(_Text, Fic_SaisieMemo.Memo.Lines.Count);
    For NoLigne := 0 To Fic_SaisieMemo.Memo.Lines.Count - 1 Do _Text[NoLigne] := Fic_SaisieMemo.Memo.Lines[NoLigne];
    Result := True;
  End Else Result := False;
End;
{ ************************************************************************************************************************************************** }


{ ************************************************************************************************************************************************** }

End.
