Unit Ventil_Logement;
  
{$Include 'Ventil_Directives.pas'}

Interface

Uses
  Classes, SysUtils, advGrid, BaseGrid, AdvObj,Graphics, DB,
  Ventil_Troncon,
  Ventil_ChiffrageTypes,
  Ventil_Utils, Ventil_Types, Ventil_SystemeVMC, Ventil_EdibatecProduits, Ventil_Const,
  XMLIntf,
{$IFDEF ACTHYS_DIMVMBP}
  ImportEVenthysXSD,
  ExportEVenthysXSD,
{$ENDIF}
  BBScad_Interface_Aeraulique;

Type
{ ************************************************************************************************************************************************** }
  TVentil_Logement = Class(TVentil_Objet)
    Constructor Create;
    Destructor Destroy; Override;
  Private
    IsTertiaire: Boolean;
    IsCollectif : Boolean;
    IsHotel : Boolean;
    NbPiecesAvecEntreeAir: Integer;
    FIdImportXML : Integer;
    FChiffrageT2A: TChiffrage;
    Function  SelectionEntreeAir(var EntreeAirS : TEdibatec_Produit): String;
    Procedure SaveDescription;
    Procedure LoadDescription;
    Procedure SaveAcoustDn;
    Procedure LoadAcoustDn;
    Procedure ChiffrageEA_DoubleFlux(_Caisson : TVentil_Troncon);
    Procedure ChiffrageEA_Tertiaire(_Caisson : TVentil_Troncon);
    Procedure ChiffrageEA_Collectif(_Caisson : TVentil_Troncon);
    Procedure ChiffrageEA_CollectifDn(_Caisson : TVentil_Troncon);
    Procedure ChiffrageEA_CollectifStandard(_Caisson : TVentil_Troncon);
    Procedure ChiffrageEA_Accessoire(_Caisson : TVentil_Troncon; _NoEdibatec: String; _Qte : integer);
    Procedure ClearChiffrageT2A;
  Public
    TypeLogement     : Integer;

{$IfDef OLD_ATVENTIL}
    T3Optimise       : Integer;
{$Else}
    IdxConfig         : Integer;
    IdxSolutionEA   : array[1..c_NbPiecesSeches] of Integer;
{$EndIf}
    SystemeBatiment  : Integer;
    Systeme          : TVentil_Systeme;
    ChaudiereCuisine : Integer;
    CellierVentile   : Integer;
    NbEntreeAirLocal : Integer;
    CalculDn         : Integer;

    CuisineFermee    : Integer;

    { Pour l'acoustique }
    Tr_Cuisine         : Real;
    InfosPieces        : Array [1..c_NbPiecesHumide] Of Array[1..2] Of Real; // (ht sous plafond, surface)


    EntreeAirSelection1: TEdibatec_Produit;
    EntreeAirSelection2: TEdibatec_Produit;
    EntreeAirSelection3: TEdibatec_Produit;
    EntreeAirSelection4: TEdibatec_Produit;
    Ventil_EntreesAirLogement: Array[c_TypeT1..c_TypeT7] Of TVentil_EntreesAirLogement;
    EntreesDAirDnChoisies: Boolean;

    LongueursConduits : TVentil_Longueurs;

    nb_Cuisine : Array of Integer;
    nb_Sdb : Array of Integer;
    nb_WC: Array of Integer;
    nb_Sdb_WC_Unique : Array of Integer;
    nb_Sdb_Unique : Array of Integer;


    property IdImportXML : Integer read FIdImportXML write FIdImportXML;
    property ChiffrageT2A: TChiffrage read FChiffrageT2A write FChiffrageT2A;
    Procedure Paste(_Source: TVentil_Objet); Override;
    Procedure Affiche(_Grid: TAdvStringGrid); Override;
    Procedure AfficheResultatsHotel(_Grid: TAdvStringGrid);
    Procedure AfficheResultats(_Grid: TAdvStringGrid); Override;
    Procedure Recupere(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType; Override;
    Procedure PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  HasComboBox(_Ligne: Integer): Boolean; Override;
    Function  AfficheQuestion(_NoQuestion: Integer): Boolean; Override;
    Function  EllipsClick(_Grid: TAdvStringGrid; _Ligne: Integer): String; Override;
    Function  Get_EA_VIM : String;

    Procedure Chiffrage_EntreesAir(_Caisson : TVentil_Troncon);
    Procedure ImportXMLActhys(_Node : IXMLNode); Override;
    Procedure ExportXMLActhys(_Node : IXMLNode); Override;
    Procedure ExportXMLActhys2ndPasse(_Node : IXMLNode); Override;
    Procedure Save; Override;
    Procedure Load; Override;
    Function  UtiliseCalcul: Boolean;
    Procedure MajEntreesAir;
    Procedure ReinitialiseAcoustiqueDn;
    Procedure ReinitialiseAcoustiqueDnVIM;
    Procedure ReinitialiseAcoustiqueDnANJOS;
    class Function FamilleObjet : String; Override;
    Procedure AddChiffrage(_Produit: TEdibatec_Produit; _Quantite: Real; _CategorieChiffrage, _CodeClasseChiffrage: Integer; _LibelleSup : String = '');
    Procedure AddChiffrageErreur(Const _TypeProduit, _Supplement: String; _Supplement2: String = '');


    Function Snb_Cuisine : Integer;
    Function Snb_Sdb : Integer;
    Function Snb_WC: Integer;
    Function Snb_Sdb_WC_Unique : Integer;
    Function Snb_Sdb_Unique : Integer;
    function Snb_Logements : Integer;
    function nb_Logements(Index : Integer) : Integer;
    function LibTypeLogement : String;
    function Optimise : Boolean;
  Protected
    class Function NomParDefaut : String; Override;
    Procedure IAero_Fredo_Base_RemplacerPar(Objet : IAero_Fredo_Base); Override;
  End;
{ ************************************************************************************************************************************************** }
  TListeLogements = Class(TList)
    Constructor Create;
    Destructor Destroy; Override;
  Private
    Procedure SetItems(Index:Integer; Value:TVentil_Logement);
    Function  GetItems(Index:Integer):TVentil_Logement;
  Public
    TypesPiecesHumides : Array[Low(c_NomsPiecesBouche)..High(c_NomsPiecesBouche)] of TVentil_TypeCombo;
    TypesPiecesSeches : Array[Low(c_NomsPiecesEA)..High(c_NomsPiecesEA)] of TVentil_TypeCombo;

    TypesLogementsCollectif : Array[Low(c_TypesLogements)..High(c_TypesLogements)] of TVentil_TypeCombo;
    TypesChambresHotel : Array[Low(c_TypesChambres)..High(c_TypesChambres)] of TVentil_TypeCombo;
{$IFDEF ACTHYS_DIMVMBP}
    Procedure ImportXMLActhys(_Node : ImportEVenthysXSD.IXMLLOGEMENTS);
    Procedure ExportXMLActhys(_Node : ExportEVenthysXSD.IXMLLOGEMENTS);
    Procedure ExportXMLActhys2ndPasse(_Node : ExportEVenthysXSD.IXMLLOGEMENTS);
{$ENDIF}
    Function  GetLogementFromIDXML(_Id : Integer):TVentil_Logement;
    Function  Add(Value:TVentil_Logement):Integer; reintroduce;
    property  Item[Index:Integer]:TVentil_Logement read GetItems write SetItems; Default;
    Procedure Load;
    Procedure Save;
    Procedure MiseAjourQuantitesGaines;
    Procedure ChiffrageAccessoiresT2A;
    Procedure ClearChiffrageT2A;
  End;
{ ************************************************************************************************************************************************** }

Implementation
Uses
  Menus,
  Grids,
  Windows,
  BbsgFonc,
  Ventil_Firebird, Ventil_Collecteur, Ventil_Declarations, Ventil_DescriptionLogement, Ventil_EdibatecChoixProduit,
  Ventil_Edibatec,
  Ventil_Caisson,
  Ventil_Bouche,
  Ventil_AcoustiqueDn,
  Math, Ventil_Etude;

{ ******************************************************************* TVentil_Logement ***************************************************************** }
Procedure TVentil_Logement.Paste(_Source: TVentil_Objet);
var
  cpt : Integer;
Begin
inherited;
    Reference                   := 'Copie de ' + TVentil_Logement(_Source).Reference;
//    IdImportXML                 := TVentil_Logement(_Source).IdImportXML;
    IsTertiaire                 := TVentil_Logement(_Source).IsTertiaire;
    IsCollectif                 := TVentil_Logement(_Source).IsCollectif;
    IsHotel                     := TVentil_Logement(_Source).IsHotel;
    NbPiecesAvecEntreeAir       := TVentil_Logement(_Source).NbPiecesAvecEntreeAir;
    TypeLogement                := TVentil_Logement(_Source).TypeLogement;
{$IfDef OLD_ATVENTIL}
    T3Optimise                  := TVentil_Logement(_Source).T3Optimise;
{$Else}
    IdxConfig                   := TVentil_Logement(_Source).IdxConfig;
      for cpt := 1 to c_NbPiecesSeches do
        idxSolutionEA[cpt]               := TVentil_Logement(_Source).idxSolutionEA[cpt];
{$EndIf}
    SystemeBatiment             := TVentil_Logement(_Source).SystemeBatiment;
    Systeme                     := TVentil_Logement(_Source).Systeme;
    ChaudiereCuisine            := TVentil_Logement(_Source).ChaudiereCuisine;
    CellierVentile              := TVentil_Logement(_Source).CellierVentile;
    NbEntreeAirLocal            := TVentil_Logement(_Source).NbEntreeAirLocal;
    CalculDn                    := TVentil_Logement(_Source).CalculDn;
    CuisineFermee               := TVentil_Logement(_Source).CuisineFermee;
    Tr_Cuisine                  := TVentil_Logement(_Source).Tr_Cuisine;
    InfosPieces                 := TVentil_Logement(_Source).InfosPieces;
    nb_Cuisine                  := TVentil_Logement(_Source).nb_Cuisine;
    nb_Sdb                      := TVentil_Logement(_Source).nb_Sdb;
    nb_WC                       := TVentil_Logement(_Source).nb_WC;
    nb_Sdb_WC_Unique            := TVentil_Logement(_Source).nb_Sdb_WC_Unique;
    nb_Sdb_Unique               := TVentil_Logement(_Source).nb_Sdb_Unique;
    EntreeAirSelection1         := TVentil_Logement(_Source).EntreeAirSelection1;
    EntreeAirSelection2         := TVentil_Logement(_Source).EntreeAirSelection2;
    EntreeAirSelection3         := TVentil_Logement(_Source).EntreeAirSelection3;
    EntreeAirSelection4         := TVentil_Logement(_Source).EntreeAirSelection4;
    Ventil_EntreesAirLogement   := TVentil_Logement(_Source).Ventil_EntreesAirLogement;
    EntreesDAirDnChoisies       := TVentil_Logement(_Source).EntreesDAirDnChoisies;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.IAero_Fredo_Base_RemplacerPar(Objet : IAero_Fredo_Base);
Var
  cpt : Integer;
Begin
inherited;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).Reference                 := Reference;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).IsTertiaire               := IsTertiaire;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).IsCollectif               := IsCollectif;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).IsHotel                   := IsHotel;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).IdImportXML               := IdImportXML;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).NbPiecesAvecEntreeAir     := NbPiecesAvecEntreeAir;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).TypeLogement              := TypeLogement;
{$IfDef OLD_ATVENTIL}
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).T3Optimise                := T3Optimise;
{$Else}
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).IdxConfig                := IdxConfig;
      for cpt := 1 to c_NbPiecesSeches do
        TVentil_Logement(Objet.IAero_Fredo_Base_Objet).IdxSolutionEA[cpt]                := IdxSolutionEA[cpt];
{$EndIf}
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).SystemeBatiment           := SystemeBatiment;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).Systeme                   := Systeme;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).ChaudiereCuisine          := ChaudiereCuisine;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).CellierVentile            := CellierVentile;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).NbEntreeAirLocal          := NbEntreeAirLocal;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).CalculDn                  := CalculDn;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).CuisineFermee             := CuisineFermee;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).Tr_Cuisine                := Tr_Cuisine;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).InfosPieces               := InfosPieces;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).nb_Cuisine                := nb_Cuisine;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).nb_Sdb                    := nb_Sdb;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).nb_WC                     := nb_WC;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).nb_Sdb_WC_Unique          := nb_Sdb_WC_Unique;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).nb_Sdb_Unique             := nb_Sdb_Unique;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).EntreeAirSelection1       := EntreeAirSelection1;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).EntreeAirSelection2       := EntreeAirSelection2;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).EntreeAirSelection3       := EntreeAirSelection3;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).EntreeAirSelection4       := EntreeAirSelection4;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).Ventil_EntreesAirLogement := Ventil_EntreesAirLogement;
TVentil_Logement(Objet.IAero_Fredo_Base_Objet).EntreesDAirDnChoisies     := EntreesDAirDnChoisies;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.Affiche(_Grid: TAdvStringGrid);
Var
  m_Row      : Integer;
  m_Question : Integer;
  m_Str      : String;
Begin
  Inherited;
  m_Row := 1;
  For m_Question := Low(c_LibellesQ_Logement) To High(c_LibellesQ_Logement) Do If AfficheQuestion(m_Question) Then Begin
    _Grid.Cells[0, m_Row] := c_LibellesQ_Logement[m_Question];
    Case m_Question Of
      c_QLogement_Reference         : m_Str := Reference;
      c_QLogement_TypeLogement      : m_Str := c_TypesLogements[TypeLogement];
      c_QLogement_TypeChambre       : m_Str := c_TypesChambres[TypeLogement];
{$IfDef OLD_ATVENTIL}
      c_QLogement_T3Optimise,
      c_QLogement_WCSepares         : m_Str := c_OUINON[T3Optimise];
      c_QLogement_IdxConfig         : m_Str := '';
      c_QLogement_IdxSolutionEASj     : m_Str := '';
      c_QLogement_IdxSolutionEACh     : m_Str := '';
{$Else}
      c_QLogement_T3Optimise,
      c_QLogement_WCSepares         : m_Str := c_OUINON[c_non];
      c_QLogement_IdxConfig         : m_Str := Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).GetLibellePiecesHumides(False);
      c_QLogement_IdxSolutionEASj   : m_Str := Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[c_Sejour][IdxSolutionEA[c_Sejour]].GetLibelle;
      c_QLogement_IdxSolutionEACh   : m_Str := Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[c_Chambre][IdxSolutionEA[c_Chambre]].GetLibelle;
{$EndIf}
      c_QLogement_VentilationGen    : m_Str := c_IsVentilationGen[SystemeBatiment];
      c_QLogement_Ventilation       : If IsNotNil(Systeme) Then m_Str := Systeme.Reference
                                                           Else m_Str := ' *** ';
      c_QLogement_PositionChaudiere : m_Str := c_OUINON[ChaudiereCuisine];
      c_QLogement_CellierVentile    : m_Str := c_OUINON[CellierVentile];
      c_QLogement_CuisineFermee     : m_Str := c_OUINON[CuisineFermee];
      c_QLogement_Description       : m_Str := 'Clic pour modifier';
      c_QLogement_NbEntreeAirLocal  : m_Str := IntToStr(NbEntreeAirLocal);
      c_QLogement_EntreeAir1 : If EntreeAirSelection1 <> nil Then m_Str := EntreeAirSelection1.Reference
                                                       Else m_Str := ' *** ';
      c_QLogement_EntreeAir2 : If EntreeAirSelection2 <> nil Then m_Str := EntreeAirSelection2.Reference
                                                       Else m_Str := ' *** ';
      c_QLogement_EntreeAir3 : If EntreeAirSelection3 <> nil Then m_Str := EntreeAirSelection3.Reference
                                                       Else m_Str := ' *** ';
      c_QLogement_EntreeAir4 : If EntreeAirSelection4 <> nil Then m_Str := EntreeAirSelection4.Reference
                                                       Else m_Str := ' *** ';
      c_QLogement_CalculDN: m_Str := c_OUINON[CalculDN];
      c_QLogement_EntreesDairDn : m_Str := 'Clic pour modifier';
      Else m_Str := '';
    End;
    _Grid.Cells[1, m_Row] := m_Str;
    Inc(m_Row);
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.AfficheResultatsHotel(_Grid: TAdvStringGrid);
Var
  m_Row     : Integer;
  m_Col     : Integer;
  m_NoPiece : Integer;
  m_NoCh    : Integer;
  m_InfosEA : TVentil_EntreesAirLogement;
  m_NoEA    : Integer;
  ConditionVerifiee : Boolean;
  ColorPantone372 : TColor;
Begin
  ColorPantone372 := RGB(215, 233, 161);
  For m_Row := 0 To 1 Do For m_Col := 0 To _Grid.ColCount - 1 Do Begin
    _Grid.CellProperties[m_Col, m_Row].Alignment := taCenter;
    _Grid.CellProperties[m_Col, m_Row].Alignment := taCenter;
{$IFDEF ACTHYS_DIMVMBP}
    _Grid.CellProperties[m_Col, m_Row].BrushColor := ColorPantone372;
    _Grid.CellProperties[m_Col, m_Row].BrushColorTo := ClWhite;
{$ELSE}
    _Grid.CellProperties[m_Col, m_Row].BrushColor := clSilver;
{$ENDIF}
  End;

  For m_Row := 2 To _Grid.RowCount - 1 Do Begin
    _Grid.cells[2, m_Row] := '';
    _Grid.cells[3, m_Row] := '';
  End;

  _Grid.Cells[0, 0] := 'Extraction';
  _Grid.Cells[2, 0] := 'Soufflage';
  _Grid.Cells[0, 1] := 'Pi�ce';
  _Grid.Cells[2, 1] := 'Pi�ce';
  _Grid.Cells[1, 1] := 'Bouche';
  _Grid.Cells[3, 1] := 'Entr�e d''air';
  If IsNotNil(Systeme) Then Begin
    { Bouches }
{$IfDef OLD_ATVENTIL}
      if T3Optimise = c_Oui then
        begin
        _Grid.Cells[0, 2] := c_NomsPiecesHumides[c_Sdb];
        _Grid.Cells[1, 2] := Systeme.Bouche[TypeLogement, c_Sdb, T3Optimise = c_Oui].GetFirstLibelle;
        _Grid.Cells[0, 3] := c_NomsPiecesHumides[c_WC];
        _Grid.Cells[1, 3] := Systeme.Bouche[TypeLogement, c_WC, T3Optimise = c_Oui].GetFirstLibelle;
        end
      else
        begin
        _Grid.Cells[0, 2] := c_NomsPiecesHumides[c_SdbWC];
        _Grid.Cells[1, 2] := Systeme.Bouche[TypeLogement, c_SdbWC, T3Optimise = c_Oui].GetFirstLibelle;
        _Grid.Cells[0, 3] := '';
        _Grid.Cells[1, 3] := '';
        end;
{$Else}
      if  Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).Bouches[c_Sdb] <> Nil then
        begin
        _Grid.Cells[0, 2] := c_NomsPiecesHumides[c_Sdb];
        _Grid.Cells[1, 2] := Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).Bouches[c_Sdb].GetFirstLibelle;
        _Grid.Cells[0, 3] := c_NomsPiecesHumides[c_WC];
        _Grid.Cells[1, 3] := Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).Bouches[c_WC].GetFirstLibelle;
        end
      else
      if  Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).Bouches[c_SdbWC] <> Nil then
        begin
        _Grid.Cells[0, 2] := c_NomsPiecesHumides[c_SdbWC];
        _Grid.Cells[1, 2] := Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).Bouches[c_SdbWC].GetFirstLibelle;
        _Grid.Cells[0, 3] := '';
        _Grid.Cells[1, 3] := '';
        end;
{$EndIf}

    { Entr�es d'air }


if Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN then
        ConditionVerifiee := ((CalculDn = c_Oui) or (Etude.Batiment.CalculDN = c_Oui)) And (EntreesDAirDnChoisies) and (Etude.Batiment.ChiffrageDN = c_Oui)
else
(*
if Etude.Batiment.IsFabricantACT then
        ConditionVerifiee := false
else
*)
        ConditionVerifiee := ((CalculDn = c_Oui)) And (EntreesDAirDnChoisies);

    If ConditionVerifiee Then
    Begin
      m_InfosEA := Ventil_EntreesAirLogement[TypeLogement];
      m_NoCh  := 1;
      For m_NoPiece := 1 To m_InfosEA.Count Do Begin
          _Grid.Cells[2, m_NoPiece + 1] := 'Chambre ' + IntToStr(m_NoCh);
          inc(m_NoCh);
        _Grid.Cells[3, m_NoPiece + 1] := m_InfosEA[m_NoPiece - 1].EntreeChoisie.Reference;
      End;
    End Else
    If ((Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN) And (Etude.Batiment.ChiffrageDN = c_Oui)) or (not(Etude.Batiment.IsFabricantVIM) and Not(Etude.Batiment.IsFabricantMVN)) Then
    Begin
{$IfDef OLD_ATVENTIL}
      if Systeme.EntreeAir[TypeLogement, c_Chambre, T3Optimise = c_Oui] <> Nil then
        For m_NoEA := 0 To Systeme.EntreeAir[TypeLogement, c_Chambre, T3Optimise = c_Oui].Count - 1 Do
          If Systeme.EntreeAir[TypeLogement, c_Chambre, T3Optimise = c_Oui][m_NoEA].Nombre > 0 Then
            Begin
            _Grid.Cells[2, 2] := 'Chambre';
            _Grid.Cells[3, 2] := Systeme.EntreeAir[TypeLogement, c_Chambre, T3Optimise = c_Oui][m_NoEA].Libelle2;
            Inc(m_Row);
            End;
{$Else}
      if Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[c_Chambre][IdxSolutionEA[c_Chambre]] <> Nil then
        For m_NoEA := 0 To Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[c_Chambre][IdxSolutionEA[c_Chambre]].Count - 1 Do
          If Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[c_Chambre][IdxSolutionEA[c_Chambre]][m_NoEA].Nombre > 0 Then
            Begin
            _Grid.Cells[2, 2] := 'Chambre';
            _Grid.Cells[3, 2] := Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[c_Chambre][IdxSolutionEA[c_Chambre]][m_NoEA].Libelle2;
            Inc(m_Row);
            End;
{$EndIf}
    End;

  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.AfficheResultats(_Grid: TAdvStringGrid);
Var
  m_NoPiece : Integer;
  m_Row     : Integer;
  m_Col     : Integer;
  m_InfosEA : TVentil_EntreesAirLogement;
  m_NoEA    : Integer;
  m_NoSej   : Integer;
  m_NoCh    : Integer;
  ConditionVerifiee : Boolean;
  ColorPantone372 : TColor;
Begin

  if Etude.Batiment.TypeBat = c_BatimentHotel then
    begin
    AfficheResultatsHotel(_Grid);
    Exit;
    end;

  ColorPantone372 := RGB(215, 233, 161);
  For m_Row := 0 To 1 Do For m_Col := 0 To _Grid.ColCount - 1 Do Begin
    _Grid.CellProperties[m_Col, m_Row].Alignment := taCenter;
    _Grid.CellProperties[m_Col, m_Row].Alignment := taCenter;
{$IFDEF ACTHYS_DIMVMBP}
    _Grid.CellProperties[m_Col, m_Row].BrushColor := ColorPantone372;
    _Grid.CellProperties[m_Col, m_Row].BrushColorTo := ClWhite;
{$ELSE}
    _Grid.CellProperties[m_Col, m_Row].BrushColor := clSilver;
{$ENDIF}
  End;
  For m_Row := 2 To _Grid.RowCount - 1 Do Begin
    _Grid.cells[2, m_Row] := '';
    _Grid.cells[3, m_Row] := '';
  End;

if Etude.Batiment.TypeBat = c_BatimentCollectif then
begin
  _Grid.Cells[0, 0] := 'Extraction';
  _Grid.Cells[2, 0] := 'Soufflage';
  _Grid.Cells[0, 1] := 'Pi�ce'; _Grid.Cells[2, 1] := 'Pi�ce';
  _Grid.Cells[1, 1] := 'Bouche'; _Grid.Cells[3, 1] := 'Entr�e d''air';
  If IsNotNil(Systeme) Then Begin
    { Bouches }
    For m_NoPiece := 1 To c_NbPiecesHumide - 1 Do Begin
      _Grid.Cells[0, m_NoPiece + 1] := c_NomsPiecesHumides[m_NoPiece];
{$IfDef OLD_ATVENTIL}
      If Not Systeme.AutoGaz Then
        Begin
          if (m_NoPiece = c_Cuisine) and (ChaudiereCuisine = c_Non) Then
            begin
              if SystemesVMC.SystemeEquivalentNonGaz(Systeme).Bouche[TypeLogement, m_NoPiece, T3Optimise = c_Oui] <> Nil then
                _Grid.Cells[1, m_NoPiece + 1] := SystemesVMC.SystemeEquivalentNonGaz(Systeme).Bouche[TypeLogement, m_NoPiece, T3Optimise = c_Oui].GetFirstLibelle;
            end
          else
            begin
              if Systeme.Bouche[TypeLogement, m_NoPiece, T3Optimise = c_Oui] <> Nil then
                _Grid.Cells[1, m_NoPiece + 1] := Systeme.Bouche[TypeLogement, m_NoPiece, T3Optimise = c_Oui].GetFirstLibelle;
            end;
        End
      Else Begin
        If ChaudiereCuisine = c_Oui Then
          begin
            if Systeme.Bouche[TypeLogement, m_NoPiece, T3Optimise = c_Oui] <> Nil then
              _Grid.Cells[1, m_NoPiece + 1] := Systeme.Bouche[TypeLogement, m_NoPiece, T3Optimise = c_Oui].GetFirstLibelle;
          end
        Else Begin
          If m_NoPiece = c_Cuisine Then
             begin
              if SystemesVMC.SystemeEquivalentNonGaz(Systeme).Bouche[TypeLogement, m_NoPiece, T3Optimise = c_Oui] <> Nil then
                _Grid.Cells[1, m_NoPiece + 1] := SystemesVMC.SystemeEquivalentNonGaz(Systeme).Bouche[TypeLogement, m_NoPiece, T3Optimise = c_Oui].GetFirstLibelle;
             end
          Else Begin
            If CellierVentile = c_Oui Then
              begin
                if SystemesVMC.SystemeEquivalentNonGaz(Systeme).Bouche[TypeLogement, m_NoPiece, T3Optimise = c_Oui] <> Nil then
                  _Grid.Cells[1, m_NoPiece + 1] := SystemesVMC.SystemeEquivalentNonGaz(Systeme).Bouche[TypeLogement, m_NoPiece, T3Optimise = c_Oui].GetFirstLibelle;
              end
            Else
              begin
                if Systeme.Bouche[TypeLogement, m_NoPiece, T3Optimise = c_Oui] <> Nil then
                  _Grid.Cells[1, m_NoPiece + 1] := Systeme.Bouche[TypeLogement, m_NoPiece, T3Optimise = c_Oui].GetFirstLibelle;
              end;
          End;
        End;
      End;
{$Else}
    if TypeLogement <> c_TypeAutre then
    begin
      If Not Systeme.AutoGaz Then
        Begin
          if (m_NoPiece = c_Cuisine) and (ChaudiereCuisine = c_Non) Then
            begin
              if SystemesVMC.SystemeEquivalentNonGaz(Systeme).ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).Bouches[m_NoPiece] <> Nil then
                _Grid.Cells[1, m_NoPiece + 1] := SystemesVMC.SystemeEquivalentNonGaz(Systeme).ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).Bouches[m_NoPiece].GetFirstLibelle;
            end
          else
            begin
              if Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).Bouches[m_NoPiece] <> Nil then
                _Grid.Cells[1, m_NoPiece + 1] := Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).Bouches[m_NoPiece].GetFirstLibelle;
            end;
        End
      Else Begin
        If ChaudiereCuisine = c_Oui Then
          begin
            if (Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig) <> Nil) and (Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).Bouches[m_NoPiece] <> Nil) then
              _Grid.Cells[1, m_NoPiece + 1] := Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).Bouches[m_NoPiece].GetFirstLibelle;
          end
        Else Begin
          If m_NoPiece = c_Cuisine Then
             begin
              if SystemesVMC.SystemeEquivalentNonGaz(Systeme).ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).Bouches[m_NoPiece] <> Nil then
                _Grid.Cells[1, m_NoPiece + 1] := SystemesVMC.SystemeEquivalentNonGaz(Systeme).ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).Bouches[m_NoPiece].GetFirstLibelle;
             end
          Else Begin
            If CellierVentile = c_Oui Then
              begin
                if SystemesVMC.SystemeEquivalentNonGaz(Systeme).ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).Bouches[m_NoPiece] <> Nil then
                  _Grid.Cells[1, m_NoPiece + 1] := SystemesVMC.SystemeEquivalentNonGaz(Systeme).ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).Bouches[m_NoPiece].GetFirstLibelle;
              end
            Else
              begin
                if Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).Bouches[m_NoPiece] <> Nil then
                  _Grid.Cells[1, m_NoPiece + 1] := Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).Bouches[m_NoPiece].GetFirstLibelle;
              end;
          End;
        End;
      End;
    end;

{$EndIf}
    End;

{$IfDef OLD_ATVENTIL}
    { �ventuellement bouche cellier }
    If (Systeme.Gaz) And (ChaudiereCuisine = c_NON) Then Begin
      _Grid.Cells[0, c_Cellier + 1] := 'Cellier';
      _Grid.Cells[1, c_Cellier + 1] := Systeme.BoucheCellier[TypeLogement, Optimise].GetFirstLibelle
    End Else Begin
      _Grid.Cells[0, c_Cellier + 1] := '';
      _Grid.Cells[1, c_Cellier + 1] := '';
    End;
{$Else}
{$EndIf}
    m_Row := 2; 
    { Entr�es d'air }


if Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN then
        ConditionVerifiee := ((CalculDn = c_Oui) or (Etude.Batiment.CalculDN = c_Oui)) And (EntreesDAirDnChoisies) and (Etude.Batiment.ChiffrageDN = c_Oui)
else
(*
if Etude.Batiment.IsFabricantACT then
        ConditionVerifiee := false
else
*)
        ConditionVerifiee := ((CalculDn = c_Oui)) And (EntreesDAirDnChoisies);

    If ConditionVerifiee Then
    Begin
      m_InfosEA := Ventil_EntreesAirLogement[TypeLogement];
      m_NoSej := 1;
      m_NoCh  := 1;
      For m_NoPiece := 1 To m_InfosEA.Count Do Begin
        If m_InfosEA[m_NoPiece - 1].Piece = c_Sejour Then Begin
          _Grid.Cells[2, m_NoPiece + 1] := 'S�jour ' + IntToStr(m_NoSej);
          inc(m_NoSej);
        End Else Begin
          _Grid.Cells[2, m_NoPiece + 1] := 'Chambre ' + IntToStr(m_NoCh);
          inc(m_NoCh);
        End;                          
        _Grid.Cells[3, m_NoPiece + 1] := m_InfosEA[m_NoPiece - 1].EntreeChoisie.Reference;
      End;


    End Else
    If ((Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN) And (Etude.Batiment.ChiffrageDN = c_Oui)) or (not(Etude.Batiment.IsFabricantVIM) and not(Etude.Batiment.IsFabricantMVN)) Then
    For m_NoPiece := 1 To c_NbPiecesSeches Do
    Begin

{$IfDef OLD_ATVENTIL}
      If Systeme.AutoGaz And (ChaudiereCuisine = c_NON) And (CellierVentile = c_Non) Then
        _Grid.Cells[3, m_NoPiece + 1] := Systeme.EntreeAirSiCellier[TypeLogement, m_NoPiece, Optimise][0].Libelle2
      Else
        Begin
          if Systeme.EntreeAir[TypeLogement, m_NoPiece, T3Optimise = c_Oui] <> Nil then
                For m_NoEA := 0 To Systeme.EntreeAir[TypeLogement, m_NoPiece, T3Optimise = c_Oui].Count - 1 Do
                        If Systeme.EntreeAir[TypeLogement, m_NoPiece, T3Optimise = c_Oui][m_NoEA].Nombre > 0 Then
                                Begin
                                        If m_NoPiece = 1 Then
                                                _Grid.Cells[2, m_Row] := 'S�jour '// + IntToStr(m_NoEA + 1)
                                        Else
                                                _Grid.Cells[2, m_Row] := 'Chambre ';// + IntToStr(m_NoEA + 1);
                                _Grid.Cells[3, m_Row] := Systeme.EntreeAir[TypeLogement, m_NoPiece, T3Optimise = c_Oui][m_NoEA].Libelle2;
                                Inc(m_Row);
                                End;

//        _Grid.Cells[3, m_NoPiece + 1] := Systeme.EntreeAir[TypeLogement, m_NoPiece].Libelle2;
        End;
{$Else}
        Begin
        if TypeLogement <> c_TypeAutre then
        begin
          if Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[m_NoPiece][IdxSolutionEA[m_NoPiece]] <> Nil then
                For m_NoEA := 0 To Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[m_NoPiece][IdxSolutionEA[m_NoPiece]].Count - 1 Do
                        If Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[m_NoPiece][IdxSolutionEA[m_NoPiece]][m_NoEA].Nombre > 0 Then
                                Begin
                                        If m_NoPiece = 1 Then
                                                _Grid.Cells[2, m_Row] := 'S�jour '// + IntToStr(m_NoEA + 1)
                                        Else
                                                _Grid.Cells[2, m_Row] := 'Chambre ';// + IntToStr(m_NoEA + 1);
                                _Grid.Cells[3, m_Row] := Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[m_NoPiece][IdxSolutionEA[m_NoPiece]][m_NoEA].Libelle2;
                                Inc(m_Row);
                                End;

//        _Grid.Cells[3, m_NoPiece + 1] := Systeme.EntreeAir[TypeLogement, m_NoPiece].Libelle2;
        End;
        end;
      //_Grid.Cells[2, m_NoPiece + 1] := c_NomsPiecesSeches[m_NoPiece];

{$EndIf}

    End;

  End;
end
else
begin

end;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Logement.AfficheQuestion(_NoQuestion: Integer): Boolean;
Begin
  Result := False;
  Case _NoQuestion Of
    c_QLogement_Reference         : Result := True;
    c_QLogement_TypeLogement      : Result := IsCollectif;
    c_QLogement_TypeChambre       : Result := IsHotel;
    c_QLogement_VentilationGen    : Result := (Not Etude.Batiment.SystemeCollectif.DoubleFlux) and (IsCollectif);
{$IfDef OLD_ATVENTIL}
    c_QLogement_T3Optimise        : begin
                                      if Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantMVN then
                                        Result := False
                                      else
                                        Result := IsCollectif And IsNotNil(Systeme) And Systeme.AlizeIII And not(Systeme.Gaz) And (TypeLogement = c_TypeT3);
                                    end;
    c_QLogement_WCSepares         : begin
                                      if Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantMVN then
                                        Result := IsCollectif And (TypeLogement <= c_TypeT3)
                                      else
                                        Result := IsHotel;
                                    end;
    c_QLogement_IdxConfig         : Result := False;
    c_QLogement_IdxSolutionEASj   : Result := False;
    c_QLogement_IdxSolutionEACh   : Result := False;
{$Else}
    c_QLogement_T3Optimise        : Result := False;
    c_QLogement_WCSepares         : Result := False;
    c_QLogement_IdxConfig         : Result := (TypeLogement <> c_TypeAutre) and (Systeme.ConfigsLogements.NbConfigTypeLogement(TypeLogement) > 1);
    c_QLogement_IdxSolutionEASj   : Result := (TypeLogement <> c_TypeAutre) and (Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig) <> Nil) and (Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[c_Sejour] <> Nil) and (Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[c_Sejour].Count > 1);
    c_QLogement_IdxSolutionEACh   : Result := (TypeLogement <> c_TypeAutre) and (Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig) <> Nil) and (Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[c_Chambre] <> Nil) and (Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[c_Chambre].Count > 1);
{$EndIf}
    c_QLogement_Ventilation       : Result := Not IsTertiaire And (SystemeBatiment = c_Non);
    c_QLogement_PositionChaudiere : Result := IsCollectif And IsNotNil(Systeme) And Systeme.Gaz;
    c_QLogement_CellierVentile    : if Etude.Batiment.IsFabricantMVN then
                                      Result := False
                                    else
                                      Result := IsCollectif And IsNotNil(Systeme) And Systeme.Gaz;
    c_QLogement_CuisineFermee     : begin
                                      if Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantMVN then
                                        Result := False
                                      else
                                        Result := IsCollectif;
                                    end;
    c_QLogement_Description       : begin
                                      if Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantMVN then
                                        Result := False
                                      else
                                        Result := IsCollectif;
                                    end;
    c_QLogement_NbEntreeAirLocal  : Result := (Etude.Batiment.ChiffrageDN = C_OUI) and IsTertiaire;
    c_QLogement_EntreeAir1        : Result := (Etude.Batiment.ChiffrageDN = C_OUI) and IsTertiaire and (NbEntreeAirLocal >= 1);
    c_QLogement_EntreeAir2        : Result := (Etude.Batiment.ChiffrageDN = C_OUI) and IsTertiaire and (NbEntreeAirLocal >= 2);
    c_QLogement_EntreeAir3        : Result := (Etude.Batiment.ChiffrageDN = C_OUI) and IsTertiaire and (NbEntreeAirLocal >= 3);
    c_QLogement_EntreeAir4        : Result := (Etude.Batiment.ChiffrageDN = C_OUI) and IsTertiaire and (NbEntreeAirLocal >= 4);
    c_QLogement_CalculDN          : {$IfNDef AERAU_CLIMAWIN}
                                     if Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN then
                                        Result :=  (Etude.Batiment.ChiffrageDN = C_OUI) and (not IsTertiaire) and (TypeLogement <> c_TypeAutre)
                                     else
                                     if Etude.Batiment.IsFabricantACT then
                                        Result :=  False
                                     else
                                        Result := not IsTertiaire and (TypeLogement <> c_TypeAutre) and Not (Systeme.DoubleFlux);
                                     {$Else}
                                        Result := False;
                                     {$EndIf}

    c_QLogement_EntreesDairDn     : {$IfNDef AERAU_CLIMAWIN}
                                        if Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN then
                                        Result := (Etude.Batiment.ChiffrageDN = C_OUI) and (not IsTertiaire) and (TypeLogement <> c_TypeAutre) and (CalculDn = c_OUI)
                                    else
                                        Result := not IsTertiaire and (TypeLogement <> c_TypeAutre) and (CalculDn = c_OUI) and Not (Systeme.DoubleFlux);
                                    {$Else}
                                        Result := False;                                    
                                    {$EndIf}
  End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Logement.EllipsClick(_Grid: TAdvStringGrid; _Ligne: Integer): String;
Begin
  Case NoQuestion(_Ligne) Of
    c_QLogement_Description : Begin
      DescriptionLogements(Self);
      Result := 'Clic pour modifier';

        if Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN then
              if (Etude.Batiment.CalculDN = c_Oui) or (CalculDn = c_Oui) then
                begin
                ForceAcoustiqueDN(Self);
                EntreesDAirDnChoisies := true;
                end;
    End;
    c_QLogement_EntreeAir1 : Result := SelectionEntreeAir(EntreeAirSelection1);
    c_QLogement_EntreeAir2 : Result := SelectionEntreeAir(EntreeAirSelection2);
    c_QLogement_EntreeAir3 : Result := SelectionEntreeAir(EntreeAirSelection3);
    c_QLogement_EntreeAir4 : Result := SelectionEntreeAir(EntreeAirSelection4);
    c_QLogement_EntreesDairDn: begin
      EntreesDAirDnChoisies := AcoustiqueDN(self) or EntreesDAirDnChoisies;
      Result := 'Clic pour modifier';
    end;
  End;
  Recupere(_Grid, _Ligne);
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Logement.Get_EA_VIM : String;
Begin
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.ChiffrageEA_DoubleFlux(_Caisson : TVentil_Troncon);
begin

end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.ChiffrageEA_Tertiaire(_Caisson : TVentil_Troncon);
Var
  m_Entree : TEdibatec_Produit;
Begin
  If NbEntreeAirLocal >= 1 Then
    Begin
    m_Entree := nil;
        if EntreeAirSelection1 <> nil then
                m_Entree := BaseEdibatec.Produit(EntreeAirSelection1.CodeProduit);
    If IsNotNil(m_Entree) Then TVentil_Caisson(_Caisson).AddChiffrageCaisson(m_Entree, 1, cst_Vertical, cst_Chiff_EA, True, Self)
                          Else
                          if EntreeAirSelection1 <> nil then
                                TVentil_Caisson(_Caisson).AddChiffrageErreurCaisson('Entr�es d''air', EntreeAirSelection1.Reference, True, Self{$IfDeF VIM_OPTAIR}, '1 unit�'{$EndIf});
    End;
  If NbEntreeAirLocal >= 2 Then
    Begin
    m_Entree := nil;
        if EntreeAirSelection2 <> nil then
                m_Entree := BaseEdibatec.Produit(EntreeAirSelection2.CodeProduit);
    If IsNotNil(m_Entree) Then TVentil_Caisson(_Caisson).AddChiffrageCaisson(m_Entree, 1, cst_Vertical, cst_Chiff_EA, True, Self)
                          Else
                          if EntreeAirSelection2 <> nil then
                                TVentil_Caisson(_Caisson).AddChiffrageErreurCaisson('Entr�es d''air', EntreeAirSelection2.Reference, True, Self{$IfDeF VIM_OPTAIR}, '1 unit�'{$EndIf});
    End;
  If NbEntreeAirLocal >= 3 Then
    Begin
    m_Entree := nil;
        if EntreeAirSelection3 <> nil then
                m_Entree := BaseEdibatec.Produit(EntreeAirSelection3.CodeProduit);
    If IsNotNil(m_Entree) Then TVentil_Caisson(_Caisson).AddChiffrageCaisson(m_Entree, 1, cst_Vertical, cst_Chiff_EA, True, Self)
                          Else
                          if EntreeAirSelection3 <> nil then
                                TVentil_Caisson(_Caisson).AddChiffrageErreurCaisson('Entr�es d''air', EntreeAirSelection3.Reference, True, Self{$IfDeF VIM_OPTAIR}, '1 unit�'{$EndIf});
    End;
  If NbEntreeAirLocal >= 4 Then
    Begin
    m_Entree := nil;
        if EntreeAirSelection4 <> nil then
                m_Entree := BaseEdibatec.Produit(EntreeAirSelection4.CodeProduit);
    If IsNotNil(m_Entree) Then TVentil_Caisson(_Caisson).AddChiffrageCaisson(m_Entree, 1, cst_Vertical, cst_Chiff_EA, True, Self)
                          Else
                          if EntreeAirSelection4 <> nil then
                                TVentil_Caisson(_Caisson).AddChiffrageErreurCaisson('Entr�es d''air', EntreeAirSelection4.Reference, True, Self{$IfDeF VIM_OPTAIR}, '1 unit�'{$EndIf});
    End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.ChiffrageEA_Collectif(_Caisson : TVentil_Troncon);
var
        ConditionVerifiee : Boolean;
Begin
If (Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN) and (Etude.Batiment.ChiffrageDN = c_Non) Then exit;

If Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN then
        ConditionVerifiee := ((CalculDn = c_Oui) or (Etude.Batiment.CalculDN = c_Oui)) And EntreesDAirDnChoisies
else
If Etude.Batiment.IsFabricantACT then
        ConditionVerifiee := false
else
        ConditionVerifiee := ((CalculDn = c_Oui)) And EntreesDAirDnChoisies;

                If  ConditionVerifiee Then
                        ChiffrageEA_CollectifDn(_Caisson)
                Else
                        ChiffrageEA_CollectifStandard(_Caisson);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.ChiffrageEA_CollectifDn(_Caisson : TVentil_Troncon);
Var
  m_InfosEA : TVentil_EntreesAirLogement;
  m_NoPiece : Integer;
  m_Stl     : TStringList;
  m_Entree  : TEdibatec_Produit;
  i         : Integer;
  Ed1       : String;
  TempNbEA  : integer;

Begin
If (Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN) and (Etude.Batiment.ChiffrageDN = c_Non) Then exit;
  m_InfosEA := Ventil_EntreesAirLogement[TypeLogement];
  For m_NoPiece := 0 To m_InfosEA.Count - 1 Do Begin
    m_Stl := StrToStringList(m_InfosEA[m_NoPiece].EntreeChoisie.AChiffrer, '|');

    If (Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN) and (m_Stl.Count = 0) then
        TVentil_Caisson(_Caisson).AddChiffrageErreurCaisson('Entr�es d''air', m_InfosEA[m_NoPiece].EntreeChoisie.Reference, True, Self, inttostr(1) + ' unit�(s)')
    Else
    For i := 0 To m_Stl.Count - 1 Do Begin
      Ed1 := m_Stl[i];
      m_Entree := BaseEdibatec.Produit(Ed1);
        if EntreesDAirDnChoisies then
                TempNbEA := 1
        else
                TempNbEA := m_InfosEA[m_NoPiece].NbDansPiece;
      If IsNotNil(m_Entree) Then TVentil_Caisson(_Caisson).AddChiffrageCaisson(m_Entree, TempNbEA, cst_Vertical, cst_Chiff_EA, True, Self)
                            Else TVentil_Caisson(_Caisson).AddChiffrageErreurCaisson('Entr�es d''air', m_InfosEA[m_NoPiece].EntreeChoisie.Reference, True, Self{$IfDeF VIM_OPTAIR}, inttostr(TempNbEA) + ' unit�(s)'{$EndIf});
    End;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.ChiffrageEA_Accessoire(_Caisson : TVentil_Troncon; _NoEdibatec: String; _Qte : integer);
Var
  m_Entree : TEdibatec_Produit;
Begin
if not(Etude.Batiment.IsFabricantVIM) and not(Etude.Batiment.IsFabricantMVN) then
begin
  m_Entree := BaseEdibatec.Produit(_NoEdibatec);
  If IsNotNil(m_Entree) Then TVentil_Caisson(_Caisson).AddChiffrageCaisson(m_Entree, _Qte, cst_Vertical, cst_Chiff_EA, True, Self)
                        Else TVentil_Caisson(_Caisson).AddChiffrageErreurCaisson('Accessoire Entr�e d''air', '', True, Self{$IfDeF VIM_OPTAIR}, inttostr(_Qte) + ' unit�(s)'{$EndIf});
end;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.ClearChiffrageT2A;
begin
  LongueursConduits.Clear;
  if ChiffrageT2A <> Nil then
    ChiffrageT2A.Destroy;
  ChiffrageT2A := TChiffrage.Create(BaseEdibatec);
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.ChiffrageEA_CollectifStandard(_Caisson : TVentil_Troncon);
  Procedure AddEntrees(_EntrSyst : TVentil_EA; _CoefMult : Integer = 1);
  Var
    EntrEdi : TEdibatec_Produit;
    NbTotal : Integer;
  begin
      if _EntrSyst <> Nil then
        begin
        NbTotal := 0;
          if (_EntrSyst.Edibatec <> '') and (_EntrSyst.Nombre > 0) then
            begin
            NbTotal := NbTotal + _EntrSyst.Nombre;
            EntrEdi := BaseEdibatec.Produit(_EntrSyst.Edibatec);
              if EntrEdi <> Nil then
                TVentil_Caisson(_Caisson).AddChiffrageCaisson(EntrEdi, _EntrSyst.Nombre * _CoefMult, cst_Vertical, cst_Chiff_EA, True, Self)
              else
                TVentil_Caisson(_Caisson).AddChiffrageErreurCaisson('Entr�es d''air', _EntrSyst.GetFirstLibelle, True, Self{$IfDeF VIM_OPTAIR}, inttostr(_EntrSyst.Nombre * _CoefMult) + ' unit�(s)'{$EndIf});
            end;

          if _EntrSyst._2Chiff <> '' Then
            ChiffrageEA_Accessoire(_Caisson, _EntrSyst._2Chiff, NbTotal * _CoefMult);
        end
      else
        begin
        end;
  end;
Var
  m_NoEA   : Integer;
Begin
{$IfDef OLD_ATVENTIL}
  If (Systeme.AutoGaz) And (ChaudiereCuisine = c_Non) And (CellierVentile = c_Non) Then
    Begin
    { S�jour }
      For m_NoEA := 0 To Systeme.EntreeAirSiCellier[TypeLogement, c_Sejour, Optimise].Count - 1 Do
        AddEntrees(Systeme.EntreeAirSiCellier[TypeLogement, c_Sejour, Optimise][m_NoEA]);

    { Chambres }
      If TypeLogement > c_TypeT1 Then
        AddEntrees(Systeme.EntreeAirSiCellier[TypeLogement, c_Chambre, Optimise][0], TypeLogement - 1);
    End
  Else
    Begin
    { S�jour }
      For m_NoEA := 0 To Systeme.EntreeAir[TypeLogement, c_Sejour, T3Optimise = c_Oui].Count - 1 Do
        AddEntrees(Systeme.EntreeAir[TypeLogement, c_Sejour, T3Optimise = c_Oui][m_NoEA]);
    { Chambres }
      If TypeLogement > c_TypeT1 Then
        AddEntrees(Systeme.EntreeAir[TypeLogement, c_Chambre, T3Optimise = c_Oui][0], TypeLogement - 1);

    End;
{$Else}
    Begin
    { S�jour }
      For m_NoEA := 0 To Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[c_Sejour][IdxSolutionEA[c_Sejour]].Count - 1 Do
        AddEntrees(Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[c_Sejour][IdxSolutionEA[c_Sejour]][m_NoEA]);
    { Chambres }
      If TypeLogement > c_TypeT1 Then
        AddEntrees(Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[c_Chambre][IdxSolutionEA[c_Chambre]][0], TypeLogement - 1);

    End;

{$EndIf}

End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.Chiffrage_EntreesAir(_Caisson : TVentil_Troncon);
Begin
  If ((Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN) and (Etude.Batiment.ChiffrageDN = C_OUI)) or (not(Etude.Batiment.IsFabricantVIM) and not(Etude.Batiment.IsFabricantMVN)) then
        Begin
                if Etude.Batiment.TypeBat in [c_BatimentCollectif, c_BatimentHotel] then
                  begin
                    if Systeme.DoubleFlux then
                        ChiffrageEA_DoubleFlux(_Caisson)
                    else
                        ChiffrageEA_Collectif(_Caisson);
                  end
                else
                        ChiffrageEA_Tertiaire(_Caisson);
        End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.ImportXMLActhys(_Node : IXMLNode);
  procedure AddElementChiffrage(_CodeArticle : String; Var _OutPut : String);
  var
    m_Classe : TEdibatec_Classe;
    m_Gamme  : TEdibatec_Gamme;
    m_Produit: TEdibatec_EA;
    m_NoGam  : Integer;
    m_NoProd : Integer;
    ItemFind : Boolean;
  begin
  m_Classe := BaseEdibatec.Classe(c_Edibatec_EntreesAir);
  ItemFind := False;
    For m_NoGam := 0 To m_Classe.Gammes.Count - 1 Do
      Begin
      m_Gamme := m_Classe.Gammes.Gamme(m_NoGam);
        For m_NoProd := 0 To m_Gamme.Produits.Count - 1 Do
          Begin
          m_Produit := TEdibatec_EA(m_Gamme.Produits.Produit(m_NoProd));
            If (m_Produit.CodeArticle = _CodeArticle) Then
              Begin
                if _OutPut <> '' then
                  _OutPut := _OutPut + '|';
              _OutPut := _OutPut + m_Produit.CodeProduit;
              ItemFind := True;
              Break;
              End;
          end;
            if ItemFind then
                  break;
          End;
  end;
{$IFDEF ACTHYS_DIMVMBP}
Var
  cpt : Integer;
  cptAccessoires : Integer;
{$EndIf}
begin
  Inherited;
    if _Node = Nil then
      Exit;
{$IFDEF ACTHYS_DIMVMBP}
  Reference := (_Node as ImportEVenthysXSD.IXMLLOGEMENT).Ref_Logement;
  TypeLogement := Max(1, Min(c_NbTypeLogements, (_Node as ImportEVenthysXSD.IXMLLOGEMENT).Type_));

  Systeme := XML2SystemeVMC((_Node as ImportEVenthysXSD.IXMLLOGEMENT).Systeme_Particulier);
  SystemeBatiment := XML2VentilBool((_Node as ImportEVenthysXSD.IXMLLOGEMENT).Systeme_Batiment);
  IdImportXML := (_Node as ImportEVenthysXSD.IXMLLOGEMENT).Identifiant;

{$IfDef OLD_ATVENTIL}
  //On se base sur la balise T3Optimise pour simuler la configuration avec WC Commun
  T3Optimise := XML2VentilBool((_Node as ImportEVenthysXSD.IXMLLOGEMENT).WC_Commun);
    //Pour faire simple, cette valeur est invers�e par rapport � ce qu'on attend...
    if T3Optimise = c_Oui then
      T3Optimise := c_Non
    else
      T3Optimise := c_Oui;

{$Else}
  for cpt := 0 to Systeme.ConfigsLogements.NbConfigTypeLogement(TypeLogement) - 1 do
    begin
        if XML2Bool((_Node as ImportEVenthysXSD.IXMLLOGEMENT).WC_Commun) and (Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, cpt).NbSDBWC > 0) then
          begin
          IdxConfig := cpt;
          Break;
          end
        else
        if Not(XML2Bool((_Node as ImportEVenthysXSD.IXMLLOGEMENT).WC_Commun)) and (Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, cpt).NbSDBWC = 0) then
          begin
          IdxConfig := cpt;
          Break;
          end;
    end;

{$EndIf}

  //entr�es d'air
  Ventil_EntreesAirLogement[TypeLogement].ClearKeepObjects;
    for cpt := 0 to (_Node as ImportEVenthysXSD.IXMLLOGEMENT).ENTREESAIR.Count - 1 do
      begin
      Ventil_EntreesAirLogement[TypeLogement].Add(TVentil_DonneesEA.Create((_Node as ImportEVenthysXSD.IXMLLOGEMENT).ENTREESAIR[cpt].Type_Piece - 5, (_Node as ImportEVenthysXSD.IXMLLOGEMENT).ENTREESAIR[cpt].Code_Article));
      TVentil_DonneesEA(Ventil_EntreesAirLogement[TypeLogement].Last).Piece := (_Node as ImportEVenthysXSD.IXMLLOGEMENT).ENTREESAIR[cpt].Type_Piece - 5;
      TVentil_DonneesEA(Ventil_EntreesAirLogement[TypeLogement].Last).NbDansPiece := (_Node as ImportEVenthysXSD.IXMLLOGEMENT).ENTREESAIR[cpt].Quantite;
      TVentil_DonneesEA(Ventil_EntreesAirLogement[TypeLogement].Last).EntreeChoisie.Reference := (_Node as ImportEVenthysXSD.IXMLLOGEMENT).ENTREESAIR[cpt].Code_Article;
      TVentil_DonneesEA(Ventil_EntreesAirLogement[TypeLogement].Last).EntreeChoisie.AChiffrer := '';

      AddElementChiffrage((_Node as ImportEVenthysXSD.IXMLLOGEMENT).ENTREESAIR[cpt].Code_Article, TVentil_DonneesEA(Ventil_EntreesAirLogement[TypeLogement].Last).EntreeChoisie.AChiffrer);

        for cptAccessoires := 0 to (_Node as ImportEVenthysXSD.IXMLLOGEMENT).ENTREESAIR[cpt].ACCESSOIRES.Count - 1 do
          AddElementChiffrage((_Node as ImportEVenthysXSD.IXMLLOGEMENT).ENTREESAIR[cpt].ACCESSOIRES[cptAccessoires].Code_Article, TVentil_DonneesEA(Ventil_EntreesAirLogement[TypeLogement].Last).EntreeChoisie.AChiffrer);

      end;

{$ENDIF}
end;
{ *******************************************************************S******************************************************************************* }
Procedure TVentil_Logement.ExportXMLActhys(_Node : IXMLNode);
begin
{$IFDEF ACTHYS_DIMVMBP}
  (_Node as ExportEVenthysXSD.IXMLLOGEMENT).Ref_Logement := Reference;
  (_Node as ExportEVenthysXSD.IXMLLOGEMENT).Type_ := TypeLogement;
{$IfDef OLD_ATVENTIL}
  //On se base sur la balise T3Optimise pour simuler la configuration avec WC Commun
    //Pour faire simple, cette valeur est invers�e par rapport � ce qu'on attend...
    if T3Optimise = c_Oui then
{$Else}
    if Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).NbSDBWC = 0 then
{$EndIf}
      (_Node as ExportEVenthysXSD.IXMLLOGEMENT).WC_Commun := VentilBool2XML(c_Non)
    else
      (_Node as ExportEVenthysXSD.IXMLLOGEMENT).WC_Commun := VentilBool2XML(c_Oui);
  (_Node as ExportEVenthysXSD.IXMLLOGEMENT).Systeme_Particulier := SystemeVMC2XML(Systeme);
  (_Node as ExportEVenthysXSD.IXMLLOGEMENT).Systeme_Batiment := VentilBool2XML(SystemeBatiment);
    if IdImportXML > -1 then
          (_Node as ExportEVenthysXSD.IXMLLOGEMENT).Identifiant := IdImportXML
        else
          (_Node as ExportEVenthysXSD.IXMLLOGEMENT).Identifiant := Etude.Batiment.Logements.IndexOf(Self);

  TChiffrage(ChiffrageT2A).ExportXMLActhys((_Node as ExportEVenthysXSD.IXMLLOGEMENT).QUANTITATIF);
{$ENDIF}
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.ExportXMLActhys2ndPasse(_Node : IXMLNode);
begin
{$IFDEF ACTHYS_DIMVMBP}
  TChiffrage(ChiffrageT2A).ExportXMLActhys2ndPasse((_Node as ExportEVenthysXSD.IXMLLOGEMENT).QUANTITATIF);
{$ENDIF}
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.Save;
Begin
  Table_Etude_Logement.Donnees.Insert;
  Table_Etude_Logement.Donnees.FieldByName('ID_LOGEMENT').AsWideString := GUIDToString(Id);
  {$IfDef AERAU_CLIMAWIN}
  Table_Etude_Logement.Donnees.FieldByName('IDRESEAU').AsWideString := Etude.IdReseau;
  {$EndIf}
        if Trim(Reference) <> '' then
                Table_Etude_Logement.Donnees.FieldByName('REFERENCE').AsWideString := Reference
        Else
                Table_Etude_Logement.Donnees.FieldByName('REFERENCE').AsWideString := 'None';
  Table_Etude_Logement.Donnees.FieldByName('TYPE_LOGEMENT').AsInteger := TypeLogement;
{$IfDef OLD_ATVENTIL}
  Table_Etude_Logement.Donnees.FieldByName('T3_OPTIMISE').AsInteger := T3Optimise;
{$Else}
  Table_Etude_Logement.Donnees.FieldByName('IDXCONFIG').AsInteger := IdxConfig;
  Table_Etude_Logement.Donnees.FieldByName('IDXSOLUTIONEASJ').AsInteger := IdxSolutionEA[c_Sejour];
  Table_Etude_Logement.Donnees.FieldByName('IDXSOLUTIONEACH').AsInteger := IdxSolutionEA[c_Chambre];
{$EndIf}
  Table_Etude_Logement.Donnees.FieldByName('VENTILATION_BATIMENT').AsInteger := SystemeBatiment;
{$IfDef OLD_ATVENTIL}
  If IsNotNil(Systeme) Then Table_Etude_Logement.Donnees.FieldByName('ID_VENTILATION').AsWideString := GUIDToString(Systeme.Id)
                       Else Table_Etude_Logement.Donnees.FieldByName('ID_VENTILATION').AsWideString := '';
{$Else}
  If IsNotNil(Systeme) Then Table_Etude_Logement.Donnees.FieldByName('ID_VENTILATIONXML').AsWideString := Systeme.IdAT
                       Else Table_Etude_Logement.Donnees.FieldByName('ID_VENTILATIONXML').AsWideString := '';
  //On sauvegarde l'ancien id firebird, par pr�caution (si le xml de correspondance n'est pas saisi)
  If IsNotNil(Systeme) Then Table_Etude_Logement.Donnees.FieldByName('ID_VENTILATION').AsWideString := GUIDToString(Systeme.Id)
                       Else Table_Etude_Logement.Donnees.FieldByName('ID_VENTILATION').AsWideString := '';
{$EndIf}
  Table_Etude_Logement.Donnees.FieldByName('POSITION_CHAUDIERE').AsInteger := ChaudiereCuisine;
  Table_Etude_Logement.Donnees.FieldByName('CELLIER_VENTILE').AsInteger := CellierVentile;
  Table_Etude_Logement.Donnees.FieldByName('TR_CUISINE').AsFloat := Tr_Cuisine;
  Table_Etude_Logement.Donnees.FieldByName('NB_ENTREEAIR').AsInteger := NbEntreeAirLocal;
  If EntreeAirSelection1 <> nil Then Table_Etude_Logement.Donnees.FieldByName('CODE_ENTREEAIR1').AsWideString := EntreeAirSelection1.CodeProduit
                            Else Table_Etude_Logement.Donnees.FieldByName('CODE_ENTREEAIR1').AsWideString := '';
  If EntreeAirSelection2 <> nil Then Table_Etude_Logement.Donnees.FieldByName('CODE_ENTREEAIR2').AsWideString := EntreeAirSelection2.CodeProduit
                            Else Table_Etude_Logement.Donnees.FieldByName('CODE_ENTREEAIR2').AsWideString := '';
  If EntreeAirSelection3 <> nil Then Table_Etude_Logement.Donnees.FieldByName('CODE_ENTREEAIR3').AsWideString := EntreeAirSelection3.CodeProduit
                            Else Table_Etude_Logement.Donnees.FieldByName('CODE_ENTREEAIR3').AsWideString := '';
  If EntreeAirSelection4 <> nil Then Table_Etude_Logement.Donnees.FieldByName('CODE_ENTREEAIR4').AsWideString := EntreeAirSelection4.CodeProduit
                            Else Table_Etude_Logement.Donnees.FieldByName('CODE_ENTREEAIR4').AsWideString := '';
  Table_Etude_Logement.Donnees.FieldByName('CALCULDN').AsInteger := CalculDn;
  Table_Etude_Logement.Donnees.FieldByName('CHOIXDN').AsInteger :=  Bool2Int(EntreesDAirDnChoisies);
  Table_Etude_Logement.Donnees.FieldByName('CUISINE_FERMEE').AsInteger :=  CuisineFermee;
  Table_Etude_Logement.Donnees.FieldByName('IDIMPORTXML').AsInteger := IdImportXML;
  SaveDescription;
  SaveAcoustDn;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.SaveAcoustDn;
Var
  Infos    : TVentil_DonneesEA;
  Type_Lgt : SmallInt;
  NoEA_Dn  : SmallInt;
Begin
  For Type_Lgt := C_TypeT1 To C_TypeT7 Do
    For NoEa_Dn := 0 To Ventil_EntreesAirLogement[Type_Lgt].Count - 1 Do Begin
      Infos := Ventil_EntreesAirLogement[Type_Lgt][NoEa_Dn];

      If Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantMVN or Etude.Batiment.IsFabricantVIM or ((not(Etude.Batiment.IsFabricantVIM and not(Etude.Batiment.IsFabricantMVN))) and (Infos.EntreeChoisie.Numero <> '-1')) Then
      Begin
        Table_Etude_DnLogement.Donnees.Insert;
        Table_Etude_DnLogement.Donnees.FieldByName('ID_LOGEMENT').AsWideString := GUIDToString(Id);
        {$IfDef AERAU_CLIMAWIN}
        Table_Etude_DnLogement.Donnees.FieldByName('IDRESEAU').AsWideString := Etude.IdReseau;
        {$EndIf}
        Table_Etude_DnLogement.Donnees.FieldByName('TYPE_LOGEMENT').AsInteger := Type_Lgt;
        Table_Etude_DnLogement.Donnees.FieldByName('NO_INFO').AsInteger := NoEA_Dn;
        Table_Etude_DnLogement.Donnees.FieldByName('EDIBATEC').AsWideString := Infos.EntreeChoisie.Edibatec;
        Table_Etude_DnLogement.Donnees.FieldByName('REFERENCE').AsWideString := Infos.EntreeChoisie.Reference;
        Table_Etude_DnLogement.Donnees.FieldByName('NUMERO').AsInteger :=  StrToInt(Infos.EntreeChoisie.Numero);
        Table_Etude_DnLogement.Donnees.FieldByName('TYPE_POSE').AsInteger := Infos.EntreeChoisie.Pose;
        Table_Etude_DnLogement.Donnees.FieldByName('A_CHIFFRER').AsWideString := Infos.EntreeChoisie.AChiffrer;
        Table_Etude_DnLogement.Donnees.FieldByName('EA_CLASSEMENT').AsInteger := Infos.Classement;
        Table_Etude_DnLogement.Donnees.FieldByName('EA_INSTALLATION').AsInteger := Infos.Installation;
        Table_Etude_DnLogement.Donnees.FieldByName('EA_DEBIT').AsInteger := Infos.Debit;
        Table_Etude_DnLogement.Donnees.FieldByName('EA_REFERENCE').AsWideString := Infos.Reference;
        Table_Etude_DnLogement.Donnees.FieldByName('EA_REFERENCES_VIM').AsWideString := Infos.References_VIM;
        Table_Etude_DnLogement.Donnees.FieldByName('EA_REFERENCES_CHIFFRAGE').AsWideString := Infos.References_Chiffrage;
        Table_Etude_DnLogement.Donnees.FieldByName('SURFACE').AsFloat := Infos.SurfacePiece;
        Table_Etude_DnLogement.Donnees.FieldByName('HAUTEUR').AsFloat := Infos.HauteurPiece;
        Table_Etude_DnLogement.Donnees.FieldByName('CLASSEMENT').AsInteger := Infos.Cl_Facade;
        Table_Etude_DnLogement.Donnees.FieldByName('ISOL').AsInteger := Infos.Isol_facade;
        Table_Etude_DnLogement.Donnees.Post;
      End;
    End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.LoadAcoustDn;
Var
  Infos  : TVentil_DonneesEA;
Begin
  Table_Etude_DnLogement.Donnees.Filter := 'ID_LOGEMENT=''' + GUIDToString(Id) + '''';
  Table_Etude_DnLogement.Donnees.Filtered := True;
  Table_Etude_DnLogement.Donnees.First;
  While Not Table_Etude_DnLogement.Donnees.Eof Do Begin

    If Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantMVN or Etude.Batiment.IsFabricantVIM or ((not(Etude.Batiment.IsFabricantVIM) and not(Etude.Batiment.IsFabricantMVN) )and (Table_Etude_DnLogement.Donnees.FieldByName('NUMERO').AsInteger <> -1)) Then
    Begin
    try
    if Table_Etude_DnLogement.Donnees.FieldByName('NO_INFO').AsInteger <= Ventil_EntreesAirLogement[Table_Etude_DnLogement.Donnees.FieldByName('TYPE_LOGEMENT').AsInteger].count - 1 then
    begin
      Infos := Ventil_EntreesAirLogement[Table_Etude_DnLogement.Donnees.FieldByName('TYPE_LOGEMENT').AsInteger][Table_Etude_DnLogement.Donnees.FieldByName('NO_INFO').AsInteger];
      Infos.EntreeChoisie.Edibatec := Table_Etude_DnLogement.Donnees.FieldByName('EDIBATEC').AsWideString;
      Infos.EntreeChoisie.Reference:= Table_Etude_DnLogement.Donnees.FieldByName('REFERENCE').AsWideString;
      Infos.EntreeChoisie.Numero   := Table_Etude_DnLogement.Donnees.FieldByName('NUMERO').AsWideString;
      Infos.EntreeChoisie.Pose     := Table_Etude_DnLogement.Donnees.FieldByName('TYPE_POSE').AsInteger;
      Infos.EntreeChoisie.AChiffrer:= Table_Etude_DnLogement.Donnees.FieldByName('A_CHIFFRER').AsWideString;
      Infos.Classement             := Table_Etude_DnLogement.Donnees.FieldByName('EA_CLASSEMENT').AsInteger;
      Infos.Installation           := Table_Etude_DnLogement.Donnees.FieldByName('EA_INSTALLATION').AsInteger;
      Infos.Debit                  := Table_Etude_DnLogement.Donnees.FieldByName('EA_DEBIT').AsInteger;
      Infos.Reference              := Table_Etude_DnLogement.Donnees.FieldByName('EA_REFERENCE').AsWideString;
      Infos.References_VIM         := Table_Etude_DnLogement.Donnees.FieldByName('EA_REFERENCES_VIM').AsWideString;
      Infos.References_Chiffrage   := Table_Etude_DnLogement.Donnees.FieldByName('EA_REFERENCES_CHIFFRAGE').AsWideString;
      Infos.SurfacePiece           := Table_Etude_DnLogement.Donnees.FieldByName('SURFACE').AsFloat;
      Infos.HauteurPiece           := Table_Etude_DnLogement.Donnees.FieldByName('HAUTEUR').AsFloat;
      Infos.Cl_Facade              := Table_Etude_DnLogement.Donnees.FieldByName('CLASSEMENT').AsInteger;
      Infos.Isol_facade            := Table_Etude_DnLogement.Donnees.FieldByName('ISOL').AsInteger;
    end;
    except
    end;
    End;
    Table_Etude_DnLogement.Donnees.Next;
  End;
  Table_Etude_DnLogement.Donnees.Filtered := False;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.SaveDescription;
Var
  m_NoPiece: Integer;
Begin
  For m_NoPiece := Low(InfosPieces) To High(InfosPieces) Do Begin
    Table_Etude_DimLogement.Donnees.Insert;
    Table_Etude_DimLogement.Donnees.FieldByName('ID_LOGEMENT').AsWideString := GUIDToString(Id);
    {$IfDef AERAU_CLIMAWIN}
    Table_Etude_DimLogement.Donnees.FieldByName('IDRESEAU').AsWideString := Etude.IdReseau;
    {$EndIf}
    Table_Etude_DimLogement.Donnees.FieldByName('ID_PIECE').AsInteger := m_NoPiece;
    Table_Etude_DimLogement.Donnees.FieldByName('HT_SOUSPLAFOND').AsFloat := InfosPieces[m_NoPiece, 1];
    Table_Etude_DimLogement.Donnees.FieldByName('SURFACE').AsFloat := InfosPieces[m_NoPiece, 2];
    Table_Etude_DimLogement.Donnees.Post;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.LoadDescription;
Var
  m_DS    : TDataSet;
  m_Req   : String;
Begin
  m_Req := 'SELECT * FROM ' + Cst_PrefixBanques + 'DIMLOGEMENT WHERE ID_LOGEMENT = ''' + GUIDToString(Id) + '''';
  m_Ds := Base_Etude.Requete_Selection(m_Req);
  m_Ds.First;
  While Not m_DS.Eof Do Begin
    InfosPieces[m_DS.FieldByName('ID_PIECE').AsInteger, 1] := m_DS.FieldByName('HT_SOUSPLAFOND').AsFloat;
    InfosPieces[m_DS.FieldByName('ID_PIECE').AsInteger, 2] := m_DS.FieldByName('SURFACE').AsFloat;
    m_DS.Next;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.Load;
Var
  m_No  : Integer;
{$IfDef OLD_ATVENTIL}
{$Else}
  SystemeTrouve : Boolean;
  m_NoOldSysteme  : Integer;
{$EndIf}
  m_Str : String;
  ConditionVerifiee : Boolean;
Begin

  Id := Str2GUID(Table_Etude_Logement.Donnees.FieldByName('ID_LOGEMENT').AsWideString);
  Reference    := Table_Etude_Logement.Donnees.FieldByName('REFERENCE').AsWideString;
    if Reference = 'None' then
      Reference := '';

{$IfDef OLD_ATVENTIL}
{$Else}
  SystemeTrouve := False;
{$EndIf}

  TypeLogement := Table_Etude_Logement.Donnees.FieldByName('TYPE_LOGEMENT').AsInteger;
{$IfDef OLD_ATVENTIL}
  T3Optimise   := Table_Etude_Logement.Donnees.FieldByName('T3_OPTIMISE').AsInteger;
{$Else}
  //pour compatibilit� avec les anciennes �tudes
    If (Table_Etude_Logement.Donnees.FieldByName('ID_VENTILATIONXML').AsWideString = '') then
      If IsHotel then
        TypeLogement := TypeLogement + 1;
{$EndIf}

  SystemeBatiment := Table_Etude_Logement.Donnees.FieldByName('VENTILATION_BATIMENT').AsInteger;
{$IfDef OLD_ATVENTIL}
  If Table_Etude_Logement.Donnees.FieldByName('ID_VENTILATION').AsWideString <> '' Then
    For m_No := 0 To SystemesVMC.Count - 1 Do If GUIDToString(SystemesVMC[m_No].Id) = Table_Etude_Logement.Donnees.FieldByName('ID_VENTILATION').AsWideString Then
      Systeme := SystemesVMC[m_No];
{$Else}
  //Pour compatibilit� avec anciens AT Firebird
  If (Table_Etude_Logement.Donnees.FieldByName('ID_VENTILATIONXML').AsWideString = '') and
     (Table_Etude_Logement.Donnees.FieldByName('ID_VENTILATION').AsWideString <> '') Then
      begin
        For m_No := 0 To SystemesVMC.Count - 1 Do If GUIDToString(SystemesVMC[m_No].Id) = Table_Etude_Logement.Donnees.FieldByName('ID_VENTILATION').AsWideString Then
          begin
          Systeme := SystemesVMC[m_No];
          SystemeTrouve := True;
          Break;
          end;
      end
     else
      begin
        If Table_Etude_Logement.Donnees.FieldByName('ID_VENTILATIONXML').AsWideString <> '' Then
          For m_No := 0 To SystemesVMC.Count - 1 Do If SystemesVMC[m_No].IdAT = Table_Etude_Logement.Donnees.FieldByName('ID_VENTILATIONXML').AsWideString Then
            begin
            Systeme := SystemesVMC[m_No];
            SystemeTrouve := True;
            break;
            end;
      end;

    if Not(SystemeTrouve) then
      For m_No := 0 To SystemesVMC.Count - 1 Do
        begin
          if SystemeTrouve then
            Break;
          For m_NoOldSysteme := 0 To SystemesVMC[m_No].OldATS.Count - 1 Do
            If SystemesVMC[m_No].OldATS[m_NoOldSysteme] = Table_Etude_Logement.Donnees.FieldByName('ID_VENTILATIONXML').AsWideString Then
              begin
              Systeme := SystemesVMC[m_No];
              SystemeTrouve := True;
              Break;
              end;
        end;

    if Table_Etude_Logement.Donnees.FieldByName('IDXCONFIG').AsWideString = '' then
      begin
        for m_No := 0 to Systeme.ConfigsLogements.NbConfigTypeLogement(TypeLogement) - 1 do
          begin
            if (Table_Etude_Logement.Donnees.FieldByName('T3_OPTIMISE').AsInteger = c_Oui) and Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, m_No).Optimise then
              begin
              IdxConfig := m_No;
              Break;
              end
            else
            if (Table_Etude_Logement.Donnees.FieldByName('T3_OPTIMISE').AsInteger = c_Non) and Not(Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, m_No).Optimise) then
              begin
              IdxConfig := m_No;
              Break;
              end
          end;
      end
    else
  IdxConfig   := Table_Etude_Logement.Donnees.FieldByName('IDXCONFIG').AsInteger;
    if Table_Etude_Logement.Donnees.FieldByName('IDXSOLUTIONEASJ').AsWideString <> '' then
      IdxSolutionEA[c_Sejour] := Table_Etude_Logement.Donnees.FieldByName('IDXSOLUTIONEASJ').AsInteger;
    if Table_Etude_Logement.Donnees.FieldByName('IDXSOLUTIONEACH').AsWideString <> '' then
      IdxSolutionEA[c_Chambre] := Table_Etude_Logement.Donnees.FieldByName('IDXSOLUTIONEACH').AsInteger;
{$EndIf}

  MajEntreesAir;
  ChaudiereCuisine := Table_Etude_Logement.Donnees.FieldByName('POSITION_CHAUDIERE').AsInteger;
  CellierVentile := Table_Etude_Logement.Donnees.FieldByName('CELLIER_VENTILE').AsInteger;
  // Tr_Cuisine     := Table_Etude_Logement.Donnees.FieldByName('TR_CUISINE').AsFloat;
  NbEntreeAirLocal := Table_Etude_Logement.Donnees.FieldByName('NB_ENTREEAIR').AsInteger;
  m_Str         := Table_Etude_Logement.Donnees.FieldByName('CODE_ENTREEAIR1').AsWideString;
  If m_Str <> '' Then EntreeAirSelection1 := TEdibatec_Produit(BaseEdibatec.Produit(m_Str))
                 Else EntreeAirSelection1 := nil;
  m_Str         := Table_Etude_Logement.Donnees.FieldByName('CODE_ENTREEAIR2').AsWideString;
  If m_Str <> '' Then EntreeAirSelection2 := TEdibatec_Produit(BaseEdibatec.Produit(m_Str))
                 Else EntreeAirSelection2 := nil;
  m_Str         := Table_Etude_Logement.Donnees.FieldByName('CODE_ENTREEAIR3').AsWideString;
  If m_Str <> '' Then EntreeAirSelection3 := TEdibatec_Produit(BaseEdibatec.Produit(m_Str))
                 Else EntreeAirSelection3 := nil;
  m_Str         := Table_Etude_Logement.Donnees.FieldByName('CODE_ENTREEAIR4').AsWideString;
  If m_Str <> '' Then EntreeAirSelection4 := TEdibatec_Produit(BaseEdibatec.Produit(m_Str))
                 Else EntreeAirSelection4 := nil;

    CalculDn              := Table_Etude_Logement.Donnees.FieldByName('CALCULDN').AsInteger;
    EntreesDAirDnChoisies := Int2Bool(Table_Etude_Logement.Donnees.FieldByName('CHOIXDN').AsInteger);
    CuisineFermee         := Table_Etude_Logement.Donnees.FieldByName('CUISINE_FERMEE').AsInteger;

    IdImportXML         := Table_Etude_Logement.Donnees.FieldByName('IDIMPORTXML').AsInteger;

  LoadDescription;

if Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN then
        ConditionVerifiee := ((CalculDn = c_Oui) or (Etude.Batiment.CalculDN = c_Oui)) And (EntreesDAirDnChoisies)
else
if Etude.Batiment.IsFabricantACT then
        ConditionVerifiee := True
else
        ConditionVerifiee := ((CalculDn = c_Oui)) And (EntreesDAirDnChoisies);

  If ConditionVerifiee Then LoadAcoustDn;

End;
{ ************************************************************************************************************************************************** }
Function TVentil_Logement.UtiliseCalcul: Boolean;
Var
  m_No : Integer;
Begin
  Result := False;
  For m_No := 0 To Etude.Batiment.Composants.Count - 1 Do
    If (Etude.Batiment.Composants[m_No].InheritsFrom(TVentil_Collecteur)) and (TVentil_Collecteur(Etude.Batiment.Composants[m_No]).Logement = Self) Then Begin
      Result := true;
      Break;
    End;
End;
{ ************************************************************************************************************************************************** }
Constructor TVentil_Logement.Create;
Var
  i: Integer;
Begin
  Inherited;
  LongueursConduits := TVentil_Longueurs.Create(Nil, Self);
  IdImportXML := -1;
  IsTertiaire := Etude.Batiment.TypeBat = c_BatimentTertiaire;
  IsCollectif := Etude.Batiment.TypeBat = c_BatimentCollectif;
  IsHotel := Etude.Batiment.TypeBat = c_BatimentHotel;
  If IsTertiaire Then Begin
    TypeLogement     := c_TypeAutre;
    Reference        := 'Local';
  End Else
  If IsHotel Then Begin
    TypeLogement     := c_TypeCh2;
    Reference        := 'Chambre';
  End Else Begin
    TypeLogement     := c_TypeT1;
    Reference        := NomParDefaut;
  End;
{$IfDef OLD_ATVENTIL}
  {$IfDef ANJOS_OPTIMA3D}
  T3Optimise       := c_Oui;
  {$Else}
  T3Optimise       := c_Non;
  {$EndIf}
{$Else}
    IdxConfig         := 0;
      for i := 1 to c_NbPiecesSeches do
        IdxSolutionEA[i]     := 0;
{$EndIf}
  SystemeBatiment  := c_Oui;
  Systeme          := Etude.Batiment.SystemeCollectif;
  ChaudiereCuisine := c_Oui;
  CellierVentile   := c_Oui;
  Tr_Cuisine       := 0.5;
  NbEntreeAirLocal := 1;
  EntreeAirSelection1 := nil;
  EntreeAirSelection2 := nil;
  EntreeAirSelection3 := nil;
  EntreeAirSelection4 := nil;
  CalculDn            := c_NON;
  EntreesDAirDnChoisies := False;
  CuisineFermee    := c_Oui;

  { On initialise les hauteurs de pi�ce extraction � 2.5m }
  For i := 1 To c_NbPiecesHumide Do InfosPieces[i,1] := max(0, Etude.BAtiment.Ht_DefautEtages {$IfDef VIM_OPTAIR} - 0.2 {$EnDif});
  { Infos pour l'acoustique Dn }
  For i := c_TypeT1 To c_TypeT7 Do Ventil_EntreesAirLogement[i] := TVentil_EntreesAirLogement.Create;
  MajEntreesAir;
  NbQuestions := c_NbQ_Logement;

if Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN then
        if (Etude.Batiment.CalculDN = c_Oui) or (CalculDn = c_Oui) then
                begin
                ForceAcoustiqueDN(Self);
                EntreesDAirDnChoisies := true;
                end;

  ChiffrageT2A := TChiffrage.Create(BaseEdibatec);

End;
{ ************************************************************************************************************************************************** }
Destructor TVentil_Logement.Destroy;
begin
  ChiffrageT2A.Destroy;
  LongueursConduits.Destroy;
  Inherited;
end;
{ ************************************************************************************************************************************************** }
class Function TVentil_Logement.NomParDefaut : String;
Begin
Result := 'Logement';
End;
{ ************************************************************************************************************************************************** }
class Function TVentil_Logement.FamilleObjet : String;
Begin
Result := 'Logement';
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.AddChiffrage(_Produit: TEdibatec_Produit; _Quantite: Real; _CategorieChiffrage, _CodeClasseChiffrage: Integer; _LibelleSup : String = '');
begin
  ChiffrageT2A.AddProduitChiffrage(_Produit, _Quantite, _CategorieChiffrage, _CodeClasseChiffrage, _LibelleSup);
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.AddChiffrageErreur(Const _TypeProduit, _Supplement: String; _Supplement2: String = '');
var
        TempText : String;
Begin

    TempText := _TypeProduit + ': ' + _Supplement;
        if Trim(_Supplement2) <> '' then
                TempText := TempText + ', ' + _Supplement2;

    ChiffrageT2A.Erreurs.Add(TempText);
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Logement.Snb_Cuisine : Integer;
var
  cpt : Integer;
begin
  Result := 0;
    for cpt := Low(nb_Cuisine) to High(nb_Cuisine) do
      Result := Result + nb_Cuisine[cpt];
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Logement.Snb_Sdb : Integer;
var
  cpt : Integer;
begin
  Result := 0;
    for cpt := Low(nb_Sdb) to High(nb_Sdb) do
      Result := Result + nb_Sdb[cpt];
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Logement.Snb_WC: Integer;
var
  cpt : Integer;
begin
  Result := 0;
    for cpt := Low(nb_WC) to High(nb_WC) do
      Result := Result + nb_WC[cpt];
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Logement.Snb_Sdb_WC_Unique : Integer;
var
  cpt : Integer;
begin
  Result := 0;
    for cpt := Low(nb_Sdb_WC_Unique) to High(nb_Sdb_WC_Unique) do
      Result := Result + nb_Sdb_WC_Unique[cpt];
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Logement.Snb_Sdb_Unique : Integer;
var
  cpt : Integer;
begin
  Result := 0;
    for cpt := Low(nb_Sdb_Unique) to High(nb_Sdb_Unique) do
      Result := Result + nb_Sdb_Unique[cpt];
end;
{ ************************************************************************************************************************************************** }
function TVentil_Logement.Snb_Logements : Integer;
begin
  Case Etude.Batiment.TypeBat of
    c_BatimentCollectif : Result := Snb_Cuisine;
    c_BatimentTertiaire : Result := 0;
    c_BatimentHotel     : begin
{$IfDef OLD_ATVENTIL}
                            if T3Optimise = c_Oui then
                              Result := Snb_Sdb_Unique
                            else
                              Result := Snb_Sdb_WC_Unique;
{$Else}
                            if Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).NbSDBWC = 0 then
                              Result := Snb_Sdb_Unique
                            else
                              Result := Snb_Sdb_WC_Unique;
{$EndIf}
                          end
  else
    Result := 0;
  end;
end;
{ ************************************************************************************************************************************************** }
function TVentil_Logement.Nb_Logements(Index : Integer) : Integer;
begin
  Case Etude.Batiment.TypeBat of
    c_BatimentCollectif : Result := nb_Cuisine[Index];
    c_BatimentTertiaire : Result := 0;
    c_BatimentHotel     : begin
{$IfDef OLD_ATVENTIL}
                            if T3Optimise = c_Oui then
                              Result := nb_Sdb_Unique[Index]
                            else
                              Result := nb_Sdb_WC_Unique[Index];
{$Else}
                            if Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).NbSDBWC = 0 then
                              Result := nb_Sdb_Unique[Index]
                            else
                              Result := nb_Sdb_WC_Unique[Index];
{$EndIf}
                          end
  else
    Result := 0;
  end;
end;
{ ************************************************************************************************************************************************** }
function TVentil_Logement.LibTypeLogement : String;
begin
  Case Etude.Batiment.TypeBat of
    c_BatimentCollectif : Result := c_TypesLogements[TypeLogement];
    c_BatimentTertiaire : Result := '';
    c_BatimentHotel     : Result := c_TypesChambres[TypeLogement];
  else
    Result := '';
  end;
end;
{ ************************************************************************************************************************************************** }
function TVentil_Logement.Optimise : Boolean;
begin
{$IfDef OLD_ATVENTIL}
  Result := T3Optimise = c_Oui;
{$Else}
  Result := (Systeme <> Nil) and Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).Optimise;
{$EndIf}
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Logement.SelectionEntreeAir(var EntreeAirS : TEdibatec_Produit): String;
Var
  m_CodeProduit : String;
Begin
  If IsNotNil(EntreeAirS) Then m_CodeProduit := EntreeAirS.CodeProduit
                               Else m_CodeProduit := 'VMCENTR';
  m_CodeProduit := SelectionDunProduit(m_CodeProduit);
  If m_CodeProduit <> '' Then Begin
    EntreeAirS :=  TEdibatec_Bouche(BaseEdibatec.Produit(m_CodeProduit));
    Result := EntreeAirS.Reference;
  End Else Result := ' *** ';
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.Recupere(_Grid: TAdvStringGrid; _Ligne: Integer);
var
  cptPiece : Integer;
Begin
  If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QLogement_Reference         : Reference        := _Grid.Cells[1, _Ligne];
    c_QLogement_TypeLogement,
    c_QLogement_TypeChambre       : Begin
      TypeLogement := TVentil_TypeCombo(_Grid.Combobox.Items.Objects[_Grid.Combobox.ItemIndex]).idx;
{$IfDef OLD_ATVENTIL}
{$Else}
        if IdxConfig > Systeme.ConfigsLogements.NbConfigTypeLogement(TypeLogement) - 1 then
          IdxConfig := 0;
        if Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig) <> Nil then
          for cptPiece := 1  to c_NbPiecesSeches do
           begin
              if IdxSolutionEA[cptPiece] > Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[cptPiece].Count - 1 then
                IdxSolutionEA[cptPiece] := 0;
            end;
{$EndIf}
      MajEntreesAir;
    End;
    c_QLogement_T3Optimise,
    c_QLogement_WCSepares         : Begin
{$IfDef OLD_ATVENTIL}
                                    T3Optimise := _Grid.Combobox.ItemIndex;
                                    MajEntreesAir;
{$Else}
{$EndIf}
                                    End;
{$IfDef OLD_ATVENTIL}
    c_QLogement_IdxConfig         : ;
    c_QLogement_IdxSolutionEASj   : ;
    c_QLogement_IdxSolutionEACh   : ;
{$Else}
    c_QLogement_IdxConfig         : begin
                                    IdxConfig     := _Grid.Combobox.ItemIndex;
                                      for cptPiece := 1 to c_NbPiecesSeches do
                                        begin
                                          if IdxSolutionEA[cptPiece] > Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[cptPiece].Count - 1 then
                                            IdxSolutionEA[cptPiece] := 0;
                                        end;
                                    MajEntreesAir;;
                                    end;
    c_QLogement_IdxSolutionEASj    : begin
                                    IdxSolutionEA[c_Sejour]     := _Grid.Combobox.ItemIndex;
                                    MajEntreesAir;;
                                    end;
    c_QLogement_IdxSolutionEACh    : begin
                                    IdxSolutionEA[c_Chambre]     := _Grid.Combobox.ItemIndex;
                                    MajEntreesAir;;
                                    end;
{$EndIf}
    c_QLogement_VentilationGen    : Begin
      SystemeBatiment  := _Grid.Combobox.ItemIndex;
      If SystemeBatiment = c_OUI Then Systeme := Etude.Batiment.SystemeCollectif;
      MajEntreesAir;
        if Etude.Batiment.IsFabricantMVN and Systeme.BassePression and (Etude.SelectedObject <> Nil) and Etude.SelectedObject.InheritsFrom(TVentil_Bouche) then
          Etude.SelectedObject.Affiche(Etude.GridProp);
    End;
    c_QLogement_Ventilation       : Begin
      Systeme := TVentil_Systeme(_Grid.Combobox.Items.Objects[_Grid.Combobox.ItemIndex]);
      SystemesVMC.HideInvalidSystems(Systeme);

        if Etude.Batiment.IsFabricantVIM then
        if Systeme.Gaz then
                Etude.Batiment.SystemeCollectif := Systeme;

      MajEntreesAir;
        if Etude.Batiment.IsFabricantMVN and Systeme.BassePression and (Etude.SelectedObject <> Nil) and Etude.SelectedObject.InheritsFrom(TVentil_Bouche) then
          begin
          InterfaceCAD.Aero_Vilo2Fredo_Refresh(rtAll);
          TVentil_Bouche(Etude.SelectedObject).UpdateAffichageBoucheInterface(True);
          end;
        if Etude.Batiment.IsFabricantMVN and Systeme.BassePression and (Etude.SelectedObject <> Nil) then
          Etude.SelectedObject.AfficheSynthese207(Etude.GridSynthese207);
    End;
    c_QLogement_PositionChaudiere : ChaudiereCuisine := _Grid.Combobox.ItemIndex;
    c_QLogement_CellierVentile    : CellierVentile   :=  _Grid.Combobox.ItemIndex;
    c_QLogement_CuisineFermee     : CuisineFermee    := _Grid.Combobox.ItemIndex;
    c_QLogement_Description       : ; // Rien dans le cas des boutons
    c_QLogement_NbEntreeAirLocal  : NbEntreeAirLocal := _Grid.Combobox.ItemIndex + 1;
    c_QLogement_EntreeAir1,
    c_QLogement_EntreeAir2,
    c_QLogement_EntreeAir3,
    c_QLogement_EntreeAir4 : ; // Recuperation dans le ellipsclick
    c_QLogement_CalculDN: Begin
                          CalculDn := _Grid.Combobox.ItemIndex;
                          if Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN then
                                begin
                                if CalculDn = C_Oui then
                                        ForceAcoustiqueDN(Self)
                                else
                                        ReinitialiseAcoustiqueDn;
                                end;
                          End;
  End;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Logement.HasComboBox(_Ligne: Integer): Boolean;
Begin
  Result := AfficheQuestion(NoQuestion(_Ligne)) And (NoQuestion(_Ligne) In[c_QLogement_TypeLogement, c_QLogement_TypeChambre, c_QLogement_T3Optimise, c_QLogement_WCSepares, c_QLogement_IdxConfig, c_QLogement_IdxSolutionEASj, c_QLogement_IdxSolutionEACh, c_QLogement_VentilationGen,
                                                                           c_QLogement_Ventilation, c_QLogement_PositionChaudiere, c_QLogement_CellierVentile,
                                                                           c_QLogement_NbEntreeAirLocal, c_QLogement_CalculDN, c_QLogement_CuisineFermee]);
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Logement.TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType;
Var
  m_NoChoix : Integer;
Begin
  Result := edNone;
  If (Etude = Nil) or (Etude.Batiment = Nil) or (Etude.Batiment.ChiffrageVerouille  = c_OUI) Then Exit;
  //Parfois le GoEditing saute sans raison...
  _Grid.Options := _Grid.Options + [goEditing];
  If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QLogement_Reference    : Result := edNormal;
    c_QLogement_TypeLogement,
    c_QLogement_TypeChambre : Begin
      Result := edComboList;
    End;
    c_QLogement_T3Optimise,
    c_QLogement_WCSepares,
    c_QLogement_IdxConfig : Begin
      Result := edComboList;
    End;

    c_QLogement_IdxSolutionEASj : Begin
      Result := edComboList;
    End;

    c_QLogement_IdxSolutionEACh : Begin
      Result := edComboList;
    End;

    c_QLogement_VentilationGen : Begin
      Result := edComboList;
    End;
    c_QLogement_Ventilation       : Begin
      Result := edComboList;
    End;
    c_QLogement_PositionChaudiere,
    c_QLogement_CellierVentile: Begin
      Result := edComboList;
    End;
    c_QLogement_CuisineFermee: Begin
      Result := edComboList;
    End;
    c_QLogement_Description: Result := edEditBtn;
    c_QLogement_NbEntreeAirLocal: Begin
                                  Result := edComboList;
                                  End;
    c_QLogement_EntreeAir1,
    c_QLogement_EntreeAir2,
    c_QLogement_EntreeAir3,
    c_QLogement_EntreeAir4 : Result := edEditBtn;
    c_QLogement_CalculDN: begin
      Result := edComboList;
    end;
    c_QLogement_EntreesDairDn: Result := edEditBtn;

  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer);
Var
  m_NoChoix : Integer;
  tempListeSystemes : TListeBaseSystemes;
Begin
  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
  If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QLogement_TypeLogement : Begin
      _Grid.ClearComboString;
      For m_NoChoix := Low(c_TypesLogements) To High(c_TypesLogements) Do _Grid.AddComboStringObject(c_TypesLogements[m_NoChoix], Etude.Batiment.Logements.TypesLogementsCollectif[m_NoChoix])
    End;
    c_QLogement_TypeChambre : Begin
      _Grid.ClearComboString;
      For m_NoChoix := Low(c_TypesChambres) To High(c_TypesChambres) Do _Grid.AddComboStringObject(c_TypesChambres[m_NoChoix], Etude.Batiment.Logements.TypesChambresHotel[m_NoChoix])
    End;
    c_QLogement_T3Optimise,
    c_QLogement_WCSepares : Begin
      _Grid.ClearComboString;
      For m_NoChoix := Low(c_OUINON) To High(c_OUINON) Do _Grid.AddComboString(c_OUINON[m_NoChoix]);
    End;
{$IfDef OLD_ATVENTIL}
    c_QLogement_IdxConfig : ;
    c_QLogement_IdxSolutionEASj : ;
    c_QLogement_IdxSolutionEACh : ;
{$Else}
    c_QLogement_IdxConfig : begin
                            _Grid.ClearComboString;
                              for m_NoChoix := 0 to Systeme.ConfigsLogements.NbConfigTypeLogement(TypeLogement) - 1 do
                                  _Grid.AddComboString(Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, m_Nochoix).GetLibellePiecesHumides(False));
                             end;

    c_QLogement_IdxSolutionEASj : begin
                            _Grid.ClearComboString;
                              for m_NoChoix := 0 to Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[c_Sejour].Count - 1 do
                                  _Grid.AddComboString(Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[c_Sejour][m_NoChoix].GetLibelle);
                             end;
    c_QLogement_IdxSolutionEACh : begin
                            _Grid.ClearComboString;
                              for m_NoChoix := 0 to Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[c_Chambre].Count - 1 do
                                  _Grid.AddComboString(Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[c_Chambre][m_NoChoix].GetLibelle);
                             end;

{$EndIf}
    c_QLogement_VentilationGen : Begin
      _Grid.ClearComboString;
      For m_NoChoix := Low(c_IsVentilationGen) To High(c_IsVentilationGen) Do _Grid.AddComboString(c_IsVentilationGen[m_NoChoix]);
    End;
    c_QLogement_Ventilation       : Begin
      _Grid.ClearComboString;
      SystemesVMC.HideInvalidSystems(Systeme);
      tempListeSystemes := SystemesVMC.GetSystemesFabricant(Etude.Batiment.CodeFabricant);
        For m_NoChoix := 0 To tempListeSystemes.Count - 1 Do  _Grid.AddComboStringObject(tempListeSystemes[m_NoChoix].Reference, tempListeSystemes[m_NoChoix]);
      tempListeSystemes.DestroyKeepObjects;
    End;
    c_QLogement_PositionChaudiere,
    c_QLogement_CellierVentile: Begin
      _Grid.ClearComboString;
      For m_NoChoix := Low(c_OUINON) To High(c_OUINON) Do _Grid.AddComboString(c_OUINON[m_NoChoix]);
    End;
    c_QLogement_CuisineFermee: Begin
      _Grid.ClearComboString;
      For m_NoChoix := Low(c_OUINON) To High(c_OUINON) Do _Grid.AddComboString(c_OUINON[m_NoChoix]);
    End;
    c_QLogement_NbEntreeAirLocal: Begin
                                  _Grid.ClearComboString;
                                  For m_NoChoix := 1 To 4 Do _Grid.AddComboString(inttostr(m_NoChoix));
                                  End;
    c_QLogement_CalculDN: begin
      _Grid.ClearComboString;
      For m_NoChoix := Low(c_OUINON) To High(c_OUINON) Do _Grid.AddComboString(c_OUINON[m_NoChoix]);
    end;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.ReinitialiseAcoustiqueDn;
Begin
        if Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN then
                ReinitialiseAcoustiqueDnVIM
        else
                ReinitialiseAcoustiqueDnANJOS;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.ReinitialiseAcoustiqueDnVIM;
Var
  m_NoPiece : SmallInt;
  m_NoEntree: SmallInt;
  m_TypeLgt : SmallInt;
  m_Sys     : TVentil_Systeme;
  m_EA      : SmallInt;
Begin
  m_Sys := Systeme;
  If (m_Sys <> Nil) Then Begin
    { On vide l'ancien module Dn }
    For m_TypeLgt := c_TypeT1 To c_TypeT7 Do Begin
      For m_NoEntree := Ventil_EntreesAirLogement[m_TypeLgt].Count - 1 DownTo 0 Do Ventil_EntreesAirLogement[m_TypeLgt][m_NoEntree].Destroy;
      Ventil_EntreesAirLogement[m_TypeLgt].ClearKeepObjects;
    End;
    { On remplit par defaut avec les valeurs du nouveau systeme }
{$IfDef OLD_ATVENTIL}
    For m_TypeLgt := c_TypeT1 To c_TypeT7 Do
    Begin
      { Sejour }
      For m_EA := 0 To m_Sys.EntreeAir[m_TypeLgt,1, T3Optimise = c_Oui].Count - 1 Do
        begin
          For m_NoEntree := 1 To m_Sys.EntreeAir[m_TypeLgt,1, T3Optimise = c_Oui][m_EA].Nombre Do
          Begin
            Ventil_EntreesAirLogement[m_TypeLgt].Add(TVentil_DonneesEA.Create(1, m_Sys.EntreeAir[m_TypeLgt,1, T3Optimise = c_Oui][m_EA].Edibatec));
            TVentil_DonneesEA(Ventil_EntreesAirLogement[m_TypeLgt].Last).NbDansPiece := m_Sys.EntreeAir[m_TypeLgt, 1, T3Optimise = c_Oui][m_EA].Nombre;
          End;
        end;
      { Chambres }
      if m_Sys.EntreeAir[m_TypeLgt, 2, T3Optimise = c_Oui] <> Nil then
      For m_NoPiece := 1 To NbPiecesAvecEntreeAir - 1 Do { On ne recompte pas le sejour }
      For m_EA := 0 To m_Sys.EntreeAir[m_TypeLgt, 2, T3Optimise = c_Oui].Count - 1 Do
        begin
          For m_NoEntree := 1 To m_Sys.EntreeAir[m_TypeLgt, 2, T3Optimise = c_Oui][m_EA].Nombre Do
          Begin
            Ventil_EntreesAirLogement[m_TypeLgt].Add(TVentil_DonneesEA.Create(2, m_Sys.EntreeAir[m_TypeLgt, 2, T3Optimise = c_Oui][m_EA].Edibatec));
            TVentil_DonneesEA(Ventil_EntreesAirLogement[m_TypeLgt].Last).NbDansPiece := m_Sys.EntreeAir[m_TypeLgt, 2, T3Optimise = c_Oui][m_EA].Nombre;
          End;
        end;
    End;
{$Else}
      m_TypeLgt := TypeLogement;
          { Sejour }
            if (m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig) <> nil) and (m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig).SolutionsEAs[c_Sejour][idxSolutionEA[c_Sejour]] <> Nil) then
            For m_EA := 0 To m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig).SolutionsEAs[c_Sejour][idxSolutionEA[c_Sejour]].Count - 1 Do
              begin
                For m_NoEntree := 1 To m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig).SolutionsEAs[c_Sejour][idxSolutionEA[c_Sejour]][m_EA].Nombre Do
                Begin
                  Ventil_EntreesAirLogement[m_TypeLgt].Add(TVentil_DonneesEA.Create(1, m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig).SolutionsEAs[c_Sejour][idxSolutionEA[c_Sejour]][m_EA].Edibatec));
                  TVentil_DonneesEA(Ventil_EntreesAirLogement[m_TypeLgt].Last).NbDansPiece := m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig).SolutionsEAs[c_Sejour][idxSolutionEA[c_Sejour]][m_EA].Nombre;
                End;
              end;
          { Chambres }
            if (m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig) <> nil) and (m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig).SolutionsEAs[c_Chambre][idxSolutionEA[c_Chambre]] <> Nil) then
            For m_NoPiece := 1 To NbPiecesAvecEntreeAir - 1 Do { On ne recompte pas le sejour }
            For m_EA := 0 To m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig).SolutionsEAs[c_Chambre][idxSolutionEA[c_Chambre]].Count - 1 Do
              begin
                For m_NoEntree := 1 To m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig).SolutionsEAs[c_Chambre][idxSolutionEA[c_Chambre]][m_EA].Nombre Do
                Begin
                  Ventil_EntreesAirLogement[m_TypeLgt].Add(TVentil_DonneesEA.Create(2, m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig).SolutionsEAs[c_Chambre][idxSolutionEA[c_Chambre]][m_EA].Edibatec));
                  TVentil_DonneesEA(Ventil_EntreesAirLogement[m_TypeLgt].Last).NbDansPiece := m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig).SolutionsEAs[c_Chambre][idxSolutionEA[c_Chambre]][m_EA].Nombre;
                End;
              end;

{$EndIf}
  End;
  EntreesDAirDnChoisies := False;

End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.ReinitialiseAcoustiqueDnANJOS;
Var
  m_NoPiece : SmallInt;
  m_NoEntree: SmallInt;
  m_TypeLgt : SmallInt;
  m_Sys     : TVentil_Systeme;
  m_EA      : SmallInt;
  departChambre : Integer;
Begin
  m_Sys := Systeme;
  If (m_Sys <> Nil) Then Begin
    if m_Sys.DoubleFlux then exit;
    { On vide l'ancien module Dn }
    For m_TypeLgt := c_TypeT1 To c_TypeT7 Do Begin
      For m_NoEntree := Ventil_EntreesAirLogement[m_TypeLgt].Count - 1 DownTo 0 Do Ventil_EntreesAirLogement[m_TypeLgt][m_NoEntree].Destroy;
      Ventil_EntreesAirLogement[m_TypeLgt].ClearKeepObjects;
    End;
    { On remplit par defaut avec les valeurs du nouveau systeme }
{$IfDef OLD_ATVENTIL}
    For m_TypeLgt := c_TypeT1 To c_TypeT7 Do
    Begin
      { Sejour }
      if m_Sys.EntreeAir[m_TypeLgt,1, T3Optimise = c_Oui] <> Nil then
      For m_EA := 0 To m_Sys.EntreeAir[m_TypeLgt,1, T3Optimise = c_Oui].Count - 1 Do
        begin
          For m_NoEntree := 1 To m_Sys.EntreeAir[m_TypeLgt,1, T3Optimise = c_Oui][m_EA].Nombre Do
          Begin
            Ventil_EntreesAirLogement[m_TypeLgt].Add(TVentil_DonneesEA.Create(1, m_Sys.EntreeAir[m_TypeLgt,1, T3Optimise = c_Oui][m_EA].Edibatec));
            TVentil_DonneesEA(Ventil_EntreesAirLogement[m_TypeLgt].Last).NbDansPiece := m_Sys.EntreeAir[m_TypeLgt, 1, T3Optimise = c_Oui][m_EA].Nombre;
          End;
        end;
      { Chambres }
      if Etude.Batiment.TypeBat = c_BatimentHotel then
        departChambre := 0
      else
        departChambre := 1;
      if m_Sys.EntreeAir[m_TypeLgt, 2, T3Optimise = c_Oui] <> Nil then
      For m_NoPiece := departChambre To NbPiecesAvecEntreeAir - 1 Do { On ne recompte pas le sejour }
      For m_EA := 0 To m_Sys.EntreeAir[m_TypeLgt, 2, T3Optimise = c_Oui].Count - 1 Do
        begin
          For m_NoEntree := 1 To m_Sys.EntreeAir[m_TypeLgt, 2, T3Optimise = c_Oui][m_EA].Nombre Do
          Begin
            Ventil_EntreesAirLogement[m_TypeLgt].Add(TVentil_DonneesEA.Create(2, m_Sys.EntreeAir[m_TypeLgt, 2, T3Optimise = c_Oui][m_EA].Edibatec));
            TVentil_DonneesEA(Ventil_EntreesAirLogement[m_TypeLgt].Last).NbDansPiece := m_Sys.EntreeAir[m_TypeLgt, 2, T3Optimise = c_Oui][m_EA].Nombre;
          End;
        end;
    End;
{$Else}
    m_TypeLgt := TypeLogement;
          { Sejour }
            if m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig).SolutionsEAs[c_Sejour][idxSolutionEA[c_Sejour]] <> Nil then
            For m_EA := 0 To m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig).SolutionsEAs[c_Sejour][idxSolutionEA[c_Sejour]].Count - 1 Do
              begin
                For m_NoEntree := 1 To m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig).SolutionsEAs[c_Sejour][idxSolutionEA[c_Sejour]][m_EA].Nombre Do
                Begin
                  Ventil_EntreesAirLogement[m_TypeLgt].Add(TVentil_DonneesEA.Create(1, m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig).SolutionsEAs[c_Sejour][idxSolutionEA[c_Sejour]][m_EA].Edibatec));
                  TVentil_DonneesEA(Ventil_EntreesAirLogement[m_TypeLgt].Last).NbDansPiece := m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig).SolutionsEAs[c_Sejour][idxSolutionEA[c_Sejour]][m_EA].Nombre;
                End;
              end;
          { Chambres }
          if Etude.Batiment.TypeBat = c_BatimentHotel then
            departChambre := 0
          else
            departChambre := 1;
            if m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig).SolutionsEAs[c_Chambre][idxSolutionEA[c_Chambre]] <> Nil then
            For m_NoPiece := departChambre To NbPiecesAvecEntreeAir - 1 Do { On ne recompte pas le sejour }
            For m_EA := 0 To m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig).SolutionsEAs[c_Chambre][idxSolutionEA[c_Chambre]].Count - 1 Do
              begin
                For m_NoEntree := 1 To m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig).SolutionsEAs[c_Chambre][idxSolutionEA[c_Chambre]][m_EA].Nombre Do
                Begin
                  Ventil_EntreesAirLogement[m_TypeLgt].Add(TVentil_DonneesEA.Create(2, m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig).SolutionsEAs[c_Chambre][idxSolutionEA[c_Chambre]][m_EA].Edibatec));
                  TVentil_DonneesEA(Ventil_EntreesAirLogement[m_TypeLgt].Last).NbDansPiece := m_Sys.ConfigsLogements.GetConfigLogement(m_TypeLgt, IdxConfig).SolutionsEAs[c_Chambre][idxSolutionEA[c_Chambre]][m_EA].Nombre;
                End;
              end;


{$EndIf}

  End;
  EntreesDAirDnChoisies := False;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Logement.MajEntreesAir;
Var
  cptPieceSeche : Integer;
Begin
  If (TypeLogement >= C_TypeT1) And (TypeLogement <= C_TypeT7) Then Begin
    if Etude.Batiment.TypeBat = c_BatimentHotel then
      NbPiecesAvecEntreeAir := 1
    else
      NbPiecesAvecEntreeAir := TypeLogement;

{$IfDef OLD_ATVENTIL}
{$Else}
      for cptPieceSeche := 1 to c_NbPiecesSeches do
        if (Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig) = Nil) or (IdxSolutionEA[cptPieceSeche] > Systeme.ConfigsLogements.GetConfigLogement(TypeLogement, IdxConfig).SolutionsEAs[cptPieceSeche].Count - 1) then
          IdxSolutionEA[cptPieceSeche] := 0;
{$EndIf}
    ReinitialiseAcoustiqueDn;
  End;
End;
{ ************************************************************************************************************************************************** }


{ ******************************************************************* TListeLogements ***************************************************************** }
Function TListeLogements.Add(Value: TVentil_Logement): Integer;
Begin
  Result := Inherited Add(Value);
End;
{ ************************************************************************************************************************************************** }
Constructor TListeLogements.Create;
Var
  cpt : Integer;
Begin
  Inherited;
    for cpt := Low(TypesPiecesHumides) to High(TypesPiecesHumides) do
      TypesPiecesHumides[cpt] := TVentil_TypeCombo.Create(cpt);
    for cpt := Low(TypesPiecesSeches) to High(TypesPiecesSeches) do
      TypesPiecesSeches[cpt] := TVentil_TypeCombo.Create(cpt);
    for cpt := Low(c_TypesLogements) to High(c_TypesLogements) do
      TypesLogementsCollectif[cpt] := TVentil_TypeCombo.Create(cpt);
    for cpt := Low(c_TypesChambres) to High(c_TypesChambres) do
      TypesChambresHotel[cpt] := TVentil_TypeCombo.Create(cpt);
End;
{ ************************************************************************************************************************************************** }
Destructor TListeLogements.Destroy;
Var
  m_No : Integer;
  cpt : Integer;
Begin
  For m_No := Count - 1 DownTo 0 Do TVentil_Logement(Items[m_No]).Destroy;

    for cpt := Low(TypesPiecesHumides) to High(TypesPiecesHumides) do
      TypesPiecesHumides[cpt].Destroy;
    for cpt := Low(TypesPiecesSeches) to High(TypesPiecesSeches) do
      TypesPiecesSeches[cpt].Destroy;
    for cpt := Low(c_TypesLogements) to High(c_TypesLogements) do
      TypesLogementsCollectif[cpt].Destroy;
    for cpt := Low(c_TypesChambres) to High(c_TypesChambres) do
      TypesChambresHotel[cpt].Destroy;

  Inherited;
End;
{ ************************************************************************************************************************************************** }
{$IFDEF ACTHYS_DIMVMBP}
Procedure TListeLogements.ImportXMLActhys(_Node : ImportEVenthysXSD.IXMLLOGEMENTS);
Var
  cpt : Integer;
begin
  if _Node = Nil then
      Exit;
    for Cpt := Count - 1 Downto 0  do
      If (Item[Cpt] <> Nil) and Not(Item[Cpt].UtiliseCalcul) Then
        Begin
        Item[Cpt].Destroy;
        Item[Cpt] := Nil;
        Delete(Cpt);
        End;

    for Cpt := 0 to _Node.Count - 1 do
      begin
      Etude.AddLogement;
      TVentil_Logement(Etude.Batiment.Logements.Last).ImportXMLActhys(_Node[cpt]);
      end;

end;
{ ************************************************************************************************************************************************** }
Procedure TListeLogements.ExportXMLActhys(_Node : ExportEVenthysXSD.IXMLLOGEMENTS);
Var
  cpt : Integer;
begin
    for Cpt := 0 to Count - 1 do
      Item[cpt].ExportXMLActhys((_Node as ExportEVenthysXSD.IXMLLOGEMENTS).Add);
end;
{ ************************************************************************************************************************************************** }
Procedure TListeLogements.ExportXMLActhys2ndPasse(_Node : ExportEVenthysXSD.IXMLLOGEMENTS);
Var
  cpt : Integer;
begin
    for Cpt := 0 to Count - 1 do
      Item[cpt].ExportXMLActhys2ndPasse((_Node as ExportEVenthysXSD.IXMLLOGEMENTS).LOGEMENT[cpt]);
end;
{$ENDIF}
{ ************************************************************************************************************************************************** }
Function  TListeLogements.GetLogementFromIDXML(_Id : Integer):TVentil_Logement;
Var
  cpt : Integer;
begin
  Result := Nil;
  for cpt := 0 to Count - 1 do
    if Item[cpt].IdImportXML = _Id then
      begin
      Result := Item[cpt];
      Exit;
      end;

end;
{ ************************************************************************************************************************************************** }
Function TListeLogements.GetItems(Index: Integer): TVentil_Logement;
Begin
  Result := Items[Index];
End;
{ ************************************************************************************************************************************************** }
Procedure TListeLogements.Load;
Var
  m_Logement : TVentil_Logement;
  Condition : String;
Begin

    {$IfDef AERAU_CLIMAWIN}
    Condition := 'IDRESEAU=''' + Etude.IdReseau  + '''';
    Table_Etude_Logement.Donnees.Filter := Condition;
    Table_Etude_Logement.Donnees.Filtered := True;
    {$EndIf}

  Table_Etude_Logement.Donnees.First;
  While Not Table_Etude_Logement.Donnees.Eof Do Begin
    m_Logement := TVentil_Logement.Create;
    m_Logement.Load;
    Add(m_Logement);
    Table_Etude_Logement.Donnees.Next;
  End;

    {$IfDef AERAU_CLIMAWIN}
    Table_Etude_Logement.Donnees.Filtered := False;
    {$EndIf}

End;
{ ************************************************************************************************************************************************** }
Procedure TListeLogements.Save;
Var
  m_No : Integer;
Begin
  For m_No := 0 To Count - 1 Do TVentil_Logement(Items[m_No]).Save;
End;
{ ************************************************************************************************************************************************** }
Procedure TListeLogements.SetItems(Index: Integer; Value: TVentil_Logement);
Begin
  Items[Index] := Value;
End;
{ ************************************************************************************************************************************************** }
Procedure TListeLogements.MiseAjourQuantitesGaines;
Var
  m_No : Integer;
Begin
  For m_No := 0 To Count - 1 Do TVentil_Logement(Items[m_No]).LongueursConduits.MiseAjourQuantitesGaines;
end;
{ ************************************************************************************************************************************************** }
Procedure TListeLogements.ChiffrageAccessoiresT2A;
Var
  m_No : Integer;
Begin
  For m_No := 0 To Count - 1 Do TVentil_Logement(Items[m_No]).LongueursConduits.ChiffrageAccessoiresT2A;
end;
{ ************************************************************************************************************************************************** }
Procedure TListeLogements.ClearChiffrageT2A;
Var
  m_No : Integer;
Begin
  For m_No := 0 To Count - 1 Do TVentil_Logement(Items[m_No]).ClearChiffrageT2A;
end;
{ ******************************************************************* \ TListeLogements *************************************************************** }



End.
