Unit Ventil_SortieToiture;

{$Include 'Ventil_Directives.pas'}

Interface
Uses
  Classes, SysUtils, advGrid, BaseGrid, AdvObj,
  BBScad_Interface_Aeraulique,
  Ventil_Types, Ventil_Caisson, Ventil_edibatecProduits, Ventil_Utils, Ventil_Troncon;

Type
{ ************************************************************************************************************************************************** }
  TVentil_SortieToit = Class(TVentil_Troncon, IAero_Fredo_SortieToiture)

    Constructor Create; Override;
  Private
    Caisson : TVentil_Caisson;
    FCategorie : Integer;
    FCodeGamme : String;
    FProduit   : TEdibatec_SortieToiture;
    FDzetaMini     : Real;
    FDzetaMaxi     : Real;

    Procedure Set_Categorie(Const _Value: Integer);
    Procedure Set_SortieToiture(Const _Value: TEdibatec_SortieToiture);
    Procedure Set_DzetaMini(Const _Value: Real);
    Procedure Set_DzetaMaxi(Const _Value: Real);
  Public
    Property Categorie : Integer read FCategorie write Set_Categorie;
    Property Produit   : TEdibatec_SortieToiture read FProduit write Set_SortieToiture;
    Property DzetaMini     : Real read FDzetaMini write Set_DzetaMini;
    Property DzetaMaxi     : Real read FDzetaMaxi write Set_DzetaMaxi;

    Procedure Affiche (_Grid: TAdvStringGrid); Override;
    Procedure Recupere(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType; Override;
    Procedure PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  HasComboBox(_Ligne: Integer): Boolean; Override;
    Function  AfficheQuestion(_NoQuestion: Integer): Boolean; Override;
    Function  EllipsClick(_Grid: TAdvStringGrid; _Ligne: Integer): String; Override;

    Procedure InfosFromCad; Override;

    Procedure CalculeRecapitulatif(_Iteratif : Boolean); Override;

    Procedure Load; Override;
    Procedure MiseAJourDiametre;

    function CalculePDCVentDT2014 : Integer;

    function IAero_Fredo_SortieToiture_GetType : IAero_SortieToiture_Types;

  Protected
    Function  ImageAssociee: String; Override;
    Procedure Save; Override;
    Procedure SelectionneDiametre_SelonMethode; Override;
    Procedure GenerationAccidents; Override;
    Procedure CalculerQuantitatifInternal; Override;
    Procedure CalculePdcReguliere_GaineRigide(_longueur : Real; _Diametre : Integer; _DebitMini : Real; _DebitMaxi : Real); Override;
    Procedure CalculePdcReguliere_GaineSemiRigide(_longueur : Real; _Diametre : Integer; _DebitMini : Real; _DebitMaxi : Real); Override;
    Procedure CalculePdcReguliere_LiaisonFlexible; Override;
  End;
{ ************************************************************************************************************************************************** }

Implementation
Uses
  Grids,
  BbsgFonc,
  Ventil_Firebird,
  Ventil_Const,
  Ventil_EdibatecChoixProduit,
  Ventil_Declarations,
  Ventil_Edibatec, Ventil_Accident, Ventil_Etude;
  
{ ******************************************************************* TVentil_SortieToit ***************************************************************** }
Procedure TVentil_SortieToit.Affiche(_Grid: TAdvStringGrid);
Var
  m_Row      : Integer;
  m_Question : Integer;
  m_Str      : String;
Begin
  Inherited;
  m_Row := GetNbQuestionAfficheTroncon + 1;
  For m_Question := Low(c_LibellesQ_SortieT) To High(c_LibellesQ_SortieT) Do If AfficheQuestion(m_Question) Then Begin
    _Grid.Cells[0, m_Row] := c_LibellesQ_SortieT[m_Question];
    Case m_Question Of
      c_QSortieToit_Type  : if (Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN) then
                                Begin
                                        if FCodeGamme = BaseEdibatec.Assistant.Gamme('c_Gamme_SortieToit_Grille') then
                                                m_Str := 'Sortie g�n�rique'
                                        Else
                                                m_Str := BaseEdibatec.Gamme(FCodeGamme).Reference;
                                End
                            else
                                m_Str := c_LibellesSortieToit[Categorie];
      c_QSortieToit_Choix :     If IsNotNil(Produit) Then m_Str := Produit.Reference
                                Else m_Str := 'non choisi';
      c_QSortieToit_Dzeta : m_Str := Float2Str(DzetaMaxi, 2);
      Else m_Str := '';
    End;
    _Grid.Cells[1, m_Row] := m_Str;
    Inc(m_Row);
  End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_SortieToit.AfficheQuestion(_NoQuestion: Integer): Boolean;
Begin
  If _NoQuestion <= c_NbQ_Troncon Then Result := Inherited AfficheQuestion(_NoQuestion)
  Else Case _NoQuestion Of
    c_QSortieToit_Type  : {$IfNDef AERAU_CLIMAWIN}if Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN  then Result := True else {$EndIf}Result := False;
    c_QSortieToit_Choix : {$IfNDef AERAU_CLIMAWIN}Result := FCodeGamme <> BaseEdibatec.Assistant.Gamme('c_Gamme_SortieToit_Grille');{$Else}Result := False;{$EndIf}
    c_QSortieToit_Dzeta : {$IfNDef AERAU_CLIMAWIN}if Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN then Result := True else {$EndIf}Result := Categorie In [c_SortieToit_Generic];
    Else Result := False;
  End;

{$IfNDef AERAU_CLIMAWIN}
  if not(Etude.Batiment.IsFabricantVIM) then
  if _NoQuestion = c_QTroncon_Diametre then Result := False;
{$EndIf}    

End;
{ ************************************************************************************************************************************************** }
Function TVentil_SortieToit.EllipsClick(_Grid: TAdvStringGrid; _Ligne: Integer): String;
Var
  m_CodeProduit : String;
Begin                  
  If IsNotNil(Produit) Then m_CodeProduit := Produit.CodeProduit
                       Else m_CodeProduit := FCodeGamme;
  m_CodeProduit := SelectionDunProduit(m_CodeProduit);
  If m_CodeProduit <> '' Then Begin
    Produit :=  TEdibatec_SortieToiture(BaseEdibatec.Produit(m_CodeProduit));
    Result := Produit.Reference;
  End Else Result := ' *** ';
  Recupere(_Grid, _Ligne);
End;
{ ************************************************************************************************************************************************** }
function TVentil_SortieToit.CalculePDCVentDT2014 : Integer;
begin
Result := 0;
  if Caisson <> Nil then
    begin
      if (Caisson.Position = c_Caisson_Combles) and  (Categorie In [c_SortieToit_CT, c_SortieToit_CTBis]) then
        begin
        Result := 0;
        Exit;
        end
      else
      if (Caisson.Position = c_Caisson_Terrasse) and  (Categorie In [c_SortieToit_Generic, c_SortieToit_CTGR]) then
        begin
        Result := 0;
        Exit;
        end
      else
        Result := 20;
    end;
end;

{ ************************************************************************************************************************************************** }
Procedure TVentil_SortieToit.CalculePdcReguliere_GaineRigide(_longueur : Real; _Diametre : Integer; _DebitMini : Real; _DebitMaxi : Real);
//var
//  tempDPVent : Integer;
begin

inherited;

{
  if Etude.DTU2014 then
    begin
    tempDPVent := Get_PerteChargeVentDT2014;
    PdcReguliereMini := PdcReguliereMini + tempDPVent;
    PdcReguliereMaxi := PdcReguliereMaxi + tempDPVent;
    end;
}
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_SortieToit.CalculePdcReguliere_GaineSemiRigide(_longueur : Real; _Diametre : Integer; _DebitMini : Real; _DebitMaxi : Real);
//var
//  tempDPVent : Integer;
begin

inherited;
{
  if Etude.DTU2014 then
    begin
    tempDPVent := Get_PerteChargeVentDT2014;
    PdcReguliereMini := PdcReguliereMini + tempDPVent;
    PdcReguliereMaxi := PdcReguliereMaxi + tempDPVent;
    end;
}
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_SortieToit.CalculePdcReguliere_LiaisonFlexible;
//var
//  tempDPVent : Integer;
begin

inherited;
{
  if Etude.DTU2014 then
    begin
    tempDPVent := Get_PerteChargeVentDT2014;
    PdcReguliereMini := PdcReguliereMini + tempDPVent;
    PdcReguliereMaxi := PdcReguliereMaxi + tempDPVent;
    end;
}
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_SortieToit.CalculeRecapitulatif(_Iteratif : Boolean);
var
  cptTemperature : TTemperatureCalc;
Begin
if Etude.Batiment.IsFabricantVIM then
begin
inherited;
 for cptTemperature := Low(TTemperatureCalc) to High(TTemperatureCalc) do
begin
DeltaP[cptTemperature].MinAQmin := PdcTotaleTotaleMini;
DeltaP[cptTemperature].MinAQmax := PdcTotaleTotaleMini;
DeltaP[cptTemperature].MaxAQmin := PdcTotaleTotaleMaxi;
DeltaP[cptTemperature].MaxAQmax := PdcTotaleTotaleMaxi;
End;
end;

End;
{ ************************************************************************************************************************************************** }
Constructor TVentil_SortieToit.Create;
Begin
  Inherited;
  NbQuestions := NbQuestions + c_NbQ_SortieToit;
  DzetaMaxi       := 1;
  Produit     := nil;
  Reference   := 'Sortie toiture';
  {$IFDEF ANJOS_OPTIMA3D}
  Categorie   := c_SortieToit_CT;
  {$ELSE}
  Categorie   := c_SortieToit_Generic;
  {$ENDIF}
End;
{ ************************************************************************************************************************************************** }
Function TVentil_SortieToit.HasComboBox(_Ligne: Integer): Boolean;
Begin
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Result := Inherited HasComboBox(_Ligne)
  Else Result := AfficheQuestion(NoQuestion(_Ligne)) And (NoQuestion(_Ligne) In[c_QSortieToit_Type]);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_SortieToit.InfosFromCad;
Var
  m_Tr : TVentil_Troncon;
Begin
  Inherited;
  { Affectation du caisson }
  VersSortieToit := True;
  m_Tr := Parent;
  While IsNotNil(m_Tr) And Not m_Tr.InheritsFrom(TVentil_Caisson) Do Begin
    m_Tr.VersSortieToit := True;
    m_Tr := m_Tr.Parent;
  End;
  If IsNotNil(m_Tr) And m_Tr.InheritsFrom(TVentil_Caisson) Then Caisson := TVentil_Caisson(m_Tr)
                                                        Else Caisson := Nil;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_SortieToit.Load;
Var
  m_Str : String;
Begin
  Categorie := Table_Etude_SortieToit.Donnees.FieldByName('CATEGORIE').AsInteger;
  m_Str := Table_Etude_SortieToit.Donnees.FieldByName('PRODUIT').AsWideString;
  DzetaMaxi     := Table_Etude_SortieToit.Donnees.FieldByName('DZETA').AsFloat;

        if trim(Table_Etude_SortieToit.Donnees.FieldByName('CODEGAMME').AsWideString) <> '' then
                FCodeGamme := Table_Etude_SortieToit.Donnees.FieldByName('CODEGAMME').AsWideString;


  If m_Str <> '' Then Produit := TEdibatec_SortieToiture(BaseEdibatec.Produit(m_Str))
                 Else Produit := nil;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_SortieToit.Recupere(_Grid: TAdvStringGrid; _Ligne: Integer);
Var
        m_Classe             : TEdibatec_Classe;
Begin
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Inherited
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QSortieToit_Type  : if (Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN) then
                          Begin
                          m_Classe := BaseEdibatec.Classe(c_Edibatec_SortiesToit);
                                  if _Grid.Combobox.ItemIndex < _Grid.Combobox.Items.Count - 1 then
                                          FCodeGamme := m_Classe.Gammes.Gamme(_Grid.Combobox.ItemIndex).CodeGamme
                                  else
                                          FCodeGamme := BaseEdibatec.Assistant.Gamme('c_Gamme_SortieToit_Grille');
                          End
                          else
                          Categorie := _Grid.Combobox.ItemIndex;
    c_QSortieToit_Choix : ; // c'est dans le ellipsclick qu'on recupere
    c_QSortieToit_Dzeta : DzetaMaxi := _Grid.Floats[1, _Ligne];
  End;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_SortieToit.ImageAssociee: String;
begin
  {$IfDef MVN_MVNAIR}
  Result := 'SortieToiture.jpg'
  {$Else}
  Result := Inherited;
  {$EndIf}
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_SortieToit.Save;
Begin
  Table_Etude_SortieToit.Donnees.Insert;
  Table_Etude_SortieToit.Donnees.FieldByName('ID_SORTIE_TOIT').AsWideString := GUIDToString(Id);
  {$IfDef AERAU_CLIMAWIN}
  Table_Etude_SortieToit.Donnees.FieldByName('IDRESEAU').AsWideString := Etude.IdReseau;
  {$EndIf}
  Table_Etude_SortieToit.Donnees.FieldByName('CATEGORIE').AsInteger := Categorie;
  Table_Etude_SortieToit.Donnees.FieldByName('CODEGAMME').AsWideString := FCodeGamme;
  If IsNotNil(Produit) Then Table_Etude_SortieToit.Donnees.FieldByName('PRODUIT').AsWideString := Produit.CodeProduit
                       Else Table_Etude_SortieToit.Donnees.FieldByName('PRODUIT').AsWideString := '';
  Table_Etude_SortieToit.Donnees.FieldByName('DZETA').AsFloat := DzetaMaxi;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_SortieToit.MiseAJourDiametre;
Begin
if Etude.Batiment.IsFabricantVIM then
begin
SelectionneDiametre_SelonMethode;
Etude.Calculer;
end;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_SortieToit.SelectionneDiametre_SelonMethode;
Var
        m_Gamme             : TEdibatec_Gamme;
        m_ProduitSortieToit : TEdibatec_SortieToiture;
        m_NoProd            : Integer;
Begin
  If IsNotNil(Caisson) Then Begin
    DebitMini := Caisson.DebitMini;
    DebitMaxi := Caisson.DebitMaxi;
  End;
  Inherited;

{$IfNDef AERAU_CLIMAWIN}
if not(Etude.Batiment.IsFabricantVIM) then
        if Produit <> nil then
                Diametre := Produit.Diametre;


if Etude.Batiment.IsFabricantVIM then
begin
    If IsNotNil(Caisson) and IsNotNil(Caisson.ProduitAssocie) and (IndiceDiametre = c_Diam_Calc) then
        Diametre := Caisson.ProduitAssocie.DiametreRejet;



Produit := nil;
m_Gamme := BaseEdibatec.Gamme(FCodeGamme);
                If m_Gamme <> nil Then
                        For m_NoProd := 0 To m_Gamme.Produits.Count - 1 Do
                                Begin
                                m_ProduitSortieToit := TEdibatec_SortieToiture(m_Gamme.Produits.Produit(m_NoProd));
                                        If (m_ProduitSortieToit.Diametre = Diametre) Then
                                                Begin
                                                Produit := m_ProduitSortieToit;
                                                exit;
                                                End;
                                End;

    If  Etude.Batiment.IsFabricantVIM and (FCodeGamme = BaseEdibatec.Assistant.Gamme('c_Gamme_SortieToit_Grille')) Then
        Categorie := c_SortieToit_Grille
                                        
end;
{$EndIf}
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_SortieToit.GenerationAccidents;
Var
  m_Acc : TVentil_Accident_Genere;
Begin
  Inherited;           
  If Parentcalcul.InheritsFrom(TVentil_Caisson) Then Begin
  if ParentCalcul.CanAddAccidentAmont then
  begin
    m_Acc := TVentil_Accident_Genere.Create;
    m_Acc.DzetaMini := DzetaMini;
    m_Acc.DzetaMaxi := DzetaMaxi;
    m_Acc.TronconPorteur := ParentCalcul;
    ParentCalcul.Accidents.Add(m_Acc);
  End;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_SortieToit.CalculerQuantitatifInternal;
Begin
  Inherited;
  If IsNotNil(Produit) Then
        AddChiffrage(Produit, 1, {$IfDef VIM_OPTAIR}cst_Rejet {$Else} cst_Horizontal{$EndIf}, {$IfDef VIM_OPTAIR}cst_Chiff_RE{$Else}cst_Chiff_AC{$EndIf})
  else
        Begin
        if Etude.Batiment.IsFabricantVIM then
        begin
                if FCodeGamme <> BaseEdibatec.Assistant.Gamme('c_Gamme_SortieToit_Grille') then
                        AddChiffrageErreur('Sortie toiture', BaseEdibatec.Gamme(FCodeGamme).Reference + ' ' + IntToStr(Diametre){$IfDeF VIM_OPTAIR}, inttostr(1) + ' unit�'{$EndIf});
        end;
        End;

End;
{ ************************************************************************************************************************************************** }
Function TVentil_SortieToit.TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType;
Begin
  Result := edNone;
  If Etude.Batiment.ChiffrageVerouille = c_OUI Then Exit;
  //Parfois le GoEditing saute sans raison...
  _Grid.Options := _Grid.Options + [goEditing];
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Result := Inherited TypeEdition(_Grid, _Ligne)
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QSortieToit_Type  : Begin
      Result := edComboList;
    End;
    c_QSortieToit_Choix   : if not(Etude.Batiment.IsFabricantVIM) then Result := edEditBtn;
    c_QSortieToit_Dzeta   : Result := edFloat;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_SortieToit.PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer);
Var
  m_NoChoix : Integer;
  m_Classe             : TEdibatec_Classe;
Begin
  If Etude.Batiment.ChiffrageVerouille = c_OUI Then Exit;
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Inherited
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QSortieToit_Type  : Begin
      _Grid.ClearComboString;
        if (Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN) then
          begin
          m_Classe := BaseEdibatec.Classe(c_Edibatec_SortiesToit);
            For m_NoChoix := 0 to m_Classe.Gammes.Count - 1 do _Grid.AddComboString(m_Classe.Gammes.Gamme(m_NoChoix).Reference);
            _Grid.AddComboString('Sortie g�n�rique');
          end
        else
          begin
            For m_NoChoix := Low(c_LibellesSortieToit) To High(c_LibellesSortieToit) Do _Grid.AddComboString(c_LibellesSortieToit[m_NoChoix]);
          end;
      End;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_SortieToit.Set_Categorie(Const _Value: Integer);
Begin
  If _Value <> Categorie Then Begin
    FCategorie := _Value;
    Case _Value Of
      c_SortieToit_Sifflet : Begin
        FCodeGamme := BaseEdibatec.Assistant.Gamme('c_Gamme_SortieToit_Sifflet');
        If LienCad <> Nil Then (LienCad As IAero_SortieToiture).IAero_SortieToiture_SetType(stSifflet);
      End;
      c_SortieToit_CT      : Begin
        FCodeGamme := BaseEdibatec.Assistant.Gamme('c_Gamme_SortieToit_CT');
        If LienCad <> Nil Then (LienCad As IAero_SortieToiture).IAero_SortieToiture_SetType(stChapeau);
      End;
      c_SortieToit_Generic : Begin
        FCodeGamme := BaseEdibatec.Assistant.Gamme('c_Gamme_SortieToit_Generic');
        If LienCad <> Nil Then (LienCad As IAero_SortieToiture).IAero_SortieToiture_SetType(stGenerique);
      End;
      c_SortieToit_CTBis   : if (Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN) then
                                Begin
                                FCodeGamme := BaseEdibatec.Assistant.Gamme('c_Gamme_SortieToit_CTBis');
                                If LienCad <> Nil Then (LienCad As IAero_SortieToiture).IAero_SortieToiture_SetType(stChapeau);
                              End
                              else
                                Begin
                                FCodeGamme := BaseEdibatec.Assistant.Gamme('c_Gamme_SortieToit_Generic');
                                If LienCad <> Nil Then (LienCad As IAero_SortieToiture).IAero_SortieToiture_SetType(stGenerique);
                                End;
      c_SortieToit_Grille   : if Etude.Batiment.IsFabricantVIM then
                                Begin
                                FCodeGamme := BaseEdibatec.Assistant.Gamme('c_Gamme_SortieToit_Grille');
                                If LienCad <> Nil Then (LienCad As IAero_SortieToiture).IAero_SortieToiture_SetType(stGrille);
                                DzetaMaxi := 1;
                              End
                              else
                                Begin
                                End;

      c_SortieToit_CTGR   : if Etude.Batiment.IsFabricantVIM then
                                Begin
                                FCodeGamme := BaseEdibatec.Assistant.Gamme('c_Gamme_SortieToit_CTGR');
                                If LienCad <> Nil Then (LienCad As IAero_SortieToiture).IAero_SortieToiture_SetType(stGenerique);
                              End
                              else
                                Begin
                                FCodeGamme := BaseEdibatec.Assistant.Gamme('c_Gamme_SortieToit_Generic');
                                If LienCad <> Nil Then (LienCad As IAero_SortieToiture).IAero_SortieToiture_SetType(stGenerique);
                                End;

    Else
        Begin
        FCodeGamme := BaseEdibatec.Assistant.Gamme('c_Gamme_SortieToit_Generic');
        If LienCad <> Nil Then (LienCad As IAero_SortieToiture).IAero_SortieToiture_SetType(stGenerique);
      End
    End;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_SortieToit.Set_SortieToiture(Const _Value: TEdibatec_SortieToiture);
Begin
  If _Value <> FProduit Then Begin
    FProduit := _Value;
        if FProduit = nil then                                                
                exit;
    { Mise � jour de la cat�gorie si on a chang� }
    If Copy(_Value.CodeProduit, 1, 12) = BaseEdibatec.Assistant.Gamme('c_Gamme_SortieToit_Sifflet') Then
        Categorie := c_SortieToit_Sifflet
    Else
    If Copy(_Value.CodeProduit, 1, 12) = BaseEdibatec.Assistant.Gamme('c_Gamme_SortieToit_CT') Then
        Categorie := c_SortieToit_CT
    Else
    If  (Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN) and (Copy(_Value.CodeProduit, 1, 12) = BaseEdibatec.Assistant.Gamme('c_Gamme_SortieToit_CTBis')) Then
        Categorie := c_SortieToit_CTBis
    Else
    If  (Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN) and (Copy(_Value.CodeProduit, 1, 12) = BaseEdibatec.Assistant.Gamme('c_Gamme_SortieToit_CTGR')) Then
        Categorie := c_SortieToit_CTGR
    Else
    If  (Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN) and (_Value.CodeProduit = BaseEdibatec.Assistant.Gamme('c_Gamme_SortieToit_Grille')) Then
        Categorie := c_SortieToit_Grille
    Else
    Categorie := c_SortieToit_Generic;

    DzetaMaxi := _Value.Dzeta;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_SortieToit.Set_DzetaMini(Const _Value: Real);
Begin
  If _Value <> FDzetaMini Then FDzetaMini := _Value;
  If FDzetaMini = 0 Then FDzetaMini := 1;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_SortieToit.Set_DzetaMaxi(Const _Value: Real);
Begin
  If _Value <> FDzetaMaxi Then FDzetaMaxi := _Value;
  If FDzetaMaxi = 0 Then FDzetaMaxi := 1;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_SortieToit.IAero_Fredo_SortieToiture_GetType : IAero_SortieToiture_Types;
Begin
  Case Categorie Of
    c_SortieToit_Sifflet : Result := stSifflet;
    c_SortieToit_CT      : Result := stChapeau;
    c_SortieToit_CTBis   : Result := stChapeau;
    c_SortieToit_Generic : Result := stGenerique;
    c_SortieToit_Grille  : Result := stGrille;
    c_SortieToit_CTGR    : Result := stGenerique;
    Else Result := stChapeau;
  End;
End;
{ ******************************************************************* \ TVentil_SortieToit *************************************************************** }

End.
