Unit Ventil_Options;
           
{$Include 'Ventil_Directives.pas'}

Interface

Uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Registry,
  Buttons, ExtCtrls, Grids, BaseGrid, AdvGrid, ComCtrls,
  comobj, variants, Dialogs, ExtDlgs,
  Ventil_Impressions,
  BbsTypeOptions,
  BBScad_Interface_Aeraulique,
  BBScad_Interface_Commun,
  AdvObj,
  BbsgTec, FolderDialog, AdvEdit, HTMLabel;

Type
  TDlg_Options = class(TForm)
    Bevel1: TBevel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    PageControl1: TPageControl;
    TabSheet_Chiffrage: TTabSheet;
    GroupBox1: TGroupBox;
    CheckBox_ColonneExpress: TCheckBox;
    CheckBox_Terrasse: TCheckBox;
    CheckBox_PiegesASon: TCheckBox;
    GroupBoxExportCSV: TGroupBox;
    CheckBox_Libelle: TCheckBox;
    CheckBox_PUnitaire: TCheckBox;
    CheckBox_Conditionnement: TCheckBox;
    CheckBox_Quantite: TCheckBox;
    CheckBox_MainDoeuvre: TCheckBox;
    CheckBox_PrixTotal: TCheckBox;
    TabSheet_Remises: TTabSheet;
    Grid_Remises: TAdvStringGrid;
    TabSheet_Etiquettes: TTabSheet;
    GroupBox3: TGroupBox;
    CheckBox_EtiqCaissons: TCheckBox;
    CheckBox_EtiqBifurcations: TCheckBox;
    CheckBox_EtiqTeSouches: TCheckBox;
    CheckBox_EtiqCollecteurs: TCheckBox;
    CheckBox_EtiqBouches: TCheckBox;
    CheckBox_EtiqColonnes: TCheckBox;
    CheckBox_EtiqGaines: TCheckBox;
    CheckBox_Veloduct: TCheckBox;
    CheckBox_EtiqSilenc: TCheckBox;
    RadioGroupEtGaines: TRadioGroup;
    GroupBoxEtGaineReseau: TGroupBox;
    GroupBoxEtBoucheOptions: TGroupBox;
    CheckBoxEtBoucheRef: TCheckBox;
    CheckBoxEtGaineRHor: TCheckBox;
    CheckBoxEtGaineRVert: TCheckBox;
    CheckBoxEtGaineRToit: TCheckBox;
    TabSheet_Save: TTabSheet;
    GroupBoxEtiGainAff: TGroupBox;
    CheckBoxEtiGaineLong: TCheckBox;
    CheckBoxEtiGaineDiam: TCheckBox;
    TabSheet_ParamImpressions: TTabSheet;
    GroupBox9: TGroupBox;
    BtnBitmapEntete: TSpeedButton;
    BtnLogoDelete: TSpeedButton;
    ImagePrintEnTete: TImage;
    OpenPictureDialog1: TOpenPictureDialog;
    CheckBox_LogoOriginal: TCheckBox;
    ButtonImportXLS: TButton;
    OpenDialogXLS: TOpenDialog;
    RadioGroupGaine125: TRadioGroup;
    CheckBox_EtiqRegDebit: TCheckBox;
    CheckBox_ColliersIso: TCheckBox;
    RadioGroupTypeEtiquette: TRadioGroup;
    CheckBox_EtiqPlenums: TCheckBox;
    FolderDialog1: TFolderDialog;
    CheckBox_EtiqColonnesSsPlenums: TCheckBox;
    CheckBoxEtBouchePression: TCheckBox;
    CheckBoxEtBoucheDebit: TCheckBox;
    CheckBox_GestionnaireVuesCAD: TCheckBox;
    CheckBoxEtiGaineDebit: TCheckBox;
    GroupBoxEtiTeteColAff: TGroupBox;
    CheckBoxEtiTeteColDiametre: TCheckBox;
    CheckBoxEtiTeteColDebit: TCheckBox;
    CheckBoxEtiTeteColPression: TCheckBox;
    TabSheet_ParamCalculs: TTabSheet;
    CheckBox_IteratifActif: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    AdvEdit_IteratifConvergence: TAdvEdit;
    AdvEdit_IteratifMax: TAdvEdit;
    CheckBox_IteratifExport: TCheckBox;
    TabSheet_ParamDivers: TTabSheet;
    CheckBox_DesactiverUpdate: TCheckBox;
    Label4: TLabel;
    CheckBox_EtiqRegistres: TCheckBox;
    CheckBox_EtiqClapetCoupeFeu: TCheckBox;
    PanelFoldersActhys: TPanel;
    ButtonDefautFolderExportXML: TButton;
    EditDefautFolderExportXML: TEdit;
    LabelDefautFolderExportXML: TLabel;
    ButtonDefautFolderProjects: TButton;
    EditDefautFolderProjects: TEdit;
    LabelDefautFolderProjects: TLabel;
    PanelAutoSave: TPanel;
    Label8: TLabel;
    UpDown_delaiSauvegarde: TUpDown;
    Edit_DelaiSauvegarde: TEdit;
    Label7: TLabel;
    CheckBox_AutoSaveActivate: TCheckBox;
    Panel_Enregistrement_Copies: TPanel;
    Label_NbSvgDisk: TLabel;
    Shape5: TShape;
    Label_Enregistrement_Copies: THTMLabel;
    Edit_NbSvgDisk: TEdit;
    UpDown_NbSvgDisk: TUpDown;
    procedure CheckBox_EtiqGainesClick(Sender: TObject);
    procedure CheckBox_EtiqBouchesClick(Sender: TObject);
    procedure Edit_DelaiSauvegardeKeyPress(Sender: TObject; var Key: Char);
    procedure ButtonImportXLSClick(Sender: TObject);
    procedure BtnBitmapEnteteClick(Sender: TObject);
    procedure BtnLogoDeleteClick(Sender: TObject);
    procedure ButtonDefautFolderProjectsClick(Sender: TObject);
    procedure ButtonDefautFolderExportXMLClick(Sender: TObject);
    procedure CheckBox_EtiqTeSouchesClick(Sender: TObject);
    procedure CheckBox_EtiqPlenumsClick(Sender: TObject);
    procedure AdvEdit_IteratifConvergenceValueValidate(Sender: TObject;
      Value: string; var IsValid: Boolean);
    procedure AdvEdit_IteratifMaxValueValidate(Sender: TObject; Value: string;
      var IsValid: Boolean);
    procedure Label8Click(Sender: TObject);
    procedure Edit_NbSvgDiskKeyPress(Sender: TObject; var Key: Char);
  private
    DeleteImagePerso : Boolean;
    Procedure PutToDlg;
    Procedure GetFromDlg;
  public
    { Public declarations }
  end;

  TVentil_Options = Class
    Constructor Create;
    Destructor Destroy;
  Private
    FGestionnaireVuesCAD : Boolean;
    procedure Set_GestionnaireVuesCAD(_Value : Boolean);
    Procedure InitRemises;
  Public
//    ImpressionsA3           : Boolean;
//    GestionnaireVuesCAD     : Boolean;
    NbSvgDisque             : Integer;
    ColonnesPiquagesExpress : Boolean;
    TerrassePiquageExpress  : Boolean;
    PiegesASon              : Boolean;
    GaineVeloduct           : Boolean;
    ColliersIso             : Boolean;
    ColonnesExcel           : Array[1..7] Of Boolean;
    ListeRemises            : TStringList;
    AutoSaveActive          : Boolean;
    AutoSaveDelai           : Integer;
    DefautFolderProjects    : String;
    DefautFolderExportXML   : String;
    { Pour l'affichage des �tiquettes }
    AffEtiq_Caisson : Boolean;
    AffEtiq_Plenum  : Boolean;
    AffEtiq_ClapetCoupeFeu : Boolean;
    AffEtiq_Registre  : Boolean;
    AffEtiq_ColonneSsPlenum  : Boolean;
    AffEtiq_Bifurc  : Boolean;
    AffEtiq_TeSouche: Boolean;
    AffEtiq_Collect : Boolean;
    AffEtiq_Bouche  : Boolean;
    AffEtiq_EA      : Boolean;
    AffEtiq_Gaine   : Boolean;
    AffEtiq_Col2nd  : Boolean;
    AffEtiq_RegDebit: Boolean;
    AffEtiq_Silenc  : Boolean;
    AffEtiq_LongGaine : Integer;
    AffEtiq_125Gaine : Integer;
    AffEtiq_BoucheReference : Boolean;
    AffEtiq_EAReference     : Boolean;
    AffEtiq_BouchePression  : Boolean;
    AffEtiq_BoucheDebit     : Boolean;
    AffEtiq_GaineReseauH    : Boolean;
    AffEtiq_GaineReseauV    : Boolean;
    AffEtiq_GaineReseauT    : Boolean;
    AffEtiq_GaineLongueur   : Boolean;
    AffEtiq_GaineDiametre   : Boolean;
    AffEtiq_GaineDebit      : Boolean;
    AffEtiq_TeteColDiametre : Boolean;
    AffEtiq_TeteColDebit    : Boolean;
    AffEtiq_TeteColPression : Boolean;
    TypeEtiquette   : TObjetCAD_Etiquette_ModeAffichage;
    Plombe          : Boolean;
    CodeUtilisateur : String;
    Nom             : String;
    Adresse1        : String;
    Adresse2        : String;
    Ville           : String;
    CodePostal      : String;
    LogoOriginal : Boolean;
    Iterate_Actif : Boolean;
    Iterate_Convergence : Single;
    Iterate_Max : Integer;
    Iterate_Export : Boolean;

    Impressions : TListeImpression;
    Property GestionnaireVuesCAD : Boolean read FGestionnaireVuesCAD write Set_GestionnaireVuesCAD;
    function GetPathLogo : String;
    procedure FinalisationLogo;
    procedure GestionLogo;
    Procedure SaveOptions;
    Procedure LoadOptions;
    Procedure LaunchDossierImpression(Sender : TObject);
  End;

Var
  Dlg_Options: TDlg_Options;


Const
{$IfDef VIM_OPTAIR}
  Cst_Remise : Array[0..36] Of String = ('A1', 'A2', 'A3', 'B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'C1', 'C2', 'C3', 'C4', 'D1', 'D2', 'E1', 'E2',
                                         'F1', 'F2', 'F3', 'G1', 'G2', 'G3', 'H1', 'H2', 'I1', 'J1', 'K1', 'K2', 'L1', 'L2', 'L3', 'L4', 'L5', 'L6',
                                         'M1', 'Y1');
                       
  Cst_CelluleXLS : Array[0..36] Of String = ('I2', 'I3', 'I4', 'I5', 'I6', 'I7', 'I8', 'I9', 'I10', 'I11', 'I12', 'I13', 'I14', 'I15', 'I16', 'I17', 'I18',
                                             'I19','I20', 'I21', 'K2', 'K3', 'K4', 'K5', 'K6', 'K7', 'K8', 'K9', 'K10', 'K11', 'K12', 'K13', 'K14', 'K15', 'K16',
                                             'K17', 'K18');
{$Else}
  Cst_Remise : Array[0..17] Of String = ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R');
{$EndIf}
  
Procedure ChangeOptions(var _Timer : TTimer);

Implementation
Uses
  IOUtils,
  BbsCom,
  Math,
  Ventil_ChoixImpressions,
  Ventil_Utils,
  Ventil_Const,
  BBS_Message,
  {$IfDef AERAU_CLIMAWIN}
  xtaeraulique,
  {$Else}
  VMC_Etude,
  {$EndIf}
  BbsgType,
  BbsImpFiche,
  Ventil_Declarations;

{$R *.dfm}

{ ************************************************************************************************************************************************** }
Procedure TVentil_Options.SaveOptions;
Var
  Reg        : TRegistry;
  m_NoRemise : Integer;
Begin
  Reg := TRegistry.Create;
  Reg.RootKey:=HKEY_CURRENT_USER;
  If Reg.OpenKey('\Software\' + EditeurName + '\' + ExtractFileName(Application.Exename), True) Then
  Begin
    Reg.WriteInteger('NbSvgDisque', NbSvgDisque);
    Reg.WriteBool('ColonnesPiquagesExpress', ColonnesPiquagesExpress);
    Reg.WriteBool('TerrassePiquageExpress', TerrassePiquageExpress);
    Reg.WriteBool('PiegesASon', PiegesASon);
    Reg.WriteBool('GaineVeloduct', GaineVeloduct);
    Reg.WriteBool('ColliersIso', ColliersIso);    
    Reg.WriteBool('XLS_Libelle', ColonnesExcel[cst_XLS_Libelle]);
    Reg.WriteBool('XLS_Conditionnement', ColonnesExcel[cst_XLS_Conditionnement]);
    Reg.WriteBool('XLS_PrixUnitaire', ColonnesExcel[cst_XLS_PUnitaire]);
    Reg.WriteBool('XLS_Quantite', ColonnesExcel[cst_XLS_Quantite]);
    Reg.WriteBool('XLS_MainOeuvre', ColonnesExcel[cst_XLS_MainDoeuvre]);
    Reg.WriteBool('XLS_PrixTotal', ColonnesExcel[cst_XLS_PTotal]);

    Reg.WriteBool('AutoSaveActive', AutoSaveActive);
    Reg.WriteInteger('AutoSaveDelai', AutoSaveDelai);

    Reg.WriteString('DefautFolderProjects', DefautFolderProjects);
    Reg.WriteString('DefautFolderExportXML', DefautFolderExportXML);

//    Reg.WriteBool('IMPRESSIONS_A3', ImpressionsA3);
{$IfNDef NO_GESTIONNAIREVUECAD}
    Reg.WriteBool('GESTIONNAIREVUESCAD', GestionnaireVuesCAD);
{$EndIf}

    Reg.WriteBool('ETIQUETTES_CAISSONS', AffEtiq_Caisson);
    Reg.WriteBool('ETIQUETTES_PLENUMS', AffEtiq_Plenum);
    Reg.WriteBool('ETIQUETTES_CLAPETSCF', AffEtiq_ClapetCoupeFeu);
    Reg.WriteBool('ETIQUETTES_REGISTRES', AffEtiq_Registre);
    Reg.WriteBool('ETIQUETTES_COLSPLENUMS', AffEtiq_ColonneSsPlenum);
    Reg.WriteBool('ETIQUETTES_BIFURCATIONS', AffEtiq_Bifurc);
    Reg.WriteBool('ETIQUETTES_TESSOUCHES', AffEtiq_TeSouche);
    Reg.WriteBool('ETIQUETTES_COLLECTEURS', AffEtiq_Collect);
    Reg.WriteBool('ETIQUETTES_BOUCHES', AffEtiq_Bouche);
    Reg.WriteBool('ETIQUETTES_EAS', AffEtiq_EA);    
    Reg.WriteBool('ETIQUETTES_GAINES', AffEtiq_Gaine);
    Reg.WriteBool('ETIQUETTES_COL2ND', AffEtiq_Col2nd);
    Reg.WriteBool('ETIQUETTES_REGDEBIT', AffEtiq_RegDebit);
    Reg.WriteBool('ETIQUETTES_SILENC', AffEtiq_Silenc);
    Reg.WriteInteger('ETIQUETTES_LONGGAINES', AffEtiq_LongGaine);
    Reg.WriteInteger('ETIQUETTES_125GAINES', AffEtiq_125Gaine);    
    Reg.WriteBool('ETIQUETTES_GAINERESEAUH', AffEtiq_GaineReseauH);
    Reg.WriteBool('ETIQUETTES_GAINERESEAUV', AffEtiq_GaineReseauV);
    Reg.WriteBool('ETIQUETTES_GAINERESEAUT', AffEtiq_GaineReseauT);
    Reg.WriteBool('ETIQUETTES_GAINELONGUEUR', AffEtiq_GaineLongueur);
    Reg.WriteBool('ETIQUETTES_GAINEDIAMETRE', AffEtiq_GaineDiametre);
    Reg.WriteBool('ETIQUETTES_GAINEDEBIT', AffEtiq_GaineDebit);
    Reg.WriteBool('ETIQUETTES_BOUCHEREFERENCE', AffEtiq_BoucheReference);
    Reg.WriteBool('ETIQUETTES_BOUCHEPRESSION', AffEtiq_BouchePression);
    Reg.WriteBool('ETIQUETTES_BOUCHEDEBIT', AffEtiq_BoucheDebit);
    Reg.WriteBool('ETIQUETTES_EAREFERENCE', AffEtiq_EAReference);
    Reg.WriteBool('ETIQUETTES_TETECOLDIAMETRE', AffEtiq_TeteColDiametre);
    Reg.WriteBool('ETIQUETTES_TETECOLDEBIT', AffEtiq_TeteColDebit);
    Reg.WriteBool('ETIQUETTES_TETECOLPRESSION', AffEtiq_TeteColPression);
    Reg.WriteInteger('TYPEETIQUETTE', Integer(TypeEtiquette));

    {$IfDEF ANJOS_OPTIMA3D}
    Reg.WriteString('CODEUTILISATEUR', CodeUtilisateur);
    Reg.WriteString('Nom', Nom);                                                            
    Reg.WriteString('Adresse1', Adresse1);
    Reg.WriteString('Adresse2', Adresse2);
    Reg.WriteString('Ville', Ville);
    Reg.WriteString('CodePostal', CodePostal);
    {$ENDIF}

    Reg.WriteBool('LOGOORIGINAL', LogoOriginal);

    Reg.WriteBool('ITERATE_ACTIF', Iterate_Actif);
    Reg.WriteFloat('ITERATE_CONVERGENCE', Iterate_Convergence);
    Reg.WriteInteger('ITERATE_MAX', Iterate_Max);
    Reg.WriteBool('ITERATE_EXPORT', Iterate_Export);
    {$IfDef VIM_OPTAIR}
    //logo
    {$ENDIF}
    GestionLogo;


    For m_NoRemise := 0 To ListeRemises.Count - 1 Do
    Begin
      Reg.WriteString('Remise_' + intToStr(m_NoRemise + 1), ListeRemises[m_NoRemise]);
    End;
  End;
End;
{ ************************************************************************************************************************************************** }
function TVentil_Options.GetPathLogo : String;
begin
  {$IfDef AERAU_CLIMAWIN}
  Result := XtAerauliqueMainForm.InitialDir + '\Logo.bmp';
  {$Else}
  Result := Fic_Etude.InitialDir + '\Logo.bmp';
  {$EndIf}
end;
{ ************************************************************************************************************************************************** }
procedure TVentil_Options.FinalisationLogo;
begin
  if IniLogiciel.OptionsLogiciel.OptionsImpressions.BitmapEnTete <> Nil then
    IniLogiciel.OptionsLogiciel.OptionsImpressions.BitmapEnTete.Free;
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Options.GestionLogo;
var
        ConditionLogoPerso : Boolean;
begin
  {$IfnDef AERAU_CLIMAWIN}
ConditionLogoPerso := not(Options.LogoOriginal);
{$IfDef VIM_OPTAIR}
  {$IfDef MVN_MVNAIR}
  {$Else}
    {$IfDef ACTHYS_DIMVMBP}
    {$Else}
    ConditionLogoPerso := ConditionLogoPerso And Fic_Etude.VersionAutonome;
    {$EndIf}
  {$EndIf}
{$EndIf}
  
        if ConditionLogoPerso then
                begin
                        if FileExists(GetPathLogo) then
                                begin
                                IniLogiciel.OptionsLogiciel.OptionsImpressions.BitmapEnTete := TBitmap.Create;
                                IniLogiciel.OptionsLogiciel.OptionsImpressions.BitmapEnTete.LoadFromFile(GetPathLogo);
                                end
                        else
                                begin
                                IniLogiciel.OptionsLogiciel.OptionsImpressions.BitmapEnTete := nil;
                                end;
                end
        else
 {$EndIf}

                begin
                IniLogiciel.OptionsLogiciel.OptionsImpressions.BitmapEnTete := TBitmap.Create;
                IniLogiciel.OptionsLogiciel.OptionsImpressions.BitmapEnTete.LoadFromFile(ExtractFilePath(Application.Exename) + 'Bibliotheque\Progress.bmp');
                end;


ParametrageImpressions.BitmapEnTete := IniLogiciel.OptionsLogiciel.OptionsImpressions.BitmapEnTete;
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Options.LoadOptions;
Var
  Reg      : TRegistry;
  NoRemise : Integer;
  ValRemise: String;
Begin
  Reg := TRegistry.Create;
  Reg.RootKey:=HKEY_CURRENT_USER;
  If Reg.OpenKey('\Software\' + EditeurName + '\' + ExtractFileName(Application.Exename), True) Then
  Begin
    If Reg.ValueExists('NbSvgDisque') Then NbSvgDisque := Reg.ReadInteger('NbSvgDisque');

    If Reg.ValueExists('ColonnesPiquagesExpress') Then ColonnesPiquagesExpress := Reg.ReadBool('ColonnesPiquagesExpress');
    If Reg.ValueExists('TerrassePiquageExpress')  Then TerrassePiquageExpress  := Reg.ReadBool('TerrassePiquageExpress');
    If Reg.ValueExists('PiegesASon')              Then PiegesASon              := Reg.ReadBool('PiegesASon');

    If Reg.ValueExists('GaineVeloduct')           Then GaineVeloduct           := Reg.ReadBool('GaineVeloduct');
    If Reg.ValueExists('ColliersIso')            Then ColliersIso              := Reg.ReadBool('ColliersIso');    

    {$IfDEF ANJOS_OPTIMA3D}
    If Reg.ValueExists('CODEUTILISATEUR')         Then CodeUtilisateur         := Reg.ReadString('CODEUTILISATEUR');
    If Reg.ValueExists('Nom')                     Then Nom                     := Reg.ReadString('Nom');
    If Reg.ValueExists('Adresse1')                Then Adresse1                := Reg.ReadString('Adresse1');
    If Reg.ValueExists('Adresse2')                Then Adresse2                := Reg.ReadString('Adresse2');
    If Reg.ValueExists('Ville')                   Then Ville                   := Reg.ReadString('Ville');
    If Reg.ValueExists('CodePostal')              Then CodePostal              := Reg.ReadString('CodePostal');
    {$ENDIF}

    If Reg.ValueExists('AutoSaveActive') Then AutoSaveActive := Reg.ReadBool('AutoSaveActive');
    If Reg.ValueExists('AutoSaveDelai')  Then AutoSaveDelai  := Reg.ReadInteger('AutoSaveDelai');
    If Reg.ValueExists('DefautFolderProjects') Then DefautFolderProjects := Reg.ReadString('DefautFolderProjects');
    If Reg.ValueExists('DefautFolderExportXML') Then DefautFolderExportXML := Reg.ReadString('DefautFolderExportXML');

    If Reg.ValueExists('XLS_Libelle')         Then ColonnesExcel[cst_XLS_Libelle        ] := Reg.ReadBool('XLS_Libelle');
    If Reg.ValueExists('XLS_Conditionnement') Then ColonnesExcel[cst_XLS_Conditionnement] := Reg.ReadBool('XLS_Conditionnement');
    If Reg.ValueExists('XLS_PrixUnitaire')    Then ColonnesExcel[cst_XLS_PUnitaire      ] := Reg.ReadBool('XLS_PrixUnitaire');
    If Reg.ValueExists('XLS_Quantite')        Then ColonnesExcel[cst_XLS_Quantite       ] := Reg.ReadBool('XLS_Quantite');
    If Reg.ValueExists('XLS_MainOeuvre')      Then ColonnesExcel[cst_XLS_MainDoeuvre    ] := Reg.ReadBool('XLS_MainOeuvre');
    If Reg.ValueExists('XLS_PrixTotal')       Then ColonnesExcel[cst_XLS_PTotal         ] := Reg.ReadBool('XLS_PrixTotal');

    {$IfDEF ANJOS_OPTIMA3D}
    ColonnesExcel[cst_XLS_Libelle        ] := True;
    ColonnesExcel[cst_XLS_Conditionnement] := False;
    ColonnesExcel[cst_XLS_PUnitaire      ] := True;
    ColonnesExcel[cst_XLS_Quantite       ] := True;
    ColonnesExcel[cst_XLS_MainDoeuvre    ] := True;
    ColonnesExcel[cst_XLS_PTotal         ] := True;
    {$ENDIF}

    {$Ifdef FRANCEAIR_AIRGIVMC}
    ColonnesExcel[cst_XLS_Libelle        ] := True;
    ColonnesExcel[cst_XLS_Conditionnement] := False;
    ColonnesExcel[cst_XLS_PUnitaire      ] := False;
    ColonnesExcel[cst_XLS_Quantite       ] := True;
    ColonnesExcel[cst_XLS_MainDoeuvre    ] := False;
    ColonnesExcel[cst_XLS_PTotal         ] := False;
    {$EndIf}

  {$Ifdef MVN_MVNAIR}
  ColonnesExcel[cst_XLS_Libelle        ] := True;
  ColonnesExcel[cst_XLS_Conditionnement] := False;
  ColonnesExcel[cst_XLS_PUnitaire      ] := False;
  ColonnesExcel[cst_XLS_Quantite       ] := True;
  ColonnesExcel[cst_XLS_MainDoeuvre    ] := False;
  ColonnesExcel[cst_XLS_PTotal         ] := False;
  {$EndIf}

    If Reg.ValueExists('ETIQUETTES_CAISSONS')     Then AffEtiq_Caisson  := Reg.ReadBool('ETIQUETTES_CAISSONS');
    If Reg.ValueExists('ETIQUETTES_PLENUMS')      Then AffEtiq_Plenum   := Reg.ReadBool('ETIQUETTES_PLENUMS');
    If Reg.ValueExists('ETIQUETTES_CLAPETSCF')      Then AffEtiq_ClapetCoupeFeu   := Reg.ReadBool('ETIQUETTES_CLAPETSCF');
    If Reg.ValueExists('ETIQUETTES_REGISTRES')      Then AffEtiq_Registre   := Reg.ReadBool('ETIQUETTES_REGISTRES');
    If Reg.ValueExists('ETIQUETTES_COLSPLENUMS')      Then AffEtiq_ColonneSsPlenum   := Reg.ReadBool('ETIQUETTES_COLSPLENUMS');
    If Reg.ValueExists('ETIQUETTES_BIFURCATIONS') Then AffEtiq_Bifurc   := Reg.ReadBool('ETIQUETTES_BIFURCATIONS');
    If Reg.ValueExists('ETIQUETTES_TESSOUCHES')   Then AffEtiq_TeSouche := Reg.ReadBool('ETIQUETTES_TESSOUCHES');
    If Reg.ValueExists('ETIQUETTES_COLLECTEURS')  Then AffEtiq_Collect  := Reg.ReadBool('ETIQUETTES_COLLECTEURS');
    If Reg.ValueExists('ETIQUETTES_BOUCHES')      Then AffEtiq_Bouche   := Reg.ReadBool('ETIQUETTES_BOUCHES');
    If Reg.ValueExists('ETIQUETTES_EAS')          Then AffEtiq_EA       := Reg.ReadBool('ETIQUETTES_EAS');    
    If Reg.ValueExists('ETIQUETTES_GAINES')       Then AffEtiq_Gaine    := Reg.ReadBool('ETIQUETTES_GAINES');
    If Reg.ValueExists('ETIQUETTES_COL2ND')       Then AffEtiq_Col2nd   := Reg.ReadBool('ETIQUETTES_COL2ND');
    If Reg.ValueExists('ETIQUETTES_REGDEBIT')     Then AffEtiq_RegDebit := Reg.ReadBool('ETIQUETTES_REGDEBIT');
    If Reg.ValueExists('ETIQUETTES_SILENC')       Then AffEtiq_Silenc   := Reg.ReadBool('ETIQUETTES_SILENC');
    if Reg.ValueExists('ETIQUETTES_LONGGAINES')   Then AffEtiq_LongGaine:= Reg.ReadInteger('ETIQUETTES_LONGGAINES');
    if Reg.ValueExists('ETIQUETTES_125GAINES')   Then AffEtiq_125Gaine:= Reg.ReadInteger('ETIQUETTES_125GAINES');    
    if Reg.ValueExists('ETIQUETTES_GAINERESEAUH')    Then AffEtiq_GaineReseauH    := Reg.ReadBool('ETIQUETTES_GAINERESEAUH');
    if Reg.ValueExists('ETIQUETTES_GAINERESEAUV')    Then AffEtiq_GaineReseauV    := Reg.ReadBool('ETIQUETTES_GAINERESEAUV');
    if Reg.ValueExists('ETIQUETTES_GAINERESEAUT')    Then AffEtiq_GaineReseauT    := Reg.ReadBool('ETIQUETTES_GAINERESEAUT');
    if Reg.ValueExists('ETIQUETTES_GAINELONGUEUR')    Then AffEtiq_GaineLongueur    := Reg.ReadBool('ETIQUETTES_GAINELONGUEUR');
    if Reg.ValueExists('ETIQUETTES_GAINEDIAMETRE')    Then AffEtiq_GaineDiametre    := Reg.ReadBool('ETIQUETTES_GAINEDIAMETRE');
    if Reg.ValueExists('ETIQUETTES_GAINEDEBIT')       Then AffEtiq_GaineDebit    := Reg.ReadBool('ETIQUETTES_GAINEDEBIT');
    if Reg.ValueExists('ETIQUETTES_BOUCHEREFERENCE') Then AffEtiq_BoucheReference := Reg.ReadBool('ETIQUETTES_BOUCHEREFERENCE');
    if Reg.ValueExists('ETIQUETTES_BOUCHEPRESSION') Then AffEtiq_BouchePression := Reg.ReadBool('ETIQUETTES_BOUCHEPRESSION');
    if Reg.ValueExists('ETIQUETTES_BOUCHEDEBIT') Then AffEtiq_BoucheDebit := Reg.ReadBool('ETIQUETTES_BOUCHEDEBIT');
    if Reg.ValueExists('ETIQUETTES_EAREFERENCE') Then AffEtiq_EAReference := Reg.ReadBool('ETIQUETTES_EAREFERENCE');
    if Reg.ValueExists('ETIQUETTES_TETECOLDIAMETRE') then AffEtiq_TeteColDiametre := Reg.ReadBool('ETIQUETTES_TETECOLDIAMETRE');
    if Reg.ValueExists('ETIQUETTES_TETECOLDEBIT') then AffEtiq_TeteColDebit := Reg.ReadBool('ETIQUETTES_TETECOLDEBIT');
    if Reg.ValueExists('ETIQUETTES_TETECOLPRESSION') then AffEtiq_TeteColPression := Reg.ReadBool('ETIQUETTES_TETECOLPRESSION');
    if Reg.ValueExists('TYPEETIQUETTE')    Then TypeEtiquette    := TObjetCAD_Etiquette_ModeAffichage(Reg.ReadInteger('TYPEETIQUETTE'));

    if Reg.ValueExists('LOGOORIGINAL') Then
        LogoOriginal := Reg.ReadBool('LOGOORIGINAL')
    else
        LogoOriginal := True;

    if Reg.ValueExists('ITERATE_ACTIF')    Then Iterate_Actif    := Reg.ReadBool('ITERATE_ACTIF');
    if Reg.ValueExists('ITERATE_CONVERGENCE')    Then Iterate_Convergence    := Reg.ReadFloat('ITERATE_CONVERGENCE');
    if Reg.ValueExists('ITERATE_MAX')    Then Iterate_Max    := Reg.ReadInteger('ITERATE_MAX');
    if Reg.ValueExists('ITERATE_EXPORT')    Then Iterate_Export    := Reg.ReadBool('ITERATE_EXPORT');

    If IniLogiciel.OptionsLogiciel.OptionsImpressions = Nil Then IniLogiciel.OptionsLogiciel.OptionsImpressions := TOptionsImpressions.Create;
    GestionLogo;

//    If Reg.ValueExists('IMPRESSIONS_A3') Then ImpressionsA3 := Reg.ReadBool('IMPRESSIONS_A3');
{$IfNDef NO_GESTIONNAIREVUECAD}
    If Reg.ValueExists('GESTIONNAIREVUESCAD') Then GestionnaireVuesCAD := Reg.ReadBool('GESTIONNAIREVUESCAD')
    else GestionnaireVuesCAD := True;
{$EndIf}
    { Lecture des systemes de remise ou creation s'il n'existent pas }
    NoRemise  := 1;
    If Reg.ValueExists('Remise_' + IntToStr(NoRemise)) Then
    While Reg.ValueExists('Remise_' + IntToStr(NoRemise)) Do Begin
      ValRemise := Reg.ReadString('Remise_' + IntToStr(NoRemise));
      ListeRemises.Add(ValRemise);
      Inc(NoRemise);
    End;
    If ListeRemises.Count <> length(Cst_Remise) Then InitRemises;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Options.LaunchDossierImpression(Sender : TObject);
begin
  ImpressionComplete;
end;
{ ************************************************************************************************************************************************** }
procedure TVentil_Options.Set_GestionnaireVuesCAD(_Value : Boolean);
begin
  FGestionnaireVuesCAD := _Value;
    if InterfaceCAD <> Nil then
      InterfaceCAD.Aero_Vilo2Fredo_Refresh(rtButtons);
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Options.InitRemises;
Var
  m_NoRemise : Integer;
Begin
ListeRemises.Clear;
  For m_NoRemise := Low(Cst_Remise) To High(Cst_Remise) Do ListeRemises.Add(Cst_Remise[m_NoRemise] + '|0' {$IfDef VIM_OPTAIR} + '|' + Cst_CelluleXLS[m_NoRemise]{$EndIf});
End;
{ ************************************************************************************************************************************************** }
Constructor TVentil_Options.Create;
Begin
  Inherited;
//  ImpressionsA3           := False;
  NbSvgDisque := 1;
{$IfNDef NO_GESTIONNAIREVUECAD}
  GestionnaireVuesCAD     := True;
{$Else}
  GestionnaireVuesCAD     := False;
{$EndIf}
  ColonnesPiquagesExpress := False;
  TerrassePiquageExpress  := False;
  PiegesASon              := False;
  {$Ifdef VIM_OPTAIR}
  GaineVeloduct           := True;
  ColliersIso             := False;
  {$Else}
  GaineVeloduct           := False;
  ColliersIso             := False;
  {$EndIf}
  {$Ifdef PANOL_VMCCONCEPT3D}
  ColliersIso             := True;
  {$EndIf}

  {$Ifdef FRANCEAIR_AIRGIVMC}
  ColonnesExcel[cst_XLS_Libelle        ] := True;
  ColonnesExcel[cst_XLS_Conditionnement] := False;
  ColonnesExcel[cst_XLS_PUnitaire      ] := False;
  ColonnesExcel[cst_XLS_Quantite       ] := True;
  ColonnesExcel[cst_XLS_MainDoeuvre    ] := False;
  ColonnesExcel[cst_XLS_PTotal         ] := False;
  {$Else}
  ColonnesExcel[cst_XLS_Libelle        ] := True;
  ColonnesExcel[cst_XLS_Conditionnement] := False;
  ColonnesExcel[cst_XLS_PUnitaire      ] := True;
  ColonnesExcel[cst_XLS_Quantite       ] := True;
  ColonnesExcel[cst_XLS_MainDoeuvre    ] := True;
  ColonnesExcel[cst_XLS_PTotal         ] := True;
  {$EndIf}
  {$Ifdef PANOL_VMCCONCEPT3D}
  ColonnesExcel[cst_XLS_Libelle        ] := True;
  ColonnesExcel[cst_XLS_Conditionnement] := False;
  ColonnesExcel[cst_XLS_PUnitaire      ] := False;
  ColonnesExcel[cst_XLS_Quantite       ] := True;
  ColonnesExcel[cst_XLS_MainDoeuvre    ] := False;
  ColonnesExcel[cst_XLS_PTotal         ] := False;
  {$EndIf}
  {$Ifdef MVN_MVNAIR}
  ColonnesExcel[cst_XLS_Libelle        ] := True;
  ColonnesExcel[cst_XLS_Conditionnement] := False;
  ColonnesExcel[cst_XLS_PUnitaire      ] := False;
  ColonnesExcel[cst_XLS_Quantite       ] := True;
  ColonnesExcel[cst_XLS_MainDoeuvre    ] := False;
  ColonnesExcel[cst_XLS_PTotal         ] := False;
  {$EndIf}
  AffEtiq_Caisson  := True;
  AffEtiq_Plenum   := True;
  AffEtiq_ClapetCoupeFeu := False;
  AffEtiq_Registre   := False;
  AffEtiq_ColonneSsPlenum   := True;
  AffEtiq_Bifurc   := True;
  AffEtiq_TeSouche := True;
  AffEtiq_Collect  := True;
  AffEtiq_Bouche   := True;
  AffEtiq_BoucheReference:= False;
  AffEtiq_BouchePression:= False;
  AffEtiq_BoucheDebit:= False;
  AffEtiq_EA       := True;
  AffEtiq_EAReference       := True;
  AffEtiq_Gaine    := True;
  AffEtiq_Col2nd   := True;
  AffEtiq_RegDebit := True;
  AffEtiq_Silenc   := True;
  ListeRemises     := TStringList.Create;
  Plombe           := True;
  AffEtiq_LongGaine := c_etiq_LongGaineAll;
  AffEtiq_125Gaine := c_etiq_125GaineAll;
  {$IfnDef AERAU_CLIMAWIN}
  DefautFolderProjects := Fic_Etude.InitialDir;
  DefautFolderExportXML := Fic_Etude.InitialDir;
  {$EndIf}

  Iterate_Actif := False;
  Iterate_Convergence := 0.5;
  Iterate_Max := 100;
  Iterate_Export := False;

  Impressions := TListeImpression.Create(Self);
End;
{ ************************************************************************************************************************************************** }
Destructor TVentil_Options.Destroy;
begin
  Impressions.Destroy;
  Inherited;
end;
{ ************************************************************************************************************************************************** }
Procedure TDlg_Options.GetFromDlg;
Var
  NoRemise : Integer;
Begin
  Options.NbSvgDisque                            := StrToInt(Edit_NbSvgDisk.Text);
  Options.ColonnesPiquagesExpress                := CheckBox_ColonneExpress.Checked;
  Options.TerrassePiquageExpress                 := CheckBox_Terrasse.Checked;
  Options.PiegesASon                             := CheckBox_PiegesASon.Checked;
  Options.GaineVeloduct                          := CheckBox_Veloduct.Checked;
  Options.ColliersIso                            := CheckBox_ColliersIso.Checked;
  Options.ColonnesExcel[cst_XLS_Libelle]         := CheckBox_Libelle.Checked;
  Options.ColonnesExcel[cst_XLS_PUnitaire]       := CheckBox_PUnitaire.Checked;
  Options.ColonnesExcel[cst_XLS_Conditionnement] := CheckBox_Conditionnement.Checked;
  Options.ColonnesExcel[cst_XLS_Quantite]        := CheckBox_Quantite.Checked;
  Options.ColonnesExcel[cst_XLS_MainDOeuvre]     := CheckBox_MainDoeuvre.Checked;
  Options.ColonnesExcel[cst_XLS_PTotal]          := CheckBox_PrixTotal.Checked;

  {$IfDef VIM_OPTAIR}
  Options.AutoSaveActive  := CheckBox_AutoSaveActivate.Checked;
  Options.AutoSaveDelai   := Round(StrToInt(Edit_DelaiSauvegarde.Text ) * 60 * 1000);
  {$EndIf}

  {$IfDef MVN_MVNAIR}
  Options.AutoSaveActive  := CheckBox_AutoSaveActivate.Checked;
  Options.AutoSaveDelai   := Round(StrToInt(Edit_DelaiSauvegarde.Text ) * 60 * 1000);
  {$EndIf}

  {$IfDef ACTHYS_DIMVMBP}
  Options.DefautFolderProjects  := EditDefautFolderProjects.Text;
  Options.DefautFolderExportXML  := EditDefautFolderExportXML.Text;

  Options.AutoSaveActive  := CheckBox_AutoSaveActivate.Checked;
  Options.AutoSaveDelai   := Round(StrToInt(Edit_DelaiSauvegarde.Text ) * 60 * 1000);
  {$EndIf}

  Options.LogoOriginal    := CheckBox_LogoOriginal.Checked;

  Options.Iterate_Actif   := CheckBox_IteratifActif.Checked;
  Options.Iterate_Convergence   := AdvEdit_IteratifConvergence.FloatValue;
  Options.Iterate_Max   := AdvEdit_IteratifMax.IntValue;
  Options.Iterate_Export   := CheckBox_IteratifExport.Checked;

        if (ImagePrintEnTete.Picture.Bitmap = nil) or DeleteImagePerso then
                begin
                        if FileExists(Options.GetPathLogo) then
                                DeleteFile(Options.GetPathLogo);
                end
        else
                begin
                TDirectory.CreateDirectory(ExtractFilePath(Options.GetPathLogo));
                ImagePrintEnTete.Picture.SaveToFile(Options.GetPathLogo);
                end;

                        if FileExists(Options.GetPathLogo) then
                ImagePrintEnTete.Picture.LoadFromFile(Options.GetPathLogo);


  Options.GestionLogo;
  { Impressions }
//  Options.ImpressionsA3 := CheckBox_ImpressionsA3.Checked;
  Options.GestionnaireVuesCAD := CheckBox_GestionnaireVuesCAD.Checked;
  { Affichage �tiquettes }
  Options.AffEtiq_Caisson  := CheckBox_EtiqCaissons.Checked;
  Options.AffEtiq_Plenum   := CheckBox_EtiqPlenums.Checked;
  Options.AffEtiq_ClapetCoupeFeu   := CheckBox_EtiqClapetCoupeFeu.Checked;
  Options.AffEtiq_Registre := CheckBox_EtiqRegistres.Checked;
  Options.AffEtiq_ColonneSsPlenum   := CheckBox_EtiqColonnesSsPlenums.Checked;
  Options.AffEtiq_Bifurc   := CheckBox_EtiqBifurcations.Checked;
  Options.AffEtiq_TeSouche := CheckBox_EtiqTeSouches.Checked;
  Options.AffEtiq_Collect  := CheckBox_EtiqCollecteurs.Checked;
  Options.AffEtiq_Bouche   := CheckBox_EtiqBouches.Checked;
  Options.AffEtiq_EA       := CheckBox_EtiqBouches.Checked;  
  Options.AffEtiq_Gaine    := CheckBox_EtiqGaines.Checked;
  Options.AffEtiq_Col2nd   := CheckBox_EtiqColonnes.Checked;
  Options.AffEtiq_RegDebit := CheckBox_EtiqRegDebit.Checked;
  Options.AffEtiq_Silenc   := CheckBox_EtiqSilenc.Checked;
  Options.AffEtiq_LongGaine := RadioGroupEtGaines.ItemIndex;
  Options.AffEtiq_125Gaine := RadioGroupGaine125.ItemIndex;  
  Options.AffEtiq_GaineReseauH    := CheckBoxEtGaineRHor.Checked;
  Options.AffEtiq_GaineReseauV    := CheckBoxEtGaineRVert.Checked;
  Options.AffEtiq_GaineReseauT    := CheckBoxEtGaineRToit.Checked;
  Options.AffEtiq_GaineLongueur   := CheckBoxEtiGaineLong.Checked;
  Options.AffEtiq_GaineDiametre    := CheckBoxEtiGaineDiam.Checked;
  Options.AffEtiq_GaineDebit    := CheckBoxEtiGaineDebit.Checked;
  Options.AffEtiq_BoucheReference := CheckBoxEtBoucheRef.Checked;
  Options.AffEtiq_EAReference := CheckBoxEtBoucheRef.Checked;
  Options.AffEtiq_BouchePression := CheckBoxEtBouchePression.Checked;
  Options.AffEtiq_BoucheDebit := CheckBoxEtBoucheDebit.Checked;
  Options.AffEtiq_TeteColDiametre := CheckBoxEtiTeteColDiametre.Checked;
  Options.AffEtiq_TeteColDebit := CheckBoxEtiTeteColDebit.Checked;
  Options.AffEtiq_TeteColPression := CheckBoxEtiTeteColPression.Checked;
  Options.TypeEtiquette    := TObjetCAD_Etiquette_ModeAffichage(RadioGroupTypeEtiquette.ItemIndex);

  For NoRemise := 1 To Grid_Remises.RowCount - 1 Do
    Begin
    Options.ListeRemises[NoRemise - 1] := Grid_Remises.Cells[0, NoRemise] + '|' + Grid_Remises.Cells[1, NoRemise] {$IfDef VIM_OPTAIR} + '|' + Cst_CelluleXLS[NoRemise - 1]{$EndIf};
    End;

  Options.SaveOptions;
  
        if Etude <> nil then
                Begin
                Etude.MiseAjourEtiquettesVisibles;
                End;

InterfaceCAD.Aero_Vilo2Fredo_ValidateFCADPropertiesEtiquettesParent;
End;
procedure TDlg_Options.Label8Click(Sender: TObject);
begin

end;

{ ************************************************************************************************************************************************** }
Procedure TDlg_Options.PutToDlg;
Var
  NoRemise : Integer;
  TabSheetCAD : TTabSheet;
  cpt : Integer;
Begin

  Edit_NbSvgDisk.Text       := IntToStr(Options.NbSvgDisque);
  UpDown_NbSvgDisk.Position := StrToInt(Edit_NbSvgDisk.Text);

  CheckBox_ColonneExpress.Checked  := Options.ColonnesPiquagesExpress;
  CheckBox_Terrasse.Checked        := Options.TerrassePiquageExpress;
  CheckBox_PiegesASon.Checked      := Options.PiegesASon;
  CheckBox_Veloduct.Checked        := Options.GaineVeloduct;
  CheckBox_ColliersIso.Checked     := Options.ColliersIso;  
  CheckBox_Libelle.Checked         := Options.ColonnesExcel[cst_XLS_Libelle];
  CheckBox_PUnitaire.Checked       := Options.ColonnesExcel[cst_XLS_PUnitaire];
  CheckBox_Conditionnement.Checked := Options.ColonnesExcel[cst_XLS_Conditionnement];
  CheckBox_Quantite.Checked        := Options.ColonnesExcel[cst_XLS_Quantite];
  CheckBox_MainDoeuvre.Checked     := Options.ColonnesExcel[cst_XLS_MainDoeuvre];
  CheckBox_PrixTotal.Checked       := Options.ColonnesExcel[cst_XLS_PTotal];

  CheckBox_EtiqCaissons.Checked     := Options.AffEtiq_Caisson;
  CheckBox_EtiqPlenums.Checked      := Options.AffEtiq_Plenum;
  CheckBox_EtiqClapetCoupeFeu.Checked  := Options.AffEtiq_ClapetCoupeFeu;
  CheckBox_EtiqRegistres.Checked      := Options.AffEtiq_Registre;
  CheckBox_EtiqColonnesSsPlenums.Checked      := Options.AffEtiq_ColonneSsPlenum;
  CheckBox_EtiqBifurcations.Checked := Options.AffEtiq_Bifurc;
  CheckBox_EtiqTeSouches.Checked    := Options.AffEtiq_TeSouche;
  CheckBox_EtiqCollecteurs.Checked  := Options.AffEtiq_Collect;
  CheckBox_EtiqBouches.Checked      := Options.AffEtiq_Bouche;
  CheckBox_EtiqGaines.Checked       := Options.AffEtiq_Gaine;
  CheckBox_EtiqColonnes.Checked     := Options.AffEtiq_Col2nd;
  CheckBox_EtiqRegDebit.Checked     := Options.AffEtiq_RegDebit;
  CheckBox_EtiqSilenc.Checked       := Options.AffEtiq_Silenc;
  RadioGroupEtGaines.ItemIndex      := Options.AffEtiq_LongGaine;
  RadioGroupGaine125.ItemIndex      := Options.AffEtiq_125Gaine;  
  CheckBoxEtGaineRVert.Checked      := Options.AffEtiq_GaineReseauV;
  CheckBoxEtGaineRToit.Checked      := Options.AffEtiq_GaineReseauT;
  CheckBoxEtGaineRHor.Checked       := Options.AffEtiq_GaineReseauH;
  CheckBoxEtiGaineLong.Checked      := Options.AffEtiq_GaineLongueur;
  CheckBoxEtiGaineDiam.Checked      := Options.AffEtiq_GaineDiametre;
  CheckBoxEtiGaineDebit.Checked      := Options.AffEtiq_GaineDebit;
  CheckBoxEtBoucheRef.Checked       := Options.AffEtiq_BoucheReference;
  CheckBoxEtBouchePression.Checked  := Options.AffEtiq_BouchePression;
  CheckBoxEtBoucheDebit.Checked     := Options.AffEtiq_BoucheDebit;
  CheckBoxEtiTeteColDiametre.Checked := Options.AffEtiq_TeteColDiametre;
  CheckBoxEtiTeteColDebit.Checked := Options.AffEtiq_TeteColDebit;
  CheckBoxEtiTeteColPression.Checked := Options.AffEtiq_TeteColPression;
  RadioGroupTypeEtiquette.ItemIndex := Integer(Options.TypeEtiquette);

  CheckBox_LogoOriginal.Caption := 'Utiliser le logo ' + cst_NomFabricant;
  {$IfDef VIM_OPTAIR}
    {$IfDef ACTHYS_DIMVMBP}
    CheckBox_Veloduct.Caption := 'Gaines Accessoires � joint';
    {$Else}
    CheckBox_Veloduct.Caption := 'Gaines Galva � joint';
    {$EndIf}
  {$Else}
        {$IfDef FRANCEAIR_AIRGIVMC}
        CheckBox_Veloduct.Caption := 'Gaines Accessoires � joint';
        {$Else}
                {$IfDef CALADAIR_VMC}
                CheckBox_Veloduct.Caption := 'Gaines Accessoires � joint';
                {$Else}
                  {$IfDef LINDAB_VMC}
                  CheckBox_Veloduct.Caption := 'Gaines Accessoires � joint';
                  {$Else}
                    {$IfDef ACTHYS_DIMVMBP}
                    CheckBox_Veloduct.Caption := 'Gaines Accessoires � joint';
                    {$Else}
                    {$IfDef MVN_MVNAIR}
                    CheckBox_Veloduct.Caption := 'Gaines Accessoires � joint';
                    {$Else}
                    CheckBox_Veloduct.Caption := 'Gaines Accessoires � joint';
                    {$EndIf}
                  {$EndIf}
                  {$EndIf}
                {$EndIf}
        {$EndIf}
  {$EndIf}


  TabSheet_ParamCalculs.TabVisible := False;
  TabSheet_ParamDivers.TabVisible := False;
  {$IfDef ACTHYS_DIMVMBP}
  CheckBox_EtiqCaissons.Top := CheckBox_EtiqBifurcations.Top;
  CheckBox_EtiqPlenums.Top := CheckBox_EtiqTeSouches.Top;
  CheckBox_EtiqColonnesSsPlenums.Top := CheckBox_EtiqColonnes.Top;
  TabSheet_ParamCalculs.TabVisible := True;
  TabSheet_ParamDivers.TabVisible := True;
  TabSheet_Chiffrage.TabVisible := False;
  {$EndIf}

 {$IfDef AERAU_CLIMAWIN}
  TabSheet_Chiffrage.TabVisible := False;
  TabSheet_Remises.TabVisible := False;
 {$EndIf}

//  LockCWAutoSave;
//  AbortAllActions;

  TabSheetCAD := TTabSheet.Create(PageControl1);
  TabSheetCAD.PageControl := PageControl1;
  TabSheetCAD.Caption := 'Etiquettes graphiques';
  TabSheetCAD.PageIndex := TabSheet_Etiquettes.PageIndex + 1;
  InterfaceCAD.Aero_Vilo2Fredo_SetFCADPropertiesEtiquettesParent(TabSheetCAD, clWindow);

    CheckBox_GestionnaireVuesCAD.Visible := False;
{$IfNDef NO_GESTIONNAIREVUECAD}
  if Not(TabSheet_ParamImpressions.TabVisible) then
    begin
    TabSheet_ParamImpressions.TabVisible := True;
    GroupBox9.Visible := False;
    CheckBox_LogoOriginal.Visible := False;
    CheckBox_GestionnaireVuesCAD.Top := CheckBox_LogoOriginal.Top;
    CheckBox_GestionnaireVuesCAD.Left := CheckBox_LogoOriginal.Left;
    end;
  CheckBox_GestionnaireVuesCAD.Visible := True;
{$EndIf}

  CheckBox_EtiqClapetCoupeFeu.Visible := False;

  GroupBoxEtiTeteColAff.Visible := False;
  GroupBoxEtBoucheOptions.Height := 25;
  CheckBoxEtBouchePression.Visible := False;
  CheckBoxEtBoucheDebit.Visible := False;
  CheckBox_EtiqPlenums.Visible      := False;
  CheckBox_EtiqRegistres.Visible      := False;
  CheckBox_EtiqColonnesSsPlenums.Visible      := False;
  {$IfDef ACTHYS_DIMVMBP}
  CheckBox_EtiqPlenums.Visible      := True;
  CheckBox_EtiqColonnesSsPlenums.Visible      := True;
  {$EndIf}
  {$IfDef MVN_MVNAIR}
  CheckBox_EtiqRegistres.Visible      := True;
  CheckBox_EtiqPlenums.Caption  := 'T�tes de colonnes';
  CheckBox_EtiqColonnesSsPlenums.Caption  := 'Colonnes';
  CheckBox_EtiqPlenums.Visible      := True;
  CheckBox_EtiqColonnesSsPlenums.Visible      := True;
  GroupBoxEtBoucheOptions.Height := 57;
  CheckBoxEtBouchePression.Visible := True;
  CheckBoxEtBoucheDebit.Visible := True;
  CheckBox_EtiqPlenums.Top := CheckBox_EtiqBouches.Top + 28;
  CheckBox_EtiqCaissons.Top := CheckBox_EtiqBifurcations.Top;
  CheckBox_EtiqColonnesSsPlenums.Top := CheckBox_EtiqTesouches.Top;
  CheckBox_EtiqColonnesSsPlenums.Left := CheckBox_EtiqBifurcations.Left;
  CheckBox_EtiqTesouches.Left := CheckBox_EtiqGaines.Left;
  CheckBox_EtiqTesouches.Top := CheckBox_EtiqColonnes.Top;
  CheckBox_EtiqRegistres.Top := CheckBox_EtiqColonnesSsPlenums.Top;
  CheckBox_EtiqRegistres.Left := CheckBox_EtiqTesouches.Left;
  GroupBoxEtiTeteColAff.Visible := CheckBox_EtiqTeSouches.Checked or CheckBox_EtiqPlenums.Checked;
  {$EndIf}

  TabSheet_Save.TabVisible      := True;
  Panel_Enregistrement_Copies.Visible := False;
  {$IfDef ANJOS_OPTIMA3D}
  CheckBox_EtiqRegDebit.Visible     := False;
  CheckBox_EtiqSilenc.Visible       := False;
  TabSheet_Remises.TabVisible       := False;
  TabSheet_Save.TabVisible          := True;
  PanelFoldersActhys.Visible        := False;
  PanelAutoSave.Visible             := False;
  Panel_Enregistrement_Copies.Visible := True;
  RadioGroupEtGaines.Visible        := False;
  RadioGroupGaine125.Visible        := False;
  GroupBoxEtGaineReseau.Visible     := Options.AffEtiq_Gaine;
  GroupBoxEtBoucheOptions.Visible   := Options.AffEtiq_Bouche;
  GroupBoxEtiGainAff.Visible        := False;
  CheckBox_ColliersIso.Visible      := True;
  {$EndIf}

  {$IfDef ACTHYS_DIMVMBP}
  TabSheet_Save.TabVisible      := False;
  {$EndIf}

  {$IfDef MVN_MVNAIR}
  GroupBoxEtiGainAff.Visible        := Options.AffEtiq_Gaine;
  GroupBoxEtiGainAff.Height         := 31;
  CheckBoxEtiGaineLong.Visible      := False;
  CheckBoxEtiGaineDiam.Visible      := False;
  CheckBoxEtiGaineDebit.Visible     := True;
  CheckBoxEtiGaineDebit.Top         := CheckBoxEtiGaineLong.Top;
  {$EndIF}

  CheckBox_LogoOriginal.Checked     := Options.LogoOriginal;

  CheckBox_IteratifActif.Checked     := Options.Iterate_Actif;
  CheckBox_IteratifExport.Checked     := Options.Iterate_Export;
  AdvEdit_IteratifConvergence.FloatValue := Options.Iterate_Convergence;
  AdvEdit_IteratifMax.IntValue := Options.Iterate_Max;

  {$IfDef VIM_OPTAIR}
  CheckBox_ColliersIso.Visible      := False;
  CheckBox_ColonneExpress.Visible   := False;
  CheckBox_Terrasse.Visible         := False;
  RadioGroupEtGaines.Visible        := Options.AffEtiq_Gaine;
  RadioGroupGaine125.Visible        := Options.AffEtiq_Gaine;
  GroupBoxEtGaineReseau.Visible     := False;
  GroupBoxEtBoucheOptions.Visible   := False;
  GroupBoxEtiGainAff.Visible        := Options.AffEtiq_Gaine;
  GroupBoxEtiGainAff.Height         := 50;
  CheckBoxEtiGaineLong.Visible      := True;
  CheckBoxEtiGaineDiam.Visible      := True;
  CheckBoxEtiGaineDebit.Visible     := False;
  CheckBox_EtiqClapetCoupeFeu.Visible  := True;
  CheckBox_EtiqClapetCoupeFeu.Top   := CheckBox_EtiqPlenums.Top;
  CheckBox_EtiqClapetCoupeFeu.Left  := CheckBox_EtiqPlenums.Left;

  CheckBox_AutoSaveActivate.Checked := Options.AutoSaveActive;
  Edit_DelaiSauvegarde.Text         := IntToStr(Round(Options.AutoSaveDelai / 60 / 1000));
  UpDown_delaiSauvegarde.Position   := StrToInt(Edit_DelaiSauvegarde.Text);

  {$IfnDef AERAU_CLIMAWIN}
{$IfDef MVN_MVNAIR}
{$Else}
{$IfDef ACTHYS_DIMVMBP}
{$Else}
  TabSheet_ParamImpressions.TabVisible := Fic_Etude.VersionAutonome;
{$EndIf}
{$EndIf}
  {$Else}
  TabSheet_ParamImpressions.TabVisible := False;

  CheckBox_AutoSaveActivate.Checked := Options.AutoSaveActive;
  Edit_DelaiSauvegarde.Text         := IntToStr(Round(Options.AutoSaveDelai / 60 / 1000));
  UpDown_delaiSauvegarde.Position   := StrToInt(Edit_DelaiSauvegarde.Text);
  {$EndIf}

  {$EndIf}

  {$IfDef ACTHYS_DIMVMBP}
  TabSheet_AutoSave.TabVisible      := True;
  TabSheet_AutoSave.Caption := 'Sauvegarde';
  Panel_Enregistrement_Copies.Visible := False;
  EditDefautFolderProjects.Text := Options.DefautFolderProjects;
  LabelDefautFolderProjects.Visible := True;
  EditDefautFolderProjects.Visible := True;
  ButtonDefautFolderProjects.Visible := True;
  EditDefautFolderExportXML.Text := Options.DefautFolderExportXML;
  LabelDefautFolderExportXML.Visible := True;
  EditDefautFolderExportXML.Visible := True;
  ButtonDefautFolderExportXML.Visible := True;
  CheckBox_AutoSaveActivate.Visible := True;
  Edit_DelaiSauvegarde.Visible := True;
  UpDown_delaiSauvegarde.Visible := True;
  Label7.Visible := True;
  Label8.Visible := True;
//  LabelDefautFolderProjects.Top := CheckBox_AutoSaveActivate.Top;
  EditDefautFolderProjects.Top := LabelDefautFolderProjects.Top + LabelDefautFolderProjects.Height;
  ButtonDefautFolderProjects.Top := LabelDefautFolderProjects.Top + LabelDefautFolderProjects.Height;

  LabelDefautFolderExportXML.Top := EditDefautFolderProjects.Top + EditDefautFolderProjects.Height + 10;
  EditDefautFolderExportXML.Top := LabelDefautFolderExportXML.Top + LabelDefautFolderExportXML.Height;
  ButtonDefautFolderExportXML.Top := LabelDefautFolderExportXML.Top + LabelDefautFolderExportXML.Height;

  FolderDialog1.Directory := Options.DefautFolderProjects;

  CheckBox_AutoSaveActivate.Checked := Options.AutoSaveActive;
  Edit_DelaiSauvegarde.Text         := IntToStr(Round(Options.AutoSaveDelai / 60 / 1000));
  UpDown_delaiSauvegarde.Position   := StrToInt(Edit_DelaiSauvegarde.Text);
  {$Else}
  LabelDefautFolderProjects.Visible := False;
  EditDefautFolderProjects.Visible := False;
  ButtonDefautFolderProjects.Visible := False;
  LabelDefautFolderExportXML.Visible := False;
  EditDefautFolderExportXML.Visible := False;
  ButtonDefautFolderExportXML.Visible := False;
  {$EndIf}

  {$IfDef MVN_MVNAIR}
  TabSheet_Save.TabVisible      := True;
  Panel_Enregistrement_Copies.Visible := False;
  CheckBox_AutoSaveActivate.Checked := Options.AutoSaveActive;
  Edit_DelaiSauvegarde.Text         := IntToStr(Round(Options.AutoSaveDelai / 60 / 1000));
  UpDown_delaiSauvegarde.Position   := StrToInt(Edit_DelaiSauvegarde.Text);
  GroupBoxExportCSV.Visible := False;
  {$EndIf}

 {$Ifdef FRANCEAIR_AIRGIVMC}
 GroupBoxExportCSV.Visible := False;
 {$EndIf}


//  CheckBox_ImpressionsA3.Checked    := Options.ImpressionsA3;
  CheckBox_GestionnaireVuesCAD.Checked    := Options.GestionnaireVuesCAD;


  DeleteImagePerso := False;
        if FileExists(Options.GetPathLogo) then
                ImagePrintEnTete.Picture.LoadFromFile(Options.GetPathLogo);

  Grid_Remises.RowCount := Options.ListeRemises.Count + 1;
  For NoRemise := 0 To Options.ListeRemises.Count - 1 Do Begin
    Grid_Remises.Cells[0, NoRemise +1] := GetSubStr(Options.ListeRemises[NoRemise], 1, '|');
    Grid_Remises.Cells[1, NoRemise +1] := GetSubStr(Options.ListeRemises[NoRemise], 2, '|');
  End;

    for cpt := 0 to PageControl1.PageCount - 1 do
      if PageControl1.Pages[cpt].TabVisible then
        begin
        PageControl1.ActivePageIndex := cpt;
        Break;
        end;



End;
{ ************************************************************************************************************************************************** }
Procedure ChangeOptions(var _Timer : TTimer);
Begin
        if Etude <> nil then
                Etude.AutoSaveAutorisee := False;
  Application.CreateForm(TDlg_Options, Dlg_Options);

  Dlg_Options.putToDlg;
  If Dlg_Options.ShowModal = Id_Ok Then
        Begin
        Dlg_Options.GetFromDlg;
                if _Timer <> nil then
                        Begin
                        _Timer.Enabled  := Options.AutoSaveActive;
                        _Timer.Interval := Options.AutoSaveDelai;
                        End;
        End;

  InterfaceCAD.Aero_Vilo2Fredo_ResetFCADPropertiesEtiquettesParent;

  FreeAndNil(Dlg_Options);
        if Etude <> nil then
                Begin
                Etude.AutoSaveAutorisee := True;
                {$IfDef ANJOS_OPTIMA3D}
                Etude.Calculer;
                {$EndIf}
                End;
End;
{ ************************************************************************************************************************************************** }

procedure TDlg_Options.CheckBox_EtiqGainesClick(Sender: TObject);
begin
{$IfDef VIM_OPTAIR}
RadioGroupEtGaines.Visible := CheckBox_EtiqGaines.Checked;
RadioGroupGaine125.Visible := CheckBox_EtiqGaines.Checked;
GroupBoxEtiGainAff.Visible := CheckBox_EtiqGaines.Checked;
{$EndIf}
{$IfDef ANJOS_OPTIMA3D}
GroupBoxEtGaineReseau.Visible := CheckBox_EtiqGaines.Checked;
{$EndIf}
{$IfDef MVN_MVNAIR}
GroupBoxEtiGainAff.Visible := CheckBox_EtiqGaines.Checked;
{$EndIf}
end;
{ ************************************************************************************************************************************************** }
procedure TDlg_Options.CheckBox_EtiqPlenumsClick(Sender: TObject);
begin
{$IfDef MVN_MVNAIR}
GroupBoxEtiTeteColAff.Visible := CheckBox_EtiqTeSouches.Checked or CheckBox_EtiqPlenums.Checked;
{$EndIf}
end;
{ ************************************************************************************************************************************************** }
procedure TDlg_Options.CheckBox_EtiqTeSouchesClick(Sender: TObject);
begin
{$IfDef MVN_MVNAIR}
GroupBoxEtiTeteColAff.Visible := CheckBox_EtiqTeSouches.Checked or CheckBox_EtiqPlenums.Checked;
{$EndIf}
end;
{ ************************************************************************************************************************************************** }
procedure TDlg_Options.CheckBox_EtiqBouchesClick(Sender: TObject);
begin
{$IfDef ANJOS_OPTIMA3D}
GroupBoxEtBoucheOptions.Visible := CheckBox_EtiqBouches.Checked;
{$EndIf}

end;
{ ************************************************************************************************************************************************** }
procedure TDlg_Options.Edit_DelaiSauvegardeKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Not Ord(Key) In [VK_DELETE, VK_RIGHT, VK_LEFT] Then Begin
    If Str2Int(Edit_DelaiSauvegarde.Text + Key) = 0 Then Key := #0;
  End;
end;
procedure TDlg_Options.Edit_NbSvgDiskKeyPress(Sender: TObject; var Key: Char);
begin
  if not Ord(Key) in [VK_DELETE, VK_RIGHT, VK_LEFT] then
    if Str2Int(Edit_NbSvgDisk.Text+Key)=0 then
      Key := #0;
end;

{ ************************************************************************************************************************************************** }
procedure TDlg_Options.ButtonDefautFolderExportXMLClick(Sender: TObject);
begin
  FolderDialog1.Directory := Options.DefautFolderExportXML;
  if FolderDialog1.Execute then
  EditDefautFolderExportXML.Text := FolderDialog1.Directory;
end;
{ ************************************************************************************************************************************************** }
procedure TDlg_Options.ButtonDefautFolderProjectsClick(Sender: TObject);
begin
  FolderDialog1.Directory := Options.DefautFolderProjects;
  if FolderDialog1.Execute then
    EditDefautFolderProjects.Text := FolderDialog1.Directory;
end;
{ ************************************************************************************************************************************************** }
procedure TDlg_Options.ButtonImportXLSClick(Sender: TObject);
var
        vMSExcel : OleVariant;
        vXLWorkbooks : OleVariant;
        vXLWorkbook : OleVariant;
        vWorksheet : OleVariant;
        vCell : OleVariant;
        aValue : OleVariant;
        aSheetName : String;
        fichier  : String;
        aRange : String;
        NoRemise : integer;
begin

If OpenDialogXLS.Execute Then
Begin

{ Nouvelle instance }
  vMSExcel := CreateOleObject('Excel.Application');
  vMSExcel.Visible := False ;
 
  { Ouverture d'un classeur }
  fichier := OpenDialogXLS.FileName;
  vXLWorkbooks := vMSExcel.Workbooks;
  vXLWorkbook := vXLWorkbooks.Open(fichier);

  { Acc�der � une feuille de calcul }
  try
  aSheetName := 'Tarif Public VIM';
  vWorksheet := vXLWorkbook.WorkSheets[aSheetName];

  For NoRemise := 0 To Options.ListeRemises.Count - 1 Do
    Begin

    aRange:=GetSubStr(Options.ListeRemises[NoRemise], 3, '|');
    vCell := vWorksheet.Range[aRange];
    aValue := vCell.Value;

        try
        //si la valeur r�cup�r�e n'est pas num�rique, cela permet de ne pas s'arr�ter.
        Grid_Remises.cells[1, NoRemise + 1] := aValue * 100;
        except
        aValue := aValue;
        end;
    End;
  except
  BBS_ShowMessage('L''import n''a pas pu se finaliser', mtError, [mbOK], 0)
  end;
  vMSExcel.Quit;
  vMSExcel := unassigned;
End;
end;
{ ************************************************************************************************************************************************** }
procedure TDlg_Options.AdvEdit_IteratifConvergenceValueValidate(Sender: TObject;
  Value: string; var IsValid: Boolean);
begin
  if Not(IsValid) then
    begin
    AdvEdit_IteratifConvergence.FloatValue := Min(AdvEdit_IteratifConvergence.FloatValue, AdvEdit_IteratifConvergence.MaxFloatValue);
    AdvEdit_IteratifConvergence.FloatValue := Max(AdvEdit_IteratifConvergence.FloatValue, AdvEdit_IteratifConvergence.MinFloatValue);
    end;
end;
{ ************************************************************************************************************************************************** }
procedure TDlg_Options.AdvEdit_IteratifMaxValueValidate(Sender: TObject;
  Value: string; var IsValid: Boolean);
begin
  if Not(IsValid) then
    begin
    AdvEdit_IteratifMax.IntValue := Min(AdvEdit_IteratifMax.IntValue, AdvEdit_IteratifMax.MaxValue);
    AdvEdit_IteratifMax.IntValue := Max(AdvEdit_IteratifMax.IntValue, AdvEdit_IteratifMax.MinValue);
    end;
end;
{ ************************************************************************************************************************************************** }
procedure TDlg_Options.BtnBitmapEnteteClick(Sender: TObject);
begin
if OpenPictureDialog1.Execute then
begin
ImagePrintEnTete.Picture.LoadFromFile(OpenPictureDialog1.FileName);
DeleteImagePerso := False;
end
else
if Not(FileExists(Options.GetPathLogo)) then
DeleteImagePerso := True;
end;
{ ************************************************************************************************************************************************** }
procedure TDlg_Options.BtnLogoDeleteClick(Sender: TObject);
begin
DeleteImagePerso := True;
ImagePrintEnTete.Picture.Bitmap.Assign(Nil);
//ImagePrintEnTete.Picture.Bitmap := Nil;
end;
{ ************************************************************************************************************************************************** }
End.
