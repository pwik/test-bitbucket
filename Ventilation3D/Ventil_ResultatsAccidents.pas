Unit Ventil_ResultatsAccidents;
   
{$Include 'Ventil_Directives.pas'}

Interface

Uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Grids, BaseGrid, AdvGrid, AdvObj;

Type
  TDlg_AffichAccidents = class(TForm)
    Grid_accidentsAssocies: TAdvStringGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Var
  Dlg_AffichAccidents: TDlg_AffichAccidents;

Implementation

{$R *.dfm}

Procedure TDlg_AffichAccidents.FormCreate(Sender: TObject);
Begin
  Grid_accidentsAssocies.MergeCells(0, 0, 2, 1);
  Grid_accidentsAssocies.Cells[0, 0] := 'Accidents associ�s au tron�on';
  Grid_accidentsAssocies.CellProperties[0, 0].Alignment := taCenter;
  Grid_accidentsAssocies.Cells[0, 1] := 'Type d''accident';
  Grid_accidentsAssocies.Cells[1, 1] := 'Dz�ta';
  Grid_accidentsAssocies.CellProperties[1, 1].Alignment := taCenter;
End;

End.
