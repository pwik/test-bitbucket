Unit Ventil_ConsultationSystemes;
          
{$Include 'Ventil_Directives.pas'}

Interface

Uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, Buttons, ExtCtrls,
  Ventil_SystemeVMC,
  Grids, BaseGrid, AdvGrid, ComCtrls, AdvPageControl, AdvObj, tmsAdvGridExcel;

Type
  TDlg_ConsultationSystemes = class(TForm)
    PageControl_Systemes: TAdvPageControl;
    Grid_Systeme: TAdvStringGrid;
    advtbsht1: TAdvTabSheet;
    AdvGridExcelIO1: TAdvGridExcelIO;
    procedure Grid_SystemeGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure PageControl_SystemesChange(Sender: TObject);
    procedure Grid_SystemeCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
  private
    Procedure FillGrid(m_Systeme : TVentil_Systeme);
  public
    { Public declarations }
    Procedure Initialisation(_WithFabName : Boolean = False);
  end;

Var
  Dlg_ConsultationSystemes: TDlg_ConsultationSystemes;

Procedure ConsultationDesSystemes;

Implementation

Uses
  BbsVPE,
  Ventil_Declarations;
{
  Ventil_Const, Ventil_Etude, Ventil_Types;
}
{$R *.dfm}

{ ************************************************************************************************************************************************* }
Procedure ConsultationDesSystemes;
Begin
  Application.CreateForm(TDlg_ConsultationSystemes, Dlg_ConsultationSystemes);
  Dlg_ConsultationSystemes.WindowState := wsMaximized;
  Dlg_ConsultationSystemes.Initialisation;
{$IfDef AERAU_CLIMAWIN}
        if (Etude <> nil) and (Etude.Batiment <> nil) then
                Begin
                Dlg_ConsultationSystemes.Caption := 'Consultation des syst�me de ventilation pour le fabricant ';
                Dlg_ConsultationSystemes.Caption := Dlg_ConsultationSystemes.Caption + FabricantsVMC.Fabricant(Etude.Batiment.CodeFabricant).Reference;
                End
        else
                Dlg_ConsultationSystemes.Caption := 'Consultation des syst�me de ventilation';
{$EndIf}

  Dlg_ConsultationSystemes.ShowModal;
  FreeAndNil(Dlg_ConsultationSystemes);
End;
{ ************************************************************************************************************************************************* }
Procedure TDlg_ConsultationSystemes.FillGrid(m_Systeme : TVentil_Systeme);
Var
  tempStringGrid : TStringGrid;
  cptCol : Integer;
  cptRow : Integer;
  ColorPantone372 : TColor;
Begin
  ColorPantone372 := RGB(215, 233, 161);
  tempStringGrid := TStringGrid.Create(Nil);
  m_Systeme.FillGrid(tempStringGrid);
  Grid_Systeme.WordWrap := False;
  Grid_Systeme.ColCount := tempStringGrid.ColCount;
  Grid_Systeme.RowCount := tempStringGrid.RowCount;
    for cptCol := 0 to tempStringGrid.ColCount - 1 do
      for cptRow := 0 to tempStringGrid.RowCount - 1 do
        Grid_Systeme.Objects[cptCol, cptRow] := tempStringGrid.Objects[cptCol, cptRow];
  FreeAndNil(tempStringGrid);
    for cptCol := 0 to Grid_Systeme.ColCount - 1 do
      begin
      for cptRow := 0 to Grid_Systeme.RowCount - 1 do
        begin
        Grid_Systeme.Fonts[cptCol, cptRow] := TCelluleImp(Grid_Systeme.Objects[cptCol, cptRow]).IFont;
        Grid_Systeme.Cells[cptCol, cptRow] := TCelluleImp(Grid_Systeme.Objects[cptCol, cptRow]).ITexte;
        Grid_Systeme.CellProperties[cptCol, cptRow].BrushColor := TCelluleImp(Grid_Systeme.Objects[cptCol, cptRow]).IBrushColor;
          {$IFDEF ACTHYS_DIMVMBP}
            if Grid_Systeme.CellProperties[cptCol, cptRow].BrushColor = ColorPantone372 then
              Grid_Systeme.CellProperties[cptCol, cptRow].BrushColorTo := clWhite;
          {$EndIf}
        TCelluleImp(Grid_Systeme.Objects[cptCol, cptRow]).Destroy;

        Grid_Systeme.CellProperties[cptCol, cptRow].Alignment := taCenter;
        Grid_Systeme.CellProperties[cptCol, cptRow].VAlignment := vtaCenter;

        end;
      end;

  Grid_Systeme.Parent := PageControl_Systemes.ActivePage;

  Grid_Systeme.ColumnSize.Stretch := False;
  Grid_Systeme.ColumnSize.StretchColumn := -1;
  Grid_Systeme.ColumnSize.StretchAll := False;
  Grid_Systeme.FixedCols := 1;
  Grid_Systeme.FixedRows := 1;
  Grid_Systeme.Options := Grid_Systeme.Options + [goColSizing, goRowSizing];

  Grid_Systeme.AutoSize := True;
//  Grid_Systeme.AutoSizeRows(true, 12);
  Grid_Systeme.AutoSizeCells(True, 12, 12);

end;
{ ************************************************************************************************************************************************* }
Procedure TDlg_ConsultationSystemes.Grid_SystemeGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
Begin
  HAlign := taCenter;
//  VAlign := vtaBottom;
  VAlign := vtaCenter;
End;
{ ************************************************************************************************************************************************* }
Procedure TDlg_ConsultationSystemes.Initialisation(_WithFabName : Boolean = False);
Var
  m_Tab: TAdvTabSheet;
  m_No : SmallInt;
Begin
{$IfDef OLD_ATVENTIL}
  Grid_Systeme.ColWidths[0] := Round(Grid_Systeme.ColWidths[0] / 2);
  Grid_Systeme.ColWidths[3] := Grid_Systeme.ColWidths[3] + Round(Grid_Systeme.ColWidths[0] / 2) + 10;
{$Else}
{$EndIf}
Grid_Systeme.Parent := Self;
//  For m_No := 0 To SystemesVMC.Count - 1 Do If Not SystemesVMC[m_No].Hidden Then
  For m_No := PageControl_Systemes.PageCount - 1 DownTo 0 do
    PageControl_Systemes.Pages[m_No].Free;
  For m_No := 0 To SystemesVMC.Count - 1 Do If Not SystemesVMC[m_No].NotInConsult Then
{$IfDef AERAU_CLIMAWIN}
    if (Etude <> nil) and (Etude.Batiment <> nil) and (UpperCase(SystemesVMC[m_No].CodeFab) = UpperCase(Etude.Batiment.CodeFabricant)) or
    ((Etude = nil) or (Etude.Batiment = nil)) then
{$EndIf}
  Begin
    m_Tab := TAdvTabSheet.Create(PageControl_Systemes);
    m_Tab.Tag := m_No;
    m_Tab.Caption := SystemesVMC[m_No].Reference;
{$IfDef AERAU_CLIMAWIN}
    if ((Etude = nil) or (Etude.Batiment = nil)) then
{$Else}
    if _WithFabName then
{$EndIf}
      m_Tab.Caption := SystemesVMC[m_No].Fabricant + ' - ' + m_Tab.Caption;

    m_Tab.AdvPageControl := PageControl_Systemes;
  End;

  if SystemesVMC.Count > 0 then
    FillGrid(SystemesVMC[PageControl_Systemes.Pages[0].Tag]);
End;
{ ************************************************************************************************************************************************* }
Procedure TDlg_ConsultationSystemes.PageControl_SystemesChange(Sender: TObject);
Begin
  If PageControl_Systemes.ActivePageIndex > - 1 Then FillGrid(SystemesVMC[PageControl_Systemes.ActivePage.Tag]);
End;
{ ************************************************************************************************************************************************* }

procedure TDlg_ConsultationSystemes.Grid_SystemeCanEditCell(
  Sender: TObject; ARow, ACol: Integer; var CanEdit: Boolean);
begin
CanEdit := False;
end;
{ ************************************************************************************************************************************************* }
End.
