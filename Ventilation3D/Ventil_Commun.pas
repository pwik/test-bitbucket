Unit Ventil_Commun;
 
{$Include 'Ventil_Directives.pas'}

Interface
Uses
  ShellApi, SysUtils, Types, Graphics, BbsVPE;

{ Op�rations de fichiers }
Function Ventil_CreateFile(_Fichier: String): Boolean;
Function Ventil_RenameFile(_Source, _Destination: String): Boolean;
Function Ventil_CopyFile(_Source, _Destination: String): Boolean;
Function Ventil_DeleteFile(_Source: String; _VersCorbeille: Boolean = False): Word;
Function Ventil_FichierSurDisque(_Fichier: String): Boolean;
Function TempInternalDir: String;

{ Pour le dessin de courbe }
Procedure PtCalc2PtDessine(_Xdeb, _Ydeb: Real; Var _Xret, _Yret: Integer; _MaxX, _MaxY: Real; _Rect: TRect);
Procedure PtCalc2PtDessineVPE(_Xdeb, _Ydeb: Real; Var _Xret, _Yret : Double; _MaxX, _MaxY: Real; _Rect: TRectVPE);

Function Bool2OUINON(_ValBool: Boolean): Integer;
Function OUINON2Bool(_ValOUINON: Integer): Boolean;

Implementation
Uses
  Ventil_Const, Forms;

{ ************************************************************************************************************************************************** }
Function Ventil_CreateFile(_Fichier: String): Boolean;
Var
        T:TextFile;
Begin
AssignFile(T, _Fichier);
Rewrite(T); // Cr�e le fichier s'il n'existe pas le r��cris autrement
WriteLN(T);
CloseFile(T);
End;
{ ************************************************************************************************************************************************** }
Function Ventil_RenameFile(_Source, _Destination: String): Boolean;
Var
  fos : TSHFileOpStruct;
Begin
  FillChar(fos, SizeOf(fos), 0);
  With fos Do Begin
    wFunc := FO_RENAME;
    pFrom := PChar(_Source + #0);
    pTO   := PChar(_Destination + #0);
    fFlags:= FO_RENAME Or FOF_SILENT;
  End;
  Result := (0 = ShFileOperation(fos));
End;
{ ************************************************************************************************************************************************** }
Function Ventil_DeleteFile(_Source: String; _VersCorbeille: Boolean = False): Word;
Var
  fos : TSHFileOpStruct;
Begin
  FillChar(fos, SizeOf(fos), 0);
  With fos Do Begin
    wFunc := FO_DELETE;
    pFrom := PChar(_Source + #0);
    If _VersCorbeille Then fFlags:= FO_DELETE Or FOF_NOCONFIRMATION Or FOF_SILENT Or FOF_ALLOWUNDO
                      Else fFlags:= FO_DELETE Or FOF_NOCONFIRMATION Or FOF_SILENT;
  End;
  Result := ShFileOperation(fos);
End;
{ ************************************************************************************************************************************************** }
Function Ventil_CopyFile(_Source, _Destination: String): Boolean;
Var
  fos : TSHFileOpStruct;
Begin
  Result := False;
  If DirectoryExists(ExtractFilePath(_Destination)) Then Begin
    FillChar(fos, SizeOf(fos), 0);
    With fos Do Begin
      wFunc := FO_COPY;
      pFrom := PChar(_Source + #0);
      pTO   := PChar(_Destination + #0);
      fFlags:= FOF_ALLOWUNDO Or FOF_NOCONFIRMATION Or FOF_SILENT;
    End;
    Result := (0 = ShFileOperation(fos));
  End;
End;
{ ************************************************************************************************************************************************** }
Function Ventil_FichierSurDisque(_Fichier: String): Boolean;
Var
  sr: TSearchRec;
Begin
  Result := FindFirst(_Fichier, faAnyFile - faDirectory, sr) = 0;
End;
{ ************************************************************************************************************************************************* }
Function TempInternalDir: String;
Var
  m_Str : String;
Begin
  m_Str := ExtractFilePath(Application.ExeName);
  m_Str := m_Str + 'Temp';
  If Not DirectoryExists(m_Str) Then CreateDir(m_Str);
  Result := m_Str + '\';
End;
{ ************************************************************************************************************************************************** }


  { ************************************************************************************************************************************************** }
Procedure PtCalc2PtDessine(_Xdeb, _Ydeb: Real; Var _Xret, _Yret: Integer; _MaxX, _MaxY: Real; _Rect: TRect);
const
{$IfDef VIM_OPTAIR}
        PcOccupationX = 0.86;
        PcOccupationY = 0.90;
{$Else}
        PcOccupationX = 0.8;
        PcOccupationY = 0.8;
{$EndIf}
Begin
  If _MaxX <> 0 Then _Xret := Round(_Xdeb * (_Rect.Right - _Rect.Left) / (_MaxX) * PcOccupationX)
                Else _Xret := 0;
  If _MaxY <> 0 Then _Yret := Round(_Ydeb * (_Rect.Bottom - _Rect.Top) / (_MaxY) * PcOccupationY)
                Else _Yret := 0;
End;
{ ************************************************************************************************************************************************** }
Procedure PtCalc2PtDessineVPE(_Xdeb, _Ydeb: Real; Var _Xret, _Yret : Double; _MaxX, _MaxY: Real; _Rect: TRectVPE);
const
{$IfDef VIM_OPTAIR}
        PcOccupationX = 0.86;
        PcOccupationY = 0.90;
{$Else}
        PcOccupationX = 0.8;
        PcOccupationY = 0.8;
{$EndIf}
Begin
  If _MaxX <> 0 Then _Xret := _Xdeb * (_Rect.Right - _Rect.Left) / (_MaxX) * PcOccupationX
                Else _Xret := 0;
  If _MaxY <> 0 Then _Yret := _Ydeb * (_Rect.Bottom - _Rect.Top) / (_MaxY) * PcOccupationY
                Else _Yret := 0;
End;
{ ************************************************************************************************************************************************** }

{ ************************************************************************************************************************************************** }
Function Bool2OUINON(_ValBool: Boolean): Integer;
Begin
  If _ValBool Then Result := c_OUI
              Else Result := c_NON;
End;
{ ************************************************************************************************************************************************** }
Function OUINON2Bool(_ValOUINON: Integer): Boolean;
Begin
  Result := _ValOUINON = c_OUI;
End;
{ ************************************************************************************************************************************************** }
End.
